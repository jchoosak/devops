package TommyBahamaOutletRepository;

public class OutletConfirmationPage {
	
	private final String theByLinkEmailGS = "email Guest Services";
	private final String theURL = "https://";
	private final String theOrderNumber = "//div[@id='content']/div[2]/div/p";
	private final String theShippingAmount = "//table[@id='billing-summary']/tbody/tr/td[3]/span[2]";
	private final String theByIdTotalShipping = "total_shipping";
	private final String thePageName = "ConfirmationPage";
	private final String theSubTotal = "order_subtotal";
	private final String theSplitTenderSubTotal = "//table[@id='billing-summary']/tbody/tr/td[2]/span[4]";
	private final String theTotalTax = "total_tax";
	private final String theByIdDuty = "spanDuty";
	
	private final String theTotalShippingAmount = "//span[@id='total_shipping']";
	private final String theTotalAmount = "total_order";

	public String getTheByLinkEmailGS() {
		return theByLinkEmailGS;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheOrderNumber() {
		return theOrderNumber;
	}

	public String getTheShippingAmount() {
		return theShippingAmount;
	}

	public String getTheByIdTotalShipping() {
		return theByIdTotalShipping;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheSubTotal() {
		return theSubTotal;
	}

	public String getTheSplitTenderSubTotal() {
		return theSplitTenderSubTotal;
	}

	public String getTheTotalTax() {
		return theTotalTax;
	}

	public String getTheByIdDuty() {
		return theByIdDuty;
	}

	public String getTheTotalShippingAmount() {
		return theTotalShippingAmount;
	}

	public String getTheTotalAmount() {
		return theTotalAmount;
	}

}
