package TommyBahamaOutletRepository;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class OutletCSC {

	private final String theByNameUsernameInput = "LOGIN<>userid";
	private final String theMerchUsername = "jwest";
	private final String theQAUsername = "csr";
	private final String theByNamePswrdInput = "LOGIN<>password";
	private final String theMerchPswrd = "martini123";
	private final String theQAPswrd = "martini";
	private final String theExpectedCCStatus = "Authorization complete";
	private final String theByNameLogInSubmit = "login";
	private final String theByNameCCFrame = "cc_frame_content";
	private final String theByNameOrderSearch = "ORDER_SEARCH<>orderNum";
	private final String theByNameOrderSearchSubmit = "find_order";
	private final String theOrderTax = "//div[2]/div/div/div[2]/div/div/div[2]/span[2]";
	private final String theDutyOrderTax = "//div[@id='idContentContainer']/table[2]/tbody/tr/td/table[2]/tbody/tr[6]/td[2]";
	// div[@id='idContentContainer']/table[2]/tbody/tr/td/table[2]/tbody/tr[7]/td[2]
	private final String theDuty = "//div[@id='idContentContainer']/table[2]/tbody/tr/td/table[2]/tbody/tr[6]/td[2]";
	private final String theDutyOrderTotal = "//div[@id='idContentContainer']/table[2]/tbody/tr/td/table[2]/tbody/tr[8]/td[2]";
	// span[@id='idOrderTotal']
	private final String theOrderTotal = "//div[2]/div/div/div[2]/div/div/div[5]/span[2]";
	// private final String theOrderTotal = "//span[@id='idOrderTotal']";
	private final String theOrderCCStatus = "//div[@id='idContentContainer']/table[2]/tbody/tr[3]/td/table[2]/tbody/tr[2]/td[5]";
	private final String theByNameUnBlockOrderBtn = "unblock_order";
	private final String theByNameWDEmailSearch = "USER_ACCOUNT_SEARCH<>email_search";
	private final String theSearchContacts = "css=input[name='cc_home_contacts_search']";
	private final String theByCssWDSearchContacts = "input[name='cc_home_contacts_search']";
	private final String theContact = "link=West, Jack C";
	private final String theByLinkTextWDContact = "West, Jack C";
	private final String theCreateOrder = "link=Create an order";
	private final String theByLinkTextWDCreateOrder = "Create an order";
	private final String theShippingType = "id=shippingMethodList";
	private final String theFedXGround = "FDXGROUND";
	private final String theFedXThreeDay = "FDX3DAY";
	private final String theFedXTwoDay = "FDX2DAY";
	private final String theFedXNextDay = "FDXNEXTDAY";
	private final String theSKUSearch = "name=ADD_CART_ITEM<>skuCode";
	private final String theSandelSKU = "TFW2001";
	private final String theAddSKUBtn = "name=add_search";
	private final String theAddSkuWindow = "name=cc_dialog";
	private final String theNameSkuSearchBtn = "name=CC_search_ok";
	private final String theAddSingleSkuBtn = "id=add_single_sku";
	private final String theByNameWDGiftServiceBtn = "button_gift_services";
	private final String theByNameWDGiftServiceWindow = "window";
	private final String theByIdWDGiftServiceToInput = "to_note_0";
	private final String theByIdWDGiftServiceFromInput = "from_note_0";
	private final String theByIdWDGiftServiceMessageInput = "message_note_0";
	private final String theByNameWDGiftServiceCreateBtn = "create";
	private final String theWDCreditCardVerificationNumberInput = "CREDIT_CARD_ARRAY<>cardVerifyNum";
	private final String theCreditCardVerificationNumberInput = "//input[@name='CREDIT_CARD_ARRAY<>cardVerifyNum']";
	private final String theCreditCardTotalInput = "CREDIT_CARD_ARRAY<>amount";
	private final String theByXpathCreditCardTotalInput = "//div[@id='idContentContainer']/table/tbody/tr[3]/td/table[2]/tbody/tr[2]/td[7]/input[2]";
	private final String theGifCardTotalInput = "ACCOUNT_ARRAY<>amount";
	private final String theByXPathGiftCardTotalInput = "//input[@name='ACCOUNT_ARRAY<>amount']";

	// input[@name='ACCOUNT_ARRAY<>amount']
	private final String theGiftCardNumberInput = "ACCOUNT_ARRAY<>accountNumber";
	private final String theGiftCardPinInput = "ACCOUNT_ARRAY<>pinNumber";
	private final String theByXPathGiftCardPinInput = "//tr[3]/td[6]/input[3]";

	private final String theSubmitOrderBtn = "div > button[name='submit_order']";
	private final String theOrderSummaryHeader = "span.bmsSummaryHeader";
	private final String theByXPathOrderSummaryHeader = "//td[@id='idMainViewContainer']/form/table/tbody/tr/td/span";
    private final String theByIdOrderTotal = "idOrderTotal";
	private final String theAddProductTitle = "Add Products to Order";
	private final String theSKUSearchInput = "name=SEARCH_INPUT<>searchFor";
	private final String theBrowseCatalogLink = "link=Browse Catalog";
	private final String theWDBrowseCatalogLink = "Browse Catalog";
	private final String theAccesoriesLink = "xpath=(//a[contains(text(),'Accessories')])[2]";
	private final String theByXPathWDAccesoriesLink = "(//a[contains(text(),'Accessories')])[2]";
	private final String theByLinkTextWDSandalShoeLink = "Shoes & Sandals";
	private final String theByLinkTextWDSandalLink = "TFW00036";
	private final String theByNameWDSandalQty = "ADD_CART_ITEM_ARRAY<>qtyToBuy";
	private final String theByIdWDAddToOrderBtn = "add_button";
	private final String theReCalcOrderBtn = "(//button[@name='recalc_button'])[2]";
	private final String theSelectPaymentType = "idSelectItems";
	private final String theGiftCardType = "Gift Card";
	private final String theAddPaymentTypeBtn = "addPaymentID";
	private final String theCCcreatedOrderTotal = "//span[@id='idOrderTotal']";
	// private final String theCCcreatedOrderTotal =
	// "//div[@id='idContentContainer']/table/tbody/tr[3]/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td[2]";
	private final String theUpdatePaymentBtn = "updatePaymentID";
	private final String theWDShippingOptions = "shippingMethodList";
	
	private final String thePageName = "ContactCenter";
	
	private WebDriver driver;
	
	private OutletWebdriverEnvironment myEnvironment;
	
	private String testEnvironment;
	
	private String tax;
	
	
	
	private String orderNumber;
	
	private String testName;
	
	private String browser;
	
	
	public OutletCSC()
	{
		
	}
	
	public OutletCSC(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment, String theTestEnvironment,
			String theTax, String theOrderNumber, String theTest,
			String theBrowser)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		testEnvironment = theTestEnvironment;
		tax = theTax;
		orderNumber = theOrderNumber;
		testName = theTest;
		browser = theBrowser;
	}
	

	public String getTheByNameUsernameInput() {
		return theByNameUsernameInput;
	}

	public String getTheMerchUsername() {
		return theMerchUsername;
	}

	public String getTheQAUsername() {
		return theQAUsername;
	}

	public String getTheByNamePswrdInput() {
		return theByNamePswrdInput;
	}

	public String getTheMerchPswrd() {
		return theMerchPswrd;
	}

	public String getTheQAPswrd() {
		return theQAPswrd;
	}

	public String getTheExpectedCCStatus() {
		return theExpectedCCStatus;
	}

	public String getTheByNameLogInSubmit() {
		return theByNameLogInSubmit;
	}

	public String getTheByNameCCFrame() {
		return theByNameCCFrame;
	}

	public String getTheByNameOrderSearch() {
		return theByNameOrderSearch;
	}

	public String getTheByNameOrderSearchSubmit() {
		return theByNameOrderSearchSubmit;
	}

	public String getTheOrderTax() {
		return theOrderTax;
	}

	public String getTheDutyOrderTax() {
		return theDutyOrderTax;
	}

	public String getTheDuty() {
		return theDuty;
	}

	public String getTheDutyOrderTotal() {
		return theDutyOrderTotal;
	}

	public String getTheOrderTotal() {
		return theOrderTotal;
	}

	public String getTheOrderCCStatus() {
		return theOrderCCStatus;
	}

	public String getTheByNameUnBlockOrderBtn() {
		return theByNameUnBlockOrderBtn;
	}

	public String getTheByNameWDEmailSearch() {
		return theByNameWDEmailSearch;
	}

	public String getTheSearchContacts() {
		return theSearchContacts;
	}

	public String getTheByCssWDSearchContacts() {
		return theByCssWDSearchContacts;
	}

	public String getTheContact() {
		return theContact;
	}

	public String getTheByLinkTextWDContact() {
		return theByLinkTextWDContact;
	}

	public String getTheCreateOrder() {
		return theCreateOrder;
	}

	public String getTheByLinkTextWDCreateOrder() {
		return theByLinkTextWDCreateOrder;
	}

	public String getTheShippingType() {
		return theShippingType;
	}

	public String getTheFedXGround() {
		return theFedXGround;
	}

	public String getTheFedXThreeDay() {
		return theFedXThreeDay;
	}

	public String getTheFedXTwoDay() {
		return theFedXTwoDay;
	}
	
	public String getTheFedXNextDay() {
		return theFedXNextDay;
	}

	public String getTheSKUSearch() {
		return theSKUSearch;
	}

	public String getTheSandelSKU() {
		return theSandelSKU;
	}

	public String getTheAddSKUBtn() {
		return theAddSKUBtn;
	}

	public String getTheAddSkuWindow() {
		return theAddSkuWindow;
	}

	public String getTheNameSkuSearchBtn() {
		return theNameSkuSearchBtn;
	}

	public String getTheAddSingleSkuBtn() {
		return theAddSingleSkuBtn;
	}

	public String getTheByNameWDGiftServiceBtn() {
		return theByNameWDGiftServiceBtn;
	}

	public String getTheByNameWDGiftServiceWindow() {
		return theByNameWDGiftServiceWindow;
	}

	public String getTheByIdWDGiftServiceToInput() {
		return theByIdWDGiftServiceToInput;
	}

	public String getTheByIdWDGiftServiceFromInput() {
		return theByIdWDGiftServiceFromInput;
	}

	public String getTheByIdWDGiftServiceMessageInput() {
		return theByIdWDGiftServiceMessageInput;
	}

	public String getTheByNameWDGiftServiceCreateBtn() {
		return theByNameWDGiftServiceCreateBtn;
	}

	public String getTheWDCreditCardVerificationNumberInput() {
		return theWDCreditCardVerificationNumberInput;
	}

	public String getTheCreditCardVerificationNumberInput() {
		return theCreditCardVerificationNumberInput;
	}

	public String getTheCreditCardTotalInput() {
		return theCreditCardTotalInput;
	}

	public String getTheByXpathCreditCardTotalInput() {
		return theByXpathCreditCardTotalInput;
	}

	public String getTheGifCardTotalInput() {
		return theGifCardTotalInput;
	}

	public String getTheByXPathGiftCardTotalInput() {
		return theByXPathGiftCardTotalInput;
	}

	public String getTheGiftCardNumberInput() {
		return theGiftCardNumberInput;
	}

	public String getTheGiftCardPinInput() {
		return theGiftCardPinInput;
	}

	public String getTheByXPathGiftCardPinInput() {
		return theByXPathGiftCardPinInput;
	}

	public String getTheSubmitOrderBtn() {
		return theSubmitOrderBtn;
	}

	public String getTheOrderSummaryHeader() {
		return theOrderSummaryHeader;
	}

	public String getTheByXPathOrderSummaryHeader() {
		return theByXPathOrderSummaryHeader;
	}

	public String getTheAddProductTitle() {
		return theAddProductTitle;
	}

	public String getTheSKUSearchInput() {
		return theSKUSearchInput;
	}

	public String getTheBrowseCatalogLink() {
		return theBrowseCatalogLink;
	}

	public String getTheWDBrowseCatalogLink() {
		return theWDBrowseCatalogLink;
	}

	public String getTheAccesoriesLink() {
		return theAccesoriesLink;
	}

	public String getTheByXPathWDAccesoriesLink() {
		return theByXPathWDAccesoriesLink;
	}

	public String getTheByLinkTextWDSandalShoeLink() {
		return theByLinkTextWDSandalShoeLink;
	}

	public String getTheByLinkTextWDSandalLink() {
		return theByLinkTextWDSandalLink;
	}

	public String getTheByNameWDSandalQty() {
		return theByNameWDSandalQty;
	}

	public String getTheByIdWDAddToOrderBtn() {
		return theByIdWDAddToOrderBtn;
	}

	public String getTheReCalcOrderBtn() {
		return theReCalcOrderBtn;
	}

	public String getTheSelectPaymentType() {
		return theSelectPaymentType;
	}

	public String getTheGiftCardType() {
		return theGiftCardType;
	}

	public String getTheAddPaymentTypeBtn() {
		return theAddPaymentTypeBtn;
	}

	public String getTheCCcreatedOrderTotal() {
		return theCCcreatedOrderTotal;
	}

	public String getTheUpdatePaymentBtn() {
		return theUpdatePaymentBtn;
	}

	public String getTheWDShippingOptions() {
		return theWDShippingOptions;
	}

	public void checkOrderInCSC(String theTotal) throws InterruptedException {
		WebElement ce;
		
		System.out.println("We are getting into the CSC method.");
		OutletSignInPage mySIO = new OutletSignInPage(driver, myEnvironment, testName);
		OutletCSCOrderTab myOrderTab = new OutletCSCOrderTab(myEnvironment, testName);
		CSCMenu myCSCMenu = new CSCMenu();
		// which contact center are we using
    	driver.get(System.getenv("SELENIUM_TEST_CSC_URL"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
	
		System.out.println("We made it to the sign in method.");
		mySIO.signInCSC("jack.west@tommybahama.com", "testing123");
		
		myEnvironment.waitForURL("index");
		
		if(driver.getCurrentUrl().contains("dev"))
			driver.navigate().refresh();
	    
		ce = myEnvironment.waitForDynamicElement(By.linkText(myCSCMenu.getTheByLinkFindOrderLink()));

		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				testName, "CSCLandingPage", "");
		
		ce.click();	
	
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myCSCMenu.getTheByXpathSubmitOrderSearchButton()));
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				testName, "SearchOrderForm", "");
		 
		ce.click();	

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myCSCMenu.getTheByXPathSearchSelectButton()));		
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				testName, "SearchOrderFormWithResults", "");		
	
		ce.click();

	ce = WaitTool.waitForElement(driver, 
			By.xpath(myOrderTab.getTheByXpathCreateNewTicketButton()), 10);

	myEnvironment.waitForDocumentReadyState();

	Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
			By.xpath(myOrderTab.getTheByXpathCreateNewTicketButton()), "Create New Ticket", 10));
	
		
	myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
			testName, "OrderTab", "");	
	
	
	String theTax = driver.findElement(
			By.xpath(myOrderTab.getTheOrderTax())).getText();
	
	System.out.println("This is the CSC tax: 	" + theTax);
	System.out.println("This is the WS tax: 	" + tax);
//	String theDuty = driver.findElement(
//			By.id(myPreviewObjs.getTheByIdDuty())).getText();
	String total = driver.findElement(
			By.xpath(myOrderTab.getTheOrderTotal())).getText();
	
	System.out.println("This is the WS total amount: 	" + theTotal);
	System.out.println("This is the CSC total amount: 	" + total);
	
	Assert.assertTrue(total.equalsIgnoreCase(theTotal));
	Assert.assertTrue(theTax.equalsIgnoreCase(tax));
	
	Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
			By.xpath(myOrderTab.getTheByXpathRefundButton()), "Refund Order", 10));

	Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
			By.xpath(myOrderTab.getTheByXpathWaiveShippingButton()), "Submit", 10));
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myOrderTab.getTheByXpathWaiveShippingCheckbox()));
	ce.click();
	
	
	myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
			testName, "OrderTab2", "");	
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByIdOrderTotal() {
		return theByIdOrderTotal;
	}

}
