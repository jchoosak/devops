package TommyBahamaOutletRepository;

public class OutletDeliveryMethodPage {
	
	private final String theByIdContinueCheckoutButton = "chooseDeliveryMethod_continue_button";
	private final String thePageName = "DeliveryMethodPage";

	public String getTheByIdContinueCheckoutButton() {
		return theByIdContinueCheckoutButton;
	}

	public String getThePageName() {
		return thePageName;
	}
	

}
