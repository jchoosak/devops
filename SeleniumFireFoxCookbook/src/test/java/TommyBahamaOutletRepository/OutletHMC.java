package TommyBahamaOutletRepository;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;



public class OutletHMC {
	
	private final String theByXPathSubmitLogin = "//span";
	
	
	
	private final String theByIdSearchByJob = "Content/AllInstancesSelectEditor[in Content/GenericCondition[CronJob.job]]_select";
	
	
	private final String theSiteDetailsCronJobIndex = "42";
	
/*	private final String theByIdSiteDetailsFirstCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap01]_span";
	private final String theByIdSiteDetailsSecondCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap02]_span";
	private final String theByIdSiteDetailsThirdCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap03]_span";
	private final String theByIdSiteDetailsFourthCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap04]_span";*/
	
	private final String theByIdSiteDetailsFirstCronJob = "Content/OrganizerListEntry[Tommy Bahama Site Details Job Ap01][search 2]_img";
	private final String theByIdSiteDetailsSecondCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap02]_span";
	private final String theByIdSiteDetailsThirdCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap03]_span";
	private final String theByIdSiteDetailsFourthCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap04]_span";
	
	private final String theByIdSubmitSearchButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	
	private final String theByIdSiteDetailsStartFirstCronJob = "Content/GenericItemChip$1[action.performcronjob]";
	private final String theByIdSiteDetailsStartSecondCronJob = "Content/GenericItemChip$1[action.performcronjob][1]_label";
	private final String theByIdSiteDetailsStartThirdCronJob = "Content/GenericItemChip$1[action.performcronjob][2]_label";
	private final String theByIdSiteDetailsStartFourthCronJob = "Content/GenericItemChip$1[action.performcronjob][3]_label";
	
	
	private final String theByIdSearchResultsButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	private final String theByIdHomeButton = "Explorer[mcc]_label";
	private final String theByIdEndSessionButton = "Toolbar/ImageToolbarAction[closesession]_img";
	
	
	private WebDriver driver;
	private int cronJobsRan = 1;
	private String testName;
	
	private OutletWebdriverEnvironment myEnvironment;
	
	private OutletHmcLeftNav myHmcLeftNav;

	public OutletHMC(){
		
	}
	
    public OutletHMC(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment, OutletHmcLeftNav theHmcLeftNav, String theTestName){
    	
    	driver = theDriver;
    	myEnvironment = theEnvironment;
    	myHmcLeftNav = theHmcLeftNav;
    	testName = theTestName;
    	
		
	}
	
	
	public String getTheByXPathSubmitLogin() {
		return theByXPathSubmitLogin;
	}




	public String getTheByIdSearchByJob() {
		return theByIdSearchByJob;
	}




	public String getTheSiteDetailsCronJobIndex() {
		return theSiteDetailsCronJobIndex;
	}




	public String getTheByIdSiteDetailsFirstCronJob() {
		return theByIdSiteDetailsFirstCronJob;
	}




	public String getTheByIdSiteDetailsSecondCronJob() {
		return theByIdSiteDetailsSecondCronJob;
	}




	public String getTheByIdSiteDetailsThirdCronJob() {
		return theByIdSiteDetailsThirdCronJob;
	}




	public String getTheByIdSiteDetailsFourthCronJob() {
		return theByIdSiteDetailsFourthCronJob;
	}




	public String getTheByIdSubmitSearchButton() {
		return theByIdSubmitSearchButton;
	}




	public String getTheByIdSiteDetailsStartFirstCronJob() {
		return theByIdSiteDetailsStartFirstCronJob;
	}




	public String getTheByIdSiteDetailsStartSecondCronJob() {
		return theByIdSiteDetailsStartSecondCronJob;
	}




	public String getTheByIdSiteDetailsStartThirdCronJob() {
		return theByIdSiteDetailsStartThirdCronJob;
	}




	public String getTheByIdSiteDetailsStartFourthCronJob() {
		return theByIdSiteDetailsStartFourthCronJob;
	}




	public String getTheByIdSearchResultsButton() {
		return theByIdSearchResultsButton;
	}
	
	public void fireSiteDetailsCronJob(String theJob) throws InterruptedException {
		
		if(driver.getCurrentUrl().contains("dev"))
		driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;		
    	// log into the hmc
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheByXPathSubmitLogin()));
		ce.click();		
		myEnvironment.waitForURL("wid");
		// click the System folder in the hmc
		ce = myEnvironment.waitForDynamicElement(
				By.id(myHmcLeftNav.getTheByIDLeftNavSystemFolder()));
		ce.click();		
		ce = myEnvironment.waitForDynamicElement(
				By.id(myHmcLeftNav.getTheByIDLeftNavCronJob()));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSearchByJob()));
		Select clickThis = new Select(ce);
		Thread.sleep(1000);
		if(driver.getCurrentUrl().contains("dev"))
			clickThis.selectByIndex(40);
			else clickThis.selectByIndex(43);
				
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdSearchResultsButton));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(theJob));
		Actions action = new Actions(driver);
		action.doubleClick(ce).perform();
			ce = myEnvironment.waitForDynamicElement(
					By.id("Content/GenericItemChip$1[action.performcronjob]_label"));
			ce.click();
		// this is for the cron job window, for some reason it affects logging into CSC.		
		String mainwindow=driver.getWindowHandle();		
		Set<String> windows = driver.getWindowHandles();

        for (String window : windows) {
       	 System.out.println("These are the windows available           :"   + window);
            driver.switchTo().window(window);
            System.out.println("this is the titles of the windows:              "  + driver.getTitle());
            if (driver.getTitle().contains("Start CronJob now - hybris Management Console (hMC)")) {
            
            	driver.close();
            	driver.switchTo().window(mainwindow);
            }
        }
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdEndSessionButton));
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdEndSessionButton));
		ce.click();

	}

	public String getTheByIdHomeButton() {
		return theByIdHomeButton;
	}

	public String getTheByIdEndSessionButton() {
		return theByIdEndSessionButton;
	}

}
