package TommyBahamaOutletRepository;

public class OutletHmcLeftNav{

	private final String theByIDLeftNavSystemFolder = "Tree/GenericExplorerMenuTreeNode[system]_label";

	private final String theByIDLeftNavCronJob = "Tree/GenericLeafNode[CronJob]_label";

	
	public String getTheByIDLeftNavSystemFolder() {
		return theByIDLeftNavSystemFolder;
	}


	public String getTheByIDLeftNavCronJob() {
		return theByIDLeftNavCronJob;
	}
	
	
}
