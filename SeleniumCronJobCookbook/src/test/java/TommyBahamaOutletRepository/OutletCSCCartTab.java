package TommyBahamaOutletRepository;



public class OutletCSCCartTab {

	
	   private final String theByXpathCartTab = "//li[2]/div/div/div/span";
	    private final String theByXpathCartSearchInput = "//div[2]/input";
	    private final String theByXpathCartSearchButton = "//div[3]/span/table/tbody/tr[2]/td[2]";
		private final String theByXpathCartSearchResultQtyDropdown = "//td[4]/div/select";
		private final String theByXpathCartAddtoBagButton = "//td[5]/div/span/table/tbody/tr[2]/td[2]";
	    private final String theByXpathCheckoutButton = "//div[2]/div/div[2]/div/div[3]/span/table/tbody/tr[2]/td[2]";
	  
	    private final String theByXpathCartId = "//div[2]/div/div/div/div/div/div/div/div/div/span[2]";
	    
	    private OutletWebdriverEnvironment myEnvironment;
		private String testName;
		
		
	    public OutletCSCCartTab(OutletWebdriverEnvironment theEnvironment, String theTestName) {
	    	
	    	myEnvironment = theEnvironment; 
	    	testName = theTestName;
	    }
	    
	   

		public String getTheByXpathCartTab() {
			return theByXpathCartTab;
		}



		public String getTheByXpathCartSearchInput() {
			return theByXpathCartSearchInput;
		}



		public String getTheByXpathCartSearchButton() {
			return theByXpathCartSearchButton;
		}



		public String getTheByXpathCartSearchResultQtyDropdown() {
			return theByXpathCartSearchResultQtyDropdown;
		}



		public String getTheByXpathCartAddtoBagButton() {
			return theByXpathCartAddtoBagButton;
		}



		public String getTheByXpathCheckoutButton() {
			return theByXpathCheckoutButton;
		}



		public String getTheByXpathCartId() {
			return theByXpathCartId;
		}

	
}
