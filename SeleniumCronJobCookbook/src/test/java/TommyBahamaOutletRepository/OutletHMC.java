package TommyBahamaOutletRepository;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;



public class OutletHMC {
	
	private final String theByXPathSubmitLogin = "//span";
	
	private final String theUsrName = "admin";
	private final String thePswrd = "nimda";
	
	private final String theByIdSearchByJob = "Content/AllInstancesSelectEditor[in Content/GenericCondition[CronJob.job]]_select";
	
	
	private final String theSiteDetailsCronJobIndex = "42";
	
/*	private final String theByIdSiteDetailsFirstCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap01]_span";
	private final String theByIdSiteDetailsSecondCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap02]_span";
	private final String theByIdSiteDetailsThirdCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap03]_span";
	private final String theByIdSiteDetailsFourthCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap04]_span";*/
	
	private final String theByIdSiteDetailsFirstCronJob = "Content/OrganizerListEntry[Tommy Bahama Site Details Job Ap01][search 2]_img";
	private final String theByIdSiteDetailsSecondCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap02]_span";
	private final String theByIdSiteDetailsThirdCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap03]_span";
	private final String theByIdSiteDetailsFourthCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap04]_span";
	
	private final String theByIdSearchResultsButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	private final String theByIdCronJobName = "Content/StringEditor[in Content/GenericCondition[CronJob.code]]_input";
	private final String theByIdSubmitSearchButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	
	private final String theByIdSiteDetailsStartFirstCronJob = "Content/GenericItemChip$1[action.performcronjob]";
	private final String theByIdSiteDetailsStartSecondCronJob = "Content/GenericItemChip$1[action.performcronjob][1]_label";
	private final String theByIdSiteDetailsStartThirdCronJob = "Content/GenericItemChip$1[action.performcronjob][2]_label";
	private final String theByIdSiteDetailsStartFourthCronJob = "Content/GenericItemChip$1[action.performcronjob][3]_label";
	
	
	private final String theByIdHomeButton = "Explorer[mcc]_label";
	private final String theByIdEndSessionButton = "Toolbar/ImageToolbarAction[closesession]_img";
	
	
	private WebDriver driver;
	private int cronJobsRan = 1;
	private String testName;
	
	private OutletWebdriverEnvironment myEnvironment;
	
	private OutletHmcLeftNav myHmcLeftNav;

	public OutletHMC(){
		
	}
	
    public OutletHMC(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment, OutletHmcLeftNav theHmcLeftNav, String theTestName){
    	
    	driver = theDriver;
    	myEnvironment = theEnvironment;
    	myHmcLeftNav = theHmcLeftNav;
    	testName = theTestName;
    	
		
	}
	
	
	public String getTheByXPathSubmitLogin() {
		return theByXPathSubmitLogin;
	}




	public String getTheByIdSearchByJob() {
		return theByIdSearchByJob;
	}




	public String getTheSiteDetailsCronJobIndex() {
		return theSiteDetailsCronJobIndex;
	}




	public String getTheByIdSiteDetailsFirstCronJob() {
		return theByIdSiteDetailsFirstCronJob;
	}




	public String getTheByIdSiteDetailsSecondCronJob() {
		return theByIdSiteDetailsSecondCronJob;
	}




	public String getTheByIdSiteDetailsThirdCronJob() {
		return theByIdSiteDetailsThirdCronJob;
	}




	public String getTheByIdSiteDetailsFourthCronJob() {
		return theByIdSiteDetailsFourthCronJob;
	}




	public String getTheByIdSubmitSearchButton() {
		return theByIdSubmitSearchButton;
	}




	public String getTheByIdSiteDetailsStartFirstCronJob() {
		return theByIdSiteDetailsStartFirstCronJob;
	}




	public String getTheByIdSiteDetailsStartSecondCronJob() {
		return theByIdSiteDetailsStartSecondCronJob;
	}




	public String getTheByIdSiteDetailsStartThirdCronJob() {
		return theByIdSiteDetailsStartThirdCronJob;
	}




	public String getTheByIdSiteDetailsStartFourthCronJob() {
		return theByIdSiteDetailsStartFourthCronJob;
	}




	public String getTheByIdSearchResultsButton() {
		return theByIdSearchResultsButton;
	}
	
	public void fireSiteDetailsCronJob(String theJob, OutletSignInPage mySignInPage) throws InterruptedException {
		
		if(driver.getCurrentUrl().contains("dev"))
		driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;		
    	// log into the hmc
	//    myEnvironment.IsElementPresent(By.xpath(this.getTheByXPathSubmitLogin()));
		if (myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
			mySignInPage.signInHMC(theUsrName, thePswrd);
			myEnvironment.waitForURL("wid");

			
			// click the System folder in the hmc
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHmcLeftNav.getTheByIDLeftNavSystemFolder()));
			ce.click();	
		}
	
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(myHmcLeftNav.getTheByIDLeftNavCronJob()));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCronJobName()));
	//	Select clickThis = new Select(ce);
	//	Thread.sleep(1000);
	//	if(driver.getCurrentUrl().contains("dev"))
	//		clickThis.selectByIndex(41);
	//		else clickThis.selectByIndex(43);
		ce.sendKeys("Tommy Bahama Site");
				
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdSearchResultsButton));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(theJob));
		if(myEnvironment.getBrowser().contains("Safari")) {
		ce.click();
		if(theJob.contains("2"))
			ce = myEnvironment.waitForDynamicElement(
					By.id("Content/OrganizerListEntry[Tommy Bahama Site Details Job Ap02][search 2]_img"));
		else  ce = myEnvironment.waitForDynamicElement(
				By.id("Content/OrganizerListEntry[Tommy Bahama Site Details Job Ap01][search 2]_img"));
		ce.click();
		} else {
		Actions action = new Actions(driver);
		action.doubleClick(ce).perform();
		}
		
		
		myEnvironment.IsElementPresent(By.id("Content/GenericItemChip$1[action.performcronjob]_label"));
			ce = myEnvironment.waitForDynamicElement(
					By.id("Content/GenericItemChip$1[action.performcronjob]_label"));
			ce.click();
			
			Thread.sleep(9000);
		// this is for the cron job window, for some reason it affects logging into CSC.		
		String mainwindow=driver.getWindowHandle();		
		Set<String> windows = driver.getWindowHandles();

        for (String window : windows) {
       	 System.out.println("These are the windows available           :"   + window);
            driver.switchTo().window(window);
            System.out.println("this is the titles of the windows:              "  + driver.getTitle());
            if (!window.equals(mainwindow)) {
            System.out.println("Are we getting in here to close the cron job window??????????");
            	driver.close();
            	driver.switchTo().window(mainwindow);
            }
        }
        
   myEnvironment.IsElementPresent(By.id(this.theByIdEndSessionButton));
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdEndSessionButton));
		ce.click();
		
		driver.switchTo().window(mainwindow);
	}
	
	/**
	* Confirm alert, prompt, or confirmation dialog that is present on the page.
	* Does nothing if confirmation not present on page and returns empty string.
	* @return String 		alert text or empty string
	*/
	public String getConfirmation() {		
		try{
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			alert.accept();
			return alertText;		
		}catch(Exception ex){
		//}catch(UnexpectedAlertOpenException ex){ //assume this is the correct exception to detect in Safari, per  issue 3969 ? I haven't tested yet.
		//}catch(NoAlertPresentException ex){
			return ""; //if there is no alert displayed, do nothing, return empty string.
		}
	}

	public String getTheByIdHomeButton() {
		return theByIdHomeButton;
	}

	public String getTheByIdEndSessionButton() {
		return theByIdEndSessionButton;
	}

	public String getTheByIdCronJobName() {
		return theByIdCronJobName;
	}

}
