package SeleniumCronJobCookbook.tommyBahama.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import TommyBahamaOutletRepository.OutletHMC;
import TommyBahamaOutletRepository.OutletHmcLeftNav;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;

@RunWith(JUnit4.class)
public class GoogleTest {

	protected WebDriver driver;
	//private StringBuffer verificationErrors = new StringBuffer();
	private OutletWebdriverEnvironment myEnvironment;
	private String testName = "index";
	private OutletHMC myHMC;
	private OutletHmcLeftNav myHmcLeftNav = new OutletHmcLeftNav();
	private OutletSignInPage mySignInPage;

	@Before
	public void openBrowser() {
		Platform currentPlatform = Platform.getCurrent();
		if (System.getenv("SELENIUM_BROWSER").contains("FF")) {
			driver = new FirefoxDriver();
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		} else if (System.getenv("SELENIUM_BROWSER").contains("SAFARI")) {
			DesiredCapabilities mc = DesiredCapabilities.safari();
			mc.setCapability("applicationCacheEnabled", true);
			driver = new SafariDriver(mc);
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		} else {
			if (currentPlatform.is(Platform.WINDOWS))
				System.setProperty("webdriver.chrome.driver",
						"./chromedriver.exe");
			else {
				System.setProperty("webdriver.chrome.driver",
						"/usr/bin/chromedriver");
			}
			driver = new ChromeDriver();
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myEnvironment.setBrowser(myEnvironment.getChromeBrowser());

		}
		myHMC = new OutletHMC(driver, myEnvironment, myHmcLeftNav, testName);
		mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
		
		String tempFile = "../seleniumhq";
		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());	
		fns.add("LogInPage");
		fns.add("CSCLandingPage");
		fns.add("SearchOrderFormWithResults");
		fns.add("OrderTab");
		fns.add("OrderTab2");
		myEnvironment.removeSS(fns, tempFile);

	}

	@Test
	public void testCronJob() throws InterruptedException,
	IOException {

		WebElement ce;

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		System.out.println(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTheTestingEnvironment(System
				.getenv("SELENIUM_TEST_URL"));
		
	//	System.out.println(System.getenv("SELENIUM_TEST_CSC_URL"));
	//	myEnvironment.setTheTestingEnvironment(System.getenv("SELENIUM_TEST_CSC_URL"));
		myEnvironment.setTestSubject(testName);

		driver.get(myEnvironment.getTheTestingEnvironment());

		// this is to make the right cronjob per environment.
		if (driver.getCurrentUrl().contains("dev")) {
			System.out.println("We are going to start the DEV cron job!");
			myHMC.fireSiteDetailsCronJob(					
					myHMC.getTheByIdSiteDetailsFirstCronJob(), mySignInPage);
		} else {
			System.out.println("We are going to start the QA cron job!");
			myHMC.fireSiteDetailsCronJob(
					myHMC.getTheByIdSiteDetailsFirstCronJob(), mySignInPage);
			myHMC.fireSiteDetailsCronJob(
					myHMC.getTheByIdSiteDetailsSecondCronJob(), mySignInPage);
		}
		
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException, IOException {
	
		// fires if the test did not pass. Takes SS of last rendered page. 
		// need something changed in here. 
		if(!myEnvironment.getTestPassed()) {
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				"TestFailed", "OnCurrentPage");
		}

		myEnvironment.setNetworkFile("../seleniumhq/" + this.testName);

		BufferedWriter bw = new BufferedWriter(new FileWriter(
				myEnvironment.getNetworkFile() + ".html"));

		bw.write("<html><body><center><h2>Outlet Single Division Order</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This test is to see if a single division order is completed correctly on WS and in CSC.</h3> <table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ this.testName
				+ "</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName,
						mySignInPage.getThePageName(),
						"This is the Sign In page.")
				+

			

				myEnvironment.addSSToFile(testName, "LogInPage",
						"This is the CSC login page.")
				+

				myEnvironment.addSSToFile(testName, "CSCLandingPage",
						"This is the CSC landing page.")
				+

				myEnvironment.addSSToFile(testName,
						"SearchOrderFormWithResults",
						"This is the search modal for orders.")
				+

				myEnvironment.addSSToFile(testName, "OrderTab",
						"This is the Order Detail page.")
				+

				myEnvironment.addSSToFile(testName, "OrderTab2",
						"This is the bottom of the Order Detail page.") +

				myEnvironment.getPageTestOutcome() +

				"</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Single Divison Order</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This test is to see if a single division order is correct on WS and CSC.</h3> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "" + myEnvironment.getNetworkFile() + ".html"
						+ "<br/></center></body></html>", "text/html");

	/*	myEnvironment.sendTestResultEmail(htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle());*/
		driver.quit();

	}

}
