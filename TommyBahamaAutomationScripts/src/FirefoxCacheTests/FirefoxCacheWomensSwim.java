package FirefoxCacheTests;



import java.io.File;
import java.util.concurrent.TimeUnit;
import net.lightbody.bmp.proxy.ProxyServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.Women;
import TommyBahamaRepository.WomenSwim;

	public class FirefoxCacheWomensSwim {

			private SeleniumEnvironment myEnvironment;
			private Header myHeaderObjs;
			private WomenSwim myWomenSwimObjs = new WomenSwim();
			private ProductListingPage myProductListingObjs;
			private ProductDetailPage myProductDetailObjs;
		    private String testName =  "WomensSwim";
			WebDriver driver;
			ProxyServer server = new ProxyServer(8090);
			
			@Before
			public void openBrowser() throws Exception {

				server.start();		
			    server.setCaptureHeaders(true);
			    server.setCaptureContent(true);
				
				Proxy proxy = server.seleniumProxy();		
				proxy.setHttpProxy("localhost:8090");
				proxy.setSslProxy("localhost:8090");
			
			
				DesiredCapabilities capabilities = new DesiredCapabilities();
				File fileToProfile = new File(myEnvironment.getFfProfile());
				FirefoxProfile profile = new FirefoxProfile(fileToProfile);	
				    profile.setPreference("network.proxy.type", 1);
				    profile.setPreference("network.proxy.http", "proxy.domain.test.tommybahama.com");
				    profile.setPreference("network.proxy.http_port", 8090);
				    profile.setPreference("network.proxy.ssl", "proxy.domain.test.tommybahama.com");
				    profile.setPreference("network.proxy.ssl_port", 8090);
				    profile.setPreference("webdriver.load.strategy", "unstable");
				    capabilities.setCapability(FirefoxDriver.PROFILE,profile);
					capabilities.setCapability(CapabilityType.PROXY, proxy);
				    driver = new FirefoxDriver(capabilities);
				myEnvironment = new SeleniumEnvironment(driver);
				myHeaderObjs = new Header(driver, myEnvironment);
				 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
				 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
						 myHeaderObjs, testName);
				 
					server.newHar("test.tommybahama.com");
			}

			@Test
			public void TheTraverseWomensSwimProductsTest() throws InterruptedException {
				WebElement ce;

				driver.get(myEnvironment.getTheTestingEnvironment());
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				myEnvironment.setBrowser(myEnvironment.getFfBrowser());
				
				ce = myEnvironment.waitForDynamicElement(
						By.id("emailFormFooterId"));
				
				ce.sendKeys("No wait times on hover creates problems!");
						
				myEnvironment.parseHarFile(server, testName);

				ce = driver.findElement(By.id(myHeaderObjs.getTheByIdWomensSwimTab()));
				
				ce.click();
				
				ce = myEnvironment.waitForDynamicElement(
						By.id("emailFormFooterId"));
				
				ce.sendKeys("No wait times on hover creates problems!");
				
				/*				
				
		        ce = driver.findElement(By.xpath(myWomenSwimObjs.getTheByXPathNewArrivals()));
				
				ce.click();
				
		
		
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
				myWomenSwimObjs.CreateWomensSwimNewArrivalsLinksList();
				myProductListingObjs.traverseLinks(
						myWomenSwimObjs.getTheWomensSwimNewArrivalsLinks(), 
						myProductDetailObjs, "xpath", server, testName);
				System.out
						.println("                 I HAVE MADE IT PAST THE NEW ARRIVALS LIST!!!!!!!!!!!!!!");

				
				myEnvironment.waitForDynamicElement(
						By.xpath(myWomenSwimObjs.getTheByXPathSummerSwimShop()));
				driver.findElement(By.xpath(myWomenSwimObjs.getTheByXPathSummerSwimShop()))
						.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				myWomenSwimObjs.CreateWomenSummerSwimByShopLinksList();
				myProductListingObjs.traverseLinks(
						myWomenSwimObjs.getTheWomensSummerSwimByShopLinks(), myProductDetailObjs, "xpath", server, testName);
				System.out
						.println("                 I HAVE MADE IT PAST THE Summer Swim By Shop By LIST!!!!!!!!!!!!!!");

				
				myEnvironment.waitForDynamicElement(
						By.xpath(myWomenSwimObjs.getTheByXPathSwimByCollections()));
				driver.findElement(By.xpath(myWomenSwimObjs.getTheByXPathSwimByCollections()))
						.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				myWomenSwimObjs.CreateWomensSwimByCollectionLinksList();
				myProductListingObjs.traverseLinks(
						myWomenSwimObjs.getTheWomensSwimByCollectionLinks(), myProductDetailObjs, "linktext", server, testName);
				System.out
						.println("                 I HAVE MADE IT PAST THE Swim By Collections LIST!!!!!!!!!!!!!!");

				myEnvironment.waitForDynamicElement(
						By.xpath(myWomenSwimObjs.getTheByXPathSwimCoverups()));
				driver.findElement(By.xpath(myWomenSwimObjs.getTheByXPathSwimCoverups()))
						.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				myWomenSwimObjs.CreateWomenSwimCoverupsLinksList();
				myProductListingObjs.traverseLinks(
						myWomenSwimObjs.getTheWomensSwimCoverupsLinks(), 
						myProductDetailObjs, "linktext", server, testName);
				System.out
						.println("                 I HAVE MADE IT PAST THE Swim Coverups LIST!!!!!!!!!!!!!!");

			
				myEnvironment.waitForDynamicElement(
						By.xpath(myWomenSwimObjs.getTheByXPathBeachAccessories()));
				driver.findElement(By.xpath(myWomenSwimObjs.getTheByXPathBeachAccessories()))
						.click();
				myWomenSwimObjs.CreateWomensBeachAccessoriesLinksList();
				myProductListingObjs.traverseProducts(
						myWomenSwimObjs.getTheWomensSwimBeachAccessoriesLinks(), 
						myProductDetailObjs, "linktext", server, testName);

				System.out
						.println("                 I HAVE MADE IT PAST THE ACCESSORIES LIST!!!!!!!!!!!!!!"); 
				// driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
				// now we start on the sub links. This is the sub links of New Arrivals

		

				myWomenSwimObjs.CreateWomensShopLinksList();
				myProductListingObjs.traverseProducts(myWomenSwimObjs.getTheWomensSwimShopLinks(),
						 myProductDetailObjs, "linktext", server, testName);
				System.out
						.println("                 I have PAST THE Main Product linktext LISTs!!!!!!!!!!!!!!");
*/
		
				myWomenSwimObjs.CreateWomensSwimLinksList();
				myProductListingObjs.traverseProducts(myWomenSwimObjs.getTheWomensSwimLinks(),
						 myProductDetailObjs, "xpath", server, testName);
				System.out
						.println("                 I have PAST THE Main Product xpath LISTs!!!!!!!!!!!!!!");

		


			}

			@After
			public void quitDriver() {

				driver.quit();

			}
		}

