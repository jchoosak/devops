package FirefoxCacheTests;



	import java.io.File;
import java.util.concurrent.TimeUnit;

import net.lightbody.bmp.proxy.ProxyServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;

	public class FirefoxCacheMens {

		private SeleniumEnvironment myEnvironment;
		private Header myHeaderObjs;
		private Men myMenObjs = new Men();
		private ProductListingPage myProductListingObjs;
		private ProductDetailPage myProductDetailObjs;
	    private String testName =  "Mens";
		WebDriver driver;
		ProxyServer server = new ProxyServer(8090);
		@SuppressWarnings("deprecation")
		@Before
		public void openBrowser() throws Exception {

			server.start();		
		    server.setCaptureHeaders(true);
		    server.setCaptureContent(true);
			
			Proxy proxy = server.seleniumProxy();		
			proxy.setHttpProxy("localhost:8090");
			proxy.setSslProxy("localhost:8090");
		
			myEnvironment = new SeleniumEnvironment();
		
			DesiredCapabilities capabilities = new DesiredCapabilities();
			File fileToProfile = new File(myEnvironment.getFfProfile());
			FirefoxProfile profile = new FirefoxProfile(fileToProfile);	
			    profile.setPreference("network.proxy.type", 1);
			    profile.setPreference("network.proxy.http", "proxy.domain.test.tommybahama.com");
			    profile.setPreference("network.proxy.http_port", 8090);
			    profile.setPreference("network.proxy.ssl", "proxy.domain.test.tommybahama.com");
			    profile.setPreference("network.proxy.ssl_port", 8090);
			    profile.setPreference("webdriver.load.strategy", "unstable");
			    capabilities.setCapability(FirefoxDriver.PROFILE,profile);
				capabilities.setCapability(CapabilityType.PROXY, proxy);
				capabilities.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
			    driver = new FirefoxDriver(capabilities);
	         
			myEnvironment = new SeleniumEnvironment(driver);
			myHeaderObjs = new Header(driver, myEnvironment);
			 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
			 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
					 myHeaderObjs, testName);
			
			server.newHar("test.tommybahama.com");
		}

		@Test
		public void TheTraverseMensProductsTest() throws InterruptedException {
			WebElement ce;
			
		    server.newHar("test.tommybahama.com");
			
			driver.get(myEnvironment.getTheTestingEnvironment());
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		
			ce = myEnvironment.waitForDynamicElement(
					By.id("emailFormFooterId"));
			
			ce.sendKeys("No wait times on hover creates problems!");
					
			myEnvironment.parseHarFile(server, testName);
			
			ce = driver.findElement(By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		
			ce.click();
			
			ce = myEnvironment.waitForDynamicElement(
					By.id("emailFormFooterId"));
			
			ce.sendKeys("No wait times on hover creates problems!");
					
			ce = driver.findElement(By.xpath(myMenObjs.getTheByXPathNewArrivals()));
			
			ce.click();
		
		//	myEnvironment.waitUntilJQueryProcessingHasCompleted();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensNewArrivalsLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensNewArrivalsLinks(), 
					myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE NEW ARRIVALS LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathInternationalFit()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathInternationalFit()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensInternationalFitLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensInternationoalFitLinks(), myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE INTERNATIONAL LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathMensTShirts()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathMensTShirts()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensTShirtsLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensTShirtLinks(), 
					myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE T-SHIRTS LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathPants()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathPants())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensPantsLinksList();
			myProductListingObjs.traverseLinks(myMenObjs.getTheMensPantsLinks(),
					 myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Links Pants LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathJeans()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathJeans())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensJeansLinksList();
			myProductListingObjs.traverseLinks(myMenObjs.getTheMensJeansLinks(),
					 myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE JEANS LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathShorts()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathShorts())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensShortsLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensShortsLinks(), 
					myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE SHORTS LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathSwim()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathSwim())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensSwimLinksList();
			myProductListingObjs.traverseLinks(myMenObjs.getTheMensSwimLinks(),
					 myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE SWIM LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathCologneGrooming()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathCologneGrooming()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myMenObjs.CreateMensCologneGroomingLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensCologneGroomingLinks(),  myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE COLOGNE & GROOMING LIST!!!!!!!!!!!!!!");


			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathShirts()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathShirts())).click();
			myMenObjs.CreateMensShirtsLinksList();
			myProductListingObjs.traverseLinks(myMenObjs.getTheMensShirtLinks(),
					 myProductDetailObjs, "linktext", server, testName);

			System.out
					.println("                 I HAVE MADE IT PAST THE SHIRTS LIST!!!!!!!!!!!!!!");
			
			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathWedding()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathWedding())).click();
			myMenObjs.CreateMensWeddingLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensWeddingCollectionLinks(),  myProductDetailObjs, "linktext", server, testName);

			System.out
					.println("                 I HAVE MADE IT PAST THE WEDDING LIST!!!!!!!!!!!!!!");
			
			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathMLB()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathMLB())).click();
			myMenObjs.CreateMensMLBLinksList();
			myProductListingObjs.traverseLinks(myMenObjs.getTheMensMLBLinks(),
					 myProductDetailObjs, "linkText", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE MLB LIST!!!!!!!!!!!!!!");
			
			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathAccessories()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathAccessories()))
					.click();
			myMenObjs.CreateMensAccessoriesLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensAccessoriesLinks(), 
					myProductDetailObjs, "linktext", server, testName);

			System.out
					.println("                 I HAVE MADE IT PAST THE LINKTEXT ACCESSORIES LIST!!!!!!!!!!!!!!");
			
			
			myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathAccessories()));
			driver.findElement(By.xpath(myMenObjs.getTheByXPathAccessories()))
					.click();
			myMenObjs.CreateMensXpathAccessoriesLinksList();
			myProductListingObjs.traverseLinks(
					myMenObjs.getTheMensXpathAccessoriesLinks(), 
					myProductDetailObjs, "xpath", server, testName);

			System.out
					.println("                 I HAVE MADE IT PAST THE XPATH ACCESSORIES LIST!!!!!!!!!!!!!!");
		
			myMenObjs.CreateMensLinksList();
			myProductListingObjs.traverseProducts(myMenObjs.getTheMensLinks(),
					 myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I have PAST THE Main Product LISTs!!!!!!!!!!!!!!");

		   // driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
			// now we start on the sub links. This is the sub links of New Arrivals

		}

		@After
		public void quitDriver() {

			driver.quit();

		}
	}


