package FirefoxCacheTests;

import java.io.File;
import java.util.concurrent.TimeUnit;
import net.lightbody.bmp.proxy.ProxyServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import TommyBahamaRepository.BigAndTall;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;

public class FirefoxCacheBigAndTall {

	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	
	private String testName = "BigAndTall";

	private BigAndTall myBigAndTallObjs = new BigAndTall();
	WebDriver driver;

	ProxyServer server = new ProxyServer(8090);
	
	@Before
	public void openBrowser() throws Exception {
		
		server.start();		
	    server.setCaptureHeaders(true);
	    server.setCaptureContent(true);
		
		Proxy proxy = server.seleniumProxy();		
		proxy.setHttpProxy("localhost:8090");
		proxy.setSslProxy("localhost:8090");
	
	
		DesiredCapabilities capabilities = new DesiredCapabilities();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile profile = new FirefoxProfile(fileToProfile);	
		    profile.setPreference("network.proxy.type", 1);
		    profile.setPreference("network.proxy.http", "proxy.domain.test.tommybahama.com");
		    profile.setPreference("network.proxy.http_port", 8090);
		    profile.setPreference("network.proxy.ssl", "proxy.domain.test.tommybahama.com");
		    profile.setPreference("network.proxy.ssl_port", 8090);
		    profile.setPreference("webdriver.load.strategy", "unstable");
		    capabilities.setCapability(FirefoxDriver.PROFILE,profile);
			capabilities.setCapability(CapabilityType.PROXY, proxy);
		    driver = new FirefoxDriver(capabilities);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		
		server.newHar("test.tommybahama.com");
	
		 
	}

	@Test
	public void TheTraverseBigAndTallProductsTest() throws InterruptedException {
		
	WebElement ce;
	
	    server.newHar("test.tommybahama.com");
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());

		
		
		//Har har = server.getHar();
		
		
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLiveTheLifeTab()));
		
	
		
		myEnvironment.parseHarFile(server, testName);
		
		ce.click();
		
		
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("/html/body/div[2]/div/div[2]/div[2]/ul/li[15]/ul/li[5]/a"));
		
		myEnvironment.parseHarFile(server, testName);
		
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathBigAndTallTab()));
		
		
		myEnvironment.parseHarFile(server, testName);
		
		
		
	   //myEnvironment.parseHarFile(networkfile);
		
		// We first run through each link that directly shows PLP page from the
		// Men's Tab.
		driver.findElement(By.xpath(myHeaderObjs.getTheByXPathBigAndTallTab()))
				.click();
		myBigAndTallObjs.CreateBigAndTallLinks();
		
		myEnvironment.parseHarFile(server, testName);
		
		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallLinks(), 
				 myProductDetailObjs, "xpath", server, testName);

		System.out
				.println("                 I have PAST THE Main Product LISTs!!!!!!!!!!!!!!");
		
	

		// now we start on the sub links. This is the sub links of New Arrivals
		// myEnvironment.waitForDynamicElement(
		// By.xpath(myBigAndTallObjs.getTheByXPathShirts()));
		driver.findElement(By.xpath(myBigAndTallObjs.getTheByXPathShirts()))
				.click();
		myBigAndTallObjs.CreateBigAndTallShirtLinks();
		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallShirtLinks(), 
				 myProductDetailObjs, "linktext", server, testName);

		System.out
				.println("                 I HAVE MADE IT PAST THE NEW Shirt Links LIST!!!!!!!!!!!!!!");

		myEnvironment.waitForDynamicElement(
				By.xpath(myBigAndTallObjs.getTheByXPathCologneGrooming()));
		driver.findElement(
				By.xpath(myBigAndTallObjs.getTheByXPathCologneGrooming()))
				.click();
		myBigAndTallObjs.CreateBigAndTallCologneGroomingLinks();

		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallCologneGroomingLinks(),
			 myProductDetailObjs, "linktext", server, testName);
		System.out
				.println("                 I HAVE MADE IT PAST THE Grooming LIST!!!!!!!!!!!!!!");

		myEnvironment.waitForDynamicElement(
				By.xpath(myBigAndTallObjs.getTheByXPathAccessories()));
		driver.findElement(
				By.xpath(myBigAndTallObjs.getTheByXPathAccessories())).click();
		myBigAndTallObjs.CreateBigAndTallAccessoriesLinks();
		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallAccessoriesLinks(),  myProductDetailObjs, "linktext", server, testName);

		System.out
				.println("                 I HAVE MADE IT PAST THE Accessories LIST!!!!!!!!!!!!!!");

	}

	@After
	public void quitDriver() throws Exception {

		driver.quit();
		server.stop();

	}

}

