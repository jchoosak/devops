package FirefoxCacheTests;

import java.io.File;
import java.util.concurrent.TimeUnit;

import net.lightbody.bmp.proxy.ProxyServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;

public class StoresAndRestaurants {
	
	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private StoresAndRestaurants mySandR = new StoresAndRestaurants();
	
	
	private String testName = "TheTraverseHomeDecorProductsTest";
	ProxyServer server = new ProxyServer(8090);
	WebDriver driver;

	@Before
	public void openBrowser() throws Exception {
		server.start();		
	    server.setCaptureHeaders(true);
	    server.setCaptureContent(true);
		
		Proxy proxy = server.seleniumProxy();		
		proxy.setHttpProxy("localhost:8090");
		proxy.setSslProxy("localhost:8090");
	
	
		DesiredCapabilities capabilities = new DesiredCapabilities();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile profile = new FirefoxProfile(fileToProfile);	
		    profile.setPreference("network.proxy.type", 1);
		    profile.setPreference("network.proxy.http", "proxy.domain.test.tommybahama.com");
		    profile.setPreference("network.proxy.http_port", 8090);
		    profile.setPreference("network.proxy.ssl", "proxy.domain.test.tommybahama.com");
		    profile.setPreference("network.proxy.ssl_port", 8090);
		    profile.setPreference("webdriver.load.strategy", "unstable");
		    capabilities.setCapability(FirefoxDriver.PROFILE,profile);
			capabilities.setCapability(CapabilityType.PROXY, proxy);
		    driver = new FirefoxDriver(capabilities);
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
	}

	@Test
	public void TheTraverseHomeDecorProductsTest() throws InterruptedException {
		WebElement ce;
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		ce = driver.findElement(By.id(myHeaderObjs.getTheByXPathStoresAndRestaurantsTab()));
		
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id("emailFormFooterId"));
		
		ce.sendKeys("No wait times on hover creates problems!");
		
	
		
		//mySandR.creat;
	//	myProductListingObjs.traverseProducts(
	//			mySandR.getTheHomeDecorLinkList(), myProductDetailObjs, "xpath", server, testName);

		System.out
				.println("                 I have PAST THE Main XPath Product LISTs!!!!!!!!!!!!!!");
	
		
	}
	
	@After
	public void quitDriver() {

		driver.quit();

	}

}
