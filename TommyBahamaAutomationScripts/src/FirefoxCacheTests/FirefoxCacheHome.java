package FirefoxCacheTests;

    import java.io.File;
import java.util.concurrent.TimeUnit;

	import net.lightbody.bmp.proxy.ProxyServer;

	import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;


	public class FirefoxCacheHome {

		private SeleniumEnvironment myEnvironment;
		private Header myHeaderObjs;
		private ProductListingPage myProductListingObjs;
		private ProductDetailPage myProductDetailObjs;
		private HomeDecor myHomeDecorObjs = new HomeDecor();
		
		private String testName = "TheTraverseHomeDecorProductsTest";
		ProxyServer server = new ProxyServer(8090);
		WebDriver driver;

		@Before
		public void openBrowser() throws Exception {
			server.start();		
		    server.setCaptureHeaders(true);
		    server.setCaptureContent(true);
			
			Proxy proxy = server.seleniumProxy();		
			proxy.setHttpProxy("localhost:8090");
			proxy.setSslProxy("localhost:8090");
		
		
			DesiredCapabilities capabilities = new DesiredCapabilities();
			File fileToProfile = new File(myEnvironment.getFfProfile());
			FirefoxProfile profile = new FirefoxProfile(fileToProfile);	
			    profile.setPreference("network.proxy.type", 1);
			    profile.setPreference("network.proxy.http", "proxy.domain.test.tommybahama.com");
			    profile.setPreference("network.proxy.http_port", 8090);
			    profile.setPreference("network.proxy.ssl", "proxy.domain.test.tommybahama.com");
			    profile.setPreference("network.proxy.ssl_port", 8090);
			    profile.setPreference("webdriver.load.strategy", "unstable");
			    capabilities.setCapability(FirefoxDriver.PROFILE,profile);
				capabilities.setCapability(CapabilityType.PROXY, proxy);
			    driver = new FirefoxDriver(capabilities);
			myEnvironment = new SeleniumEnvironment(driver);
			myHeaderObjs = new Header(driver, myEnvironment);
			 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
			 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
					 myHeaderObjs, testName);
		}

		@Test
		public void TheTraverseHomeDecorProductsTest() throws InterruptedException {
			// IWebElement element;
			driver.get(myEnvironment.getTheTestingEnvironment());

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			

			// We first run through each link that directly shows PLP page from the
			// Men's Tab.
			driver.findElement(By.xpath(myHeaderObjs.getTheByXPathHomeDecorTab()))
					.click();
			
			myHomeDecorObjs.createShopPetList();
			myProductListingObjs.traverseLinks(
					myHomeDecorObjs.getTheHomeDecorPetsList(), 
					myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE NEW Pets LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myHomeDecorObjs.getTheByXPathShopByScent()));
			driver.findElement(By.xpath(myHomeDecorObjs.getTheByXPathShopByScent()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myHomeDecorObjs.createShopByScentList();
			myProductListingObjs.traverseLinks(
					myHomeDecorObjs.getTheHomeDecorShopByScentList(), myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Shop By Scent LIST!!!!!!!!!!!!!!");

		
			myEnvironment.waitForDynamicElement(
					By.xpath(myHomeDecorObjs.getTheByXPathShopBySize()));
			driver.findElement(By.xpath(myHomeDecorObjs.getTheByXPathShopBySize()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myHomeDecorObjs.createShopBySizeList();
			myProductListingObjs.traverseLinks(
					myHomeDecorObjs.getTheHomeDecorShopBySizeList(), myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Shop By Size LIST!!!!!!!!!!!!!!");
			
			myEnvironment.waitForDynamicElement(
					By.xpath(myHomeDecorObjs.getTheByXPathTumblers()));
			driver.findElement(By.xpath(myHomeDecorObjs.getTheByXPathTumblers()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myHomeDecorObjs.createHomeDecorTumblerList();
			myProductListingObjs.traverseLinks(
					myHomeDecorObjs.getTheHomeDecorTumblersList(), myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Tumbler LIST!!!!!!!!!!!!!!");

			
			myHomeDecorObjs.createHomeDecorLinkList();
			myProductListingObjs.traverseProducts(
					myHomeDecorObjs.getTheHomeDecorLinkList(), myProductDetailObjs, "xpath", server, testName);

			System.out
					.println("                 I have PAST THE Main XPath Product LISTs!!!!!!!!!!!!!!");
		}

		@After
		public void quitDriver() {

			driver.quit();

		}
	}
