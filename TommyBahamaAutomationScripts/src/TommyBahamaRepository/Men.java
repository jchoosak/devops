package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class Men {
	private List<String> theMensLinks = new ArrayList<String>();
	private List<String> theMensShirtsViewAllLinks = new ArrayList<String>();
	private List<String> theMensMLBViewAllLinks = new ArrayList<String>();
	private List<String> theMensNewArrivalsLinks = new ArrayList<String>();
	private List<String> theMensAccessoriesLinks = new ArrayList<String>();
	private List<String> theMensXpathAccessoriesLinks = new ArrayList<String>();
	private List<String> theMensMLBLinks = new ArrayList<String>();
	private List<String> theMensInternationoalFitLinks = new ArrayList<String>();
	private List<String> theMensWeddingCollectionLinks = new ArrayList<String>();
	private List<String> theMensShirtLinks = new ArrayList<String>();
	private List<String> theMensTShirtLinks = new ArrayList<String>();
	private List<String> theMensPantsLinks = new ArrayList<String>();
	private List<String> theMensJeansLinks = new ArrayList<String>();
	private List<String> theMensShortsLinks = new ArrayList<String>();
	private List<String> theMensSwimLinks = new ArrayList<String>();
	private List<String> theMensCologneGroomingLinks = new ArrayList<String>();

	private final String theByXPathShirts = "(//a[@href='http://test.tommybahama.com/TBG/Men/Shirts.jsp'])[2]";
	private final String theByXPathGolf = "(//a[contains(text(),'Golf')])[2]";
	private final String theByXPathCologneGrooming = "(//a[contains(text(),'Cologne & Grooming')])[2]";
	private final String theByXPathMensTShirts = "(//a[@href='http://test.tommybahama.com/TBG/Men/Tees.jsp'])[2]";
	private final String theByXPathAccessories = "(//a[contains(text(),'Accessories')])[5]";
	private final String theByXPathShoes = "(//a[contains(text(),'Shoes & Sandals')])[4]";
	private final String theByXPathGiftsForDad = "//li[2]/a/span";
	private final String theByXPathInternationalFit = "(//a[@href='http://test.tommybahama.com/TBG/Men/International_Fit.jsp'])[2]";
	private final String theByXPathGuestFavorites = "(//a[contains(text(),'Guest Favorites')])[5]";
	private final String theByXPathPolos = "(//a[@href='http://test.tommybahama.com/TBG/Men/Polos.jsp'])[2]";
	private final String theByXPathSweatShirts = "(//a[@href='http://test.tommybahama.com/TBG/Men/Sweatshirts.jsp'])[2]";
	private final String theByXPathSweaters = "(//a[@href='http://test.tommybahama.com/TBG/Men/Sweaters.jsp'])[2]";
	private final String theByXPathJackets = "(//a[@href='http://test.tommybahama.com/TBG/Men/Outerwear.jsp'])[2]";
	private final String theByXPathPants = "(//a[@href='http://test.tommybahama.com/TBG/Men/Pants.jsp'])[2]";
	private final String theByXPathJeans = "(//a[@href='http://test.tommybahama.com/TBG/Men/Jeans.jsp'])[2]";
	private final String theByXPathShorts = "(//a[@href='http://test.tommybahama.com/TBG/Men/Shorts.jsp'])[2]";
	private final String theByXPathSwim = "(//a[@href='http://test.tommybahama.com/TBG/Men/Swim.jsp'])[2]";
	private final String theByXPathBoxers = "(//a[@href='http://test.tommybahama.com/TBG/Men/Boxers_and_Robes.jsp'])[2]";
	private final String theByXPathWedding = "(//a[@href='http://test.tommybahama.com/TBG/Men/Wedding.jsp'])[2]";
	
	
	private final String theByXPathNAShirts = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Shirts.jsp']";
	private final String theByXPathNAPolos = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Polos.jsp']";
	private final String theByXPathNATshirts = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Crews_Tees.jsp']";
	private final String theByXPathNASweaters = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Sweatshirts.jsp']";
	private final String theByXPathNAJackets = "(//a[contains(text(),'Jackets & Blazers')])[2]";
	private final String theByXPathNAShorts = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Pants_Shorts.jsp']";
	private final String theByXPathNASwim = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Swim_Trunks.jsp']";
	private final String theByXPathNABoxers = "(//a[contains(text(),'Loungewear & Boxers')])[3]";
	private final String theByXPathNAAccessories = "//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products/Accessories.jsp']";
	
	
	private final String theByXPathMLB = "(//a[@href='http://test.tommybahama.com/TBG/Men/MLB_Shop.jsp'])[2]";
	private final String theByXPathViewAll = "//li[3]/ul/li[4]/a";
	private final String theByXPathCollectorsSeries = "(//a[contains(text(),'MLB® Collection')])[2]";
	private final String theByXPathTeamTrackJackets = "//li[3]/ul/li[6]/a";
	private final String theByXPathTeamHalfZips = "(//li[@id='leftnav_sub']/a)[2]";
	private final String theByXPathTeamShirts = "(//li[@id='leftnav_sub']/a)[5]";

	private final String theByXpath4InchInseam = "//a[contains(text(),'4.5\"-6.5\" Inseam')]";
	private final String theByXpath7InchInseam = "//a[contains(text(),'7.5\" Inseam')]";
	private final String theByXpath9InchInseam = "//a[contains(text(),'9\" & 11\" Inseam')]";
	private final String theByXPathInternationalFitTops = "//a[@href='http://test.tommybahama.com/TBG/Men/International_Fit/Tops.jsp']";
	private final String theByXPathInternationalFitBottoms = "//a[@href='http://test.tommybahama.com/TBG/Men/International_Fit/Bottoms.jsp']";
	private final String theByXPathColongeLink = "(//a[contains(text(),'Cologne & Grooming')])[2]";
	private final String theByXPathAccessorieBoxersLink = "(//a[contains(text(),'Loungewear & Boxers')])[4]";
	private final String theByXPathJewelryLink = "(//a[contains(text(),'Jewelry')])[2]";
	private final String theByXPathCigarwareLink = "(//a[contains(text(),'Cigarware')])[2]";
	private final String theByXPathNewArrivals = "(//a[@href='http://test.tommybahama.com/TBG/Men/Mens_New_Products.jsp'])[2]";
	private final String theByXPathCreditCardsLink = "(//a[contains(text(),'Gift Cards')])[6]";

	private final String theByLinkShortSleeveShirts = "Short-Sleeve Shirts";
	private final String theByLinkLongSleeveShirts = "Long-Sleeve Shirts";
	private final String theByLinkSolidShirts = "Solid Shirts";
	private final String theByLinkShirtsWithPockets = "Shirts with Pockets";
	private final String theByLinkSilkShirts = "Silk Shirts";
	private final String theByLinkLinenShirts = "Linen Shirts";
	private final String theByLinkDressShirts = "Dress Shirts";
	private final String theByLinkIslandShirts = "Island Modern Fit Shirts";
	private final String theByLinkPanelBackShirts = "Panelback Shirts";
	private final String theShirtsViewAll = "View All";
	private final String theByLinkAuthenticJeansFit = "Authentic Fit";
	private final String theByLinkStandardJeansFit = "Standard Fit";
	private final String theByLinkVintageStraightJeansFit = "Vintage Straight Fit";
	private final String theByLinkClassicFit = "Classic Fit";
	private final String theByLinkAuthenticFit = "Authentic Fit";
	private final String theByLinkStandardFit = "Standard Fit";
	private final String theByLinkEasyFit = "Easy Fit";
	private final String theByLinkDoublePleat = "Double Pleat";
	private final String theByLinkFlatFront = "Flat Front";
	private final String theByLinkFlatFrontShorts = "Flat-Front Shorts";
	private final String theByLinkCargoShorts = "Cargo Shorts";
	private final String theByLinkPatternShorts = "Patterned Shorts";
	private final String theByLinkPleatedShorts = "Pleated Shorts";
	private final String theByLinkSolidTShirts = "Solid T-Shirts";
	private final String theByLinkGraphicTShirts = "Graphic T-Shirts";
	private final String theByLinkMensSwimShop = "Visit the Swim Shop";
	private final String theByLinkViewAll = "View All";
	private final String theByLinkTeamTrackJackets = "Team Track Jackets";
	private final String theByLinkTeamHalfZips = "Team Half-Zips";
	private final String theByLinkTeamShirts = "2013 Team Shirts";	
	private final String theByLinkTechCollection = "Tech Collection";
	private final String theByLinkForTheGroom = "For the Groom";
	private final String theByLinkForTheGroomsmen = "For the Groomsmen";
	private final String theByLinkGroomsmenGifts = "Groomsmen Gifts";
	private final String theByLinkWeddingGifts = "Wedding Gifts";
	private final String theByLinkSocks = "Socks";
	private final String theByLinkHats = "Hats & Caps";
	private final String theByLinkSunglasses = "Sunglasses";
	private final String theByLinkBelts = "Belts";
	private final String theByLinkWatches = "Watches";
	private final String theByLinkWatchStraps = "Watch Straps";
	private final String theByLinkWallet = "Wallets & Money Clips";
	private final String theByLinkBags = "Bags & Travel";
	private final String theByLinkTowels = "Beach Towels";
	private final String theByLinkScarves = "Scarves";
	private final String theByLinkTechCases = "Tech Cases";
	private final String theByLinkTies = "Ties & Cufflinks";
	private final String theByLinkSetSailMartinique = "Set Sail Martinique";
	private final String theByLinkStBarts = "St. Barts";
	private final String theByLinkCompass = "Compass";
	private final String theByLinkTBForHim = "TB for Him";
	private final String theByLinkVintageParadise = "Vintage Paradise";

	private final String thePageName = "Mens";
 private final String theMensIndex = "MensIndex";
 private final String theMlbIndex = "MlbIndex";
 private final String theWeddingIndex = "WeddingIndex";
	

	public void CreateMensNewArrivalsLinksList() {	
		theMensNewArrivalsLinks.add(this.getTheByXPathNAShirts());
		theMensNewArrivalsLinks.add(this.getTheByXPathNAPolos());
		theMensNewArrivalsLinks.add(this.getTheByXPathNATshirts());
		theMensNewArrivalsLinks.add(this.getTheByXPathNASweaters());
		theMensNewArrivalsLinks.add(this.getTheByXPathNAShorts());
		theMensNewArrivalsLinks.add(this.getTheByXPathNASwim());
		theMensNewArrivalsLinks.add(this.getTheByXPathNAAccessories());
	}

	public void CreateMensShirtsLinksList() {
		theMensShirtLinks.add(this.getTheByLinkShortSleeveShirts());
		theMensShirtLinks.add(this.getTheByLinkLongSleeveShirts());
		theMensShirtLinks.add(this.getTheByLinkSolidShirts());
		theMensShirtLinks.add(this.getTheByLinkShirtsWithPockets());
		theMensShirtLinks.add(this.getTheByLinkSilkShirts());
		theMensShirtLinks.add(this.getTheByLinkLinenShirts());
		theMensShirtLinks.add(this.getTheByLinkDressShirts());
	    theMensShirtLinks.add(this.getTheByLinkIslandShirts());
	    theMensShirtLinks.add(this.getTheByLinkPanelBackShirts());
	}

	public void CreateMensShirtViewAllList() {
		theMensShirtsViewAllLinks.add(this.getTheShirtsViewAll());
	}

	public void CreateMensMLBViewAllList() {
		theMensMLBViewAllLinks.add(this.getTheShirtsViewAll());
	}

	public void CreateMensTShirtsLinksList() {
		theMensTShirtLinks.add(this.getTheByLinkSolidTShirts());
		theMensTShirtLinks.add(this.getTheByLinkGraphicTShirts());
	}

	public void CreateMensSwimLinksList() {
		theMensSwimLinks.add(this.gettheByXpath4InchInseam());
		theMensSwimLinks.add(this.gettheByXpath7InchInseam());
		theMensSwimLinks.add(this.gettheByXpath9InchInseam());
	}

	public void CreateMensWeddingLinksList() {
		theMensWeddingCollectionLinks.add(this.getTheByLinkForTheGroom());
		theMensWeddingCollectionLinks.add(this.getTheByLinkForTheGroomsmen());
		theMensWeddingCollectionLinks.add(this.getTheByLinkGroomsmenGifts());
		theMensWeddingCollectionLinks.add(this.getTheByLinkWeddingGifts());
	}

	public void CreateMensShortsLinksList() {
		theMensShortsLinks.add(this.getTheByLinkCargoShorts());
		theMensShortsLinks.add(this.getTheByLinkPatternShorts());
		theMensShortsLinks.add(this.getTheByLinkFlatFrontShorts());
		theMensShortsLinks.add(this.getTheByLinkPleatedShorts());
	}
	
	public void CreateMensCologneGroomingLinksList() {
		theMensCologneGroomingLinks.add(this.getTheByLinkSetSailMartinique());
		theMensCologneGroomingLinks.add(this.getTheByLinkCompass());
		theMensCologneGroomingLinks.add(this.getTheByLinkTBForHim());
		theMensCologneGroomingLinks.add(this.getTheByLinkStBarts());
	}

	public void CreateMensJeansLinksList() {
		theMensJeansLinks.add(this.getTheByLinkAuthenticJeansFit());
		theMensJeansLinks.add(this.getTheByLinkStandardJeansFit());
		theMensJeansLinks.add(this.getTheByLinkVintageStraightJeansFit());
	}

	public void CreateMensPantsLinksList() {
		theMensPantsLinks.add(this.getTheByLinkDoublePleat());
		theMensPantsLinks.add(this.getTheByLinkFlatFront());
		theMensPantsLinks.add(this.getTheByLinkStandardFit());
		theMensPantsLinks.add(this.getTheByLinkAuthenticFit());
		theMensPantsLinks.add(this.getTheByLinkClassicFit());
		theMensPantsLinks.add(this.getTheByLinkEasyFit());
	}

	public void CreateMensMLBLinksList() {
		theMensMLBLinks.add(this.getTheByLinkViewAll());
		//theMensMLBLinks.add(this.getTheByLinkTeamTrackJackets());
	//	theMensMLBLinks.add(this.getTheByLinkTeamHalfZips());
	//	theMensMLBLinks.add(this.getTheByLinkTeamShirts());
	}
	
	public void CreateMensXpathAccessoriesLinksList() {
		System.out.println("IT is reading the correct repository!");
		theMensXpathAccessoriesLinks.add(this.getTheByXPathAccessorieBoxersLink());
		theMensXpathAccessoriesLinks.add(this.getTheByXPathColongeLink());	
		theMensXpathAccessoriesLinks.add(this.getTheByXPathJewelryLink());
		theMensXpathAccessoriesLinks.add(this.getTheByXPathCigarwareLink());
	}

	public void CreateMensAccessoriesLinksList() {
		System.out.println("IT is reading the correct repository!");
		theMensAccessoriesLinks.add(this.getTheByLinkHats());
		theMensAccessoriesLinks.add(this.getTheByLinkTechCases());
		theMensAccessoriesLinks.add(this.getTheByLinkBelts());
		theMensAccessoriesLinks.add(this.getTheByLinkSunglasses());
		theMensAccessoriesLinks.add(this.getTheByLinkWatches());
		theMensAccessoriesLinks.add(this.getTheByLinkWatchStraps());
		theMensAccessoriesLinks.add(this.getTheByLinkSocks());
		theMensAccessoriesLinks.add(this.getTheByLinkWallet());
		theMensAccessoriesLinks.add(this.getTheByLinkBags());
		theMensAccessoriesLinks.add(this.getTheByLinkTowels());
		theMensAccessoriesLinks.add(this.getTheByLinkScarves());
		theMensAccessoriesLinks.add(this.getTheByLinkTies());
		
		System.out.println("IT is reading the correct repository!");
		// theMensAccessoriesLinks.Add(Umbrellas);
	}

	public void CreateMensInternationalFitLinksList() {
		theMensInternationoalFitLinks.add(this
				.getTheByXPathInternationalFitTops());
		theMensInternationoalFitLinks.add(this
				.getTheByXPathInternationalFitBottoms());

	}

	public void CreateMensLinksList() {
//	theMensLinks.add(this.getTheByXPathGiftsForDad());
		theMensLinks.add(this.getTheByXPathSweaters());
		theMensLinks.add(this.getTheByXPathNewArrivals());
		theMensLinks.add(this.getTheByXPathSweatShirts());
		theMensLinks.add(this.getTheByXPathPolos());
		theMensLinks.add(this.getTheByXPathMensTShirts());
		theMensLinks.add(this.getTheByXPathInternationalFit());
		theMensLinks.add(this.getTheByXPathJeans());
		theMensLinks.add(this.getTheByXPathBoxers());
		theMensLinks.add(this.getTheByXPathPants());	
		theMensLinks.add(this.getTheByXPathJackets());	
		theMensLinks.add(this.getTheByXPathShoes());
		theMensLinks.add(this.getTheByXPathCologneGrooming());
		theMensLinks.add(this.getTheByXPathShorts());

	}

	public List<String> getTheMensLinks() {
		return theMensLinks;
	}

	public void setTheMensLinks(List<String> theMensLinks) {
		this.theMensLinks = theMensLinks;
	}

	public List<String> getTheMensShirtsViewAllLinks() {
		return theMensShirtsViewAllLinks;
	}

	public void setTheMensShirtsViewAllLinks(
			List<String> theMensShirtsViewAllLinks) {
		this.theMensShirtsViewAllLinks = theMensShirtsViewAllLinks;
	}

	public List<String> getTheMensMLBViewAllLinks() {
		return theMensMLBViewAllLinks;
	}

	public void setTheMensMLBViewAllLinks(List<String> theMensMLBViewAllLinks) {
		this.theMensMLBViewAllLinks = theMensMLBViewAllLinks;
	}

	public List<String> getTheMensNewArrivalsLinks() {
		return theMensNewArrivalsLinks;
	}

	public void setTheMensNewArrivalsLinks(List<String> theMensNewArrivalsLinks) {
		this.theMensNewArrivalsLinks = theMensNewArrivalsLinks;
	}
	public List<String> getTheMensXpathAccessoriesLinks() {
		return theMensXpathAccessoriesLinks;
	}
	public List<String> getTheMensAccessoriesLinks() {
		return theMensAccessoriesLinks;
	}

	public void setTheMensAccessoriesLinks(List<String> theMensAccessoriesLinks) {
		this.theMensAccessoriesLinks = theMensAccessoriesLinks;
	}

	public List<String> getTheMensMLBLinks() {
		return theMensMLBLinks;
	}

	public void setTheMensMLBLinks(List<String> theMensMLBLinks) {
		this.theMensMLBLinks = theMensMLBLinks;
	}

	public List<String> getTheMensInternationoalFitLinks() {
		return theMensInternationoalFitLinks;
	}

	public void setTheMensInternationoalFitLinks(
			List<String> theMensInternationoalFitLinks) {
		this.theMensInternationoalFitLinks = theMensInternationoalFitLinks;
	}

	public List<String> getTheMensWeddingCollectionLinks() {
		return theMensWeddingCollectionLinks;
	}

	public void setTheMensWeddingCollectionLinks(
			List<String> theMensWeddingCollectionLinks) {
		this.theMensWeddingCollectionLinks = theMensWeddingCollectionLinks;
	}

	public List<String> getTheMensShirtLinks() {
		return theMensShirtLinks;
	}

	public void setTheMensShirtLinks(List<String> theMensShirtLinks) {
		this.theMensShirtLinks = theMensShirtLinks;
	}

	public List<String> getTheMensTShirtLinks() {
		return theMensTShirtLinks;
	}

	public void setTheMensTShirtLinks(List<String> theMensTShirtLinks) {
		this.theMensTShirtLinks = theMensTShirtLinks;
	}

	public List<String> getTheMensPantsLinks() {
		return theMensPantsLinks;
	}

	public void setTheMensPantsLinks(List<String> theMensPantsLinks) {
		this.theMensPantsLinks = theMensPantsLinks;
	}

	public List<String> getTheMensJeansLinks() {
		return theMensJeansLinks;
	}

	public void setTheMensJeansLinks(List<String> theMensJeansLinks) {
		this.theMensJeansLinks = theMensJeansLinks;
	}

	public List<String> getTheMensShortsLinks() {
		return theMensShortsLinks;
	}

	public void setTheMensShortsLinks(List<String> theMensShortsLinks) {
		this.theMensShortsLinks = theMensShortsLinks;
	}

	public List<String> getTheMensSwimLinks() {
		return theMensSwimLinks;
	}

	public void setTheMensSwimLinks(List<String> theMensSwimLinks) {
		this.theMensSwimLinks = theMensSwimLinks;
	}

	public List<String> getTheMensCologneGroomingLinks() {
		return theMensCologneGroomingLinks;
	}

	public void setTheMensCologneGroomingLinks(
			List<String> theMensCologneGroomingLinks) {
		this.theMensCologneGroomingLinks = theMensCologneGroomingLinks;
	}

	public String getTheByXPathShirts() {
		return theByXPathShirts;
	}

	public String getTheByXPathGolf() {
		return theByXPathGolf;
	}

	public String getTheByXPathCologneGrooming() {
		return theByXPathCologneGrooming;
	}

	public String getTheByXPathMensTShirts() {
		return theByXPathMensTShirts;
	}

	public String getTheByXPathAccessories() {
		return theByXPathAccessories;
	}

	public String getTheByXPathShoes() {
		return theByXPathShoes;
	}

	public String getTheByXPathInternationalFit() {
		return theByXPathInternationalFit;
	}

	public String getTheByXPathGuestFavorites() {
		return theByXPathGuestFavorites;
	}

	public String getTheByXPathPolos() {
		return theByXPathPolos;
	}

	public String getTheByXPathSweatShirts() {
		return theByXPathSweatShirts;
	}

	public String getTheByXPathSweaters() {
		return theByXPathSweaters;
	}

	public String getTheByXPathJackets() {
		return theByXPathJackets;
	}

	public String getTheByXPathPants() {
		return theByXPathPants;
	}

	public String getTheByXPathJeans() {
		return theByXPathJeans;
	}

	public String getTheByXPathShorts() {
		return theByXPathShorts;
	}

	public String getTheByXPathSwim() {
		return theByXPathSwim;
	}

	public String getTheByXPathBoxers() {
		return theByXPathBoxers;
	}

	public String getTheByXPathWedding() {
		return theByXPathWedding;
	}

	public String getTheByXPathNAShirts() {
		return theByXPathNAShirts;
	}

	public String getTheByXPathNAPolos() {
		return theByXPathNAPolos;
	}

	public String getTheByXPathNASweaters() {
		return theByXPathNASweaters;
	}

	public String getTheByXPathNAJackets() {
		return theByXPathNAJackets;
	}

	public String getTheByXPathNAShorts() {
		return theByXPathNAShorts;
	}

	public String getTheByXPathNASwim() {
		return theByXPathNASwim;
	}

	public String getTheByXPathNAAccessories() {
		return theByXPathNAAccessories;
	}

	public String getTheByLinkLongSleeveShirts() {
		return theByLinkLongSleeveShirts;
	}

	public String getTheByLinkSolidShirts() {
		return theByLinkSolidShirts;
	}

	public String getTheByLinkShirtsWithPockets() {
		return theByLinkShirtsWithPockets;
	}

	public String getTheByLinkSilkShirts() {
		return theByLinkSilkShirts;
	}

	public String getTheByLinkLinenShirts() {
		return theByLinkLinenShirts;
	}

	public String getTheShirtsViewAll() {
		return theShirtsViewAll;
	}

	public String getTheByLinkClassicFit() {
		return theByLinkClassicFit;
	}

	public String getTheByLinkAuthenticFit() {
		return theByLinkAuthenticFit;
	}

	public String getTheByLinkStandardFit() {
		return theByLinkStandardFit;
	}

	public String getTheByLinkEasyFit() {
		return theByLinkEasyFit;
	}

	public String getTheByLinkFlatFrontShorts() {
		return theByLinkFlatFrontShorts;
	}

	public String getTheByLinkCargoShorts() {
		return theByLinkCargoShorts;
	}

	public String getTheByLinkPatternShorts() {
		return theByLinkPatternShorts;
	}

	public String getTheByLinkPleatedShorts() {
		return theByLinkPleatedShorts;
	}

	public String getTheByLinkGraphicTShirts() {
		return theByLinkGraphicTShirts;
	}

	public String getTheByLinkMensSwimShop() {
		return theByLinkMensSwimShop;
	}

	public String getTheByXPathMLB() {
		return theByXPathMLB;
	}

	public String getTheByXPathViewAll() {
		return theByXPathViewAll;
	}

	public String getTheByXPathCollectorsSeries() {
		return theByXPathCollectorsSeries;
	}

	public String getTheByXPathTeamTrackJackets() {
		return theByXPathTeamTrackJackets;
	}

	public String getTheByXPathTeamHalfZips() {
		return theByXPathTeamHalfZips;
	}

	public String getTheByXPathTeamShirts() {
		return theByXPathTeamShirts;
	}

	public String getTheByXPathInternationalFitTops() {
		return theByXPathInternationalFitTops;
	}

	public String getTheByXPathInternationalFitBottoms() {
		return theByXPathInternationalFitBottoms;
	}

	public String getTheByLinkForTheGroom() {
		return theByLinkForTheGroom;
	}

	public String getTheByLinkForTheGroomsmen() {
		return theByLinkForTheGroomsmen;
	}

	public String getTheByLinkGroomsmenGifts() {
		return theByLinkGroomsmenGifts;
	}

	public String getTheByLinkWeddingGifts() {
		return theByLinkWeddingGifts;
	}

	public String getTheByLinkSocks() {
		return theByLinkSocks;
	}

	public String getTheByLinkHats() {
		return theByLinkHats;
	}

	public String getTheByLinkSunglasses() {
		return theByLinkSunglasses;
	}

	public String getTheByLinkBelts() {
		return theByLinkBelts;
	}

	public String getTheByLinkWatches() {
		return theByLinkWatches;
	}

	public String getTheByLinkWatchStraps() {
		return theByLinkWatchStraps;
	}

	public String getTheByLinkWallet() {
		return theByLinkWallet;
	}

	public String getTheByLinkBags() {
		return theByLinkBags;
	}

	public String getTheByLinkTowels() {
		return theByLinkTowels;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathCreditCardsLink() {
		return theByXPathCreditCardsLink;
	}

	public String getTheByLinkSetSailMartinique() {
		return theByLinkSetSailMartinique;
	}

	public String getTheByLinkStBarts() {
		return theByLinkStBarts;
	}

	public String getTheByLinkTBForHim() {
		return theByLinkTBForHim;
	}

	public String getTheByLinkViewAll() {
		return theByLinkViewAll;
	}

	public String getTheByLinkTeamTrackJackets() {
		return theByLinkTeamTrackJackets;
	}

	public String getTheByLinkTeamHalfZips() {
		return theByLinkTeamHalfZips;
	}

	public String getTheByLinkTeamShirts() {
		return theByLinkTeamShirts;
	}

	public String getTheByLinkTechCases() {
		return theByLinkTechCases;
	}

	public String getTheByLinkVintageParadise() {
		return theByLinkVintageParadise;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByXPathNATshirts() {
		return theByXPathNATshirts;
	}

	public String getTheByXPathNABoxers() {
		return theByXPathNABoxers;
	}

	public String getTheByLinkSolidTShirts() {
		return theByLinkSolidTShirts;
	}

	public String getTheByLinkDoublePleat() {
		return theByLinkDoublePleat;
	}


	public String getTheByLinkFlatFront() {
		return theByLinkFlatFront;
	}

	public String getTheByLinkAuthenticJeansFit() {
		return theByLinkAuthenticJeansFit;
	}

	public String getTheByLinkStandardJeansFit() {
		return theByLinkStandardJeansFit;
	}

	public String getTheByLinkVintageStraightJeansFit() {
		return theByLinkVintageStraightJeansFit;
	}

	public String getTheByLinkTechCollection() {
		return theByLinkTechCollection;
	}

	public String gettheByXpath4InchInseam() {
		return theByXpath4InchInseam;
	}

	public String gettheByXpath7InchInseam() {
		return theByXpath7InchInseam;
	}

	public String gettheByXpath9InchInseam() {
		return theByXpath9InchInseam;
	}

	public String getTheByLinkCompass() {
		return theByLinkCompass;
	}

	public String getTheByLinkShortSleeveShirts() {
		return theByLinkShortSleeveShirts;
	}

	public String getTheByLinkDressShirts() {
		return theByLinkDressShirts;
	}

	public String getTheByLinkIslandShirts() {
		return theByLinkIslandShirts;
	}

	public String getTheByLinkPanelBackShirts() {
		return theByLinkPanelBackShirts;
	}

	public String getTheByLinkScarves() {
		return theByLinkScarves;
	}

	public String getTheByLinkTies() {
		return theByLinkTies;
	}

	public String getTheByXPathJewelryLink() {
		return theByXPathJewelryLink;
	}

	public String getTheByXPathCigarwareLink() {
		return theByXPathCigarwareLink;
	}

	public String getTheByXPathColongeLink() {
		return theByXPathColongeLink;
	}

	public String getTheByXPathAccessorieBoxersLink() {
		return theByXPathAccessorieBoxersLink;
	}

	public String getTheByXPathGiftsForDad() {
		return theByXPathGiftsForDad;
	}

	public String getTheMensIndex() {
		return theMensIndex;
	}

	public String getTheMlbIndex() {
		return theMlbIndex;
	}

	public String getTheWeddingIndex() {
		return theWeddingIndex;
	}

}
