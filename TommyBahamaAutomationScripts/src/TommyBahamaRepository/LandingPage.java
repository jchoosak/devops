package TommyBahamaRepository;

public class LandingPage {

	private final String theByIdShoppingLink = "topNavSizeId";
	private final String theWdShoppingLink = "topNavSizeId";
	private final String thePageName = "HomePage";

	public String getTheByIdShoppingLink() {
		return theByIdShoppingLink;
	}

	public String getTheWdShoppingLink() {
		return theWdShoppingLink;
	}

	public String getThePageName() {
		return thePageName;
	}

}
