package TommyBahamaRepository;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Header {
	private final String theCanadaShippingPromoBanner = "//div[@id='divPromo']/img";
	private final String theFiftyOneShippingPromoBanner = "//span[@id='topNavRightShipping']/a[2]/img";
	private final String theLanguageLayer = "//div[@id='button']/button";
	private final String theFiftyOneLayer = "TB_iframeContent";
	private final String theByCSSLogo = "img[alt='Tommy Bahama®']";
	private final String theByXPathLogo = "//div[@id='divTopNavLogo']/a/img";
	// private final String theCountryFlag =
	// "//div[@id='divTopStripRight']/span[2]/a[2]/img";
	
	private final String theByLinkCanadaFlag = "Canada";
	
	private final String theByIdaSignedIn = "aSignedIn";
	private final String theCountryFlag = "//span[@id='topNavRightShipping']/a[2]/img";
	private final String theByCSSUSHeaderFlag = "img[alt='United States']";
	private final String theByXPathCanadaFlag = "//img[@alt='Canada']";
	private final String theByXPathAustraliaFlag = "//img[@alt='Australia']";
	private final String theByXPathTrackYourOrder = "//a[contains(text(),'Track Your Order')]";
	private final String theByCSSUnitedStatesFlag = "img[alt='United States']";
	private final String theByXPathRegistrationLink = "//span[@id='spanAccountAnon']/a[2]/span";
	private final String autoComplete = "//ul";
	private final String advancedSearchOptions = "advanced_search";
	private final String theByXPathSignOut = "//a[contains(text(),'Sign Out')]";
	// private final String theSignIn =
	// "//span[@id='spanAccountAnon']/a[2]/span";
	private final String theSignIn = "//span/a[2]/span";
	private final String theByXPathEmailSignUp = "//div[@id='divTopStripLeft']/a[3]";
	

	private final String theCartCount = "//div[@id='divNumberOfItems']/a";
	// private final String theSearchInput = "//input[@id='inputSearchFor']";
	private final String theSearchBtn = "//form[@id='frmSearch']/div/a/img";

	private final String theByXPathMensTab = "//a[contains(text(),'Men')]";
	private final String theByXPathWomensTab = "//a[contains(text(),'Women')]";
	private final String theByXpathHomeDecorTab = "//a[contains(text(),'Beach, Home & Gifts')]";
	private final String theByXPathBigAndTallTab = "//a[contains(text(),'Big & Tall')]";
	private final String theByXPathSwimTab = "(//a[contains(text(),'Swim')])[4]";
	private final String theByIdWomensSwimTab = "topNavLISwim";
	private final String theByXPathStoresAndRestaurantsTab = "//li[@id='topNavLIStores']/a";
	private final String theByXPathLiveTheLifeTab = "//li[@id='topNavLILive']/a/span";
	private final String theByLink20thAnniversary = "20th Anniversary";

	// private final String theByLinkTextMensTab = "Men";
	private final String twoItemsInCheckout = "//a[contains(text(),'2 ITEMS PREVIEW')]";
	private final String theByIdPreviewCheckout = "checkout";
	private final String theFooterEmailBtn = "//div[@id='footerEmailSignUpInner']/form/div";
	private final String theFooterEmailInput = "//input[@id='emailFormFooterId']";
	private final String theItemPreviewShopping = "//a[@id='shopping']";
	private final String theByLinkCheckout = "CHECKOUT";
	private final String theByLinkFindStore = "//a[contains(text(),'Find a Tommy Bahama Store')]";
	private final String theUrlType = "http://";
	private final String theMyAccount = "//span[@id='spanAccountAnon']/a/span";
	private final String theByIdCanadaWelcome = "divCanadaWelcomeMat";
	private final String theByCSSCanadaShippingPromo = "#divPromo > img";
	private final String theByCSSShippingParadaisePromo = "#divPromo > a > img";
	private final String theByXPathSearchInput = "//input[@id='q']";
	private final String theQuickViewContent = "//div[@id='quickview-content']";
	private final String theByIdQuickViewContent = "quickview-content";
	private final String theSignedInAccount = "//span[@id='aSignedIn']";
	private final String thePageName = "Header";
	
	private final String theByIdEmailAddress = "txtEmail";
	private final String theByIdFirstName = "txtFirstName";
	private final String theByIdLastName = "txtLastName";
	private final String theByIdZipCode = "txtZipCode";
	private final String theByIdMonth = "txtDOB_mm";
	private final String theByIdDay = "txtDOB_dd";
	private final String theByIdYear = "txtDOB_yyyy";

	private final String theByNameCountry = "country";
	private final String theByNameGender = "Gender";
	private final String theByIdTBCheckBox = "chkTommyBahamaEmail";
	private final String theByIdRestaurantCheckBox = "rest";
	private final String theByIdBigAndTallCheckBox = "chkBigAndTall";
	private final String theByXpathGender = "//div[@id='divGender']/input[4]";
	
	private final String theByCssSavePreferences = "img[alt='Save Preferences']";
	private final String theByCssStartShopping = "img[alt='Start Shopping']";
    private final String theByCssCloseEmailSignUp = "a.fancybox-item.fancybox-close";
    private final String theByCssMenSwim = "area[alt='Men's swim']";
    
    private final String theBigAndTallIndex = "Big&TallIndex";
    private final String theSwimIndex = "SwimIndex";
    private final String theHomeDecorIndex = "HomeDecorIndex";
    private final String theStoresIndex = "Stores&RestaurantsIndex";
    private final String  theLiveTheLifeIndex = "LiveTheLifeIndex";

	
	private WebDriver driver;
	
	private SeleniumEnvironment myEnvironment;
	
	public Header(WebDriver theDriver, SeleniumEnvironment theEnvironment)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
	}
	
	public String getTheCanadaShippingPromoBanner() {
		return theCanadaShippingPromoBanner;
	}

	public String getTheFiftyOneShippingPromoBanner() {
		return theFiftyOneShippingPromoBanner;
	}

	public String getTheLanguageLayer() {
		return theLanguageLayer;
	}

	public String getTheFiftyOneLayer() {
		return theFiftyOneLayer;
	}

	public String getTheByCSSLogo() {
		return theByCSSLogo;
	}

	public String getTheCountryFlag() {
		return theCountryFlag;
	}

	public String getTheByCSSUSHeaderFlag() {
		return theByCSSUSHeaderFlag;
	}

	public String getTheByXPathCanadaFlag() {
		return theByXPathCanadaFlag;
	}

	public String getTheByXPathAustraliaFlag() {
		return theByXPathAustraliaFlag;
	}

	public String getTheByXPathTrackYourOrder() {
		return theByXPathTrackYourOrder;
	}

	public String getTheByCSSUnitedStatesFlag() {
		return theByCSSUnitedStatesFlag;
	}

	public String getTheByXPathRegistrationLink() {
		return theByXPathRegistrationLink;
	}

	public String getAutoComplete() {
		return autoComplete;
	}

	public String getAdvancedSearchOptions() {
		return advancedSearchOptions;
	}

	public String getTheByXPathSignOut() {
		return theByXPathSignOut;
	}

	public String getTheSignIn() {
		return theSignIn;
	}

	public String getTheCartCount() {
		return theCartCount;
	}

	public String getTheSearchBtn() {
		return theSearchBtn;
	}

	public String getTheByXPathMensTab() {
		return theByXPathMensTab;
	}

	public String getTheByXPathWomensTab() {
		return theByXPathWomensTab;
	}

	public String getTheByXPathHomeDecorTab() {
		return theByXpathHomeDecorTab;
	}

	public String getTheByXPathBigAndTallTab() {
		return theByXPathBigAndTallTab;
	}

	public String getTheByXPathSwimTab() {
		return theByXPathSwimTab;
	}

	public String getTheByXPathStoresAndRestaurantsTab() {
		return theByXPathStoresAndRestaurantsTab;
	}

	public String getTheByXPathLiveTheLifeTab() {
		return theByXPathLiveTheLifeTab;
	}

	public String getTwoItemsInCheckout() {
		return twoItemsInCheckout;
	}

	public String getTheByIdPreviewCheckout() {
		return theByIdPreviewCheckout;
	}

	public String getTheFooterEmailBtn() {
		return theFooterEmailBtn;
	}

	public String getTheFooterEmailInput() {
		return theFooterEmailInput;
	}

	public String getTheItemPreviewShopping() {
		return theItemPreviewShopping;
	}

	public String getTheByLinkCheckout() {
		return theByLinkCheckout;
	}

	public String getTheByLinkFindStore() {
		return theByLinkFindStore;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheMyAccount() {
		return theMyAccount;
	}

	public String getTheByIdCanadaWelcome() {
		return theByIdCanadaWelcome;
	}

	public String getTheByCSSCanadaShippingPromo() {
		return theByCSSCanadaShippingPromo;
	}

	public String getTheByCSSShippingParadaisePromo() {
		return theByCSSShippingParadaisePromo;
	}

	public String getTheByXPathSearchInput() {
		return theByXPathSearchInput;
	}

	public String getTheQuickViewContent() {
		return theQuickViewContent;
	}

	public String getTheSignedInAccount() {
		return theSignedInAccount;
	}

	public String getTheByXPathLogo() {
		return theByXPathLogo;
	}

	public String getTheByLink20thAnniversary() {
		return theByLink20thAnniversary;
	}

	public String getTheByIdQuickViewContent() {
		return theByIdQuickViewContent;
	}

	public void checkUrl(String thePageUrl, String theUrl) {
		Assert.assertTrue(theUrl.contains(thePageUrl));
	}

	/*
	 * Method to select a Fifty one country Passed the Webdriver object and the
	 * enviroment, fiftyone class objects and the country to change to
	 */
	
	public void selectCountry(WebDriver driver,
			SeleniumEnvironment myEnvironment, FiftyOne myFiftyOneObjs,
			String theCountry, String theTest) throws InterruptedException {
		// WebDriverWait wait = new WebDriverWait(driver,
		// TimeSpan.FromSeconds(120));
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

		WebElement currentElement;
		// Selects the flag in header to change to United States
		// myEnvironment.myWaitForWDElement(driver, By.XPath(CountryFlag));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theCountryFlag));
		currentElement.click();
	//	Thread.sleep(1000);
		// this grabs focus on the Flags layer
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(theFiftyOneLayer));
		driver.switchTo().frame(currentElement);
		Thread.sleep(2000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theCountry));
		currentElement.click();
		Thread.sleep(1000);
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				theTest, "ContextChooser", "");
		if (theCountry.contains("Canada")) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.cssSelector("div.CanadaCurrency"));

		} 
		else if (theCountry.contains("United Kingdom"))
		{
			currentElement = myEnvironment.waitForDynamicElement(
					By.cssSelector("#currencyCode > option[value=\"GBP\"]"));
		Thread.sleep(1000);
	/*	currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector("img[alt='Set Country and Currency']"));
		currentElement.click();*/
		//Thread.sleep(4000);
		}
		else
			currentElement = myEnvironment.waitForDynamicElement(
					By.cssSelector("div.USCurrency"));
		Thread.sleep(4000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector("img[alt='Set Country and Currency']"));
		currentElement.click();
		Thread.sleep(4000);
		// Thread.Sleep(10000);
		// this makes sure that focus is not longer on the flag layer and
		// defaultly goes to the main page
		driver.switchTo().defaultContent();

	}

	/*
	 * this is a method to check that the correct flag and advertisment are
	 * displaying for the chosen fifty one country Passed the webdriver object,
	 * the enviroment object, the url and the associated promo that is displayed
	 * in header, the flag that should be displayed.
	 */
	public void checkFlagInHeader(WebDriver driver,
			SeleniumEnvironment myEnvironment, String url, By theFlag,
			String thePromo) {
		String theShippingPromoBannerImage = "";
		WebElement currentElement;
		if (myEnvironment.isElementPresent(theFlag)) {
			System.out.println("The Correct Flag is Present.");
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(thePromo));
			System.out.println("This is the image of the Banner:     "
					+ currentElement.getAttribute("src"));
			theShippingPromoBannerImage = currentElement.getAttribute("src");
			if (url.contains("https:")) {
				// theShippingPromoBannerImage =
				// theShippingPromoBannerImage.Remove(0, 14);
				Assert.assertTrue(theShippingPromoBannerImage
						.contains("/media/TB001/images/static/"));
				System.out
						.println("Shipping  Banner is appearing on this secure Page!");
			} else {
				// theShippingPromoBannerImage =
				// theShippingPromoBannerImage.Remove(0, 13);
				Assert.assertTrue(theShippingPromoBannerImage
						.contains("/media/TB001/images/static/"));
				System.out
						.println("Shipping  Banner is appearing on this NON secure Page!");
			}
		}
	}

	public String getTheByIdaSignedIn() {
		return theByIdaSignedIn;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByXPathEmailSignUp() {
		return theByXPathEmailSignUp;
	}

	public String getTheByIdEmailAddress() {
		return theByIdEmailAddress;
	}

	public String getTheByIdFirstName() {
		return theByIdFirstName;
	}

	public String getTheByIdLastName() {
		return theByIdLastName;
	}

	public String getTheByIdZipCode() {
		return theByIdZipCode;
	}

	public String getTheByIdMonth() {
		return theByIdMonth;
	}

	public String getTheByIdDay() {
		return theByIdDay;
	}

	public String getTheByIdYear() {
		return theByIdYear;
	}


	public String getTheByCssSavePreferences() {
		return theByCssSavePreferences;
	}

	public String getTheByNameGender() {
		return theByNameGender;
	}

	public String getTheByCssStartShopping() {
		return theByCssStartShopping;
	}

	public String getTheByCssCloseEmailSignUp() {
		return theByCssCloseEmailSignUp;
	}

	public String getTheByCssMenSwim() {
		return theByCssMenSwim;
	}

	public String getTheByLinkCanadaFlag() {
		return theByLinkCanadaFlag;
	}


	public String getTheByIdWomensSwimTab() {
		return theByIdWomensSwimTab;
	}

	public String getTheBigAndTallIndex() {
		return theBigAndTallIndex;
	}

	public String getTheSwimIndex() {
		return theSwimIndex;
	}

	public String getTheHomeDecorIndex() {
		return theHomeDecorIndex;
	}

	public String getTheStoresIndex() {
		return theStoresIndex;
	}

	public String getTheLiveTheLifeIndex() {
		return theLiveTheLifeIndex;
	}

	public String getTheByNameCountry() {
		return theByNameCountry;
	}

	public String getTheByIdTBCheckBox() {
		return theByIdTBCheckBox;
	}

	public String getTheByIdRestaurantCheckBox() {
		return theByIdRestaurantCheckBox;
	}

	public String getTheByIdBigAndTallCheckBox() {
		return theByIdBigAndTallCheckBox;
	}

	public String getTheByXpathGender() {
		return theByXpathGender;
	}

}
