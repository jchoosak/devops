package TommyBahamaRepository;

public class FiftyOne {
	private final String theUnitedStatesFlag = "//a[@id='deleteCountry']";
	private final String theCanadaFlag = "//a[contains(text(),'Canada')]";
	//private final String theCanadaFlag = "//img[@alt='Canada']";
	//private final String theCanadaFlag = "//a[@id='CA')]";
	//private final String theCanadaFlag = "Canada";
	private final String theUnitedKingdomFlag = "//a[contains(text(),'United Kingdom')]";
	private final String theAustralianFlag = "//a[contains(text(),'Australia')]";
	private final String theSelectButton = "//div[@id='divUpdateCountry']/a/img";
	private final String theByIdBacKToBtn = "back-to-ca-btn";
	private final String theByXPathTrackOrderBtn = "//div[@id='orderStatusIntlContainerId']/div/a/img";
	private final String thePageName = "FiftyOne";
	
	public String getTheUnitedStatesFlag() {
		return theUnitedStatesFlag;
	}

	public String getTheCanadaFlag() {
		return theCanadaFlag;
	}

	public String getTheAustralianFlag() {
		return theAustralianFlag;
	}

	public String getTheSelectButton() {
		return theSelectButton;
	}

	public String getTheByIdBacKToBtn() {
		return theByIdBacKToBtn;
	}

	public String getTheByXPathTrackOrderBtn() {
		return theByXPathTrackOrderBtn;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheUnitedKingdomFlag() {
		return theUnitedKingdomFlag;
	}

}
