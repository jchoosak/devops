package TommyBahamaRepository;

public class ConfirmationPage {
	private final String theByLinkEmailGS = "email Guest Services";
	private final String theURL = "https://";
	private final String theOrderNumber = "//div[@id='order-confirmation']/h3";
	private final String theShippingAmount = "//table[@id='billing-summary']/tbody/tr/td[3]/span[2]";
	private final String theByIdTotalShipping = "total_shipping";
	private final String thePageName = "ConfirmationPage";

	public String getTheByLinkEmailGS() {
		return theByLinkEmailGS;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheOrderNumber() {
		return theOrderNumber;
	}

	public String getTheShippingAmount() {
		return theShippingAmount;
	}

	public String getTheByIdTotalShipping() {
		return theByIdTotalShipping;
	}

	public String getThePageName() {
		return thePageName;
	}

}
