package TommyBahamaRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PaymentPage {

	private final String theGiftCardNumber = "6035718888930018891";
	private final String theGiftCardPin = "2450";
	private final String theCC = "5058";
	private final String theCode = "111";
	private final String theYear = "2017";
	private final String theMonth = "05";
	private final String theCCNumber = "4111111111111111";

	private final String thePaymentTitle = "Payment Information";
	private final String theByIdCardBalenceLink = "gc_balance_anchor";
	private final String theURL = "https://";
	private final String theByIdTopContinueCheckoutBtn = "button-top";
	private final String theBottomCCBtn = "//input[@id='button-bottom']";
	private final String theByIdCCName = "credit-card-name-required";
	private final String theName = "Jack West";
	private final String theByIdCardType = "credit-card-type";
	private final String theCard = "Visa";
	private final String theByIdCardNumber = "credit-card-number";

	private final String theByIdCCMonth = "credit-card-month";

	private final String theByIdCCYear = "credit-card-year";

	private final String theByIdCCSecurityCode = "credit-card-security-code";

	private final String theByIdSavedCC = "credit-card-saved";

	private final String theByIdCardRequired = "credit-card-name-required";
	private final String theNewCC = "//select[@id='credit-card-saved']/option[5]";
	private final String theGroundShipping = "id=shipping-method-standard";
	private final String theThreeDayShipping = "(//input[@id='shipping-method-standard'])[2]";
	private final String theNextDayShipping = "(//input[@id='shipping-method-standard'])[4]";
	private final String theByIdTwoDayShipping = "shipping-method-standard";
	private final String theByXPathTwoDayShipping = "(//input[@id='shipping-method-standard'])[3]";
	private final String theNextDaySatShipping = "(//input[@id='shipping-method-standard'])[5]";
	private final String theByIdGiftCardNumberInput = "gift-card-number";
	private final String theByIdCheckCardBalanceBtn = "check-gift-card-balance-submit";
	private final String theByIdCardBalence = "balance_holder";

	private final String theByLinkX = "X";
	private final String theShipingPrice = "//div[@id='order-summary']/dl[3]/dd";
	// div[@id='order-summary']/dl[2]/dd
	private final String theByXPathCanadaShippingPrice = "//div[@id='order-summary']/dl[2]/dd";
	private final String theCanadaDutyPrice = "//div[@id='order-summary']/dl[3]/dd";
	private final String theCanadaTax = "//div[@id='order-summary']/dl[4]/dd";
	private final Double theActualGroundShippingPrice = 8.00;
	private final Double theActualCanadaShippingPrice = 16.00;
	private final Double theFreeCanadaShippingPrice = 0.00;
	private final String theActualCanadaShippingString = "$16.00";
	private final String theByCSSError = "div.site_error";
	private final String theByLinkReturnToShoppingBag = "//a[contains(text(),'Return to Shopping Bag')]";
	private final String theByCSSReturnToShoppingBag = "p.return-to-shopping-bag > a";
	private final String theByIdGiftCardInput = "gift-card-number";
	private final String theByIdGiftCardPinInput = "gift-card-pin";
	private final String theByIdGiftCardBtn = "gift-card-button";
	private final String theGiftCardAddAmount = "25.00";
	private final String theTotalAmount = "css=dl.total > dd";
	private final String theTotalAmountAfterGiftCard = "css=dl.total-payment > dd";
	private final String theTaxAmount = "css=dl.sales > dd";
	private final String theByCSSCanadaFlipSideError = "div.site_message";
	private final String theCanadaFlipSideErrorText = "- Your free gift has been removed from your order because it is unavailable to ship to international addresses. ";
	private final String theGiftCardPartialPayment = "dl.promo.gift-card > dd";
	private final String thePageName = "PaymentPage";
	private final String thePageTitle = "Payment";
  	private final String theGiftCardBalenceModal = "GiftCardBalenceModal";
  	private final String thePageFilledOut = "Completed";
  	private final String theGiftCardApplied = "GiftCardApplied";
	
	private SeleniumEnvironment myEnvironment;
	
	private String testName;
	
	
	public PaymentPage(SeleniumEnvironment theEnvironment,
			String theTestName)
	{
		myEnvironment = theEnvironment;

		testName = theTestName;
		
	}

	public String getThePaymentTitle() {
		return thePaymentTitle;
	}

	public String getTheByIdCardBalenceLink() {
		return theByIdCardBalenceLink;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheByIdTopContinueCheckoutBtn() {
		return theByIdTopContinueCheckoutBtn;
	}

	public String getTheBottomCCBtn() {
		return theBottomCCBtn;
	}

	public String getTheByIdCCName() {
		return theByIdCCName;
	}

	public String getTheName() {
		return theName;
	}

	public String getTheByIdCardType() {
		return theByIdCardType;
	}

	public String getTheCard() {
		return theCard;
	}

	public String getTheByIdCardNumber() {
		return theByIdCardNumber;
	}

	public String getTheCCNumber() {
		return theCCNumber;
	}

	public String getTheByIdCCMonth() {
		return theByIdCCMonth;
	}

	public String getTheMonth() {
		return theMonth;
	}

	public String getTheByIdCCYear() {
		return theByIdCCYear;
	}

	public String getTheYear() {
		return theYear;
	}

	public String getTheByIdCCSecurityCode() {
		return theByIdCCSecurityCode;
	}

	public String getTheCode() {
		return theCode;
	}

	public String getTheByIdSavedCC() {
		return theByIdSavedCC;
	}

	public String getTheCC() {
		return theCC;
	}

	public String getTheByIdCardRequired() {
		return theByIdCardRequired;
	}

	public String getTheNewCC() {
		return theNewCC;
	}

	public String getTheGroundShipping() {
		return theGroundShipping;
	}

	public String getTheThreeDayShipping() {
		return theThreeDayShipping;
	}

	public String getTheNextDayShipping() {
		return theNextDayShipping;
	}

	public String getTheByXPathTwoDayShipping() {
		return theByXPathTwoDayShipping;
	}

	public String getTheNextDaySatShipping() {
		return theNextDaySatShipping;
	}

	public String getTheByIdGiftCardNumberInput() {
		return theByIdGiftCardNumberInput;
	}

	public String getTheByIdCheckCardBalanceBtn() {
		return theByIdCheckCardBalanceBtn;
	}

	public String getTheByIdCardBalence() {
		return theByIdCardBalence;
	}

	public String getTheGiftCardNumber() {
		return theGiftCardNumber;
	}

	public String getTheGiftCardPin() {
		return theGiftCardPin;
	}

	public String getTheByLinkX() {
		return theByLinkX;
	}

	public String getTheShipingPrice() {
		return theShipingPrice;
	}

	public String getTheByXPathCanadaShippingPrice() {
		return theByXPathCanadaShippingPrice;
	}

	public String getTheCanadaDutyPrice() {
		return theCanadaDutyPrice;
	}

	public String getTheCanadaTax() {
		return theCanadaTax;
	}

	public Double getTheActualGroundShippingPrice() {
		return theActualGroundShippingPrice;
	}

	

	public Double getTheActualCanadaShippingPrice() {
		return theActualCanadaShippingPrice;
	}

	

	public String getTheByCSSError() {
		return theByCSSError;
	}

	public String getTheByLinkReturnToShoppingBag() {
		return theByLinkReturnToShoppingBag;
	}

	public String getTheByCSSReturnToShoppingBag() {
		return theByCSSReturnToShoppingBag;
	}

	public String getTheByIdGiftCardInput() {
		return theByIdGiftCardInput;
	}

	public String getTheByIdGiftCardPinInput() {
		return theByIdGiftCardPinInput;
	}

	public String getTheByIdGiftCardBtn() {
		return theByIdGiftCardBtn;
	}

	public String getTheGiftCardAddAmount() {
		return theGiftCardAddAmount;
	}

	public String getTheTotalAmount() {
		return theTotalAmount;
	}

	public String getTheTotalAmountAfterGiftCard() {
		return theTotalAmountAfterGiftCard;
	}

	public String getTheTaxAmount() {
		return theTaxAmount;
	}

	public String getTheByCSSCanadaFlipSideError() {
		return theByCSSCanadaFlipSideError;
	}

	public String getTheCanadaFlipSideErrorText() {
		return theCanadaFlipSideErrorText;
	}

	public String getTheGiftCardPartialPayment() {
		return theGiftCardPartialPayment;
	}

	/*
	 * Method to enter CC info on the payment page. Passed the webdriver and
	 * environment objects.
	 */
	public void enterCCInfo()
      {
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCardRequired()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCCName()));
		currentElement.sendKeys(this.theName);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCardType()));
		Select clickThis = new Select(currentElement);
		clickThis.selectByVisibleText(this.theCard);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCardNumber()));
		currentElement.sendKeys(this.theCCNumber);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCCMonth()));
		clickThis = new Select(currentElement);
		clickThis.selectByVisibleText(this.theMonth);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCCYear()));
		clickThis = new Select(currentElement);
		clickThis.selectByVisibleText(this.theYear);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCCSecurityCode()));
		currentElement.sendKeys(this.theCode);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdTopContinueCheckoutBtn()));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
				"PaymentPage", "Completed");
		currentElement.click();
	}

/*	public void checkGcBalence()
			throws InterruptedException {
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCardBalenceLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(this.theGiftCardNumber);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCardBalence()));
		// String theBalence = currentElement.getText();
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
				"PaymentPage", "GiftCardBalenceModal");
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(this.getTheByLinkX()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdGiftCardInput()));
		currentElement.clear();
	}*/
	
	public void checkGcBalence()
			throws InterruptedException {
		WebElement currentElement;
		// see that Card Services are working
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdCardBalenceLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		myEnvironment.handleMultipleWindows( "Gift Card Balance Lookup");
		
/*		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));*/
		// String theBalence = currentElement.getText();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "PaymentPage", "GiftCardBalenceModal");
	
	}

	// /*
	// * This method pulls the price out of a string and then converts it to a
	// double to be returned.
	// * Passed the string containing the price to be pulled and returned as a
	// double.
	// */
	// private Object theByIdCheckCardBalenceBtn() {
	// // TODO Auto-generated method stub
	// return null;
	// }
	public Double pullPrice(String theString) {
		Double thePrice = 0.0;
		theString = theString.substring(1);
		thePrice = Double.parseDouble(theString);
		return thePrice;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getThePageTitle() {
		return thePageTitle;
	}

	public String getTheActualCanadaShippingString() {
		return theActualCanadaShippingString;
	}

	public Double getTheFreeCanadaShippingPrice() {
		return theFreeCanadaShippingPrice;
	}

	public String getTheByIdTwoDayShipping() {
		return theByIdTwoDayShipping;
	}

	public String getTheGiftCardBalenceModal() {
		return theGiftCardBalenceModal;
	}

	public String getThePageFilledOut() {
		return thePageFilledOut;
	}

	public String getTheGiftCardApplied() {
		return theGiftCardApplied;
	}

}
