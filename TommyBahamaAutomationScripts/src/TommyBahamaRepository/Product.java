package TommyBahamaRepository;




public class Product {

	
private String productId;
private final String theShoeProduct = "TFM00206";
private final String theDinnerWareProduct = "TH2459";
private final String theGolfBag = "TBG-099";
private final String theDiffusor = "TH30875";
private final String theJacket = "T5667";
private final String theSearchProduct = "TB6020";
private final String theHighTShirtPrice = "$72.00";	
private final String theLowTShirtPrice = "$38.00";
private final String theMidTShirtPrice = "$48.00";
private final String theGBPTShirtPrice = "66.27";
private final String theHighPantsPrice = "$128.00";
private final String theLowPantsPrice = "$98.00";
private final String theColor = "3426";
private final String theSunglasses = "TB6036";
private final String theDefaultSockPrice = "$20.00";
private final String theJeansPrice = "$88.00";
private final String theBike = "TH30319";
private final String theBeachProduct = "TH31689";
private String theOversizedProduct = "";
private final String theDinnerWareName = "Dinnerware";
private final String thePantsName = "Pants";
private final String theGolfName = "GolfBag";
private final String theDiffusorName = "Diffuser";
private final String theTraditionalGiftCardName = "TraditionalGC";
private final String theShippingRestriction = "ShippingRestriction";
private final String thePageName = "Products";
private final String theTshirt = "T-Shirt";
private final String theShoes = "Shoes";
private final String theJeans = "Jeans";

private  String price;
private  String qty;
private String name;


public Product() {
	
}

public Product(String theId, String theQty, String theName) {
	
	setProductId(theId);
	setPrice(theId);
	setQty(theQty);
	setName(theName);
	
}

public String getProductId() {
	return productId;
}

public void setProductId(String productId) {
	this.productId = productId;
}

public String getTheShoeProduct() {
	return theShoeProduct;
}

public String getTheDinnerWareProduct() {
	return theDinnerWareProduct;
}

public String getTheGolfBag() {
	return theGolfBag;
}

public String getTheDiffusor() {
	return theDiffusor;
}

public String getTheJacket() {
	return theJacket;
}

public String getTheSearchProduct() {
	return theSearchProduct;
}

public String getTheHighTShirtPrice() {
	return theHighTShirtPrice;
}

public String getTheLowTShirtPrice() {
	return theLowTShirtPrice;
}

public String getTheHighPantsPrice() {
	return theHighPantsPrice;
}

public String getTheLowPantsPrice() {
	return theLowPantsPrice;
}

public String getTheColor() {
	return theColor;
}

public String getTheSunglasses() {
	return theSunglasses;
}

public String getTheDefaultSockPrice() {
	return theDefaultSockPrice;
}

public String getTheMidTShirtPrice() {
	return theMidTShirtPrice;
}

public String getTheJeansPrice() {
	return theJeansPrice;
}

public String getTheBike() {
	return theBike;
}

public String getTheGBPTShirtPrice() {
	// TODO Auto-generated method stub
	return this.theGBPTShirtPrice;
}

public String getTheBeachProduct() {
	return theBeachProduct;
}

public String getTheOversizedProduct() {
	return theOversizedProduct;
}

public void setTheOversizedProduct(String theOversizedProduct) {
	this.theOversizedProduct = theOversizedProduct;
}

public String getTheDinnerWareName() {
	return theDinnerWareName;
}

public String getThePantsName() {
	return thePantsName;
}

public String getTheGolfName() {
	return theGolfName;
}

public String getTheDiffusorName() {
	return theDiffusorName;
}

public String getTheTraditionalGiftCardName() {
	return theTraditionalGiftCardName;
}

public String getTheShippingRestriction() {
	return theShippingRestriction;
}

public String getThePageName() {
	return thePageName;
}

public String getTheTshirt() {
	return theTshirt;
}

public String getTheShoes() {
	return theShoes;
}

public String getTheJeans() {
	return theJeans;
}

public String getPrice() {
	return price;
}

public void setPrice(String price) {
	this.price = price;
}

public String getQty() {
	return qty;
}

public void setQty(String qty) {
	this.qty = qty;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
	
	
}
