package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class WomenSwim {
	private List<String> theWomensSwimByCollectionLinks = new ArrayList<String>();
	private List<String> theWomensSwimNewArrivalsLinks = new ArrayList<String>();
	private List<String> theWomensSwimBeachAccessoriesLinks = new ArrayList<String>();
	private List<String> theWomensSwimCoverupsLinks = new ArrayList<String>();
	private List<String> theWomensSummerSwimByShopLinks = new ArrayList<String>();
	private List<String> theWomensSwimLinks = new ArrayList<String>();
	private List<String> theWomensSwimShopLinks = new ArrayList<String>();
	private List<String> theWomensSwimByCollectionViewAll = new ArrayList<String>();
	private List<String> theWomensBeachAccessoriesViewAll = new ArrayList<String>();

	
	
	private final String theByXPathViewAllAccessories = "(//a[contains(text(),'View All')])[11]";
	private final String theByXPathWomensSwim = "//div[5]/div[2]/map/area";
	private final String theByXPathSummerSwimShop = "(//a[contains(text(),'Summer Swim Shop')])[3]";
	private final String theByXPathActiveSwimShop = "(//a[contains(text(),'Active Swim Shop')])[3]";
	
	private final String theByXPathNewArrivals = "(//a[contains(text(),'New Arrivals')])[6]";
	private final String theByXPathViewAllSwimWear = "(//a[contains(text(),'View All Swimwear')])[2]";
	private final String theByXPathSwimByCollections = "(//a[contains(text(),'Swim by Collections')])[2]";
	private final String theByXPathGuestFavorites = "//li[3]/ul/li[4]/a";
	private final String theByXPathTops = "(//a[contains(text(),'Tops')])[3]";
	private final String theByXPathSandals = "(//a[contains(text(),'Sandals')])[5]";
	private final String theByXPathBeachAccessories = "(//a[contains(text(),'Accessories')])[6]";
	private final String theByXPathCoverups = "(//a[contains(text(),'Island Values')])[4]";
	private final String theByXPathUnderWire = "(//a[contains(text(),'Sweatshirts')])[4]";

	private final String theByXPathIslandTops = "//li[@id='leftnav_sub']/a";
	private final String theByXPathBottoms = "(//a[contains(text(),'Bottoms')])[2]";
	private final String theByXPathOnePieceSuits = "(//a[contains(text(),'One-Piece Suits')])[2]";
	private final String theByXPathSwimCoverups = "(//a[contains(text(),'Swim Coverups')])[2]";
	
	private final String theByXPathSummerSwimShopTops = "//li[@id='leftnav_sub']/a";
	private final String theByXPathSummerSwimShopBottoms = "(//a[contains(text(),'Bottoms')])[2]";
	private final String theByXPathSummerSwimShopOnePieces = "(//a[contains(text(),'One-Piece Suits')])[2]";
	private final String theByXPathSummerSwimShopCoverups = "//a[contains(text(),'Swim Coverups')]";

	private final String theByXPathSweaters = "(//a[contains(text(),'Sweaters')])[4]";
	private final String theByXPathSkirts = "(//a[contains(text(),'Skirts')])[2]";
	private final String theByXPathOuterWear = "(//a[contains(text(),'Outerwear')])[2]";
	private final String theByXPathJeans = "(//a[contains(text(),'Jeans')])[4]";
	private final String theByXPathShorts = "(//a[contains(text(),'Shorts')])[4]";
	private final String theByXPathSwim = "(//a[contains(text(),'Swim')])[11]";
	private final String theByXPathJewlery = "(//a[contains(text(),'Jewelry')])[2]";
	private final String theByXPathViewAllTops = "(//a[contains(text(),'View All Tops')])[2]";
	private final String theByXPathLoungeWear = "(//a[contains(text(),'Loungewear')])[4]";
	private final String theByXPathPantsAndCapris = "(//a[contains(text(),'Pants & Capris')])[2]";
	private final String theByXpathCreditCardsLink = "(//a[contains(text(),'Gift Cards')])[7]";

	private final String theByLinkUnderwire = "Underwire";
	private final String theByLinkTankini = "Tankini";
	private final String theByLinkBandeau = "Bandeau";
	private final String theByLinkHalter = "Halter";
	private final String theByLinkOverTheShoulder = "Over the Shoulder";
	private final String theByLinkDCupSize = "D, DD Cup Sizes";
	private final String theByLinkHipster = "Hipster";
	private final String theByLinkHighWaist = "High-Waist";
	private final String theByLinkSkirtedHipster = "Skirted Hipster";
	private final String theByLinkString = "String";

	private final String theByLinkBlack = "Black";
	private final String theByLinkWhite = "White";
	private final String theByLinkCoral = "Coral";
	private final String theByLinkPink = "Pink";
	private final String theByLinkBlue = "Blue";
	private final String theByLinkRed = "Red";
	private final String theByLinkBrown = "Brown";
	private final String theByLinkOrange = "Orange";
	private final String theByLinkPurple = "Purple";
	private final String theByLinkYellow = "Yellow";
	private final String theByLinkGreen = "Green";
	private final String theByLinkGrey = "Grey";
	
	private final String theByXPathDresses = "Dresses";
	private final String theByXPathRashguards = "Rashguards";
	private final String theByXPathPantsShortsAndSkirts = "Pants, Shorts & Skirts";
	private final String theByXPathTunicsShirtsAndSweaters = "Tunics, Shirts & Sweaters";

	private final String theByXPathNATops = "//li[3]/ul/li[2]/a";
	private final String theByXPathNATankinis = "//li[3]/ul/li[3]/a";
	private final String theByXPathNABottoms = "//li[3]/ul/li[4]/a";
	private final String theByXPathNAOnePieceSuits = "//li[3]/ul/li[5]/a";
	private final String theByXPathNASundressesAndCoverups = "//li[3]/ul/li[6]/a";

	private final String theByXPathNAJeans = "//li[3]/ul/li[7]/a";
	private final String theByXPathNAPantsAndShorts = "//ul/li[8]/a";
	private final String theByXPathNASwimWear = "//ul/li[10]/a";
	private final String theByXPathNAAccessories = "//ul/li[9]/a";

	private final String theByXPathInternationalFitTops = "//li[3]/ul/li[5]/a";
	private final String theByXPathInternationalFitBottoms = "//li[3]/ul/li[6]/a";
	private final String theByXPathInternationalFitDresses = "//li[3]/ul/li[7]/a";

	private final String theByLinkPearlSolids = "Pearl Solids";
	private final String theByLinkMapFloral = "Map Floral";
	private final String theByLinkBrollyBeach = "Brolly Beach";
	private final String theByLinkSugarShack = "Sugar Shack";
	private final String theByLinkTikiIsle = "Tiki Isle";
	private final String theByLinkSunsetSky = "Sunset Sky";
	private final String theByLinkWaterWaves = "Water Waves";
	private final String theByLinkHazySkipperStripe = "Skipper Stripe";
	private final String theByLinkMalibuMedallion = "Malibu Medallion";
	private final String theByLinkScenicHarbor = "Scenic Harbor";
	private final String theByLinkRugybyStripeAndDot = "Rugby Stripe & Dot";
	private final String theByLinkGiftSets = "Gift Sets";
	private final String theByLinkPaintDot = "Paint Dot";
	private final String theByLinkDeckPiping = "Deck Piping";
	private final String theByLinkBaiaHotDotAndStripe = "Baia Hot Dot & Stripe";
	private final String theByLinkTajPaisley = "Taj Paisley";

	private final String theByLinkSunglasses = "Sunglasses";
	private final String theByLinkBeachTowelsAndBags = "Beach Towels & Bags";
	private final String theByLinkSunHats = "Sun Hats";
	private final String theByLinkScarves = "Scarves";
	
	private final String thePageName = "WomensSwim";

	public void CreateWomensSwimNewArrivalsLinksList() {
		theWomensSwimNewArrivalsLinks.add(this.getTheByXPathNATops());
		theWomensSwimNewArrivalsLinks.add(this.getTheByXPathNATankinis());
		theWomensSwimNewArrivalsLinks.add(this.getTheByXPathNABottoms());
		theWomensSwimNewArrivalsLinks.add(this.getTheByXPathNAOnePieceSuits());
		theWomensSwimNewArrivalsLinks.add(this
				.getTheByXPathNASundressesAndCoverups());
	}

	public void CreateWomenSwimCoverupsLinksList() {
		theWomensSwimCoverupsLinks.add(this.getTheByXPathDresses());
		theWomensSwimCoverupsLinks.add(this.getTheByXPathRashguards());
		theWomensSwimCoverupsLinks.add(this.getTheByXPathTunicsShirtsAndSweaters());
		theWomensSwimCoverupsLinks.add(this.getTheByXPathPantsShortsAndSkirts());
	}
	
	public void CreateWomenSummerSwimByShopLinksList() {
		theWomensSwimCoverupsLinks.add(this.getTheByXPathSummerSwimShopOnePieces());
		theWomensSwimCoverupsLinks.add(this.getTheByXPathSummerSwimShopBottoms());
		theWomensSwimCoverupsLinks.add(this.getTheByXPathSummerSwimShopOnePieces());
		theWomensSwimCoverupsLinks.add(this.getTheByXPathSummerSwimShopCoverups());
	}

	public void CreateWomensSwimViewAllAccessoriesLinksList() {
		theWomensBeachAccessoriesViewAll.add(this
				.getTheByXPathViewAllAccessories());
	}

	public void CreateWomensSwimByCollectionViewAllLinksList() {
		theWomensSwimByCollectionViewAll.addAll(this
				.getTheWomensSwimByCollectionViewAll());
	}

	public void CreateWomensSwimLinksList() {
	
		theWomensSwimLinks.add(this.getTheByXPathNewArrivals());
		theWomensSwimLinks.add(this.getTheByXPathViewAllSwimWear());
		theWomensSwimLinks.add(this.getTheByXPathSummerSwimShop());
		theWomensSwimLinks.add(this.getTheByXPathTops());
		theWomensSwimLinks.add(this.getTheByXPathBottoms());
		theWomensSwimLinks.add(this.getTheByXPathOnePieceSuits());
		theWomensSwimLinks.add(this.getTheByXPathSwimCoverups());
		theWomensSwimLinks.add(this.getTheByXPathSandals());
		theWomensSwimLinks.add(this.getTheByXPathActiveSwimShop());
		
		
	}

	public void CreateWomensShopLinksList() {
		theWomensSwimShopLinks.add(this.getTheByLinkUnderwire());
		theWomensSwimShopLinks.add(this.getTheByLinkTankini());
		theWomensSwimShopLinks.add(this.getTheByLinkBandeau());
		theWomensSwimShopLinks.add(this.getTheByLinkHalter());
		theWomensSwimShopLinks.add(this.getTheByLinkOverTheShoulder());
		theWomensSwimShopLinks.add(this.getTheByLinkDCupSize());
		theWomensSwimShopLinks.add(this.getTheByLinkHipster());
		theWomensSwimShopLinks.add(this.getTheByLinkHighWaist());
		theWomensSwimShopLinks.add(this.getTheByLinkSkirtedHipster());
		theWomensSwimShopLinks.add(this.getTheByLinkString());
		theWomensSwimShopLinks.add(this.getTheByLinkBlack());
		theWomensSwimShopLinks.add(this.getTheByLinkGrey());
		theWomensSwimShopLinks.add(this.getTheByLinkOrange());
		theWomensSwimShopLinks.add(this.getTheByLinkYellow());
		theWomensSwimShopLinks.add(this.getTheByLinkGreen());
		theWomensSwimShopLinks.add(this.getTheByLinkPurple());
		theWomensSwimShopLinks.add(this.getTheByLinkBlue());
		theWomensSwimShopLinks.add(this.getTheByLinkPink());
		theWomensSwimShopLinks.add(this.getTheByLinkWhite());
		theWomensSwimShopLinks.add(this.getTheByLinkRed());
		theWomensSwimShopLinks.add(this.getTheByLinkBrown());
	}

	public void CreateWomensBeachAccessoriesLinksList() {
		theWomensSwimBeachAccessoriesLinks.add(this.getTheByLinkSunglasses());
		theWomensSwimBeachAccessoriesLinks.add(this
				.getTheByLinkBeachTowelsAndBags());
		theWomensSwimBeachAccessoriesLinks.add(this.getTheByLinkSunHats());
		theWomensSwimBeachAccessoriesLinks.add(this.getTheByLinkScarves());
	}

	public void CreateWomensSwimByCollectionLinksList() {
		theWomensSwimByCollectionLinks.add(this.getTheByLinkPearlSolids());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkMapFloral());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkRugybyStripeAndDot());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkPaintDot());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkBrollyBeach());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkSugarShack());		
		theWomensSwimByCollectionLinks.add(this.getTheByLinkTikiIsle());		
		theWomensSwimByCollectionLinks.add(this.getTheByLinkSunsetSky());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkWaterWaves());		
		theWomensSwimByCollectionLinks.add(this.getTheByLinkHazySkipperStripe());		
		theWomensSwimByCollectionLinks.add(this.getTheByLinkMalibuMedallion());		
		//theWomensSwimByCollectionLinks.add(this.getTheByLinkScenicHarbor());
	//	theWomensSwimByCollectionLinks.add(this.getTheByLinkBaiaHotDotAndStripe());		
	//	theWomensSwimByCollectionLinks.add(this.getTheByLinkWatercolorFloral());		
	//	theWomensSwimByCollectionLinks.add(this.getTheByLinkSailboat());
		theWomensSwimByCollectionLinks.add(this.getTheByLinkDeckPiping());
		
	

	}

	public List<String> getTheWomensSwimByCollectionLinks() {
		return theWomensSwimByCollectionLinks;
	}

	public void setTheWomensSwimByCollectionLinks(
			List<String> theWomensSwimByCollectionLinks) {
		this.theWomensSwimByCollectionLinks = theWomensSwimByCollectionLinks;
	}

	public List<String> getTheWomensSwimNewArrivalsLinks() {
		return theWomensSwimNewArrivalsLinks;
	}

	public void setTheWomensSwimNewArrivalsLinks(
			List<String> theWomensSwimNewArrivalsLinks) {
		this.theWomensSwimNewArrivalsLinks = theWomensSwimNewArrivalsLinks;
	}

	public List<String> getTheWomensSwimBeachAccessoriesLinks() {
		return theWomensSwimBeachAccessoriesLinks;
	}

	public void setTheWomensSwimBeachAccessoriesLinks(
			List<String> theWomensSwimBeachAccessoriesLinks) {
		this.theWomensSwimBeachAccessoriesLinks = theWomensSwimBeachAccessoriesLinks;
	}

	public List<String> getTheWomensSwimLinks() {
		return theWomensSwimLinks;
	}

	public void setTheWomensSwimLinks(List<String> theWomensSwimLinks) {
		this.theWomensSwimLinks = theWomensSwimLinks;
	}

	public List<String> getTheWomensSwimShopLinks() {
		return theWomensSwimShopLinks;
	}

	public void setTheWomensSwimShopLinks(List<String> theWomensSwimShopLinks) {
		this.theWomensSwimShopLinks = theWomensSwimShopLinks;
	}

	public List<String> getTheWomensSwimByCollectionViewAll() {
		return theWomensSwimByCollectionViewAll;
	}

	public void setTheWomensSwimByCollectionViewAll(
			List<String> theWomensSwimByCollectionViewAll) {
		this.theWomensSwimByCollectionViewAll = theWomensSwimByCollectionViewAll;
	}

	public List<String> getTheWomensBeachAccessoriesViewAll() {
		return theWomensBeachAccessoriesViewAll;
	}

	public void setTheWomensBeachAccessoriesViewAll(
			List<String> theWomensBeachAccessoriesViewAll) {
		this.theWomensBeachAccessoriesViewAll = theWomensBeachAccessoriesViewAll;
	}
	
	public List<String> getTheWomensSwimCoverupsLinks() {
		return theWomensSwimCoverupsLinks;
	}

	public void setTheWomensSwimCoverupsLinks(
			List<String> theWomensSwimCoverupsLinks) {
		this.theWomensSwimCoverupsLinks = theWomensSwimCoverupsLinks;
	}

	public String getTheByXPathViewAllAccessories() {
		return theByXPathViewAllAccessories;
	}

	public String getTheByXPathWomensSwim() {
		return theByXPathWomensSwim;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathViewAllSwimWear() {
		return theByXPathViewAllSwimWear;
	}

	public String getTheByXPathSwimByCollections() {
		return theByXPathSwimByCollections;
	}

	public String getTheByXPathGuestFavorites() {
		return theByXPathGuestFavorites;
	}

	public String getTheByXPathTops() {
		return theByXPathTops;
	}

	public String getTheByXPathSandals() {
		return theByXPathSandals;
	}

	public String getTheByXPathBeachAccessories() {
		return theByXPathBeachAccessories;
	}

	public String getTheByXPathCoverups() {
		return theByXPathCoverups;
	}

	public String getTheByXPathUnderWire() {
		return theByXPathUnderWire;
	}

	public String getTheByXPathIslandTops() {
		return theByXPathIslandTops;
	}

	public String getTheByXPathBottoms() {
		return theByXPathBottoms;
	}

	public String getTheByXPathOnePieceSuits() {
		return theByXPathOnePieceSuits;
	}

	public String getTheByXPathSwimCoverups() {
		return theByXPathSwimCoverups;
	}

	public String getTheByXPathSweaters() {
		return theByXPathSweaters;
	}

	public String getTheByXPathSkirts() {
		return theByXPathSkirts;
	}

	public String getTheByXPathOuterWear() {
		return theByXPathOuterWear;
	}

	public String getTheByXPathJeans() {
		return theByXPathJeans;
	}

	public String getTheByXPathShorts() {
		return theByXPathShorts;
	}

	public String getTheByXPathSwim() {
		return theByXPathSwim;
	}

	public String getTheByXPathJewlery() {
		return theByXPathJewlery;
	}

	public String getTheByXPathViewAllTops() {
		return theByXPathViewAllTops;
	}

	public String getTheByXPathLoungeWear() {
		return theByXPathLoungeWear;
	}

	public String getTheByXPathPantsAndCapris() {
		return theByXPathPantsAndCapris;
	}

	public String getTheByXpathCreditCardsLink() {
		return theByXpathCreditCardsLink;
	}

	public String getTheByLinkUnderwire() {
		return theByLinkUnderwire;
	}

	public String getTheByLinkTankini() {
		return theByLinkTankini;
	}

	public String getTheByLinkBandeau() {
		return theByLinkBandeau;
	}

	public String getTheByLinkHalter() {
		return theByLinkHalter;
	}

	public String getTheByLinkOverTheShoulder() {
		return theByLinkOverTheShoulder;
	}

	public String getTheByLinkDCupSize() {
		return theByLinkDCupSize;
	}

	public String getTheByLinkHipster() {
		return theByLinkHipster;
	}

	public String getTheByLinkHighWaist() {
		return theByLinkHighWaist;
	}

	public String getTheByLinkSkirtedHipster() {
		return theByLinkSkirtedHipster;
	}

	public String getTheByLinkString() {
		return theByLinkString;
	}

	public String getTheByLinkBlack() {
		return theByLinkBlack;
	}

	public String getTheByLinkWhite() {
		return theByLinkWhite;
	}

	public String getTheByLinkCoral() {
		return theByLinkCoral;
	}

	public String getTheByLinkPink() {
		return theByLinkPink;
	}

	public String getTheByLinkBlue() {
		return theByLinkBlue;
	}

	public String getTheByLinkRed() {
		return theByLinkRed;
	}

	public String getTheByLinkBrown() {
		return theByLinkBrown;
	}

	public String getTheByXPathNATops() {
		return theByXPathNATops;
	}

	public String getTheByXPathNATankinis() {
		return theByXPathNATankinis;
	}

	public String getTheByXPathNABottoms() {
		return theByXPathNABottoms;
	}

	public String getTheByXPathNAOnePieceSuits() {
		return theByXPathNAOnePieceSuits;
	}

	public String getTheByXPathNASundressesAndCoverups() {
		return theByXPathNASundressesAndCoverups;
	}

	public String getTheByXPathNAJeans() {
		return theByXPathNAJeans;
	}

	public String getTheByXPathNAPantsAndShorts() {
		return theByXPathNAPantsAndShorts;
	}

	public String getTheByXPathNASwimWear() {
		return theByXPathNASwimWear;
	}

	public String getTheByXPathNAAccessories() {
		return theByXPathNAAccessories;
	}

	public String getTheByXPathInternationalFitTops() {
		return theByXPathInternationalFitTops;
	}

	public String getTheByXPathInternationalFitBottoms() {
		return theByXPathInternationalFitBottoms;
	}

	public String getTheByXPathInternationalFitDresses() {
		return theByXPathInternationalFitDresses;
	}

	public String getTheByLinkPearlSolids() {
		return theByLinkPearlSolids;
	}

	public String getTheByLinkMapFloral() {
		return theByLinkMapFloral;
	}

	public String getTheByLinkBrollyBeach() {
		return theByLinkBrollyBeach;
	}

	public String getTheByLinkSugarShack() {
		return theByLinkSugarShack;
	}

	public String getTheByLinkTikiIsle() {
		return theByLinkTikiIsle;
	}

	public String getTheByLinkSunsetSky() {
		return theByLinkSunsetSky;
	}

	public String getTheByLinkWaterWaves() {
		return theByLinkWaterWaves;
	}

	public String getTheByLinkHazySkipperStripe() {
		return theByLinkHazySkipperStripe;
	}

	public String getTheByLinkMalibuMedallion() {
		return theByLinkMalibuMedallion;
	}

	public String getTheByLinkScenicHarbor() {
		return theByLinkScenicHarbor;
	}

	public String getTheByLinkGiftSets() {
		return theByLinkGiftSets;
	}

	public String getTheByLinkDeckPiping() {
		return theByLinkDeckPiping;
	}

	public String getTheByLinkBaiaHotDotAndStripe() {
		return theByLinkBaiaHotDotAndStripe;
	}

	public String getTheByLinkTajPaisley() {
		return theByLinkTajPaisley;
	}

	public String getTheByLinkSunglasses() {
		return theByLinkSunglasses;
	}

	public String getTheByLinkBeachTowelsAndBags() {
		return theByLinkBeachTowelsAndBags;
	}

	public String getTheByLinkSunHats() {
		return theByLinkSunHats;
	}

	public String getTheByLinkScarves() {
		return theByLinkScarves;
	}

	public String getThePageName() {
		return thePageName;
	}

	public List<String> getTheWomensSummerSwimByShopLinks() {
		return theWomensSummerSwimByShopLinks;
	}

	public void setTheWomensSummerSwimByShopLinks(
			List<String> theWomensSummerSwimByShopLinks) {
		this.theWomensSummerSwimByShopLinks = theWomensSummerSwimByShopLinks;
	}

	public String getTheByXPathSummerSwimShopTops() {
		return theByXPathSummerSwimShopTops;
	}

	public String getTheByXPathSummerSwimShopBottoms() {
		return theByXPathSummerSwimShopBottoms;
	}

	public String getTheByXPathSummerSwimShopOnePieces() {
		return theByXPathSummerSwimShopOnePieces;
	}

	public String getTheByXPathSummerSwimShopCoverups() {
		return theByXPathSummerSwimShopCoverups;
	}

	public String getTheByXPathSummerSwimShop() {
		return theByXPathSummerSwimShop;
	}

	public String getTheByLinkRugybyStripeAndDot() {
		return theByLinkRugybyStripeAndDot;
	}

	public String getTheByLinkPaintDot() {
		return theByLinkPaintDot;
	}

	public String getTheByXPathDresses() {
		return theByXPathDresses;
	}

	public String getTheByXPathRashguards() {
		return theByXPathRashguards;
	}

	public String getTheByXPathPantsShortsAndSkirts() {
		return theByXPathPantsShortsAndSkirts;
	}

	public String getTheByXPathTunicsShirtsAndSweaters() {
		return theByXPathTunicsShirtsAndSweaters;
	}

	public String getTheByXPathActiveSwimShop() {
		return theByXPathActiveSwimShop;
	}

	public String getTheByLinkOrange() {
		return theByLinkOrange;
	}

	public String getTheByLinkPurple() {
		return theByLinkPurple;
	}

	public String getTheByLinkYellow() {
		return theByLinkYellow;
	}

	public String getTheByLinkGreen() {
		return theByLinkGreen;
	}

	public String getTheByLinkGrey() {
		return theByLinkGrey;
	}

}
