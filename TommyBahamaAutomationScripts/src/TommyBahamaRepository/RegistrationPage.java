package TommyBahamaRepository;

public class RegistrationPage {
	private final String theSubmitRegistrationButton = "//input[@name='continue']";
	private final String firstName = "FirstName";
	private final String lastName = "LastName";
	private final String theEmail = "//form[@id='new-account-form']/fieldset/input[3]";
	private final String theInputPswrd = "//input[@id='create-password']";
	private final String thePswrd = "testing123";
	private final String theConfirmPswrd = "//input[@id='confirm-password']";
	private final String theConfirmEmail = "//input[@id='confirm-email-address']";
	private final String companyName = "CompanyName";
	private final String phoneNumber = "BusinessPhoneNumber";
	private final String closeReg = "success-close";
	private final String theReturnToShoppingBag = "//a[@id='return-to-cart-link']";
	private final String theByLinkGuestContinue = "CONTINUE AS A GUEST";
	private final String theUrlType = "https://";

	public String getTheSubmitRegistrationButton() {
		return theSubmitRegistrationButton;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getTheEmail() {
		return theEmail;
	}

	public String getTheInputPswrd() {
		return theInputPswrd;
	}

	public String getThePswrd() {
		return thePswrd;
	}

	public String getTheConfirmPswrd() {
		return theConfirmPswrd;
	}

	public String getTheConfirmEmail() {
		return theConfirmEmail;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getCloseReg() {
		return closeReg;
	}

	public String getTheReturnToShoppingBag() {
		return theReturnToShoppingBag;
	}

	public String getTheByLinkGuestContinue() {
		return theByLinkGuestContinue;
	}

	public String getTheUrlType() {
		return theUrlType;
	}
}
