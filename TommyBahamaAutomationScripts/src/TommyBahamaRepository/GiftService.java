package TommyBahamaRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class GiftService {
	private final String theByXPathRemoveGiftServices = "//a[contains(text(),'Remove Gift Option')]";
	private final String theEmailToInputBox = "//input[@id='email-to']";
	private final String theEmailToInput = "email-to";
	private final String theEmailFromInputBox = "//input[@id='email-from']";
	private final String theEmailFromInput = "email-from";
	private final String theMessageInputBox = "//textarea[@id='message']";
	private final String theMessageInput = "message";
	private final String theSaveButton = "//input[@id='save']";
	private final String theByIdSaveButton = "save";
	private final String theByNameSaveButton = "savegift";
	private final String theEditSaveButton = "//input[@id='edit_gift_note_box']";
	private final String theGiftBoxWithMessage = "//input[@id='gift-box-with-message']";
	private final String theBoxButton = "//input[@id='boxSelectedBtn']";
	private final String theGiftMessage = "//input[@id='gift-message-only']";
	private String theToMessage = "Jack C. West";
	private String theFromMessage = "Tommy Bahama";
	private String theMessage = "For all the hard work you have done for Tommy Bahama!";
	private final String thePageName = "GiftServices";

    private final String theByIdMessageInput = "message";
    private final String theGiftServicesModal = "GiftServiceModal";
    private final String theGiftBox = "GiftBox";
    private final String theGiftWrap = "GiftWrap";
    private final String theAddToGiftBox1 = "AddToGiftBox1";
    private final String theGiftMessageText = "GiftMessage";
    private final String theGiftServiceName = "GiftService";
    private final String theGiftServiceShippingError = "GiftServiceShippingErrors";
    private final String theGiftServiceError = "GiftServiceErrors"; 
	
	
	
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	private ShoppingBagPage myShoppingBag;
	
	public GiftService(WebDriver theDriver,  SeleniumEnvironment theEnvironment,
			ShoppingBagPage theShoppingBag)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myShoppingBag = theShoppingBag;
		
	}
	
	public String getTheByNameSaveButton() {
		return theByNameSaveButton;
	}

	public String getTheEmailToInput() {
		return theEmailToInput;
	}

	public String getTheEmailFromInput() {
		return theEmailFromInput;
	}

	public String getTheMessageInputBox() {
		return theMessageInputBox;
	}

	public String getTheByIdSaveButton() {
		return theByIdSaveButton;
	}

	public String getTheEmailToInputBox() {
		return theEmailToInputBox;
	}

	public String getTheEmailFromInputBox() {
		return theEmailFromInputBox;
	}

	public String getTheMessageInput() {
		return theMessageInput;
	}

	public String getTheSaveButton() {
		return theSaveButton;
	}

	public String getTheEditSaveButton() {
		return theEditSaveButton;
	}

	public String getTheBoxButton() {
		return theBoxButton;
	}

	public String getTheGiftMessage() {
		return theGiftMessage;
	}

	public String getTheToMessage() {
		return theToMessage;
	}

	public void setTheToMessage(String theToMessage) {
		this.theToMessage = theToMessage;
	}

	public String getTheFromMessage() {
		return theFromMessage;
	}

	public void setTheFromMessage(String theFromMessage) {
		this.theFromMessage = theFromMessage;
	}

	public String getTheMessage() {
		return theMessage;
	}

	public void setTheMessage(String theMessage) {
		this.theMessage = theMessage;
	}

	public String getTheByXPathRemoveGiftServices() {
		return theByXPathRemoveGiftServices;
	}

	public String getTheGiftBoxWithMessage() {
		return theGiftBoxWithMessage;
	}

	// / <summary>
	// / This adds information for a gift box, gift message or gift Note to test
	// Gift Services.
	// / I need to tweak this method so it will work for all Gift Services.
	// Currently it does but only because
	// / of the order and situation when each type is called.
	// / </summary>
	// / <param name="driver"></param>

	// / <param name="theGiftBoxLink"></param>
	// / <param name="theTest"></param>
	public void giftBox(  String theGiftBoxLink,
			String theTest, String theBrowser) throws InterruptedException {
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theGiftBoxLink));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// myEnvironment.waitForPageLoaded(driver);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheEmailToInput()));
		currentElement.sendKeys(this.getTheToMessage());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheEmailFromInput()));
		currentElement.sendKeys(this.getTheFromMessage());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheMessageInput()));

		currentElement.sendKeys(this.getTheMessage());
		if (myEnvironment.isElementPresent(
				By.xpath(this.getTheGiftBoxWithMessage()))) {
			if (driver.findElement(By.cssSelector("label")).getText()
					.contains("WRAP"))
				myEnvironment.TakeScreenShot( theBrowser, theTest,
						"GiftServicesModal", "GiftWrap");
			else
				myEnvironment.TakeScreenShot( theBrowser, theTest,
						"GiftServicesModal", "GiftBox");
		} else
			myEnvironment.TakeScreenShot( theBrowser, theTest,
					"GiftServicesModal", "GiftMessage");
		//Thread.sleep(myEnvironment.getThe_Default_Sleep());
		if (theBrowser.contains("IE9") || theBrowser.contains("Chrome")) {
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			currentElement = myEnvironment.waitForDynamicElement(
					By.name(this.getTheByNameSaveButton()));
			currentElement.click();
		} else {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheSaveButton()));
			currentElement.click();
		}
	}

	// / <summary>
	// / This adds information for a gift box, gift message or gift Note to test
	// Gift Services.
	// / I need to tweak this method so it will work for all Gift Services.
	// Currently it does but only because
	// / of the order and situation when each type is called.
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="theGiftBoxLink"></param>
	// / <param name="theTest"></param>
	public void addToGiftBox(String theGiftBoxLink,
			String theTest, String theBrowser) throws InterruptedException {
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theGiftBoxLink));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id("existing-box"));
		Select clickThis = new Select(currentElement);
		// Thread.Sleep(myEnvironment.DefaultSleep);
		clickThis.selectByValue("Box 1");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( theBrowser, theTest,
				"GiftServicesModal", "AddToGiftBox1");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		if (theBrowser.contains("IE9")) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.id(this.getTheEmailToInput()));
			currentElement.sendKeys(this.getTheToMessage());
			currentElement = myEnvironment.waitForDynamicElement(
					By.id(this.getTheEmailFromInput()));
			currentElement.sendKeys(this.getTheFromMessage());
			currentElement = myEnvironment.waitForDynamicElement(
					By.id(this.getTheMessageInput()));			
			currentElement.sendKeys(this.getTheMessage());
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myEnvironment.TakeScreenShot( theBrowser, theTest,
					"GiftServicesModal", "IEAddToGiftBox1");
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			currentElement = myEnvironment.waitForDynamicElement(
					By.name(this.getTheByNameSaveButton()));
			currentElement.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		} else {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheSaveButton()));
			currentElement.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		}
	}

	// / <summary>
	// / This adds information for a gift box, gift message or gift Note to test
	// Gift Services.
	// / I need to tweak this method so it will work for all Gift Services.
	// Currently it does but only because
	// / of the order and situation when each type is called.
	// / </summary>
	// / <param name="theGiftBoxLink"></param>
	// / <param name="theTest"></param>
	public void giftNote( String theGiftBoxLink,
			String theTest, String theBrowser) {
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theGiftBoxLink));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.theGiftMessage));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theEmailToInputBox));
		currentElement.sendKeys(theToMessage);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theEmailFromInputBox));
		currentElement.sendKeys(theFromMessage);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theMessageInputBox));
		currentElement.sendKeys(theMessage);
		myEnvironment.TakeScreenShot( theBrowser, theTest,
				"GiftServicesModal", "GiftNote");
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theSaveButton));
		currentElement.click();
	}

	// / <summary>
	// / This adds information for a gift box, gift message or gift Note to test
	// Gift Services.
	// / I need to tweak this method so it will work for all Gift Services.
	// Currently it does but only because
	// / of the order and situation when each type is called.
	// / </summary>
	// / <param name="theGiftBoxLink"></param>
	// / <param name="theTest"></param>
	public void giftBoxForIE( String theGiftBoxLink,
			String theTest, String theBrowser) throws InterruptedException {
		WebElement currentElement;
		// JavascriptExecutor js = (JavascriptExecutor) driver;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theGiftBoxLink));
		// js.executeScript("document.getElementByXPath(" + theGiftBoxLink +
		// ")");
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// myEnvironment.waitForPageLoaded(driver);
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.id(this.getTheEmailToInput()));
		// currentElement.sendKeys(this.getTheToMessage());
		driver.findElement(By.id(this.getTheEmailToInput())).sendKeys(
				this.getTheToMessage());
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.id(this.getTheEmailFromInput()));
		// currentElement.sendKeys(this.getTheFromMessage());
		driver.findElement(By.id(this.getTheEmailFromInput())).sendKeys(
				this.getTheFromMessage());

		// currentElement = myEnvironment.waitForDynamicElement(
		// By.id(this.getTheMessageInput()));

		// currentElement.sendKeys(this.getTheMessage());
		driver.findElement(By.id(this.getTheMessageInput())).sendKeys(
				this.getTheMessage());

		if (myEnvironment.isElementPresent(
				By.xpath(this.getTheGiftBoxWithMessage()))) {
			if (driver.findElement(By.cssSelector("label")).getText()
					.contains("WRAP"))
				myEnvironment.TakeScreenShot( theBrowser, theTest,
						"GiftServicesModal", "GiftWrap");
			else
				myEnvironment.TakeScreenShot( theBrowser, theTest,
						"GiftServicesModal", "GiftBox");
		} else
			myEnvironment.TakeScreenShot( theBrowser, theTest,
					"GiftServicesModal", "GiftMessage");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		if (theBrowser.contains("IE9")) {
		

			driver.findElement(By.name(this.getTheByNameSaveButton())).click();

		} else {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheSaveButton()));
			currentElement.click();
		}
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByIdMessageInput() {
		return theByIdMessageInput;
	}

	public String getTheGiftServicesModal() {
		return theGiftServicesModal;
	}

	public String getTheGiftBox() {
		return theGiftBox;
	}

	public String getTheGiftWrap() {
		return theGiftWrap;
	}

	public String getTheAddToGiftBox1() {
		return theAddToGiftBox1;
	}

	public String getTheGiftMessageText() {
		return theGiftMessageText;
	}

	public String getTheGiftServiceName() {
		return theGiftServiceName;
	}

	public String getTheGiftServiceShippingError() {
		return theGiftServiceShippingError;
	}

	public String getTheGiftServiceError() {
		return theGiftServiceError;
	}

}
