package TommyBahamaRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PayTech {
	private final String theUsernameInput = "username";
	private final String theCityQuestion = "ans0";
	private final String theRememberComputerCheckBox = "dp_bind";
	private final String theCityQuestionSubmitBtn = "//input[@value='Submit']";
	private final String theAuthBtn = "button";
	private final String thePaswrdInput = "password";
	private final String theUsername = "tommy0b1";
	private final String thePaswrd = "blueM2014Summer";
	private final String theGiftCardTab = "vt_nav_fle";
	private final String theBatchTab = "vt_nav_opebat";
	private final String theLast4 = "name=searchBean.lastFourAccountNumber";
	private final String theByNameOrderNumber = "searchBean.orderNumber";
	private final String theSearchBtn = "subaction";

	private final String theFirstResult = "sel-ra";
	private final String theAuthSuccess = "//form/table/tbody/tr/td/table/tbody/tr/td[2]";

	private final String theZipCode = "//tr[9]/td[2]";
	private final String theFour = "1111";
	private final String theAuthResult = "Auth Success";
	private final String theZipResult = "98404-4722";
	private final String theAmount = "//tr[4]/td[2]";
	private final String theAmountResult = "1.00";
	private final String theAddValueSelection = "(//input[@name='transaction'])[2]";
	private final String theGCNumberInput = "transactionBean.flexCacheRequest.ccAccountNum";
	private final String theGCPinInput = "transactionBean.flexCacheRequest.ccCardVerifyNum";
	private final String theAddedAmountInput = "transactionBean.amount";
	private final String theOrderId = "transactionBean.flexCacheRequest.orderID";
	private final String theGCSubmitBtn = "//input[@value='Add Value']";
	private final String theApprovedMessage = "td.positive";
	private final String theGCAddedAmount = "$25.00";
	
	private final String thePageName = "PaymenTech";
	
	private Random RandomNumber = new Random();
	
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	private String orderNumber;
	private String testName;
	private String browser;
	
	
	public PayTech(WebDriver theDriver,
			SeleniumEnvironment theEnvironment, 
			String theOrderNumber, String theTestName, String theBrowser)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;		
		orderNumber = theOrderNumber;
		testName = theTestName;
		browser = theBrowser;
	}

	public PayTech() {
		// TODO Auto-generated constructor stub
	}

	public String getTheUsernameInput() {
		return theUsernameInput;
	}

	public String getTheCityQuestion() {
		return theCityQuestion;
	}

	public String getTheRememberComputerCheckBox() {
		return theRememberComputerCheckBox;
	}

	public String getTheCityQuestionSubmitBtn() {
		return theCityQuestionSubmitBtn;
	}

	public String getTheAuthBtn() {
		return theAuthBtn;
	}

	public String getThePaswrdInput() {
		return thePaswrdInput;
	}

	public String getTheUsername() {
		return theUsername;
	}

	public String getThePaswrd() {
		return thePaswrd;
	}

	public String getTheGiftCardTab() {
		return theGiftCardTab;
	}

	public String getTheBatchTab() {
		return theBatchTab;
	}

	public String getTheLast4() {
		return theLast4;
	}

	public String getTheByNameOrderNumber() {
		return theByNameOrderNumber;
	}

	public String getTheSearchBtn() {
		return theSearchBtn;
	}

	public String getTheFirstResult() {
		return theFirstResult;
	}

	public String getTheAuthSuccess() {
		return theAuthSuccess;
	}

	public String getTheZipCode() {
		return theZipCode;
	}

	public String getTheFour() {
		return theFour;
	}

	public String getTheAuthResult() {
		return theAuthResult;
	}

	public String getTheZipResult() {
		return theZipResult;
	}

	public String getTheAmount() {
		return theAmount;
	}

	public String getTheAmountResult() {
		return theAmountResult;
	}

	public String getTheAddValueSelection() {
		return theAddValueSelection;
	}

	public String getTheGCNumberInput() {
		return theGCNumberInput;
	}

	public String getTheGCPinInput() {
		return theGCPinInput;
	}

	public String getTheAddedAmountInput() {
		return theAddedAmountInput;
	}

	public String getTheOrderId() {
		return theOrderId;
	}

	public String getTheGCSubmitBtn() {
		return theGCSubmitBtn;
	}

	public String getTheApprovedMessage() {
		return theApprovedMessage;
	}

	public String getTheGCAddedAmount() {
		return theGCAddedAmount;
	}

	public Random getRandomNumber() {
		return RandomNumber;
	}

	public void setRandomNumber(Random randomNumber) {
		RandomNumber = randomNumber;
	}

	/*
	 * Method to check that an order is present in OG that has the correct
	 * total, zip and order number. This method is passed the Webdriver, the
	 * enviroment object and the total, zip and order number to be compared to.
	 */
	public void checkOrderInPayTech( String theZip, String theTotal) { // navigate
																		// to
																		// paymentech
		WebElement currentElement;
		driver.get("https://securevar.paymentech.com/signin/pages/login.faces?aa_param=user&CT_ORIG_URL=https%3A%2F%2Fsecurevar.paymentech.com%3A443%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&ct_orig_uri=%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&aa_image_phrase=true");
		//https://securevar.paymentech.com/signin/pages/login.faces?aa_param=user&CT_ORIG_URL=https%3A%2F%2Fsecurevar.paymentech.com%3A443%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&ct_orig_uri=%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&aa_image_phrase=true
		// sign in
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheUsernameInput()));

		driver.findElement(By.id(this.getTheUsernameInput())).sendKeys(
				theUsername);
	/*	driver.findElement(By.id(this.getTheAuthBtn())).click();
		// Thread.Sleep(myEnvironment.DefaultSleep);
		if (myEnvironment.IsElementPresent(By.id(this.getTheCityQuestion()))) {
			System.out.println("The security questions have been deployed.");
			driver.findElement(By.id(this.getTheCityQuestion())).sendKeys(
					"Seattle");
			driver.findElement(By.id(this.getTheRememberComputerCheckBox()))
					.click();
			driver.findElement(By.xpath(this.getTheCityQuestionSubmitBtn()))
					.click();
		}*/
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getThePaswrdInput()));

		driver.findElement(By.id(this.getThePaswrdInput())).sendKeys(
				this.thePaswrd);
		driver.findElement(By.id(this.getTheAuthBtn())).click();
		// select the batch tab and search by the order number
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheBatchTab()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheByNameOrderNumber()));
		currentElement.sendKeys(orderNumber);

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheSearchBtn()));
		currentElement.click();
		// Select the first result in the page. (Very Low Possibility of this
		// being the wrong one if more then one person ordered at the exact
		// time)
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheFirstResult()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheSearchBtn()));
		currentElement.click();
		// Check that this was successfully authorized.

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheAuthSuccess()));
		Assert.assertEquals(currentElement.getText(), theAuthResult);
		// Check to see if there is a matching zipcode.
		String ogZip = driver.findElement(By.xpath(this.getTheZipCode()))
				.getText();
		ogZip.trim();
		System.out.println("this is the ogzip before touching it " + ogZip);
		System.out
				.println("this is the length of the og zip " + ogZip.length());
		if (ogZip.length() == 9) {
			ogZip = ogZip.substring(0, 5);
			System.out
					.println("This is the zip in OG after taking out the frist five "
							+ ogZip);
		}
		if (ogZip.length() == 8) {
			ogZip = ogZip.substring(0, 5) + ogZip.substring(6);
			System.out.println("This is the zip in OG after taking out the - "
					+ ogZip);
		}
		System.out.println("This is the ogzip after removing the - " + ogZip);
		System.out.println("the passed Zip is:       " + theZip);
		Assert.assertEquals(ogZip, theZip);
		// Make sure the total is the correct amount
		Assert.assertEquals(driver.findElement(By.xpath(this.getTheAmount()))
				.getText(), theTotal);
		myEnvironment.TakeScreenShot( browser, testName, "PaymenTech",
				orderNumber);
	}

	/*
	 * Method to check that an order is present in OG that has the correct total
	 * and order number. This method is passed the Webdriver, the enviroment
	 * object and the total and order number to be compared to.
	 */
	public void checkOrderInPayTech(String theTotal)
			throws InterruptedException { // navigate to paymentech
		WebElement currentElement;
		driver.get("https://securevar.paymentech.com/signin/pages/login.faces?aa_param=user&CT_ORIG_URL=https%3A%2F%2Fsecurevar.paymentech.com%3A443%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&ct_orig_uri=%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&aa_image_phrase=true");
		//https://securevar.paymentech.com/signin/pages/login.faces?aa_param=user&CT_ORIG_URL=https%3A%2F%2Fsecurevar.paymentech.com%3A443%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&ct_orig_uri=%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&aa_image_phrase=true
		// sign in
		Thread.sleep(5000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheUsernameInput()));
		driver.findElement(By.id(this.getTheUsernameInput())).sendKeys(
				theUsername);
	/*	driver.findElement(By.id(this.getTheAuthBtn())).click();
		// Thread.Sleep(myEnvironment.DefaultSleep);
		if (myEnvironment.IsElementPresent(By.id(this.getTheCityQuestion()))) {
			System.out.println("The security questions have been deployed.");
			driver.findElement(By.id(this.getTheCityQuestion())).sendKeys(
					"Seattle");
			driver.findElement(By.id(this.getTheRememberComputerCheckBox()))
					.click();
			driver.findElement(By.xpath(this.getTheCityQuestionSubmitBtn()))
					.click();
		}*/
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getThePaswrdInput()));
		currentElement.sendKeys(this.thePaswrd);
		driver.findElement(By.id(this.getTheAuthBtn())).click();
		// select the batch tab and search by the order number
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheBatchTab()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheByNameOrderNumber()));
		currentElement.sendKeys(orderNumber);

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheSearchBtn()));
		currentElement.click();
		// Select the first result in the page. (Very Low Possibility of this
		// being the wrong one if more then one person ordered at the exact
		// time)
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheFirstResult()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheSearchBtn()));
		currentElement.click();
		System.out.println("Selected the first item in the list of results");
		// Check that this was successfully authorized.

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheAuthSuccess()));

		System.out.println("The Authorized Success text is:      "
				+ currentElement.getText());
		Assert.assertEquals(currentElement.getText(), theAuthResult);
		// Make sure the total is the correct amount

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheAmount()));

		Assert.assertEquals(currentElement.getText(), theTotal);
		myEnvironment.TakeScreenShot( browser, testName, this.getThePageName(),
				orderNumber);
	}

	/*
	 * This method opens OG and then adds money to a card. This is done so cards
	 * used in website pages are repopulated with money so the test can be ran N
	 * amount of times This method is pased the webdriver, the enviroment object
	 * and the data needed to add money to a card... the card number, the pin
	 * number and the amount to be added.
	 */
	public void addMoneyToCard( String theCardNumber,
			String thePinNumber, String theAmount) throws InterruptedException {
		// navigate to paymentech
		WebElement currentElement;
		driver.get("https://securevar.paymentech.com/signin/pages/login.faces?aa_param=user&CT_ORIG_URL=https%3A%2F%2Fsecurevar.paymentech.com%3A443%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&ct_orig_uri=%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&aa_image_phrase=true");
		//https://securevar.paymentech.com/signin/pages/login.faces?aa_param=user&CT_ORIG_URL=https%3A%2F%2Fsecurevar.paymentech.com%3A443%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&ct_orig_uri=%2Fmanager%2FloginAction.do%3Fsubaction%3Dfirst&aa_image_phrase=true
		// sign in
		Thread.sleep(5000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheUsernameInput()));
		driver.findElement(By.id(this.getTheUsernameInput())).sendKeys(
				theUsername);
		/*driver.findElement(By.id(this.getTheAuthBtn())).click();
		// Thread.Sleep(myEnvironment.DefaultSleep);
		if (myEnvironment.IsElementPresent(By.id(this.getTheCityQuestion()))) {
			System.out.println("The security questions have been deployed.");
			driver.findElement(By.id(this.getTheCityQuestion())).sendKeys(
					"Seattle");
			driver.findElement(By.id(this.getTheRememberComputerCheckBox()))
					.click();
			driver.findElement(By.xpath(this.getTheCityQuestionSubmitBtn()))
					.click();
		}*/
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getThePaswrdInput()));
		currentElement.sendKeys(this.thePaswrd);
		driver.findElement(By.id(this.getTheAuthBtn())).click();
		// select the gift card tab
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheGiftCardTab()));
		currentElement.click();
		// click to add amount
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheAddValueSelection()));
		currentElement.click();
		// enter number, pin number, and the amount
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheGCNumberInput()));
		currentElement.sendKeys(theCardNumber);

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheGCPinInput()));
		currentElement.sendKeys(thePinNumber);

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheAddedAmountInput()));
		currentElement.sendKeys(theAmount);
		// this is to build the code number for the transaction. I need to write
		// a function for this to read a counter and date
		// to a file so the the number only changes if the date is different.
		// That way it counts up everytime the test
		// runs and then resets each day.
		String builder = "";
		DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		builder = dateformat.format(date);
		builder = builder + "00";
		Random r = new Random();
		int n = r.nextInt(50);
		builder = builder + n;
		System.out
				.println("This is the made up code for the added amount to the Giftcard:       "
						+ builder);

		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheOrderId()));
		currentElement.sendKeys(builder);
		// submit the added amount
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheGCSubmitBtn()));
		currentElement.click();
		try {
			driver.switchTo().alert().accept(); // prepares Selenium to handle
												// alert in Paytech complaining
												// about multiple runs
		} catch (NoAlertPresentException e) {
			// no alert message
		}
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// test to see that the added amount was successful.
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(this.getTheApprovedMessage()));

		Assert.assertEquals(currentElement.getText(), "APPROVED");
		myEnvironment.TakeScreenShot( browser, testName, this.getThePageName(),
				theCardNumber);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
	}

	public String formatTotal(String theTotal, Double theDTotal) 
			throws InterruptedException {
		char decimal = '.';
		if (theDTotal > 999) {
			theTotal = "" + theDTotal;
			theTotal = new StringBuffer(theTotal).insert(1, ",").toString();

		} else
			theTotal = "" + theDTotal;
		Thread.sleep(myEnvironment.getThe_Special_Sleep());

		System.out
				.println("This is the char at the end of the total string:       "
						+ theTotal.charAt(theTotal.length() - 2));
		if (theTotal.charAt(theTotal.length() - 2) == decimal)
			theTotal = theTotal + "0";
		// new StringBuffer(newTotal).insert(newTotal.length() - 1,
		// "0").toString();
		System.out.println("This is the new newtotal.      " + theTotal);

		return theTotal;
	}

	public String getThePageName() {
		return thePageName;
	}

}
