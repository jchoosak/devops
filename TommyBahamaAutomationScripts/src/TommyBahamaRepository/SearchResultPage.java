package TommyBahamaRepository;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchResultPage {
	private final String theSearchResultTitle = "//div[@id='title_search']/h1";
	private final String thePriceHighToLowLink = "Price High-Low";
	private final String thePriceLowToHighLink = "Price Low-High";
	private final String theNewestLink = "Newest";
	private final String theHighestRated = "Highest Rated";
	private final String theNextPageLink = "Next Page >";

	private final String thePageName = "SearchResultsPage";

	   private String theShowAllLink = "Show All";
	   private String theShowAllMessage = "ShowAll";
	   private String thePriceLowToHighMessage = "ByPriceLowToHigh"; 
	   private String thePriceHighToLowMessage = "ByPriceHighToLow";   
	   private String theRatingsMessage = "ByRatings";
	
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	private ProductListingPage myPLP;
	private PaymentPage myPayment;
	
	public SearchResultPage(WebDriver theDriver,
			SeleniumEnvironment theEnvironment, PaymentPage thePayment,
			ProductListingPage theProductListing)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myPLP = theProductListing;
		myPayment = thePayment;
		
	}

	public String getTheSearchResultTitle() {
		return theSearchResultTitle;
	}

	public String getThePriceHighToLowLink() {
		return thePriceHighToLowLink;
	}

	public String getThePriceLowToHighLink() {
		return thePriceLowToHighLink;
	}

	public String getTheNewestLink() {
		return theNewestLink;
	}

	public String getTheHighestRated() {
		return theHighestRated;
	}

	public String getTheNextPageLink() {
		return theNextPageLink;
	}

	public String getTheShowAllLink() {
		return theShowAllLink;
	}

	/*
	 * this method used with selenium runs a search and checks that the term was
	 * search for and results are present. Passes the header, selenium,
	 * shoppingbag, enviroment and home page objects.
	 */
	// public void testSearch(ISelenium selenium, ShoppingBag
	// myShoppingBag,
	// SeleniumEnvironment myEnvironment, Header myHome)
	// {
	// // Commented since search is not working for QA enviroments
	// if (selenium.GetLocation().Contains(myEnvironment.MerchEnviroment))
	// {
	// selenium.Type(myShoppingBag.SearchInput,
	// myShoppingBag.SearchInputValue);
	// selenium.Click(myShoppingBag.SearchBtn);
	// selenium.Click(myShoppingBag.RefineSearch);
	// selenium.WaitForPageToLoad(myEnvironment.MyDefaultTimeout);
	// myEnvironment.waitForElement(selenium, this.SearchTitle);
	// Assert.True(selenium.IsTextPresent(myShoppingBag.SearchInputValue));
	// selenium.Click(myHome.Checkout);
	// }
	// else
	// {
	// selenium.Click(myHome.Checkout);
	// }
	// }
	/*
	 * this test tests the Price High to Low functionality. It clicks the link
	 * and then compares the prices of the current and next product. This
	 * continues until all products have been compared. This method is passed
	 * the webdriver, environment, and page objects.
	 */
	public void testPriceHighToLow()
			throws InterruptedException {
		WebElement ce;
		WebElement ne;

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(this.getThePriceHighToLowLink()));
		ce.click();
		Thread.sleep(5000);
		myEnvironment.waitForPageLoaded();
		Double firstAmount = 0.0;
		Double secondAmount = 0.0;
		for (int i = 1; i < 16; i++) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i + "']/div[2]/div"));
			ne = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + (i + 1)
							+ "']/div[2]/div"));
			String thePrice = ce.getText();
			String theNextPrice = ne.getText();
			firstAmount = myPayment.pullPrice(thePrice);
			secondAmount = myPayment.pullPrice(theNextPrice);
			System.out.println("The First Price:      " + firstAmount);
			System.out.println("The Second Price:     " + secondAmount);
			Assert.assertTrue((firstAmount - secondAmount) >= 0);

			if ((i == 15)
					&& myEnvironment.isElementPresent(
							By.linkText(this.getTheNextPageLink()))) {
				ce = myEnvironment.waitForDynamicElement(
						By.linkText(this.getTheNextPageLink()));
				ce.click();
				Thread.sleep(5000);
				i = 0;
			} else if (!myEnvironment.isElementPresent(
					By.xpath("//div[@id='imgContainer_" + (i + 2)
							+ "']/div[2]/div"))) {
				System.out.println("It made it into the break if statement");
				break;
			}
		}
	}

	/*
	 * this test tests the Price Low To High functionality. It clicks the link
	 * and then compares the prices of the current and next product. This
	 * continues until all products have been compared. This method is passed
	 * the webdriver, environment, and page objects.
	 */
	public void testLowToHigh()	throws InterruptedException {
		WebElement ce; WebElement ne;		
		ce = myEnvironment.waitForDynamicElement(
				By.linkText(this.getThePriceLowToHighLink()));
		ce.click();
		Thread.sleep(5000);
		myEnvironment.waitForPageLoaded();
		Double firstAmount = 0.0;
		Double secondAmount = 0.0;
		for (int i = 1; i < 16; i++) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i + "']/div[2]/div"));
			ne = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + (i + 1)
							+ "']/div[2]/div"));
			String thePrice = ce.getText();
			String theNextPrice = ne.getText();
			firstAmount = myPayment.pullPrice(thePrice);
			secondAmount = myPayment.pullPrice(theNextPrice);
			System.out.println("The First Price:      " + firstAmount);
			System.out.println("The Second Price:     " + secondAmount);
			Assert.assertTrue((firstAmount - secondAmount) <= 0);

			if ((i == 15)
					&& myEnvironment.isElementPresent(
							By.linkText(this.getTheNextPageLink()))) {
				ce = myEnvironment.waitForDynamicElement(
						By.linkText(this.getTheNextPageLink()));
				ce.click();
				Thread.sleep(5000);
				i = 0;
			} else if (!myEnvironment.isElementPresent(
					By.xpath("//div[@id='imgContainer_" + (i + 2)
							+ "']/div[2]/div"))) {
				System.out.println("It made it into the break if statement");
				break;
			}
		}
	}

	/*
	 * this method pulls the rating out of the src of an element. Each rating is
	 * an image with the name as 5_0 for the image of a 5.0 rating. This method
	 * is passed the src url and that string is parsed and then converted to a
	 * double.
	 */
	public Double pullRating(String theString) {
		String url = driver.getCurrentUrl();
		String theStringCopy = theString;
		Double theIntegerRating = 0.0;
		Double theDecimalRating = 0.0;
		Double theTotalRating = 0.0;
		System.out.println(theString);
		if (url.contains("ecap04"))
			theString = theString.substring(43);
		else
			theString = theString.substring(57);
		System.out
				.println("This is the Integer string with the front of the url removed.				"
						+ theString);
		theString = theString.substring(0, 1);
		System.out
				.println("This is the Integer string with the back of the url removed.				"
						+ theString);
		theIntegerRating = Double.parseDouble(theString);

		if (url.contains("ecap04"))
			theStringCopy = theStringCopy.substring(45);
		else
			theStringCopy = theStringCopy.substring(59);
		System.out
				.println("This is the Decimal string with the front of the url removed.				"
						+ theStringCopy);

		System.out.println(theStringCopy);
		theStringCopy = theStringCopy.substring(0, 1);
		System.out
				.println("This is the Decimal string with the back of the url removed.				"
						+ theStringCopy);
		System.out.println(theStringCopy);
		theStringCopy = "." + theStringCopy;
		System.out.println(theStringCopy);
		theDecimalRating = Double.parseDouble(theStringCopy);
		theTotalRating = theIntegerRating + theDecimalRating;
		return theTotalRating;
	}

	/*
	 * This method tests the ratings functionality of the search result page.
	 * The method clicks the highest rated link and then traverses through the
	 * search result and parses out the ratings from the image url. NOTE: After
	 * making this method I discovered that our search actually does not support
	 * ordering ratings results that tie in value by the product with the most
	 * reviews. Method is passed the webdriver, environment and page objects
	 */
	public void testRatings()
			throws InterruptedException {
		WebElement ce;
		WebElement ne;
		ce = myEnvironment.waitForDynamicElement(
				By.linkText(this.getTheHighestRated()));
		ce.click();
		// myEnvironment.waitForPageLoaded(driver);

		// ce = myEnvironment.waitForDynamicElement(
		// By.linkText(this.getTheHighestRated()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// ce.getAttribute(("color").getText().equals("");
		Double firstAmount = 0.0;
		Double secondAmount = 0.0;
		for (int i = 1; i < 500; i++) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i
							+ "']/div[4]/div/div/img"));
			ne = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + (i + 1)
							+ "']/div[4]/div/div/img"));
			String theRatingUrl = ce.getAttribute("src");
			System.out
					.println("this is the first rating url before the rating is pulled         "
							+ theRatingUrl);
			String theNextRatingUrl = ne.getAttribute("src");
			System.out
					.println("this is the next rating url before the rating is pulled         "
							+ theNextRatingUrl);
			if (theRatingUrl.contains("spacer")
					|| theNextRatingUrl.contains("spacer")) {
				System.out.println("It made it into the break if statement");
				break;
			}
			firstAmount = this.pullRating(theRatingUrl);
			secondAmount = this.pullRating(theNextRatingUrl);
			System.out.println("The First Rating:      " + firstAmount);
			System.out.println("The Second Rating:     " + secondAmount);
			Assert.assertTrue((firstAmount - secondAmount) >= 0);
			// / Not sure about this here, this functionality obviously does not
			// work after dealing with 5.0 ratings.
			// / Not sure if this is by design or a bug? Not sure how important
			// this is either. Does sort the ratings
			// / but does not continue the order by how many reviews a product
			// has.
			// if (firstAmount == secondAmount)
			// {
			// Double theNumberOfReviews = 0.0;
			// Double theNextNumberOfReviews = 0.0;
			// String theReviewAmount =
			// driver.FindElement(By.XPath("//div[@id='imgContainer_" + i +
			// "']/div[4]/div/div")).Text;
			// Console.WriteLine(theReviewAmount);
			// theReviewAmount = theReviewAmount.Replace("(", "");
			// theReviewAmount = theReviewAmount.Replace(")", "");
			// String theNextReviewAmount =
			// driver.FindElement(By.XPath("//div[@id='imgContainer_" + (i + 1)
			// + "']/div[4]/div/div")).Text;
			// Console.WriteLine(theNextReviewAmount);
			// theNextReviewAmount = theNextReviewAmount.Replace("(", "");
			// theNextReviewAmount = theNextReviewAmount.Replace(")", "");
			// Double.TryParse(theReviewAmount, out theNumberOfReviews);
			// Double.TryParse(theNextReviewAmount, out theNextNumberOfReviews);
			// Assert.True(theNumberOfReviews - theNextNumberOfReviews >= 0);
			// Console.WriteLine("The First has this many reviews:   " +
			// theNumberOfReviews);
			// Console.WriteLine("The Next has this many reviews:   " +
			// theNextNumberOfReviews);
			// }
			if (!myEnvironment.isElementPresent(
					By.xpath("//div[@id='imgContainer_" + (i + 2)
							+ "']/div[4]/div/div"))
					|| (firstAmount == 0 && secondAmount == 0)) {
				System.out.println("It made it into the break if statement");
				break;
			}
		}
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheShowAllMessage() {
		return theShowAllMessage;
	}

	public void setTheShowAllMessage(String theShowAllMessage) {
		this.theShowAllMessage = theShowAllMessage;
	}

	public String getThePriceLowToHighMessage() {
		return thePriceLowToHighMessage;
	}

	public void setThePriceLowToHighMessage(String thePriceLowToHighMessage) {
		this.thePriceLowToHighMessage = thePriceLowToHighMessage;
	}

	public String getThePriceHighToLowMessage() {
		return thePriceHighToLowMessage;
	}

	public void setThePriceHighToLowMessage(String thePriceHighToLowMessage) {
		this.thePriceHighToLowMessage = thePriceHighToLowMessage;
	}

	public String getTheRatingsMessage() {
		return theRatingsMessage;
	}

	public void setTheRatingsMessage(String theRatingsMessage) {
		this.theRatingsMessage = theRatingsMessage;
	}
}
