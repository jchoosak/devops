package TommyBahamaRepository;




public class OrderTab {
	
	private final String theByXpathCreateNewTicketButton = "//div[5]/div/div/div[2]/div/span/table/tbody/tr[2]/td[2]";
	private final String theByXpathRefundButton = "//span[3]/table/tbody/tr[2]/td[2]";
	private final String theByXpathWaiveShippingButton = "//td[5]/span/table/tbody/tr[2]/td[2]";
    private final String theByXpathWaiveShippingCheckbox = "//td/span/input";
    private final String theByXpathAddAddressStreet1 = "//div[3]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressStreet2 = "//div[4]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressCity = "//div[5]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressState = "//table[2]/tbody/tr/td[3]/select";
    private final String theByXpathAddAddressZip = "//div[6]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressPhone = "//div[7]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressCreateButton = "//div[2]/div/div[2]/span/table/tbody/tr[2]/td[2]";

	private SeleniumEnvironment myEnvironment;
	private String testName;
	
	
    public OrderTab(SeleniumEnvironment theEnvironment, String theTestName) {
    	
    	myEnvironment = theEnvironment; 
    	testName = theTestName;
    }
 

	public String getTheByXpathAddAddressStreet1() {
		return theByXpathAddAddressStreet1;
	}




	public String getTheByXpathAddAddressStreet2() {
		return theByXpathAddAddressStreet2;
	}




	public String getTheByXpathAddAddressCity() {
		return theByXpathAddAddressCity;
	}




	public String getTheByXpathAddAddressState() {
		return theByXpathAddAddressState;
	}




	public String getTheByXpathAddAddressZip() {
		return theByXpathAddAddressZip;
	}




	public String getTheByXpathAddAddressPhone() {
		return theByXpathAddAddressPhone;
	}




	public String getTheByXpathAddAddressCreateButton() {
		return theByXpathAddAddressCreateButton;
	}

	
	

	public String getTheByXpathCreateNewTicketButton() {
		return theByXpathCreateNewTicketButton;
	}



	public String getTheByXpathRefundButton() {
		return theByXpathRefundButton;
	}







	public String getTheByXpathWaiveShippingButton() {
		return theByXpathWaiveShippingButton;
	}


	public String getTheByXpathWaiveShippingCheckbox() {
		return theByXpathWaiveShippingCheckbox;
	}
}
