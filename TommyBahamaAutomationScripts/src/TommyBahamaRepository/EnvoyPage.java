package TommyBahamaRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EnvoyPage {

	
private SeleniumEnvironment myEnvironment;
private WebDriver driver;
	
private final String theByIdFirstNameTextBox = "formId:fld_SHIPPING_ADDRESS__FIRST_NAME_id";
private final String theByIdLastNameTextBox = "formId:fld_SHIPPING_ADDRESS__LAST_NAME_id";
private final String theByIdAddress1 = "formId:fld_SHIPPING_ADDRESS__ADDRESS1_id";
private final String theByIdAddress2 = "formId:fld_SHIPPING_ADDRESS__ADDRESS2_id";
private final String theByIdAddress3 = "formId:fld_SHIPPING_ADDRESS__ADDRESS3_id";
private final String theByIdCityTextBox = "formId:fld_SHIPPING_ADDRESS__CITY_id";
private final String theByIdRegionTextBox = "formId:fld_SHIPPING_ADDRESS__REGION_id";
private final String theByIdPostalCodeTextBox = "formId:fld_SHIPPING_ADDRESS__POSTAL_CODE_id";
private final String theByIdPhonePrimaryTextBox = "formId:fld_SHIPPING_ADDRESS__PHONE_PRIMARY_id";
private final String theByIdEmailTextBox = "formId:fld_SHIPPING_ADDRESS__EMAIL_id";
private final String theByIdPaymentMethodCC = "formId:fld_PAYMENT_INFORMATION__PAYMENT_METHOD_id:0";
private final String theByIdCreditCardTextBox = "formId:fld_PAYMENT_INFORMATION__CARD_NUMBER_id";
private final String theByIdCCExpMonth = "formId:fld_PAYMENT_INFORMATION__EXPIRATION_MONTH_id";
private final String theByIdCCExpYear = "formId:fld_PAYMENT_INFORMATION__EXPIRATION_YEAR_id";
private final String theByIdSecurityCodeTextBox = "formId:fld_PAYMENT_INFORMATION__SECURITY_CODE_id";
private final String theByIdSubmitBtn = "formId:submitBtnId";


private final String theByNameEnvoyFrame = "envoy";



public EnvoyPage(SeleniumEnvironment theEnvironment, WebDriver theDriver){
	
	driver = theDriver;
	
	myEnvironment = theEnvironment;
	
	//myEnvironment = new SeleniumEnvironment(driver);
	
}

public String getTheByIdFirstNameTextBox() {
	return theByIdFirstNameTextBox;
}

public String getTheByNameEnvoyFrame() {
	return theByNameEnvoyFrame;
}

public String getTheByIdLastNameTextBox() {
	return theByIdLastNameTextBox;
}

public String getTheByIdAddress1() {
	return theByIdAddress1;
}

public String getTheByIdAddress2() {
	return theByIdAddress2;
}

public String getTheByIdAddress3() {
	return theByIdAddress3;
}

public String getTheByIdCityTextBox() {
	return theByIdCityTextBox;
}

public String getTheByIdRegionTextBox() {
	return theByIdRegionTextBox;
}

public String getTheByIdPostalCodeTextBox() {
	return theByIdPostalCodeTextBox;
}

public String getTheByIdPrimaryPhoneTextBox() {
	return theByIdPhonePrimaryTextBox;
}

public String getTheByIdEmailTextBox() {
	return theByIdEmailTextBox;
}

public String getTheByIdPaymentMethodCC() {
	return theByIdPaymentMethodCC;
}

public String getTheByIdCreditCardTextBox() {
	return theByIdCreditCardTextBox;
}

public String getTheByIdCCExpMonth() {
	return theByIdCCExpMonth;
}

public String getTheByIdCCExpYear() {
	return theByIdCCExpYear;
}

public String getTheByIdSecurityCodeTextBox() {
	return theByIdSecurityCodeTextBox;
}

public String getTheByIdSubmitBtn() {
	return theByIdSubmitBtn;
}

public void enterUkAddress() {
	// TODO Auto-generated method stub
	WebElement ce;
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdFirstNameTextBox()));
	ce.sendKeys("Jack");
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdLastNameTextBox()));
	ce.sendKeys("West");
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdAddress1()));
	ce.sendKeys("Elton Way");
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdCityTextBox()));
	ce.sendKeys("Watford");
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdPostalCodeTextBox()));
	ce.sendKeys("WD25 8HA");
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdPrimaryPhoneTextBox()));
	ce.sendKeys("44-1923-235-881");
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdEmailTextBox()));
	ce.sendKeys("jack.west@tommybahama.com");
}

public void enterCcInfo() {
	// TODO Auto-generated method stub
	WebElement ce;
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdPaymentMethodCC()));
	ce.click();
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdCreditCardTextBox()));
	ce.sendKeys("4111111111111111");
	ce = driver.findElement(By.id(this
			.getTheByIdCCExpMonth()));
	ce = ce.findElement(By
			.cssSelector("option[value=\"7\"]"));
	// Thread.Sleep(myEnvironment.DefaultSleep);
	ce.click();
	
	ce = driver.findElement(By.id(this
			.getTheByIdCCExpYear()));
	ce = ce.findElement(By
			.xpath("(//option[@value='5'])[2]"));
	// Thread.Sleep(myEnvironment.DefaultSleep);
	ce.click();
	
	ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdSecurityCodeTextBox()));
	ce.sendKeys("111");
	
	
}


	
}
