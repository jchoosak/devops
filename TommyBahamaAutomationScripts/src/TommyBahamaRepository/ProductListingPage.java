package TommyBahamaRepository;

import java.util.List;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.ProxyServer;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProductListingPage {
	private final String theFirstItem = "//div[2]/div[2]/a";
	private final String theFirstImage = "//div[@id='imgThumb_1']/a/img";
	private final String theSecondItem = "//td[2]/div/div[2]/div[2]/a";
	private final String theThridItem = "//td[3]/div/div[2]/div[2]/a";
	private final String theFourthItem = "//table[@id='cat_list_table']/tbody/tr/td[4]/div/div[2]/div[2]/a";
	private final String theEighthItem = "//div[@id='imgContainer_7']/div[2]/div[2]/a";
	private final String theUrlType = "http://";
	private final String theFourtyEightPrice = "$48.00";
	private final String theSeventyTwoPrice = "$72.00";
	private final String theSeventyEightPrice = "$78.00";
	private final String theFourtyFivePrice = "$45.00";
	private final String theThirtyEightPrice = "$38.00";
	private final String theSeventeenPrice = "$17.00";
	private final String theTwentyPrice = "$20.00";
	private final String theEightyEightPrice = "$88.00";
	private final String theNinetyEightPrice = "$98.00";
	private final String theByXPathDiffuser = "//a[contains(text(),'Island Blend Room Diffuser')]";
	private final String theByXPathProductionDiffuser = "//a[contains(text(),'Diffuser Oil Refill')]";
	private final String theFPNextPageLink = "//div[@id='divFooterItemsNav']/a[2]";
	private final String theNextPageLink = "//div[@id='divFooterItemsNav']/a[3]";
	private final String theShowAllLink = "(//a[contains(text(),'Show All')])[2]";
	private final String theByXPathNextPageLink = "//a[contains(text(),'next page >')]";
	// private final String theByLinkNextPageLink = "next page >";
	private final String theCandaShippingRestrictionText = "Please note, this product cannot be shipped to CANADA using our Canada FedEx";
	private final String theAustraliaShippingRestrictionText = "We're sorry, this product cannot be shipped to AUSTRALIA";
	private final String theUnitedKingdomShippingRestrictionText = "We're sorry, this product cannot be shipped to United Kingdom";
	
	private final String theSearchResultCanadaShippingRestrictionText = "Shipping Restriction May Apply";
	private final String theByXPathAustralianCurrency = "//div[@id='imgContainer_2']/div[2]/div";

	private final String thePageName = "PLP";
	
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	
	public ProductListingPage(WebDriver theDriver,
			SeleniumEnvironment theEnvironment)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
	}
	
	public String getTheFirstItem() {
		return theFirstItem;
	}

	public String getTheFirstImage() {
		return theFirstImage;
	}

	public String getTheSecondItem() {
		return theSecondItem;
	}

	public String getTheThridItem() {
		return theThridItem;
	}

	public String getTheFourthItem() {
		return theFourthItem;
	}

	public String getTheEighthItem() {
		return theEighthItem;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheFourtyEightPrice() {
		return theFourtyEightPrice;
	}

	public String getTheSeventyTwoPrice() {
		return theSeventyTwoPrice;
	}

	public String getTheSeventyEightPrice() {
		return theSeventyEightPrice;
	}

	public String getTheFourtyFivePrice() {
		return theFourtyFivePrice;
	}

	public String getTheThirtyEightPrice() {
		return theThirtyEightPrice;
	}

	public String getTheSeventeenPrice() {
		return theSeventeenPrice;
	}

	public String getTheTwentyPrice() {
		return theTwentyPrice;
	}

	public String getTheEightyEightPrice() {
		return theEightyEightPrice;
	}

	public String getTheNinetyEightPrice() {
		return theNinetyEightPrice;
	}

	public String getTheByXPathDiffuser() {
		return theByXPathDiffuser;
	}

	public String getTheByXPathProductionDiffuser() {
		return theByXPathProductionDiffuser;
	}

	public String getTheFPNextPageLink() {
		return theFPNextPageLink;
	}

	public String getTheNextPageLink() {
		return theNextPageLink;
	}

	public String getTheShowAllLink() {
		return theShowAllLink;
	}

	public String getTheByXPathNextPageLink() {
		return theByXPathNextPageLink;
	}

	public String getTheCandaShippingRestrictionText() {
		return theCandaShippingRestrictionText;
	}

	public String getTheAustraliaShippingRestrictionText() {
		return theAustraliaShippingRestrictionText;
	}

	public String getTheSearchResultCanadaShippingRestrictionText() {
		return theSearchResultCanadaShippingRestrictionText;
	}

	public String getTheByXPathAustralianCurrency() {
		return theByXPathAustralianCurrency;
	}

	// / <summary>
	// / This just runs through the PLP for cards and checks that each has the
	// correct Restriction message.
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="myEnvironment"></param>
	public void checkAllTraditionalCards() {
		WebElement ce;
		for (int i = 0; i < 2; i++) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i
							+ "']/div[2]/div[2]"));
			System.out.println(ce.getText());
			System.out.println(theSearchResultCanadaShippingRestrictionText);
			Assert.assertTrue(ce
					.getText()
					.trim()
					.contains(
							theSearchResultCanadaShippingRestrictionText.trim()));
			System.out
					.println("The Restriction text is present for this product!        "
							+ ce.getText());
			if (i == 2) {
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
				ce.click();
			}
		}

	}

	// / <summary>
	// / This method traverses a PLP and finds the matching product and checks
	// to see that the product
	// / has the proper restriction text. This method is passed the Webdriver
	// and Environment objects and the product ID
	// / of the product being tested.
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="myEnvironment"></param>
	// / <param name="theProductID"></param>
	public void checkRestrictionText(String theProductID)
			throws InterruptedException {
		WebElement ce;
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));

		if (myEnvironment.isElementPresent(By.xpath(this.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheShowAllLink()));
			ce.click();
			// ce = myEnvironment.waitForDynamicElement(
			// By.XPath("//div[@id='imgThumb_" + 0 + "']/a/img"));
		}
		for (int i = 0; i < 250; i++) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
			System.out.println(ce.getAttribute("src"));
			Thread.sleep(500);
			if (ce.getAttribute("src").contains(theProductID)) {
				// ce = myEnvironment.waitForDynamicElement(
				// By.XPath("//div[@id='imgContainer_" + i +
				// "']/div[2]/div[2]"));
				Assert.assertTrue(myEnvironment.isElementPresent(
						By.cssSelector("#imgContainer_"
								+ i
								+ " > div.cat_product_desc > div.intl_restricted_icon")));
				ce = myEnvironment
						.waitForDynamicElement(
								By.cssSelector("#imgContainer_"
										+ i
										+ " > div.cat_product_desc > div.intl_restricted_icon"));
				Assert.assertEquals(ce.getText(),
						theSearchResultCanadaShippingRestrictionText);
				System.out
						.println("The Restriction text is present for this product!        "
								+ ce.getText());
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
				ce.click();
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='choiceContainer']/div[2]"));
				if (ce.getText().contains("Canada")) {
					Assert.assertEquals(ce.getText(),
							theCandaShippingRestrictionText);
					break;
				} else if (ce.getText().contains("AUSTRALIA")) {
					Assert.assertEquals(ce.getText(),
							theAustraliaShippingRestrictionText);
					break;
				}  else if (ce.getText().contains("United Kingdom")) {
					Assert.assertEquals(ce.getText(),
							theUnitedKingdomShippingRestrictionText);
					break;
				}
			}
		}
	}

	// / <summary>
	// / This helper method pulls the product ID number from the swatch's src
	// string.
	// / This helper mehtod is passed the parts of the xpath to find the swatch
	// and the webdriver object.
	// / </summary>
	// / <param name="swatchStringStart"></param>
	// / <param name="swatchStringEnd"></param>
	// / <param name="driver"></param>
	// / <param name="i"></param>
	// / <returns></returns>
	public String pullProductCodeFromSwatch(String swatchStringStart,
			String swatchStringEnd, int i) {
		String url = driver.getCurrentUrl();
		String pulledProduct;
		pulledProduct = driver.findElement(
				By.xpath(swatchStringStart + i + swatchStringEnd))
				.getAttribute("src");
		if (url.contains("www"))
			pulledProduct = pulledProduct.substring(47);
		else
			pulledProduct = pulledProduct.substring(49);
		System.out.println(pulledProduct);
		int index = pulledProduct.indexOf("_");
		pulledProduct = pulledProduct.substring(0, index);
		System.out.println(pulledProduct);
		return pulledProduct;
	}

	// / <summary>
	// / This helper method pulls the Color ID number from the swatch's src
	// string.
	// / This helper mehtod is passed the parts of the xpath to find the swatch
	// and the webdriver object.
	// / </summary>
	// / <param name="swatchStringStart"></param>
	// / <param name="swatchStringEnd"></param>
	// / <param name="driver"></param>
	// / <param name="i"></param>
	// / <returns></returns>
	public String pullColorCodeFromSwatch(String swatchStringStart,
			String swatchStringEnd, int i) {
		String url = driver.getCurrentUrl();
		String pulledColor;
		pulledColor = driver.findElement(
				By.xpath(swatchStringStart + i + swatchStringEnd))
				.getAttribute("src");
		if (url.contains("ecap"))
			pulledColor = pulledColor.substring(56);
		else
			pulledColor = pulledColor.substring(54);
		System.out.println(pulledColor);
		int index = pulledColor.indexOf("_");
		pulledColor = pulledColor.substring(0, index);
		System.out.println(pulledColor);
		return pulledColor;
	}

	// / <summary>
	// / This helper method pulls the Color ID number from the thumbnail's src
	// string.
	// This helper mehtod is passed the parts of the xpath to find the thumbnail
	// and the webdriver object.
	// / </summary>
	// / <param name="thumbNailStart"></param>
	// / <param name="thumbNailEnd"></param>
	// / <param name="driver"></param>
	// / <param name="j"></param>
	// / <returns></returns>
	public String pullColorCodeFromThumbNail(String thumbNailStart,
			String thumbNailEnd, int j) {
		String pulledColor;
		pulledColor = driver.findElement(
				By.xpath(thumbNailStart + j + thumbNailEnd))
				.getAttribute("src");
		pulledColor = pulledColor.substring(54);
		System.out.println(pulledColor);
		int index = pulledColor.indexOf("_");
		pulledColor = pulledColor.substring(0, index);
		System.out.println(pulledColor);
		return pulledColor;
	}

	/*
	 * This helper method pulls the product ID number from the thumbnail's src
	 * string. This helper mehtod is passed the parts of the xpath to find the
	 * swatch and the webdriver object.
	 */
	public String pullProductCodeFromThumbNail(String thumbNailStart,
			String thumbNailEnd, int j) {
		String url = driver.getCurrentUrl();
		String productCode;
		productCode = driver.findElement(
				By.xpath(thumbNailStart + j + thumbNailEnd))
				.getAttribute("src");
		System.out.println("This is the src:            " + productCode);
		//if (url.contains("ecap") || url.contains("test"))
		if (url.contains("test"))
			productCode = productCode.substring(49);
		else
			productCode = productCode.substring(47);
		System.out.println(productCode);
		int index = productCode.indexOf("_");
		productCode = productCode.substring(0, index);
		System.out.println(productCode);
		return productCode;
	}

	/*
	 * Choose Swatch method searches for a certain colored swatch and then makes
	 * sure when the product is selected that the PDP loads with this color as
	 * the default. This method also changes the color of each thumbnail as it
	 * searches for the certain color. Plan on now making a version of this
	 * method that just traverses all swatches on a PLP and checks the thumb
	 * nail each time a swatch is clicked. This method is passed the webdriver
	 * and environment objects, the color to be searced for and the PDP object.
	 */
	public void chooseSwatch( String theColorName,
			ProductDetailPage myProductDetailObjs) {
		WebElement ce;
		String swatchStringStart = "(//img[@id=''])[";
		String swatchStringEnd = "]";
		String currentProduct = "";
		String thumbNailStart = "//div[@id='imgThumb_";
		String thumbNailEnd = "']/a/img";
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(thumbNailStart + 0 + thumbNailEnd));
		// myEnvironment.myWaitForWDElement(driver, By.XPath(thumbNailStart + 0
		// + thumbNailEnd));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheShowAllLink()));
		ce.click();

		for (int i = 1; i < 1000; i++) {
			if (!myEnvironment.isElementPresent(
					By.xpath(swatchStringStart + i + swatchStringEnd))) {
				System.out
						.println("This entered color is not present for this product group!");
				break;
			}
			currentProduct = this.pullProductCodeFromSwatch(swatchStringStart,
					swatchStringEnd, i);
			System.out.println("The following is the product code:");
			System.out.println(currentProduct);

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(swatchStringStart + i + swatchStringEnd));
			System.out.println(ce.getAttribute("src"));
			ce.click();
			String currentColor = this.pullColorCodeFromSwatch(
					swatchStringStart, swatchStringEnd, i);
			System.out
					.println("The following is the current color of the swatch that has been clicked.");
			System.out.println(currentColor);

			if (ce.getAttribute("src").contains(theColorName)) {
				// Assert.True(driver.FindElement(By.XPath(thumbNailStart + i +
				// thumbNailEnd)).GetAttribute("src").Contains(currentColor));
				System.out.println("It has the color name...............");
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(thumbNailStart + 0 + thumbNailEnd));
				if (!ce.getAttribute("src").contains(currentProduct)) {
					currentProduct = this.pullProductCodeFromSwatch(
							swatchStringStart, swatchStringEnd, i);
					System.out.println(currentProduct);
					System.out
							.println("Does not contain the current product name..................");
					for (int k = 0; k < 40; k++) {
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(thumbNailStart + k + thumbNailEnd));
						System.out.println(ce.getAttribute("src"));
						if (ce.getAttribute("src").contains(currentProduct)) {
							Assert.assertTrue(ce.getAttribute("src").contains(
									currentColor));
							ce.click();
							System.out.println("A thumbnail image was clicked");
							break;
						}
					}
				} else {
					ce = myEnvironment.waitForDynamicElement(
							By.xpath(thumbNailStart + 0 + thumbNailEnd));
					ce.click();
				}

				ce = myEnvironment.waitForDynamicElement( By
						.xpath(myProductDetailObjs.getTheFirstCrossSellItem()));
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//*[@id='imgDetail']"));
				System.out.println(ce.getAttribute("src"));
				System.out.println(theColorName);
				Assert.assertTrue(ce.getAttribute("src").contains(theColorName));
				System.out
						.println("We are getting past the Assert, why are we not breaking out of this loop?");
				break;
			}
		}
	}

	/*
	 * THis method goes through a PLP and selects each swatch and then checks to
	 * see if the cooresponding thumbnail changed to the correct color. Passed
	 * the Webdriver, environment and PDP objects.
	 */
	public void testPLPSwatches(ProductDetailPage myProductDetailObjs) {
		WebElement ce;
		String swatchStringStart = "(//img[@id=''])[";
		String swatchStringEnd = "]";
		String currentProduct = "";
		String thumbNailStart = "//div[@id='imgThumb_";
		String thumbNailEnd = "']/a/img";
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(thumbNailStart + 0 + thumbNailEnd));

		if (myEnvironment.isElementPresent(By.xpath(this.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheShowAllLink()));
			ce.click();
		}

		// ce = myEnvironment.waitForDynamicElement(
		// By.XPath(this.ShowAllLink));
		// ce.Click();
		for (int i = 1; i < 1000; i++) {
			if (!myEnvironment.isElementPresent(
					By.xpath(swatchStringStart + i + swatchStringEnd))) {
				System.out
						.println("There is no more swatches to test!");
				break;
			}
			currentProduct = this.pullProductCodeFromSwatch(swatchStringStart,
					swatchStringEnd, i);
			System.out.println("The following is the productcode:");
			System.out.println(currentProduct);
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(swatchStringStart + i + swatchStringEnd));
			System.out.println(ce.getAttribute("src"));
			ce.click();
			String currentColor = this.pullColorCodeFromSwatch(
					swatchStringStart, swatchStringEnd, i);
			System.out
					.println("The following is the current color of the swatch that has been clicked.");
			System.out.println(currentColor);
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(swatchStringStart + i + swatchStringEnd));
			if (ce.getAttribute("src").contains(currentColor)) {
				System.out.println("It has the color name...............");
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(thumbNailStart + 0 + thumbNailEnd));
				if (!ce.getAttribute("src").contains(currentProduct)) {
					currentProduct = this.pullProductCodeFromSwatch(
							swatchStringStart, swatchStringEnd, i);
					System.out.println("The following is the ProductCode.");
					System.out.println(currentProduct);
					for (int k = 0; k < 40; k++) {
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(thumbNailStart + k + thumbNailEnd));
						System.out.println(ce.getAttribute("src"));
						if (ce.getAttribute("src").contains(currentProduct)) {
							Assert.assertTrue(ce.getAttribute("src").contains(
									currentColor));
							break;
						}
					}
				} else
					continue;
			}
		}
	}

	/*
	 * This method goes through a list of links that are in xpath form and then
	 * traverses through the products on the PLP by the xpaths of each product
	 * This method is used to cache the different PLP pages after a build in an
	 * environment. This method is passed the list of links to the different
	 * PLPs, the webdriver and the PLP object.
	 */

	public void traverseLinks(List<String> theList, 
			ProductDetailPage myProductDetailObjs, String by, ProxyServer server, String testName)
			throws InterruptedException {
		WebElement ce;
		myEnvironment.waitForPageLoaded();
		for (String currentLink : theList) {
			String Url;
			ce = myEnvironment.waitForDynamicElement(
					By.id("emailFormFooterId"));
			System.out.println("This is the Start of a new PLP page.");
			System.out.println(Url = driver.getCurrentUrl());
			myEnvironment.ExtractJSLogs();
			myEnvironment.parseHarFile(server, testName);

			if (by == "xpath") {
				Thread.sleep(5000);
				ce = myEnvironment.waitForDynamicElement(
						By.id("emailFormFooterId"));
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(currentLink));
				ce.click();
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
				myEnvironment.ExtractJSLogs();
				myEnvironment.parseHarFile(server, testName);
			} else {
				ce = myEnvironment.waitForDynamicElement(
						By.id("emailFormFooterId"));
				ce = myEnvironment.waitForDynamicElement(
						By.linkText(currentLink));
				ce.click();
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
				myEnvironment.ExtractJSLogs();
				myEnvironment.parseHarFile(server, testName);
			}
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));

			if (myEnvironment.isElementPresent(
					By.xpath(this.getTheShowAllLink()))) { // Thread.sleep(1000);
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(this.getTheShowAllLink()));
				ce.click();
				Thread.sleep(5000);
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				System.out.println(Url = driver.getCurrentUrl());
				Url = driver.getCurrentUrl() + "?showAll=1";
				myEnvironment.ExtractJSLogs();
				myEnvironment.parseHarFile(server, testName);
			}

			for (int i = 0; i < 250; i++) {
				ce = myEnvironment.waitForDynamicElement(
						By.id("emailFormFooterId"));
				ce.sendKeys("stupid ass no delay on hover");
				if (myEnvironment.isElementPresent(
						By.xpath("//div[@id='imgThumb_" + i + "']/a/img"))) {
					ce = myEnvironment.waitForClickableElement(
							By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
					ce.click();
					
					myEnvironment.waitForElementToBePresent(By.cssSelector(myProductDetailObjs.getTheByCSSAddToBagLink()), 30);
					// Thread.sleep(myEnvironment.getThe_Default_Sleep());
					myEnvironment.waitForClickableElement( By
							.cssSelector(myProductDetailObjs
									.getTheByCSSAddToBagLink()));
					
					myEnvironment.parseHarFile(server, testName);
					myEnvironment.ExtractJSLogs();
					// Thread.Sleep(myEnvironment.DefaultSleep);
					driver.get(Url);
					myEnvironment.waitForElementToBePresent(By.id("emailFormFooterId"), 30);
					ce = myEnvironment.waitForDynamicElement(
							By.id("emailFormFooterId"));

				} else {
					break;
				}
			}
		}
	}

	/*
	 * This method goes through a list of links that are in xpath form and then
	 * traverses through the products on the PLP by the xpaths of each product
	 * This method is used to cache the different PLP pages after a build in an
	 * environment. This method is passed the list of links to the different
	 * PLPs, the webdriver and the PLP object.
	 */

	public void traverseProducts(List<String> theList,
			ProductDetailPage myProductDetailObjs, String by, ProxyServer server, String testName)
			throws InterruptedException {
		WebElement ce;
		myEnvironment.waitForDocumentReadyState();
		for (String currentLink : theList) {
			String Url;
			System.out.println("This is the Start of a new PLP page.");
			myEnvironment.waitForElementToBePresent(By.id("emailFormFooterId"), 30);
			ce = myEnvironment.waitForDynamicElement(By.id("emailFormFooterId"));
			System.out.println(Url = driver.getCurrentUrl());
			myEnvironment.parseHarFile(server, testName);
		//	myEnvironment.ExtractJSLogs();
			if (by == "xpath") {
				// Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce = myEnvironment.waitForDynamicElement(By.xpath(currentLink));
				ce.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce = myEnvironment.waitForDynamicElement(By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
				myEnvironment.parseHarFile(server, testName);
			} else {
				ce = myEnvironment.waitForDynamicElement(
						By.linkText(currentLink));
				ce.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce = myEnvironment.waitForDynamicElement(By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
				myEnvironment.parseHarFile(server, testName);
			}
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
			// Thread.sleep(myEnvironment.getThe_Default_Sleep());

			if (myEnvironment.isElementPresent(
					By.xpath(this.getTheShowAllLink()))) {
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheShowAllLink()));
				ce.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce = myEnvironment.waitForDynamicElement(By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				System.out.println(Url = driver.getCurrentUrl());
				Url = driver.getCurrentUrl();
				myEnvironment.parseHarFile(server, testName);

			}
		    ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
			ce.click();
			while (myEnvironment.isElementPresent(
					By.linkText("next product >"))) {
				
				ce = myEnvironment.waitForDynamicElement(By.id("emailFormFooterId"));
				ce.sendKeys("Need to waste some time here to make sure the timings are added to the HAR file.");
			
				if(!driver.getCurrentUrl().contains("Gift+Card")){
			    	ce = myEnvironment.waitForDynamicElement(
							By.id(myProductDetailObjs.getTheByIdSelectQuantity()));
					Select clickThis = new Select(ce);
					clickThis.selectByValue("2");
			    }
			
				ce = myEnvironment.waitForDynamicElement(
						By.linkText("next product >"));
				ce.click();	
				
			//	myEnvironment.waitForPageLoaded();
				
				//myEnvironment.waitForPageLoaded();
				ce = myEnvironment.waitForDynamicElement(By.id("emailFormFooterId"));
				
				
				
			    if(driver.getCurrentUrl().contains("Gift+Card")){

			    	myEnvironment.waitForDynamicElement(By.xpath("//img[@alt='Traditional Gift Card']"));
			    }
			    else {

				myEnvironment.waitForDynamicElement(By.xpath(myProductDetailObjs.getTheMainImage()));
			    }
/*				ce = myEnvironment.waitForDynamicElement(By.id("emailFormFooterId"));
				ce.sendKeys("Seems I need to waste a bit more time here to make sure the timings are added to the HAR file.");*/
				myEnvironment.parseHarFile(server, testName);
			}
			driver.get(Url);
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		}
	}

	public void traversePLPLinks(List<String> theList,
			ProductDetailPage myProductDetailObjs, String theBy) {
		int page;
		WebElement ce;
		for (String currentLink : theList) {
			page = 1;
			System.out.println("This is the Start of a new PLP page.");
			System.out.println(driver.getCurrentUrl());

			if (theBy == "xpath") {
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(currentLink));
				ce.click();
			} else {
				ce = myEnvironment.waitForDynamicElement(
						By.linkText(currentLink));
				ce.click();
			}

			while (myEnvironment.isElementPresent(
					By.xpath(this.getTheNextPageLink()))
					|| myEnvironment.isElementPresent(
							By.xpath(this.getTheByXPathNextPageLink()))) {
				myEnvironment.checkForBrokenImages();
				if (myEnvironment.isElementPresent(
						By.xpath("//div[@id='imgThumb_" + 19 + "']/a/img"))
						&& myEnvironment.isElementPresent(
								By.xpath(this.theFPNextPageLink))
						&& page == 1) {
					driver.findElement(By.xpath(this.theFPNextPageLink))
							.click();
					myEnvironment.waitForDynamicElement(
							By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
					// i = 0;
					page = page + 1;
				} else if (myEnvironment.isElementPresent(
						By.xpath("//div[@id='imgThumb_" + 19 + "']/a/img"))
						&& myEnvironment.isElementPresent(
								By.xpath(this.getTheByXPathNextPageLink()))) {
					driver.findElement(
							By.xpath(this.getTheByXPathNextPageLink())).click();
					myEnvironment.waitForDynamicElement(
							By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
					// i = 0;
				} else
					break;
			}
		}
	}
	
	public void traverseOutletPLP(ProductDetailPage myProductDetailObjs) {
		WebElement ce;
		String URL = driver.getCurrentUrl();

		for (int i = 1; i < 6; i++) {

			for (int j = 1; j < 4; j++) {
			
				if (myEnvironment.isElementPresent(By
						.xpath("//div[@id='content']/div[3]/div[2]/div[2]/div[" + i
								+ "]/div[" + j + "]/div/div/a/span/img"))) {
					//div[@id='content']/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/a/span/img
					ce = myEnvironment.waitForDynamicElement(By
							.xpath("//div[@id='content']/div[3]/div[2]/div[2]/div[" + i
								+ "]/div[" + j + "]/div/div/a/span/img"));
					ce.click();
					// Thread.sleep(myEnvironment.getThe_Default_Sleep());
					myEnvironment.waitForDynamicElement(By
							.xpath("//form[@id='addToCartForm']/h2/span[2]"));
					// Thread.Sleep(myEnvironment.DefaultSleep);
					driver.get(URL);

				}
				if (i == 5 && j == 3
						&& myEnvironment.isElementPresent(By
								.xpath("//a[contains(text(),'Next Page �')]"))) {
					ce = myEnvironment.waitForDynamicElement(By
							.xpath("//a[contains(text(),'Next Page �')]"));
					ce.click();
					i = 1; 
					j = 1;
					URL = driver.getCurrentUrl();
				}
				else if (i == 14
						&& !myEnvironment.isElementPresent(By
								.xpath("//a[contains(text(),'Next Page �')]"))) {
					break;
				}
			}
		}
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheUnitedKingdomShippingRestrictionText() {
		return theUnitedKingdomShippingRestrictionText;
	}
}
