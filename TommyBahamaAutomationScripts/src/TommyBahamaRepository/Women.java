package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class Women {
	private List<String> theWomensLinks = new ArrayList<String>();
	private List<String> theWomensViewAllAccessoriesLinks = new ArrayList<String>();
	private List<String> theWomensNewArrivalsLinks = new ArrayList<String>();
	private List<String> theWomensAccessoriesLinks = new ArrayList<String>();
	private List<String> theWomensInternationoalFitLinks = new ArrayList<String>();
	private List<String> theWomensDressesLinks = new ArrayList<String>();
	private List<String> theWomenSweatersLinks = new ArrayList<String>();
	private List<String> theWomensSwimLinks = new ArrayList<String>();
	private List<String> theWomensSummerSwimShopLinks = new ArrayList<String>();
	
	private List<String> theWomensJewelryLinks = new ArrayList<String>();
	private List<String> theWomensFragranceLinks = new ArrayList<String>();
	private List<String> theByXPathWomensAccessoriesLinks = new ArrayList<String>();

	private final String theByXPathViewAllAccessories = "(//a[contains(text(),'View All')])[11]";
	private final String theByXPathShirtsAndTunics = "(//a[@href='http://test.tommybahama.com/TBG/Women/Shirts_and_Tops.jsp'])[2]";
	private final String theByXPathNewArrivals = "(//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals.jsp'])[2]";
	private final String theByXPathFragranceAndBody = "(//a[@href='http://test.tommybahama.com/TBG/Women/Fragrance.jsp'])[2]";
	private final String theByXPathTeesAndTanks = "(//a[@href='http://test.tommybahama.com/TBG/Women/Tees_Tanks.jsp'])[2]";
	private final String theByXPathAccessories = "(//a[@href='http://test.tommybahama.com/TBG/Women/Accessories.jsp'])[2]";
	private final String theByXPathShoes = "(//a[@href='http://test.tommybahama.com/TBG/Women/Sandals.jsp'])[2]";


	private final String theByXPathInternationalFit = "(//a[@href='http://test.tommybahama.com/TBG/Women/International_Fit.jsp'])[2]";
	private final String theByXPathGuestFavorites = "(//a[contains(text(),'Guest Favorites')])[5]";
	private final String theByXPathDresses = "(//a[@href='http://test.tommybahama.com/TBG/Women/Dresses.jsp'])[2]";
	private final String theByXPathSweatShirts = "(//a[@href='http://test.tommybahama.com/TBG/Women/Sweatshirts.jsp'])[2]";
	private final String theByXPathSweaters = "(//a[@href='http://test.tommybahama.com/TBG/Women/Sweaters.jsp'])[2]";
	private final String theByXPathSkirts = "(//a[@href='http://test.tommybahama.com/TBG/Women/Skirts.jsp'])[2]";
	private final String theByXPathOuterWear = "(//a[@href='http://test.tommybahama.com/TBG/Women/Outerwear.jsp'])[2]";
	private final String theByXPathJeans = "(//a[@href='http://test.tommybahama.com/TBG/Women/Jeans.jsp'])[2]";
	private final String theByXPathShorts = "(//a[contains(text(),'Shorts')])[4]";
	private final String theByXPathSwim = "(//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear.jsp'])[2]";
	private final String theByXPathJewelry = "(//a[@href='http://test.tommybahama.com/TBG/Women/Trend_Jewelry.jsp'])[2]";
	private final String theByXPathViewAllTops = "(//a[@href='http://test.tommybahama.com/TBG/Women/View_All_Tops.jsp'])[2]";
	private final String theByXPathLoungeWear = "(//a[@href='http://test.tommybahama.com/TBG/Women/Lounge_Sleepwear.jsp'])[2]";
	private final String theByXPathPantsAndCapris = "(//a[@href='http://test.tommybahama.com/TBG/Women/Pants.jsp'])[2]";
	private final String theByXpathCreditCardsLink = "(//a[contains(text(),'Gift Cards')])[7]";
	private final String theByXPathHandBags = "(//a[@href='http://test.tommybahama.com/TBG/Women/Handbags.jsp'])[2]";
	
	
	private final String theByLinkTravelDresses = "Travel Dresses";
	private final String theByLinkBlackEssentials = "Black Essentials";
	private final String theByLinkPartyDresses = "Party Dresses";
	private final String theByLinkMaxiDresses = "Maxi Dresses";
	private final String theByLinkFloralPrints = "Floral Prints";
	private final String theByLinkSundresses = "Sundresses";
	private final String theByLinkViewAll = "View All";

	private final String theByXPathNAShirtsAndTunics = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Shirts.jsp']";
	private final String theByXPathNADressesAndSkirts = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Dresses_Skirts.jsp']";
	private final String theByXPathNATeesAndTanks = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Tees_Tanks.jsp']";
	private final String theByXPathNASweaters = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Sweaters.jsp']";
	private final String theByXPathNASweatShirts = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Sweatshirts.jsp']";
	private final String theByXPathNAJeans = "//li[3]/ul/li[7]/a";
	private final String theByXPathNAPantsAndShorts = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Pants_Shorts.jsp']";
	private final String theByXPathNASwimWear = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Swimwear.jsp']";
	private final String theByXPathNAAccessories = "//a[@href='http://test.tommybahama.com/TBG/Women/New_Product_Arrivals/Accessories.jsp']";

	private final String theByXPathTops = "//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear/Tops_Swim.jsp']";
	private final String theByXPathBottoms = "//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear/Bottoms_Swim.jsp']";
	private final String theByXPathOnePieceSuits = "//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear/One_Piece_Suits.jsp']";
	private final String theByXPathSundressesAndCoverups = "//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear/Sundresses_Coverups.jsp']";
	private final String theByXPathTankini = "//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear/Tankini.jsp']";
    private final String theByXPathRashguards = "//a[@href='http://test.tommybahama.com/TBG/Women/Swimwear/Rashguards.jsp']";
	
	private final String theByLinkCardigans = "Cardigans";
	private final String theByLinkLightweight = "Lightweight";
	private final String theByLinkCozyAndWarm = "Cozy & Warm";

	private final String theByLinkLoisHillSilverExclusives = "Lois Hill Silver Exclusives";
	private final String theByLinkGoldTones = "Gold Tones";
	private final String theByLinkEarrings = "Earrings";
	private final String theByLinkNecklaces = "Necklaces";
	private final String theByLinkBraceletsAndCuffs = "Bracelets & Cuffs";
	private final String theByLinkRings = "Rings";
	private final String theByLinkWatches = "Watches";
	
	private final String theByXPathWatches = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Watches.jsp']";
	private final String theByXPathTravelBags = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Travel_Bags.jsp']";
	private final String theByXPathSunglasses = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Sunglasses.jsp']";
	private final String theByXPathBelts = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Belts.jsp']";
	private final String theByXPathScarves = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Scarves.jsp']";
	private final String theByXPathAccessoriesShoes = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Shoes_Sandals.jsp']";
	private final String theByXPathTechCases = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Tech_Cases.jsp']";
	private final String theByXPathHats = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Hats.jsp']";
	private final String theByXPathTotesAndHandbags = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Totes_and_Handbags.jsp']";
	private final String theByXPathAccessoriesJewelry = "//a[@href='http://test.tommybahama.com/TBG/Women/Accessories/Jewelry.jsp']";
	
	private final String theByLinkSetSailMartinique = "Set Sail Martinique";
	private final String theByLinkTBforHer= "TB for Her";
	private final String theByLinkSignature = "Signature Scent";
	private final String theByLinkIslandParadiseAndBody = "Island Paradise & Body";
	private final String theByLinkGiftSets = "Gift Sets";

	private final String theByLinkSwimShop = "Visit the Swim Shop";
	
	private final String theByXPathSummerSwimShop = "(//a[@href='http://test.tommybahama.com/TBG/Women/Summer_Swim_Shop.jsp'])[2]";
	private final String theByXPathSummerSwimShopTops ="//a[@href='http://test.tommybahama.com/TBG/Women/Summer_Swim_Shop/Tops.jsp']";
	private final String theByXPathSummerSwimShopBottoms ="//a[@href='http://test.tommybahama.com/TBG/Women/Summer_Swim_Shop/Bottoms.jsp']";
	private final String theByXPathSummerSwimShopOnePiece ="//a[@href='http://test.tommybahama.com/TBG/Women/Summer_Swim_Shop/One_Pieces.jsp']";
	private final String theByXPathSummerSwimShopCoverups ="//a[@href='http://test.tommybahama.com/TBG/Women/Summer_Swim_Shop/Coverups.jsp']";


	private final String theByXPathInternationalFitTops = "(//a[contains(text(),'Tops')])[3]";
	private final String theByXPathInternationalFitBottoms = "(//a[contains(text(),'Bottoms')])[2]";
	private final String theByXPathInternationalFitDresses = "(//a[contains(text(),'Dresses')])[2]";
	

  	private final String thePageName = "Womens";
  	 private final String theWomensIndex = "WomensIndex";
  	 private final String theWomensAccessoriesIndex = "WomensAccessoriesIndex";
  	 private final String theWomensBeachAccessoriesIndex = "WomensBeachAccessories";
  	 private final String theWomensSwimShopIndex = "WomensSwimShopIndex";

	public void CreateWomensFragranceLinksList() {
		theWomensFragranceLinks.add(this.getTheByLinkSetSailMartinique());
		//theWomensFragranceLinks.add(this.getTheByLinkSignature());
		theWomensFragranceLinks.add(this.getTheByLinkGiftSets());
		theWomensFragranceLinks.add(this.getTheByLinkTBforHer());
	//	theWomensFragranceLinks.add(this.getTheByLinkIslandParadiseAndBody());
	}

	public void CreateWomensNewArrivalsLinksList() {
		theWomensNewArrivalsLinks.add(this.getTheByXPathNAShirtsAndTunics());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNADressesAndSkirts());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNATeesAndTanks());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNASweaters());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNAPantsAndShorts());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNASwimWear());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNAAccessories());
		theWomensNewArrivalsLinks.add(this.getTheByXPathNASweatShirts());
	}

	public void CreateWomensInternationalFitLinksList() {
		theWomensInternationoalFitLinks.add(this
				.getTheByXPathInternationalFitTops());
		theWomensInternationoalFitLinks.add(this
				.getTheByXPathInternationalFitBottoms());
		theWomensInternationoalFitLinks.add(this
				.getTheByXPathInternationalFitDresses());

	}

	public void CreateWomensDressesLinksList() {
		theWomensDressesLinks.add(this.getTheByLinkTravelDresses());
		theWomensDressesLinks.add(this.getTheByLinkMaxiDresses());
		theWomensDressesLinks.add(this.getTheByLinkViewAll());
		theWomensDressesLinks.add(this.getTheByLinkSundresses());
		theWomensDressesLinks.add(this.getTheByLinkBlackEssentials());
	}

	public void CreateWomensSweaterLinksList() {
		theWomenSweatersLinks.add(this.getTheByLinkCardigans());
		theWomenSweatersLinks.add(this.getTheByLinkLightweight());
		theWomenSweatersLinks.add(this.getTheByLinkCozyAndWarm());
	}

	public void CreateWomensSwimLinksList() {
		theWomensSwimLinks.add(this.getTheByXPathTops());
		theWomensSwimLinks.add(this.getTheByXPathRashguards());
		theWomensSwimLinks.add(this.getTheByXPathTankini());
		theWomensSwimLinks.add(this.getTheByXPathBottoms());
		theWomensSwimLinks.add(this.getTheByXPathOnePieceSuits());
		theWomensSwimLinks.add(this.getTheByXPathSundressesAndCoverups());		
	}
	
	public void CreateWomensSummerSwimShopLinksList() {
		theWomensSummerSwimShopLinks.add(this.getTheByXPathSummerSwimShopTops());
		theWomensSummerSwimShopLinks.add(this.getTheByXPathSummerSwimShopBottoms());
		theWomensSummerSwimShopLinks.add(this.getTheByXPathSummerSwimShopCoverups());
		theWomensSummerSwimShopLinks.add(this.getTheByXPathSummerSwimShopOnePiece());
	
	}

	public void CreateWomensJewelryLinksList() {
		theWomensJewelryLinks.add(this.getTheByLinkLoisHillSilverExclusives());
		theWomensJewelryLinks.add(this.getTheByLinkGoldTones());
		theWomensJewelryLinks.add(this.getTheByLinkRings());
		theWomensJewelryLinks.add(this.getTheByLinkBraceletsAndCuffs());
		theWomensJewelryLinks.add(this.getTheByLinkNecklaces());
		theWomensJewelryLinks.add(this.getTheByLinkEarrings());
		theWomensJewelryLinks.add(this.getTheByLinkWatches());
	}

	public void CreateWomensByXPathAccessoriesLinksList() {
		theByXPathWomensAccessoriesLinks.add(this.getTheByXPathAccessories());
		theByXPathWomensAccessoriesLinks.add(this
				.getTheByXPathAccessoriesJewelry());
	}

	public void CreateWomensAccessoriesLinksList() {
		
		theWomensAccessoriesLinks.add(this.getTheByXPathScarves());
		theWomensAccessoriesLinks.add(this.getTheByXPathWatches());
		theWomensAccessoriesLinks.add(this.getTheByXPathAccessoriesJewelry());
		theWomensAccessoriesLinks.add(this.getTheByXPathSunglasses());
		theWomensAccessoriesLinks.add(this.getTheByXPathAccessoriesShoes());
		theWomensAccessoriesLinks.add(this.getTheByXPathTravelBags());
		theWomensAccessoriesLinks.add(this.getTheByXPathTechCases());
		theWomensAccessoriesLinks.add(this.getTheByXPathHats());
		theWomensAccessoriesLinks.add(this.getTheByXPathBelts());
		theWomensAccessoriesLinks.add(this.getTheByXPathTotesAndHandbags());
	}

	public void CreateWomensViewAllAccessoriesLinksList() {
		theWomensViewAllAccessoriesLinks.add(this
				.getTheByXPathViewAllAccessories());
	}

	public void CreateWomensLinksList() {
			
	
	
		theWomensLinks.add(this.getTheByXPathViewAllTops());
		theWomensLinks.add(this.getTheByXPathSweaters());
		theWomensLinks.add(this.getTheByXPathSweatShirts());
		theWomensLinks.add(this.getTheByXPathShirtsAndTunics());
		theWomensLinks.add(this.getTheByXPathTeesAndTanks());
		theWomensLinks.add(this.getTheByXPathLoungeWear());
		theWomensLinks.add(this.getTheByXPathPantsAndCapris());
		theWomensLinks.add(this.getTheByXPathJeans());		
		theWomensLinks.add(this.getTheByXPathSkirts());
		theWomensLinks.add(this.getTheByXPathOuterWear());
		theWomensLinks.add(this.getTheByXPathSwim());
		theWomensLinks.add(this.getTheByXPathShoes());
		theWomensLinks.add(this.getTheByXPathJewelry());
		theWomensLinks.add(this.getTheByXPathFragranceAndBody());
		theWomensLinks.add(this.getTheByXPathNewArrivals());
		theWomensLinks.add(this.getTheByXPathInternationalFit());

	}

	public List<String> getTheWomensLinks() {
		return theWomensLinks;
	}

	public void setTheWomensLinks(List<String> theWomensLinks) {
		this.theWomensLinks = theWomensLinks;
	}

	public List<String> getTheWomensViewAllAccessoriesLinks() {
		return theWomensViewAllAccessoriesLinks;
	}

	public void setTheWomensViewAllAccessoriesLinks(
			List<String> theWomensViewAllAccessoriesLinks) {
		this.theWomensViewAllAccessoriesLinks = theWomensViewAllAccessoriesLinks;
	}

	public List<String> getTheWomensNewArrivalsLinks() {
		return theWomensNewArrivalsLinks;
	}

	public void setTheWomensNewArrivalsLinks(
			List<String> theWomensNewArrivalsLinks) {
		this.theWomensNewArrivalsLinks = theWomensNewArrivalsLinks;
	}

	public List<String> getTheWomensAccessoriesLinks() {
		return theWomensAccessoriesLinks;
	}

	public void setTheWomensAccessoriesLinks(
			List<String> theWomensAccessoriesLinks) {
		this.theWomensAccessoriesLinks = theWomensAccessoriesLinks;
	}

	public List<String> getTheWomensInternationoalFitLinks() {
		return theWomensInternationoalFitLinks;
	}

	public void setTheWomensInternationoalFitLinks(
			List<String> theWomensInternationoalFitLinks) {
		this.theWomensInternationoalFitLinks = theWomensInternationoalFitLinks;
	}

	public List<String> getTheWomensDressesLinks() {
		return theWomensDressesLinks;
	}

	public void setTheWomensDressesLinks(List<String> theWomensDressesLinks) {
		this.theWomensDressesLinks = theWomensDressesLinks;
	}

	public List<String> getTheWomenSweatersLinks() {
		return theWomenSweatersLinks;
	}

	public void setTheWomenSweatersLinks(List<String> theWomenSweatersLinks) {
		this.theWomenSweatersLinks = theWomenSweatersLinks;
	}

	public List<String> getTheWomensSwimLinks() {
		return theWomensSwimLinks;
	}

	public void setTheWomensSwimLinks(List<String> theWomensSwimLinks) {
		this.theWomensSwimLinks = theWomensSwimLinks;
	}

	public List<String> getTheWomensJewelryLinks() {
		return theWomensJewelryLinks;
	}

	public void setTheWomensJewelryLinks(List<String> theWomensJewelryLinks) {
		this.theWomensJewelryLinks = theWomensJewelryLinks;
	}

	public List<String> getTheWomensFragranceLinks() {
		return theWomensFragranceLinks;
	}

	public void setTheWomensFragranceLinks(List<String> theWomensFragranceLinks) {
		this.theWomensFragranceLinks = theWomensFragranceLinks;
	}

	public List<String> getTheByXPathWomensAccessoriesLinks() {
		return theByXPathWomensAccessoriesLinks;
	}

	public void setTheByXPathWomensAccessoriesLinks(
			List<String> theByXPathWomensAccessoriesLinks) {
		this.theByXPathWomensAccessoriesLinks = theByXPathWomensAccessoriesLinks;
	}

	public String getTheByXPathViewAllAccessories() {
		return theByXPathViewAllAccessories;
	}

	public String getTheByXPathShirtsAndTunics() {
		return theByXPathShirtsAndTunics;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathFragranceAndBody() {
		return theByXPathFragranceAndBody;
	}

	public String getTheByXPathTeesAndTanks() {
		return theByXPathTeesAndTanks;
	}

	public String getTheByXPathAccessories() {
		return theByXPathAccessories;
	}

	public String getTheByXPathShoes() {
		return theByXPathShoes;
	}

	public String getTheByXPathAccessoriesShoes() {
		return theByXPathAccessoriesShoes;
	}

	public String getTheByXPathInternationalFit() {
		return theByXPathInternationalFit;
	}

	public String getTheByXPathGuestFavorites() {
		return theByXPathGuestFavorites;
	}

	public String getTheByXPathDresses() {
		return theByXPathDresses;
	}

	public String getTheByXPathSweatShirts() {
		return theByXPathSweatShirts;
	}

	public String getTheByXPathSweaters() {
		return theByXPathSweaters;
	}

	public String getTheByXPathSkirts() {
		return theByXPathSkirts;
	}

	public String getTheByXPathOuterWear() {
		return theByXPathOuterWear;
	}

	public String getTheByXPathJeans() {
		return theByXPathJeans;
	}

	public String getTheByXPathShorts() {
		return theByXPathShorts;
	}

	public String getTheByXPathSwim() {
		return theByXPathSwim;
	}

	public String getTheByXPathJewelry() {
		return theByXPathJewelry;
	}

	public String getTheByXPathAccessoriesJewelry() {
		return theByXPathAccessoriesJewelry;
	}

	public String getTheByXPathViewAllTops() {
		return theByXPathViewAllTops;
	}

	public String getTheByXPathLoungeWear() {
		return theByXPathLoungeWear;
	}

	public String getTheByXPathPantsAndCapris() {
		return theByXPathPantsAndCapris;
	}

	public String getTheByXpathCreditCardsLink() {
		return theByXpathCreditCardsLink;
	}

	public String getTheByLinkTravelDresses() {
		return theByLinkTravelDresses;
	}

	public String getTheByLinkPartyDresses() {
		return theByLinkPartyDresses;
	}

	public String getTheByLinkMaxiDresses() {
		return theByLinkMaxiDresses;
	}

	public String getTheByLinkFloralPrints() {
		return theByLinkFloralPrints;
	}

	public String getTheByLinkSundresses() {
		return theByLinkSundresses;
	}

	public String getTheByXPathNAShirtsAndTunics() {
		return theByXPathNAShirtsAndTunics;
	}

	public String getTheByXPathNADressesAndSkirts() {
		return theByXPathNADressesAndSkirts;
	}

	public String getTheByXPathNATeesAndTanks() {
		return theByXPathNATeesAndTanks;
	}

	public String getTheByXPathNASweaters() {
		return theByXPathNASweaters;
	}

	public String getTheByXPathNASweatShirts() {
		return theByXPathNASweatShirts;
	}

	public String getTheByXPathNAJeans() {
		return theByXPathNAJeans;
	}

	public String getTheByXPathNAPantsAndShorts() {
		return theByXPathNAPantsAndShorts;
	}

	public String getTheByXPathNASwimWear() {
		return theByXPathNASwimWear;
	}

	public String getTheByXPathNAAccessories() {
		return theByXPathNAAccessories;
	}

	public String getTheByXPathTops() {
		return theByXPathTops;
	}

	public String getTheByXPathBottoms() {
		return theByXPathBottoms;
	}

	public String getTheByXPathOnePieceSuits() {
		return theByXPathOnePieceSuits;
	}

	public String getTheByXPathSundressesAndCoverups() {
		return theByXPathSundressesAndCoverups;
	}

	public String getTheByXPathTankini() {
		return theByXPathTankini;
	}

	public String getTheByLinkCardigans() {
		return theByLinkCardigans;
	}

	public String getTheByLinkLightweight() {
		return theByLinkLightweight;
	}

	public String getTheByLinkCozyAndWarm() {
		return theByLinkCozyAndWarm;
	}

	public String getTheByLinkLoisHillSilverExclusives() {
		return theByLinkLoisHillSilverExclusives;
	}

	public String getTheByLinkGoldTones() {
		return theByLinkGoldTones;
	}

	public String getTheByLinkEarrings() {
		return theByLinkEarrings;
	}

	public String getTheByLinkNecklaces() {
		return theByLinkNecklaces;
	}

	public String getTheByLinkBraceletsAndCuffs() {
		return theByLinkBraceletsAndCuffs;
	}

	public String getTheByLinkRings() {
		return theByLinkRings;
	}





	public String getTheByLinkSetSailMartinique() {
		return theByLinkSetSailMartinique;
	}

	public String getTheByLinkSignature() {
		return theByLinkSignature;
	}

	public String getTheByLinkIslandParadiseAndBody() {
		return theByLinkIslandParadiseAndBody;
	}

	public String getTheByLinkGiftSets() {
		return theByLinkGiftSets;
	}

	public String getTheByLinkSwimShop() {
		return theByLinkSwimShop;
	}

	public String getTheByXPathInternationalFitTops() {
		return theByXPathInternationalFitTops;
	}

	public String getTheByXPathInternationalFitBottoms() {
		return theByXPathInternationalFitBottoms;
	}

	public String getTheByXPathInternationalFitDresses() {
		return theByXPathInternationalFitDresses;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByLinkBlackEssentials() {
		return theByLinkBlackEssentials;
	}

	public String getTheByLinkTBforHer() {
		return theByLinkTBforHer;
	}

	public String getTheWomensIndex() {
		return theWomensIndex;
	}

	public String getTheWomensAccessoriesIndex() {
		return theWomensAccessoriesIndex;
	}

	public String getTheWomensBeachAccessoriesIndex() {
		return theWomensBeachAccessoriesIndex;
	}

	public String getTheWomensSwimShopIndex() {
		return theWomensSwimShopIndex;
	}

	public String getTheByXPathRashguards() {
		return theByXPathRashguards;
	}

	public String getTheByXPathSummerSwimShop() {
		return theByXPathSummerSwimShop;
	}

	public String getTheByXPathSummerSwimShopTops() {
		return theByXPathSummerSwimShopTops;
	}

	public String getTheByXPathSummerSwimShopBottoms() {
		return theByXPathSummerSwimShopBottoms;
	}

	public String getTheByXPathSummerSwimShopOnePiece() {
		return theByXPathSummerSwimShopOnePiece;
	}

	public String getTheByXPathSummerSwimShopCoverups() {
		return theByXPathSummerSwimShopCoverups;
	}

	public List<String> getTheWomensSummerSwimShopLinks() {
		return theWomensSummerSwimShopLinks;
	}

	public void setTheWomensSummerSwimShopLinks(
			List<String> theWomensSummerSwimShopLinks) {
		this.theWomensSummerSwimShopLinks = theWomensSummerSwimShopLinks;
	}

	public String getTheByXPathHandBags() {
		return theByXPathHandBags;
	}

	public String getTheByLinkViewAll() {
		return theByLinkViewAll;
	}

	public String getTheByXPathTravelBags() {
		return theByXPathTravelBags;
	}

	public String getTheByXPathSunglasses() {
		return theByXPathSunglasses;
	}

	public String getTheByXPathBelts() {
		return theByXPathBelts;
	}

	public String getTheByXPathScarves() {
		return theByXPathScarves;
	}



	public String getTheByXPathHats() {
		return theByXPathHats;
	}

	public String getTheByXPathTotesAndHandbags() {
		return theByXPathTotesAndHandbags;
	}



	public String getTheByXPathWatches() {
		return theByXPathWatches;
	}

	public String getTheByXPathTechCases() {
		return theByXPathTechCases;
	}

	public String getTheByLinkWatches() {
		return theByLinkWatches;
	}

}

