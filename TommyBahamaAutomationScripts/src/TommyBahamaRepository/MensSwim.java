package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class MensSwim {
	private List<String> theMensSwimXpathLinks = new ArrayList<String>();

	private List<String> theMensSwimBeachAccessoriesLinks = new ArrayList<String>();
	private List<String> theMensSwimLinks = new ArrayList<String>();

	private final String theByXPathMensSwim = "//div[5]/div[2]/map/area[2]";
	private final String theByXPathNewArrivals = "(//a[contains(text(),'New Arrivals')])[7]";
	private final String theByXPathViewAllSwimTrunks = "(//a[contains(text(),'View All Swim Trunks')])[2]";

	private final String theByXPathSwimByCollections = "(//a[contains(text(),'Swim by Collections')])[2]";
	private final String theByXPathViewAllBAndTSwimTrunks = "//a[contains(text(),'View All B&T Swim Trunks')]";
	private final String theByXShirtsAndGraphicTShirts = "//a[contains(text(),'Shirts & Graphic T-Shirts')]";

	private final String theByXPath4InchInseam = "//a[contains(text(),'4.5\"-6.5\" Inseam')]";
	private final String theByXPath7InchInseam = "(//a[contains(text(),'7.5\" Inseam')])[2]";
	private final String theByXPath9InchInseam = "(//a[contains(text(),'9\" & 11\" Inseam')])[2]";

	// private final String theByXPathSandals =
	// "(//a[contains(text(),'Shoes & Sandals')])[5]";
	private final String theByXPathBeachAccessories = "(//a[contains(text(),'Beach Accessories')])[3]";
	private final String theByXPathIslandValues = "(//a[contains(text(),'Island Values')])[3]";
	private final String theByXPathUnderWire = "(//a[contains(text(),'Sweatshirts')])[4]";

	private final String theByXPathSweaters = "(//a[contains(text(),'Sweaters')])[4]";
	private final String theByXPathSkirts = "(//a[contains(text(),'Skirts')])[2]";
	private final String theByXPathOuterWear = "(//a[contains(text(),'Outerwear')])[2]";
	private final String theByXPathJeans = "(//a[contains(text(),'Jeans')])[4]";
	private final String theByXPathShorts = "(//a[contains(text(),'Shorts')])[4]";
	private final String theByXPathSwim = "(//a[contains(text(),'Swim')])[11]";
	private final String theByXPathJewlery = "(//a[contains(text(),'Jewelry')])[2]";
	private final String theByXPathViewAllTops = "(//a[contains(text(),'View All Tops')])[2]";
	private final String theByXPathLoungeWear = "(//a[contains(text(),'Loungewear')])[4]";
	private final String theByXPathPantsAndCapris = "(//a[contains(text(),'Pants & Capris')])[2]";
	private final String theByXpathCreditCardsLink = "(//a[contains(text(),'Gift Cards')])[7]";

	private final String theByLinkSolid = "Solid";
	private final String theByLinkAllPrints = "All Prints";

	private final String theByLinkSunglasses = "Sunglasses";
	private final String theByLinkBeachAndBags = "Towels & Bags";
	private final String theByLinkCapsAndHats = "Caps & Hats";
	private final String theByLinkRelaxWatches = "Relax Watches";
	
	private final String thePageName = "MensSwim";

	public void CreateMensSwimLinksList() {
		theMensSwimLinks.add(this.getTheByLinkSolid());
		theMensSwimLinks.add(this.getTheByLinkAllPrints());
	}

	public void CreateMensBeachAccessoriesLinksList() {
		theMensSwimBeachAccessoriesLinks.add(this.getTheByLinkSunglasses());
		theMensSwimBeachAccessoriesLinks.add(this.getTheByLinkBeachAndBags());
		theMensSwimBeachAccessoriesLinks.add(this.getTheByLinkCapsAndHats());
		theMensSwimBeachAccessoriesLinks.add(this.getTheByLinkRelaxWatches());
	}

	public void CreateMensXPathLinksList() {
		theMensSwimXpathLinks.add(this.getTheByXPathNewArrivals());
		theMensSwimXpathLinks.add(this.getTheByXPathViewAllSwimTrunks());
		theMensSwimXpathLinks.add(this.getTheByXPathViewAllBAndTSwimTrunks());
		theMensSwimXpathLinks.add(this.getTheByXShirtsAndGraphicTShirts());
		theMensSwimXpathLinks.add(this.getTheByXPath4InchInseam());
		theMensSwimXpathLinks.add(this.getTheByXPathBeachAccessories());
		theMensSwimXpathLinks.add(this.getTheByXPath7InchInseam());
		theMensSwimXpathLinks.add(this.getTheByXPath9InchInseam());

	}

	public List<String> getTheMensSwimXpathLinks() {
		return theMensSwimXpathLinks;
	}

	public void setTheMensSwimXpathLinks(List<String> theMensSwimXpathLinks) {
		this.theMensSwimXpathLinks = theMensSwimXpathLinks;
	}

	public List<String> getTheMensSwimBeachAccessoriesLinks() {
		return theMensSwimBeachAccessoriesLinks;
	}

	public void setTheMensSwimBeachAccessoriesLinks(
			List<String> theMensSwimBeachAccessoriesLinks) {
		this.theMensSwimBeachAccessoriesLinks = theMensSwimBeachAccessoriesLinks;
	}

	public List<String> getTheMensSwimLinks() {
		return theMensSwimLinks;
	}

	public void setTheMensSwimLinks(List<String> theMensSwimLinks) {
		this.theMensSwimLinks = theMensSwimLinks;
	}

	public String getTheByXPathMensSwim() {
		return theByXPathMensSwim;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathViewAllSwimTrunks() {
		return theByXPathViewAllSwimTrunks;
	}

	public String getTheByXPathSwimByCollections() {
		return theByXPathSwimByCollections;
	}

	public String getTheByXPathViewAllBAndTSwimTrunks() {
		return theByXPathViewAllBAndTSwimTrunks;
	}

	public String getTheByXShirtsAndGraphicTShirts() {
		return theByXShirtsAndGraphicTShirts;
	}

	public String getTheByXPath4InchInseam() {
		return theByXPath4InchInseam;
	}

	public String getTheByXPath7InchInseam() {
		return theByXPath7InchInseam;
	}

	public String getTheByXPath9InchInseam() {
		return theByXPath9InchInseam;
	}

	public String getTheByXPathBeachAccessories() {
		return theByXPathBeachAccessories;
	}

	public String getTheByXPathIslandValues() {
		return theByXPathIslandValues;
	}

	public String getTheByXPathUnderWire() {
		return theByXPathUnderWire;
	}

	public String getTheByXPathSweaters() {
		return theByXPathSweaters;
	}

	public String getTheByXPathSkirts() {
		return theByXPathSkirts;
	}

	public String getTheByXPathOuterWear() {
		return theByXPathOuterWear;
	}

	public String getTheByXPathJeans() {
		return theByXPathJeans;
	}

	public String getTheByXPathShorts() {
		return theByXPathShorts;
	}

	public String getTheByXPathSwim() {
		return theByXPathSwim;
	}

	public String getTheByXPathJewlery() {
		return theByXPathJewlery;
	}

	public String getTheByXPathViewAllTops() {
		return theByXPathViewAllTops;
	}

	public String getTheByXPathLoungeWear() {
		return theByXPathLoungeWear;
	}

	public String getTheByXPathPantsAndCapris() {
		return theByXPathPantsAndCapris;
	}

	public String getTheByXpathCreditCardsLink() {
		return theByXpathCreditCardsLink;
	}

	public String getTheByLinkSolid() {
		return theByLinkSolid;
	}

	public String getTheByLinkAllPrints() {
		return theByLinkAllPrints;
	}

	public String getTheByLinkSunglasses() {
		return theByLinkSunglasses;
	}

	public String getTheByLinkBeachAndBags() {
		return theByLinkBeachAndBags;
	}

	public String getTheByLinkCapsAndHats() {
		return theByLinkCapsAndHats;
	}

	public String getTheByLinkRelaxWatches() {
		return theByLinkRelaxWatches;
	}

	public String getThePageName() {
		return thePageName;
	}

}
