package TommyBahamaRepository;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

// This is the Product Detail Page Class that contains the element repository for the page and methods that 
// execute functionality that is centered around the PDP. 
public class ProductDetailPage {
	private final String theByXPathFirstSize = "//li/div[2]/div[2]/div/a";
	private final String theByLinkMediumSize = "M";
	private final String theByXPathSecondSize = "//div[2]/div[2]/a";
	private final String theByCSSProductPrice = "p.content_price";
	private final String theByXPathThirdSize = "//div[@id='XXL']";
	private final String theByLinkTextXLargeSize = "XL";
	private final String theByLinkTextLargeSize = "L";
	private final String theByLinkText34By32Size = "34 x 32";
	private final String theByLinkText36By32Size = "36 x 32";
	private final String theByXpathSizeEleven = "(//a[contains(text(),'11')])[2]";
	private final String theByXpathAddToBagButton = "//*[@id='aBtnAddToBagLink']";
	private final String theByCSSAddToBagLink = "#aBtnAddToBagLink";
	private final String theByIdAddToBagLink = "aBtnAddToBagLink";
	private final String theByIdAddToBagBtn = "divAddToBagButton";
	private final String theByOneXByBigLink = "//div[@id='1X x BG']/a";
	private final String theFirstCrossSellItem = "//p/a";
	private final String theByIdSelectQuantity = "selQuantity";
	private final String theProductSize = "//div[@id='divProductSize']/div/div/a";
	private final String theHelpLink = "(Help)";
	private final String theQuantityFour = "4";
	private final String theQuantityTwo = "2";
	private final String theByIDSelectQty = "selQuantity";
	private final String theUrlType = "http://";
	private final String theByXpathCanadaRestriction = "//div[@id='choiceContainer']/div[2]";
	private final String theByXPathSwatchDescription = "//span[@id='swatch_description']";
	private final String theMainImage = "//*[@id='divMainImageContainer']";
	private final String theMainProduct = "//div[@id='divMainProductContainer']/div[2]";
	
	private final String thePageName = "PDP";	
  	private final String thePreviewPaneName = "ProductPreview";
  	private final String the8AddedToBagMessage = "8AddedToBag";
	
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	private ProductListingPage myProductListingObjs;
	private Header myHeaderObjs;
	private String testName;
	
	public ProductDetailPage(WebDriver theDriver,
			SeleniumEnvironment theEnvironment,
			ProductListingPage theProductListingObjs, Header theHeaderObjs,
		    String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myProductListingObjs = theProductListingObjs;
		myHeaderObjs = theHeaderObjs;
		testName = theTestName;
	}

	public String getTheByIdAddToBagBtn() {
		return theByIdAddToBagBtn;
	}

	public String getTheByIdAddToBagLink() {
		return theByIdAddToBagLink;
	}

	public String getTheMainProduct() {
		return theMainProduct;
	}

	public String getTheByXPathFirstSize() {
		return theByXPathFirstSize;
	}

	public String getTheByLinkMediumSize() {
		return theByLinkMediumSize;
	}

	public String getTheByXPathSecondSize() {
		return theByXPathSecondSize;
	}

	public String getTheByCSSProductPrice() {
		return theByCSSProductPrice;
	}

	public String getTheByXPathThirdSize() {
		return theByXPathThirdSize;
	}

	public String getTheByLinkTextXLargeSize() {
		return theByLinkTextXLargeSize;
	}

	public String getTheByLinkTextLargeSize() {
		return theByLinkTextLargeSize;
	}

	public String getTheByLinkText34By32Size() {
		return theByLinkText34By32Size;
	}

	public String getTheByLinkText36By32Size() {
		return theByLinkText36By32Size;
	}

	public String getTheByXpathSizeEleven() {
		return theByXpathSizeEleven;
	}

	public String getTheByXpathAddToBagButton() {
		return theByXpathAddToBagButton;
	}

	public String getTheByCSSAddToBagLink() {
		return theByCSSAddToBagLink;
	}

	public String getTheByOneXByBigLink() {
		return theByOneXByBigLink;
	}

	public String getTheFirstCrossSellItem() {
		return theFirstCrossSellItem;
	}

	public String getTheByIdSelectQuantity() {
		return theByIdSelectQuantity;
	}

	public String getTheProductSize() {
		return theProductSize;
	}

	public String getTheHelpLink() {
		return theHelpLink;
	}

	public String getTheQuantityFour() {
		return theQuantityFour;
	}

	public String getTheQuantityTwo() {
		return theQuantityTwo;
	}

	public String getTheByIDSelectQty() {
		return theByIDSelectQty;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheByXpathCanadaRestriction() {
		return theByXpathCanadaRestriction;
	}

	public String getTheByXPathSwatchDescription() {
		return theByXPathSwatchDescription;
	}

	public String getTheMainImage() {
		return theMainImage;
	}

	/*
	 * This method is a universal method to add products by price to the
	 * shopping bag that are located in sub categories of a section An example
	 * of this is Socks under Mens Accessories. This method is passed the
	 * Webdriver and Environmental objects, the product listing and header
	 * objects The method is also passed the tab to select the first and sub
	 * link to select and the price and quantity
	 */
	public void selectSubProductByPrice(
			String theTab, String theLink, String sublink, String thePrice,
			String theQuantity, String theProductTitle) throws InterruptedException {
		WebElement ce;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce.click();
		// WebElement home =
		// driver.findElement(By.cssSelector(myHeaderObjs.getTheByCSSLogo()));
		// Actions action = new Actions(driver);//simply my webdriver
		// action.moveToElement(home).perform();//move to list element that
		// needs to be hovered
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(sublink));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle + thePrice);
		String theSelectedItem = "";
		theSelectedItem = this.findWantedUSPrice(thePrice);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		System.out
				.println("THis is the selected item       " + theSelectedItem);
		ce = driver.findElement(By.xpath(theSelectedItem));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				thePrice);
		this.addQuantityToBag( theQuantity,	 thePrice);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag that are located in sub categories of a section An example
	 * of this is Socks under Mens Accessories. This method is passed the
	 * Webdriver and Environmental objects, the product listing and header
	 * objects The method is also passed the tab to select the first and sub
	 * link to select and the product and quantity
	 */
	public void selectSubProductByProduct(
			String theTab, String theLink, String sublink, String theProduct,
			String theQuantity) throws InterruptedException {
		WebElement ce;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
	
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		// WebElement home =
		// driver.findElement(By.cssSelector(myHeaderObjs.getTheByCSSLogo()));
		// Actions action = new Actions(driver);//simply my webdriver
		// action.moveToElement(home).perform();//move to list element that
		// needs to be hovered
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();
		
		myEnvironment.waitForPageLoaded();
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		ce = myEnvironment.waitForDynamicElement(
				By.linkText(sublink));
		ce.click();

		myEnvironment.waitForPageLoaded();
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				"Products");
		String theSelectedItem = "";
		theSelectedItem = this.findProduct(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);
		this.addQuantityToBag(theQuantity);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag This method is passed the Webdriver and Environmental
	 * objects, the product listing and header objects The method is also passed
	 * the tab to select the first link to select and the product and quantity
	 */
	public void selectProduct(String theTab, String theLink, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {

		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
		myEnvironment.waitForPageLoaded();

		ce = myEnvironment.waitForDynamicElement(
				By.id("emailFormFooterId"));
		ce.sendKeys("Stupid no dely on hover kills automation");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();
	
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		String theSelectedItem = "";
		theSelectedItem = this.findProduct(theProduct);
		System.out.println("This is the found product:           "
				+ theSelectedItem);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSelectQuantity()));
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='divPinterest']/span/a"));
		ce.click();
	
		myEnvironment.handleMultipleWindows( "Pinterest");
		Thread.sleep(5000);
		
		driver.close();
		
		Thread.sleep(5000);
		
	//	driver.switchTo().defaultContent();
		
		myEnvironment.handleMultipleWindows(theProductTitle);
		
		Thread.sleep(5000);
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);

		this.addQuantityToBag(theQuantity);
	}

	public void selectProductByHover(String theTab, String theLink, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {
		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(ce).perform();// move to list element
														// that needs to be
														// hovered

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		// action = new Actions(driver);//simply my webdriver
		// action.moveToElement(ce).perform();//move to list element
		// that needs to be hovered
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		String theSelectedItem = "";

		theSelectedItem = this.findProduct(theProduct);
		System.out.println("This is the found product:           "
				+ theSelectedItem);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		// Thread.Sleep(1000);
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);
		this.addQuantityToBag(theQuantity);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag by the products price This method is passed the Webdriver
	 * and Environmental objects, the product listing and header objects The
	 * method is also passed the tab to select the first link to select and the
	 * price and quantity
	 */
	public void selectProductByPrice(
			String theTab, String theLink, String thePrice, String theQuantity,
			 String theProductTitle)
			throws InterruptedException {
		
		WebElement ce = myEnvironment.fluentWait(
				By.xpath(theTab));
		ce.click();

		ce = myEnvironment.fluentWait(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(ce).perform();// move to list element
														// that needs to be
														// hovered
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		
				
		String thePriceWithoutSpaces = thePrice;
		thePriceWithoutSpaces = thePriceWithoutSpaces.replace(" ", "_");
		System.out.println("this is the Price with a underscore included .                 "  +    thePriceWithoutSpaces);
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle + thePriceWithoutSpaces);
		String theSelectedItem = "";
		theSelectedItem = this.findWantedUSPrice(thePrice);
		System.out.println("This is the selected item:        "
				+ theSelectedItem);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce = myEnvironment.fluentWait(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProductTitle + thePriceWithoutSpaces);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		this.addQuantityToBag( theQuantity,	 thePrice);
	}

	/*
	 * This method finds a product by the product id that is located in the
	 * src attribute of an element.  This method is passed the
	 * product id of the product that is going to be selected.
	 */
	public String findProduct(String theProduct) {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
		WebElement ce;
		Integer pc = 20;
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='imgContainer_" + 0 + "']/div[2]/div"));
		if (myEnvironment.isElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			ce.click();
			// driver.FindElement(By.XPath(myProductListingObjs.ShowAllLink)).Click();
			pc = this.getProductCount(); 	
		}
		
		String Product = "";
		String thumbNailStart = "//div[@id='imgThumb_";
		String thumbNailEnd = "']/a/img";
		for (int i = 0; i < pc; i++) {
			ce = myEnvironment.waitForClickableElement(
					By.xpath(thumbNailStart + i + thumbNailEnd));
			Product = myProductListingObjs.pullProductCodeFromThumbNail(
					thumbNailStart, thumbNailEnd, i);
			if (Product.contains(theProduct)) {
				Product = thumbNailStart + i + thumbNailEnd;
				return Product;
			}
		}
		return Product;
	}

	public String findWantedUSPrice( String theWantedPrice) {
		String theProduct = "";
		WebElement ce;
		Integer pc = 20;
	
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='imgContainer_" + 0 + "']/div[2]/div"));
		if (myEnvironment.isElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			ce.click();
			// driver.FindElement(By.XPath(myProductListingObjs.ShowAllLink)).Click();
			pc = this.getProductCount(); 	
		}
		
			
		
		for (int i = 0; i < pc; i++) {
			String Price = "";
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i + "']/div[2]/div"));
			// myEnvironment.myWaitForWDElement(driver,
			// By.XPath("//div[@id='imgContainer_" + i + "']/div[2]/div"));

			Price = ce.getText();
			Price = Price.trim();
			Assert.assertTrue(Price.contains("$") || Price.contains("GBP"));

			System.out.println("This is the price:       " + Price);
			System.out.println("This is the wanted price      "
					+ theWantedPrice);
			if (Price.contains(theWantedPrice)) {
				theProduct = "//div[@id='cat_list_title_" + i + "']/a";
				return theProduct;
			}
		}
		return theProduct;
	}

	/*
	 * This method adds products to the shopping bag This is passed the normal
	 * objects and the quantity of the object that is to be added to the
	 * shopping bag. For some reason find element combined with the click method
	 * can throw a timed out error. in some instances its needed to put the
	 * element into the IWebElement object and then find it sleep for 205 ms and
	 * then click it Hopefully this bug will be fixed soon or I get an upgrade
	 * on my machine to execute the methods(says this happens on slower
	 * machines). I have done this in places I do not think its required. It is
	 * Required presently in situations like this one where the test is
	 * selecting a value from a drop down. After this it is necesssary to
	 * perfrom the break up that is described above.
	 */
	public void addQuantityToBag(String theQuantity,
			 String thePrice) throws InterruptedException {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
		WebElement ce;
		// IWebElement element;
		// WebDriverWait wait = new WebDriverWait(driver,
		// TimeSpan.FromSeconds(180));
		if (myEnvironment.isElementPresent(By.xpath(this.getTheProductSize()))) {
			for (int i = 0; i < 10; i++) {
				if (myEnvironment.isElementPresent(
						By.xpath("//div[2]/div[2]/div[" + i + "]/a"))) {
					ce = myEnvironment.waitForDynamicElement(
							By.xpath("//div[2]/div[2]/div[" + i + "]/a"));
					System.out.println(ce.getAttribute("style"));
					if (ce.getAttribute("style").contains(
							"rgb(51, 103, 153)")
							|| ce.getAttribute("style").contains(
									"#336799")) {
						System.out
								.println("We are at the click so the size should be selected.");
						ce.click();

						Thread.sleep(myEnvironment.getThe_Default_Sleep());

						// TODO Auto-generated catch block

						break;
					}
				}
			}
			// This is code to add multiples of the same items to the cart at
			// the same time.
			// ce =
			if (theQuantity == "8") {
				ce = driver.findElement(By.id(this
						.getTheByIdSelectQuantity()));
				ce = ce.findElement(By
						.cssSelector("option[Value='8']"));
				// Thread.Sleep(myEnvironment.DefaultSleep);
				ce.click();
				// Thread.Sleep(myEnvironment.DefaultSleep);
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));

				Thread.sleep(myEnvironment.getThe_Default_Sleep());

				myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
						"PDP", thePrice);

				Thread.sleep(myEnvironment.getThe_Default_Sleep());

				ce.click();

				Thread.sleep(myEnvironment.getThe_Default_Sleep());

			} else {
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				Select clickThis = new Select(ce);
				Thread.sleep(1000);
				clickThis.selectByValue(theQuantity);
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				// myEnvironment.TakeScreenShot( theBrowser, theTest,
				// "PDP",
				// thePrice);
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
			}
		} else {
			// This is code to add multiples of the same items to the cart at
			// the same time.
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdSelectQuantity()));
			Select clickThis = new Select(ce);
			clickThis.selectByValue(theQuantity);
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdAddToBagLink()));
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			// myEnvironment.TakeScreenShot( theBrowser, theTest, "PDP",
			// thePrice);
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			ce.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		}
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
	}

	/*
	 * This method adds products to the shopping bag This is passed the normal
	 * objects and the quantity of the object that is to be added to the
	 * shopping bag. For some reason find element combined with the click method
	 * can throw a timed out error. in some instances its needed to put the
	 * element into the IWebElement object and then find it sleep for 205 ms and
	 * then click it Hopefully this bug will be fixed soon or I get an upgrade
	 * on my machine to execute the methods(says this happens on slower
	 * machines). I have done this in places I do not think its required. It is
	 * Required presently in situations like this one where the test is
	 * selecting a value from a drop down. After this it is necesssary to
	 * perfrom the break up that is described above.
	 */
	public void addQuantityToBag(String theQuantity)
			throws InterruptedException {
		WebElement ce;

		if (myEnvironment.isElementPresent(By.xpath(this.getTheProductSize()))) {
			for (int i = 0; i < 10; i++) {
				if (myEnvironment.isElementPresent(
						By.xpath("//div[2]/div[2]/div[" + i + "]/a"))) {
					ce = myEnvironment.waitForDynamicElement(
							By.xpath("//div[2]/div[2]/div[" + i + "]/a"));
					System.out.println(ce.getAttribute("style"));
					if (ce.getAttribute("style").contains(
							"rgb(51, 103, 153)")
							|| ce.getAttribute("style").contains(
									"#336799")) {
						System.out
								.println("We are at the click so the size should be selected.");
						ce.click();
						Thread.sleep(myEnvironment.getThe_Default_Sleep());
						break;
					}
				}
			}
			// This is code to add multiples of the same items to the cart at
			// the same time.
			if (theQuantity == "8") {
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				Select clickThis = new Select(ce);
				clickThis.selectByValue(theQuantity);
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
						"PDP", "8AddedToBag");
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				ce.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());

			} else {
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				// Thread.Sleep(myEnvironment.DefaultSleep);
				Select clickThis = new Select(ce);
				// Thread.Sleep(myEnvironment.DefaultSleep);
				clickThis.selectByValue(theQuantity);
				 Thread.sleep(myEnvironment.getThe_Special_Sleep());
				 
				 
				//	ce = myEnvironment.waitForDynamicElement(
				//			By.id(this.getTheByIdAddToBagBtn()));
				//	ce.click();
				 
					ce = myEnvironment.waitForDynamicElement(
							By.id(this.getTheByIdAddToBagLink()));
					ce.click();
				// Thread.Sleep(myEnvironment.DefaultSleep);
			
				// myEnvironment.TakeScreenShot( theBrowser, theTest,
				// "PDP", "");
			//	Thread.sleep(myEnvironment.getThe_Special_Sleep());
				
				
			
			
			}
		} else {
			// This is code to add multiples of the same items to the cart at
			// the same time.
		//	ce = myEnvironment.waitForDynamicElement(
		//			By.id("BVRRDisplayContentSelectBVFrameID"));
		//	Select clickThis = new Select(ce);
		//	clickThis.selectByValue("Newest");
			
			
		
			
			
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdSelectQuantity()));
		    Select clickThis = new Select(ce);
			clickThis.selectByValue(theQuantity);
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathAddToBagButton()));
			
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			ce.click();
		//	ce = myEnvironment.waitForDynamicElement(
		//			By.id(this.getTheByIdAddToBagBtn()));
		//	ce.click();
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
		}

		Thread.sleep(myEnvironment.getThe_Special_Sleep());

	}

	
	public Integer getProductCount(){
		WebElement ce;
		ce = myEnvironment.waitForDynamicElement(
				By.id("divItemsNav"));
		String productListingCount = ce.getText();
		
		String productCount = productListingCount.substring(productListingCount.lastIndexOf("f") + 2);
		
		productCount.trim();
		
		return Integer.parseInt(productCount);
	}
	
	public String getThePageName() {
		return thePageName;
	}

	public String getThePreviewPaneName() {
		return thePreviewPaneName;
	}

	public String getThe8AddedToBagMessage() {
		return the8AddedToBagMessage;
	}

}
