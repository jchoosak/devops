package TommyBahamaRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignInPage {
	private final String signInLink = "signin";
	// private final String signInLink = "Sign In or Register";

	private final String byXPathUserNameObj = "//input[@id='sign-in-email']";
	private final String byXpathPasWrd = "//input[@id='sign-in-enter-password']";
	private final String byXPathSignInButton = "//input[@id='account-sign-in-btn']";
	private final String theCanadaUsername = "ecommtest5@tommybahama.com";
	private final String theSBUsername = "ecommtest4@tommybahama.com";
	private final String theEndToEndUsername = "ecommtest3@tommybahama.com";
	private final String theWAUsername = "ecommtest@tommybahama.com";
	private final String theCALUsername = "ecommtest2@tommybahama.com";
	private final String theUsername = "Jack.West@TommyBahama.com";
	private final String theCanadaFourUserName = "ecommtest4@tommybahama.com";
	private final String theWAGCUserName = "ecommtest7@tommybahama.com";
	private final String theUSSplitTenderUserName = "ecommtest8@tommybahama.com";
	private final String theCanadaNineUserName = "ecommtest9@tommybahama.com";
	private final String theCanadaTenUserName = "ecommtest10@tommybahama.com";
	private final String thePassword = "testing123";
	private final String theContinueAsGuestBtn = "//a[contains(text(),'CONTINUE AS A GUEST')]";

	private final String theOutletUsernameId = "j_username";
	private final String theCSCOutletUsernameId = "j_username";
	private final String theOutletPasswordId = "j_password";
	private final String theCSCOutletPasswordId = "j_password";
	private final String theCSCOutletLoginButton = "td.z-button-cm";
	// private final String default_wait = "600000";
	
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	private Header myHeader;
	private String testName;
	
	private final String thePageName = "SignInPage";
	
	public SignInPage(WebDriver theDriver, SeleniumEnvironment theEnvironment,
			Header theHeader,String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myHeader = theHeader;
		testName = theTestName;
		
	}

	public SignInPage(WebDriver theDriver, SeleniumEnvironment theEnvironment
			,String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;	
		testName = theTestName;		
	}
	
	public String getTheCanadaFourUserName() {
		return theCanadaFourUserName;
	}

	public String getTheWAGCUserName() {
		return theWAGCUserName;
	}

	public String getTheUSSplitTenderUserName() {
		return theUSSplitTenderUserName;
	}

	public String getTheCanadaNineUserName() {
		return theCanadaNineUserName;
	}

	public String getTheCanadaTenUserName() {
		return theCanadaTenUserName;
	}

	public String getSignInLink() {
		return signInLink;
	}

	public String getByXPathUserNameObj() {
		return byXPathUserNameObj;
	}

	public String getByXpathPasWrd() {
		return byXpathPasWrd;
	}

	public String getByXPathSignInButton() {
		return byXPathSignInButton;
	}

	public String getTheCanadaUsername() {
		return theCanadaUsername;
	}

	public String getTheSBUsername() {
		return theSBUsername;
	}

	public String getTheEndToEndUsername() {
		return theEndToEndUsername;
	}

	public String getTheWAUsername() {
		return theWAUsername;
	}

	public String getTheCALUsername() {
		return theCALUsername;
	}

	public String getTheUsername() {
		return theUsername;
	}

	public String getThePassword() {
		return thePassword;
	}

	public String getTheContinueAsGuestBtn() {
		return theContinueAsGuestBtn;
	}

	public void signIn(String theUsername, String password) {
		WebElement currentElement;

		System.out.println("This is the browser:    "   + myEnvironment.getBrowser());
		System.out.println("This is the current url:     " + driver.getCurrentUrl());
		
		if (myEnvironment.getBrowser().contains("IE9")
				& driver.getCurrentUrl().contains(myEnvironment.getProdTest())) {
			System.out.println("We are in the if statement!!!!!!!!");
		driver.get("https://ecweb01/store/account/login_or_register.jsp");
			driver.get("javascript:document.getElementById('overridelink').click();");
		} else {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheSignIn()));
			currentElement.click();
		}
		// if(myEnvironment.IsElementPresent(By.id("overridelink"), driver))
		// currentElement.findElement(By.id("overridelink")).click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getByXPathUserNameObj()));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, this.getThePageName(),
				"");
		// myEnvironment.hoverToBody(driver, By.xpath("//body"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(getByXPathUserNameObj()));
		currentElement.sendKeys(theUsername);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(getByXpathPasWrd()));
		currentElement.sendKeys(thePassword);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getByXPathSignInButton()));

		currentElement.click();
		// Thread.Sleep(10000);
	}
	
	public void signInOutlet(String theUsername, String password) {
		WebElement currentElement;

	
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheOutletUsernameId()));
	//	myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "SignInPage",
	//			"Blank");
		// myEnvironment.hoverToBody(driver, By.xpath("//body"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(getTheOutletUsernameId()));
		currentElement.sendKeys(theUsername);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(getTheOutletPasswordId()));
		currentElement.sendKeys(thePassword);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//button[@type='submit']"));

		currentElement.click();
		// Thread.Sleep(10000);
	}
	

	public String getThePageName() {
		return thePageName;
	}

	public String getTheOutletPasswordId() {
		return theOutletPasswordId;
	}

	public String getTheOutletUsernameId() {
		return theOutletUsernameId;
	}

	public String getTheCSCOutletUsernameId() {
		return theCSCOutletUsernameId;
	}

	public String getTheCSCOutletPasswordId() {
		return theCSCOutletPasswordId;
	}
	
	public void signInCSC(String username, String password) throws InterruptedException {
		WebElement currentElement;
      
	
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(this.getTheCSCOutletUsernameId()));
		currentElement.clear();
	//	myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "SignInPage",
	//			"Blank");
		// myEnvironment.hoverToBody(driver, By.xpath("//body"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(getTheCSCOutletUsernameId()));
		currentElement.sendKeys(username);
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(getTheCSCOutletPasswordId()));
		currentElement.clear();
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.name(getTheCSCOutletPasswordId()));
		currentElement.sendKeys(password);
		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "LogInPage", "");
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(getTheCSCOutletLoginButton()));

		currentElement.click();
		// Thread.Sleep(10000);
	}

	public String getTheCSCOutletLoginButton() {
		return theCSCOutletLoginButton;
	}


}
