package TommyBahamaRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class TraditionalGiftCard {
	private final String theByXPathShippingRestrictions = "//div[@id='divShippingRestrictions']";
	private final String theByXPathCardLink = "//div[@id='imgThumb_1']/a/img";
	private final String theByXPathTraditionalCardImageLink = "//div[@id='divCards2']/map/area[6]";
	private final String theTraditionalCardLink = "//td/ul/li[5]/a";
	private final String theByIdDropDown = "drop_down";
	private final String theByIdToInput = "gw_to_1";
	private final String theByIdFromInput = "gw_from_1";
	private final String theByIdMessageInput = "gw_message_1";
	private final String theByIdEmailInput = "gw_input_notify_recipient";

	private final String theByXPathAddToBagButton = "//img[@alt='Add to Bag']";

	// private final String theByLinkPolos = "Polos";
	// private final String theByLinkTextNewArrivals = "New Arrivals";
	public String getTheByXPathShippingRestrictions() {
		return theByXPathShippingRestrictions;
	}

	public String getTheByXPathCardLink() {
		return theByXPathCardLink;
	}

	public String getTheByXPathTraditionalCardImageLink() {
		return theByXPathTraditionalCardImageLink;
	}

	public String getTheTraditionalCardLink() {
		return theTraditionalCardLink;
	}

	public String getTheByIdDropDown() {
		return theByIdDropDown;
	}

	public String getTheByIdToInput() {
		return theByIdToInput;
	}

	public String getTheByIdFromInput() {
		return theByIdFromInput;
	}

	public String getTheByIdMessageInput() {
		return theByIdMessageInput;
	}

	public String getTheByIdEmailInput() {
		return theByIdEmailInput;
	}

	public String getTheByXPathAddToBagButton() {
		return theByXPathAddToBagButton;
	}

	public void addTraditionalCard(SeleniumEnvironment myEnvironment,
			WebDriver driver, Header myHeaderObjs, Men myMenObjs,
			String theTest, String theName, String theBrowser)
			throws InterruptedException {
		WebElement currentElement;
		// Add the Gift Card
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		currentElement.click();
		myEnvironment.waitForTitle("Men's");
		//Thread.sleep(myEnvironment.getThe_D_Sleep());

		
	//	if (myEnvironment.getBrowser().contains("IE9")
	//			& driver.getCurrentUrl().contains("test")) {
			// Thread.sleep(myEnvironment.getThe_Special_Sleep());
	//		currentElement = myEnvironment.waitForDynamicElement(
	//				By.xpath("//li[3]/ul/li[5]/a"));
	//			currentElement.click();
	///	} else {
			// Thread.sleep(myEnvironment.getThe_Special_Sleep());
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(myMenObjs.getTheByXPathCreditCardsLink()));

			currentElement.click();
	//	}

		// Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myMenObjs.getTheByXPathCreditCardsLink()));

		currentElement.click();
	
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXPathTraditionalCardImageLink()));
		String linkUrl = currentElement.getAttribute("href");
		System.out.println("this is the url for the TGC       " + linkUrl);
		driver.get(linkUrl);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheByXPathCardLink()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdToInput()));
		currentElement.sendKeys("Kalli");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdFromInput()));
		currentElement.sendKeys("Tommy Bahama");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdMessageInput()));
		currentElement.sendKeys("For All The Extra Work You Do!");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdEmailInput()));
		currentElement.sendKeys("jack.west@tommybahama.com");
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				theTest, theName, "");
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheByXPathAddToBagButton()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdQuickViewContent()));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
	}
}
