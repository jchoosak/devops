package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class StoresAndRestaurantsPage {

	private final List<String> theStoresAndRestaurantsLinks = new ArrayList<String>();

	

	private final String theByXPathSearchForTommyBahama = "//a[contains(text(),'Search for Tommy Bahama')]";
	private final String theByXPathNewsAndEvents = "(//a[contains(text(),'News & Events')])[2]";
	private final String theByXPathComingSoon = "(//a[contains(text(),'Coming Soon')])[2]";
	private final String theByXPathGroupAndEventDining = "//a[contains(text(),'Group & Event Dining')]";
	private final String theByXPathNationalSales = "(//a[contains(text(),'National Sales')])[2]";
	private final String theByXPathUnitedStatesLocations = "//a[contains(text(),'U.S. Locations')]";
	private final String theByXPathInterNationalLocations = "//a[contains(text(),'International Locations')]";
	private final String theByXPathHomeStore = "//a[contains(text(),'Home Store Newport Beach')]";
	private final String theByXPathAustralia = "(//a[contains(text(),'Australia')])[2]";
	private final String theByXPathCanada = "(//a[contains(text(),'Canada')])[2]')]";
	private final String theByXPathJupiter = "//li[12]/ul/li/a";
	private final String theByXPathLagunaBeach = "(//a[contains(text(),'Laguna Beach, CA')])[2]";
	private final String theByXPathMacau = "(//a[contains(text(),'Macau')])[2]";
	private final String theByXPathSingapore = "(//a[contains(text(),'Singapore')])[2]";
	private final String theByXPathHongKong = "(//a[contains(text(),'Hong Kong')])[2]";
	private final String theByXPathJapan = "(//a[contains(text(),'Japan')])[3]";	
	private final String theByXPathLasVegas = "(//a[contains(text(),'Las Vegas, NV')])[2]";
	private final String theByXPathMaunaLani = "(//a[contains(text(),'Mauna Lani, HI')])[2]";
	private final String theByXPathNaples = "(//a[contains(text(),'Naples, FL')])[2]";
	private final String theByXPathNewYork = "//a[contains(text(),'New York City, NY')]";
	private final String theByXPathNewportBeach = "//a[contains(text(),'Newport Beach, CA')]";
	private final String theByXPathOrlando = "(//a[contains(text(),'Orlando, FL')])[2]";	
	private final String theByXPathPalmDesert = "(//a[contains(text(),'Palm Desert, CA')])[2]";	
	private final String theByXPathSandestin = "(//a[contains(text(),'Sandestin, FL')])[2]";
	private final String theByXPathSarasota = "(//a[contains(text(),'Sarasota, FL')])[2]";
	private final String theByXPathScottsdale = "(//a[contains(text(),'Scottsdale, AZ')])[2]";
	private final String theByXPathWailea = "(//a[contains(text(),'Wailea, HI')])[2]";
	private final String theByXPathWoodlands = "(//a[contains(text(),'The Woodlands, TX')])[2]";
	private final String theByXPathTokyo = "(//a[contains(text(),'Tokyo, Japan')])[2]";
	
	public void CreateSAndRLinks() {
		theStoresAndRestaurantsLinks.add(this.theByXPathSearchForTommyBahama);
		theStoresAndRestaurantsLinks.add(this.theByXPathAustralia);
		theStoresAndRestaurantsLinks.add(this.theByXPathCanada);
		theStoresAndRestaurantsLinks.add(this.theByXPathComingSoon);
		theStoresAndRestaurantsLinks.add(this.theByXPathGroupAndEventDining);
		theStoresAndRestaurantsLinks.add(this.theByXPathHomeStore);
		theStoresAndRestaurantsLinks.add(this.theByXPathHongKong);
		theStoresAndRestaurantsLinks.add(this.theByXPathInterNationalLocations);
		theStoresAndRestaurantsLinks.add(this.theByXPathJapan);
		theStoresAndRestaurantsLinks.add(this.theByXPathJupiter);
		theStoresAndRestaurantsLinks.add(this.theByXPathLagunaBeach);
		theStoresAndRestaurantsLinks.add(this.theByXPathLasVegas);
		theStoresAndRestaurantsLinks.add(this.theByXPathMacau);
		theStoresAndRestaurantsLinks.add(this.theByXPathMaunaLani);
		theStoresAndRestaurantsLinks.add(this.theByXPathNaples);
		theStoresAndRestaurantsLinks.add(this.theByXPathNationalSales);
		theStoresAndRestaurantsLinks.add(this.theByXPathNewportBeach);
		theStoresAndRestaurantsLinks.add(this.theByXPathNewsAndEvents);
		theStoresAndRestaurantsLinks.add(this.theByXPathNewYork);
		theStoresAndRestaurantsLinks.add(this.theByXPathOrlando);
		theStoresAndRestaurantsLinks.add(this.theByXPathPalmDesert);
		theStoresAndRestaurantsLinks.add(this.theByXPathSandestin);
		theStoresAndRestaurantsLinks.add(this.theByXPathSarasota);
		theStoresAndRestaurantsLinks.add(this.theByXPathScottsdale);
		theStoresAndRestaurantsLinks.add(this.theByXPathSingapore);
		theStoresAndRestaurantsLinks.add(this.getTheByXPathTokyo());
		theStoresAndRestaurantsLinks.add(this.getTheByXPathUnitedStatesLocations());
		theStoresAndRestaurantsLinks.add(this.getTheByXPathWailea());
		theStoresAndRestaurantsLinks.add(this.getTheByXPathWoodlands());
	}
	
	
	
	public List<String> getTheStoresAndRestaurantsLinks() {
		return theStoresAndRestaurantsLinks;
	}

	public String getTheByXPathSearchForTommyBahama() {
		return theByXPathSearchForTommyBahama;
	}



	public String getTheByXPathNewsAndEvents() {
		return theByXPathNewsAndEvents;
	}



	public String getTheByXPathComingSoon() {
		return theByXPathComingSoon;
	}



	public String getTheByXPathGroupAndEventDining() {
		return theByXPathGroupAndEventDining;
	}



	public String getTheByXPathNationalSales() {
		return theByXPathNationalSales;
	}



	public String getTheByXPathUnitedStatesLocations() {
		return theByXPathUnitedStatesLocations;
	}



	public String getTheByXPathInterNationalLocations() {
		return theByXPathInterNationalLocations;
	}



	public String getTheByXPathHomeStore() {
		return theByXPathHomeStore;
	}



	public String getTheByXPathAustralia() {
		return theByXPathAustralia;
	}



	public String getTheByXPathCanada() {
		return theByXPathCanada;
	}



	public String getTheByXPathHongKong() {
		return theByXPathHongKong;
	}



	public String getTheByXPathJapan() {
		return theByXPathJapan;
	}



	public String getTheByXPathMacau() {
		return theByXPathMacau;
	}



	public String getTheByXPathJupiter() {
		return theByXPathJupiter;
	}



	public String getTheByXPathSingapore() {
		return theByXPathSingapore;
	}



	public String getTheByXPathLagunaBeach() {
		return theByXPathLagunaBeach;
	}



	public String getTheByXPathLasVegas() {
		return theByXPathLasVegas;
	}



	public String getTheByXPathMaunaLani() {
		return theByXPathMaunaLani;
	}



	public String getTheByXPathNaples() {
		return theByXPathNaples;
	}



	public String getTheByXPathNewYork() {
		return theByXPathNewYork;
	}



	public String getTheByXPathNewportBeach() {
		return theByXPathNewportBeach;
	}



	public String getTheByXPathOrlando() {
		return theByXPathOrlando;
	}



	public String getTheByXPathPalmDesert() {
		return theByXPathPalmDesert;
	}



	public String getTheByXPathSandestin() {
		return theByXPathSandestin;
	}



	public String getTheByXPathSarasota() {
		return theByXPathSarasota;
	}



	public String getTheByXPathScottsdale() {
		return theByXPathScottsdale;
	}



	public String getTheByXPathWailea() {
		return theByXPathWailea;
	}



	public String getTheByXPathWoodlands() {
		return theByXPathWoodlands;
	}



	public String getTheByXPathTokyo() {
		return theByXPathTokyo;
	}
}
