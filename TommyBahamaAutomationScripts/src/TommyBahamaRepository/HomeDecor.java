package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class HomeDecor {

	private List<String> theHomeDecorLinkList = new ArrayList<String>();

	private List<String> theHomeDecorTumblersList = new ArrayList<String>();
	private List<String> theHomeDecorPetsList = new ArrayList<String>();
	private List<String> theHomeDecorShopByScentList = new ArrayList<String>();
	private List<String> theHomeDecorViewAllBeddingList = new ArrayList<String>();
	private List<String> theHomeDecorViewAllScentsList = new ArrayList<String>();
	private List<String> theHomeDecorShopByCollectionList = new ArrayList<String>();
	private List<String> theHomeDecorShopBySizeList = new ArrayList<String>();


	private List<String> theHomeDecorViewAllBeddingSetList = new ArrayList<String>();

	private final String theByXPathShopByScent = "(//a[contains(text(),'Shop by Scent')])[2]";
	private final String theByXPath25LittleLuxuries = "//a[contains(text(),'25 Little Luxuries')]";
	private final String theByXPathPouredCandles = "//a[contains(text(),'Poured Candles')]";
	private final String theByXPathPillarCandleholders = "(//a[contains(text(),'Pillars & Candleholders')])[2]";
	private final String theByXPathDiffusers = "//a[contains(text(),'Diffusers & Room Spray')]";
	private final String theByLinkDiffusers = "Diffusers & Room Spray";
	private final String theByXPathCigarWare = "(//a[contains(text(),'Cigarware')])[2]";
	private final String theByXPathGolf = "//a[contains(text(),'Golf')]";
	private final String theByXPathTech = "(//a[contains(text(),'Tech Cases & Covers')])[2]";
	private final String theByXPathHomeAccessories = "(//a[contains(text(),'Home Accessories')])[2]";
	private final String theByXPathGlasswareBar = "(//a[contains(text(),'Glassware & Bar')])[2]";
	private final String theByXPathDinnerware = "(//a[contains(text(),'Dinnerware')])[2]";
	private final String theByXPathTableLinens = "(//a[contains(text(),'Table Linens')])[2]";
	private final String theByXPathOutDoorEntertaining = "(//a[contains(text(),'Outdoor Entertaining')])[2]";
	private final String theByXPathViewAllBath = "(//a[contains(text(),'View All Bath')])[2]";
	private final String theByXPathNewArrivals = "(//a[contains(text(),'New Arrivals')])[7]";
	private final String theByXPathGuestFavs = "(//a[contains(text(),'Guest Favorites')])[5]";
	private final String theByXPathCookbook = "(//a[contains(text(),'Our Cookbook')])[2]";
	private final String theByXPathTumblers = "//a[contains(text(),'Tervis Tumbler� Collection')]";
    private final String theByXPathChairsAndUmbrellas = "(//a[contains(text(),'Chairs & Umbrellas')])[2]";
    private final String theByXPathBagsAndTowels = "//a[contains(text(),'Bags & Towels')]";
    private final String theByXPathLongAndPaddleBoards = "//a[contains(text(),'Longboards & Paddleboards')]";
    private final String theByXPathBikes = "//a[contains(text(),'Tommy Bahama Bikes')";
	private final String theByXPathPillarsAndCandleHolders = "//a[contains(text(),'Pillars & Candleholders')]";
	private final String theByXPathBooksAndGames = "(//a[contains(text(),'Books & Games')])[2]";
	private final String theByXPathKitchenLinens = "(//a[contains(text(),'Kitchen Linens')])[2]";
	private final String theByXPathShopBySize = "//a[contains(text(),'Shop by Size')]";
	private final String theByXPathViewAllBeachGear = "(//a[contains(text(),'View All Beach Gear')])[2]";	
	private final String theByXPathPalmGlassware = "//a[contains(text(),'Etched Palm Glassware')]";
	private final String theByXPathIslandValues = "(//a[contains(text(),'Island Values')])[3]";
	private final String theByXPathPets = "//a[contains(text(),'Tommy Bahama Pet')]";
	private final String theByXPathViewAllGiftsDecor = "//li[@id='leftnav_on']/a";
	private final String theByXPathViewAllTableTop = "//li[12]/ul/li/a";
	private final String theByXPathViewAllBeddingBath = "(//a[contains(text(),'View All Bedding')])[2]";
	private final String theByXPathViewAllCandlesAndFragrance = "(//a[contains(text(),'View All Bedding')])[2]";
	private final String theByXPathShopByCollection = "(//a[contains(text(),'Shop Bedding Sets')])[2]";
	private final String theByXPathViewAll = "(//a[contains(text(),'View All')])[14]";
	private final String theByXPathGiftssForDad = "//li[2]/a/span";
	
	private final String theByLinkTextNewArrivals = "New Arrivals";
	private final String theByLink16OunceLink = "16 ounce";
	private final String theByLink24OunceLink = "24 ounce";
	private final String theByLinkLeashesAndCollars = "Leashes & Collars";
	private final String theByLinkToysAndAccessories = "Toys & Accessories";
	private final String theByLinkFeedersAndBowels = "Feeders & Bowls";
	private final String theByLinkIslandBlendLink = "Island Blend";
	private final String theByLinkMauiMangoLink = "Maui Mango";
	private final String theByLinkPineappleCilantroLink = "Pineapple Cilantro";
	private final String theByLinkCoconutRumLink = "Coconut Rum";
	private final String theByLinkPomegranateLycheeLink = "Pomegranate Lychee";
	private final String theByLinkSummerEscapeLink = "Summer Escape";
	private final String theByLinkParadisePalmLink = "Paradise Palm";
	private final String theByLinkSurfsideStripeLink = "Surfside Stripe";
	private final String theByLinkLagunaRidgeLink = "Laguna Ridge";
	private final String theByLinkSouthernBreezeLink = "Southern Breeze";
	private final String theByLinkBahamianBreezeLink = "Bahamian Breeze";
	private final String theByLinkBambooBreezeLink = "Bamboo Breeze";
	private final String theByLinkIslandBotanicalLink = "Island Botanical";
	private final String theByLinkHavanaGardenlLink = "Havana Garden";
	private final String theByLinkEmbroideredTrellislLink = "Embroidered Trellis";
	private final String theByLinkCatalinallLink = "Catalinal";
	private final String theByLinkOrangeCayLink = "Orange Cay";
	private final String theByLinkMontaukDrifterLink = "Montauk Drifter";
    private final String theByLinkGolf = "Golf";
	private final String theByLinkQueenLink = "Queen";
	private final String theByLinkKingLink = "King";
	private final String theByLinkCaliforniaKingLink = "California King";
	private final String theByLinkViewAllLink = "View All";
	
	
	
	private final String thePageName = "HomeDecor";

	public void createShopByCollectionList() {
		
		theHomeDecorShopByCollectionList.add(getTheByLinkOrangeCayLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkParadisePalmLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkSouthernBreezeLink());		
		theHomeDecorShopByCollectionList.add(getTheByLinkLagunaRidgeLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkBambooBreezeLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkHavanaGardenlLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkEmbroideredTrellislLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkCatalinallLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkSurfsideStripeLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkMontaukDrifterLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkBahamianBreezeLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkIslandBotanicalLink());		
	}
	
	public void createShopBySizeList() {		
    	theHomeDecorShopBySizeList.add(getTheByLinkQueenLink());
		theHomeDecorShopBySizeList.add(getTheByLinkKingLink());
		theHomeDecorShopBySizeList.add(getTheByLinkCaliforniaKingLink());					
	}
	
	public void createShopPetList() {
		theHomeDecorShopByCollectionList.add(this.getTheByLinkParadisePalmLink());
		theHomeDecorShopByCollectionList.add(getTheByLinkLeashesAndCollars());
		theHomeDecorShopByCollectionList.add(getTheByLinkToysAndAccessories());
		theHomeDecorShopByCollectionList.add(getTheByLinkFeedersAndBowels());	
	}

	public void createShopByScentList() {
		theHomeDecorShopByScentList.add(this.getTheByLinkViewAllLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkCoconutRumLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkPomegranateLycheeLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkIslandBlendLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkMauiMangoLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkPineappleCilantroLink());
	}

	public void createShopBeddingSetViewAllList() {
		theHomeDecorViewAllBeddingSetList.add(this
				.getTheByXPathViewAllBeddingBath());

	}

	public void createViewAllBeddingList() {
		theHomeDecorViewAllBeddingList.add(this.getTheByLinkQueenLink());
		theHomeDecorViewAllBeddingList.add(this.getTheByLinkKingLink());
		theHomeDecorViewAllBeddingList.add(this
				.getTheByLinkCaliforniaKingLink());
	}

	public void createHomeDecorTumblerList() {
		theHomeDecorTumblersList.add(this.getTheByLink16OunceLink());
		theHomeDecorTumblersList.add(this.getTheByLink24OunceLink());
	}

	public void createHomeDecorLinkList() {		
		theHomeDecorLinkList.add(this.getTheByXPathBikes());		
		theHomeDecorLinkList.add(this.getTheByXPathNewArrivals());
		theHomeDecorLinkList.add(this.getTheByXPathGiftssForDad());		
		theHomeDecorLinkList.add(this.getTheByXPathPets());
		theHomeDecorLinkList.add(this.getTheByXPathTech());		
		theHomeDecorLinkList.add(this.getTheByXPathViewAllBeachGear());
		theHomeDecorLinkList.add(this.getTheByXPathChairsAndUmbrellas());
		theHomeDecorLinkList.add(this.getTheByXPathOutDoorEntertaining());
		theHomeDecorLinkList.add(this.getTheByXPathBagsAndTowels());
		theHomeDecorLinkList.add(this.getTheByXPathLongAndPaddleBoards());		
		theHomeDecorLinkList.add(this.getTheByXPathViewAllCandlesAndFragrance());
		theHomeDecorLinkList.add(this.getTheByXPathShopByScent());
		theHomeDecorLinkList.add(this.getTheByXPathPouredCandles());
		theHomeDecorLinkList.add(this.getTheByXPathPillarsAndCandleHolders());
		theHomeDecorLinkList.add(this.getTheByXPathDiffusers());		
		theHomeDecorLinkList.add(this.getTheByXPathViewAllGiftsDecor());
		theHomeDecorLinkList.add(this.getTheByXPathCigarWare());
		theHomeDecorLinkList.add(this.getTheByXPathGolf());
		theHomeDecorLinkList.add(this.getTheByXPathBooksAndGames());		
		theHomeDecorLinkList.add(this.getTheByXPathViewAllTableTop());
		theHomeDecorLinkList.add(this.getTheByXPathGlasswareBar());
		theHomeDecorLinkList.add(this.getTheByXPathPalmGlassware());
		theHomeDecorLinkList.add(this.getTheByXPathTumblers());		
		theHomeDecorLinkList.add(this.getTheByXPathDinnerware());
		theHomeDecorLinkList.add(this.getTheByXPathKitchenLinens());
		theHomeDecorLinkList.add(this.getTheByXPathTableLinens());
		theHomeDecorLinkList.add(this.getTheByXPathViewAllBeddingBath());		
		theHomeDecorLinkList.add(this.getTheByXPathShopBySize());	
		theHomeDecorLinkList.add(this.getTheByXPathViewAllBath());	
	}

	public List<String> getTheHomeDecorLinkList() {
		return theHomeDecorLinkList;
	}

	public List<String> getTheHomeDecorTumblersList() {
		return theHomeDecorTumblersList;
	}

	public List<String> getTheHomeDecorShopByScentList() {
		return theHomeDecorShopByScentList;
	}

	public List<String> getTheHomeDecorViewAllBeddingList() {
		return theHomeDecorViewAllBeddingList;
	}

	public List<String> getTheHomeDecorViewAllScentsList() {
		return theHomeDecorViewAllScentsList;
	}

	public List<String> getTheHomeDecorShopByCollectionList() {
		return theHomeDecorShopByCollectionList;
	}

	public List<String> getTheHomeDecorViewAllBeddingSetList() {
		return theHomeDecorViewAllBeddingSetList;
	}

	public List<String> getTheHomeDecorShopBySizeList() {
		return theHomeDecorShopBySizeList;
	}

	public String getTheByXPathShopByScent() {
		return theByXPathShopByScent;
	}

	public String getTheByXPath25LittleLuxuries() {
		return theByXPath25LittleLuxuries;
	}

	public String getTheByXPathPouredCandles() {
		return theByXPathPouredCandles;
	}

	public String getTheByXPathPillarCandleholders() {
		return theByXPathPillarCandleholders;
	}

	public String getTheByXPathDiffusers() {
		return theByXPathDiffusers;
	}

	public String getTheByLinkDiffusers() {
		return theByLinkDiffusers;
	}

	public String getTheByXPathCigarWare() {
		return theByXPathCigarWare;
	}

	public String getTheByXPathGolf() {
		return theByXPathGolf;
	}

	public String getTheByXPathHomeAccessories() {
		return theByXPathHomeAccessories;
	}

	public String getTheByXPathGlasswareBar() {
		return theByXPathGlasswareBar;
	}

	public String getTheByXPathDinnerware() {
		return theByXPathDinnerware;
	}

	public String getTheByXPathTableLinens() {
		return theByXPathTableLinens;
	}

	public String getTheByXPathOutDoorEntertaining() {
		return theByXPathOutDoorEntertaining;
	}

	public String getTheByXPathViewAllBath() {
		return theByXPathViewAllBath;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathGuestFavs() {
		return theByXPathGuestFavs;
	}

	public String getTheByXPathCookbook() {
		return theByXPathCookbook;
	}

	public String getTheByXPathTumblers() {
		return theByXPathTumblers;
	}

	public String getTheByXPathViewAllBeachGear() {
		return theByXPathViewAllBeachGear;
	}

	public String getTheByXPathPalmGlassware() {
		return theByXPathPalmGlassware;
	}

	public String getTheByXPathIslandValues() {
		return theByXPathIslandValues;
	}

	public String getTheByXPathViewAllGiftsDecor() {
		return theByXPathViewAllGiftsDecor;
	}

	public String getTheByXPathViewAllTableTop() {
		return theByXPathViewAllTableTop;
	}

	public String getTheByXPathViewAllBeddingBath() {
		return theByXPathViewAllBeddingBath;
	}

	public String getTheByXPathShopByCollection() {
		return theByXPathShopByCollection;
	}

	public String getTheByXPathViewAll() {
		return theByXPathViewAll;
	}

	public String getTheByLink16OunceLink() {
		return theByLink16OunceLink;
	}

	public String getTheByLink24OunceLink() {
		return theByLink24OunceLink;
	}

	public String getTheByLinkIslandBlendLink() {
		return theByLinkIslandBlendLink;
	}

	public String getTheByLinkMauiMangoLink() {
		return theByLinkMauiMangoLink;
	}

	public String getTheByLinkPineappleCilantroLink() {
		return theByLinkPineappleCilantroLink;
	}

	public String getTheByLinkSummerEscapeLink() {
		return theByLinkSummerEscapeLink;
	}

	public String getTheByLinkParadisePalmLink() {
		return theByLinkParadisePalmLink;
	}

	public String getTheByLinkSurfsideStripeLink() {
		return theByLinkSurfsideStripeLink;
	}

	public String getTheByLinkLagunaRidgeLink() {
		return theByLinkLagunaRidgeLink;
	}

	public String getTheByLinkSouthernBreezeLink() {
		return theByLinkSouthernBreezeLink;
	}

	public String getTheByLinkBahamianBreezeLink() {
		return theByLinkBahamianBreezeLink;
	}

	public String getTheByLinkIslandBotanicalLink() {
		return theByLinkIslandBotanicalLink;
	}

	public String getTheByLinkOrangeCayLink() {
		return theByLinkOrangeCayLink;
	}

	public String getTheByLinkQueenLink() {
		return theByLinkQueenLink;
	}

	public String getTheByLinkKingLink() {
		return theByLinkKingLink;
	}

	public String getTheByLinkCaliforniaKingLink() {
		return theByLinkCaliforniaKingLink;
	}

	public String getTheByLinkTextNewArrivals() {
		return theByLinkTextNewArrivals;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByLinkGolf() {
		return theByLinkGolf;
	}

	public String getTheByXPathGiftssForDad() {
		return theByXPathGiftssForDad;
	}

	public String getTheByXPathPets() {
		return theByXPathPets;
	}

	public String getTheByXPathTech() {
		return theByXPathTech;
	}

	public String getTheByXPathChairsAndUmbrellas() {
		return theByXPathChairsAndUmbrellas;
	}

	public String getTheByXPathBagsAndTowels() {
		return theByXPathBagsAndTowels;
	}

	public String getTheByXPathLongAndPaddleBoards() {
		return theByXPathLongAndPaddleBoards;
	}

	public String getTheByXPathBikes() {
		return theByXPathBikes;
	}

	public String getTheByXPathViewAllCandlesAndFragrance() {
		return theByXPathViewAllCandlesAndFragrance;
	}

	public String getTheByXPathPillarsAndCandleHolders() {
		return theByXPathPillarsAndCandleHolders;
	}

	public String getTheByXPathBooksAndGames() {
		return theByXPathBooksAndGames;
	}

	public String getTheByXPathKitchenLinens() {
		return theByXPathKitchenLinens;
	}

	public String getTheByXPathShopBySize() {
		return theByXPathShopBySize;
	}

	public String getTheByLinkLeashesAndCollars() {
		return theByLinkLeashesAndCollars;
	}

	public String getTheByLinkToysAndAccessories() {
		return theByLinkToysAndAccessories;
	}

	public String getTheByLinkFeedersAndBowels() {
		return theByLinkFeedersAndBowels;
	}

	public List<String> getTheHomeDecorPetsList() {
		return theHomeDecorPetsList;
	}

	public void setTheHomeDecorPetsList(List<String> theHomeDecorPetsList) {
		this.theHomeDecorPetsList = theHomeDecorPetsList;
	}

	public String getTheByLinkCoconutRumLink() {
		return theByLinkCoconutRumLink;
	}

	public String getTheByLinkPomegranateLycheeLink() {
		return theByLinkPomegranateLycheeLink;
	}

	public String getTheByLinkViewAllLink() {
		return theByLinkViewAllLink;
	}

	public String getTheByLinkBambooBreezeLink() {
		return theByLinkBambooBreezeLink;
	}

	public String getTheByLinkHavanaGardenlLink() {
		return theByLinkHavanaGardenlLink;
	}

	public String getTheByLinkEmbroideredTrellislLink() {
		return theByLinkEmbroideredTrellislLink;
	}

	public String getTheByLinkCatalinallLink() {
		return theByLinkCatalinallLink;
	}

	public String getTheByLinkMontaukDrifterLink() {
		return theByLinkMontaukDrifterLink;
	}

}
