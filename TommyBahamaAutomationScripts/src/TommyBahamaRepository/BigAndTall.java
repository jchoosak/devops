package TommyBahamaRepository;

import java.util.ArrayList;
import java.util.List;

public class BigAndTall {
	private List<String> theBigAndTallLinks = new ArrayList<String>();
	private List<String> theBigAndTallShirtLinks = new ArrayList<String>();
	private List<String> theBigAndTallAccessoriesLinks = new ArrayList<String>();
	private List<String> theBigAndTallCologneGroomingLinks = new ArrayList<String>();

	private final String theByXPathNewArrivals = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/New_Arrivals.jsp'])[2]";
	private final String theByXPathSweaters = "(//a[contains(text(),'Sweaters')])[4]";
	private final String theByXPathCigarware = "(//a[contains(text(),'Cigarware')])[2]";
	private final String theByXPathShirts = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Shirts.jsp'])[2]";
	private final String theByXPathPolos = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Polos_Tees.jsp'])[2]";
	private final String theByXPathSweatShirts = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Sweatshirts.jsp'])[2]";
	private final String theByXPathJackets = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Outerwear.jsp'])[2]";
	private final String theByXPathPants = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Jeans.jsp'])[2]";
	private final String theByXPathJeans = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Jeans.jsp'])[2]";
	private final String theByXPathShorts = "(//a[contains(text(),'Shorts')])[4]";
	private final String theByXPathSwim = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Swim.jsp'])[2]";
	private final String theByXPathBoxers = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Loungewear_Boxers.jsp'])[2]";
	private final String theByXPathShoes = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Sandals.jsp'])[2]";
	private final String theByXPathAccessories = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Accessories.jsp'])[2]";
	private final String theByXPathCologneGrooming = "(//a[@href='http://test.tommybahama.com/TBG/Big_and_Tall/Fragrance_Body.jsp'])[2]";
	private final String theByXPathGolf = "(//a[contains(text(),'Golf')])[4]";
	private final String theByXPathIslandValues = "(//a[contains(text(),'Island Values')])[4]";
	private final String theByXPathJewelry = "(//a[contains(text(),'Jewelry')])[2]";
	
	
	private final String theByLinkHatsAndCaps = "Hats & Caps";
	private final String theByLinkSunglasses = "Sunglasses";
	private final String theByLinkWatches = "Watches";
	private final String theByLinkWatchStraps = "Watch Straps";
	private final String theByLinkBelts = "Belts";
	private final String theByLinkWalletsAndMoneyClips = "Wallets & Money Clips";
	private final String theByLinkTravelAndBags = "Bags & Travel";
	private final String theByLinkBeachTowels = "Beach Towels";
	private final String theByLinkScarves = "Scarves";
	private final String theByLinkTies = "Ties & Cufflinks";
	private final String theByLinkSocks = "Socks";
	private final String theByLinkTechCases = "Tech Cases";
	
	private final String theByLinkShortSleeveShirts = "Short-Sleeve";
	private final String theByLinkLongSleeveShirts = "Long-Sleeve";
	private final String theByLinkMensSwimShop = "Visit the Swim Shop";

	private final String theByLinkSetSailMartinique = "Set Sail Martinique";
	private final String theByLinkStBarts = "St. Barts";
	private final String theByLinkTBforHim = "TB for Him";
	private final String theByLinkCompass = "Compass";
	
	private final String thePageName = "BigAndTall";

	public void CreateBigAndTallLinks() {

		theBigAndTallLinks.add(this.getTheByXPathNewArrivals());
		theBigAndTallLinks.add(this.getTheByXPathSwim());
		theBigAndTallLinks.add(this.getTheByXPathShirts());
		theBigAndTallLinks.add(this.getTheByXPathPolos());
		theBigAndTallLinks.add(this.getTheByXPathSweatShirts());
		theBigAndTallLinks.add(this.getTheByXPathJackets());		
		theBigAndTallLinks.add(this.getTheByXPathJeans());
		theBigAndTallLinks.add(this.getTheByXPathPants());
		//theBigAndTallLinks.add(this.getTheByXPathShorts());
	
		theBigAndTallLinks.add(this.getTheByXPathBoxers());
		theBigAndTallLinks.add(this.getTheByXPathShoes());
		theBigAndTallLinks.add(this.getTheByXPathCologneGrooming());	

	}

	public void CreateBigAndTallShirtLinks() {
		theBigAndTallShirtLinks.add(this.getTheByLinkShortSleeveShirts());
		theBigAndTallShirtLinks.add(this.getTheByLinkLongSleeveShirts());
	}

	public void CreateBigAndTallAccessoriesLinks() {
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkHatsAndCaps());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkSunglasses());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkScarves());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkTies());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkTechCases());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkWatches());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkWatchStraps());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkBelts());
		theBigAndTallAccessoriesLinks.add(this
				.getTheByLinkWalletsAndMoneyClips());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkTravelAndBags());
		theBigAndTallAccessoriesLinks.add(this.getTheByLinkBeachTowels());
		System.out.println("IT is reading the correct repository!");
	
	}

	public void CreateBigAndTallCologneGroomingLinks() {
		theBigAndTallCologneGroomingLinks.add(this
				.getTheByLinkSetSailMartinique());
		theBigAndTallCologneGroomingLinks.add(this.getTheByLinkCompass());
		theBigAndTallCologneGroomingLinks.add(this.getTheByLinkTBforHim());
		theBigAndTallCologneGroomingLinks.add(this.getTheByLinkStBarts());
	}

	public List<String> getTheBigAndTallLinks() {
		return theBigAndTallLinks;
	}

	public void setTheBigAndTallLinks(List<String> theBigAndTallLinks) {
		this.theBigAndTallLinks = theBigAndTallLinks;
	}

	public List<String> getTheBigAndTallShirtLinks() {
		return theBigAndTallShirtLinks;
	}

	public void setTheBigAndTallShirtLinks(List<String> theBigAndTallShirtLinks) {
		this.theBigAndTallShirtLinks = theBigAndTallShirtLinks;
	}

	public List<String> getTheBigAndTallAccessoriesLinks() {
		return theBigAndTallAccessoriesLinks;
	}

	public void setTheBigAndTallAccessoriesLinks(
			List<String> theBigAndTallAccessoriesLinks) {
		this.theBigAndTallAccessoriesLinks = theBigAndTallAccessoriesLinks;
	}

	public List<String> getTheBigAndTallCologneGroomingLinks() {
		return theBigAndTallCologneGroomingLinks;
	}

	public void setTheBigAndTallCologneGroomingLinks(
			List<String> theBigAndTallCologneGroomingLinks) {
		this.theBigAndTallCologneGroomingLinks = theBigAndTallCologneGroomingLinks;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathShirts() {
		return theByXPathShirts;
	}

	public String getTheByXPathPolos() {
		return theByXPathPolos;
	}

	public String getTheByXPathSweatShirts() {
		return theByXPathSweatShirts;
	}

	public String getTheByXPathJackets() {
		return theByXPathJackets;
	}

	public String getTheByXPathPants() {
		return theByXPathPants;
	}

	public String getTheByXPathJeans() {
		return theByXPathJeans;
	}

	public String getTheByXPathShorts() {
		return theByXPathShorts;
	}

	public String getTheByXPathSwim() {
		return theByXPathSwim;
	}

	public String getTheByXPathBoxers() {
		return theByXPathBoxers;
	}

	public String getTheByXPathShoes() {
		return theByXPathShoes;
	}

	public String getTheByXPathAccessories() {
		return theByXPathAccessories;
	}

	public String getTheByXPathCologneGrooming() {
		return theByXPathCologneGrooming;
	}

	public String getTheByXPathGolf() {
		return theByXPathGolf;
	}

	public String getTheByXPathIslandValues() {
		return theByXPathIslandValues;
	}

	public String getTheByLinkHatsAndCaps() {
		return theByLinkHatsAndCaps;
	}

	public String getTheByLinkSunglasses() {
		return theByLinkSunglasses;
	}

	public String getTheByLinkWatches() {
		return theByLinkWatches;
	}

	public String getTheByLinkWatchStraps() {
		return theByLinkWatchStraps;
	}

	public String getTheByLinkBelts() {
		return theByLinkBelts;
	}

	public String getTheByLinkWalletsAndMoneyClips() {
		return theByLinkWalletsAndMoneyClips;
	}

	public String getTheByLinkTravelAndBags() {
		return theByLinkTravelAndBags;
	}

	public String getTheByLinkBeachTowels() {
		return theByLinkBeachTowels;
	}

	public String getTheByLinkShortSleeveShirts() {
		return theByLinkShortSleeveShirts;
	}

	public String getTheByLinkLongSleeveShirts() {
		return theByLinkLongSleeveShirts;
	}

	public String getTheByLinkMensSwimShop() {
		return theByLinkMensSwimShop;
	}

	public String getTheByLinkSetSailMartinique() {
		return theByLinkSetSailMartinique;
	}

	public String getTheByLinkStBarts() {
		return theByLinkStBarts;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByXPathSweaters() {
		return theByXPathSweaters;
	}

	public String getTheByLinkScarves() {
		return theByLinkScarves;
	}

	public String getTheByLinkTies() {
		return theByLinkTies;
	}

	public String getTheByXPathJewelry() {
		return theByXPathJewelry;
	}

	public String getTheByLinkSocks() {
		return theByLinkSocks;
	}

	public String getTheByLinkTechCases() {
		return theByLinkTechCases;
	}

	public String getTheByXPathCigarware() {
		return theByXPathCigarware;
	}

	public String getTheByLinkTBforHim() {
		return theByLinkTBforHim;
	}

	public String getTheByLinkCompass() {
		return theByLinkCompass;
	}

}
