package WebDriverTests;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;



public class ContactCenterOrder {


  // this is the class where all enviromental details are kept: what site is being
  //tested and any methods that interact with the driver object
  private SeleniumEnvironment myEnvironment;
  private Header myHeaderObjs;

  // The following are the different objects that make up the Tommy Bahama repository that are used throught this scipt. 
  private SignInPage mySIO;
 
  private PaymentPage myPaymentPage;
 
 // private AccountObjs myAccountObjs = new AccountObjs();
  private PayTech myPayTechObjs;
  private ContactCenter myCCObjs = new ContactCenter();   
  
  String testName = "TheContactOrderTest";
  String theEnvironment = "";
  // Main object in the test, driver object is per browser and is used for all interactivity with web page elements. 
  WebDriver driver;


  private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}



	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		 myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
			myHeaderObjs = new Header(driver, myEnvironment);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		
	}

	@Test	
  public void TheContactOrderTest() throws InterruptedException, IOException
  {
     // IWebElement element;
      WebElement currentElement;
  
      // navigates to the environment that is currently being tested. merch, production, qa... ect...
      driver.get(myEnvironment.getTheTestingEnvironment());
      
      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      
      // use this to designate which contact center are we using
      if ((driver.getCurrentUrl().contains("7020")) || (driver.getCurrentUrl().contains("8020")))
      {
          driver.get("http://ecap03:7087/contact/login.jsp");
          theEnvironment = "QA";
      }
      else
      {
          driver.get("http://ecap03:7077/contact/login.jsp");
          theEnvironment = "Merch";
      }
      
      
      myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "Log In", "ContactCenter");
      // need to log in with the correct log in info
      if (theEnvironment == "Merch")
      {
          currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNameUsernameInput()));            
          currentElement.sendKeys(myCCObjs.getTheMerchUsername());
          currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNamePswrdInput()));               
          currentElement.sendKeys(myCCObjs.getTheMerchPswrd());
      }
      else
      {
          currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNamePswrdInput()));            
          currentElement.sendKeys(myCCObjs.getTheQAUsername());

          currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNamePswrdInput()));

          currentElement.sendKeys(myCCObjs.getTheQAPswrd());
      }
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNameLogInSubmit()));

      currentElement.click();

      // wait for this element
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNameCCFrame()));       
     System.out.println("This method is calling once only");
      myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "SearchPage", "ContactCenter");
      // select the main frame    
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNameCCFrame()));

      driver.switchTo().frame(currentElement);

      // enter the email of the contact the user is searching for     
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNameWDEmailSearch()));
     System.out.println("This method is calling once only");
     
      currentElement.sendKeys(mySIO.getTheUsername());
      currentElement = myEnvironment.waitForDynamicElement( By.cssSelector(myCCObjs.getTheByCssWDSearchContacts()));          
     System.out.println("This method is calling once only");
 
      currentElement.click();

      // select the contact from the results

      currentElement = myEnvironment.waitForDynamicElement( By.linkText(myCCObjs.getTheByLinkTextWDContact()));
     System.out.println("This method is calling once only");
      myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "EmailSearchResults", "ContactCenter");
      currentElement.click();

      currentElement = myEnvironment.waitForDynamicElement( By.linkText(myCCObjs.getTheByLinkTextWDCreateOrder()));

      // click the create order link 
      currentElement.click();

      // select ground shipping for this order 
     
      currentElement = myEnvironment.waitForDynamicElement( By.linkText(myCCObjs.getTheWDBrowseCatalogLink()));

      // look for sandals in the catalog  
      currentElement = myEnvironment.waitForDynamicElement( By.linkText(myCCObjs.getTheWDBrowseCatalogLink()));
      currentElement.click();
      currentElement = myEnvironment.waitForDynamicElement( By.xpath(myCCObjs.getTheByXPathWDAccesoriesLink()));
      currentElement.click();
      currentElement = myEnvironment.waitForDynamicElement( By.linkText(myCCObjs.getTheByLinkTextWDSandalShoeLink()));
      currentElement.click();
      currentElement = myEnvironment.waitForDynamicElement( By.linkText(myCCObjs.getTheByLinkTextWDSandalLink()));
      currentElement.click();
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheByNameWDSandalQty()));     
      currentElement.sendKeys("1");
      myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "SandalsCatalog", "ContactCenter");
      currentElement = myEnvironment.waitForDynamicElement( By.id(myCCObjs.getTheByIdWDAddToOrderBtn()));
      currentElement.click();
      currentElement = myEnvironment.waitForDynamicElement( By.id(myCCObjs.getTheWDShippingOptions()));

      Select clickThis = new Select(currentElement);
      clickThis.selectByVisibleText(myCCObjs.getTheFedXNextDay());
      
      Thread.sleep(myEnvironment.getThe_Special_Sleep());
      
      currentElement = myEnvironment.waitForDynamicElement( By.xpath(myCCObjs.getTheReCalcOrderBtn()));
      currentElement.click();
   
      Thread.sleep(myEnvironment.getThe_Special_Sleep());
      currentElement = myEnvironment.waitForDynamicElement( By.id(myCCObjs.getTheByIdOrderTotal()));
      myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "OrderPage", "ContactCenter");

      
   
      
      
      String theTotal = currentElement.getText();
     Thread.sleep(myEnvironment.getThe_Special_Sleep());
  //   System.out.println("Is anything I am doing here even making a difference in this output");
     System.out.println("this is the total:   " + theTotal);
      Double thePulledTotal = myPaymentPage.pullPrice(theTotal);
     System.out.println("this is the pulledtotal:   " + thePulledTotal);
      thePulledTotal = this.roundTwoDecimals(thePulledTotal);
     System.out.println("this is the rounded pulledtotal:   " + thePulledTotal);
   //   String theGCTotal = "$25.00"; 
   //   Double thePulledGCTotal = myPaymentObjs.pullPrice(theGCTotal);
 //    System.out.println("this is the pulledGCtotal:   " + thePulledGCTotal);    
//      thePulledTotal = thePulledTotal - thePulledGCTotal;
  //   System.out.println("this is the pulledtotal - the GCTotal:   " + thePulledTotal);       
      String theCCTotal = thePulledTotal.toString();
 //     String theGCTotalAmount = thePulledGCTotal.toString();
  //   System.out.println("this is the GCTotalAmount:   " + theGCTotalAmount);
    

      ////enter the cc verification number
      
     currentElement = myEnvironment.waitForDynamicElement( By.xpath(myCCObjs.getTheCreditCardVerificationNumberInput()));
      currentElement.sendKeys("111");
     System.out.println("the cc verification number is entered. ");
  
     
      // Add a giftcard to split the payment of this order
   //   currentElement = myEnvironment.waitForDynamicElement( By.id(myCCObjs.getTheSelectPaymentType()));        
    // System.out.println("Selected the Giftcard payment type  ");
  //    clickThis = new Select(currentElement);
 //     clickThis.selectByVisibleText(myCCObjs.getTheGiftCardType());
      
  /*    //click the add payment type button
      currentElement = myEnvironment.waitForDynamicElement( By.id(myCCObjs.getTheAddPaymentTypeBtn()));
      currentElement.click();
     System.out.println("Selected the Giftcard payment type  ");

      // enter the gift card number
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheGiftCardNumberInput()));
      currentElement.sendKeys(myPaymentObjs.getTheGiftCardNumber());
     System.out.println("Entered the Gift Card Number  ");
  
     
      // enter the gift card pin
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheGiftCardPinInput()));
      currentElement.sendKeys(myPaymentObjs.getTheGiftCardPin());
     System.out.println("Entered the Gift Card Pin Number  ");
*/
     
      // enter the amount paid by the CC. 
      currentElement = myEnvironment.waitForDynamicElement( By.name(myCCObjs.getTheCreditCardTotalInput()));
      currentElement.clear();
      currentElement.sendKeys(theCCTotal);
     System.out.println("Entered the Credit Card Total:   " + theCCTotal);
 
      // Enter the Amount paid by the GC       
   //   currentElement = myEnvironment.waitForDynamicElement( By.xpath(myCCObjs.getTheByXPathGiftCardTotalInput()));
   //   currentElement.clear();
    //  currentElement.sendKeys(theGCTotalAmount);
   //  System.out.println("Entered the Gift Card Total   " + theGCTotalAmount);
      currentElement = myEnvironment.waitForDynamicElement( By.id(myCCObjs.getTheUpdatePaymentBtn()));
      currentElement.click();
     System.out.println("Updated the Payment   ");
     myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "CompletedOrderPage", "ContactCenter");
     
    Thread.sleep(2000);
     currentElement = myEnvironment.waitForDynamicElement( By.cssSelector(myCCObjs.getTheSubmitOrderBtn()));
    
      
      currentElement.click();
     System.out.println("Submitted the Payement   ");

    //  currentElement = myEnvironment.waitForPresenceOfElement(driver, By.name(myCCObjs.getTheByNameCCFrame()));
      // element = driver.FindElement(By.Name(myCCObjs.CCFrame));
     Thread.sleep(myEnvironment.getThe_Special_Sleep());
   //   driver.switchTo().frame(currentElement);
      //// Verify the success page loads
      currentElement = myEnvironment.waitForDynamicElement( By.cssSelector(myCCObjs.getTheOrderSummaryHeader()));
     
     // myEnvironment.myWaitForWDElement(driver, By.XPath(myCCObjs.OrderSummeryHeaderByXPath));
      String theOrderNumber = driver.findElement(By.xpath(myCCObjs.getTheByXPathOrderSummaryHeader())).getText();
    // (theOrderNumber.length());
      myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(), testName, "SuccessPage", "ContactCenter");
 
      //get rid of the $ signs
      theOrderNumber = theOrderNumber.substring(42);
      theTotal = theTotal.substring(1);
      
      myPayTechObjs = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getChromeBrowser());
		
		
	
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(theTotal);
	
    //  myPaytechObjs.addMoneyToCard(driver, myEnvironment, myPaymentObjs.getTheGiftCardNumber(), myPaymentObjs.getTheGiftCardPin(),
    //      myPaymentObjs.getTheGiftCardAddAmount(), testName, myEnvironment.getChromeBrowser());
     
  }

 
  @After
  public void quitDriver() 
  {
  
 		
  driver.quit();
  
  }
  public String generateRandomString(int length)
  {
      //Initiate objects & vars   
      Random random = new Random();
      String randomString = "@corbqa.com";
      //String baseString = "@corbqa.com";
      int randNumber;

      //Loop ‘length’ times to generate a random number or character
      for (int i = 0; i < length; i++)
      {
          if ((random.nextInt(3) + 1) == 1)
              randNumber = random.nextInt(123) + 97; //char {a-z}
          else
              randNumber = random.nextInt(90) + 65; //int {A-Z}

          //append random char or digit to random string
          randomString = (char)randNumber + randomString;
      }
      //return the random string
      return randomString;
  }

  double roundTwoDecimals(double d) {
    DecimalFormat twoDForm = new DecimalFormat("#.##");
return Double.valueOf(twoDForm.format(d));
}

}