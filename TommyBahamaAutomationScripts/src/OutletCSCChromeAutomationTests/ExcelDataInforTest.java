package OutletCSCChromeAutomationTests;

import java.io.FileInputStream;
import java.io.IOException;

import javax.mail.MessagingException;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelDataInforTest {
	
	 static ChromeEmailSignUpTest myInforTest = new ChromeEmailSignUpTest();

public static void main (String args[]) throws InterruptedException, MessagingException {
	
	
 
    try {
        // Open the Excel file
        FileInputStream fis = new FileInputStream("C:\\Users\\jwest\\Documents\\TestExcelSheet.xls");
        // Access the required test data sheet
        HSSFWorkbook wb = new HSSFWorkbook(fis);
        HSSFSheet sheet = wb.getSheet("testdata");
        // Loop through all rows in the sheet
        // Start at row 1 as row 0 is our header row
        for(int count = 1;count<=sheet.getLastRowNum();count++){
            HSSFRow row = sheet.getRow(count);
            System.out.println("Running test case " + row.getCell(0).toString());
            // Run the test for the current test data row
          myInforTest.openBrowser();
          myInforTest.EmailSignUpTest(row.getCell(1).toString(),row.getCell(2).toString(),
        		  row.getCell(3).toString(), row.getCell(4).toString(),
        		  row.getCell(5).toString(), row.getCell(6).toString(),
        		  row.getCell(7).toString(), row.getCell(8).toString(),
        		  row.getCell(9).toString(), row.getCell(10).toString() );
        }
     //   myInforTest.quitDriver();
        fis.close();
    } catch (IOException e) {
        System.out.println("Test data file not found");
    }   
}
	

}
