package OutletCSCChromeAutomationTests;


	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletMyAccountPage;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;

	@RunWith(JUnit4.class)
	public class ChromeOrderDetailPageTest {
		// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private OutletWebdriverEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private OutletHeader myHeader;
		private String testName = "EmailSignUp";
		private OutletSignInPage mySignInPage; 
		private OutletMyAccountPage myAccountPage;

		// The object which is how all interaction with web elements is made.
		private int ss = 0;

		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {
			
			
			Assert.assertTrue(isSupportedPlatform());
			File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myHeader = new OutletHeader(driver, myEnvironment);
			mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
			myAccountPage = new OutletMyAccountPage(myEnvironment);
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\Chrome\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add("EmailSignUpBlank");
			fns.add("EmailSignUpFilledOut");
			fns.add("EmailSignUpSuccessModal");
			
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void OrderDetailPageTest() throws InterruptedException, IOException {
			WebElement ce;
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigate to the testing environment
			driver.get(myEnvironment.getTheTestingEnvironment());
		//	driver.get("https://outlet.tommybahama.com/en/login");

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			myEnvironment.setTestSubject("Index Page Test Results");

	for(int i = 0; i < 10; i++) {
				
				System.out.println("This is how many times we have signed in:         " + i);
				
	mySignInPage.signIn(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myHeader.getTheMyAccount()));
	ce.click();	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myAccountPage.getTheOrderHistory()));
	ce.click();	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myAccountPage.getTheFirstOrder()));
	ce.click();	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myHeader.getTheByXPathSignOut()));
	ce.click();	
				
			}
			
	/*		
			for(int i = 50; i < 60; i++) {
				
				
	mySignInPage.signIn(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());
				
				
	
				
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "SignInPage",
						"Blank");
				
				Thread.sleep(1000);
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheByXPathEmailSignUp()));
			ce.click();		
			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "EmailSignUp",
					"Blank");
		
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdEmailAddress()));
			ce.sendKeys("ecommtest" + i + "@tommybahama.com");
			
	
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdFirstName()));
			ce.sendKeys("Jack");
			
	

			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdLastName()));
			ce.sendKeys("West");
			
	
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdZipCode()));
			ce.sendKeys("98027");
			
	
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdMonth()));
			ce.sendKeys("10");
			
	
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdDay()));
			ce.sendKeys("30");
			

			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdYear()));
			ce.sendKeys("1970");
			
		
			
			ce = myEnvironment.waitForDynamicElement(
					By.name(myHeader.getTheByNameGender()));
			ce.click();
			
			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "EmailSignUp",
					"FilledOut");
			
			Thread.sleep(1000);
			
			ce = myEnvironment.waitForDynamicElement(
					By.cssSelector(myHeader.getTheByCssSavePreferences()));
			ce.click();
			
			Thread.sleep(1000);
			
			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "EmailSignUp",
					"SuccessModal");
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='divWrapper']/a/img"));
			ce.click();
			
		     Thread.sleep(1000);
		}
			*/
			
	
			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>Outlet Email Sign Up Test</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if user is able to sign up for emails.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
							"This is the landing page.")
					+
					
					myEnvironment.addSSToFile(testName, "EmailSignUpBlank",
							"This is the email sign up modal.")
					+
					
					myEnvironment.addSSToFile(testName, "EmailSignUpFilledOut",
							"This is the filled out sign up modal.")
					+
					
					myEnvironment.addSSToFile(testName, "EmailSignUpSuccessModal",
							"This is the email sign up success modal.")
					+
					
			
			     	myEnvironment.getPageTestOutcome()
					+

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Outlet Email Sign Up</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+"<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if the User is able to sign up for emails.</h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						
							
				
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add("EmailSignUpBlank");
			fns.add("EmailSignUpFilledOut");
			fns.add("EmailSignUpSuccessModal");
			
			ss = ss - 3;
			

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
			driver.quit();

		}

	}
