	package OutletCSCChromeAutomationTests;


	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.SeleniumEnvironment;

	@RunWith(JUnit4.class)
	public class ChromeLogInTest {
		// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private OutletWebdriverEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private OutletHeader myHeader;
		private String testName = "EmailSignUp";
		private OutletSignInPage mySignIn; 
		// The object which is how all interaction with web elements is made.
		private int ss = 0;

		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {
			
			
			Assert.assertTrue(isSupportedPlatform());
			File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
		
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myHeader = new OutletHeader(driver, myEnvironment);
			mySignIn = new OutletSignInPage(driver, myEnvironment, testName);
			
		
	
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\" + myEnvironment.getFfBrowser() + "\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
				
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void LogInTest() throws InterruptedException, IOException {
			WebElement ce;
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigate to the testing environment
			driver.get(myEnvironment.getTheTestingEnvironment());
		//	driver.get("https://outlet.tommybahama.com/en/login");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			myEnvironment.setBrowser(myEnvironment.getFfBrowser());
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			myEnvironment.setTestSubject("Email Sign Up Test Results");

			
			
			for(int i = 0; i < 1; i++) {
				
				System.out.println("This is how many times we have signed in:         " + i);
				
	mySignIn.signInOutlet(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());
	Thread.sleep(1000);
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myHeader.getTheByXPathSignOut()));
	ce.click();	
				
			}
			
	
			
	
			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>Outlet Email Sign Up Test</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if user is able to sign up for emails.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, "SignInPageBlank",
							"This is the landing page.")
					+
					
			    	myEnvironment.getPageTestOutcome()
					+

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Outlet Email Sign Up</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+"<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if the User is able to sign up for emails.</h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						
							
				
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("EmailSignUpBlank");
			fns.add("EmailSignUpFilledOut");
			fns.add("EmailSignUpSuccessModal");
			
			ss = ss - 3;
			

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
			driver.quit();

		}

	}
