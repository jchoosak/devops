package OutletCSCChromeAutomationTests;


	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

	import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

	import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.SeleniumEnvironment;

	@RunWith(JUnit4.class)
	public class ChromeEmailSignUpTest {
		// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private OutletWebdriverEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private OutletHeader myHeaderObjs;
		private String testName = "IndexPageTest";
		private OutletSignInPage mySignIn; 

		// The object which is how all interaction with web elements is made.
		private int ss = 0;

		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {
		
		/*	Assert.assertTrue(isSupportedPlatform());
			myEnvironment = new OutletWebdriverEnvironment();
			File file = new File(myEnvironment.getIeWebdriver());
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
			ieCap.setVersion("9");
			ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
			driver = new InternetExplorerDriver(ieCap);	*/
			
			Assert.assertTrue(isSupportedPlatform());
			File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myHeaderObjs = new OutletHeader(driver, myEnvironment);
			mySignIn = new OutletSignInPage(driver, myEnvironment, testName);
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\" + myEnvironment.getChromeBrowser() + "\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("EmailSignUpBlank");
			fns.add("EmailSignUpFilledOut");
			fns.add("EmailSignUpSuccessModal");
			
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void EmailSignUpTest(String email, String firstName,
				String lastName, String zip, String month, String day,
				String year, String country, String gender, String exceptions) throws InterruptedException, IOException, MessagingException {
			WebElement ce;
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigate to the testing environment
	
	
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


			myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			myEnvironment.setTestSubject("Index Page Test Results");
			
			driver.get(myEnvironment.getTheTestingEnvironment());
			
		//	mySignIn.signIn(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());

	//		for(int i = 0; i < 10; i++) {
				
				
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeaderObjs.getTheByXPathEmailSignUp()));
			ce.click();		
			
			Thread.sleep(1000);
			// this grabs focus on the Flags layer
			//ce = myEnvironment.waitForDynamicElement(
		//			By.xpath("//*[@id='emailSignupContainer']"));
		//	driver.switchTo().frame(ce);
		//	Thread.sleep(4000);
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdEmailAddress()));
		//	ce.sendKeys("ecommtest" + i + "@tommybahama.com");
			ce.sendKeys(email);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdFirstName()));
			ce.sendKeys(firstName);

			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdLastName()));
			ce.sendKeys(lastName);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdZipCode()));
			ce.sendKeys(zip);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdMonth()));
			ce.sendKeys(month);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdDay()));
			ce.sendKeys(day);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdYear()));
			ce.sendKeys(year);
			
			ce = myEnvironment.waitForDynamicElement(
					By.name(myHeaderObjs.getTheByNameCountry()));
			Select clickThis = new Select(ce);
			
			Thread.sleep(5000);
		/*	if(country.contains("United Kingdom"))
			clickThis.selectByIndex(61);
			else if(country.contains("Canada"))
				clickThis.selectByIndex(2);*/
		//	clickThis.selectByValue(country);
			clickThis.selectByVisibleText(country);
			
			

			 // Find the checkbox or radio button element by Name
			 List<WebElement> oCheckBox = driver.findElements(By.name(myHeaderObjs.getTheByNameGender()));
			
			
			 // This will tell you the number of checkboxes are present
			 int iSize = oCheckBox.size();
			 
			 // Start the loop from first checkbox to last checkboxe
			 for(int i=0; i < iSize ; i++ ){
			 
			     // Store the checkbox name to the string variable, using 'Value' attribute
			     String sValue = oCheckBox.get(i).getAttribute("value");
			 
			     // Select the checkbox it the value of the checkbox is same what you are looking for
			     if (gender.contains("M") && sValue.equalsIgnoreCase("m")){
			         oCheckBox.get(i).click();
			 
			         // This will take the execution out of for loop
			         break;
			         }
			     else if(gender.contains("F") && sValue.equalsIgnoreCase("f")){
			         oCheckBox.get(i).click();
			         break;
		         }
			    }
			
			
		/*	if(gender.contains("M")) {
				// if a male
			ce = myEnvironment.waitForDynamicElement(
					By.name(myHeaderObjs.getTheByNameGender()));
			ce.click();
			} else {
				// if a female
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(myHeaderObjs.getTheByXpathGender()));
			}*/
			
		    if (exceptions.contains("T")) {
		    	ce = myEnvironment.waitForDynamicElement(
						By.id(myHeaderObjs.getTheByIdTBCheckBox()));
		    	ce.click();
		    } 
		    if (exceptions.contains("R")) {
		    	ce = myEnvironment.waitForDynamicElement(
						By.id(myHeaderObjs.getTheByIdRestaurantCheckBox()));
		    	ce.click();
		    } 
		    if (exceptions.contains("B")) {
		    	ce = myEnvironment.waitForDynamicElement(
						By.id(myHeaderObjs.getTheByIdBigAndTallCheckBox()));
		    	ce.click();
		    } 
		    
		    
			ce = myEnvironment.waitForDynamicElement(
					By.cssSelector(myHeaderObjs.getTheByCssSavePreferences()));
			ce.click();
			
			//Thread.sleep(4000);
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='divWrapper']/a/img"));
			ce.click();
			
			//Thread.sleep(4000);
	
			
		
	
			myEnvironment.setTestPassed(true);
			this.quitDriver();
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

		/*	myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>Outlet Email Sign Up Test</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if user is able to sign up for emails.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, "SignInPageBlank",
							"This is the landing page.")
					+
					
					myEnvironment.addSSToFile(testName, "EmailSignUpBlank",
							"This is the email sign up modal.")
					+
					
					myEnvironment.addSSToFile(testName, "EmailSignUpFilledOut",
							"This is the filled out sign up modal.")
					+
					
					myEnvironment.addSSToFile(testName, "EmailSignUpSuccessModal",
							"This is the email sign up success modal.")
					+
					
			
			     	myEnvironment.getPageTestOutcome()
					+

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Outlet Email Sign Up</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+"<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if the User is able to sign up for emails.</h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						
							
				
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("EmailSignUpBlank");
			fns.add("EmailSignUpFilledOut");
			fns.add("EmailSignUpSuccessModal");
			
			ss = ss - 3;
			

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);*/
			driver.quit();

		}

	}
