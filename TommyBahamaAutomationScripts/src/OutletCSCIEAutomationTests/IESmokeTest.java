package OutletCSCIEAutomationTests;



	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

	import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import net.lightbody.bmp.proxy.ProxyServer;

	import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaOutletRepository.OutletDeliveryMethodPage;
import TommyBahamaOutletRepository.OutletAddressPage;
import TommyBahamaOutletRepository.OutletCSC;
import TommyBahamaOutletRepository.OutletConfirmationPage;
import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletHomeDecor;
import TommyBahamaOutletRepository.OutletPaymentPage;
import TommyBahamaOutletRepository.OutletPreviewPage;
import TommyBahamaOutletRepository.OutletProducts;
import TommyBahamaOutletRepository.OutletSearchResultsPage;
import TommyBahamaOutletRepository.OutletShoppingCart;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;
import TommyBahamaOutletRepository.OutletPaymentech;
import TommyBahamaOutletRepository.OutletProductDetailPage;
import TommyBahamaOutletRepository.OutletProductListingPage;



	@RunWith(JUnit4.class)
	public class IESmokeTest {
		// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private OutletWebdriverEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private OutletHeader myHeaderPage;
		private OutletProductListingPage myProductListingPage;
		private OutletProductDetailPage myProductDetailPage;
		private OutletSignInPage mySignInPage; 
		private OutletHomeDecor myHomeDecor;
		private OutletProducts  myProducts;
		private OutletShoppingCart myShoppingCart;
		private OutletAddressPage myAddressPage;
		private OutletPaymentPage myPaymentPage;
		private OutletDeliveryMethodPage myDeliveryMethod;
		private OutletPreviewPage myPreviewPage;
		private OutletConfirmationPage myConfirmationPage;
	    private OutletCSC myCSC;
	    private OutletSearchResultsPage mySearchResults;
	    private OutletPaymentech myPaymentech;
		private String testName = "IESmokeTest";
		
		Proxy proxy = new Proxy();
		
		ProxyServer server = new ProxyServer(8105);
		// The object which is how all interaction with web elements is made.
		private int ss = 0;
		
		String OrderNumber = "";
		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {
		
		   
			Assert.assertTrue(isSupportedPlatform());
			myEnvironment = new OutletWebdriverEnvironment();
			File file = new File(myEnvironment.getIeWebdriver());
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
			ieCap.setVersion("9");
			ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		
			driver = new InternetExplorerDriver(ieCap);	
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myHeaderPage = new OutletHeader(driver, myEnvironment);
			mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
			 myProductListingPage = new OutletProductListingPage(driver, myEnvironment);
			 myProductDetailPage = new OutletProductDetailPage(driver, myEnvironment, myProductListingPage, 
					 myHeaderPage, testName);
			 
			 myPaymentPage = new OutletPaymentPage(myEnvironment, testName);
			 mySearchResults = new OutletSearchResultsPage(myEnvironment, myHeaderPage);
			 myHomeDecor = new OutletHomeDecor();
			 myProducts = new OutletProducts();
			 myAddressPage = new OutletAddressPage();
			 myDeliveryMethod = new OutletDeliveryMethodPage();
			 myPreviewPage = new OutletPreviewPage();
			 myConfirmationPage = new OutletConfirmationPage();
			myShoppingCart = new OutletShoppingCart(driver, myEnvironment);
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\" + myEnvironment.getIeBrowser() + "\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("PLPDinnerware");
			fns.add("PDPTH31473");
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void IESmokeEnvironmentTest() throws InterruptedException, IOException {
			WebElement ce;
		//	String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		//	JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigate to the testing environment
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			myEnvironment.setBrowser(myEnvironment.getIeBrowser());
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			myEnvironment.setTestSubject(testName);
			
			driver.get(myEnvironment.getTheTestingEnvironment());
			
			mySignInPage.signIn(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());
			
			
			
			// execute a search
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(myHeaderPage.getTheSearchInput()));
						ce.sendKeys("Men");
						
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(myHeaderPage.getTheSearchBtn()));
						ce.click();
						
						myEnvironment.waitForURL("Men");			
						
						// check if black products are present. this should be every product but for now just one.
               

						ce = myEnvironment.waitForDynamicElement(
								By.xpath(mySearchResults.getTheByXpathSecondProduct()));
						
						System.out.println(ce.getAttribute("src"));

						Assert.assertTrue(ce.getAttribute("src").contains("s7"));
						
			
			// add a product to the cart 
						
			myProductDetailPage.selectProduct(
					myHeaderPage.getTheByXPathHomeDecorTab(),
					myHomeDecor.getTheByXPathDinnerware(),
					myProducts.getTheDinnerWareProduct(), "4",  "Dinnerware");
			
			
			// continue to shopping bag
			ce = myEnvironment.waitForDynamicElement(
					By.linkText(myHeaderPage.getTheByLinkCheckout()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			
			
			ce = myEnvironment.waitForDynamicElement(
					By.name(myShoppingCart.getTheByNameQtyUpdate()));
			// Thread.Sleep(myEnvironment.DefaultSleep);
			Select clickThis = new Select(ce);
			// Thread.Sleep(myEnvironment.DefaultSleep);
			clickThis.selectByValue("3");
			
			myEnvironment.waitForPageLoaded();	  
			myEnvironment.waitForDocumentReadyState();
			
			// continue to address page
			ce = myEnvironment.waitForDynamicElement(
					By.id(myShoppingCart.getTheByIdBottomContinueCheckout()));
	
	        ce.click();
	        
	    	myEnvironment.waitForPageLoaded();	    	
	    	myEnvironment.waitForDocumentReadyState();
	        
	    	// continue to delivery options
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
	
	        ce.click();
	        
	    	myEnvironment.waitForPageLoaded();
	    	myEnvironment.waitForDocumentReadyState();
	    	myEnvironment.waitForClickableElement(
 					By.id(myDeliveryMethod.getTheByIdContinueCheckoutButton()));
 	
	    	  // continue to PaymentPage 
 			ce = myEnvironment.waitForDynamicElement(
 					By.id(myDeliveryMethod.getTheByIdContinueCheckoutButton()));
 	
 	        ce.click(); 	        
	    	
 	      	myEnvironment.waitForPageLoaded();
	    	myEnvironment.waitForDocumentReadyState();
	    	myEnvironment.waitForClickableElement(By.id(myPaymentPage.getTheByIdContinueCheckout()));
	    	
	     // continue to PaymentPage 
	     			ce = myEnvironment.waitForDynamicElement(
	     					By.id(myPaymentPage.getTheByIdContinueCheckout()));
	     	
	     	        ce.click();
	     	        
	     	  
	     	    	myEnvironment.waitForPageLoaded();
	    	    	myEnvironment.waitForDocumentReadyState();
	    	    	
	    	     // enter the cc verification number
	    	     			ce = myEnvironment.waitForDynamicElement(
	    	     					By.id(myPaymentPage.getTheByIdCCSecurityCode()));
	    	     	
	    	     	        ce.sendKeys(myPaymentPage.getTheCode());      
	     	
	    	    	
	    	     // continue to Order Review paypage 
	    	     			ce = myEnvironment.waitForDynamicElement(
	    	     					By.id(myPaymentPage.getTheSubmitPaymentBtn()));
	    	     	
	    	     	        ce.click();
	    	     	        
	    	     	   	myEnvironment.waitForPageLoaded();
		    	    	myEnvironment.waitForDocumentReadyState();
	    	     	        
	    	     	       // submit order
	    	     			ce = myEnvironment.waitForDynamicElement(
	    	     					By.xpath(myPreviewPage.getTheByXpathSubmitButton()));
	    	     	
	    	     	        ce.click();
			
	    	     	      	myEnvironment.waitForPageLoaded();
			    	    	myEnvironment.waitForDocumentReadyState();
			    	    	
			    	    	OrderNumber = driver.getCurrentUrl();
			    			OrderNumber = OrderNumber.substring(64);
			    			
			    			String theTax = driver.findElement(
			    					By.id(myConfirmationPage.getTheTotalTax())).getText();
			    			
			    			System.out.println("This is the tax: 	" + theTax);
			    		//	String theDuty = driver.findElement(
			    		//			By.id(myPreviewPage.getTheByIdDuty())).getText();
			    			String theTotal = driver.findElement(
			    					By.id(myConfirmationPage.getTheTotalAmount())).getText();
			    			
			    			System.out.println("This is the total amount: 	" + theTotal);
			    			myCSC = new OutletCSC(driver, myEnvironment,  "",
			    					theTax, OrderNumber, testName, myEnvironment.getFfBrowser());
			    			
			    			myCSC.checkOrderInCSC(theTotal);
	
			    			// See that the auth happened in OG
			    			ss = ss + 1;
			    			// remove the $ sign because money values in Contact center do not
			    			// dispaly the dollar sign
			    			Double theDTotal = 0.0;
			    			theDTotal = myPaymentPage.pullPrice(theTotal);
			    			String newTotal = "";
			    			
			    			myPaymentech = new OutletPaymentech(driver, myEnvironment,  
			    					OrderNumber, testName, myEnvironment.getFfBrowser());
			    			
			    			
			    			newTotal = myPaymentech
			    					.formatTotal(newTotal, theDTotal);
			    			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			    			// runs the script to look up the order in Paytech.
			    			
			    			
			    			myPaymentech
			    					.checkOrderInPayTech(newTotal);
			    			
			    			
			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>Outlet Single Division Order</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if a single division order is completed correctly on WS and in CSC.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, "SignInPageBlank",
							"This is the Sign In page.")
					+
					
					myEnvironment.addSSToFile(testName, "PLPDinnerware",
							"This is the Dinnerware PLP.")
					+
					
					myEnvironment.addSSToFile(testName, "PDPTH31473",
							"This is the PDP for the product TH31473.")
					+
					
					myEnvironment.addSSToFile(testName, "LogInPage",
							"This is the CSC login page.")
					+
					
					myEnvironment.addSSToFile(testName, "CSCLandingPage",
							"This is the CSC landing page.")
					+

					myEnvironment.addSSToFile(testName, "SearchOrderFormWithResults",
							"This is the search modal for orders.")
					+
					
					myEnvironment.addSSToFile(testName, "OrderTab",
							"This is the Order Detail page.")
					+

					myEnvironment.addSSToFile(testName, "OrderTab2",
							"This is the bottom of the Order Detail page.")
					+

			     	myEnvironment.getPageTestOutcome()
					+

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Single Divison Order</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+"<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if a single division order is correct on WS and CSC.</h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						
							
				
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("PLPDinnerware");
			fns.add("PDPTH31473");
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
			
			ss = ss - 7;
			

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
			driver.quit();

		}

	}

