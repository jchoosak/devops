package OutletCSCIEAutomationTests;





	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TommyBahamaOutletRepository.CSCMenu;
import TommyBahamaOutletRepository.OutletCSCOrderTab;
import TommyBahamaOutletRepository.OutletCustomerTab;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;
import TommyBahamaRepository.OrderTab;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.CustomerTab;
import TommyBahamaRepository.WaitTool;

	@RunWith(JUnit4.class)
	public class IeCSCFindOrder {
		
		// this is the class where all enviromental details are kept: what site is
		// being tested and any methods that interact with the driver object
		private OutletWebdriverEnvironment myEnvironment;
		
		// Sign In page for different sites
		private OutletSignInPage mySIO;
		
		// This is the left panel in the CSC 
		private CSCMenu myCSCMenu;
		
		// This is the customer tab in the CSC. 
		private OutletCustomerTab myCustomerTab;
		
		// This is the order tab in the CSC.
		private OutletCSCOrderTab myOrderTab;
		
		private String testName = "IeCSCFindOrder";
		

		// The object which is how all interaction with web elements is made.
		private int ss = 0;

		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {
		
			Assert.assertTrue(isSupportedPlatform());
			myEnvironment = new OutletWebdriverEnvironment();
			File file = new File(myEnvironment.getIeWebdriver());
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
			ieCap.setVersion("9");
			ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		//	ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
			driver = new InternetExplorerDriver(ieCap);	
		
			myCSCMenu = new CSCMenu();
		
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myCustomerTab = new OutletCustomerTab(myEnvironment, testName);
			 mySIO = new OutletSignInPage(driver, myEnvironment, testName);
			myOrderTab = new OutletCSCOrderTab(myEnvironment, testName);
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\" + myEnvironment.getIeBrowser() + "\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderForm");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void CSCFindOrder() throws InterruptedException, IOException {
			
			WebElement ce;
			
		
			// set how timeouts should be handled
			driver.manage().timeouts().implicitlyWait(WaitTool.DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);
			
			
			// Navigate to the testing environment
			driver.get(myEnvironment.getTheOutletCSCTestingEnvironment());
		

			// set the test results email up. 
			myEnvironment.setBrowser(myEnvironment.getIeBrowser());
			myEnvironment.setEnvironment(myEnvironment.getTheOutletCSCTestingEnvironment());
			myEnvironment.setTestSubject("CSC Add Find an Order Test Results");

			// this is to get past the lack of a ssl security certificate. 
		//	driver.get("javascript:document.getElementById('overridelink').click();");
		
			
			// this is a wait tell an emlement is clickable since the element is always present in the DOM.   
			ce = (new WebDriverWait(driver, 10))
					  .until(ExpectedConditions.elementToBeClickable(By.cssSelector(mySIO.getTheCSCOutletLoginButton())));
		//	ce = WaitTool.waitForElement(driver, By.xpath(myCustomerTab.getTheByXpathPasswordResetButton()), 10);

			
			mySIO.signInCSC(myEnvironment.getJackEmail(), myEnvironment.getPassWrd());
			
		
			
			ce = myEnvironment.waitForDynamicElement(By.linkText(myCSCMenu.getTheByLinkFindOrderLink()));
			
			myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
					testName, "CSCLandingPage", "");
			
			ce.click();	
		
			ce = myEnvironment.waitForDynamicElement(By.xpath(myCSCMenu.getTheByXpathCustomerName()));
			
			
		    ce.sendKeys(myOrderTab.getDefaultOrder());		

			
			myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
					testName, "SearchOrderForm", "");
			 
			
			String selectEnter = Keys.chord(Keys.ENTER);
			driver.findElement(By.xpath(myCSCMenu.getTheByXpathSubmitOrderSearchButton())).sendKeys(selectEnter);

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myCSCMenu.getTheByXpathSubmitOrderSearchButton()));
			ce.click();
			

			
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myCSCMenu.getTheByXPathSearchSelectButton()));
			
			
			myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
					testName, "SearchOrderFormWithResults", "");
			
		
			ce.click();



		ce = WaitTool.waitForElement(driver, 
				By.xpath(myOrderTab.getTheByXpathCreateNewTicketButton()), 10);

		myEnvironment.waitForDocumentReadyState();
		//ce = WaitTool.waitForElement(driver, By.xpath("//div[2]/span/table/tbody/tr[2]/td[2]"), 10);
		
		//ce = WaitTool.waitForElement(driver, By.xpath(myCustomerTab.getTheByXpathPasswordResetButton()), 10);
		
		Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
				By.xpath(myOrderTab.getTheByXpathCreateNewTicketButton()), "Create New Ticket", 10));
		
			
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "OrderTab", "");	
		
		
		Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
				By.xpath(myOrderTab.getTheByXpathRefundButton()), "Refund Order", 10));
		
	//	Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
		//		By.xpath(myOrderTab.getTheByXpathWaiveShippingCheckbox()), "Waive Return Shipping Fee", 10));
		
		Assert.assertTrue( WaitTool.waitForTextPresent(driver, 
				By.xpath(myOrderTab.getTheByXpathWaiveShippingButton()), "Submit", 10));
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myOrderTab.getTheByXpathWaiveShippingCheckbox()));
		ce.click();
		
		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "OrderTab2", "");	
		

			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>Finding an Order in the Customer Service Cockpit!</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if a GSR can find an order correctly in the Customer Service Cockpit.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, "LogInPage",
							"This is the Customer Service Cockpit login page.")
					+
					
					myEnvironment.addSSToFile(testName, "CSCLandingPage",
							"This is the Customer Service Cockpit landing page.")
					+
					
					myEnvironment.addSSToFile(testName, "SearchOrderForm",
							"This is the Customer Service Cockpit Search For Orders form.")
					+
					
					myEnvironment.addSSToFile(testName, "SearchOrderFormWithResults",
							"This is the Customer Service Cockpit Search For Orders form with results.")
					+		
					myEnvironment.addSSToFile(testName, "OrderTab",
							"This is the top of the Customer Service Cockpit Order Tab for the Order that was searched for.")
					+	
					
						myEnvironment.addSSToFile(testName, "OrderTab2",
							"This is the bottom of the Customer Service Cockpit Order Tab for the Order that was searched for.")
					+	

			     	myEnvironment.getPageTestOutcome()
					+

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Find an Order in the Customer Service Cockpit</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+"<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if a Guest Service Representative can find an Order in the Customer Service Cockpit. </h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						
							
				
			List<String> fns = new ArrayList<String>();
	
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderForm");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
		
			
			ss = ss - 5;
			

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
			driver.quit();

		}
		
	}


