package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.LandingPage;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

@RunWith(JUnit4.class)
public class FFUnitedStatesSplitTenderOrderTest {
	// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private SeleniumEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private SignInPage mySignInPage;
		private Header myHeader;
		private Men myMen = new Men();
		private ProductListingPage myPLP;
		private ProductDetailPage myPDP;
		private ShoppingBagPage myShoppingBag;
		private GiftService myGiftService;
		private AddressPage myAddressPage = new AddressPage();
		private PaymentPage myPaymentPage;
		private PreviewPage myPreviewPage = new PreviewPage();
		private ConfirmationPage myConfirmationPage = new ConfirmationPage();
		private AccountPage myAccountPage;
		private PayTech myPayTech = new PayTech();
		private ContactCenter myContactCenter = new ContactCenter();
		private HomeDecor myHomeDecor = new HomeDecor();
		private LandingPage myLandingPage = new LandingPage();
		private Product myProducts = new Product();
		
		private Product highPriceTshirtProduct = new Product("72.00", "1", "highPricedTshirt");
		private Product homeProduct = new Product( "TH31414", "2", "Dinnerware");
		  private Product shoeProduct = new Product("TFM00206", "1", "Shoes");
		  
		// amount to be added to a gift card
		private String theEnvironment = "";
		private String theOrderNumber = "";
		private int ss = 0;
		private final String testName = "TheUSSplitTenderOrderTest";
		// url to be checked on various pages
		private String url = "";

		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
	}
	
	private WebDriver driver;
	
	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		
		myEnvironment = new SeleniumEnvironment(driver);
		 myAccountPage = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftService = new GiftService(driver, myEnvironment, myShoppingBag);
			myHeader = new Header(driver, myEnvironment);
			 myPLP = new ProductListingPage(driver, myEnvironment);
			 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
					 myHeader, testName);
		 myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySignInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
		 
		 List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myAccountPage.getThePageName());
			fns.add(myLandingPage.getThePageName());
			fns.add(myPLP.getThePageName()  + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice() );
			fns.add(myPDP.getThePageName() + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice() );
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName() + shoeProduct.getName());
			fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftWrap());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
			fns.add(myAddressPage.getTheAddressPagePreFilled());
			fns.add(myPaymentPage.getThePageName());
			fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
			fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardApplied());
			fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());
			fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
			fns.add(myPayTech.getThePageName() + theOrderNumber + "");
			fns.add(myPayTech.getThePageName() + myPaymentPage.getTheGiftCardNumber());
			
			String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getSafariBrowser() + "/" + testName;
			myEnvironment.removeSS(fns, tempFile);
			
	}

//	  private static ChromeDriverService service;
//	   private WebDriver driver;
//	 
//	   @BeforeClass
//	   public static void createAndStartService() throws IOException {
//	     service = new ChromeDriverService.Builder()
//	         .usingDriverExecutable(new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe"))
//	         .usingAnyFreePort()
//	         .build();
//	     service.start();
//	   }


	@Test
	public void TheUSSplitTenderOrderTest() throws InterruptedException,
			IOException {
		WebElement currentElement;
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());

		myEnvironment
				.setTestSubject("United States Split Tender Order Test Results");
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Sign into an Washington State account
		mySignInPage.signIn(
				mySignInPage.getTheUSSplitTenderUserName(), mySignInPage.getThePassword());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountPage.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myAccountPage.getThePageName(), "");
		ss = ss + 1;
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myLandingPage.getThePageName(), "");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		myPDP.selectProduct(				
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathDinnerware(),
				homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				homeProduct.getProductId());

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));
		
		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeader
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myPDP.selectProductByPrice(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathMensTShirts(), highPriceTshirtProduct.getPrice(), highPriceTshirtProduct.getQty(), 
				highPriceTshirtProduct.getName());
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice());

	

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));
		
		// add shoes to the bag
		myPDP.selectProduct(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathShoes(), shoeProduct.getProductId(), shoeProduct.getQty(), 
				shoeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				shoeProduct.getName() + shoeProduct.getPrice());

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));
		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myShoppingBag.getThePageName(), "");
		ss = ss + 1;

		// add gift services to the products
		// myGiftService.giftBox(driver, currentElement, myEnvironment,
		// myShoppingBag, myShoppingBag.ThirdGiftLink, testName);
		myGiftService.giftBox(
				myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getSafariBrowser());
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myShoppingBag.getThePageName(), myProducts.getThePageName() + myGiftService.getThePageName());
		ss = ss + 1;

		// check to see if the url is secure on the address page
		driver.findElement(By.xpath(myShoppingBag.getTheContinueCheckout()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressPage.getTheURL()));

		// Shipping address same as billing
		driver.findElement(By.id(myAddressPage.getTheByIdSameAddress()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingZipCode()));
		String theZip = driver.findElement(
				By.id(myAddressPage.getTheByIdBillingZipCode())).getAttribute(
				"value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myAddressPage.getThePageName(), "PreFilled");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		driver.findElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(myPayment.getTheByXPathTwoDayShipping()));
		myHeader.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myPaymentPage.getThePageName(), "");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheNextDayShipping()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		/*
		 * This is commented out because currently we are under a month of free
		 * shipping, will uncomment when shipping is returned to normal for US
		 * orders.
		 */
		// Checks to see if the shipping price is correct on the payment page.
		// ShippingPrice = "";
		// ShippingPrice =
		// driver.FindElement(By.XPath(myPayment.GroundShipping)).getTheText();
		// ShippingPrice = ShippingPrice.Remove(0, 1);
		// Double.TryParse(ShippingPrice, out TheShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPayment.ActualCanadaShippingPrice);

		// Checks to see that there is no other shipping options present
		// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPayment.getTheByXPathTwoDayShipping()),
		// driver));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// see that Card Services are working
		// see that Card Services are working
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalenceLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		myEnvironment.handleMultipleWindows( "Gift Card Balance Lookup");
		
/*		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));*/
		// String theBalence = currentElement.getText();
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myPaymentPage.getThePageName(), myPaymentPage.getTheGiftCardApplied());
		ss = ss + 1;
		
		driver.close();

		myEnvironment.handleMultipleWindows(
				"Payment Information");

		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// use the giftcard for partial payment
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardPinInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardPin());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheGiftCardPartialPayment()));

		System.out.println(currentElement.getText().trim());
		String thePartialPayment = currentElement.getText().trim();
		Assert.assertTrue(thePartialPayment.equals("-$25.00"));
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myPaymentPage.getThePageName(), myPaymentPage.getTheGiftCardApplied());
		ss = ss + 1;

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// waits for preview page to load
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewPage.getTheTotalTax()));
		myHeader.checkUrl(myPreviewPage.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myPreviewPage.getThePageName(), "");
		ss = ss + 1;

		// check to see if the tax is what it should be for a Canada order
		Thread.sleep(5000);
		String theTax = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalTax())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewPage.getTheSplitTenderOrderTotalAmount())).getText();
		String theShipping = driver
				.findElement(By.xpath(myPreviewPage.getTheShippingAmount()))
				.getText().trim();
		thePartialPayment = driver
				.findElement(By.xpath(myPreviewPage.getTheGCPartialPayment()))
				.getText().trim();
		String theUSGC = driver.findElement(
				By.xpath(myPreviewPage.getTheUSGC())).getText();
		System.out.println(theShipping);
		System.out.println(thePartialPayment);

		// Assert.assertTrue(thePartialPayment.Contains("GIFT CARD:"));
		Assert.assertTrue(thePartialPayment.contains("$25.00"));
		System.out.println(thePartialPayment);
		// Assert.assertTrue(theShipping == "$16.00");
		System.out.println(theShipping);
		System.out.println(theUSGC);
		System.out.println(myPaymentPage.getTheGiftCardNumber().trim());
		Assert.assertTrue(theUSGC.equals(myPaymentPage.getTheGiftCardNumber()
				.trim()));

		// Submit the order
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPreviewPage.getTheByCSSSubmitButton()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationPage.getTheOrderNumber()));
		myHeader.checkUrl(myConfirmationPage.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myConfirmationPage.getThePageName(), "");
		ss = ss + 1;

		// Store the Order number for CC and for Paytech

		theOrderNumber = currentElement.getText();
		theOrderNumber = theOrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		// Assert.assertTrue("$16.00" ==
		// driver.FindElement(By.XPath(myConfirmationPage.getTheShippingAmount())).getTheText().Trim());

		// Insert a comma for how totals appear in contact center and OG
		// String theContactTotal = theTotal.Insert(2, ",");
		System.out.println(theTotal);
		// runs the script to look up the order in Contact Center
		// Insert a comma for how totals appear in contact center and OG

		myContactCenter = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, theOrderNumber, testName,
				myEnvironment.getSafariBrowser());
		
		String theContactTotal = myContactCenter.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenter.checkOrderInCC(theContactTotal);
		
		
		ss = ss + 1;
		
		
		myPayTech = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getSafariBrowser());
		
		
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		System.out.println(theTotal);
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		Double theGCTotal = 0.0;
		theGCTotal = myPaymentPage.pullPrice(myPayTech
				.getTheGCAddedAmount());
		theDTotal = theDTotal - theGCTotal;
		String newTotal = "";
		
		
		
		
		newTotal = myPayTech
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
			myPayTech
		.checkOrderInPayTech(newTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ss = ss + 1;
		// Adds money back to the gift card so the script is ready to run again
		myPayTech.addMoneyToCard(
				myPaymentPage.getTheGiftCardNumber(),
				myPaymentPage.getTheGiftCardPin(),
				myPaymentPage.getTheGiftCardAddAmount());
		ss = ss + 1;
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>United States Split Tender Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The United States Split Tender Order Test."
				+ " This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, myAccountPage.getThePageName(),
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, myLandingPage.getThePageName(),
						"This is the Home Page.")
				+

						myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice(),
						"This is the T-Shirts PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice() ,
						"This is the T-Shirt PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice() ,
						"This is the T-Shirt PDP with Preview pane displaying added product.")
				+

					myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+
		
		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+
				
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+

		myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  shoeProduct.getName(),
						"This is the Shoes PLP.")
				+		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + shoeProduct.getProductId(),
						"This is the Shoe PDP.")
				+		
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + shoeProduct.getProductId(),
						"This is the Shoe PDP with Preview pane displaying added product.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName(),
						"This is the Shopping Bag with products.")
				+

				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftWrap(),
								"This is the Shopping Bag with the Gift Wrap Modal.")
						+

					myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(),
								"This is the Shopping Bag containing Products with Gift Services.")
						+

						myEnvironment.addSSToFile(testName, myAddressPage.getTheAddressPagePreFilled(),
						"This is the Address Page with pre populated fields.")
				+

						myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName(),
						"This is the Payment Page.")
				+
				
				
			myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getTheGiftCardBalenceModal(),
						"This is the Gift Card Balence Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut(),
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, myPreviewPage.getThePageName(),
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, myConfirmationPage.getThePageName(),
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, myContactCenter.getThePageName() + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + theOrderNumber,
						"This is the PaymenTech Page.")
				+

				
				myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + myPaymentPage.getTheGiftCardNumber(),
						"This is the approved add money to gift card PaymenTech Page.")
				
				+
			    
					myEnvironment.getPageTestOutcome()
					
				+ "</center></body></html>");
		
		bw.close();


		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>United States Split Tender Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The United States Split Tender Order Test"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada. This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myAccountPage.getThePageName());
		fns.add(myLandingPage.getThePageName());
		fns.add(myPLP.getThePageName()  + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice() );
		fns.add(myPDP.getThePageName() + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + highPriceTshirtProduct.getName() + highPriceTshirtProduct.getPrice() );
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPLP.getThePageName() + homeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
		fns.add(myPLP.getThePageName() + shoeProduct.getName());
		fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftWrap());
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
		fns.add(myAddressPage.getTheAddressPagePreFilled());
		fns.add(myPaymentPage.getThePageName());
		fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
		fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardApplied());
		fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());
		fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
		fns.add(myPayTech.getThePageName() + theOrderNumber + "");
		fns.add(myPayTech.getThePageName() + myPaymentPage.getTheGiftCardNumber());

		// could not find where I am missing my calls for two screenshots. Have bigger fish to fry!
		ss = ss + 2;
		
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);
		
		System.out.println("This is how many screenshots that are allowed to be displayed:                " +  ss + "");

		driver.quit();
	}

}