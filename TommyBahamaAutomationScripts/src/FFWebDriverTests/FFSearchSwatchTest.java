package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SearchResultPage;
import TommyBahamaRepository.SeleniumEnvironment;


@RunWith(JUnit4.class)
public class FFSearchSwatchTest {
	private SeleniumEnvironment myEnvironment;
	private Header myHeader;
	private Men myMen = new Men();
	private ProductListingPage myPLP;
	private ProductDetailPage myPDP;
	private SearchResultPage mySearchResultPage;
	private PaymentPage myPaymentPage;
	private String testName = "SearchSwatchTests";
	private Product myProducts = new Product();
	private Product socksProduct = new Product("20.00", "4", "HighPricedSocks");
	private Product sunglassesProduct = new Product("TB6016", "4", "Sunglasses");
	
	
	private WebDriver driver;
	
	private int ss = 0;
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		
		driver = new FirefoxDriver(p);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeader = new Header(driver, myEnvironment);
		 myPLP = new ProductListingPage(driver, myEnvironment);
		 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
				 myHeader, testName);
		 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myPaymentPage = new PaymentPage(myEnvironment, testName);
		mySearchResultPage = new SearchResultPage(driver, myEnvironment, myPaymentPage, myPLP);
		
		List<String> fns = new ArrayList<String>();
		fns.add(mySearchResultPage.getThePageName());
		fns.add(mySearchResultPage.getThePageName() + mySearchResultPage.getTheShowAllMessage());
		fns.add(mySearchResultPage.getThePageName()+ mySearchResultPage.getTheRatingsMessage());
		fns.add(mySearchResultPage.getThePageName() + mySearchResultPage.getThePriceHighToLowMessage());
		fns.add(mySearchResultPage.getThePageName() + mySearchResultPage.getThePriceLowToHighMessage());
		fns.add(myPDP.getThePageName() + myProducts.getTheColor());
		fns.add(myPLP.getThePageName()  + socksProduct.getName() + socksProduct.getPrice() );
		fns.add(myPDP.getThePageName() + socksProduct.getName() + socksProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + socksProduct.getName() + socksProduct.getPrice() );
		fns.add(myPLP.getThePageName()  + sunglassesProduct.getName() + sunglassesProduct.getProductId() );
		fns.add(myPDP.getThePageName() + sunglassesProduct.getName() + sunglassesProduct.getProductId()  );
		fns.add(myPDP.getThePreviewPaneName() + sunglassesProduct.getName() + sunglassesProduct.getProductId() );
		String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
		
		myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void SearchSwatchTests() throws InterruptedException, IOException {
		WebElement ce;
		// String mouseOverScript =
		// "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setTestSubject("Search & Swatch Test Results");

		// ss = ss + 1;

		// test search input and the search page functionality
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathSearchInput()));
		ce.sendKeys("red swimsuit");
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheSearchBtn()));
		ce.click();
		 myEnvironment.getThe_Default_Sleep();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySearchResultPage.getTheSearchResultTitle()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().contains("Search Results: red swimsuit"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, mySearchResultPage.getThePageName(), "");

		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myPLP.getTheFirstImage()));
		ce = myEnvironment.waitForDynamicElement(
				By.linkText(mySearchResultPage.getTheShowAllLink()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		myEnvironment.waitForPageLoaded();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myPLP.getTheFirstImage()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, mySearchResultPage.getThePageName(), "ShowAll");

		ss = ss + 1;
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		mySearchResultPage.testRatings();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, mySearchResultPage.getThePageName(), "ByRatings");
	//	ss = ss + 1;
		
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathSearchInput()));
		ce.sendKeys("black bikini");
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheSearchBtn()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySearchResultPage.getTheSearchResultTitle()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().equals("Search Results: black bikini"));

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myPLP.getTheFirstImage()));

		mySearchResultPage.testPriceHighToLow();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, mySearchResultPage.getThePageName(), "ByPriceHighToLow");

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathSearchInput()));
		ce.sendKeys("black bikini");
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheSearchBtn()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySearchResultPage.getTheSearchResultTitle()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().equals("Search Results: black bikini"));

		mySearchResultPage.testLowToHigh();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, mySearchResultPage.getThePageName(), "ByPriceLowToHigh");

		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathMensTab()));
		ce.click();
		 myEnvironment.getThe_Default_Sleep();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myMen.getTheByXPathJeans()));
		ce.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPLP.getThePageName(), "");
		// ss = ss + 1;
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// test the swatches on the PLP page, when clicking on a swatch, the
		// cooresponding thumbnail will change to the color
		myPLP.testPLPSwatches(myPDP);

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));
		// this is a test to see that the selected swatch color on the PLP
		// displays
		// when the PDP loads
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathMensTab()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myMen.getTheByXPathMensTShirts()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		myPLP.chooseSwatch( "5775",
				myPDP);
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPDP.getThePageName(), "5775");
		ss = ss + 1;

		myPDP.selectSubProductByPrice(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathAccessories(),
				myMen.getTheByLinkSocks(), socksProduct.getPrice(), socksProduct.getQty(), 
				socksProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPDP.getThePageName(), socksProduct.getPrice());
		ss = ss + 1;

		myPDP.selectSubProductByProduct(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathAccessories(),
				myMen.getTheByLinkSunglasses(),sunglassesProduct.getProductId(), sunglassesProduct.getQty());

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPDP.getThePageName(), sunglassesProduct.getProductId());
		ss = ss + 1;

		myEnvironment.setTestPassed(true);

	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Search & Swatch Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This is the Search & Swatch test.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+
									
				myEnvironment.addSSToFile(testName, mySearchResultPage.getThePageName(),
						"This is the Search Results Page.")
				+
				
				myEnvironment.addSSToFile(testName, mySearchResultPage.getThePageName() + mySearchResultPage.getTheShowAllMessage(),
						"This is a Show All Search Result Page.")
				+
				
				myEnvironment.addSSToFile(testName, mySearchResultPage.getThePageName()+ mySearchResultPage.getTheRatingsMessage(),
						"This is a Show All Search Result Page.")
				+
				
				myEnvironment.addSSToFile(testName, mySearchResultPage.getThePageName()+ mySearchResultPage.getThePriceLowToHighMessage(),
						"This is the Search Results sorted by price low to high.")
				+
				
				myEnvironment.addSSToFile(testName, mySearchResultPage.getThePageName() + mySearchResultPage.getThePriceHighToLowMessage(),
						"This is the Search Results sorted by price low to high.")
				+

				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myProducts.getTheColor(),
						"This is the PDP of a product with the color: 3426, which is the color, Melonball. This product was chosen by searching through swatches until the correct color was found.")
				+

						myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + socksProduct.getName() + socksProduct.getPrice(),
						"This is the Socks PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + socksProduct.getName() + socksProduct.getPrice() ,
						"This is the Socks PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + socksProduct.getName() + socksProduct.getPrice() ,
						"This is the Socks PDP with Preview pane displaying added product.")
				+

						myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + sunglassesProduct.getName() + sunglassesProduct.getProductId(),
						"This is the Socks PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + sunglassesProduct.getName() + sunglassesProduct.getProductId() ,
						"This is the sunglasses PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + sunglassesProduct.getName() + sunglassesProduct.getProductId() ,
						"This is the sunglasses PDP with Preview pane displaying added product.")
				+

			  myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Search & Swatch Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This is the Search & Swatch test.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(mySearchResultPage.getThePageName());
		fns.add(mySearchResultPage.getThePageName() + mySearchResultPage.getTheShowAllMessage());
		fns.add(mySearchResultPage.getThePageName()+ mySearchResultPage.getTheRatingsMessage());
		fns.add(mySearchResultPage.getThePageName() + mySearchResultPage.getThePriceHighToLowMessage());
		fns.add(mySearchResultPage.getThePageName() + mySearchResultPage.getThePriceLowToHighMessage());
		fns.add(myPDP.getThePageName() + myProducts.getTheColor());
		fns.add(myPLP.getThePageName()  + socksProduct.getName() + socksProduct.getPrice() );
		fns.add(myPDP.getThePageName() + socksProduct.getName() + socksProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + socksProduct.getName() + socksProduct.getPrice() );
		fns.add(myPLP.getThePageName()  + sunglassesProduct.getName() + sunglassesProduct.getProductId() );
		fns.add(myPDP.getThePageName() + sunglassesProduct.getName() + sunglassesProduct.getProductId()  );
		fns.add(myPDP.getThePreviewPaneName() + sunglassesProduct.getName() + sunglassesProduct.getProductId() );
	

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();

	}
}
