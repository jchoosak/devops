package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.LandingPage;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class FFSplitTenderCanadaOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// this is the class where all enviromental details are kept: what site is
	// being

	private SignInPage mySignInPage;
	private Header myHeader;
	private Men myMen = new Men();
	private ProductListingPage myPLP;
	private ProductDetailPage myPDP;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftService;
	private AddressPage myAddressPage = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewPage = new PreviewPage();
	private ConfirmationPage myConfirmationPage = new ConfirmationPage();
	private AccountPage myAccountPage;
	private PayTech myPayTech = new PayTech();
	private ContactCenter myContactCenter = new ContactCenter();
	private LandingPage myLandingPage = new LandingPage();
	private FiftyOne myFiftyOne = new FiftyOne();
	private HomeDecor myHomeDecor = new HomeDecor();
	private TraditionalGiftCard myTraditionalCard = new TraditionalGiftCard();
	private Product myProducts = new Product();
	
	private Product highPricePantsProduct = new Product("128.00", "1", "Pants");
	private Product lowPricePantsProduct = new Product("98.00", "1", "LowPricedPants");
	private Product homeProduct = new Product( "TH31823", "1", "Dinnerware");
	private Product golfProduct = new Product("TBG-099", "1", "GolfBag");

	private String theEnvironment = "";
	private String ShippingPrice = "";

	// url to be checked on various pages
	private String url = "";
	private String theOrderNumber = "";
	private int ss = 0;
	private String testName = "STCanadaOrderTest";
	

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}
	
	private WebDriver driver;

	@Before
	public void openBrowser() {

		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		

		myEnvironment = new SeleniumEnvironment(driver);
		 myAccountPage = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftService = new GiftService(driver, myEnvironment, myShoppingBag);
			myHeader = new Header(driver, myEnvironment);
			 myPLP = new ProductListingPage(driver, myEnvironment);
			 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
					 myHeader, testName);
		 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySignInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
		 
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myAccountPage.getThePageName());
			fns.add(myLandingPage.getThePageName());
			fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
			fns.add(myPDP.getThePageName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
			fns.add(myPLP.getThePageName()  + golfProduct.getName());
			fns.add(myPDP.getThePageName() + golfProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
			fns.add(myPDP.getThePageName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox());
			fns.add(myGiftService.getTheGiftServicesModal() +  myGiftService.getTheAddToGiftBox1());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
			fns.add(myShoppingBag.getThePageName() +  myGiftService.getTheGiftServiceShippingError());
			fns.add(myAddressPage.getTheAddressPagePreFilled());
			fns.add(myPaymentPage.getThePageName());
			fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
			fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardApplied());
			fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());
			fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
			fns.add(myPayTech.getThePageName() + theOrderNumber + "");
			fns.add(myPayTech.getThePageName() + myPaymentPage.getTheGiftCardNumber());
			
			String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
			myEnvironment.removeSS(fns, tempFile);
			
	}

	@Test
	public void TheSplitTenderCanadaOrderTest() throws InterruptedException, IOException {

		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// WebDriverWait wait = new WebDriverWait(driver,
		// TimeSpan.FromSeconds(30));
		WebElement currentElement;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());

		myEnvironment.setTestSubject("Canada Split Tender Order Test Results");
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Signe into an canada account
		mySignInPage.signIn(
				mySignInPage.getTheCanadaTenUserName(), mySignInPage.getThePassword());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountPage.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myAccountPage.getThePageName(), "");
		ss = ss + 1;
		// Go to home page
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		currentElement.click();

		Thread.sleep(5000);
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "HomePage", "Canada");
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeader
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// // Add the Gift Card
		myTraditionalCard.addTraditionalCard(myEnvironment, driver,
				myHeader, myMen, testName, "PDPTraditionalGC", "");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		 home = driver.findElement(By.xpath(myHeader
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);
		
		myPDP.selectProductByPrice(myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathPants(), lowPricePantsProduct.getPrice(), lowPricePantsProduct.getQty(),
				lowPricePantsProduct.getName() );

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice());

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// Add second restricted product
		myPDP.selectProduct(
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathGolf(), golfProduct.getProductId(), golfProduct.getQty(),
				golfProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				golfProduct.getName());

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myPDP.selectProduct(				
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathDinnerware(),
				homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				homeProduct.getProductId());

		myPDP.selectProductByPrice(myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathPants(), highPricePantsProduct.getPrice(), highPricePantsProduct.getQty(),
				highPricePantsProduct.getName() );

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				highPricePantsProduct.getName() + highPricePantsProduct.getPrice());
		myEnvironment.waitForPageLoaded();

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myHeader.getTheByIdPreviewCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
    myEnvironment.getThe_Default_Sleep();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "Products");
		ss = ss + 1;
		// add gift services to the products
		myGiftService.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		myGiftService.addToGiftBox( myShoppingBag.getTheFirstGiftLink(),
				testName, myEnvironment.getFfBrowser());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		myGiftService.giftBox(
				myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getFfBrowser());
		ss = ss + 1;
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "ProductsWithGiftServices");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;

		// continue to address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "GiftServicesAndShippingErrors");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

		System.out.println(currentElement.getText());
		System.out.println(myShoppingBag.getTheShippingRestrictionText());
		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRestrictionText()));
		// String errorColor =
		// driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).GetCssValue("Color");
		String errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(182, 72, 59)"));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheCustomsGiftServiceErrorText()));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).Text.Contains(myShoppingBag.CustomsGiftServiceErrorText));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).Text.Contains(myShoppingBag.TraditionalGiftCardCanadaErrorText));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstRowGiftServicesError()));
		System.out.println("This is the GiftServicesError:       "
				+ currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheSecondRowGiftServivesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
	/*	currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheLastRowGiftServicesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myShoppingBag.getTheThirdRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
		currentElement = myEnvironment
				.waitForDynamicElement( By.xpath(myShoppingBag
						.getTheFourthRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

		// checks for the yellow outline
		myShoppingBag.checkProductsForYellowOutline(driver, myEnvironment,
				7);*/

		// see that the shipping restriction message is present in the shopping
		// bag
		/*
		 * currentElement = myEnvironment.waitForDynamicElement(
		 * By.xpath(myShoppingBag.getTheFirstShippingRestriction()));
		 * System.
		 * out.println(currentElement.findElement(By.tagName("a")).getText());
		 * Assert.assertTrue(currentElement.getText().contains(
		 * "Shipping restrictions apply"));
		 * Assert.assertTrue(currentElement.findElement
		 * (By.tagName("a")).getText().contains("DETAILS")); currentElement =
		 * myEnvironment.waitForDynamicElement(
		 * By.xpath(myShoppingBag.getTheSecondShippingRestriction()));
		 * System
		 * .out.println(currentElement.findElement(By.tagName("a")).getText());
		 * Assert.assertTrue(currentElement.getText().contains(
		 * "Shipping restrictions apply"));
		 * Assert.assertTrue(currentElement.findElement
		 * (By.tagName("a")).getText().contains("DETAILS")); currentElement =
		 * myEnvironment.waitForDynamicElement(
		 * By.xpath(myShoppingBag.getTheThirdShippingRestriction()));
		 * System.
		 * out.println(currentElement.findElement(By.tagName("a")).getText());
		 * Assert.assertTrue(currentElement.getText().contains(
		 * "Shipping restrictions apply"));
		 * Assert.assertTrue(currentElement.findElement
		 * (By.tagName("a")).getText().contains("DETAILS"));
		 */
		// remove all the products that cannot be shipped to Canada
		myShoppingBag.removeProducts(2);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "AddressPage", "PreFilled");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// removes the gift services from remaining products
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftService.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftService.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		// currentElement.click(); Thread.sleep(2000);
		// Go Back to United States
		myHeader.selectCountry(driver, myEnvironment, myFiftyOne,
				myFiftyOne.getTheUnitedStatesFlag(), testName);
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		// click the first image to add more qty
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPDP.getTheMainImage()));
		myPDP.addQuantityToBag("8");

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "Normal");

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);
		
		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));

		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressPage.getTheURL()));

		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);

		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		 Thread.sleep(myEnvironment.getThe_Default_Sleep());
		 
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheBottomCCBtn()));
		myHeader.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "PaymentPage", "");
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));

		// Checks to see if the shipping price is correct on the payment page.
		ShippingPrice = "";
		ShippingPrice = currentElement.getText();
		ShippingPrice = ShippingPrice.substring(1);
		// TheShippingPrice = Double.parseDouble(ShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPayment.getTheActualCanadaShippingPrice());

		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// use the giftcard for partial payment
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardPinInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardPin());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardBtn()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheGiftCardPartialPayment()));
		// String giftCardPPColor = currentElement.getCssValue("Color");
		System.out.println(currentElement.getText().trim());
		String thePartialPayment = currentElement.getText().trim();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "PaymentPage", "GiftCardPayment");

		Assert.assertTrue(thePartialPayment.contains("-$25.00"));
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;
		// waits for preview page to load
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewPage.getTheTotalTax()));
		myHeader.checkUrl(myPreviewPage.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "PreviewPage", "");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = currentElement.getText();
		// String theDuty =
		// driver.findElement(By.id(myPreviewPage.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewPage.getTheSplitTenderSubTotal())).getText();
		String theShipping = driver
				.findElement(By.xpath(myPreviewPage.getTheShippingAmount()))
				.getText().trim();
		thePartialPayment = driver
				.findElement(By.xpath(myPreviewPage.getTheGCPartialPayment()))
				.getText().trim();
		String theGC = driver.findElement(By.xpath(myPreviewPage.getTheUSGC()))
				.getText();
		System.out.println(theShipping);
		System.out.println(thePartialPayment);

		// Assert.True(thePartialPayment.Contains("GIFT CARD:"));
		Assert.assertTrue(thePartialPayment.contains("$25.00"));
		System.out.println(thePartialPayment);
		Assert.assertTrue(theShipping.contains(myPaymentPage.getTheActualCanadaShippingString()));
		System.out.println(theShipping);
		System.out.println(theGC);
		System.out.println(myPaymentPage.getTheGiftCardNumber().trim());
		Assert.assertTrue(theGC.contains(myPaymentPage.getTheGiftCardNumber()
				.trim()));

		// Submit the order
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPreviewPage.getTheByCSSSubmitButton()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationPage.getTheOrderNumber()));

		myHeader.checkUrl(myConfirmationPage.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ConfirmationPage", "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		String orderNumber = currentElement.getText();
		theOrderNumber = orderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		Assert.assertTrue(myPaymentPage.getTheActualCanadaShippingString().contains(driver
				.findElement(
						By.xpath(myConfirmationPage.getTheShippingAmount()))
				.getText().trim()));

		// Insert a comma for how totals appear in contact center and OG
		

		myContactCenter = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, theOrderNumber, testName,
				myEnvironment.getFfBrowser());
		
		String theContactTotal = myContactCenter.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenter.checkOrderInCC(theContactTotal);
		
		
		ss = ss + 1;
		
		
		myPayTech = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getFfBrowser());
		
		
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		System.out.println(theTotal);
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		Double theGCTotal = 0.0;
		theGCTotal = myPaymentPage.pullPrice(myPayTech
				.getTheGCAddedAmount());
		theDTotal = theDTotal - theGCTotal;
		String newTotal = "";
		
		
		
		
		newTotal = myPayTech
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
			myPayTech
		.checkOrderInPayTech(newTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ss = ss + 1;
		// Adds money back to the gift card so the script is ready to run again
		myPayTech.addMoneyToCard(
				myPaymentPage.getTheGiftCardNumber(),
				myPaymentPage.getTheGiftCardPin(),
				myPaymentPage.getTheGiftCardAddAmount());
		ss = ss + 1;
		// Go to home page

		myEnvironment.setTestPassed(true);
		}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));

		
		bw.write("<html><body><center><h2>Canada Split Tender Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The Canada Split Tender Order Test"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is Canada. This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

						myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
								"This is the blank Sign-In Page.")
						+
						
						myEnvironment.addSSToFile(testName, myAccountPage.getThePageName(),
								"This is the Account Page.")
						+
											
						myEnvironment.addSSToFile(testName, myLandingPage.getThePageName(),
								"This is the Canada Home Page.")
						+
						
						myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(),
					     	"This is the GiftCard PDP.")
			    	+
				
				myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() ,
						"This is the Pants PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() ,
						"This is the Pants PDP with Preview pane displaying added product.")
				+

						
			myEnvironment.addSSToFile(testName, myPLP.getThePageName() + golfProduct.getName(),
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + golfProduct.getProductId(),
						"This is the Golf PDP.")
				+
	
								
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + golfProduct.getProductId(),
						"This is the Golf PDP with Preview pane displaying added product.")
				+
				
				myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+
		
		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+
				
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+
				
		myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() ,
						"This is the Pants PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() ,
						"This is the Pants PDP with Preview pane displaying added product.")
				+
						

				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName(),
						"This is the Shopping Bag with products.")
				+
				
				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox(),
						"This is the Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal()+ myGiftService.getTheAddToGiftBox1(),
						"This is the Add To Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText(),
						"This is the Gift Message Modal.")
				+
						
					myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(),
						"This is the Shopping Bag containing Products with Gift Services.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceShippingError(),
						"This is the Shopping Bag containing Products with different types of error messages.")
				+
				
				
					myEnvironment.addSSToFile(testName, myAddressPage.getTheAddressPagePreFilled(),
						"This is the Address Page with pre populated fields.")
				+

					myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName(),
								"This is the Payment Page.")
						+

						myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut(),
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, myPreviewPage.getThePageName(),
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, myConfirmationPage.getThePageName(),
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, myContactCenter.getThePageName() + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + theOrderNumber,
						"This is the PaymenTech Page.")
				+			    
					
						myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + myPaymentPage.getTheGiftCardNumber(),
								"This is the approved add money to gift card PaymenTech Page.")
					    
						
						+				
			         myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Split Tender Order</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This is the Canada Split Tender Order test.</h3>"
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myAccountPage.getThePageName());
		fns.add(myLandingPage.getThePageName());
		fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
		fns.add(myPDP.getThePageName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
		fns.add(myPLP.getThePageName()  + golfProduct.getName());
		fns.add(myPDP.getThePageName() + golfProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPLP.getThePageName() + homeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
		fns.add(myPLP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
		fns.add(myPDP.getThePageName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox());
		fns.add(myGiftService.getTheGiftServicesModal() +  myGiftService.getTheAddToGiftBox1());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
		fns.add(myShoppingBag.getThePageName() +  myGiftService.getTheGiftServiceShippingError());
		fns.add(myAddressPage.getTheAddressPagePreFilled());
		fns.add(myPaymentPage.getThePageName());
		fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
		fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardApplied());
		fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());
		fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
		fns.add(myPayTech.getThePageName() + theOrderNumber + "");
		fns.add(myPayTech.getThePageName() + myPaymentPage.getTheGiftCardNumber());

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();

	}
	}