package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.thoughtworks.selenium.Selenium;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.Women;
import TommyBahamaRepository.WomenSwim;

@RunWith(JUnit4.class)
public class FFWomenIndexPages {
	// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private SeleniumEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private Header myHeader;
		private Women myWomen = new Women();
		private WomenSwim myWomanSwim = new WomenSwim();
		private String testName = "WomenIndexPageTest";

	
		// The object which is how all interaction with web elements is made.
		private int ss = 0;

		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
	
		
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeader = new Header(driver, myEnvironment);
		
		List<String> fns = new ArrayList<String>();
		fns.add(myWomen.getTheWomensIndex());
		fns.add(myWomen.getTheWomensAccessoriesIndex());
		fns.add(myWomen.getTheWomensSwimShopIndex());
		fns.add(myWomen.getTheWomensBeachAccessoriesIndex());
		
		String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getBrowser() +  "/" + testName;
		myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void WomenIndexPageTest() throws InterruptedException, IOException {
		WebElement ce;
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setTestSubject("Women's Index Page Test Results");

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathWomensTab()));
		ce.click();
		
		
		myEnvironment.waitForDocumentReadyState();  
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myWomen.getTheWomensIndex(), "");
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
//		WebElement home = driver.findElement(By.xpath(myHeader
//				.getTheFooterEmailBtn()));
	//	js.executeScript(mouseOverScript, home);

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myWomen.getTheByXPathAccessories()));
		ce.click();
		myEnvironment.waitForDocumentReadyState();  
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myWomen.getTheWomensAccessoriesIndex(), "");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(myWomen.getTheByLinkSwimShop()));
		ce.click();
		myEnvironment.waitForDocumentReadyState();  
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myWomen.getTheWomensSwimShopIndex(), "");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myWomanSwim.getTheByXPathBeachAccessories()));
		ce.click();
		myEnvironment.waitForDocumentReadyState();  
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getSafariBrowser(),
				testName, myWomen.getTheWomensBeachAccessoriesIndex(), "");
		ss = ss + 1;

		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		
		bw.write("<html><body><center><h2>Different Women's Index Pages</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This test is just to see if the Women's Index Pages are displaying correctly.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+
				
				myEnvironment.addSSToFile(testName, myWomen.getTheWomensIndex(),
						"This is the Women's Index page.")
				+
				
				myEnvironment.addSSToFile(testName, myWomen.getTheWomensAccessoriesIndex(),
						"This is the Women's Accessories Index page.")
				+
									
				myEnvironment.addSSToFile(testName, myWomen.getTheWomensSwimShopIndex(),
						"This is the Women's Swim Shop Index page.")
				+

		    	myEnvironment.addSSToFile(testName, myWomen.getTheWomensBeachAccessoriesIndex(),
						"This is the Women's Beach Accessories Index page.")
				+

				myEnvironment.getPageTestOutcome()
				
				+ "</center></body></html>");
		bw.close();
		
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Different Women's Index Pages</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This test is just to see if the Women's Index Pages are displaying correctly.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(myWomen.getTheWomensIndex());
		fns.add(myWomen.getTheWomensAccessoriesIndex());
		fns.add(myWomen.getTheWomensSwimShopIndex());
		fns.add(myWomen.getTheWomensBeachAccessoriesIndex());

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();

	}

}