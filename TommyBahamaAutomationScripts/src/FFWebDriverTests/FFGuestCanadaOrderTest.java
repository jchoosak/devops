package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

public class FFGuestCanadaOrderTest {

	// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private SeleniumEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private SignInPage mySingInPage;
		private Header myHeader;
		private Men myMenObjs = new Men();
		private ProductListingPage myPLP;
		private ProductDetailPage myPDP;
		private ShoppingBagPage myShoppingBag;
		private GiftService myGiftService;
		private AddressPage myAddressPage = new AddressPage();
		private PaymentPage myPaymentPage;
		private PreviewPage myPreviewPage = new PreviewPage();
		private ConfirmationPage myConfirmationPage = new ConfirmationPage();
		private PayTech myPayTech = new PayTech();
		private ContactCenter myContactCenter = new ContactCenter();
		private HomeDecor myHomeDecor = new HomeDecor();
		
		
		private Product myProducts = new Product();
		private Product midPricedTshirt = new Product("$68.00", "2", "midPricedTshirt");
		private Product homeProduct = new Product( "TH31823", "1", "homeProduct");
        private Product shoeProduct = new Product("TFM00206", "1", "Shoes");
        private Product golfProduct = new Product("TBG-099", "1", "GolfBag");
		
	private String theEnvironment = "";
	int ss = 0;
	double theShippingPrice = 16.00;
	String OrderNumber = "";
	String testName = "TheGuestCanadaOrderTest";
	// url to be checked on various pages
	private String url = "";
	// Main object in the test object is per browser and is used for all
	// interactivity with web page elements.
	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
	
		
		myEnvironment = new SeleniumEnvironment(driver);
		myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		myGiftService = new GiftService(driver, myEnvironment, myShoppingBag);
		myHeader = new Header(driver, myEnvironment);
		 myPLP = new ProductListingPage(driver, myEnvironment);
		 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
				 myHeader, testName);
		 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySingInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
		 String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + midPricedTshirt.getName() + midPricedTshirt.getPrice() );
			fns.add(myPDP.getThePageName() + midPricedTshirt.getName() + midPricedTshirt.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + midPricedTshirt.getName() + midPricedTshirt.getPrice() );
			fns.add(myPLP.getThePageName() + shoeProduct.getName());
			fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + golfProduct.getName());
			fns.add(myPDP.getThePageName() + golfProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftWrap());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
			fns.add(myAddressPage.getTheGuestAddressPage());
			fns.add(myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors());
			fns.add(myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceError());
			fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());
			fns.add(myContactCenter.getThePageName() + OrderNumber + "");
			fns.add(myPayTech.getThePageName() + OrderNumber + "");
			
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheGuestCanadaOrderTest() throws InterruptedException, IOException {
		WebElement currentElement;
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment.setTestSubject("Guest Canada Order Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// //adds a t-shirt to the shopping bag

		myPDP.selectProduct(				
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathDinnerware(),
				homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				homeProduct.getProductId());

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeader
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myPDP.selectProductByPrice(
				myHeader.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathMensTShirts(), midPricedTshirt.getPrice(), midPricedTshirt.getQty(), 
				midPricedTshirt.getName());
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "ItemPreview",
				midPricedTshirt.getName());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// add shoes to the bag
		myPDP.selectProduct(
				myHeader.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), shoeProduct.getProductId(), shoeProduct.getQty(), 
				shoeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				shoeProduct.getName());
		
		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// Add second restricted product
		myPDP.selectProduct(
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathGolf(), golfProduct.getProductId(), golfProduct.getQty(),
				golfProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				golfProduct.getName());

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myHeader.getTheByIdPreviewCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		// add gift services to the products
		myGiftService.giftBox(myShoppingBag.getTheThirdGiftLink(), testName,
				myEnvironment.getFfBrowser());
		ss = ss + 1;
		Thread.sleep(4000);
		myGiftService.giftBox(myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		ss = ss + 1;
		myGiftService.addToGiftBox(myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		ss = ss + 1;
		Thread.sleep(4000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBagProducts", "WithGiftServices");
		ss = ss + 1;
		driver.findElement(By.xpath(myShoppingBag.getTheContinueCheckout()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySingInPage.getTheContinueAsGuestBtn()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdNewEmail()));
		currentElement.sendKeys(myEnvironment.getTheEcommEmailFive());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdConfirmedEmail()));
		currentElement.sendKeys(myEnvironment.getTheEcommEmailFive());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdPhone1()));
		currentElement.sendKeys("253");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdPhone2()));
		currentElement.sendKeys("205");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdPhone3()));
		currentElement.sendKeys("1750");

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingCountry()));
		currentElement = currentElement.findElement(By
				.cssSelector(myAddressPage.getTheCanadaCountry()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingFName()));
		currentElement.sendKeys(myEnvironment.getTheUsShopperFName());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingMName()));
		currentElement.sendKeys(myEnvironment.getTheUsShopperMName());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingLName()));
		currentElement.sendKeys(myEnvironment.getTheUsShopperLName());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingAddress()));
		currentElement.sendKeys(myAddressPage.getTheCanadaAddress());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingCity()));
		currentElement.sendKeys(myAddressPage.getTheCanadaCity());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingState()));

		Select clickThis = new Select(currentElement);
		clickThis.selectByValue(myAddressPage.getTheCanadaState());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingZipCode()));
		currentElement.sendKeys(myAddressPage.getTheCanadaZip());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressPage.getTheURL()));

		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "AddressPage", "Guest");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		 myEnvironment.getThe_Default_Sleep();
		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "OversizedShippingErrors");
		ss = ss + 1;
		System.out.println(currentElement.getText());

		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRestrictionText()));
		String errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(182, 72, 59)"));

//		currentElement = myEnvironment
//				.waitForDynamicElement( By.xpath(myShoppingBagObjs
//						.getTheFourthRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBagObjs.getTheShippingRemovalText()));
//		errorColor = currentElement.getCssValue("Color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheThirdRemoveLink()));
		currentElement.click();
	
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheThirdRemoveLink()));
		currentElement.click();
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheThirdRemoveLink()));
		currentElement.click();
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySingInPage.getByXPathSignInButton()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySingInPage.getTheContinueAsGuestBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));

		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "WithGiftServicesErrors");
		ss = ss + 1;
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftService.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));

		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));

		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftService.getTheByXPathRemoveGiftServices()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySingInPage.getTheContinueAsGuestBtn()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText("CONTINUE AS A GUEST"));

		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingCountry()));
		currentElement = currentElement.findElement(By
				.cssSelector(myAddressPage.getTheCanadaCountry()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
	
		
		
		
		myEnvironment.waitForTitle("Payment");
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheBottomCCBtn()));
		myHeader.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());
		
		System.out.println("This should be the payment page.      " + driver.getCurrentUrl());

		// * This is commented out because currently we are under a month
		// * of free shipping, will uncomment when shipping is returned to
		// normal for US orders.

		// Checks to see if the shipping price is correct on the payment page.
	/*	String ShippingPrice = "";
		ShippingPrice = driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText();
		ShippingPrice = ShippingPrice.substring(1);
		theShippingPrice = Double.parseDouble(ShippingPrice);
		Assert.assertTrue(theShippingPrice == myPaymentPage
				.getTheActualCanadaShippingPrice());*/

		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		// waits for preview page to load
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewPage.getTheSubTotal()));
		myHeader.checkUrl(myPreviewPage.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPreviewPage.getThePageName(), "Page");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalTax())).getText();
		//String theDuty = driver.findElement(
		//		By.id(myPreviewPage.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewPage.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewPage.getTheByCSSSubmitButton()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationPage.getTheOrderNumber()));

		myHeader.checkUrl(myConfirmationPage.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationPage.getTheOrderNumber())).getText());
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationPage.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myConfirmationPage.getThePageName(), "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		OrderNumber = driver.findElement(
				By.xpath(myConfirmationPage.getTheOrderNumber())).getText();
		OrderNumber = OrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		Assert.assertTrue(myPaymentPage.getTheActualCanadaShippingString().contains(driver
				.findElement(
						By.id(myConfirmationPage.getTheByIdTotalShipping()))
				.getText().trim()));

		// Insert a comma for how totals appear in contact center and OG
		

		myContactCenter = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, OrderNumber, testName,
				myEnvironment.getFfBrowser());
		
		String theContactTotal = myContactCenter.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenter.checkOrderInCC(theContactTotal);
		
		ss = ss + 1;
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		String newTotal = "";
		
		myPayTech = new PayTech(driver, myEnvironment,  
				OrderNumber, testName, myEnvironment.getFfBrowser());
		
		newTotal = myPayTech
				.formatTotal( newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
			myPayTech
		.checkOrderInPayTech(newTotal);
		ss = ss + 1;
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Guest Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Guest Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ myEnvironment.getTestTextDescription()
						+ myEnvironment.getBrowser()
						+ "\\"
						+ testName
						+ ".jpeg</b></p></td></tr></table><br/>"
						+

						myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(),
								"This is the GiftCard PDP.")
						+
						
							myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+		
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+
						
		myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + midPricedTshirt.getName() + midPricedTshirt.getPrice(),
						"This is the T-Shirt PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + midPricedTshirt.getName() + midPricedTshirt.getPrice() ,
						"This is the T-Shirt PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + midPricedTshirt.getName() + midPricedTshirt.getPrice() ,
						"This is the T-Shirt PDP with Preview pane displaying added product.")
				+
						
			myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  shoeProduct.getName(),
						"This is the Shoes PLP.")
				+		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + shoeProduct.getProductId(),
						"This is the Shoe PDP.")
				+		
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + shoeProduct.getProductId(),
						"This is the Shoe PDP with Preview pane displaying added product.")
				+
						
	myEnvironment.addSSToFile(testName, myPLP.getThePageName() + golfProduct.getName(),
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + golfProduct.getProductId(),
						"This is the Golf PDP.")
				+	
								
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + golfProduct.getProductId(),
						"This is the Golf PDP with Preview pane displaying added product.")
				+

			  		myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText(),
								"This is the Gift Message Modal.")
						+

					myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox(),
								"This is the Gift Box Modal.")
						+

					myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftWrap(),
								"This is the Shopping Bag with the Gift Wrap Modal.")
						+

					myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(),
								"This is the Shopping Bag containing Products with Gift Services.")
						+
						
						myEnvironment.addSSToFile(testName, myAddressPage.getTheGuestAddressPage(),
								"This is the Address Page for a Guest User.")
						+
						
						myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors(),
								"This is the Shopping Bag with Oversized and other Shipping Errors Displayed.")
						+

						myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceError(),
								"This is the Shopping Bag containing Products with Gift Services error messages.")
						+

					myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut(),
								"This is the Payment Page after Credit Card information entered.")
						+

					myEnvironment.addSSToFile(testName, myPreviewPage.getThePageName(),
								"This is the Preview Page.")
						+
						
						myEnvironment.addSSToFile(testName, myConfirmationPage.getThePageName(),
								"This is the Confirmation Page.")
						+
						
						myEnvironment.addSSToFile(testName, myContactCenter.getThePageName() + OrderNumber,
								"This is the Contact Center Page.")
						+
						
						myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + OrderNumber,
								"This is the PaymenTech Page.")
						+
					myEnvironment.getPageTestOutcome()
						+ "</center></body></html>");
		bw.close();
	

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Guest Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Guest Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPLP.getThePageName() + homeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
		fns.add(myPLP.getThePageName()  + midPricedTshirt.getName() + midPricedTshirt.getPrice() );
		fns.add(myPDP.getThePageName() + midPricedTshirt.getName() + midPricedTshirt.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + midPricedTshirt.getName() + midPricedTshirt.getPrice() );
		fns.add(myPLP.getThePageName() + shoeProduct.getName());
		fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
		fns.add(myPLP.getThePageName()  + golfProduct.getName());
		fns.add(myPDP.getThePageName() + golfProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftWrap());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox());
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
		fns.add(myAddressPage.getTheGuestAddressPage());
		fns.add(myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors());
		fns.add(myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceError());
		fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());
		fns.add(myContactCenter.getThePageName() + OrderNumber + "");
		fns.add(myPayTech.getThePageName() + OrderNumber + "");

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}

