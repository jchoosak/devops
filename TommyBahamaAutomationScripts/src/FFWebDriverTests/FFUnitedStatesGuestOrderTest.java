package FFWebDriverTests;



import org.junit.After;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.testng.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.LandingPage;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

@RunWith(JUnit4.class)
public class FFUnitedStatesGuestOrderTest {

	// this is the class where all enviromental details are kept: what site is
			// being
			// tested and any methods that interact with the driver object
			private SeleniumEnvironment myEnvironment;
			// The following are the different objects that make up the Tommy Bahama
			// repository that are used throught this scipt.
			private SignInPage mySignInPage;
			private Header myHeader;
			private Men myMen = new Men();
			private ProductListingPage myPLP;
			private ProductDetailPage myPDP;
			private ShoppingBagPage myShoppingBag;
			private GiftService myGiftService;
			private AddressPage myAddressPage = new AddressPage();
			private PaymentPage myPaymentPage;
			private PreviewPage myPreviewPage = new PreviewPage();
			private ConfirmationPage myConfirmationPage = new ConfirmationPage();
			private PayTech myPayTech = new PayTech();
			private ContactCenter myContactCenter = new ContactCenter();
			private LandingPage myLandingPage = new LandingPage();
			private HomeDecor myHomeDecor = new HomeDecor();
			private Product myProducts = new Product();

			private Product homeProduct = new Product( "TH31414", "2", "Dinnerware");
			private Product lowPriceTshirtProduct = new Product("58.00", "1", "LowPricedTshirt");
			  private Product shoeProduct = new Product("TFM00206", "1", "Shoes");
				private Product golfProduct = new Product("TBG-099", "1", "GolfBag");
			
		private String theEnvironment = "";
		private String url = "";
		private String theOrderNumber = "";
		private final String testName = "TheGuestUSOrderTest";
		private int ss = 0;

		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {

			// baseUrl = System.getProperty("webdriver.base.url");
			Assert.assertTrue(isSupportedPlatform());
			myEnvironment = new SeleniumEnvironment();
			File fileToProfile = new File(myEnvironment.getFfProfile());
			FirefoxProfile p = new FirefoxProfile(fileToProfile);
			p.setPreference("javascript.enabled", true);
			p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
			driver = new FirefoxDriver(p);
			
			myEnvironment = new SeleniumEnvironment(driver);
			myHeader = new Header(driver, myEnvironment);
			 myPLP = new ProductListingPage(driver, myEnvironment);
			 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
					 myHeader, testName);
			 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
			 myGiftService = new GiftService(driver, myEnvironment, myShoppingBag);
			 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
			 myPaymentPage = new PaymentPage(myEnvironment, testName);
			 mySignInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
			 
				List<String> fns = new ArrayList<String>();

				fns.add(myLandingPage.getThePageName());
				fns.add(myPLP.getThePageName() + homeProduct.getName());
				fns.add(myPLP.getThePageName() + homeProduct.getProductId());
				fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
				fns.add(myPLP.getThePageName()  + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice() );
				fns.add(myPDP.getThePageName() + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice()  );
				fns.add(myPDP.getThePreviewPaneName() + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice() );
				fns.add(myPLP.getThePageName() + shoeProduct.getName());
				fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
				fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
				fns.add(myPLP.getThePageName()  + golfProduct.getName());
				fns.add(myPDP.getThePageName() + golfProduct.getProductId());
				fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
				fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
				fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
				fns.add(myAddressPage.getTheGuestAddressPage());
				fns.add(myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors());
				fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
				fns.add(myPreviewPage.getThePageName());
				fns.add(myConfirmationPage.getThePageName());
				fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
				fns.add(myPayTech.getThePageName() + theOrderNumber + "");
				
				String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
				myEnvironment.removeSS(fns, tempFile);
			 
				
		}

		@Test
		public void TheGuestUSOrderTest() throws InterruptedException, IOException {

			WebElement ce;

			// Navigate to the testing environment
			driver.get(myEnvironment.getTheTestingEnvironment());

			// set implicit wait times so pages do not load due to all the crap that
			// is added by thrid parties
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			myEnvironment.setBrowser(myEnvironment.getFfBrowser());
			myEnvironment.setTestSubject("United States Guest Order Test Results");
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			// Go Back to United States
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheFooterEmailBtn()));

			myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
					testName, myLandingPage.getThePageName(), "");
			ss = ss + 1;
			
			
			// add shoes to the bag
			myPDP.selectProduct(
					myHeader.getTheByXPathMensTab(),
					myMen.getTheByXPathShoes(), shoeProduct.getProductId(), shoeProduct.getQty(), 
					shoeProduct.getName());

			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
					shoeProduct.getName() + shoeProduct.getPrice());


			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheFooterEmailBtn()));

			myPDP.selectProduct(
					myHeader.getTheByXPathHomeDecorTab(),
					myHomeDecor.getTheByXPathDinnerware(),
					homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());


			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
					homeProduct.getName() + homeProduct.getPrice());

			// //adds a t-shirt to the shopping bag
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheFooterEmailBtn()));
			myPDP.selectProductByPrice(	myHeader.getTheByXPathMensTab(),
					myMen.getTheByXPathMensTShirts(), lowPriceTshirtProduct.getPrice(), lowPriceTshirtProduct.getQty(),
					lowPriceTshirtProduct.getName());

			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
					lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice());

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheFooterEmailBtn()));
			
			
		

			// Add second restricted product
			myPDP.selectProduct(
					myHeader.getTheByXPathHomeDecorTab(),
					myHomeDecor.getTheByXPathGolf(), golfProduct.getProductId(), golfProduct.getQty(),
					golfProduct.getName());

			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
					golfProduct.getName());

			// continue to shopping bag
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeader.getTheByIdPreviewCheckout()));
			// ce = myEnvironment.waitForDynamicElement(
			// By.CssSelector(myHeader.getTheByCSSLogo()));
			// ce.Click();

			ce = myEnvironment.waitForDynamicElement(
					By.linkText(myHeader.getTheByLinkCheckout()));
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			// url = driver.Url;
			// ce = myEnvironment.waitForDynamicElement(
			// By.XPath(myShoppingBag.BottomContinueCheckout));
			// ce = myEnvironment.waitForDynamicElement(
			// By.LinkText("(//a[contains(getTheText()(),'Continue Checkout')])[2]"));
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myShoppingBag.getTheFirstImage()));
			// url = driver.Url;
			// ce = myEnvironment.waitForDynamicElement(
			// By.XPath(myShoppingBag.BottomContinueCheckout));
			// add gift services to the products
			// driver.Navigate().GoToUrl(url);
			myGiftService.giftBox(
					myShoppingBag.getTheThirdGiftLink(), testName,
					myEnvironment.getFfBrowser());
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			ss = ss + 1;
			// myGiftService.giftBox(driver, myEnvironment, myShoppingBag,
			// myShoppingBag.getTheFirstGiftLink(), testName,
			// myEnvironment.getFfBrowser());
			//
			//
			// myGiftService.giftBox(driver, myEnvironment, myShoppingBag,
			// myShoppingBag.getTheFirstGiftLink(), testName,
			// myEnvironment.getFfBrowser());
			//

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeader.getTheFooterEmailBtn()));
			
			// Test for all of the columns are present in the shopping bag
			myShoppingBag.checkShoppingBagColumns(driver);

			// Test that thumbnails are present for the items in the shopping bag
			Assert.assertTrue(myEnvironment.isElementPresent(
					By.xpath(myShoppingBag.getTheFirstImage())));

			// Test that the information details are present for the items in the
			// shopping bag
			Assert.assertTrue(myEnvironment.isElementPresent(
					By.xpath(myShoppingBag.getTheFirstItemDetails())));

			// Test that information details are showing for the remaining items.
			Assert.assertTrue(myEnvironment.isElementPresent(
					By.xpath(myShoppingBag.getTheSecondItemDetails())));
			Assert.assertTrue(myEnvironment.isElementPresent(
					By.xpath(myShoppingBag.getTheThirdItemDetails())));
			myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
					testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(), "");
			ss = ss + 1;

			driver.findElement(
					By.xpath(myShoppingBag.getTheBottomContinueCheckout()))
					.click();
			 myEnvironment.getThe_Default_Sleep();
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(mySignInPage.getTheContinueAsGuestBtn()));
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdNewEmail()));
			ce.sendKeys(myEnvironment.getTheEcommEmailOne());
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdConfirmedEmail()));
			ce.sendKeys(myEnvironment.getTheEcommEmailOne());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdPhone1()));
			ce.sendKeys("253");
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdPhone2()));
			ce.sendKeys("205");
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdPhone3()));
			ce.sendKeys("1750");

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingCountry()));
			ce = ce.findElement(By
					.cssSelector(myAddressPage.getTheUSCountry()));
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingFName()));
			ce.sendKeys(myEnvironment.getTheUsShopperFName());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingMName()));
			ce.sendKeys(myEnvironment.getTheUsShopperMName());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingLName()));
			ce.sendKeys(myEnvironment.getTheUsShopperLName());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingAddress()));
			ce.sendKeys(myAddressPage.getTheWAAddress());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingCity()));
			ce.sendKeys(myAddressPage.getTheWACity());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingState()));
			Select clickThis = new Select(ce);
			// ce.FindElement(By.CssSelector("option[value='WA']"));
			clickThis.selectByValue("WA");

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingZipCode()));
			ce.sendKeys(myAddressPage.getTheWAZipCode());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingFName()));
			ce.sendKeys(myEnvironment.getTheUsShopperFName());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingMName()));
			ce.sendKeys(myEnvironment.getTheUsShopperMName());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingLName()));
			ce.sendKeys(myEnvironment.getTheUsShopperLName());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingAddress()));
			ce.sendKeys(myAddressPage.getTheHawaiiAddress());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingCity()));
			ce.sendKeys(myAddressPage.getTheHawaiiCity());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingState()));
			clickThis = new Select(ce);
			clickThis.selectByValue("HI");
			// ce.Click();
			// myEnvironment.myWaitForWDElement(driver,
			// By.CssSelector("option[value='HI']"));
			// ce.FindElement(By.CssSelector("option[value='HI']"));
			// ce.Click();

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheShippingZipCode()));
			ce.sendKeys(myAddressPage.getTheHawaiiZipCode());

			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdSameAddress()));
			url = driver.getCurrentUrl();
			Assert.assertTrue(url.contains(myAddressPage.getTheURL()));

			// Shipping address same as billing
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdSameAddress()));
			// ce.Click();
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingZipCode()));
			String theZip = ce.getAttribute("value");
			System.out.println("This is the Zip on the Address page:         "
					+ theZip);
			myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
					testName, myAddressPage.getTheGuestAddressPage(), "");
			ss = ss + 1;

			// check to see if the url is secure on the payment page
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
			// Thread.Sleep(10000);
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			// continue from address page to verify shipping errors occurr
			ce = myEnvironment.waitForDynamicElement(
					By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

			myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
					testName, myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors(), "");
			ss = ss + 1;

			System.out.println(ce.getText());
			// Make sure to put these checks back once merch is fixed.
			// checks to see all products give the correct error messages
			Assert.assertTrue(ce.getText().contains(
					myShoppingBag.getTheHawaiiShippingText()));
			// String errorColor =
			// driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).GetCssValue("Color");
			String errorColor = ce.getCssValue("Color");
			System.out.println(errorColor);
			Assert.assertTrue(errorColor.contains(myShoppingBag.getTheRGBerrorColor()));
			// Assert.assertTrue(ce.getTheText().Contains(myShoppingBag.HawaiiShippinggetTheText()));

			if (myEnvironment.isElementPresent(By.xpath(myShoppingBag
					.getTheFifthRowShippingRemovalError()))) {
				ce = myEnvironment.waitForDynamicElement( By
						.xpath(myShoppingBag
								.getTheFifthRowShippingRemovalError()));
				Assert.assertTrue(ce.getText().contains(
						myShoppingBag.getTheShippingRemovalText()));
			} else {
				ce = myEnvironment
						.waitForDynamicElement(
								By.xpath(myShoppingBag.getTheFourthRowShippingRemovalError()));
			
				System.out.println(ce.getText());
				Assert.assertTrue(ce.getText().contains(
						myShoppingBag.getTheShippingRemovalText()));

			}
			errorColor = ce.getCssValue("Color");
			System.out.println(errorColor);
			Assert.assertTrue(errorColor.contains(myShoppingBag.getTheSecondRGBerrorColor()));

			/*ce = myEnvironment.waitForDynamicElement(
					By.xpath(myShoppingBag.getTheFifthRemoveLink()));
			ce.click();*/
			//*[@id="shopping-table"]/tbody/tr[8]/td[6]/a
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myShoppingBag.getTheFourthRemoveLink()));
			ce.click();
			
			 myEnvironment.getThe_Default_Sleep();

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(mySignInPage.getTheContinueAsGuestBtn()));
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdBillingCountry()));
			// ce =
			// ce.findElement(By.cssSelector(myAddressPage.getTheUSCountry()));
			// ce.click();
			 myEnvironment.getThe_Default_Sleep();
			ce = myEnvironment.waitForDynamicElement(
					By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));

			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			myEnvironment.waitForTitle("Payment");
			// ce = myEnvironment.waitForDynamicElement(
			// By.Id(myAddressPage.TopCheckoutButton));
			// Thread.sleep(10000);

			/*
			 * This is commented out because currently we are under a month of free
			 * shipping, will uncomment when shipping is returned to normal for US
			 * orders.
			 */
			// Checks to see if the shipping price is correct on the payment page.
			// ShippingPrice = "";
			// ShippingPrice =
			// driver.FindElement(By.XPath(myPayment.GroundShipping)).getTheText();
			// ShippingPrice = ShippingPrice.Remove(0, 1);
			// Double.TryParse(ShippingPrice, out TheShippingPrice);
			// Assert.assertEquals(TheShippingPrice,
			// myPayment.ActualCanadaShippingPrice);
			// myEnvironment.waitForDocumentReadyState(driver);
			// Checks to see that there is no other shipping options present
			// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPayment.getTheByXPathTwoDayShipping()),
			// driver));
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myPaymentPage.getTheBottomCCBtn()));
			
			ce = myEnvironment.waitForDynamicElement(By.xpath(myPaymentPage.getTheThreeDayShipping()));
			ce.click();
			 myEnvironment.getThe_Default_Sleep();
			// see that Card Services are working
			//myPaymentPage.checkGcBalence();
			
			// due to changes in OG there is a new window opened that now needs to be closed so focus 
			// can return to the payment page. 
			
		//	driver.close();

	//		myEnvironment.handleMultipleWindows(
		//			"Payment Information");

			// Enter CC info for payment page
			myPaymentPage.enterCCInfo();
			ss = ss + 1;

			// waits for preview page to load
			// Thread.Sleep(10000);
			myEnvironment.waitForDynamicElement(
					By.xpath(myPreviewPage.getTheTotalTax()));
			myHeader.checkUrl(myPreviewPage.getTheURL(),
					url = driver.getCurrentUrl());
			myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
					testName, "Preview", "Page");
			ss = ss + 1;

			// check to see if the tax is what it should be for a Canada order
			String theTax = driver.findElement(
					By.xpath(myPreviewPage.getTheTotalTax())).getText();
			String theTotal = driver.findElement(
					By.xpath(myPreviewPage.getTheTotalAmount())).getText();
			// String theShipping =
			// driver.findElement(By.xpath(myPreviewPage.getTheShippingAmount())).getText().trim();

			// Submit the order
			driver.findElement(
					By.cssSelector(myPreviewPage.getTheByCSSSubmitButton()))
					.click();
			 myEnvironment.getThe_Default_Sleep();
			myEnvironment.waitForDynamicElement(
					By.xpath(myConfirmationPage.getTheOrderNumber()));
			myHeader.checkUrl(myConfirmationPage.getTheURL(),
					url = driver.getCurrentUrl());
			System.out.println(driver.findElement(
					By.xpath(myConfirmationPage.getTheOrderNumber())).getText());
			Assert.assertTrue(driver
					.findElement(By.xpath(myConfirmationPage.getTheOrderNumber()))
					.getText().contains("ORDER CONFIRMATION NUMBER:"));
			myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
					testName, myConfirmationPage.getThePageName(), "");
			ss = ss + 1;

			theOrderNumber = driver.findElement(
					By.xpath(myConfirmationPage.getTheOrderNumber())).getText();
			theOrderNumber = theOrderNumber.substring(27);

			// Make sure that the Shipping amount is correct on preview and
			// confirmation page
			// Assert.assertTrue("$16.00" .equals(
			// driver.FindElement(By.XPath(myConfirmation.getTheShippingAmount())).getTheText().Trim());

			// runs the script to look up the order in Contact Center
			// Insert a comma for how totals appear in contact center and OG
		

			myContactCenter = new ContactCenter(driver, myEnvironment,
					theEnvironment, theTax, theOrderNumber, testName,
					myEnvironment.getFfBrowser());
			
			String theContactTotal = myContactCenter.getContactTotal(theTotal);
			// runs the script to look up the order in Contact Center
			myContactCenter.checkOrderInCC(theContactTotal);

			// remove the $ sign because money values in Contact center do not
			// dispaly the dollar sign
			Double theDTotal = 0.0;
			theDTotal = myPaymentPage.pullPrice(theTotal);
			String newTotal = "";
			myPayTech = new PayTech(driver, myEnvironment,  
					theOrderNumber, testName, myEnvironment.getFfBrowser());
			
			
			newTotal = myPayTech
					.formatTotal(newTotal, theDTotal);
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			// runs the script to look up the order in Paytech.
			
			
				myPayTech
			.checkOrderInPayTech(newTotal);
			ss = ss + 1;

			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws InterruptedException, MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>United States Guest Order Test Results</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+ "<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3 style= \"width:70%;\">The United States Guest Order Test verifies that a US order can be completed per requirements. This test also checks for different alerts that notify the user that certain functionality is restricted"
					+ " and makes sure these restrictions are properly displaying on the website when the current country"
					+ " is United States.</h3><table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ testName
					+ ".jpeg</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, myLandingPage.getThePageName(),
							"This is the Home Page.")
					+

								myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+
		
		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+
				
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+

					myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice(),
						"This is the T-Shirts PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice() ,
						"This is the T-Shirt PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice() ,
						"This is the T-Shirt PDP with Preview pane displaying added product.")
				+

								myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  shoeProduct.getName(),
						"This is the Shoes PLP.")
				+		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + shoeProduct.getProductId(),
						"This is the Shoe PDP.")
				+		
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + shoeProduct.getProductId(),
						"This is the Shoe PDP with Preview pane displaying added product.")
				+


				myEnvironment.addSSToFile(testName, myPLP.getThePageName() + golfProduct.getName(),
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + golfProduct.getProductId(),
						"This is the Golf PDP.")
				+
	
								
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + golfProduct.getProductId(),
						"This is the Golf PDP with Preview pane displaying added product.")
				+

						myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText(),
								"This is the Gift Message Modal.")
						+
					

						myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(),
								"This is the Shopping Bag containing Products with Gift Services.")
						+

					myEnvironment.addSSToFile(testName, myAddressPage.getTheGuestAddressPage(),
							"This is the Address Page for a Guest User.")
					+

					myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors(),
							"This is the Shopping Bag with Oversized and other Shipping Errors Displayed.")
					+

				myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
							"This is the Payment Page after Credit Card information entered.")
					+

				
		myEnvironment.addSSToFile(testName, myPreviewPage.getThePageName(),
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, myConfirmationPage.getThePageName(),
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, myContactCenter.getThePageName() + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + theOrderNumber,
						"This is the PaymenTech Page.")
				+
	                myEnvironment.getPageTestOutcome()
	                
					+ "</center></body></html>");
			
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>United States Guest Order Test Results</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+ "<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3 style= \"width:70%;\">The United States Guest Order Test verifies that a US order can be completed per requirements. This test also checks for different alerts that notify the user that certain functionality is restricted"
							+ " and makes sure these restrictions are properly displaying on the website when the current country"
							+ " is United States.</h3><table style= \"width:70%;\"><tr><td><p> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
			

			List<String> fns = new ArrayList<String>();

			fns.add(myLandingPage.getThePageName());
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice() );
			fns.add(myPDP.getThePageName() + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + lowPriceTshirtProduct.getName() + lowPriceTshirtProduct.getPrice() );
			fns.add(myPLP.getThePageName() + shoeProduct.getName());
			fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + golfProduct.getName());
			fns.add(myPDP.getThePageName() + golfProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
			fns.add(myAddressPage.getTheGuestAddressPage());
			fns.add(myShoppingBag.getThePageName() + myShoppingBag.getTheOversizedShippingErrors());
			fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());
			fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
			fns.add(myPayTech.getThePageName() + theOrderNumber + "");

			ss = ss - 8;
			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);

			System.out.println("This is the total number of SS		" + ss);
			driver.quit();
		}
	}
