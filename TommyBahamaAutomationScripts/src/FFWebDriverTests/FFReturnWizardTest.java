package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;

public class FFReturnWizardTest {

	private AccountPage myAccountObjs;
	private SignInPage mySignInPage;
	private Header myHeaderObjs;
	private WebDriver driver;
	private SeleniumEnvironment myEnvironment;
	private final String testName = "ReturnWizardTest";
	private String theHomePage = "HomePage";
	private String theAccountPage = "AccountPage";
	private int ss = 0;
	private int errorNumber = 0;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new SeleniumEnvironment(driver);
		 myAccountObjs = new AccountPage(myEnvironment);
		myHeaderObjs = new Header(driver, myEnvironment);
		mySignInPage = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		 
			List<String> fns = new ArrayList<String>();
			 fns.add("SignInPageBlank");
			fns.add("AccountPage");
			 fns.add("HomePage");
			fns.add("ReturnWizardPreOrderSelection");
			fns.add("ReturnWizardPostOrderSelection");
			if (errorNumber == 1)
				fns.add("ReturnWizardNothingSelectedError");
			if (errorNumber == 2)
				fns.add("ReturnWizardNoReasonSelectedError");
			if (errorNumber == 3)
				fns.add("ReturnWizardNoQtySelectedError");
			fns.add("ReturnWizardReturnItemSelected");
			fns.add("ReturnWizardEmailReturnLabel");
			fns.add("ReturnWizardReturnLabel");
	
			fns.add("ReturnWizardConfirmation");
			fns.add("ReturnWizardPrintConfirmation");
			fns.add("AccountPageWithSubmittedReturnRMA");
			fns.add("OrderDetailWithReturnInfo");
			fns.add("OrderDetailReturnLabel");
			fns.add("OrderDetailEmailReturnLabel");
			
			String tempFile = myEnvironment.getNetworkTestDirectory() +
					 myEnvironment.getFfBrowser() + "\\" + testName;
			
			System.out.println("This is the tempfile:               " + tempFile);
			
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void ReturnWizardTest() throws InterruptedException, IOException {
		WebElement ce;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		// set implicit wait times so pages do not load due to all the crap that
		// is
		// added by thrid parties

		driver.get(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());	
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());

		myEnvironment.setTestSubject("Return Wizard Test Results");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Sign into an Washington State account
		mySignInPage.signIn(mySignInPage.getTheWAUsername(), mySignInPage.getThePassword());
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "AccountPage", "");
		ss = ss + 1;

		// Go to home page
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		ce.click();
		myEnvironment.waitForDocumentReadyState();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "HomePage", "");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdaSignedIn()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(myAccountObjs.getTheByCSSReturnWizard()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "PreOrderSelection");
		ss = ss + 1;
		//a[contains(text(),'21078')]
		
		// using this explicit order until I fix this to traverse through the list of orders available
		// until one is reached that has a qty to return without having to call customer service 
		ce = myEnvironment
				.waitForDynamicElement( By.xpath("//a[contains(text(),'36758')]"));
	//	ce = myEnvironment
	//			.waitForDynamicElement( By.cssSelector(myAccountObjs
	//					.getTheByCSSFourthOrderReturnWizard()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "PostOrderSelection");
		ss = ss + 1;

		Thread.sleep(5000);

		ce = myEnvironment.waitForDynamicElement( By.name("numLink4"));

		System.out.println("What in the hell is this:                 " + ce.getText());

		
/*	    String productListingOrder= ce.getText();
		
		String orderNumber = productListingOrder.substring(productListingOrder.lastIndexOf("#") + 2);
		
		orderNumber.trim();
		
		System.out.println(orderNumber);*/
		
	//	Integer.parseInt(productCount);
		
		
	//	String orderNumber = ce.getText();

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("td.line_btm_light > a > img"));
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("span.red.bag_link > b"));
		System.out.println(ce.getText());
		
		String productListingOrder= ce.getText();
		
	String orderNumber = productListingOrder.substring(productListingOrder.lastIndexOf("#") + 2);
		
		orderNumber.trim();
		
		System.out.println(orderNumber);
		
		
	 //  orderNumber = ce.getText();
		Assert.assertTrue(ce.getText().contains(orderNumber));
		
		
		for (int i = 0; i < 5; i++) {

			if (myEnvironment.isElementPresent(
					By.cssSelector("#selectId_" + i))) {
				if (i == 0) {
					System.out.println("We got into the O loop!!!!!!!!!");
					ce = myEnvironment.waitForDynamicElement(
							By.cssSelector(myAccountObjs
									.getTheByCSSSubmitStep2()));
					ce.click();

					// Thread.sleep(20000);
					ce = myEnvironment.waitForDynamicElement( By
							.cssSelector(myAccountObjs.getTheByCSSSiteError()));
					Assert.assertTrue(ce.getText().contains(
							myAccountObjs
									.getTheNoItemSelectedReturnErrorMessage()));

					myEnvironment.TakeScreenShot(
							myEnvironment.getFfBrowser(), testName,
							"ReturnWizard", "NothingSelectedError");
					ss = ss + 1;

					errorNumber = errorNumber + 1;

					ce = myEnvironment.waitForDynamicElement(
							By.cssSelector("#selectId_" + i));
					Select select = new Select(ce);
					select.selectByVisibleText("1");

					ce = myEnvironment
							.waitForDynamicElement(
									By.xpath("(//select[@id='selectId_" + i
											+ "'])[2]"));
					select = new Select(ce);
					select.selectByVisibleText("Did not like");
					// i = 4;
				}

				else if (i % 2 == 0) {
					System.out.println("We got into the i % 2 loop!!!!!!!!!");
					ce = myEnvironment.waitForDynamicElement(
							By.cssSelector("#selectId_" + i));
					Select select = new Select(ce);
					select.selectByVisibleText("1");

					ce = myEnvironment.waitForDynamicElement(
							By.cssSelector(myAccountObjs
									.getTheByCSSSubmitStep2()));
					ce.click();

					// Thread.sleep(20000);
					ce = myEnvironment.waitForDynamicElement( By
							.cssSelector(myAccountObjs.getTheByCSSSiteError()));
					Assert.assertTrue(ce.getText().contains(
							myAccountObjs.getTheNoReasonReturnErrorMessage()));

					myEnvironment.TakeScreenShot(
							myEnvironment.getFfBrowser(), testName,
							"ReturnWizard", "NoReasonSelectedError");
					ss = ss + 1;
					errorNumber = errorNumber + 2;

					ce = myEnvironment
							.waitForDynamicElement(
									By.xpath("(//select[@id='selectId_" + i
											+ "'])[2]"));
					select = new Select(ce);
					select.selectByVisibleText("Did not like");
					// i = 4;
				}

				else {
					System.out.println("We got into the else loop!!!!!!!!!");
					ce = myEnvironment
							.waitForDynamicElement(
									By.xpath("(//select[@id='selectId_" + i
											+ "'])[2]"));
					Select select = new Select(ce);
					select.selectByVisibleText("Did not like");

					ce = myEnvironment.waitForDynamicElement(
							By.cssSelector(myAccountObjs
									.getTheByCSSSubmitStep2()));
					ce.click();

					// Thread.sleep(20000);
					ce = myEnvironment.waitForDynamicElement( By
							.cssSelector(myAccountObjs.getTheByCSSSiteError()));
					Assert.assertTrue(ce
							.getText()
							.contains(
									myAccountObjs
											.getTheNoItemQtySelectedReturnErrorMessage()));

					myEnvironment.TakeScreenShot(
							myEnvironment.getFfBrowser(), testName,
							"ReturnWizard", "NoQtySelectedError");
					ss = ss + 1;
					errorNumber = errorNumber + 3;

					ce = myEnvironment.waitForDynamicElement(
							By.cssSelector("#selectId_" + i));
					select = new Select(ce);
					select.selectByVisibleText("1");
					// i = 4;
				}
				// System.out.println("We are getting into the for loop!!!!!!!!!!!!!!!!");
				// if (i == 4)
				break;
			}
		}

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "ReturnItemSelected");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(myAccountObjs.getTheByCSSSubmitStep2()));
		ce.click();

		Thread.sleep(5000);

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(myAccountObjs.getTheByCSSRmaNumber()));
		System.out
				.println("This is suppose to be the rma Number:                 "
						+ ce.getText());
		String rmaNumber = ce.getText();
		System.out
				.println("This is the size of the string holding the rma number:             "
						+ ce.getText().length());

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "EmailReturnLabel");
		ss = ss + 1;

		rmaNumber = rmaNumber.substring(7);
		// Assert.assertTrue(ce.getText().contains(rmaNumber));
		System.out
				.println("This is suppose to be the rma Number:                 "
						+ rmaNumber);

		// ce = myEnvironment.waitForDynamicElement(
		// By.cssSelector("img.printNowImage"));
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("img.printNowImage"));
		ce.click();

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// ((JavascriptExecutor)driver).executeScript("arguments[0].click();",
		// ce);
		// ((JavascriptExecutor)driver).executeScript("labelPopUp('http://ecap03/store/myaccount/returns_usps_pdf.jsp');");
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		// js.executeScript("labelPopUp('http://ecap03/store/myaccount/returns_usps_pdf.jsp');");

		myEnvironment.handleMultipleWindows( "returns_usps_pdf.jsp");
		// driver.switchTo().frame("returns_usps_pdf.jsp");

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("button.toolbarButton.zoomIn"));
		ce.click();
		ce = myEnvironment
				.waitForDynamicElement( By.id("sidebarToggle"));
		ce.click();
		ce = myEnvironment
				.waitForDynamicElement( By.id("sidebarToggle"));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		driver.manage().window().maximize();

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "ReturnLabel");
		ss = ss + 1;

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce = myEnvironment.waitForDynamicElement( By.id("print"));
		// ce.click();
		// Thread.sleep(myEnvironment.getThe_Default_Sleep());
		driver.close();

		myEnvironment.handleMultipleWindows(
				"Returns Center - Tommy Bahama");

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(myAccountObjs.getTheByCSSRmaInfo()));
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");

		Date todaysDate = new Date();

		String theDate = dateFormat.format(todaysDate);
		System.out.println("This is the current date:             " + theDate);
		System.out
				.println("This is the point where I am trying to verify that the current date is present:						"
						+ ce.getText());

		Assert.assertTrue(ce.getText().contains(theDate));
		String theStep3RmaInfo = ce.getText();

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAccountObjs.getTheByIdEmailWizardInput()));
		System.out.println(ce.getAttribute("value"));
		Assert.assertTrue(ce.getAttribute("value").contains(
				myEnvironment.getTheEcommEmailOne()));
		ce = myEnvironment.waitForDynamicElement( By
				.cssSelector(myAccountObjs.getTheByCSSSendEmailReturnWizard()));

		ce.click();

		
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "EmailReturnLabel");
		ss = ss + 1;
		
		ce = myEnvironment.waitForDynamicElement( By
				.cssSelector(myAccountObjs.getTheByCSSConfirmEmailSentLabel()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().contains("Your email was sent"));

	

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAccountObjs.getTheByIdContinueStep4Btn()));
		ce.click();

		// ce = myEnvironment.waitForDynamicElement(
		// By.cssSelector("li > label"));

		ce = myEnvironment.waitForDynamicElement(
				By.className("return_orders"));
		System.out.println(ce.getText());

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "Confirmation");
		ss = ss + 1;

		Assert.assertTrue(ce.getText().contains(rmaNumber));
		Assert.assertTrue(ce.getText().contains(theDate));
		Assert.assertTrue(ce.getText().contains(theStep3RmaInfo));
		ce = myEnvironment.waitForDynamicElement(
				By.className("return_left_padded"));
		Assert.assertTrue(theStep3RmaInfo.contains(ce.getText()));
		ce = myEnvironment.waitForDynamicElement( By.xpath("//td[6]"));
		Assert.assertTrue(theStep3RmaInfo.contains(ce.getText()));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//tr[3]/td[5]"));
		System.out.println("This is the reason for returning:            "
				+ ce.getText());
		Assert.assertTrue(ce.getText().contains("Did not like"));
		
		String originalHandle = driver.getWindowHandle();
		System.out.println("This is the OG window handle.               " + originalHandle);

		ce = myEnvironment
				.waitForDynamicElement( By.cssSelector(myAccountObjs
						.getTheByCSSPrintConfirmationBtn()));
		ce.click();

	    Thread.sleep(myEnvironment.getThe_Special_Sleep());

		//while (driver.WindowHandles.Count == 1 && DateTime.Now < timeoutEnd)

	//	{

	///	Thread.Sleep(1000);

	//	}
		

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("h4.floatleft"));
		System.out.println(ce.getText());

		
		// myEnvironment.handleMultipleWindows(driver, "returns_usps_pdf.jsp");
		// driver.switchTo().frame("returns_usps_pdf.jsp");

	

		Assert.assertTrue(ce.getText().contains(
				"YOUR ONLINE RETURN HAS COMPLETED"));

		
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.className("return_left_padded"));
		Assert.assertTrue(theStep3RmaInfo.contains(ce.getText()));
		ce = myEnvironment.waitForDynamicElement( By.xpath("//td[6]"));
		Assert.assertTrue(theStep3RmaInfo.contains(ce.getText()));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//tr[3]/td[5]"));
		System.out.println("This is the reason for returning:            "
				+ ce.getText());
		Assert.assertTrue(ce.getText().contains("Did not like"));

		try {
			driver.switchTo().alert().accept(); // prepares Selenium to handle
												// alert in Paytech complaining
												// about multiple runs
		} catch (NoAlertPresentException e) {
			// no alert message
		}
		
	
		// Thread.sleep(1000);
		// driver.switchTo().alert().accept();
		// myEnvironment.chooseSecondWindow(driver);
		// driver.close();
		ce.sendKeys(Keys.ESCAPE);
		// ce.sendKeys(Keys.ESCAPE);
		// ce = myEnvironment.waitForDynamicElement(
		// By.cssSelector(myHeaderObjs.getTheByXPathLogo()));
		// ce.click();
		// Thread.sleep(1000);

		myEnvironment.handleMultipleWindows(
				"Returns Center - Tommy Bahama");
		
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ReturnWizard", "PrintConfirmation");
		ss = ss + 1;
		// driver.switchTo().frame("returns_usps_pdf.jsp");

		ce = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdaSignedIn()));
		ce.click();
		Thread.sleep(10000);

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "AccountPage", "WithSubmittedReturnRMA");
		ss = ss + 1;

		// Sign into an Washington State account
		ce = myEnvironment
				.waitForDynamicElement( By.linkText(rmaNumber));
		ce.click();

	//	ce = myEnvironment.waitForDynamicElement(
	//			By.className("viewOrderStatusTd"));
		
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("h2.viewOrderStatusTd"));
	
		
		System.out.println(ce.getText());
		
		Assert.assertTrue(ce.getText().contains(rmaNumber));
		Assert.assertTrue(ce.getText().contains(orderNumber));

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "OrderDetail", "WithReturnInfo");
		ss = ss + 1;

		// ce = myEnvironment.waitForDynamicElement(
		// By.cssSelector("img.printNowImage"));
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("img.printNowImage"));
		ce.click();

		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// ((JavascriptExecutor)driver).executeScript("arguments[0].click();",
		// ce);
		// ((JavascriptExecutor)driver).executeScript("labelPopUp('http://ecap03/store/myaccount/returns_usps_pdf.jsp');");
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		// js.executeScript("labelPopUp('http://ecap03/store/myaccount/returns_usps_pdf.jsp');");

		myEnvironment.handleMultipleWindows( "returns_usps_pdf.jsp");
		// driver.switchTo().frame("returns_usps_pdf.jsp");

		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("button.toolbarButton.zoomIn"));
		ce.click();
		ce = myEnvironment
				.waitForDynamicElement( By.id("sidebarToggle"));
		ce.click();
		ce = myEnvironment
				.waitForDynamicElement( By.id("sidebarToggle"));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		driver.manage().window().maximize();

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "OrderDetail", "ReturnLabel");
		ss = ss + 1;

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce = myEnvironment.waitForDynamicElement( By.id("print"));
		// ce.click();
		// Thread.sleep(myEnvironment.getThe_Default_Sleep());
		driver.close();

		myEnvironment.handleMultipleWindows("Order Detail");

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAccountObjs.getTheByIdEmailWizardInput()));
		System.out.println(ce.getAttribute("value"));
		Assert.assertTrue(ce.getAttribute("value").contains(
				myEnvironment.getTheEcommEmailOne()));
		ce = myEnvironment.waitForDynamicElement( By
				.cssSelector(myAccountObjs.getTheByCSSSendEmailReturnWizard()));

		ce.click();

		ce = myEnvironment.waitForDynamicElement( By
				.cssSelector(myAccountObjs.getTheByCSSConfirmEmailSentLabel()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().contains("Your email was sent"));

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "OrderDetail", "EmailReturnLabel");
		ss = ss + 1;
		
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			InterruptedException, IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));

		
		bw.write(	"<html><body><center><h2>United States Return Product Results</h2>"
				+ "The testing environment that was used for this test is          <br/>"
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ "The browser that was used for this test is          <br/>"
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\" >This is the United States return product test. This test traverses throught the return wizard. The test than navigates back to the Account page to click on the submitted return and view this return in the Order Detail Page.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, "AccountPage",
						"This is the Account Page.")
				+

				myEnvironment.addSSToFile(testName,"ReturnWizardPreOrderSelection",
								"This is the First Page of the Return Wizard and displays a list of orders that may contain products that are eligible for return.")
				+

				myEnvironment.addSSToFile(testName,"ReturnWizardPostOrderSelection",
								"This is the First Page of the Return Wizard with an order selected.")
				+

				myEnvironment.addSSToFile(testName, "ReturnWizardNothingSelectedError",
								"Only one of the three error pages will be displayed for each running of this test. This is the First Page of the Return Wizard with an error message displayed that no product has been selected.")
				+

				myEnvironment.addSSToFile(testName,"ReturnWizardNoReasonSelectedError",
								"Only one of the three error pages will be displayed for each running of this test. This is the First Page of the Return Wizard with an error message displayed that no reason was given for the product selected.")
				+

				myEnvironment.addSSToFile(testName, "ReturnWizardNoQtySelectedError",
								"Only one of the three error pages will be displayed for each running of this test. This is the First Page of the Return Wizard with an error message displayed that no quantity was given for the product selected.")
				+

				myEnvironment.addSSToFile(testName,	"ReturnWizardReturnItemSelected",
								"This is the First Page of the Return Wizard with a producted selected for return.")
				+

				myEnvironment.addSSToFile(testName,	"ReturnWizardEmailReturnLabel",
								"This is the Second Page of the Return Wizard.")
				+

				myEnvironment.addSSToFile(testName, "ReturnWizardReturnLabel",
						"This is the Return Label opened in a pdf to be printed by the user.")
				+

				myEnvironment.addSSToFile(testName, "ReturnWizardConfirmation",
								"This is the Confirmation Page of the Return Wizard.")
				+

				myEnvironment.addSSToFile(testName,	"ReturnWizardPrintConfirmation",
								"This is the Print Confirmation Page of the Return Wizard.")
				+

				myEnvironment.addSSToFile(testName,	"AccountPageWithSubmittedReturnRMA",
								"This is the Account page with the newly submitted return displayed by the RMA number.")
				+

				myEnvironment.addSSToFile(testName, "OrderDetailWithReturnInfo",
								"This is the Order Detail Page for the newly created return.")
				+
				myEnvironment.addSSToFile(testName, "OrderDetailReturnLabel",
								"This is the Return Label opened in a pdf from the Order Detail Page.")
				+

				 myEnvironment.getPageTestOutcome()
				 
				+ "</center></body></html>");
		bw.close();


		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>United States Return Product Results</h2>"
						+ "The testing environment that was used for this test is          <br/>"
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ "The browser that was used for this test is          <br/>"
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\" >This is the United States return product test. This test traverses throught the return wizard. The test than navigates back to the Account page to click on the submitted return and view this return in the Order Detail Page.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		 fns.add("SignInPageBlank");
		fns.add("AccountPage");
		 fns.add("HomePage");
		fns.add("ReturnWizardPreOrderSelection");
		fns.add("ReturnWizardPostOrderSelection");
		if (errorNumber == 1)
			fns.add("ReturnWizardNothingSelectedError");
		if (errorNumber == 2)
			fns.add("ReturnWizardNoReasonSelectedError");
		if (errorNumber == 3)
			fns.add("ReturnWizardNoQtySelectedError");
		fns.add("ReturnWizardReturnItemSelected");
		fns.add("ReturnWizardPrintAndEmailReturnLabel");
		fns.add("ReturnWizardReturnLabel");
		fns.add("ReturnWizardEmailReturnLabel");
		fns.add("ReturnWizardConfirmation");
		fns.add("ReturnWizardPrintConfirmation");
		fns.add("AccountPageWithSubmittedReturnRMA");
		fns.add("OrderDetailWithReturnInfo");
		fns.add("OrderDetailReturnLabel");
		fns.add("OrderDetailEmailReturnLabel");

		ss = ss - 2;
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();
	}

}