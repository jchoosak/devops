package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.LandingPage;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

@RunWith(JUnit4.class)
public class FFUnitedStatesGiftCardOrderTest {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySignInPage;
	private Header myHeader;
	private Men myMen = new Men();
	private ProductListingPage myPLP;
	private ProductDetailPage myPDP;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftService;
	private AddressPage myAddressPage = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewPage = new PreviewPage();
	private ConfirmationPage myConfirmationPage = new ConfirmationPage();
	private AccountPage myAccountPage;
	private PayTech myPayTech = new PayTech();
	private ContactCenter myContactCenter = new ContactCenter();
	private LandingPage myLandingPage = new LandingPage();
	private HomeDecor myHomeDecor = new HomeDecor();
	private TraditionalGiftCard myTraditionalCard = new TraditionalGiftCard();
    private Product myProducts = new Product();
    
	private Product midPriceTshirtProduct = new Product("58.00", "1", "MidPricedTshirt");
	private Product homeProduct = new Product( "TH31823", "2", "Dinnerware");
	  private Product shoeProduct = new Product("TFM00206", "1", "Shoes");
	
	private String theEnvironment = "";
	private String url = "";
	private String theOrderNumber = "";
	private final String testName = "TheUSGiftCardOrderTest";
	private int ss = 0;

	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {

		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		
		myEnvironment = new SeleniumEnvironment(driver);
		 myAccountPage = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftService = new GiftService(driver, myEnvironment, myShoppingBag);
			myHeader = new Header(driver, myEnvironment);
			 myPLP = new ProductListingPage(driver, myEnvironment);
			 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
					 myHeader, testName);
		 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySignInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
		 
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myAccountPage.getThePageName());
			fns.add(myLandingPage.getThePageName());
			fns.add(myPLP.getThePageName()  + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice() );
			fns.add(myPDP.getThePageName() + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice() );
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName() + shoeProduct.getName());
			fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
			fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
			fns.add(myAddressPage.getTheAddressPagePreFilled());
			fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
			fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());
			fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
			fns.add(myPayTech.getThePageName() + theOrderNumber + "");
			
			String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
			myEnvironment.removeSS(fns, tempFile);
			
	}

	@Test
	public void TheUSGiftCardOrderTest() throws InterruptedException,
			IOException {

		WebElement currentElement;

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment
				.setTestSubject("The United States Gift Card Order Test Results");
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Sign into an Washington State account
		mySignInPage.signIn(
				mySignInPage.getTheWAGCUserName(), mySignInPage.getThePassword());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountPage.getThePersonalInfoEditLink()));
		ss = ss + 1;

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myAccountPage.getThePageName(), "");
		ss = ss + 1;

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myLandingPage.getThePageName(), "");
		ss = ss + 1;

		myPDP.selectProductByPrice(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathMensTShirts(), midPriceTshirtProduct.getPrice(), midPriceTshirtProduct.getQty(), 
				midPriceTshirtProduct.getName());
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice());

	

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		myPDP.selectProduct(
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathDinnerware(),
				homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());


		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				homeProduct.getName() + homeProduct.getPrice());

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		// add shoes to the bag
		myPDP.selectProduct(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathShoes(), shoeProduct.getProductId(), shoeProduct.getQty(), 
				shoeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				shoeProduct.getName() + shoeProduct.getPrice());

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		myTraditionalCard.addTraditionalCard(myEnvironment, driver,
				myHeader, myMen, testName, myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName(), "");
		ss = ss + 1;

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));
		// myEnvironment.TakeScreenShot(
		// myEnvironment.getFfBrowser(), testName, "ShoppingBag", "");
		// url = driver.Url;

		// add gift services to the products
		myGiftService.giftBox(
				myShoppingBag.getTheThirdGiftLink(), testName,
				myEnvironment.getFfBrowser());
		// myGiftService.giftBox(driver, currentElement, myEnvironment,
		// myShoppingBag, myShoppingBag.ThirdGiftLink, testName);
		ss = ss + 1;

		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(), "");
		ss = ss + 1;

		// check to see if the url is secure on the address page
		driver.findElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressPage.getTheURL()));

		// Shipping address same as billing
		driver.findElement(By.id(myAddressPage.getTheByIdSameAddress()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myAddressPage.getTheAddressPagePreFilled(), "");
		ss = ss + 1;

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(By.id(
				myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(myPayment.getTheByXPathTwoDayShipping()));
		myHeader.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping()));
		currentElement.click();
		 myEnvironment.getThe_Default_Sleep();
		/*
		 * This is commented out because currently we are under a month of free
		 * shipping, will uncomment when shipping is returned to normal for US
		 * orders.
		 */
		// Checks to see if the shipping price is correct on the payment page.
		// ShippingPrice = "";
		// ShippingPrice =
		// driver.FindElement(By.XPath(myPayment.GroundShipping)).getTheText();
		// ShippingPrice = ShippingPrice.Remove(0, 1);
		// Double.TryParse(ShippingPrice, out TheShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPayment.ActualCanadaShippingPrice);

		// Checks to see that there is no other shipping options present
		// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPayment.getTheByXPathTwoDayShipping()),
		// driver));

		// This is presently not working will come back to this once I have update all tests.
		// see that Card Services are working
	//	myPaymentPage.checkGcBalence();
	//	ss = ss + 1;

		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPayment.getTheByIdCardBalenceLink()()));
		// currentElement.Click();
		// Thread.Sleep(myEnvironment.getThe_Default_Sleep());
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPayment.getTheByIdgetTheGiftCardNumber()Input()));
		// currentElement.SendKeys(myPayment.getTheGiftCardNumber());
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPayment.getTheByIdgetTheByIdCheckCardBalanceBtn()()));
		// currentElement.Click();
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPayment.CardBalence));
		// String theBalence = currentElement.getTheText();
		// myEnvironment.TakeScreenShot(
		// myEnvironment.getFfBrowser(), testName, "PaymentPage",
		// "GiftCardBalence");
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.LinkText(myPayment.XLink));
		// currentElement.Click();

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		myEnvironment.waitForDocumentReadyState();
		// waits for preview page to load
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewPage.getTheTotalTax()));
		myHeader.checkUrl(myPreviewPage.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPreviewPage.getThePageName(), "");
		ss = ss + 1;

		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalTax())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewPage.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewPage.getTheByCSSSubmitButton()))
				.click();
		 myEnvironment.getThe_Default_Sleep();
		myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationPage.getTheOrderNumber()));
		myHeader.checkUrl(myConfirmationPage.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationPage.getTheOrderNumber())).getText());
		// myHeader.checkCanadaHeader(driver, myEnvironment, url);
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationPage.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myConfirmationPage.getThePageName(), "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		theOrderNumber = driver.findElement(
				By.xpath(myConfirmationPage.getTheOrderNumber())).getText();
		theOrderNumber = theOrderNumber.substring(27);

		// Insert a comma for how totals appear in contact center and OG
		// Insert a comma for how totals appear in contact center and OG
		

		myContactCenter = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, theOrderNumber, testName,
				myEnvironment.getFfBrowser());
		
		String theContactTotal = myContactCenter.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenter.checkOrderInCC(theContactTotal);

		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);

		String newTotal = "";
		myPayTech = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getFfBrowser());
		
		
		newTotal = myPayTech
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTech
		.checkOrderInPayTech(newTotal);
		ss = ss + 1;
		// driver.close();
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws InterruptedException, MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>United States Gift Card Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The United States Gift Card Order tests that products like a Traditional Gift Card can be added to the Shopping Bag and processed through the entire order process. "
				+ "</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, myAccountPage.getThePageName(),
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, myLandingPage.getThePageName(),
						"This is the Home Page.")
				+

			myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice(),
						"This is the T-Shirts PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice() ,
						"This is the T-Shirt PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice() ,
						"This is the T-Shirt PDP with Preview pane displaying added product.")
				+
						

						myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+
		
		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+
				
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+

					myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  shoeProduct.getName(),
						"This is the Shoes PLP.")
				+		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + shoeProduct.getProductId(),
						"This is the Shoe PDP.")
				+		
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + shoeProduct.getProductId(),
						"This is the Shoe PDP with Preview pane displaying added product.")
				+

						myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(),
						"This is the GiftCard PDP.")
				+

		myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText(),
						"This is the Gift Message Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(),
						"This is the Shopping Bag containing Products with Gift Services.")
				+

			myEnvironment.addSSToFile(testName, myAddressPage.getTheAddressPagePreFilled(),
						"This is the Address Page with pre populated fields.")
				+
				
			myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getTheGiftCardBalenceModal(),
						"This is the Gift Card Balence Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut(),
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, myPreviewPage.getThePageName(),
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, myConfirmationPage.getThePageName(),
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, myContactCenter.getThePageName() + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + theOrderNumber,
						"This is the PaymenTech Page.")
				+

           	myEnvironment.getPageTestOutcome()
           	
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>United States Gift Card Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The United States Gift Card Order tests that products like a Traditional Gift Card can be added to the Shopping Bag and processed through the entire order process. "
						+ "</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");
		
		
		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myAccountPage.getThePageName());
		fns.add(myLandingPage.getThePageName());
		fns.add(myPLP.getThePageName()  + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice() );
		fns.add(myPDP.getThePageName() + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + midPriceTshirtProduct.getName() + midPriceTshirtProduct.getPrice() );
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPLP.getThePageName() + homeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
		fns.add(myPLP.getThePageName() + shoeProduct.getName());
		fns.add(myPLP.getThePageName() + shoeProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + shoeProduct.getProductId());
		fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
		fns.add(myAddressPage.getTheAddressPagePreFilled());
		fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
		fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());
		fns.add(myContactCenter.getThePageName() + theOrderNumber + "");
		fns.add(myPayTech.getThePageName() + theOrderNumber + "");

		ss = ss + 1;
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}
