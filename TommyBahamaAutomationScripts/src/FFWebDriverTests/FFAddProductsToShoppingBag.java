package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class FFAddProductsToShoppingBag {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySignInPage;
	private Header myHeader;
	private Men myMen = new Men();
	private ProductListingPage myPLP;
	private ProductDetailPage myPDP;
	private AccountPage myAccountPage;
	private HomeDecor myHomeDecor = new HomeDecor();
	private TraditionalGiftCard myTraditionalCard = new TraditionalGiftCard();
	private ShoppingBagPage myShoppingBag;
	
	
	private Product homeProduct = new Product( "TH31414", "1", "Dinnerware");
	private Product golfProduct = new Product("TBG-099", "1", "GolfBag");
	private Product highPricePantsProduct = new Product("128.00", "1", "Pants");
	private Product myProducts = new Product();
  

	private String testName = "TheAddProductsToShoppingBagTest";
	private int ss = 0;

	String OrderNumber = "";
	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	// private String baseUrl;
	private WebDriver driver;
	private WebElement ce;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();

		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeader = new Header(driver, myEnvironment);
		 myAccountPage = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myPLP = new ProductListingPage(driver, myEnvironment);
		 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
				 myHeader, testName);
		 mySignInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
		 
		
		 
		 
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myAccountPage.getThePageName());
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
			fns.add(myPDP.getThePageName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
			fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myPLP.getThePageName()  + golfProduct.getName());
			fns.add(myPDP.getThePageName() + golfProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
			fns.add(myShoppingBag.getTheEmptyShoppingBagName());
		 
			String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
			myEnvironment.removeSS(fns, tempFile);
	}

	
	
	
	@Test
	public void TheAddProductsToShoppingBagTest() throws InterruptedException, IOException {

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment
				.setTestSubject("Add and Remove Products from Shopping Bag Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// Signe into an canada account
		mySignInPage.signIn(
				mySignInPage.getTheCanadaNineUserName(), mySignInPage.getThePassword());
		myEnvironment.waitForPageLoaded();
		// ss = ss + 1;
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountPage.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myAccountPage.getThePageName(), "");
		// ss = ss + 1;
		// Go to home page
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// adds to the shopping bag
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		myPDP.selectProduct(				
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathDinnerware(),
				homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				homeProduct.getProductId());

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeader
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myPDP.selectProductByPrice(myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathPants(), highPricePantsProduct.getPrice(), highPricePantsProduct.getQty(),
				highPricePantsProduct.getName() );

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				highPricePantsProduct.getName() + highPricePantsProduct.getPrice());
		myEnvironment.waitForPageLoaded();

		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myTraditionalCard.addTraditionalCard(myEnvironment, driver,
				myHeader, myMen, testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(), "");
		myEnvironment.waitForPageLoaded();

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				"TraditionalCard");

		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// Add second restricted product
		myPDP.selectProduct(
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathGolf(), golfProduct.getProductId(), golfProduct.getQty(),
				golfProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				golfProduct.getName());

		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myEnvironment.waitForPageLoaded();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

	/*	myEnvironment.waitForPageLoaded();
		myPDP.selectProductByPrice(
				myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathPants(), "$98.00", "1",
				myProducts.getThePantsName());*/

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		// remove all the products
		myShoppingBag.removeAllProducts(4);
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myShoppingBag.getTheEmptyShoppingBagName(), "");
		
	
		
		myEnvironment.setTestPassed(true);

	}
	@After
	public void quitDriver() throws MessagingException,
			InterruptedException, IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Canada Order Test Setup Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%; \">This is the Canada Setup Order test. All Canada tests have similar products added. Due to the large size of the PLP and PDP, the results for added products are only shown in this test.</h3>"
				+ "<table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
						"This is the blank Sign-In Page.")
				+

			myEnvironment.addSSToFile(testName, myAccountPage.getThePageName(),
						"This is the Account Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+
		
		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+
				
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+

				myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() ,
						"This is the Pants PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() ,
						"This is the Pants PDP with Preview pane displaying added product.")
				+

				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(),
						"This is the GiftCard PDP.")
				+
				
			
				myEnvironment.addSSToFile(testName, myPLP.getThePageName() + golfProduct.getName(),
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + golfProduct.getProductId(),
						"This is the Golf PDP.")
				+
	
								
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + golfProduct.getProductId(),
						"This is the Golf PDP with Preview pane displaying added product.")
				+

				myEnvironment.addSSToFile(testName, myShoppingBag.getTheEmptyShoppingBagName(),
						"This is the Empty Shopping Bag Page.")
				+

			    myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Order Test Setup Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%; \">This is the Canada Setup Order test. All Canada tests have similar products added. Due to the large size of the PLP and PDP, the results for added products are only shown in this test.</h3>"
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");
					

		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myAccountPage.getThePageName());
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
		fns.add(myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
		fns.add(myPDP.getThePageName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
		fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myPLP.getThePageName()  + golfProduct.getName());
		fns.add(myPDP.getThePageName() + golfProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
		fns.add(myShoppingBag.getTheEmptyShoppingBagName());

		ss = ss - 9;
		
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);
		driver.quit();
	}
}

