package FFWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.LandingPage;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class FFCanadaOrderTest {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySignInPage;
	private Header myHeader;
	private Men myMen = new Men();
	private ProductListingPage myPLP;
	private ProductDetailPage myPDP;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftService;
	private AddressPage myAddressPage = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewPage = new PreviewPage();
	private ConfirmationPage myConfirmationPage = new ConfirmationPage();
	private AccountPage myAccountPage;
	private PayTech myPayTech = new PayTech();
	private ContactCenter myContactCenter = new ContactCenter();
	private FiftyOne myFiftyOne = new FiftyOne();
	private HomeDecor myHomeDecor = new HomeDecor();
	private TraditionalGiftCard myTraditionalCard = new TraditionalGiftCard();
	private Product myProducts = new Product();
	private LandingPage myLandingPage = new LandingPage();

	private String theEnvironment = "";
	private String ShippingPrice = "";
	private String testName = "TheCanadaOrderTest";
	
	private Product highPricePantsProduct = new Product("128.00", "1", "HighPricedPants");
	private Product lowPricePantsProduct = new Product("98.00", "1", "LowPricedPants");
	private Product golfProduct = new Product("TBG-099", "1", "GolfBag");
	private Product homeProduct = new Product( "TH31823", "1", "Dinnerware");
	
	private int ss = 0;
	// url to be checked on various pages
	private String url = "";
	String OrderNumber = "";
	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();

		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		myEnvironment = new SeleniumEnvironment();
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeader = new Header(driver, myEnvironment);
		 myPLP = new ProductListingPage(driver, myEnvironment);
		 myPDP = new ProductDetailPage(driver, myEnvironment, myPLP, 
				 myHeader, testName);
		 myAccountPage = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftService = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySignInPage = new SignInPage(driver, myEnvironment, myHeader, testName);
		 setMyLandingPage(new LandingPage());
		 
			String tempFile = myEnvironment.getNetworkTestDirectory() + myEnvironment.getFfBrowser() + "/" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myAccountPage.getThePageName());
			fns.add(myLandingPage.getThePageName());
			fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName());
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add(myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
			fns.add(myPDP.getThePageName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
			fns.add(myPLP.getThePageName()  + golfProduct.getName());
			fns.add(myPDP.getThePageName() + golfProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
			fns.add(myPLP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
			fns.add(myPDP.getThePageName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice()  );
			fns.add(myPDP.getThePreviewPaneName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox());
			fns.add(myGiftService.getTheGiftServicesModal() +  myGiftService.getTheAddToGiftBox1());
			fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
			fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
			fns.add(myShoppingBag.getThePageName() +  myGiftService.getTheGiftServiceShippingError());
			fns.add(myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceError());
			 fns.add(myPDP.getThePageName() + myPDP.getThe8AddedToBagMessage());
			fns.add(myAddressPage.getTheAddressPagePreFilled());
			fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
			fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());
			fns.add(myContactCenter.getThePageName() + OrderNumber + "");
			fns.add(myPayTech.getThePageName() + OrderNumber + "");
			
			myEnvironment.removeSS(fns, tempFile);
			
	}

	@Test
	public void TheCanadaOrderTest() throws InterruptedException, IOException {
		WebElement currentElement;
		// Navigate to the testing environment
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment.setTestSubject("Canada Order Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// Signe into an canada account
		mySignInPage.signIn(
				mySignInPage.getTheCanadaUsername(), mySignInPage.getThePassword());
		myEnvironment.waitForPageLoaded();
		// ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountPage.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myAccountPage.getThePageName(), "");
		// ss = ss + 1;
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathLogo()));
		currentElement.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		
		
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myLandingPage.getThePageName(), "");
		// ss = ss + 1;
		// adds a t-shirt to the shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheFooterEmailBtn()));

		myTraditionalCard.addTraditionalCard(myEnvironment, driver,
				myHeader, myMen, testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(),
				myEnvironment.getFfBrowser());
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				myProducts.getTheTraditionalGiftCardName());
		myEnvironment.waitForPageLoaded();
		


		myEnvironment.waitForPageLoaded();
		myPDP.selectProductByPrice(myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathPants(), lowPricePantsProduct.getPrice(), lowPricePantsProduct.getQty(),
				lowPricePantsProduct.getName() );

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice());
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForPageLoaded();

		myPDP.selectProduct(				
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathDinnerware(),
				homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				homeProduct.getProductId());
		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeader
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myPDP.selectProductByPrice(myHeader.getTheByXPathMensTab(),
				myMen.getTheByXPathPants(), highPricePantsProduct.getPrice(), highPricePantsProduct.getQty(),
				highPricePantsProduct.getName() );

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				highPricePantsProduct.getName() + highPricePantsProduct.getPrice());
		myEnvironment.waitForPageLoaded();

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeader.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// Add second restricted product
		myPDP.selectProduct(
				myHeader.getTheByXPathHomeDecorTab(),
				myHomeDecor.getTheByXPathGolf(), golfProduct.getProductId(), golfProduct.getQty(),
				golfProduct.getName());

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
				golfProduct.getName());



		ss = ss + 1;
		ss = ss + 1;

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myShoppingBag.getThePageName() , myProducts.getThePageName());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;

		// add gift services to the products
		myGiftService.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[2]/td[2]/span[2]/a"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myGiftService.addToGiftBox( myShoppingBag.getTheFirstGiftLink(),
				testName, myEnvironment.getFfBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[3]/td[2]/span[2]/a"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myGiftService.giftBox(myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myShoppingBag.getThePageName(), "ProductsWithGiftServices");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/a"));

		// continue to address page
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myShoppingBag.getThePageName(), myGiftService.getTheGiftServiceShippingError());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).Text.Contains(myShoppingBag.ShippingRestrictionText));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBag.getTheShippingRestrictionText()));
		// String errorColor =
		// driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).GetCssValue("Color");
		String errorColor = currentElement.getCssValue("color");
	//	System.out.println(errorColor);
	//	Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
	//	Assert.assertTrue(currentElement.getText().contains(
	//			myShoppingBag.getTheCustomsGiftServiceErrorText()));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).Text.Contains(myShoppingBag.CustomsGiftServiceErrorText));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBag.CanadaError)).Text.Contains(myShoppingBag.TraditionalGiftCardCanadaErrorText));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstRowGiftServicesError()));
		System.out.println("This is the GiftServicesError:       "
				+ currentElement.getText());

		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheSecondRowGiftServivesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

//		currentElement = myEnvironment
//				.waitForDynamicElement( By.xpath(myShoppingBag
//						.getTheFourthRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBag.getTheGiftServicesErrorText()));
//
//		currentElement = myEnvironment.waitForDynamicElement( By
//				.xpath(myShoppingBag.getTheThirdRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBag.getTheShippingRemovalText()));
//		errorColor = currentElement.getCssValue("color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
//
//		currentElement = myEnvironment.waitForDynamicElement( By
//				.xpath(myShoppingBag.getTheFifthRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBag.getTheShippingRemovalText()));
//		errorColor = currentElement.getCssValue("color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
//
//		// checks for the yellow outline
//		myShoppingBag.checkProductsForYellowOutline(driver, myEnvironment,
//				7);

		// see that the shipping restriction message is present in the shopping
		// bag
		if(!driver.getCurrentUrl().contains("test"))
		{
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[4]/td[2]/span[2]/div[2]"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/div[2]"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[6]/td[2]/span[2]/div"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		
		}

		// remove all the products that cannot be shipped to Canada
		myShoppingBag.removeProducts( 2);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// removes the gift services from remaining products
		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myShoppingBag.getThePageName(), myGiftService.getTheGiftServiceError());
		ss = ss + 1;
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftService.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		 Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
	 Thread.sleep(myEnvironment.getThe_Default_Sleep());
	 Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftService.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		 Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		// currentElement = myEnvironment.waitForDynamicElement(
		// By.xpath(myHeader.getTheByXPathLogo()));
		// // myEnvironment.myWaitForWDElement(driver,
		// By.cssSelector(myHeader.getTheByCSSLogo()));
		// currentElement.click();
		// myEnvironment.waitForPageLoaded();
		// Go Back to United States
		myHeader.selectCountry(driver, myEnvironment, myFiftyOne,
				myFiftyOne.getTheUnitedStatesFlag(), testName);
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		 Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// click the first image to add more qty
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		 myEnvironment.getThe_Default_Sleep();
		
		myEnvironment.waitForTitle("Harrison Authentic Fit Pants");
		// myEnvironment.waitForDynamicElement(
		// By.xpath(myPDP.getTheMainImage()));
		myPDP.addQuantityToBag("8");
		// ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeader.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));

		// Test for all of the columns are present in the shopping bag
	myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		// myEnvironment.TakeScreenShot(driver,
		// myEnvironment.getFfBrowser(),
		// testName, myShoppingBag.getThePageName()", "Normal");
		// ss = ss + 1;
		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressPage.getTheURL()));
		myEnvironment.TakeScreenShot(myEnvironment.getFfBrowser(),
				testName, "Address", "PreFilled");
		ss = ss + 1;
		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdSameAddress()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheShippingZipCode()));
		String theShippingZip = currentElement.getAttribute("value");
		Assert.assertTrue(theZip.contains(theShippingZip));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));
		myHeader.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		// Checks to see if the shipping price is correct on the payment page.
		ShippingPrice = "";
		
		System.out.println(driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText());
		
		ShippingPrice = driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText();
		ShippingPrice = ShippingPrice.substring(1);
		Double theShippingPrice = Double.parseDouble(ShippingPrice);
		Assert.assertEquals(theShippingPrice,
				myPaymentPage.getTheActualCanadaShippingPrice());
		// myEnvironment.TakeScreenShot(driver,
		// myEnvironment.getFfBrowser(),
		// testName, myPaymentPage.getThePageName()", "");
		// ss = ss + 1;
		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// see that Card Services are working
	//	currentElement = myEnvironment.waitForDynamicElement(
		//		By.id(myPaymentPage.getTheByIdCardBalenceLink()));
	//	currentElement.click();
	//  myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
	//	currentElement = myEnvironment.waitForDynamicElement(
	//			By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
	//	currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());

		
	//	myPaymentPage.checkGcBalence();
	/*	currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		String theBalence = currentElement.getText();

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPaymentPage.getThePageName()", "GiftCardBalenceModal");
		ss = ss + 1;
		System.out.println("This is the balence:				" + theBalence);
		Assert.assertTrue(theBalence.contains("$25.00"));

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myPaymentPage.getTheByLinkX()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();*/
		
	//	driver.close();

	//	myEnvironment.handleMultipleWindows(
	//			"Payment Information");

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		myEnvironment.waitForPageLoaded();
		ss = ss + 1;
		// waits for preview page to load
		currentElement = myEnvironment.waitForDynamicElement(
				By.className(myPreviewPage.getTheByClassNameSplitOrderTotal()));

		myHeader.checkUrl(myPreviewPage.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myPreviewPage.getThePageName(), "");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalTax())).getText();
	//	String theDuty = driver.findElement(
	//			By.id(myPreviewPage.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewPage.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewPage.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewPage.getTheByCSSSubmitButton()))
				.click();
		
		Thread.sleep(8000);
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationPage.getTheOrderNumber()));

		// myEnvironment.myWaitForWDElement(driver,
		// By.xpath(myConfirmationPage.getTheOrderNumber()));

		myHeader.checkUrl(myConfirmationPage.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationPage.getTheOrderNumber())).getText());
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationPage.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myConfirmationPage.getThePageName(), "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		OrderNumber = driver.findElement(
				By.xpath(myConfirmationPage.getTheOrderNumber())).getText();
		OrderNumber = OrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		// currently free shipping
		Assert.assertEquals(
				myPaymentPage.getTheActualCanadaShippingString(),
				driver.findElement(
						By.xpath(myConfirmationPage.getTheShippingAmount()))
						.getText().trim());
		
		myContactCenter = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, OrderNumber, testName,
				myEnvironment.getFfBrowser());

		// Insert a comma for how totals appear in contact center and OG
		String theContactTotal = myContactCenter.getContactTotal(theTotal);

		
		// runs the script to look up the order in Contact Center
		myContactCenter.checkOrderInCC(theContactTotal);
		
		
		
		ss = ss + 1;
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		String newTotal = "";
		
		myPayTech = new PayTech(driver, myEnvironment,  
				OrderNumber, testName, myEnvironment.getFfBrowser());
		
		
		newTotal = myPayTech
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTech
				.checkOrderInPayTech(newTotal);
		ss = ss + 1;
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Canada Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3  style= \"width:70%;\">The Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				
				myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, myAccountPage.getThePageName(),
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, myLandingPage.getThePageName(),
						"This is the Home Page.")
				+

				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName(),
						"This is the GiftCard PDP.")
				+
				
					myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+		
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+

			myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() ,
						"This is the Pants PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() ,
						"This is the Pants PDP with Preview pane displaying added product.")
				+

				myEnvironment.addSSToFile(testName, myPLP.getThePageName() + golfProduct.getName(),
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + golfProduct.getProductId(),
						"This is the Golf PDP.")
				+	
								
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + golfProduct.getProductId(),
						"This is the Golf PDP with Preview pane displaying added product.")
				+
				
	myEnvironment.addSSToFile(testName, myPLP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myPDP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() ,
						"This is the Pants PDP.")
				+
				
							
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() ,
						"This is the Pants PDP with Preview pane displaying added product.")
				+
							
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName(),
						"This is the Shopping Bag with products.")
				+
				
				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox(),
						"This is the Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal()+ myGiftService.getTheAddToGiftBox1(),
						"This is the Add To Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText(),
						"This is the Gift Message Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName(),
						"This is the Shopping Bag containing Products with Gift Services.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceShippingError(),
						"This is the Shopping Bag containing Products with different types of error messages.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceError(),
						"This is the Shopping Bag containing Products with Gift Services error messages.")
				+
				
				myEnvironment.addSSToFile(testName, myPDP.getThePageName() + myPDP.getThe8AddedToBagMessage(),
						"This is the PDP adding 8 more items.")
				+
				
				myEnvironment.addSSToFile(testName, myAddressPage.getTheAddressPagePreFilled(),
						"This is the Address Page with pre populated fields.")
				+
				
			myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getTheGiftCardBalenceModal(),
						"This is the Gift Card Balence Modal.")
				+
				
				myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut(),
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, myPreviewPage.getThePageName(),
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, myConfirmationPage.getThePageName(),
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, myContactCenter.getThePageName() + OrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPayTech.getThePageName() + OrderNumber,
						"This is the PaymenTech Page.")
				+

				myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();
	
		
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3  style= \"width:70%;\">The Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myAccountPage.getThePageName());
		fns.add(myLandingPage.getThePageName());
		fns.add(myPDP.getThePageName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myPDP.getThePreviewPaneName() + myProducts.getTheTraditionalGiftCardName());
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPLP.getThePageName() + homeProduct.getName());
		fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
		fns.add(myPLP.getThePageName()  + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
		fns.add(myPDP.getThePageName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + highPricePantsProduct.getName() + highPricePantsProduct.getPrice() );
		fns.add(myPLP.getThePageName()  + golfProduct.getName());
		fns.add(myPDP.getThePageName() + golfProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + golfProduct.getProductId());
		fns.add(myPLP.getThePageName()  + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
		fns.add(myPDP.getThePageName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice()  );
		fns.add(myPDP.getThePreviewPaneName() + lowPricePantsProduct.getName() + lowPricePantsProduct.getPrice() );
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftBox());
		fns.add(myGiftService.getTheGiftServicesModal() +  myGiftService.getTheAddToGiftBox1());
		fns.add(myGiftService.getTheGiftServicesModal() + myGiftService.getTheGiftMessageText());
		fns.add(myShoppingBag.getThePageName() + myProducts.getThePageName() + myGiftService.getThePageName());
		fns.add(myShoppingBag.getThePageName() +  myGiftService.getTheGiftServiceShippingError());
		fns.add(myShoppingBag.getThePageName() + myGiftService.getTheGiftServiceError());
		 fns.add(myPDP.getThePageName() + myPDP.getThe8AddedToBagMessage());
		fns.add(myAddressPage.getTheAddressPagePreFilled());
		fns.add(myPaymentPage.getThePageName()  + myPaymentPage.getTheGiftCardBalenceModal());
		fns.add(myPaymentPage.getThePageName() + myPaymentPage.getThePageFilledOut());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());
		fns.add(myContactCenter.getThePageName() + OrderNumber + "");
		fns.add(myPayTech.getThePageName() + OrderNumber + "");

		
		//ss = ss - 3;
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}

	public LandingPage getMyLandingPage() {
		return myLandingPage;
	}

	public void setMyLandingPage(LandingPage myLandingPage) {
		this.myLandingPage = myLandingPage;
	}
}