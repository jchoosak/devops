package ChromeCacheTests;



	import java.io.File;
import java.util.concurrent.TimeUnit;

import net.lightbody.bmp.proxy.ProxyServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.Women;

	public class ChromeCacheWomens {

		private SeleniumEnvironment myEnvironment;
		private Header myHeaderObjs;
		private Women myWomenObjs = new Women();
		private ProductListingPage myProductListingObjs;
		private ProductDetailPage myProductDetailObjs;
	    private String testName =  "Womens";
		WebDriver driver;
		ProxyServer server = new ProxyServer(8090);
		String PROXY = "localhost:8090";
		
		@Before
		public void openBrowser() throws Exception {

			server.start();		
		    server.setCaptureHeaders(true);
		    server.setCaptureContent(true);
			
		   

		    org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
		    proxy.setHttpProxy(PROXY)
		         .setFtpProxy(PROXY)
		         .setSslProxy(PROXY);
		    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		    capabilities.setCapability(CapabilityType.PROXY, proxy);
			
			
			//    capabilities.setCapability("chrome.switches", Arrays.asList("--proxy-server=localhost:8090"));
			 //   WebDriver driver = new ChromeDriver(capabilities);
		
				File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
				System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
				driver = new ChromeDriver(capabilities);
				
				
			myEnvironment = new SeleniumEnvironment(driver);
			myHeaderObjs = new Header(driver, myEnvironment);
			 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
			 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
					 myHeaderObjs, testName);
			 
				server.newHar("test.tommybahama.com");
		}

		@Test
		public void TheTraverseWomensProductsTest() throws InterruptedException {
			WebElement ce;

			driver.get(myEnvironment.getTheTestingEnvironment());
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
			
			ce = myEnvironment.waitForDynamicElement(
					By.id("emailFormFooterId"));
			
			ce.sendKeys("No wait times on hover creates problems!");
					
			myEnvironment.parseHarFile(server, testName);

			ce = driver.findElement(By.xpath(myHeaderObjs.getTheByXPathWomensTab()));
			
			ce.click();
			
			ce = myEnvironment.waitForDynamicElement(
					By.id("emailFormFooterId"));
			
			ce.sendKeys("No wait times on hover creates problems!");
			
			myEnvironment.parseHarFile(server, testName);
			
	      ce = driver.findElement(By.xpath(myWomenObjs.getTheByXPathNewArrivals()));
			
			ce.click();
			
			myEnvironment.parseHarFile(server, testName);
	
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensNewArrivalsLinksList();
			myProductListingObjs.traverseLinks(
					myWomenObjs.getTheWomensNewArrivalsLinks(), 
					myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE NEW ARRIVALS LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathInternationalFit()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathInternationalFit()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensInternationalFitLinksList();
			myProductListingObjs.traverseLinks(
					myWomenObjs.getTheWomensInternationoalFitLinks(), myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE INTERNATIONAL LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathDresses()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathDresses()))
					.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensDressesLinksList();
			myProductListingObjs.traverseLinks(
					myWomenObjs.getTheWomensDressesLinks(), 
					myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Dresses LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathFragranceAndBody()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathFragranceAndBody())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensFragranceLinksList();
			myProductListingObjs.traverseLinks(myWomenObjs.getTheWomensFragranceLinks(),
					 myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Fragrance LIST!!!!!!!!!!!!!!");

			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathJewelry()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathJewelry())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensJewelryLinksList();
			myProductListingObjs.traverseLinks(myWomenObjs.getTheWomensJewelryLinks(),
					 myProductDetailObjs, "linktext", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE Jewlery LIST!!!!!!!!!!!!!!");


			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathSummerSwimShop()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathSummerSwimShop())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensSummerSwimShopLinksList();
			myProductListingObjs.traverseLinks(myWomenObjs.getTheWomensSummerSwimShopLinks(),
					 myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE SWIM LIST!!!!!!!!!!!!!!");

	

			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathSwim()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathSwim())).click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myWomenObjs.CreateWomensSwimLinksList();
			myProductListingObjs.traverseLinks(myWomenObjs.getTheWomensSwimLinks(),
					 myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I HAVE MADE IT PAST THE SWIM LIST!!!!!!!!!!!!!!");

	

			myWomenObjs.CreateWomensLinksList();
			myProductListingObjs.traverseProducts(myWomenObjs.getTheWomensLinks(),
					 myProductDetailObjs, "xpath", server, testName);
			System.out
					.println("                 I have PAST THE Main Product LISTs!!!!!!!!!!!!!!");

	

			myEnvironment.waitForDynamicElement(
					By.xpath(myWomenObjs.getTheByXPathAccessories()));
			driver.findElement(By.xpath(myWomenObjs.getTheByXPathAccessories()))
					.click();
			myWomenObjs.CreateWomensAccessoriesLinksList();
			myProductListingObjs.traverseProducts(
					myWomenObjs.getTheWomensAccessoriesLinks(), 
					myProductDetailObjs, "xpath", server, testName);

			System.out
					.println("                 I HAVE MADE IT PAST THE ACCESSORIES LIST!!!!!!!!!!!!!!");
			// driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
			// now we start on the sub links. This is the sub links of New Arrivals

		}

		@After
		public void quitDriver() {

			driver.quit();

		}
	}

