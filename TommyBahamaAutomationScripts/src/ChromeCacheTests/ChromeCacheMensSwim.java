package ChromeCacheTests;

import java.io.File;
import java.util.concurrent.TimeUnit;

import net.lightbody.bmp.proxy.ProxyServer;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.MensSwim;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;

@RunWith(JUnit4.class)
public class ChromeCacheMensSwim {

	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private MensSwim myMensSwimObjs = new MensSwim();
	
	private String testName = "TheTraverseMensSwimProductsTest";
	ProxyServer server = new ProxyServer(8090);
	WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
	}

	@Test
	public void TheTraverseMensSwimProductsTest() throws InterruptedException {
		WebElement currentElement;
		driver.get(myEnvironment.getTheTestingEnvironment());

		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println("We are in this Men's Swim");

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//li[9]/ul/li/a"));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		System.out.println(myMenObjs.getTheByLinkMensSwimShop().toString());
		myMensSwimObjs.CreateMensXPathLinksList();
		myProductListingObjs.traverseProducts(
				myMensSwimObjs.getTheMensSwimXpathLinks(), myProductDetailObjs, "xpath", server, testName);
		System.out
				.println("                 I have PAST THE Main XPath Product LISTs!!!!!!!!!!!!!!");

		myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		driver.findElement(By.xpath(myHeaderObjs.getTheByXPathMensTab()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.linkText(myMenObjs.getTheByLinkMensSwimShop()));
		System.out.println(myMenObjs.getTheByLinkMensSwimShop().toString());
		driver.findElement(By.linkText(myMenObjs.getTheByLinkMensSwimShop()))
				.click();
		myMensSwimObjs.CreateMensSwimLinksList();
		myProductListingObjs.traverseProducts(
				myMensSwimObjs.getTheMensSwimLinks(), 
				myProductDetailObjs, "linktext", server, testName);
		System.out
				.println("                 I HAVE MADE IT PAST THE Main  Link Products LIST!!!!!!!!!!!!!!");

	}

	@After
	public void quitDriver() {

		driver.quit();

	}

}

