package ChromeCacheTests;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.ProxyServer;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.BigAndTall;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;

public class ChromeCacheBigAndTall {

	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	
	private String testName = "BigAndTall";

	private BigAndTall myBigAndTallObjs = new BigAndTall();
	WebDriver driver;

	ProxyServer server = new ProxyServer(8090);
	 String PROXY = "localhost:8090";
	 
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() throws Exception {
		
	//	server.start();
	//	Proxy proxy = server.seleniumProxy();
		//Proxy proxy = new Proxy();
		//proxy.setProxyAutoconfigUrl("http://test.tommybahama.com/");
		server.start();		
	    server.setCaptureHeaders(true);
	    server.setCaptureContent(true);
		
	   

	    org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
	    proxy.setHttpProxy(PROXY)
	         .setFtpProxy(PROXY)
	         .setSslProxy(PROXY);
	    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	    capabilities.setCapability(CapabilityType.PROXY, proxy);
		
		
		//    capabilities.setCapability("chrome.switches", Arrays.asList("--proxy-server=localhost:8090"));
		 //   WebDriver driver = new ChromeDriver(capabilities);
	
			File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver(capabilities);
		
	//	capabilities.setCapability(CapabilityType.PROXY, proxy);

		//ChromeProfile profile = new ChromeProfile();
		
	/*	Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		 DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	        LoggingPreferences loggingprefs = new LoggingPreferences();
	        loggingprefs.enable(LogType.BROWSER, Level.ALL);
	        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
	        driver = new ChromeDriver(capabilities);*/
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		
	//	server.newHar("myHar");
	
		 
	}

	@Test
	public void TheTraverseBigAndTallProductsTest() throws InterruptedException {
		
		WebElement ce;
		
	    server.newHar("test.tommybahama.com");
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
	
		ce = myEnvironment.waitForDynamicElement(
				By.id("emailFormFooterId"));
		
		ce.sendKeys("No wait times on hover creates problems!");
				
		myEnvironment.parseHarFile(server, testName);
	//	try {
	//		har.writeTo(networkfile);
	//	} catch (IOException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
	//	}
		this.ExtractJSLogs();
		// We first run through each link that directly shows PLP page from the
		// Men's Tab.
		driver.findElement(By.xpath(myHeaderObjs.getTheByXPathBigAndTallTab()))
				.click();
		myEnvironment.parseHarFile(server, testName);
		myBigAndTallObjs.CreateBigAndTallLinks();
		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallLinks(), 
				 myProductDetailObjs, "xpath", server, testName);

		System.out
				.println("                 I have PAST THE Main Product LISTs!!!!!!!!!!!!!!");

		// now we start on the sub links. This is the sub links of New Arrivals
		// myEnvironment.waitForDynamicElement(
		// By.xpath(myBigAndTallObjs.getTheByXPathShirts()));
		driver.findElement(By.xpath(myBigAndTallObjs.getTheByXPathShirts()))
				.click();
		myBigAndTallObjs.CreateBigAndTallShirtLinks();
		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallShirtLinks(), 
				 myProductDetailObjs, "linktext", server, testName);

		System.out
				.println("                 I HAVE MADE IT PAST THE NEW Shirt Links LIST!!!!!!!!!!!!!!");

		myEnvironment.waitForDynamicElement(
				By.xpath(myBigAndTallObjs.getTheByXPathCologneGrooming()));
		driver.findElement(
				By.xpath(myBigAndTallObjs.getTheByXPathCologneGrooming()))
				.click();
		myBigAndTallObjs.CreateBigAndTallCologneGroomingLinks();

		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallCologneGroomingLinks(),
			 myProductDetailObjs, "linktext", server, testName);
		System.out
				.println("                 I HAVE MADE IT PAST THE Grooming LIST!!!!!!!!!!!!!!");

		myEnvironment.waitForDynamicElement(
				By.xpath(myBigAndTallObjs.getTheByXPathAccessories()));
		driver.findElement(
				By.xpath(myBigAndTallObjs.getTheByXPathAccessories())).click();
		myBigAndTallObjs.CreateBigAndTallAccessoriesLinks();
		myProductListingObjs.traverseLinks(
				myBigAndTallObjs.getTheBigAndTallAccessoriesLinks(),  myProductDetailObjs, "linktext", server, testName);

		System.out
				.println("                 I HAVE MADE IT PAST THE Accessories LIST!!!!!!!!!!!!!!");

	}
	
	   public void ExtractJSLogs() {
			
			
	        LogEntries logEntries = driver.manage().logs().get("browser");
	        for (LogEntry entry : logEntries) {
	            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
	        }
	   }

	@After
	public void quitDriver() throws Exception {

		driver.quit();
	//	server.stop();

	}
	
	
}
