
package ChromeCacheTests;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;

public class ChromeOutletMenProducts {

	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
    private String testName =  "TheTraverseMensProductsTest";
    private SignInPage mySIO;
	WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		
	/*	File fileToProfile = new File("C:\\Users\\jwest\\Desktop\\Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);*/
		
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
	}

	@Test
	public void TheTraverseMensProductsTest() throws InterruptedException {
		
		WebElement currentElement;
		String URL;
		driver.get("http://174.143.230.85:14384/?site=TommyBahamaOutletCMSSite");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


		
		mySIO.signInOutlet(
				mySIO.getTheWAUsername(), mySIO.getThePassword());
		myEnvironment.waitForPageLoaded();
		
		
		myEnvironment.waitForClickableElement( By.xpath("//a[contains(text(),'Men')]"));
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//a[contains(text(),'Men')]"));
		currentElement.click();
		
		myProductListingObjs.traverseOutletPLP(myProductDetailObjs);
		
		System.out
		.println("                 I HAVE MADE IT PAST THE Men PLP!!!!!!!!!!!!!!");
		
	myEnvironment.waitForClickableElement( By.xpath("//a[contains(text(),'Big & Tall')]"));
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//a[contains(text(),'Big & Tall')]"));
		currentElement.click();
		
		myProductListingObjs.traverseOutletPLP(myProductDetailObjs);
		
		
		System.out
		.println("                 I HAVE MADE IT PAST THE Big & Tall PLP!!!!!!!!!!!!!!");
		
	myEnvironment.waitForClickableElement( By.xpath("//a[contains(text(),'Women')]"));
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//a[contains(text(),'Women')]"));
		currentElement.click();
		
		myProductListingObjs.traverseOutletPLP(myProductDetailObjs);
		
		System.out
		.println("                 I HAVE MADE IT PAST THE Women PLP!!!!!!!!!!!!!!");
		
	myEnvironment.waitForClickableElement( By.xpath("//a[contains(text(),'Swim')]"));
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//a[contains(text(),'Swim')]"));
		currentElement.click();
		
		myProductListingObjs.traverseOutletPLP(myProductDetailObjs);
		
		System.out
		.println("                 I HAVE MADE IT PAST THE Swim PLP!!!!!!!!!!!!!!");
		
	myEnvironment.waitForClickableElement( By.xpath("//a[contains(text(),'Home Decor')]"));
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//a[contains(text(),'Home Decor')]"));
		currentElement.click();
		
		myProductListingObjs.traverseOutletPLP(myProductDetailObjs);
		
		
		

		System.out
				.println("                 I HAVE MADE IT PAST THE Home Decor PLP!!!!!!!!!!!!!!");
		// driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
		// now we start on the sub links. This is the sub links of New Arrivals

	}

	@After
	public void quitDriver() {

		driver.quit();

	}
}

