package ChromeCacheTests;

import java.io.File;
import java.util.concurrent.TimeUnit;

import net.lightbody.bmp.proxy.ProxyServer;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;


public class ChromeCacheHomeDecor {

	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	
	private String testName = "TheTraverseHomeDecorProductsTest";
	ProxyServer server = new ProxyServer(8090);
	WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
	}

	@Test
	public void TheTraverseHomeDecorProductsTest() throws InterruptedException {
		// IWebElement element;
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// We first run through each link that directly shows PLP page from the
		// Men's Tab.
		driver.findElement(By.xpath(myHeaderObjs.getTheByXPathHomeDecorTab()))
				.click();
		myHomeDecorObjs.createHomeDecorLinkList();
		myProductListingObjs.traverseProducts(
				myHomeDecorObjs.getTheHomeDecorLinkList(), myProductDetailObjs, "xpath", server, testName);

		System.out
				.println("                 I have PAST THE Main XPath Product LISTs!!!!!!!!!!!!!!");
	}

	@After
	public void quitDriver() {

		driver.quit();

	}
}
