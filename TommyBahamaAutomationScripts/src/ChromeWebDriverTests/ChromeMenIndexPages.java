package ChromeWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.SeleniumEnvironment;

@RunWith(JUnit4.class)
public class ChromeMenIndexPages {

	private SeleniumEnvironment myEnvironment;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private String testName = "MenIndexPageTest";

	private int ss = 0;
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}
	   private WebDriver driver;
	 
/*	   @BeforeClass
	   public static void createAndStartService() throws IOException {
	     service = new ChromeDriverService.Builder()
	         .usingDriverExecutable(new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe"))
	         .usingAnyFreePort()
	         .build();
	     service.start();
	   }
*/
	@Before
	public void openBrowser() {

		
		ChromeOptions options = new ChromeOptions();
		//options.addExtensions("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		// set some options
		DesiredCapabilities dc = DesiredCapabilities.chrome();
		dc.setCapability(ChromeOptions.CAPABILITY, options);
		//WebDriver driver = new RemoteWebDriver(dc);
		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver(dc);
	
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		
		List<String> fns = new ArrayList<String>();
		fns.add("MensIndex");
		fns.add("MLBIndex");
		fns.add("WeddingIndex");
		fns.add("MensSwimShopIndex");
		
		String tempFile = myEnvironment.getNetworkTestDirectory() +
				 myEnvironment.getChromeBrowser() + "\\" + testName;
		
		myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void MenIndexPageTest() throws InterruptedException, IOException {
		WebElement ce;
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setTestSubject("Men's Index Page Test Results");

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		ce.click();

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MensIndex", "");
	

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheFooterEmailBtn()));
    	js.executeScript(mouseOverScript, home);
    	
    	ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailInput()));
		ce.click();
    	
    	//input[@id='emailFormFooterId']

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myMenObjs.getTheByXPathMLB()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MLBIndex", "");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myMenObjs.getTheByXPathWedding()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "WeddingIndex", "");
		ss = ss + 1;
		// this link has been removed, may return. 
		/*ce = myEnvironment.waitForDynamicElement(
				By.linkText(myMenObjs.getTheByLinkMensSwimShop()));
		ce.click();*/
		
		
		
		
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MensSwimShopIndex", "");
		ss = ss + 1;

		myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));

			
			bw.write("<html><body><center><h2>Different Men's Index Pages</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+ "<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is just to see if the Men's Index Pages are displaying correctly.</h3><table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ testName
					+ ".jpeg</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, "MensIndex",
							"This is the Men's Index page.")
					+
					
					myEnvironment.addSSToFile(testName, "MLBIndex",
							"This is the MLB Index page.")
					+
					
					myEnvironment.addSSToFile(testName, "WeddingIndex",
							"This is the Wedding Index page.")
					+
					
					myEnvironment.addSSToFile(testName, "MensSwimShopIndex",
							"This is the Men's Swim Shop Index page.")
					+

					myEnvironment.getPageTestOutcome()
					+ "</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Different Men's Index Pages</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+ "<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is just to see if the Men's Index Pages are displaying correctly.</h3><table style= \"width:70%;\"><tr><td><p> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");

			List<String> fns = new ArrayList<String>();
			fns.add("MensIndex");
			fns.add("MLBIndex");
			fns.add("WeddingIndex");
			fns.add("MensSwimShopIndex");

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);

			driver.quit();

		}

	}