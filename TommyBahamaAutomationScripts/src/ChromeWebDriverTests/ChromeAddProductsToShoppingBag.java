package ChromeWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class ChromeAddProductsToShoppingBag {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private AccountPage myAccountObjs;
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	private ShoppingBagPage myShoppingBag;
    private Product myProducts = new Product();


	private String testName = "TheAddProductsToShoppingBagTest";
	private int ss = 0;

	String OrderNumber = "";
	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	// private String baseUrl;
	private WebDriver driver;
	private WebElement ce;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();

		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());

		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
	
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myAccountObjs = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		 
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("MyAccountPage");
			fns.add(myProductListingObjs.getThePageName() + "Dinnerware");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add(myProductListingObjs.getThePageName()  + myProducts.getTheHighPantsPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice());
			fns.add("PDPTraditionalGC");
			fns.add(myProductListingObjs.getThePageName()  + "OversizeProduct");
			if(myEnvironment.getTheTestingEnvironment().contains("test"))
				myProducts.setTheOversizedProduct("TBG-099");
			else myProducts.setTheOversizedProduct("TH31689");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheOversizedProduct());
			fns.add(myProductListingObjs.getThePageName()  + myProducts.getTheLowPantsPrice());
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheLowPantsPrice());
			fns.add("ShoppingBagEmpty");
		 
			String tempFile = myEnvironment.getNetworkTestDirectory() +
					 myEnvironment.getChromeBrowser() + "\\" + testName;
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheAddProductsToShoppingBagTest() throws InterruptedException {

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment
				.setTestSubject("Add and Remove Products from Shopping Bag Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// Signe into an canada account
		mySIO.signIn(
				mySIO.getTheCanadaNineUserName(), mySIO.getThePassword());
		myEnvironment.waitForPageLoaded();
		// ss = ss + 1;
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MyAccountPage", "");
		// ss = ss + 1;
		// Go to home page
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		ce.click();
		// adds to the shopping bag
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myProductDetailObjs.selectProduct(
				
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "1", "Dinnerware");

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "ItemPreview",
				myProducts.getTheDinnerWareProduct());

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProductByPrice(myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathPants(), "$128.00", "1",
				"Pants");

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "ItemPreview",
				"Pants");
		myEnvironment.waitForPageLoaded();

		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myTraditionalCardObjs.addTraditionalCard(myEnvironment, driver,
				myHeaderObjs, myMenObjs, testName, "PDPTraditionalGC", "");
		myEnvironment.waitForPageLoaded();

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "ItemPreview",
				"TraditionalCard");

		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// Add second restricted product
		if(driver.getCurrentUrl().contains("test")) {
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
			 "OversizedProduct");
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "ItemPreview",
				"OversizedProduct");
		}
		else {
			myProductDetailObjs.selectProduct(
					myHeaderObjs.getTheByXPathHomeDecorTab(),
					myHomeDecorObjs.getTheByXPathViewAllBeachGear(), myProducts.getTheBeachProduct(), "1",
				 "Beach");
			myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "ItemPreview",
					"OversizedProduct");
			}
	

		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myEnvironment.waitForPageLoaded();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myEnvironment.waitForPageLoaded();
		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathPants(), "$128.00", "1",
				"Pants");

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		ce.click();

		// remove all the products
		myShoppingBag.removeAllProducts(5);
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "Empty");
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		myEnvironment.setTestPassed(true);

	}
	@After
	public void quitDriver() throws MessagingException,
			InterruptedException, IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Canada Order Test Setup Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%; \">This is the Canada Setup Order test. All Canada tests have similar products added. Due to the large size of the PLP and PDP, the results for added products are only shown in this test.</h3>"
				+ "<table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+

			myEnvironment.addSSToFile(testName, "MyAccountPage",
						"This is the Account Page.")
				+
				
				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + "Dinnerware",
						"This is the Dinnerware PLP.")
				+
				
			myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+

				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName()  + myProducts.getTheHighPantsPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice(),
						"This is the Pants PDP.")
				+
				
			

				myEnvironment.addSSToFile(testName, "PDPTraditionalGC",
						"This is the GiftCard PDP.")
				+
				
			

				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + "OversizeProduct",
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheOversizedProduct(),
						"This is the Golf PDP.")
				+
	
				
				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheLowPantsPrice(),
						"This is the Pants PLP.")
				+

				
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheLowPantsPrice(),
						"This is the Pants PDP.")
				+

		
				myEnvironment.addSSToFile(testName, "ShoppingBagEmpty",
						"This is the Empty Shopping Bag Page.")
				+

			    myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Order Test Setup Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%; \">This is the Canada Setup Order test. All Canada tests have similar products added. Due to the large size of the PLP and PDP, the results for added products are only shown in this test.</h3>"
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");
					

		List<String> fns = new ArrayList<String>();
		fns.add(myProductListingObjs.getThePageName() + "Dinnerware");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
		fns.add(myProductListingObjs.getThePageName()  + myProducts.getTheHighPantsPrice());
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice());
		fns.add("PDPTraditionalGC");
		fns.add(myProductListingObjs.getThePageName()  + "OversizeProduct");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheOversizedProduct());
		fns.add(myProductListingObjs.getThePageName()  + myProducts.getTheLowPantsPrice());
		fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheLowPantsPrice());
		fns.add("ShoppingBagEmpty");

		ss = ss - 9;
		
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);
		driver.quit();
	}
}

