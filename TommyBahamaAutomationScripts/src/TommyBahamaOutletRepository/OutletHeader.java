package TommyBahamaOutletRepository;


import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OutletHeader {
	
	

	private final String theByIdaSignedIn = "aSignedIn";

	private final String theByXPathRegistrationLink = "//span[@id='spanAccountAnon']/a[2]/span";

	private final String theByXPathSignOut = "//a[contains(text(),'Sign Out')]";
	// private final String theSignIn =
	// "//span[@id='spanAccountAnon']/a[2]/span";
	private final String theSignIn = "//*[@id='siteStripeRight_LO']/li[1]/div/a";
	private final String theByXPathEmailSignUp = "//a[contains(text(),'Sign up for Email')]";
	


    private final String theSearchInput = "//input[@id='search']";
	private final String theSearchBtn = "//input[@type='image']";

	private final String theByXPathMensTab = "//a[contains(text(),'Men')]";
	private final String theByXPathWomensTab = "//a[contains(text(),'Women')]";
	private final String theByXpathHomeDecorTab = "//a[contains(text(),'Home Decor')]";
	private final String theByXPathBigAndTallTab = "//a[contains(text(),'Big & Tall')]";
	private final String theByXPathSwimTab = "(//a[contains(text(),'Swim')])[4]";
	private final String theByXPathStoresAndRestaurantsTab = "//li[@id='topNavLIStores']/a";
	private final String theByXPathLiveTheLifeTab = "//li[@id='topNavLILive']/a/span";


	// private final String theByLinkTextMensTab = "Men";
	private final String twoItemsInCheckout = "//a[contains(text(),'2 ITEMS PREVIEW')]";
	private final String theByIdPreviewCheckout = "checkout";
	private final String theFooterEmailBtn = "//div[@id='footerEmailSignUpInner']/form/div";
	private final String theFooterEmailInput = "//input[@id='emailFormFooterId']";
	private final String theItemPreviewShopping = "//a[@id='shopping']";
	private final String theByLinkCheckout = "CHECKOUT";
	private final String theByLinkFindStore = "//a[contains(text(),'Find a Tommy Bahama Store')]";
	private final String theUrlType = "http://";
	private final String theMyAccount = "//a[contains(@href, '/en/my-account')]";
	private final String theByIdCanadaWelcome = "divCanadaWelcomeMat";
	private final String theByCSSCanadaShippingPromo = "#divPromo > img";
	private final String theByCSSShippingParadaisePromo = "#divPromo > a > img";
	private final String theByXPathSearchInput = "//input[@id='q']";
	private final String theQuickViewContent = "//div[@id='quickview-content']";
	private final String theByIdQuickViewContent = "quickview-content";
	private final String theSignedInAccount = "//span[@id='aSignedIn']";
	private final String thePageName = "Header";
	
	private final String theByIdEmailAddress = "txtEmail";
	private final String theByIdFirstName = "txtFirstName";
	private final String theByIdLastName = "txtLastName";
	private final String theByIdZipCode = "txtZipCode";
	private final String theByIdMonth = "txtDOB_mm";
	private final String theByIdDay = "txtDOB_dd";
	private final String theByIdYear = "txtDOB_yyyy";
	private final String theByNameCountry = "country";
	private final String theByNameGender = "Gender";
	private final String theByIdTBCheckBox = "chkTommyBahamaEmail";
	private final String theByIdRestaurantCheckBox = "chkRest";
	private final String theByIdBigAndTallCheckBox = "chkBigAndTall";
	private final String theByXpathGender = "//div[@id='divGender']/input[4]";
	
	
	
	private final String theByCssSavePreferences = "img[alt='Save Preferences']";
	private final String theByCssStartShopping = "img[alt='Start Shopping']";
    private final String theByCssCloseEmailSignUp = "a.fancybox-item.fancybox-close";
    private final String theByCssMenSwim = "area[alt='Men's swim']";
    
	
	private WebDriver driver;
	
	private OutletWebdriverEnvironment myEnvironment;
	
	public OutletHeader(WebDriver theDriver, OutletWebdriverEnvironment theEnvironment)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
	}
	
	

	

	
	public String getTheByXPathRegistrationLink() {
		return theByXPathRegistrationLink;
	}

	

	public String getTheByXPathSignOut() {
		return theByXPathSignOut;
	}

	public String getTheSignIn() {
		return theSignIn;
	}

	

	public String getTheSearchBtn() {
		return theSearchBtn;
	}

	public String getTheByXPathMensTab() {
		return theByXPathMensTab;
	}

	public String getTheByXPathWomensTab() {
		return theByXPathWomensTab;
	}

	public String getTheByXPathHomeDecorTab() {
		return theByXpathHomeDecorTab;
	}

	public String getTheByXPathBigAndTallTab() {
		return theByXPathBigAndTallTab;
	}

	public String getTheByXPathSwimTab() {
		return theByXPathSwimTab;
	}

	public String getTheByXPathStoresAndRestaurantsTab() {
		return theByXPathStoresAndRestaurantsTab;
	}

	public String getTheByXPathLiveTheLifeTab() {
		return theByXPathLiveTheLifeTab;
	}

	public String getTwoItemsInCheckout() {
		return twoItemsInCheckout;
	}

	public String getTheByIdPreviewCheckout() {
		return theByIdPreviewCheckout;
	}

	public String getTheFooterEmailBtn() {
		return theFooterEmailBtn;
	}

	public String getTheFooterEmailInput() {
		return theFooterEmailInput;
	}

	public String getTheItemPreviewShopping() {
		return theItemPreviewShopping;
	}

	public String getTheByLinkCheckout() {
		return theByLinkCheckout;
	}

	public String getTheByLinkFindStore() {
		return theByLinkFindStore;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheMyAccount() {
		return theMyAccount;
	}

	public String getTheByIdCanadaWelcome() {
		return theByIdCanadaWelcome;
	}

	public String getTheByCSSCanadaShippingPromo() {
		return theByCSSCanadaShippingPromo;
	}

	public String getTheByCSSShippingParadaisePromo() {
		return theByCSSShippingParadaisePromo;
	}

	public String getTheByXPathSearchInput() {
		return theByXPathSearchInput;
	}

	public String getTheQuickViewContent() {
		return theQuickViewContent;
	}

	public String getTheSignedInAccount() {
		return theSignedInAccount;
	}


	public String getTheByIdQuickViewContent() {
		return theByIdQuickViewContent;
	}

	public void checkUrl(String thePageUrl, String theUrl) {
		Assert.assertTrue(theUrl.contains(thePageUrl));
	}



	/*
	 * this is a method to check that the correct flag and advertisment are
	 * displaying for the chosen fifty one country Passed the webdriver object,
	 * the enviroment object, the url and the associated promo that is displayed
	 * in header, the flag that should be displayed.
	 */
	public void checkFlagInHeader(WebDriver driver,
			OutletWebdriverEnvironment myEnvironment, String url, By theFlag,
			String thePromo) {
		String theShippingPromoBannerImage = "";
		WebElement currentElement;
		if (myEnvironment.IsElementPresent(theFlag)) {
			System.out.println("The Correct Flag is Present.");
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(thePromo));
			System.out.println("This is the image of the Banner:     "
					+ currentElement.getAttribute("src"));
			theShippingPromoBannerImage = currentElement.getAttribute("src");
			if (url.contains("https:")) {
				// theShippingPromoBannerImage =
				// theShippingPromoBannerImage.Remove(0, 14);
				Assert.assertTrue(theShippingPromoBannerImage
						.contains("/media/TB001/images/static/"));
				System.out
						.println("Shipping  Banner is appearing on this secure Page!");
			} else {
				// theShippingPromoBannerImage =
				// theShippingPromoBannerImage.Remove(0, 13);
				Assert.assertTrue(theShippingPromoBannerImage
						.contains("/media/TB001/images/static/"));
				System.out
						.println("Shipping  Banner is appearing on this NON secure Page!");
			}
		}
	}

	public String getTheByIdaSignedIn() {
		return theByIdaSignedIn;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByXPathEmailSignUp() {
		return theByXPathEmailSignUp;
	}

	public String getTheByIdEmailAddress() {
		return theByIdEmailAddress;
	}

	public String getTheByIdFirstName() {
		return theByIdFirstName;
	}

	public String getTheByIdLastName() {
		return theByIdLastName;
	}

	public String getTheByIdZipCode() {
		return theByIdZipCode;
	}

	public String getTheByIdMonth() {
		return theByIdMonth;
	}

	public String getTheByIdDay() {
		return theByIdDay;
	}

	public String getTheByIdYear() {
		return theByIdYear;
	}


	public String getTheByCssSavePreferences() {
		return theByCssSavePreferences;
	}

	public String getTheByNameGender() {
		return theByNameGender;
	}

	public String getTheByCssStartShopping() {
		return theByCssStartShopping;
	}

	public String getTheByCssCloseEmailSignUp() {
		return theByCssCloseEmailSignUp;
	}

	public String getTheByCssMenSwim() {
		return theByCssMenSwim;
	}






	public String getTheSearchInput() {
		return theSearchInput;
	}






	public String getTheByXpathGender() {
		return theByXpathGender;
	}






	public String getTheByNameCountry() {
		return theByNameCountry;
	}






	public String getTheByIdTBCheckBox() {
		return theByIdTBCheckBox;
	}






	public String getTheByIdRestaurantCheckBox() {
		return theByIdRestaurantCheckBox;
	}






	public String getTheByIdBigAndTallCheckBox() {
		return theByIdBigAndTallCheckBox;
	}


}
