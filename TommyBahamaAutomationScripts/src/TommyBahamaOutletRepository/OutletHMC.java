package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaRepository.SignInPage;

public class OutletHMC {
	
	private final String theByXPathSubmitLogin = "//span";
	
	
	
	private final String theByIdSearchByJob = "Content/AllInstancesSelectEditor[in Content/GenericCondition[CronJob.job]]_select";
	
	
	private final String theSiteDetailsCronJobIndex = "42";
	
	private final String theByIdSiteDetailsFirstCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap01]_span";
	private final String theByIdSiteDetailsSecondCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap02]_span";
	private final String theByIdSiteDetailsThirdCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap03]_span";
	private final String theByIdSiteDetailsFourthCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap04]_span";
	
	
	private final String theByIdSubmitSearchButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	
	private final String theByIdSiteDetailsStartFirstCronJob = "Content/GenericItemChip$1[action.performcronjob]";
	private final String theByIdSiteDetailsStartSecondCronJob = "Content/GenericItemChip$1[action.performcronjob][1]_label";
	private final String theByIdSiteDetailsStartThirdCronJob = "Content/GenericItemChip$1[action.performcronjob][2]_label";
	private final String theByIdSiteDetailsStartFourthCronJob = "Content/GenericItemChip$1[action.performcronjob][3]_label";
	
	
	private final String theByIdSearchResultsButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	private final String theByIdHomeButton = "Explorer[mcc]_label";
	private final String theByIdEndSessionButton = "Toolbar/ImageToolbarAction[closesession]_img";
	
	
	private WebDriver driver;
	private int cronJobsRan = 1;
	private String testName;
	
	private OutletWebdriverEnvironment myEnvironment;
	
	private OutletHmcLefNav myHmcLeftNav;

	public OutletHMC(){
		
	}
	
    public OutletHMC(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment, OutletHmcLefNav theHmcLeftNav, String theTestName){
    	
    	driver = theDriver;
    	myEnvironment = theEnvironment;
    	myHmcLeftNav = theHmcLeftNav;
    	testName = theTestName;
    	
		
	}
	
	
	public String getTheByXPathSubmitLogin() {
		return theByXPathSubmitLogin;
	}




	public String getTheByIdSearchByJob() {
		return theByIdSearchByJob;
	}




	public String getTheSiteDetailsCronJobIndex() {
		return theSiteDetailsCronJobIndex;
	}




	public String getTheByIdSiteDetailsFirstCronJob() {
		return theByIdSiteDetailsFirstCronJob;
	}




	public String getTheByIdSiteDetailsSecondCronJob() {
		return theByIdSiteDetailsSecondCronJob;
	}




	public String getTheByIdSiteDetailsThirdCronJob() {
		return theByIdSiteDetailsThirdCronJob;
	}




	public String getTheByIdSiteDetailsFourthCronJob() {
		return theByIdSiteDetailsFourthCronJob;
	}




	public String getTheByIdSubmitSearchButton() {
		return theByIdSubmitSearchButton;
	}




	public String getTheByIdSiteDetailsStartFirstCronJob() {
		return theByIdSiteDetailsStartFirstCronJob;
	}




	public String getTheByIdSiteDetailsStartSecondCronJob() {
		return theByIdSiteDetailsStartSecondCronJob;
	}




	public String getTheByIdSiteDetailsStartThirdCronJob() {
		return theByIdSiteDetailsStartThirdCronJob;
	}




	public String getTheByIdSiteDetailsStartFourthCronJob() {
		return theByIdSiteDetailsStartFourthCronJob;
	}




	public String getTheByIdSearchResultsButton() {
		return theByIdSearchResultsButton;
	}
	
	public void fireSiteDetailsCronJob(String theJob) throws InterruptedException {
		
		driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;
		
    	// log into the hmc
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheByXPathSubmitLogin()));
		ce.click();
		
		myEnvironment.waitForURL("wid");
		// click the System folder in the hmc
		ce = myEnvironment.waitForDynamicElement(
				By.id(myHmcLeftNav.getTheByIDLeftNavSystemFolder()));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(myHmcLeftNav.getTheByIDLeftNavCronJob()));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSearchByJob()));
		Select clickThis = new Select(ce);
		Thread.sleep(1000);
		clickThis.selectByIndex(42);
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdSearchResultsButton));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(theJob));
		Actions action = new Actions(driver);
		action.doubleClick(ce).perform();
	
		Thread.sleep(5000);
		
		cronJobsRan = cronJobsRan + 1;
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdEndSessionButton));
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.theByIdEndSessionButton));
		ce.click();
	//	ce.click();
		
		Thread.sleep(5000);
		
	
		//this is blanked out so we do not run the cronjob and mess peeps up.
	//	ce = myEnvironment.waitForDynamicElement(
	//			By.id("Content/GenericItemChip$1[action.performcronjob]"));
	//	ce.click();
		
	/*	ce = myEnvironment.waitForDynamicElement(
				By.id("Content/OrganizerComponent[organizerlist][CronJob]_togglelabel"));
	
		ce.click();*/

	}

	public String getTheByIdHomeButton() {
		return theByIdHomeButton;
	}

	public String getTheByIdEndSessionButton() {
		return theByIdEndSessionButton;
	}

}
