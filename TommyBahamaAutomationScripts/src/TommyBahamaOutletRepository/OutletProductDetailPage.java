package TommyBahamaOutletRepository;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

// This is the Product Detail Page Class that contains the element repository for the page and methods that 
// execute functionality that is centered around the PDP. 
public class OutletProductDetailPage {
	private final String theByXPathFirstSize = "//li/div[2]/div[2]/div/a";
	private final String theByLinkMediumSize = "M";
	private final String theByXPathSecondSize = "//div[2]/div[2]/a";
	private final String theByCSSProductPrice = "p.content_price";
	private final String theByXPathThirdSize = "//div[@id='XXL']";
	private final String theByLinkTextXLargeSize = "XL";
	private final String theByLinkTextLargeSize = "L";
	private final String theByLinkText34By32Size = "34 x 32";
	private final String theByLinkText36By32Size = "36 x 32";
	private final String theByXpathSizeEleven = "(//a[contains(text(),'11')])[2]";
	private final String theByXpathAddToBagButton = "//form[@id='addToCartForm']/div[2]/div[3]/div";
	private final String theByCSSAddToBagLink = "#aBtnAddToBagLink";
	private final String theByIdAddToBagLink = "aBtnAddToBagLink";
	private final String theByIdAddToBagBtn = "divAddToBagButton";
	private final String theByOneXByBigLink = "//div[@id='1X x BG']/a";
	private final String theFirstCrossSellItem = "//p/a";
	private final String theByIdSelectQuantity = "qty";
	private final String theProductSize = "//div[@id='divProductSize']/div/div/a";
	private final String theHelpLink = "(Help)";
	private final String theQuantityFour = "4";
	private final String theQuantityTwo = "2";
	private final String theByIDSelectQty = "selQuantity";
	private final String theUrlType = "http://";
	private final String theByXpathCanadaRestriction = "//div[@id='choiceContainer']/div[2]";
	private final String theByXPathSwatchDescription = "//span[@id='swatch_description']";
	private final String theMainImage = "//*[@id='divMainImageContainer']";
	private final String theMainProduct = "//div[@id='divMainProductContainer']/div[2]";
	
	private final String thePageName = "PDP";
	private final String thePreviewPaneName = "ProductPreview";
	
	private WebDriver driver;
	private OutletWebdriverEnvironment myEnvironment;
	private OutletProductListingPage myProductListingObjs;
	private OutletHeader myOutletHeader;
	private String testName;
	
	public OutletProductDetailPage(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment,
			OutletProductListingPage theProductListingObjs, OutletHeader theOutletHeader,
		    String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myProductListingObjs = theProductListingObjs;
		myOutletHeader = theOutletHeader;
		testName = theTestName;
	}

	public String getTheByIdAddToBagBtn() {
		return theByIdAddToBagBtn;
	}

	public String getTheByIdAddToBagLink() {
		return theByIdAddToBagLink;
	}

	public String getTheMainProduct() {
		return theMainProduct;
	}

	public String getTheByXPathFirstSize() {
		return theByXPathFirstSize;
	}

	public String getTheByLinkMediumSize() {
		return theByLinkMediumSize;
	}

	public String getTheByXPathSecondSize() {
		return theByXPathSecondSize;
	}

	public String getTheByCSSProductPrice() {
		return theByCSSProductPrice;
	}

	public String getTheByXPathThirdSize() {
		return theByXPathThirdSize;
	}

	public String getTheByLinkTextXLargeSize() {
		return theByLinkTextXLargeSize;
	}

	public String getTheByLinkTextLargeSize() {
		return theByLinkTextLargeSize;
	}

	public String getTheByLinkText34By32Size() {
		return theByLinkText34By32Size;
	}

	public String getTheByLinkText36By32Size() {
		return theByLinkText36By32Size;
	}

	public String getTheByXpathSizeEleven() {
		return theByXpathSizeEleven;
	}

	public String getTheByXpathAddToBagButton() {
		return theByXpathAddToBagButton;
	}

	public String getTheByCSSAddToBagLink() {
		return theByCSSAddToBagLink;
	}

	public String getTheByOneXByBigLink() {
		return theByOneXByBigLink;
	}

	public String getTheFirstCrossSellItem() {
		return theFirstCrossSellItem;
	}

	public String getTheByIdSelectQuantity() {
		return theByIdSelectQuantity;
	}

	public String getTheProductSize() {
		return theProductSize;
	}

	public String getTheHelpLink() {
		return theHelpLink;
	}

	public String getTheQuantityFour() {
		return theQuantityFour;
	}

	public String getTheQuantityTwo() {
		return theQuantityTwo;
	}

	public String getTheByIDSelectQty() {
		return theByIDSelectQty;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheByXpathCanadaRestriction() {
		return theByXpathCanadaRestriction;
	}

	public String getTheByXPathSwatchDescription() {
		return theByXPathSwatchDescription;
	}

	public String getTheMainImage() {
		return theMainImage;
	}

	/*
	 * This method is a universal method to add products by price to the
	 * shopping bag that are located in sub categories of a section An example
	 * of this is Socks under Mens Accessories. This method is passed the
	 * Webdriver and Environmental objects, the product listing and header
	 * objects The method is also passed the tab to select the first and sub
	 * link to select and the price and quantity
	 */
	public void selectSubProductByPrice(
			String theTab, String theLink, String sublink, String thePrice,
			String theQuantity, String theProductTitle) throws InterruptedException {
		WebElement currentElement;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement.click();
		// WebElement home =
		// driver.findElement(By.cssSelector(myOutletHeader.getTheByCSSLogo()));
		// Actions action = new Actions(driver);//simply my webdriver
		// action.moveToElement(home).perform();//move to list element that
		// needs to be hovered
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(sublink));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle + thePrice);
		String theSelectedItem = "";
		theSelectedItem = this.findWantedUSPrice(thePrice);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		System.out
				.println("THis is the selected item       " + theSelectedItem);
		currentElement = driver.findElement(By.xpath(theSelectedItem));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				thePrice);
		this.addQuantityToBag( theQuantity,	 thePrice);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag that are located in sub categories of a section An example
	 * of this is Socks under Mens Accessories. This method is passed the
	 * Webdriver and Environmental objects, the product listing and header
	 * objects The method is also passed the tab to select the first and sub
	 * link to select and the product and quantity
	 */
	public void selectSubProductByProduct(
			String theTab, String theLink, String sublink, String theProduct,
			String theQuantity) throws InterruptedException {
		WebElement currentElement;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(2000);
		// WebElement home =
		// driver.findElement(By.cssSelector(myOutletHeader.getTheByCSSLogo()));
		// Actions action = new Actions(driver);//simply my webdriver
		// action.moveToElement(home).perform();//move to list element that
		// needs to be hovered
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(sublink));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				"Products");
		String theSelectedItem = "";
		theSelectedItem = this.findProduct(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);
		this.addQuantityToBag(theQuantity);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag This method is passed the Webdriver and Environmental
	 * objects, the product listing and header objects The method is also passed
	 * the tab to select the first link to select and the product and quantity
	 */
	public void selectProduct(String theTab, String theLink, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {

		WebElement currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		currentElement.click();
		
	//	myEnvironment.waitForTitle(driver, theName);
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.xpath(myOutletHeader.getTheFooterEmailBtn()));

		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		String theSelectedItem = "";
		theSelectedItem = this.findProduct(theProduct);
		System.out.println("This is the found product:           "
				+ theSelectedItem);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement.click();
	//	currentElement = myEnvironment.waitForDynamicElement(
	//			By.xpath(myOutletHeader.getTheFooterEmailBtn()));
	//	Thread.sleep(myEnvironment.getThe_Special_Sleep());
	//	currentElement = myEnvironment.waitForDynamicElement(
	//			By.xpath(myOutletHeader.getTheFooterEmailBtn()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSelectQuantity()));
		
	//	currentElement = myEnvironment.waitForDynamicElement(
	//			By.xpath("/html/body/div[2]/div/div[2]/div[3]/div[2]/div[5]/div/span/a"));
	//	currentElement.click();
	
	//	myEnvironment.handleMultipleWindows( "Pinterest");
		
	//	driver.close();
		
	//	Thread.sleep(5000);
		
	//	driver.switchTo().defaultContent();
		
	//	myEnvironment.handleMultipleWindows(theProductTitle);
		
	//	Thread.sleep(5000);
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);

		this.addQuantityToBag(theQuantity);
	}

	public void selectProductByHover(String theTab, String theLink, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {
		WebElement currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(currentElement).perform();// move to list element
														// that needs to be
														// hovered

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		// action = new Actions(driver);//simply my webdriver
		// action.moveToElement(currentElement).perform();//move to list element
		// that needs to be hovered
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		String theSelectedItem = "";

		theSelectedItem = this.findProduct(theProduct);
		System.out.println("This is the found product:           "
				+ theSelectedItem);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myOutletHeader.getTheFooterEmailBtn()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		// Thread.Sleep(1000);
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);
		this.addQuantityToBag(theQuantity);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag by the products price This method is passed the Webdriver
	 * and Environmental objects, the product listing and header objects The
	 * method is also passed the tab to select the first link to select and the
	 * price and quantity
	 */
	public void selectProductByPrice(
			String theTab, String theLink, String thePrice, String theQuantity,
			 String theProductTitle)
			throws InterruptedException {
		WebElement currentElement = myEnvironment.fluentWait(
				By.xpath(theTab));
		currentElement.click();

		currentElement = myEnvironment.fluentWait(
				By.xpath(myOutletHeader.getTheFooterEmailBtn()));
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(currentElement).perform();// move to list element
														// that needs to be
														// hovered
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		String thePriceWithoutSpaces = thePrice;
		thePriceWithoutSpaces = thePriceWithoutSpaces.replace(" ", "_");
		System.out.println("this is the Price with a underscore included .                 "  +    thePriceWithoutSpaces);
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle + thePriceWithoutSpaces);
		String theSelectedItem = "";
		theSelectedItem = this.findWantedUSPrice(thePrice);
		System.out.println("This is the selected item:        "
				+ theSelectedItem);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.fluentWait(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProductTitle + thePriceWithoutSpaces);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		this.addQuantityToBag( theQuantity,	 thePrice);
	}

	/*
	 * This method finds a product by the product id that is located in the
	 * src attribute of an element.  This method is passed the
	 * product id of the product that is going to be selected.
	 */
	public String findProduct(String theProduct) {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
		WebElement currentElement;
		if (myEnvironment.IsElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			currentElement.click();
		}
		String Product = "";
		String thumbNailStart_i = "/html/body/div[3]/div/div[4]/div[4]/div[2]/div[2]/div[";
		String thumbNailEnd_i = "]/div[";
		String thumbNailEnd_j = "]/div/div/a/span/img";
		for (int i = 1; i < 6; i++) {
			for (int j = 1; j < 4; j++) {
			currentElement = myEnvironment.waitForClickableElement(
					By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j +  thumbNailEnd_j));
			Product = myProductListingObjs.pullProductCodeFromThumbNail(
					thumbNailStart_i, thumbNailEnd_i, thumbNailEnd_j,  i, j);
			if (Product.contains(theProduct)) {
				Product = thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j;
				return Product;
			}
			}
		}
		return Product;
	}

	public String findWantedUSPrice( String theWantedPrice) {
		String theProduct = "";
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='imgContainer_" + 0 + "']/div[2]/div"));
		if (myEnvironment.IsElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			currentElement.click();
			// driver.FindElement(By.XPath(myProductListingObjs.ShowAllLink)).Click();
		}
		for (int i = 0; i < 20; i++) {
			String Price = "";
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i + "']/div[2]/div"));
			// myEnvironment.myWaitForWDElement(driver,
			// By.XPath("//div[@id='imgContainer_" + i + "']/div[2]/div"));

			Price = currentElement.getText();
			Price = Price.trim();
			Assert.assertTrue(Price.contains("$") || Price.contains("GBP"));

			System.out.println("This is the price:       " + Price);
			System.out.println("This is the wanted price      "
					+ theWantedPrice);
			if (Price.contains(theWantedPrice)) {
				theProduct = "//div[@id='cat_list_title_" + i + "']/a";
				return theProduct;
			}
		}
		return theProduct;
	}

	/*
	 * This method adds products to the shopping bag This is passed the normal
	 * objects and the quantity of the object that is to be added to the
	 * shopping bag. For some reason find element combined with the click method
	 * can throw a timed out error. in some instances its needed to put the
	 * element into the IWebElement object and then find it sleep for 205 ms and
	 * then click it Hopefully this bug will be fixed soon or I get an upgrade
	 * on my machine to execute the methods(says this happens on slower
	 * machines). I have done this in places I do not think its required. It is
	 * Required presently in situations like this one where the test is
	 * selecting a value from a drop down. After this it is necesssary to
	 * perfrom the break up that is described above.
	 */
	public void addQuantityToBag(String theQuantity,
			 String thePrice) throws InterruptedException {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
		WebElement currentElement;
		// IWebElement element;
		// WebDriverWait wait = new WebDriverWait(driver,
		// TimeSpan.FromSeconds(180));
		if (myEnvironment.IsElementPresent(By.xpath(this.getTheProductSize()))) {
			for (int i = 0; i < 10; i++) {
				if (myEnvironment.IsElementPresent(
						By.xpath("//div[2]/div[2]/div[" + i + "]/a"))) {
					currentElement = myEnvironment.waitForDynamicElement(
							By.xpath("//div[2]/div[2]/div[" + i + "]/a"));
					System.out.println(currentElement.getAttribute("style"));
					if (currentElement.getAttribute("style").contains(
							"rgb(51, 103, 153)")
							|| currentElement.getAttribute("style").contains(
									"#336799")) {
						System.out
								.println("We are at the click so the size should be selected.");
						currentElement.click();

						Thread.sleep(myEnvironment.getThe_Default_Sleep());

						// TODO Auto-generated catch block

						break;
					}
				}
			}
			// This is code to add multiples of the same items to the cart at
			// the same time.
			// currentElement =
			if (theQuantity == "8") {
				currentElement = driver.findElement(By.id(this
						.getTheByIdSelectQuantity()));
				currentElement = currentElement.findElement(By
						.cssSelector("option[Value='8']"));
				// Thread.Sleep(myEnvironment.DefaultSleep);
				currentElement.click();
				// Thread.Sleep(myEnvironment.DefaultSleep);
				currentElement = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));

				Thread.sleep(myEnvironment.getThe_Default_Sleep());

				myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
						"PDP", thePrice);

				Thread.sleep(myEnvironment.getThe_Default_Sleep());

				currentElement.click();

				Thread.sleep(myEnvironment.getThe_Default_Sleep());

			} else {
				currentElement = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				Select clickThis = new Select(currentElement);
				Thread.sleep(1000);
				clickThis.selectByValue(theQuantity);
				currentElement = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				// myEnvironment.TakeScreenShot( theBrowser, theTest,
				// "PDP",
				// thePrice);
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
			}
		} else {
			// This is code to add multiples of the same items to the cart at
			// the same time.
			currentElement = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdSelectQuantity()));
			Select clickThis = new Select(currentElement);
			clickThis.selectByValue(theQuantity);
			currentElement = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdAddToBagLink()));
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			// myEnvironment.TakeScreenShot( theBrowser, theTest, "PDP",
			// thePrice);
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			currentElement.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		}
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
	}

	/*
	 * This method adds products to the shopping bag This is passed the normal
	 * objects and the quantity of the object that is to be added to the
	 * shopping bag. For some reason find element combined with the click method
	 * can throw a timed out error. in some instances its needed to put the
	 * element into the IWebElement object and then find it sleep for 205 ms and
	 * then click it Hopefully this bug will be fixed soon or I get an upgrade
	 * on my machine to execute the methods(says this happens on slower
	 * machines). I have done this in places I do not think its required. It is
	 * Required presently in situations like this one where the test is
	 * selecting a value from a drop down. After this it is necesssary to
	 * perfrom the break up that is described above.
	 */
	public void addQuantityToBag(String theQuantity)
			throws InterruptedException {
		WebElement currentElement;

		if (myEnvironment.IsElementPresent(By.xpath(this.getTheProductSize()))) {
			for (int i = 0; i < 10; i++) {
				if (myEnvironment.IsElementPresent(
						By.xpath("//div[2]/div[2]/div[" + i + "]/a"))) {
					currentElement = myEnvironment.waitForDynamicElement(
							By.xpath("//div[2]/div[2]/div[" + i + "]/a"));
					System.out.println(currentElement.getAttribute("style"));
					if (currentElement.getAttribute("style").contains(
							"rgb(51, 103, 153)")
							|| currentElement.getAttribute("style").contains(
									"#336799")) {
						System.out
								.println("We are at the click so the size should be selected.");
						currentElement.click();
						Thread.sleep(myEnvironment.getThe_Default_Sleep());
						break;
					}
				}
			}
			// This is code to add multiples of the same items to the cart at
			// the same time.
			if (theQuantity == "8") {
				currentElement = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				Select clickThis = new Select(currentElement);
				clickThis.selectByValue(theQuantity);
				currentElement = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
						"PDP", "8AddedToBag");
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());

			} else {
				currentElement = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				// Thread.Sleep(myEnvironment.DefaultSleep);
				Select clickThis = new Select(currentElement);
				// Thread.Sleep(myEnvironment.DefaultSleep);
				clickThis.selectByValue(theQuantity);
				 Thread.sleep(myEnvironment.getThe_Special_Sleep());
				 
				 
				//	currentElement = myEnvironment.waitForDynamicElement(
				//			By.id(this.getTheByIdAddToBagBtn()));
				//	currentElement.click();
				 
					currentElement = myEnvironment.waitForDynamicElement(
							By.id(this.getTheByIdAddToBagLink()));
					currentElement.click();
				// Thread.Sleep(myEnvironment.DefaultSleep);
			
				// myEnvironment.TakeScreenShot( theBrowser, theTest,
				// "PDP", "");
			//	Thread.sleep(myEnvironment.getThe_Special_Sleep());
				
				
			
			
			}
		} else {
			// This is code to add multiples of the same items to the cart at
			// the same time.
		//	currentElement = myEnvironment.waitForDynamicElement(
		//			By.id("BVRRDisplayContentSelectBVFrameID"));
		//	Select clickThis = new Select(currentElement);
		//	clickThis.selectByValue("Newest");
			
			
		
			
			
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			currentElement = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdSelectQuantity()));
		    Select clickThis = new Select(currentElement);
			clickThis.selectByValue(theQuantity);
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathAddToBagButton()));
			
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			currentElement.click();
		//	currentElement = myEnvironment.waitForDynamicElement(
		//			By.id(this.getTheByIdAddToBagBtn()));
		//	currentElement.click();
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
		}

		Thread.sleep(myEnvironment.getThe_Special_Sleep());

	}

	public String getThePageName() {
		return thePageName;
	}

	public String getThePreviewPaneName() {
		return thePreviewPaneName;
	}

}
