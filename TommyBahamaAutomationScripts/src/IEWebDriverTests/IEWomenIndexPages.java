package IEWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.Women;
import TommyBahamaRepository.WomenSwim;

@RunWith(JUnit4.class)
public class IEWomenIndexPages {
	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private Header myHeaderObjs;
	private Women myWomensObjs = new Women();
	private WomenSwim myWomenSwimObjs = new WomenSwim();
	private String testName = "WomenIndexPageTest";

	// The object which is how all interaction with web elements is made.
	private int ss = 0;

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		
		List<String> fns = new ArrayList<String>();
		fns.add("WomensIndex");
		fns.add("WomensAccessoriesIndex");
		fns.add("WomensSwimShopIndex");
		fns.add("WomensBeachAccessories");
		
		String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\IE9\\" + testName;		
		myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void WomenIndexPageTest() throws InterruptedException, IOException {
		WebElement ce;
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setTestSubject("Women's Index Page Test Results");

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathWomensTab()));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myWomensObjs.getTheByXPathAccessories()));

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "WomensIndex", "");
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link


		ce.click();
		
		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "WomensAccessoriesIndex", "");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(myWomensObjs.getTheByLinkSwimShop()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "WomensSwimShopIndex", "");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myWomenSwimObjs.getTheByXPathBeachAccessories()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "WomensBeachAccessories", "");
		ss = ss + 1;

	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Different Women's Index Pages</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This test is just to see if the Women's Index Pages are displaying correctly.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+
				
				myEnvironment.addSSToFile(testName, "WomensIndex",
						"This is the Women's Index page.")
				+
				
				myEnvironment.addSSToFile(testName, "WomensAccessoriesIndex",
						"This is the Women's Accessories Index page.")
				+
									
				myEnvironment.addSSToFile(testName, "WomensSwimShopIndex",
						"This is the Women's Swim Shop Index page.")
				+

		    	myEnvironment.addSSToFile(testName, "WomensBeachAccessories",
						"This is the Women's Beach Accessories Index page.")
				+

				myEnvironment.getPageTestOutcome()
				
				+ "</center></body></html>");
		bw.close();
		
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Different Women's Index Pages</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This test is just to see if the Women's Index Pages are displaying correctly.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add("WomensIndex");
		fns.add("WomensAccessoriesIndex");
		fns.add("WomensSwimShopIndex");
		fns.add("WomensBeachAccessories");

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();

	}


}