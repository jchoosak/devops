package IEWebDriverTests;


import org.junit.After;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.testng.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

@RunWith(JUnit4.class)
public class IEUnitedStatesGuestOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
    private Product myProducts = new Product();
	private String theEnvironment = "";
	private String url = "";
	private String theOrderNumber = "";
	private final String testName = "TheGuestUSOrderTest";
	private int ss = 0;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);	
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		
			List<String> fns = new ArrayList<String>();

			fns.add("HomePage");
				fns.add("PLPDinnerware");
				fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
				fns.add(myProductListingObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
				fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
			fns.add("PLPShoes");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct());
			fns.add("PLPGolfBag");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag());
			fns.add("GiftServicesModalGiftMessage");
			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add("AddressPageGuest");
			fns.add("ShoppingBagOversizedShippingErrors");
			fns.add("PaymentPageCompleted");
			fns.add("PreviewPage");
			fns.add("ConfirmationPage");
			fns.add("ContactCenter" + theOrderNumber);
			fns.add("PaymenTech" + theOrderNumber);
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\IE9\\" + testName;
			myEnvironment.removeSS(fns, tempFile);
		 
	}

	@Test
	public void TheGuestUSOrderTest() throws InterruptedException, IOException {

		WebElement ce;

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		myEnvironment.setTestSubject("United States Guest Order Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// Go Back to United States
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "HomePage", "");
		ss = ss + 1;
		// Sign into an Washington State account
		mySIO.signIn(
				mySIO.getTheWAUsername(), mySIO.getThePassword());
		
		ce = myEnvironment.waitForDynamicElement( By.xpath(myHeaderObjs.getTheByXPathSignOut()));
		ce.click();
		
		
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "2",  "DinnerWare");
		ss = ss + 1;
		ss = ss + 1;

		// //adds a t-shirt to the shopping bag
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathMensTShirts(), "$45.00", "1", 
				"T-Shirt");
		ss = ss + 1;
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// add shoes to the bag
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), myProducts.getTheShoeProduct(), "1", 
				"Shoes");
		ss = ss + 1;
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// myProductDetailObjs.selectProduct(driver, myEnvironment,
		// myProductListingObjs, myHeaderObjs,
		// myHeaderObjs.HomeDecorTab, myHomeDecorObjs.Diffusers,
		// this.firstProductionCanadaProduct, "1", testName);
		// Add second restricted product
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
				 "Golf");
		ss = ss + 1;
		ss = ss + 1;

		// continue to shopping bag
		ce = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdPreviewCheckout()));
		// ce = myEnvironment.waitForDynamicElement(
		// By.CssSelector(myHeaderObjs.getTheByCSSLogo()));
		// ce.Click();

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		ce.click();

		// url = driver.Url;
		// ce = myEnvironment.waitForDynamicElement(
		// By.XPath(myShoppingBagObjs.BottomContinueCheckout));
		// ce = myEnvironment.waitForDynamicElement(
		// By.LinkText("(//a[contains(getTheText()(),'Continue Checkout')])[2]"));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		// url = driver.Url;
		// ce = myEnvironment.waitForDynamicElement(
		// By.XPath(myShoppingBagObjs.BottomContinueCheckout));
		// add gift services to the products
		// driver.Navigate().GoToUrl(url);
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheThirdGiftLink(), testName,
				myEnvironment.getIeBrowser());
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ss = ss + 1;
		// myGiftServiceObjs.giftBox(driver, myEnvironment, myShoppingBagObjs,
		// myShoppingBagObjs.getTheFirstGiftLink(), testName,
		// myEnvironment.getIeBrowser());
		//
		//
		// myGiftServiceObjs.giftBox(driver, myEnvironment, myShoppingBagObjs,
		// myShoppingBagObjs.getTheFirstGiftLink(), testName,
		// myEnvironment.getIeBrowser());
		//

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "WithGiftServices");
		ss = ss + 1;

		driver.findElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()))
				.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySIO.getTheContinueAsGuestBtn()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdNewEmail()));
		ce.sendKeys(myEnvironment.getTheEcommEmailOne());
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdConfirmedEmail()));
		ce.sendKeys(myEnvironment.getTheEcommEmailOne());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdPhone1()));
		ce.sendKeys("253");
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdPhone2()));
		ce.sendKeys("205");
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdPhone3()));
		ce.sendKeys("1750");

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingCountry()));
		ce = ce.findElement(By
				.cssSelector(myAddressObjs.getTheUSCountry()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingFName()));
		ce.sendKeys(myEnvironment.getTheUsShopperFName());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingMName()));
		ce.sendKeys(myEnvironment.getTheUsShopperMName());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingLName()));
		ce.sendKeys(myEnvironment.getTheUsShopperLName());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingAddress()));
		ce.sendKeys(myAddressObjs.getTheWAAddress());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingCity()));
		ce.sendKeys(myAddressObjs.getTheWACity());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingState()));
		Select clickThis = new Select(ce);
		// ce.FindElement(By.CssSelector("option[value='WA']"));
		clickThis.selectByValue("WA");

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		ce.sendKeys(myAddressObjs.getTheWAZipCode());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingFName()));
		ce.sendKeys(myEnvironment.getTheUsShopperFName());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingMName()));
		ce.sendKeys(myEnvironment.getTheUsShopperMName());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingLName()));
		ce.sendKeys(myEnvironment.getTheUsShopperLName());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingAddress()));
		ce.sendKeys(myAddressObjs.getTheHawaiiAddress());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingCity()));
		ce.sendKeys(myAddressObjs.getTheHawaiiCity());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingState()));
		clickThis = new Select(ce);
		clickThis.selectByValue("HI");
		// ce.Click();
		// myEnvironment.myWaitForWDElement(driver,
		// By.CssSelector("option[value='HI']"));
		// ce.FindElement(By.CssSelector("option[value='HI']"));
		// ce.Click();

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingZipCode()));
		ce.sendKeys(myAddressObjs.getTheHawaiiZipCode());

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		// ce.Click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = ce.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "AddressPage", "Guest");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		// Thread.Sleep(10000);
		ce.click();

		// continue from address page to verify shipping errors occurr
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "OversizedShippingErrors");
		ss = ss + 1;

		System.out.println(ce.getText());
		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		Assert.assertTrue(ce.getText().contains(
				myShoppingBag.getTheHawaiiShippingText()));
		// String errorColor =
		// driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).GetCssValue("Color");
		String errorColor = ce.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
		// Assert.assertTrue(ce.getTheText().Contains(myShoppingBagObjs.HawaiiShippinggetTheText()));

/*		if (myEnvironment.IsElementPresent(By.xpath(myShoppingBagObjs
				.getTheFifthRowShippingRemovalError()), driver)) {
			ce = myEnvironment.waitForDynamicElement( By
					.xpath(myShoppingBagObjs
							.getTheFifthRowShippingRemovalError()));
			Assert.assertTrue(ce.getText().contains(
					myShoppingBagObjs.getTheShippingRemovalText()));
		} else {
			ce = myEnvironment.waitForDynamicElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr[9]/td[2]/div"));
			Assert.assertTrue(ce.getText().contains(
					myShoppingBagObjs.getTheShippingRemovalText()));

		}
		errorColor = ce.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.equals("rgb(168, 49, 39)"));
*/
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFourthRemoveLink()));
		ce.click();
		Thread.sleep(5000);

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySIO.getTheContinueAsGuestBtn()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingCountry()));
		// ce =
		// ce.findElement(By.cssSelector(myAddressObjs.getTheUSCountry()));
		// ce.click();
		Thread.sleep(10000);
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));

		ce.click();

		// ce = myEnvironment.waitForDynamicElement(
		// By.Id(myAddressObjs.TopCheckoutButton));
		// Thread.sleep(10000);

		/*
		 * This is commented out because currently we are under a month of free
		 * shipping, will uncomment when shipping is returned to normal for US
		 * orders.
		 */
		// Checks to see if the shipping price is correct on the payment page.
		// ShippingPrice = "";
		// ShippingPrice =
		// driver.FindElement(By.XPath(myPaymentObjs.GroundShipping)).getTheText();
		// ShippingPrice = ShippingPrice.Remove(0, 1);
		// Double.TryParse(ShippingPrice, out TheShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPaymentObjs.ActualCanadaShippingPrice);
		// myEnvironment.waitForDocumentReadyState(driver);
		// Checks to see that there is no other shipping options present
		// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()),
		// driver));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheBottomCCBtn()));
		// see that Card Services are working
		myPaymentPage.checkGcBalence();

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		// waits for preview page to load
		// Thread.Sleep(10000);
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheTotalTax()));
		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Preview", "Page");
		ss = ss + 1;

		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalTax())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewObjs.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));
		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText());
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationObjs.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Confirmation", "Page");
		ss = ss + 1;

		theOrderNumber = driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText();
		theOrderNumber = theOrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		// Assert.assertTrue("$16.00" .equals(
		// driver.FindElement(By.XPath(myConfirmationObjs.getTheShippingAmount())).getTheText().Trim());

	

		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax,  theOrderNumber, testName,
				myEnvironment.getIeBrowser());
		
		// Insert a comma for how totals appear in contact center and OG
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		ss = ss + 1;

		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		String newTotal = "";
	
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getIeBrowser());
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		ss = ss + 1;

	}


	@After
	public void quitDriver() throws InterruptedException, MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>United States Guest Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The United States Guest Order Test verifies that a US order can be completed per requirements. This test also checks for different alerts that notify the user that certain functionality is restricted"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is United States.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+
				
				myEnvironment.addSSToFile(testName, "HomePage",
						"This is the Home Page.")
				+

				myEnvironment.addSSToFile(testName, "PLPDinnerware",
						"This is the Dinnerware PLP.")
				+
				
		    	myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+

					myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheMidTShirtPrice(),
						"This is the T-Shirt PLP.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheMidTShirtPrice(),
						"This is the T-Shirt PDP.")
				+

			myEnvironment.addSSToFile(testName, "PLPShoes",
						"This is the Shoes & Sandals PLP..")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct(),
						"This is the Shoes & Sandals PDP.")
				+


				myEnvironment.addSSToFile(testName, "PLPGolfBag",
						"This is the Golf PLP.")
				+
																
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag(),
						"This is the Golf PDP.")
				+

					myEnvironment.addSSToFile(testName, "GiftServicesModalGiftMessage",
						"This is the Gift Message Modal.")
				+
				

				myEnvironment.addSSToFile(testName, "ShoppingBagProductsWithGiftServices",
						"This is the Shopping Bag containing Products with Gift Services.")
				+

				myEnvironment.addSSToFile(testName, "AddressPageGuest",
						"This is the Address Page for a Guest User.")
				+

				myEnvironment.addSSToFile(testName, "ShoppingBagOversizedShippingErrors",
						"This is the Shopping Bag with Oversized and other Shipping Errors Displayed.")
				+

			myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
						"This is the Payment Page after Credit Card information entered.")
				+

			
			myEnvironment.addSSToFile(testName, "PreviewPage",
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ConfirmationPage",
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ContactCenter" + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymenTech" + theOrderNumber,
						"This is the PaymenTech Page.")
			    +
			    
                myEnvironment.getPageTestOutcome()
                
				+ "</center></body></html>");
		
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>United States Guest Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The United States Guest Order Test verifies that a US order can be completed per requirements. This test also checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is United States.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");
		

		List<String> fns = new ArrayList<String>();

		fns.add("HomePage");
			fns.add("PLPDinnerware");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add(myProductListingObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
		fns.add("PLPShoes");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct());
		fns.add("PLPGolfBag");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag());
		fns.add("GiftServicesModalGiftMessage");
		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("AddressPageGuest");
		fns.add("ShoppingBagOversizedShippingErrors");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + theOrderNumber);
		fns.add("PaymenTech" + theOrderNumber);

		ss = ss - 8;
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}