package IEWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.LandingPage;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

@RunWith(JUnit4.class)
public class IEUnitedStatesSplitTenderOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private AccountPage myAccountObjs;
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private LandingPage myLandingObjs = new LandingPage();
    private Product myProducts = new Product();
	private String theEnvironment = "";

	private String theOrderNumber = "";
	private int ss = 0;
	private final String testName = "TheUSSplitTenderOrderTest";
	// url to be checked on various pages
	private String url = "";

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		myAccountObjs = new AccountPage(myEnvironment);
		myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		
		 List<String> fns = new ArrayList<String>();
			fns.add(mySIO.getThePageName() + "Blank");
			fns.add(myAccountObjs.getThePageName());
			fns.add(myLandingObjs.getThePageName());
			fns.add(myProductListingObjs.getThePageName() + myProducts.getTheHighTShirtPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighTShirtPrice());
			fns.add("PLPDinnerWare");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add("PLPShoes");
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheShoeProduct());
			fns.add(myShoppingBag.getThePageName());
			fns.add("HomePageUnitedStates");
			fns.add("GiftServicesModalGiftWrap");
			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add(myAddressObjs.getThePageName() + "Prefilled");
			fns.add(myPaymentPage.getThePageName());
			fns.add("PaymentPageGiftCardBalenceModal");
			fns.add("PaymentPageAppliedGiftCard");
			fns.add("PaymentPageCompleted");
			fns.add(myPreviewObjs.getThePageName());
			fns.add(myConfirmationObjs.getThePageName());
			fns.add("ContactCenter" + theOrderNumber + "");
			fns.add("PaymenTech" + theOrderNumber + "");
			fns.add("PaymenTech" + myPaymentPage.getTheGiftCardNumber());
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\IE9\\" + testName;
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheUSSplitTenderOrderTest() throws InterruptedException,
			IOException {
		WebElement currentElement;

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheMerchEnvironment());
		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());

		myEnvironment
				.setTestSubject("United States Split Tender Order Test Results");
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Sign into an Washington State account
		mySIO.signIn(
				mySIO.getTheUSSplitTenderUserName(), mySIO.getThePassword());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, myAccountObjs.getThePageName(), "");
		ss = ss + 1;
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, myLandingObjs.getThePageName(), "UnitedStates");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "2", "DinnerWare");
		ss = ss + 1;
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathMensTShirts(), "$78.00", "1", 
				"TShirt");
		ss = ss + 1;
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		// add shoes to the bag
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), myProducts.getTheShoeProduct(), "1", 
				"Shoes");
		ss = ss + 1;
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, myShoppingBag.getThePageName(), "");
		ss = ss + 1;

		// add gift services to the products
		// myGiftServiceObjs.giftBox(driver, currentElement, myEnvironment,
		// myShoppingBagObjs, myShoppingBagObjs.ThirdGiftLink, testName);
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getIeBrowser());
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, myShoppingBag.getThePageName(), "WithGiftServices");
		ss = ss + 1;

		// check to see if the url is secure on the address page
		driver.findElement(By.xpath(myShoppingBag.getTheContinueCheckout()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		driver.findElement(By.id(myAddressObjs.getTheByIdSameAddress()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = driver.findElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode())).getAttribute(
				"value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "AddressPage", "");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		driver.findElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()))
				.click();
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PaymentPage", "");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheNextDayShipping()));
		currentElement.click();
		/*
		 * This is commented out because currently we are under a month of free
		 * shipping, will uncomment when shipping is returned to normal for US
		 * orders.
		 */
		// Checks to see if the shipping price is correct on the payment page.
		// ShippingPrice = "";
		// ShippingPrice =
		// driver.FindElement(By.XPath(myPaymentObjs.GroundShipping)).getTheText();
		// ShippingPrice = ShippingPrice.Remove(0, 1);
		// Double.TryParse(ShippingPrice, out TheShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPaymentObjs.ActualCanadaShippingPrice);

		// Checks to see that there is no other shipping options present
		// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()),
		// driver));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// see that Card Services are working
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalenceLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));
		// String theBalence = currentElement.getText();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PaymentPage", "GiftCardBalence");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myPaymentPage.getTheByLinkX()));
		currentElement.click();

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// use the giftcard for partial payment
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardPinInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardPin());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheGiftCardPartialPayment()));

		System.out.println(currentElement.getText().trim());
		String thePartialPayment = currentElement.getText().trim();
		Assert.assertTrue(thePartialPayment.equals("-$25.00"));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PaymentPage", "AppliedGiftCard");
		ss = ss + 1;

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// waits for preview page to load
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheTotalTax()));
		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Preview", "Page");
		ss = ss + 1;

		// check to see if the tax is what it should be for a Canada order
		Thread.sleep(5000);
		String theTax = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalTax())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheSplitTenderSubTotal())).getText();
		String theShipping = driver
				.findElement(By.xpath(myPreviewObjs.getTheShippingAmount()))
				.getText().trim();
		thePartialPayment = driver
				.findElement(By.xpath(myPreviewObjs.getTheGCPartialPayment()))
				.getText().trim();
		String theUSGC = driver.findElement(
				By.xpath(myPreviewObjs.getTheUSGC())).getText();
		System.out.println(theShipping);
		System.out.println(thePartialPayment);

		// Assert.assertTrue(thePartialPayment.Contains("GIFT CARD:"));
		Assert.assertTrue(thePartialPayment.contains("$25.00"));
		System.out.println(thePartialPayment);
		// Assert.assertTrue(theShipping == "$16.00");
		System.out.println(theShipping);
		System.out.println(theUSGC);
		System.out.println(myPaymentPage.getTheGiftCardNumber().trim());
		Assert.assertTrue(theUSGC.equals(myPaymentPage.getTheGiftCardNumber()
				.trim()));

		// Submit the order
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));
		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Confirmation", "Page");
		ss = ss + 1;

		// Store the Order number for CC and for Paytech

		theOrderNumber = currentElement.getText();
		theOrderNumber = theOrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		// Assert.assertTrue("$16.00" ==
		// driver.FindElement(By.XPath(myConfirmationObjs.getTheShippingAmount())).getTheText().Trim());

		// Insert a comma for how totals appear in contact center and OG
		// String theContactTotal = theTotal.Insert(2, ",");
		System.out.println(theTotal);
		// runs the script to look up the order in Contact Center
		// Insert a comma for how totals appear in contact center and OG
		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, theOrderNumber, testName,
				myEnvironment.getIeBrowser());
		
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		
		ss = ss + 1;
		
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getIeBrowser());
		
		
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		System.out.println(theTotal);
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		Double theGCTotal = 0.0;
		theGCTotal = myPaymentPage.pullPrice(myPayTechObjs
				.getTheGCAddedAmount());
		theDTotal = theDTotal - theGCTotal;
		String newTotal = "";
		
		
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ss = ss + 1;
		// Adds money back to the gift card so the script is ready to run again
		myPayTechObjs.addMoneyToCard(
				myPaymentPage.getTheGiftCardNumber(),
				myPaymentPage.getTheGiftCardPin(),
				myPaymentPage.getTheGiftCardAddAmount());
		ss = ss + 1;
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Canada Split Tender Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The Canada Split Tender Order Checks for different alerts that notify the user that certain functionality is restricted"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is Canada. This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, "AccountPage",
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, "HomePage",
						"This is the Home Page.")
				+

					myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() +  myProducts.getTheHighTShirtPrice(),
						"This is the T-Shirt PLP.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheHighTShirtPrice(),
						"This is the T-Shirt PDP.")
				+

				myEnvironment.addSSToFile(testName, "PLPDinnerware",
						"This is the Dinnerware PLP.")
				+
				
		    	myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+

		myEnvironment.addSSToFile(testName, "PLPShoes",
						"This is the Shoes & Sandals PLP.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() +myProducts.getTheShoeProduct(),
						"This is the Sandals PDP.")
				+
				
				myEnvironment.addSSToFile(testName, myShoppingBag.getThePageName(),
						"This is the Shopping Bag with products.")
				+

				myEnvironment.addSSToFile(testName,
						"GiftServicesModalGiftWrap",
						"This is the Gift Wrap Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "ShoppingBagProductsWithGiftServices",
						"This is the Shopping Bag containing Products with Gift Services.")
				+

					myEnvironment.addSSToFile(testName, "AddressPagePreFilled",
						"This is the Address Page with pre populated fields.")
				+

						myEnvironment.addSSToFile(testName, myPaymentPage.getThePageName(),
						"This is the Payment Page.")
				+
				
				
					myEnvironment.addSSToFile(testName, "PaymentPageGiftCardBalenceModal",
						"This is the Gift Card Balence Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, myPreviewObjs.getThePageName(),
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, myConfirmationObjs.getThePageName(),
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ContactCenter" + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymenTech" + theOrderNumber,
						"This is the PaymenTech Page.")
				+

				
				myEnvironment.addSSToFile(testName, myPayTechObjs.getThePageName() + myPaymentPage.getTheGiftCardNumber(),
						"This is the approved add money to gift card PaymenTech Page.")
				
				+
			    
					myEnvironment.getPageTestOutcome()
					
				+ "</center></body></html>");
		
		bw.close();


		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Split Tender Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Canada Split Tender Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada. This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add(mySIO.getThePageName() + "Blank");
		fns.add(myAccountObjs.getThePageName());
		fns.add(myLandingObjs.getThePageName());
		fns.add(myProductListingObjs.getThePageName() + myProducts.getTheHighTShirtPrice());
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighTShirtPrice());
		fns.add("PLPDinnerWare");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
		fns.add("PLPShoes");
		fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheShoeProduct());
		fns.add(myShoppingBag.getThePageName());
		fns.add("HomePageUnitedStates");
		fns.add("GiftServicesModalGiftWrap");
		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add(myAddressObjs.getThePageName() + "Prefilled");
		fns.add(myPaymentPage.getThePageName());
		fns.add("PaymentPageGiftCardBalenceModal");
		fns.add("PaymentPageAppliedGiftCard");
		fns.add("PaymentPageCompleted");
		fns.add(myPreviewObjs.getThePageName());
		fns.add(myConfirmationObjs.getThePageName());
		fns.add("ContactCenter" + theOrderNumber + "");
		fns.add("PaymenTech" + theOrderNumber + "");
		fns.add(myPayTechObjs.getThePageName() + myPaymentPage.getTheGiftCardNumber());

		// could not find where I am missing my calls for two screenshots. Have bigger fish to fry!
		ss = ss + 2;
		
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);
		
		System.out.println("This is how many screenshots that are allowed to be displayed:                " +  ss + "");

		driver.quit();
	}

}