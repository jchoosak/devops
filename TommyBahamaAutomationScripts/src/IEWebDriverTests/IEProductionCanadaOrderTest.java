package IEWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class IEProductionCanadaOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private AccountPage myAccountObjs;
	private FiftyOne myFiftyOneObjs = new FiftyOne();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	private Product myProducts = new Product();

    private final String testName = "TheProductionCanadaOrderTest";
    private int ss = 0;

	// url to be checked on various pages
	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
	
		Assert.assertTrue(isSupportedPlatform());
		File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myAccountObjs = new AccountPage(myEnvironment);
	
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		 
			List<String> fns = new ArrayList<String>();
			 fns.add("HomePage");
			 fns.add("ContextChooser");
				fns.add("SignInPageBlank");
				fns.add("MyAccountPage");
			 fns.add("PDPTraditionalGiftCard");
			 fns.add("PLPShoes$98.00");
			 fns.add("PDP$98.00");
				fns.add(myProductListingObjs.getThePageName() + myProducts.getTheHighTShirtPrice());
				fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighTShirtPrice());
				fns.add("PLPDinnerWare");
				fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			 fns.add("PLPGolfBag");
			 fns.add("PDPTBG-099");
			fns.add("GiftServicesModalGiftBox");
			fns.add("GiftServicesModalGiftWrap");
			fns.add("GiftServicesModalGiftMessage");
			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add("ShoppingBagGiftServicesAndShippingErrors");
			
			fns.add("AddressPage");
		
			fns.add(myProductListingObjs.getThePageName() + "Jacket");
			fns.add("PDP" + myProducts.getTheJacket());
		
			fns.add("ContextChooser");
		
			fns.add("PaymentPage");
			fns.add("ShoppingBagEmpty");
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\IE9\\" + testName;
			
			myEnvironment.removeSS(fns, tempFile);
		
	}

	@Test
	public void TheProductionCanadaOrderTest() throws InterruptedException {

		WebElement currentElement;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setTestSubject("Production Canada Test Results");

		// Select Canada from Fifty One Layer
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheCanadaFlag(), testName);
		myEnvironment.waitForPageLoaded();

		// check to see if the proper ad is displayed in header
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathCanadaFlag()));
		String url = driver.getCurrentUrl();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "HomePage", "");
		// ss = ss + 1;
		// Signe into an canada account
		mySIO.signIn(
				mySIO.getTheCanadaFourUserName(), mySIO.getThePassword());
		
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "MyAccountPage", "");

	
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);
		// Add the Gift Card
		myTraditionalCardObjs
				.addTraditionalCard( myEnvironment, driver, myHeaderObjs,
						myMenObjs, testName, "PDPTraditionalGiftCard", "");
		// ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// add shoes to the bag
		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), "$98.00", "1", 
				"Shoe");
		
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// adds a t-shirt to the shopping bag
		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathMensTShirts(), "$72.00", "1",
				"T-Shirt");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "1", "Dinnerware");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
				 "GolfBag");
		// ss = ss + 1;
		// ss = ss + 1;

		// ss = ss + 1;
		// ss = ss + 1;

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdPreviewCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));

		// add gift services to the products
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getIeBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getIeBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getIeBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "ProductsWithGiftServices");
		ss = ss + 1;

		// continue to address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();

		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "GiftServicesAndShippingErrors");
		ss = ss + 1;

		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRestrictionText()));
		String errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("#a83127"));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheCustomsGiftServiceErrorText()));
		Assert.assertTrue(driver
				.findElement(
						By.cssSelector(myShoppingBag
								.getTheByCSSCanadaError()))
				.getText()
				.contains(myShoppingBag.getTheCustomsGiftServiceErrorText()));
		Assert.assertTrue(driver
				.findElement(
						By.cssSelector(myShoppingBag
								.getTheByCSSCanadaError()))
				.getText()
				.contains(
						myShoppingBag
								.getTheTraditionalGiftCardCanadaErrorText()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstRowGiftServicesError()));
		System.out.println("This is the GiftServicesError:       "
				+ currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("#a83127"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheSecondRowGiftServivesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("#a83127"));
		/*currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBagObjs.getTheLastRowGiftServicesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBagObjs.getTheGiftServicesErrorText()));
		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myShoppingBagObjs.getTheThirdRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBagObjs.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("#a83127"));
		currentElement = myEnvironment
				.waitForDynamicElement( By.xpath(myShoppingBagObjs
						.getTheFourthRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBagObjs.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("#a83127"));

		// checks for the yellow outline
		myShoppingBagObjs.checkProductsForYellowOutline(driver, myEnvironment,
				7);*/

		// see that the shipping restriction message is present in the shopping
		// bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstShippingRestriction()));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
//		currentElement = myEnvironment.waitForDynamicElement(
//				By.xpath(myShoppingBagObjs.getTheSecondShippingRestriction()));
//		System.out.println(currentElement.findElement(By.tagName("a"))
//				.getText());
//		Assert.assertTrue(currentElement.getText().contains(
//				"Shipping restrictions apply"));
//		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
//				.contains("DETAILS"));
//		currentElement = myEnvironment.waitForDynamicElement(
//				By.xpath(myShoppingBagObjs.getTheThirdShippingRestriction()));
//		System.out.println(currentElement.findElement(By.tagName("a"))
//				.getText());
//		Assert.assertTrue(currentElement.getText().contains(
//				"Shipping restrictions apply"));
//		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
//				.contains("DETAILS"));

		// remove all the products that cannot be shipped to Canada
		myShoppingBag.removeProducts(2);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		// removes the gift services from remaining products
		Thread.sleep(3000);
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		Thread.sleep(3000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		currentElement.click();
		Thread.sleep(3000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);

		myEnvironment.waitForPageLoaded();
		// click the first image to add more qty
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myProductDetailObjs.getTheMainImage()));
		myProductDetailObjs.addQuantityToBag("8");

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "Normal");
		// ss = ss + 1;

	/*	currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheByXPathFlipSideMessage()));
		String flipSideText = currentElement.getText();
		System.out.println("This is the text it sees for the FlipSide:       "
				+ flipSideText);
		Assert.assertTrue(flipSideText.contains("flip side award card"));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "FlipSideAwardCard");*/

		// ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "AddressPage", "");

		ss = ss + 1;

		currentElement.click();
		myEnvironment.waitForPageLoaded();

		/*currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheByCSSCanadaFlipSideError()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PaymentPage", "FlipSideErrorText");
		ss = ss + 1;*/

//		WebElement element = currentElement;
//		System.out.println(element.getText());
//		System.out.println(myPaymentObjs.getTheCanadaFlipSideErrorText());
//		Assert.assertTrue(element.getText().trim()
//				.contains(myPaymentObjs.getTheCanadaFlipSideErrorText().trim()));
//		errorColor = currentElement.getCssValue("Color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("#a83127"));

		// //// Verify that the flip side award card was removed
//		String bodyText = driver.findElement(By.id("content")).getText();
//		Assert.assertFalse(bodyText.contains("flip side award card"));

		// continue through checkout to see that the flip side error message
		// appears on payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheByCSSReturnToShoppingBag()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		// myEnvironment.TakeScreenShot(
		// myEnvironment.getIeBrowser(), testName, "AddressPage", "");
		currentElement.click();
		// ss = ss + 1;
		myEnvironment.waitForPageLoaded();
		// Go to home page and sign out
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheByCSSReturnToShoppingBag()));
		currentElement.click();

		myEnvironment.waitForPageLoaded();
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		// add product to see if flip side card is added.
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathJackets(), myProducts.getTheJacket(), "8",
		   	"Jacket");

		ss = ss + 1;
		ss = ss + 1;

		// continue to shopping bag
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.id("content"));

	/*	Assert.assertFalse(myEnvironment.IsElementPresent(
				By.xpath("//img[@alt='PGC880001']")));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "CanadaFlipSideFalse");
		ss = ss + 1;*/
		// Select united states from Fifty One Layer
		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
		ss = ss + 1;
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// bodyText = driver.findElement(By.id("content")).getText();
		// driver.
		// System.out.println(bodyText);
/*		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.xpath("//img[@alt='PGC880001']")));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "UnitedStatesFlipSideTrue");
		ss = ss + 1;*/

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));

		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);

		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForTitle(myPaymentPage.getThePageTitle());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PaymentPage", "");
		ss = ss + 1;
		// Checks to see if the shipping price is correct on the payment page.
		String ShippingPrice = "";
		ShippingPrice = driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText();
		ShippingPrice = ShippingPrice.substring(1);
		//Double TheShippingPrice = Double.parseDouble(ShippingPrice);

		// This needs to be commented out when free shipping is running
		// Assert.assertEquals(TheShippingPrice,
		// myPaymentObjs.getTheActualCanadaShippingPrice());

		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		driver.findElement(
				By.cssSelector(myPaymentPage.getTheByCSSReturnToShoppingBag()))
				.click();

		// remove all the products
		myShoppingBag.removeAllProducts(4);
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "Empty");
		ss = ss + 1;

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		myEnvironment.setTestPassed(true);
		
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));

		
		bw.write("<html><body><center><h2>Production Canada Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The Production Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+
				
				myEnvironment.addSSToFile(testName, "ContextChooser",
						"This is the Context Chooser modal.")
				+
				
				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+

			myEnvironment.addSSToFile(testName, "MyAccountPage",
						"This is the Account Page.")
				+

				myEnvironment.addSSToFile(testName, "HomePage",
						"This is the Home Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PDPTraditionalGiftCard",
								"This is the GiftCard PDP.")
						+					
											myEnvironment.addSSToFile(testName, "PLPShoe$98.00",
								"This is the Shoes & Sandals PLP.")
						+

						myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + "Shoe$98.00",
								"This is the Sandals PDP.")
						+
						
											myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() +  myProducts.getTheHighTShirtPrice(),
								"This is the T-Shirt PLP.")
						+

						myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheHighTShirtPrice(),
								"This is the T-Shirt PDP.")
						+
						
					myEnvironment.addSSToFile(testName, "PLPDinnerware",
								"This is the Dinnerware PLP.")
						+
						
					myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
								"This is the Dinnerware PDP.")
						+
													myEnvironment.addSSToFile(testName, "PLPGolfBag",
						"This is the Golf PLP.")
				+		
				
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag(),
						"This is the Golf PDP.")
				+
						
				myEnvironment.addSSToFile(testName, "GiftServicesModalGiftBox",
						"This is the Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalGiftWrap",
						"This is the Shopping Bag with the Gift Wrap Modal.")
				+
				
					myEnvironment.addSSToFile(testName, "GiftServicesModalGiftMessage",
						"This is the Gift Message Modal.")
				+
		
				myEnvironment.addSSToFile(testName, "ShoppingBagProductsWithGiftServices",
						"This is the Shopping Bag containing Products with Gift Services.")
				+

				myEnvironment.addSSToFile(testName, "ShoppingBagGiftServicesAndShippingErrors",
						"This is the Shopping Bag containing Products with different types of error messages.")
				+

			
			myEnvironment.addSSToFile(testName, "AddressPage",
						"This is the Address Page.")
				+
				
			/*	myEnvironment.addSSToFile(testName, "PaymentPageFlipSideErrorText",
						"This is the Payment Page with the Flip Side Error Text.")
				+*/
				
				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + "Jacket",
						"This is the Jacket PLP.")
				+
				
				myEnvironment.addSSToFile(testName, "PDP" + myProducts.getTheJacket(),
						"This is the Jacket PDP.")
				+

			/*	myEnvironment.addSSToFile(testName, "ShoppingBagCanadaFlipSideFalse",
						"This is the Address Page that should NOT contain any Flip Side Award cards.")
				+*/
									
				myEnvironment.addSSToFile(testName, "ContextChooser",
						"This is the Context Chooser that is used to change countries.")
				+

		/*		myEnvironment.addSSToFile(testName, "ShoppingBagUnitedStatesFlipSideTrue",
						"This is the Shopping Bag with multiple Flip Side Award Cards.")
				+*/
			
				myEnvironment.addSSToFile(testName, "PaymentPage",
						"This is the Payment Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ShoppingBagEmpty",
						"This is the Empty Shopping Bag Page.")
				+

                myEnvironment.getPageTestOutcome()
                
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Production Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Production Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		 fns.add("HomePage");
		 fns.add("PDPTraditionalGiftCard");
		 fns.add("PLPGolfBag");
		 fns.add("PDPTBG-099");
		 fns.add("PLPShoes$98.00");
		 fns.add("PDP$98.00");
		fns.add("GiftServicesModalGiftBox");
		fns.add("GiftServicesModalGiftWrap");
		fns.add("GiftServicesModalGiftMessage");
		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("ShoppingBagGiftServicesAndShippingErrors");
		 fns.add("ShoppingBagFlipSideAwardCard");
		fns.add("AddressPage");
		fns.add("PaymentPageFlipSideErrorText");
		fns.add(myProductListingObjs.getThePageName() + "Jacket");
		fns.add("PDP" + myProducts.getTheJacket());
		//fns.add("ShoppingBagCanadaFlipSideFalse");
		fns.add("ContextChooser");
		fns.add("ShoppingBagUnitedStatesFlipSideTrue");
		fns.add("PaymentPage");
		fns.add("ShoppingBagEmpty");

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		System.out.println(myEnvironment.getTestSubject());
		driver.quit();
	}
}
