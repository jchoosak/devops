package IEWebDriverTests;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.Header;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SearchResultPage;
import TommyBahamaRepository.SeleniumEnvironment;


@RunWith(JUnit4.class)
public class IESearchSwatchTest {
	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private SearchResultPage mySearchResultObjs;
	private PaymentPage myPaymentPage;
	private String testName = "SearchSwatchTests";
    private Product myProducts = new Product();
	// The object which is how all interaction with web elements is made.
	private int ss = 0;

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySearchResultObjs = new SearchResultPage(driver, myEnvironment, myPaymentPage, myProductListingObjs);
		
		 List<String> fns = new ArrayList<String>();
			fns.add("SearchResultsPage");
			fns.add("SearchResultsPageShowAll");
			fns.add("SearchResultsPageByRatings");
			fns.add("SearchResultsPageByPriceLowToHigh");
			fns.add("SearchResultsPageByPriceHighToLow");
		
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheColor());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDefaultSockPrice());
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheSunglasses());
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\IE9\\" + testName;
			
			myEnvironment.removeSS(fns, tempFile);
	}
	
	@Test
	public void SearchSwatchTests() throws InterruptedException, IOException {
		WebElement ce;
		// String mouseOverScript =
		// "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setTestSubject("Search & Swatch Test Results");

		// ss = ss + 1;

		// test search input and the search page functionality
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathSearchInput()));
		ce.sendKeys("red swimsuit");
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheSearchBtn()));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySearchResultObjs.getTheSearchResultTitle()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().contains("Search Results: red swimsuit"));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchResultsPage", "");

		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myProductListingObjs.getTheFirstImage()));
		ce = myEnvironment.waitForDynamicElement(
				By.linkText(mySearchResultObjs.getTheShowAllLink()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.waitForPageLoaded();
		ce = myEnvironment.fluentWait(
				By.xpath(myProductListingObjs.getTheFirstImage()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchResultsPage", "ShowAll");

		ss = ss + 1;

		mySearchResultObjs.testRatings();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchResultsPage", "ByRatings");

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathSearchInput()));
		ce.sendKeys("black bikini");
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheSearchBtn()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySearchResultObjs.getTheSearchResultTitle()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().equals("Search Results: black bikini"));

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myProductListingObjs.getTheFirstImage()));

		mySearchResultObjs.testPriceHighToLow();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchResultsPage", "ByPriceHighToLow");

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathSearchInput()));
		ce.sendKeys("black bikini");
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheSearchBtn()));
		ce.click();
		Thread.sleep(5000);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(mySearchResultObjs.getTheSearchResultTitle()));
		System.out.println(ce.getText());
		Assert.assertTrue(ce.getText().equals("Search Results: black bikini"));

		mySearchResultObjs.testLowToHigh();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchResultsPage", "ByPriceLowToHigh");

		ss = ss + 1;

		// Go to home page
		ce = myEnvironment.waitForDynamicElement(
						By.xpath(myHeaderObjs.getTheByXPathLogo()));
		ce.click();
		
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		ce.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myMenObjs.getTheByXPathJeans()));
		ce.click();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PLP", "");
		// ss = ss + 1;
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// test the swatches on the PLP page, when clicking on a swatch, the
		// cooresponding thumbnail will change to the color
		myProductListingObjs.testPLPSwatches(
				myProductDetailObjs);

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		// this is a test to see that the selected swatch color on the PLP
		// displays
		// when the PDP loads
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myMenObjs.getTheByXPathMensTShirts()));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myProductListingObjs.chooseSwatch("3426",
				myProductDetailObjs);
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PDP", "3426");
		ss = ss + 1;

		myProductDetailObjs.selectSubProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathAccessories(),
				myMenObjs.getTheByLinkSocks(), "$20.00", "4", 
				"Socks");

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PDP", "$20");
		ss = ss + 1;

		myProductDetailObjs.selectSubProductByProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathAccessories(),
				myMenObjs.getTheByLinkSunglasses(), "TB6016", "4");

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PDP", "TB6015");
		ss = ss + 1;

	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));

		
		bw.write("<html><body><center><h2>Search & Swatch Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This is the Search & Swatch test.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+
									
				myEnvironment.addSSToFile(testName, "SearchResultsPage",
						"This is the Search Results Page.")
				+
				
				myEnvironment.addSSToFile(testName, "SearchResultsPageShowAll",
						"This is a Show All Search Result Page.")
				+
				
				myEnvironment.addSSToFile(testName, "SearchResultsPageByRatings",
						"This is a Show All Search Result Page.")
				+
				
				myEnvironment.addSSToFile(testName, "SearchResultsPageByPriceLowToHigh",
						"This is the Search Results sorted by price low to high.")
				+
				
				myEnvironment.addSSToFile(testName, "SearchResultsPageByPriceHighToLow",
						"This is the Search Results sorted by price low to high.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheColor(),
						"This is the PDP of a product with the color: 3426, which is the color, Melonball. This product was chosen by searching through swatches until the correct color was found.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDefaultSockPrice(),
						"This is a PDP that was selected by the displayed price of " + myProducts.getTheDefaultSockPrice())
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheSunglasses(),
						"This is a PDP that was selected by its product ID of " + myProducts.getTheSunglasses() + ".")
				+

			  myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Search & Swatch Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This is the Search & Swatch test.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add("SearchResultsPage");
		fns.add("SearchResultsPageShowAll");
		fns.add("SearchResultsPageByPriceLowToHigh");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheColor());
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDefaultSockPrice());
		fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheSunglasses());
	

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();

	}
}
