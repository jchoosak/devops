package IEWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.testng.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

@RunWith(JUnit4.class)
public class IEUnitedStatesOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private AccountPage myAccountObjs;
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	// private SessionStorage mySessionStorage;
	private HomeDecor myHomeDecorObjs = new HomeDecor();
    private Product myProducts = new Product();
	private String theEnvironment = "";
	private String url = "";
	private int ss = 0;
	private final String testName = "TheUSOrderTest";
	private String theOrderNumber = "";

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {

		Assert.assertTrue(isSupportedPlatform());
		File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		myAccountObjs = new AccountPage(myEnvironment);
		myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("MyAccountPage");
			fns.add("HomePage");
			fns.add(myProductListingObjs.getThePageName() +  myProducts.getTheJeansPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheJeansPrice());
			fns.add("PLPDinnerware");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add("PLPShoes");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct());
			fns.add("ShoppingBagProducts");
			fns.add("GiftServicesModalGiftWrap");
			fns.add("GiftServicesModalGiftMessage");
			fns.add("ShoppingBagWithGiftServices");
			fns.add("AddressPreFilled");
			fns.add("PaymentPage");
			fns.add("PaymentPageGiftCardBalenceModal");
			fns.add("PaymentPageCompleted");
			fns.add("PreviewPage");
			fns.add("ConfirmationPage");
			fns.add("ContactCenter" + theOrderNumber + "");
			fns.add("PaymenTech" + theOrderNumber + "");
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\IE9\\" + testName;
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheUSOrderTest() throws InterruptedException, IOException {

		WebElement currentElement;

		// Navigate to the testing environment
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.manage().window().setPosition(new Point(0, 0));
		driver.manage().window().setSize(new Dimension(1050, 850));
		driver.get(myEnvironment.getTheTestingEnvironment());

		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
		myEnvironment.setTestSubject("United States Order Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());

		// Sign into an Washington State account
		mySIO.signIn(
				mySIO.getTheWAUsername(), mySIO.getThePassword());

		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "MyAccountPage", "");
		ss = ss + 1;

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();

		myEnvironment.waitForDocumentReadyState();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "HomePage", "");
		ss = ss + 1;

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathJeans(), "$88.00", "2",
				"Jeans");
		ss = ss + 1;
		ss = ss + 1;

		myEnvironment.waitForDocumentReadyState();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "2", "DinnerWare");
		ss = ss + 1;
		ss = ss + 1;
		// myEnvironment.waitForDocumentReadyState(driver);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// add shoes to the bag
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), myProducts.getTheShoeProduct(), "1",
				"Shoes");
		ss = ss + 1;
		ss = ss + 1;
		// myEnvironment.waitForDocumentReadyState(driver);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForDocumentReadyState();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "Products");
		ss = ss + 1;

		// add gift services to the products
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getIeBrowser());
		myEnvironment.waitForDocumentReadyState();
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getIeBrowser());
		ss = ss + 1;

		myEnvironment.waitForDocumentReadyState();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());

		// Test for all of the columns are present in the shopping bag
	    myShoppingBag.checkShoppingBagColumns(driver);
	    
	  
	    
	    
	    

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "ShoppingBag", "WithGiftServices");
		ss = ss + 1;
		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();

		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Address", "PreFilled");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();

		myEnvironment.waitForDocumentReadyState();
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		/*
		 * This is commented out because currently we are under a month of free
		 * shipping, will uncomment when shipping is returned to normal for US
		 * orders.
		 */
		// Checks to see if the shipping price is correct on the payment page.
		// ShippingPrice = "";
		// ShippingPrice =
		// driver.FindElement(By.XPath(myPaymentObjs.GroundShipping)).getTheText();
		// ShippingPrice = ShippingPrice.Remove(0, 1);
		// Double.TryParse(ShippingPrice, out TheShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPaymentObjs.ActualCanadaShippingPrice);

		// Checks to see that there is no other shipping options present
		// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()),
		// driver));

		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Payment", "Page");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheThreeDayShipping()));

		currentElement.click();
		// see that Card Services are working
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalenceLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));
		// String theBalence = currentElement.getText();
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "PaymentPage", "GiftCardBalenceModal");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myPaymentPage.getTheByLinkX()));
		currentElement.click();

		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();
		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;
		myEnvironment.waitForDocumentReadyState();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// waits for preview page to load
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.xpath(myPreviewObjs.getTheTotalTax()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(myPreviewObjs.getThegetTheTotalTax()()));
		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Preview", "Page");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheTotalTax()));
		// check to see if the tax is what it should be for a Canada order
		String theTax = currentElement.getText();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheTotalAmount()));
		String theTotal = currentElement.getText();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheShippingAmount()));
		// String theShipping = currentElement.getText().trim();

		// Submit the order
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()));
		currentElement.click();

		myEnvironment.waitForDocumentReadyState();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));
		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "Confirmation", "Page");
		ss = ss + 1;

		// Store the Order number for CC and for Paytech

		theOrderNumber = currentElement.getText();
		theOrderNumber = theOrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation
		// page
		// Assert.assertTrue("$16.00" ==
		// driver.FindElement(By.XPath(myConfirmationObjs.getTheShippingAmount())).getTheText().Trim());

		// Insert a comma for how totals appear in contact center and OG
		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, theOrderNumber, testName,
				myEnvironment.getIeBrowser());
		
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		ss = ss + 1;

		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
	String newTotal = "";
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getIeBrowser());
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		ss = ss + 1;
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>United States Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The United States Order checks that products can be selected, added to the Shopping Bag and processed through the entire order process."
				+ "</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, "MyAccountPage",
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, "HomePage",
						"This is the Home Page.")
				+
									
				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheJeansPrice(),
						"This is the Jeans PLP.")
				+
				
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheJeansPrice(),
						"This is the Jeans PDP.")
				+						

				myEnvironment.addSSToFile(testName, "PLPDinnerware",
						"This is the Dinnerware PLP.")
				+
				
		    	myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+
				
				myEnvironment.addSSToFile(testName, "PLPShoes",
						"This is the Shoes & Sandals PLP.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() +myProducts.getTheShoeProduct(),
						"This is the Sandals PDP.")
				+

					myEnvironment.addSSToFile(testName, "ShoppingBagProducts",
						"This is the Shopping Bag with products.")
				+
				
				myEnvironment.addSSToFile(testName,
						"GiftServicesModalGiftWrap",
						"This is the Gift Wrap Modal.")
				+

				myEnvironment.addSSToFile(testName,
						"GiftServicesModalGiftMessage",
						"This is the Gift Message Modal.")
				+

				myEnvironment.addSSToFile(testName, "ShoppingBagWithGiftServices",
						"This is the Shopping Bag containing Products with Gift Services.")
				+
				
				myEnvironment.addSSToFile(testName, "AddressPreFilled",
						"This is the Address Page with pre populated fields.")
				+

					myEnvironment.addSSToFile(testName, "PaymentPage",
						"This is the Payment Page.")
				+
				
				
					myEnvironment.addSSToFile(testName, "PaymentPageGiftCardBalenceModal",
						"This is the Gift Card Balence Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, "PreviewPage",
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ConfirmationPage",
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ContactCenter" + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymenTech" + theOrderNumber,
						"This is the PaymenTech Page.")
				+
               
				myEnvironment.getPageTestOutcome()
				
				+ "</center></body></html>");
		
		bw.close();


		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>United States Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The United States Order checks that products can be selected, added to the Shopping Bag and processed through the entire order process."
						+ "</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add("SignInPageBlank");
		fns.add("MyAccountPage");
		fns.add("HomePage");
		fns.add(myProductListingObjs.getThePageName() +  myProducts.getTheJeansPrice());
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheJeansPrice());
		fns.add("PLPDinnerware");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
		fns.add("PLPShoes");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct());
		fns.add("ShoppingBagProducts");
		fns.add("GiftServicesModalGiftWrap");
		fns.add("GiftServicesModalGiftMessage");
		fns.add("ShoppingBagWithGiftServices");
		fns.add("AddressPreFilled");
		fns.add("PaymentPage");
		fns.add("PaymentPageGiftCardBalenceModal");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + theOrderNumber + "");
		fns.add("PaymenTech" + theOrderNumber + "");

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}