package FFCondensedWebdriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class FFCanadaRestrictionMessageTest {

	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private AccountPage myAccountObjs;
	private FiftyOne myFiftyOneObjs = new FiftyOne();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	private Product myProducts = new Product();
	private String testName = "TheCanadaCheckRestrictionTest";

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private WebDriver driver;
	private int ss = 0;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myAccountObjs = new AccountPage(myEnvironment);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
			List<String> fns = new ArrayList<String>();
			fns.add("ContextChooser");
			fns.add("SignInPageBlank");
			fns.add("MyAccountPage");
		    fns.add("HomePage");
			fns.add("PLPDiffuserShippingRestriction");
			fns.add("PDPDiffuserShippingRestriction");
			fns.add("PLPGolfBagShippingRestriction");
			fns.add("PDPGolfBagShippingRestriction");
			fns.add("PDPGiftCardShippingRestriction");
			fns.add("PLPGiftCardShippingRestriction");
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\FF\\" + testName;
			//Delete if tempFile exists
		
			
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheCanadaCheckRestrictionTest() throws InterruptedException {
		WebElement ce;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		// driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// myEnvironment.waitForPageLoaded();

		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		myEnvironment.setTestSubject("Canada Restriction Message Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// check to see if the proper ad is displayed in header
		// ce = myEnvironment.waitForDynamicElement(
		// By.xpath(myHeaderObjs.getTheCountryFlag()));
		// url = driver.getCurrentUrl();
		// myHeaderObjs.checkFlagInHeader(driver, myEnvironment, url,
		// By.xpath(myHeaderObjs.getTheCountryFlag()),
		// myHeaderObjs.getTheCanadaShippingPromoBanner());
		
	


		// Signe into an canada account
	//	mySIO.signIn(
		//		mySIO.getTheCanadaUsername(), mySIO.getThePassword());
	//	myEnvironment.waitForPageLoaded();
	//	ce = myEnvironment.waitForDynamicElement(
	//			By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
	//	ss = ss + 1;

	//	myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
	//			testName, "MyAccountPage", "");
	//	ss = ss + 1;
		

		// Go to home page
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		ce.click();

	//	myEnvironment.waitForPageLoaded();
	//	myEnvironment.waitForPageLoaded();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "HomePage", "");
		ss = ss + 1;
		
	
		
		//myEnvironment.waitForPageLoaded();
		
	
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathHomeDecorTab()));
		ce.click();
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheByXPathSearchInput()));
		js.executeScript(mouseOverScript, home);

		// check restriction message on diffuser product
		ce = myEnvironment.waitForDynamicElement(
				By.linkText(myHomeDecorObjs.getTheByLinkDiffusers()));
		ce.click();
		
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedKingdomFlag(), testName);
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myProductListingObjs.getTheByXPathDiffuser()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myProductListingObjs.getThePageName(), "DiffuserShippingRestriction");
		ss = ss + 1;

		myProductListingObjs.checkRestrictionText(myProducts.getTheDiffusor());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myProductDetailObjs.getThePageName(), "DiffuserShippingRestriction");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathHomeDecorTab()));
		ce.click();
		myEnvironment.waitForTitle("Home Decor | Outdoor Furniture | Designer Home Decor | Tommy Bahama");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver.findElement(By.xpath(myHeaderObjs
				.getTheByXPathSearchInput()));
		js.executeScript(mouseOverScript, home);

		
		
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHomeDecorObjs.getTheByXPathGolf()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myProductListingObjs.getThePageName(), "GolfBagShippingRestriction");
		ss = ss + 1;
		myProductListingObjs.checkRestrictionText(myProducts.getTheGolfBag());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myProductDetailObjs.getThePageName(), "GolfBagShippingRestriction");
		ss = ss + 1;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		ce.click();
		
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForTitle("Men's");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailInput()));
		ce.click();
		
		Thread.sleep(4000);
		// Check Credit restriction message
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//ul/li[8]/a"));
		ce.click();
		
		
		
		myEnvironment.waitForPageLoaded();
		ce = myEnvironment.waitForDynamicElement( By
				.xpath("//div[2]/map/area"));
		String linkUrl = ce.getAttribute("href");
		
		

		// there is a problem with these link because they are stored in a sort
		// of map, to get past his I just grab the
		// link in the link and have the driver navigate to it. Kind of a hack
		// but it seems the best until selenium supports
		// how these links are stored.
		driver.get(linkUrl);
		myEnvironment.waitForPageLoaded();
	//	ce = myEnvironment.waitForDynamicElement(
	//			By.xpath(myTraditionalCardObjs.getTheByXPathCardLink()));
	//	ce.click();
	//	myEnvironment.waitForPageLoaded();
		ce = myEnvironment.waitForDynamicElement( By
				.xpath(myTraditionalCardObjs
						.getTheByXPathShippingRestrictions()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myProductDetailObjs.getThePageName(), "GiftCardShippingRestriction");
		ss = ss + 1;
		Assert.assertTrue(ce.getText().contains(
				"Shipping restrictions apply"));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myTraditionalCardObjs.getTheTraditionalCardLink()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, myProductListingObjs.getThePageName(), "GiftCardShippingRestriction");
		ss = ss + 1;
		myProductListingObjs.checkAllTraditionalCards();

		// Go to home page
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		ce.click();
		myEnvironment.waitForPageLoaded();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));

		Thread.sleep(1000);
		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
		myEnvironment.waitForPageLoaded();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		ce.click();
		myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {
			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));

			
			bw.write("<html><body><center><h2>Canada Restriction Message Test Results</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+ "<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3 style= \"width:70%;\" >The Canada Restriction Message test is testing that Canada restriction text"
					+ " is properly displaying on the website when the current country"
					+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, "ContextChooser",
							"This is the Context Chooser modal.")
					+
					
					myEnvironment.addSSToFile(testName, "SignInPageBlank",
							"This is the blank Sign-In Page.")
					+

				myEnvironment.addSSToFile(testName, "MyAccountPage",
							"This is the Account Page.")
					+
										
					myEnvironment.addSSToFile(testName, "HomePage",
							"This is the Home Page.")
					+

					myEnvironment.addSSToFile(testName, "PLPDiffuserShippingRestriction",
							"This is the Diffusor PLP with shipping restriction text.")
					+
					
					myEnvironment.addSSToFile(testName, "PDPDiffuserShippingRestriction",
							"This is the Diffusor PDP with shipping restriction text.")
					+
					
					myEnvironment.addSSToFile(testName, "PLPGolfBagShippingRestriction",
							"This is the Golf PLP with shipping restriction text.")
					+
					
					myEnvironment.addSSToFile(testName, "PDPGolfBagShippingRestriction",
							"This is the Golf PDP with shipping restriction text.")
					+
					
					myEnvironment.addSSToFile(testName, "PDPGiftCardShippingRestriction",
							"This is the Gift Card PDP with shipping restriction text.")
					+

					
					myEnvironment.addSSToFile(testName, "PLPGiftCardShippingRestriction",
							"This is the Gift Card PLP with shipping restriction text.")
					+

					myEnvironment.getPageTestOutcome()
					+ "</center></body></html>");
			bw.close();
			
			
			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Canada Restriction Message Test Results</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+ "<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3 style= \"width:70%;\" >The Canada Restriction Message test is testing that Canada restriction text"
							+ " is properly displaying on the website when the current country"
							+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						

			List<String> fns = new ArrayList<String>();
			
			fns.add("SignInPageBlank");
			fns.add("MyAccountPage");
		    fns.add("HomePage");
			fns.add("PLPDiffuserShippingRestriction");
			fns.add("PDPDiffuserShippingRestriction");
			fns.add("PLPGolfBagShippingRestriction");
			fns.add("PDPGolfBagShippingRestriction");
			fns.add("PDPGiftCardShippingRestriction");
			fns.add("PLPGiftCardShippingRestriction");
			
			ss = ss - 8;

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
			driver.quit();
		}
	}