package FFCondensedWebdriverTests;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class FFCondensedCanadaOrderTest {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private AccountPage myAccountObjs;
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	private FiftyOne myFiftyOneObjs = new FiftyOne();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	// this is the item in a shopping bag
	private String item = "ITEM";
	// this is the color of an item in the shopping bag
	private String color = "COLOR";
	// size of an item in the shopping bag
	private String size = "SIZE";
	// price of an item in the shopping bag
	private String price = "PRICE";
	// qty of an item in the shopping bag or PDP
	private String qty = "QTY";
	// subtotal of order
	private String subtotal = "SUBTOTAL";
	// item to check restriction text
	private String firstCanadaProduct = "TH2459";
	// item to be used in place of the bike.
	private String thirdCanadaProduct = "TBG-099";
	// item to check restriction text
	// private String shoe = "TFM00020";
	private String theEnvironment = "";
	private String ShippingPrice = "";
	private String testName = "TheCanadaOrderTest";
	private int ss = 0;
	// url to be checked on various pages
	private String url = "";
	String OrderNumber = "";
	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	// private String baseUrl;
	private WebDriver driver;
	WebElement currentElement;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();

		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());

		File fileToProfile = new File(myEnvironment.getFfProfile());
		FirefoxProfile p = new FirefoxProfile(fileToProfile);

		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
															// here

		driver = new FirefoxDriver(p);
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myAccountObjs = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
			
	}

	@Test
	public void TheCanadaOrderTest() throws InterruptedException {

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		myEnvironment.setTestSubject("Canada Order Test");

		// Signe into an canada account
		mySIO.signIn(
				mySIO.getTheCanadaUsername(), mySIO.getThePassword());
		myEnvironment.waitForPageLoaded();
		// ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "MyAccountPage", "");
		// ss = ss + 1;

		// ss = ss + 1;
		// ss = ss + 1;
		// add shoes to the bag
		// myProductDetailObjs.selectProduct(driver, myEnvironment,
		// myProductListingObjs, myHeaderObjs,
		// myHeaderObjs.getTheByXpathMensTab(), myMenObjs.getTheByXPathShoes(),
		// this.shoe, "1",
		// testName, myEnvironment.getFfBrowser());
		// myEnvironment.waitForPageLoaded(driver);

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "Products");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;

		// add gift services to the products
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[2]/td[2]/span[2]/a"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myGiftServiceObjs.addToGiftBox( myShoppingBag.getTheFirstGiftLink(),
				testName, myEnvironment.getFfBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[3]/td[2]/span[2]/a"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getFfBrowser());
		ss = ss + 1;
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "ProductsWithGiftServices");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/a"));

		// continue to address page
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();

		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "GiftServicesAndShippingErrors");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.ShippingRestrictionText));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRestrictionText()));
		// String errorColor =
		// driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).GetCssValue("Color");
		String errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheCustomsGiftServiceErrorText()));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.CustomsGiftServiceErrorText));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.TraditionalGiftCardCanadaErrorText));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstRowGiftServicesError()));
		System.out.println("This is the GiftServicesError:       "
				+ currentElement.getText());

		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheSecondRowGiftServivesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

		currentElement = myEnvironment
				.waitForDynamicElement( By.xpath(myShoppingBag
						.getTheFourthRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));

		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myShoppingBag.getTheThirdRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myShoppingBag.getTheFifthRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

		// checks for the yellow outline
		myShoppingBag.checkProductsForYellowOutline(7);

		// see that the shipping restriction message is present in the shopping
		// bag
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[4]/td[2]/span[2]/div[2]"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/div[2]"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[6]/td[2]/span[2]/div"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));

		// remove all the products that cannot be shipped to Canada
		myShoppingBag.removeProducts(2);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// removes the gift services from remaining products
		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ShoppingBag", "GiftServicesErrors");
		ss = ss + 1;
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		// currentElement = myEnvironment.waitForDynamicElement(
		// By.xpath(myHeaderObjs.getTheByXPathLogo()));
		// // myEnvironment.myWaitForWDElement(driver,
		// By.cssSelector(myHeaderObjs.getTheByCSSLogo()));
		// currentElement.click();
		// myEnvironment.waitForPageLoaded(driver);
		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// click the first image to add more qty
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myProductDetailObjs.getTheMainImage()));
		// myEnvironment.waitForDynamicElement(
		// By.xpath(myProductDetailObjs.getTheMainImage()));
		myProductDetailObjs.addQuantityToBag("8");
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));

		// Test for all of the columns are present in the shopping bag
		Assert.assertEquals(
				item,
				driver.findElement(
						By.xpath(myShoppingBag.getTheItemColumn()))
						.getText());
		Assert.assertEquals(
				color,
				driver.findElement(
						By.xpath(myShoppingBag.getTheColorColumn()))
						.getText());
		Assert.assertEquals(
				size,
				driver.findElement(
						By.xpath(myShoppingBag.getTheSizeColumn()))
						.getText());
		Assert.assertEquals(
				price,
				driver.findElement(
						By.xpath(myShoppingBag.getThePriceColumn()))
						.getText());
		Assert.assertEquals(
				qty,
				driver.findElement(
						By.xpath(myShoppingBag.getTheQtyColumn()))
						.getText());
		Assert.assertEquals(
				subtotal,
				driver.findElement(
						By.xpath(myShoppingBag.getTheSubTotalColumn()))
						.getText());

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		// myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
		// testName, "ShoppingBag", "Normal");
		// ss = ss + 1;
		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "Address", "PreFilled");
		ss = ss + 1;
		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingZipCode()));
		String theShippingZip = currentElement.getAttribute("value");
		Assert.assertTrue(theZip.contains(theShippingZip));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		// Checks to see if the shipping price is correct on the payment page.
		ShippingPrice = "";
		ShippingPrice = driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText();
		ShippingPrice = ShippingPrice.substring(1);
		Double theShippingPrice = Double.parseDouble(ShippingPrice);
		Assert.assertEquals(theShippingPrice,
				myPaymentPage.getTheActualCanadaShippingPrice());
		// myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
		// testName, "PaymentPage", "");
		// ss = ss + 1;
		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// see that Card Services are working
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalenceLink()));
		currentElement.click();
		// myEnvironment.waitForPageLoaded(driver);
		Thread.sleep(1000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));
		Thread.sleep(1000);
		String theBalence = currentElement.getText();
		Assert.assertTrue(theBalence.contains("$25.00"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "PaymentPage", "GiftCardBalenceModal");
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myPaymentPage.getTheByLinkX()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		myEnvironment.waitForPageLoaded();
		ss = ss + 1;
		// waits for preview page to load
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheSubTotal()));

		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "PreviewPage", "");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalTax())).getText();
		String theDuty = driver.findElement(
				By.id(myPreviewObjs.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewObjs.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()))
				.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));

		// myEnvironment.myWaitForWDElement(driver,
		// By.xpath(myConfirmationObjs.getTheOrderNumber()));

		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText());
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationObjs.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getFfBrowser(),
				testName, "ConfirmationPage", "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		OrderNumber = driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText();
		OrderNumber = OrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		Assert.assertEquals(
				"$16.00",
				driver.findElement(
						By.xpath(myConfirmationObjs.getTheShippingAmount()))
						.getText().trim());

		// Insert a comma for how totals appear in contact center and OG
		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, OrderNumber, testName,
				myEnvironment.getFfBrowser());
		
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		ss = ss + 1;

		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
	String newTotal = "";
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				OrderNumber, testName, myEnvironment.getFfBrowser());
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		ss = ss + 1;
	}

	@After
	public void quitDriver() throws MessagingException,
			UnsupportedEncodingException, InterruptedException {

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><h2>Canada Order Test Results</h2>"
						+ "The testing environment that was used for this test is          <br/>"
						+ myEnvironment.getTheTestingEnvironment()
						+ "<br/><br/>"
						+ "The browser that was used for this test is          <br/>"
						+ myEnvironment.getBrowser()
						+ "<br/><br/>"
						+ "<h3>The Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><br/>"
						+ "Above each screenshot is a textual description of what page on the website the screen shot is of. If the the text and the screenshot do not match or the image is missing then the test failed.<br/><br/>"
						+
						// "<h3>This is the blank Sign-In Page.</h3>" +
						// "<img src=\"cid:SignInPageBlank\"/><br/> " +
						// "<h3>This is the Dinnerware PLP.</h3><br/>" +
						// "<img src=\"cid:PLPDinnerware\"/><br/> " +
						// "<h3>This is the Dinnerware PDP.</h3><br/>" +
						// "<img src=\"cid:PDPTH2459\"/><br/> " +
						// "<h3>This is the the Pants PLP.</h3><br/>" +
						// "<img src=\"cid:PLPPants$128.00\"/><br/> " +
						// // "<h3>This is the the Pants PDP.</h3><br/>" +
						// "<img src=\"cid:PDP$128.00\"/><br/> " +
						// "<h3>This is the the GiftCard PDP.</h3><br/>" +
						// "<img src=\"cid:PDPTraditionalGCFF\"/><br/> " +
						// "<h3>This is the the Golf PLP.</h3><br/>" +
						// "<img src=\"cid:PLPGolfBag\"/><br/> " +
						// "<h3>This is the the Golf PDP.</h3><br/>" +
						// "<img src=\"cid:PDPTBG-099\"/><br/> " +
						// "<h3>This is the the Pants PLP.</h3><br/>" +
						// "<img src=\"cid:PLPPants$98.00\"/><br/> " +
						// "<h3>This is the the Pants PDP.</h3><br/>" +
						// "<img src=\"cid:PDP$98.00\"/><br/> " +
						"<h3>This is the the Shopping Bag with products.</h3><br/>"
						+ "<img src=\"cid:ShoppingBagProducts\"/><br/> "
						+ "<h3>This is the Gift Box Modal.</h3><br/>"
						+ "<img src=\"cid:GiftServicesModalGiftBox\"/><br/> "
						+ "<h3>This is the Add To Gift Box Modal.</h3><br/>"
						+ "<img src=\"cid:GiftServicesModalAddToGiftBox1\"/><br/> "
						+ "<h3>This is the Gift Message Modal.</h3><br/>"
						+ "<img src=\"cid:GiftServicesModalGiftMessage\"/><br/> "
						+ "<h3>This is the the Shopping Bag containing Products with Gift Services.</h3><br/>"
						+ "<img src=\"cid:ShoppingBagProductsWithGiftServices\"/><br/> "
						+ "<h3>This is the the Shopping Bag containing Products with different types of error messages.</h3><br/>"
						+ "<img src=\"cid:ShoppingBagGiftServicesAndShippingErrors\"/><br/> "
						+ "<h3>This is the the Shopping Bag containing Products with Gift Services error messages.</h3><br/>"
						+ "<img src=\"cid:ShoppingBagGiftServicesErrors\"/><br/> "
						+

						"<h3>This is the the PDP adding 8 more items.</h3><br/>"
						+ "<img src=\"cid:PDP8AddedToBag\"/><br/> "
						+
						// "<h3>This is the the Shopping Bag with products.</h3><br/>"
						// +
						// "<img src=\"cid:ShoppingBagNormal\"/><br/> " +
						"<h3>This is the the Address Page with pre populated fields.</h3><br/>"
						+ "<img src=\"cid:AddressPreFilled\"/><br/> "
						+
						// "<h3>This is the the Payment page.</h3><br/>" +
						// "<img src=\"cid:PaymentPage\"/><br/> " +
						"<h3>This is the the Gift Card Balence Modal.</h3><br/>"
						+ "<img src=\"cid:PaymentPageGiftCardBalenceModal\"/><br/> "
						+ "<h3>This is the the Payment Page after Credit Card information entered.</h3><br/>"
						+ "<img src=\"cid:PaymentPageCompleted\"/><br/> "
						+ "<h3>This is the the Preview Page.</h3><br/>"
						+ "<img src=\"cid:PreviewPage\"/><br/> "
						+ "<h3>This is the the Confirmation Page.</h3><br/>"
						+ "<img src=\"cid:ConfirmationPage\"/><br/> "
						+

						"<h3>This is the the Contact Center Page.</h3><br/>"
						+ "<img src=\"cid:ContactCenter"
						+ OrderNumber
						+ "\"/><br/> "
						+ "<h3>This is the the PaymenTech Page.</h3><br/>"
						+ "<img src=\"cid:PaymenTech"
						+ OrderNumber
						+ "\"/><br/> "
						+

						"If you were able to see all images then the test passed."
						+ "</body></html>", "text/html");

		List<String> fns = new ArrayList<String>();

		fns.add("ShoppingBagProducts");
		fns.add("GiftServicesModalGiftBox");
		fns.add("GiftServicesModalAddToGiftBox1");
		fns.add("GiftServicesModalGiftMessage");
		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("ShoppingBagGiftServicesAndShippingErrors");
		fns.add("ShoppingBagGiftServicesErrors");
		fns.add("PDP8AddedToBag");
		// fns.add("ShoppingBagNormal");
		fns.add("AddressPreFilled");
		// fns.add("PaymentPage");
		fns.add("PaymentPageGiftCardBalenceModal");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + OrderNumber + "");
		fns.add("PaymenTech" + OrderNumber + "");

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getJackEmail(), myEnvironment.getTestSubject(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		System.out.println("This is the size of the ss list			" + fns.size());

		if (ss == fns.size())
			setUpTest();

		driver.quit();
	}

	public void setUpTest() throws InterruptedException, MessagingException,
			UnsupportedEncodingException {
		ss = ss * 0;
		try {
			// Navigate to the testing environment
			driver.get(myEnvironment.getTheTestingEnvironment());
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			JavascriptExecutor js = (JavascriptExecutor) driver;

			myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
					myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
			myEnvironment.waitForPageLoaded();
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(myHeaderObjs.getTheByXPathLogo()));
			// adds to the shopping bag
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

			myProductDetailObjs.selectProduct(
					myHeaderObjs.getTheByXPathHomeDecorTab(),
					myHomeDecorObjs.getTheByXPathDinnerware(),
					this.firstCanadaProduct, "1", "Dinnerware");

			ss = ss + 1;
			ss = ss + 1;

			// here i have to move the virtual mouse to the tommy bahama logo so
			// it does not activate the hover
			// menu because this confuses selenium and it will sometimes select
			// the incorrect link
			WebElement home = driver.findElement(By.xpath(myHeaderObjs
					.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);

			myProductDetailObjs.selectProductByPrice(
					myHeaderObjs.getTheByXPathMensTab(),
					myMenObjs.getTheByXPathPants(), "$128.00", "1",
					"Pants");

			ss = ss + 1;
			ss = ss + 1;
			// here i have to move the virtual mouse to the tommy bahama logo so
			// it does not activate the hover
			// menu because this confuses selenium and it will sometimes select
			// the incorrect link
			home = driver.findElement(By.xpath(myHeaderObjs
					.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);

			myTraditionalCardObjs.addTraditionalCard(myEnvironment, driver,
					myHeaderObjs, myMenObjs, testName, "PDPTraditionalGC",
					myEnvironment.getFfBrowser());
			myEnvironment.waitForPageLoaded();
			ss = ss + 1;

			// here i have to move the virtual mouse to the tommy bahama logo so
			// it does not activate the hover
			// menu because this confuses selenium and it will sometimes select
			// the incorrect link
			home = driver.findElement(By.xpath(myHeaderObjs
					.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);
			// Add second restricted product
			myProductDetailObjs.selectProduct(
					myHeaderObjs.getTheByXPathHomeDecorTab(),
					myHomeDecorObjs.getTheByXPathGolf(), this.thirdCanadaProduct,
					"1",  "Golf");

			ss = ss + 1;
			ss = ss + 1;

			// here i have to move the virtual mouse to the tommy bahama logo so
			// it does not activate the hover
			// menu because this confuses selenium and it will sometimes select
			// the incorrect link
			home = driver.findElement(By.xpath(myHeaderObjs
					.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);

			myEnvironment.waitForPageLoaded();
			myProductDetailObjs.selectProductByPrice(
					myHeaderObjs.getTheByXPathMensTab(),
					myMenObjs.getTheByXPathPants(), "$98.00", "1", 
					"Pants");

			ss = ss + 1;
			ss = ss + 1;
		}

		catch (Exception e) {

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><h2>Canada Order Test Setup Results</h2>"
							+ "<h3>This is a setup for the Canada Order test. If this failed, the next test will not run correctly.</h3><br/>"
							+ "Above each screenshot is a textual description of what page on the website the screen shot is of. If the the text and the screenshot do not match or the image is missing then the test failed.<br/><br/>"
							+

							"<h3>This is the Dinnerware PLP.</h3><br/>"
							+ "<img src=\"cid:PLPDinnerware\"/><br/> "
							+ "<h3>This is the Dinnerware PDP.</h3><br/>"
							+ "<img src=\"cid:PDPTH2459\"/><br/> "
							+ "<h3>This is the the Pants PLP.</h3><br/>"
							+ "<img src=\"cid:PLPPants$128.00\"/><br/> "
							+ "<h3>This is the the Pants PDP.</h3><br/>"
							+ "<img src=\"cid:PDP$128.00\"/><br/> "
							+ "<h3>This is the the GiftCard PDP.</h3><br/>"
							+ "<img src=\"cid:PDPTraditionalGCFF\"/><br/> "
							+ "<h3>This is the the Golf PLP.</h3><br/>"
							+ "<img src=\"cid:PLPGolfBag\"/><br/> "
							+ "<h3>This is the the Golf PDP.</h3><br/>"
							+ "<img src=\"cid:PDPTBG-099\"/><br/> "
							+ "<h3>This is the the Pants PLP.</h3><br/>"
							+ "<img src=\"cid:PLPPants$98.00\"/><br/> "
							+ "<h3>This is the the Pants PDP.</h3><br/>"
							+ "<img src=\"cid:PDP$98.00\"/><br/> "
							+ "If you were able to see all images then the test passed."
							+ "</body></html>", "text/html");

			List<String> fns = new ArrayList<String>();
			fns.add("PLPDinnerware");
			fns.add("PDPTH2459");
			fns.add("PLPPants$128.00");
			fns.add("PDP$128.00");
			fns.add("PDPTraditionalGCFF");
			fns.add("PLPGolfBag");
			fns.add("PDPTBG-099");
			fns.add("PLPPants$98.00");
			fns.add("PDP$98.00");

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getJackEmail(),
					myEnvironment.getTestSubject(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
		}
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><h2>Canada Order Test Setup Results</h2>"
						+ "<h3>This is a setup for the Canada Order test. If this failed, the next test will not run correctly.</h3><br/>"
						+ "Above each screenshot is a textual description of what page on the website the screen shot is of. If the the text and the screenshot do not match or the image is missing then the test failed.<br/><br/>"
						+

						"<h3>This is the Dinnerware PLP.</h3><br/>"
						+ "<img src=\"cid:PLPDinnerware\"/><br/> "
						+ "<h3>This is the Dinnerware PDP.</h3><br/>"
						+ "<img src=\"cid:PDPTH2459\"/><br/> "
						+ "<h3>This is the the Pants PLP.</h3><br/>"
						+ "<img src=\"cid:PLPPants$128.00\"/><br/> "
						+ "<h3>This is the the Pants PDP.</h3><br/>"
						+ "<img src=\"cid:PDP$128.00\"/><br/> "
						+ "<h3>This is the the GiftCard PDP.</h3><br/>"
						+ "<img src=\"cid:PDPTraditionalGCFF\"/><br/> "
						+ "<h3>This is the the Golf PLP.</h3><br/>"
						+ "<img src=\"cid:PLPGolfBag\"/><br/> "
						+ "<h3>This is the the Golf PDP.</h3><br/>"
						+ "<img src=\"cid:PDPTBG-099\"/><br/> "
						+ "<h3>This is the the Pants PLP.</h3><br/>"
						+ "<img src=\"cid:PLPPants$98.00\"/><br/> "
						+ "<h3>This is the the Pants PDP.</h3><br/>"
						+ "<img src=\"cid:PDP$98.00\"/><br/> "
						+ "If you were able to see all images then the test passed."
						+ "</body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add("PLPDinnerware");
		fns.add("PDPTH2459");
		fns.add("PLPPants$128.00");
		fns.add("PDP$128.00");
		fns.add("PDPTraditionalGCFF");
		fns.add("PLPGolfBag");
		fns.add("PDPTBG-099");
		fns.add("PLPPants$98.00");
		fns.add("PDP$98.00");

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getJackEmail(), myEnvironment.getTestSubject(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);
		driver.quit();
	}
}
