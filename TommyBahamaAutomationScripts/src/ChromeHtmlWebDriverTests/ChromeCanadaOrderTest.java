package ChromeHtmlWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class ChromeCanadaOrderTest {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private AccountPage myAccountObjs;
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs;
	private FiftyOne myFiftyOneObjs = new FiftyOne();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	private Product myProducts = new Product();

	private String theEnvironment = "";
	private String ShippingPrice = "";
	private String testName = "TheCanadaOrderTest";
	private int ss = 0;
	// url to be checked on various pages
	private String url = "";
	String OrderNumber = "";
	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();

		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());

		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myAccountObjs = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		 myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		 
		 String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\Chrome\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("MyAccountPage");
			fns.add("HomePage");
			fns.add("PDPTraditionalGC");
			fns.add("PLPDinnerware");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add(myProductListingObjs.getThePageName() + myProducts.getTheHighPantsPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice());
			fns.add("PLPGolfBag");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag());
			fns.add(myProductListingObjs.getThePageName() + myProducts.getTheLowPantsPrice());
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheLowPantsPrice());
			fns.add("ShoppingBagProducts");
			fns.add("GiftServicesModalGiftBox");
			fns.add("GiftServicesModalAddToGiftBox1");
			fns.add("GiftServicesModalGiftMessage");
			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add("ShoppingBagGiftServicesAndShippingErrors");
			fns.add("ShoppingBagGiftServicesErrors");
			 fns.add("PDP8AddedToBag");
			fns.add("AddressPreFilled");
			fns.add("PaymentPageGiftCardBalenceModal");
			fns.add("PaymentPageCompleted");
			fns.add("PreviewPage");
			fns.add("ConfirmationPage");
			fns.add("ContactCenter" + OrderNumber + "");
			fns.add("PaymenTech" + OrderNumber + "");
			
			myEnvironment.removeSS(fns, tempFile);
			
	}

	@Test
	public void TheCanadaOrderTest() throws InterruptedException {
		WebElement currentElement;
		// Navigate to the testing environment
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.get(myEnvironment.getTheTestingEnvironment());

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment.setTestSubject("Canada Order Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// Signe into an canada account
		mySIO.signIn(mySIO.getTheCanadaUsername(), mySIO.getThePassword());
		myEnvironment.waitForPageLoaded();
		// ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MyAccountPage", "");
		// ss = ss + 1;
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();

		
		
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "HomePage", "");
		// ss = ss + 1;
		// adds a t-shirt to the shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myTraditionalCardObjs.addTraditionalCard(myEnvironment, driver,
				myHeaderObjs, myMenObjs, testName, "PDPTraditionalGC",
				myEnvironment.getChromeBrowser());
		myEnvironment.waitForPageLoaded();
		ss = ss + 1;

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "1",  "Dinnerware");
		ss = ss + 1;
		ss = ss + 1;
		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathPants(), "$128.00", "1",
				"Pants");

		ss = ss + 1;
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
				 "GolfBag");
		ss = ss + 1;
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myEnvironment.waitForPageLoaded();
		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathPants(), "$98.00", "1", 
				"Pants");

		ss = ss + 1;
		ss = ss + 1;

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "Products");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;

		// add gift services to the products
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[2]/td[2]/span[2]/a"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myGiftServiceObjs.addToGiftBox( myShoppingBag.getTheFirstGiftLink(),
				testName, myEnvironment.getChromeBrowser());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[3]/td[2]/span[2]/a"));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		myGiftServiceObjs.giftBox(myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		ss = ss + 1;
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "ProductsWithGiftServices");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/a"));

		// continue to address page
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();

		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "GiftServicesAndShippingErrors");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.ShippingRestrictionText));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBagObjs.getTheShippingRestrictionText()));
		// String errorColor =
		// driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).GetCssValue("Color");
		String errorColor = currentElement.getCssValue("color");
	//	System.out.println(errorColor);
	//	Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
	//	Assert.assertTrue(currentElement.getText().contains(
	//			myShoppingBagObjs.getTheCustomsGiftServiceErrorText()));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.CustomsGiftServiceErrorText));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.TraditionalGiftCardCanadaErrorText));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstRowGiftServicesError()));
		System.out.println("This is the GiftServicesError:       "
				+ currentElement.getText());

		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheSecondRowGiftServivesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));

//		currentElement = myEnvironment
//				.waitForDynamicElement( By.xpath(myShoppingBagObjs
//						.getTheFourthRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBagObjs.getTheGiftServicesErrorText()));
//
//		currentElement = myEnvironment.waitForDynamicElement( By
//				.xpath(myShoppingBagObjs.getTheThirdRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBagObjs.getTheShippingRemovalText()));
//		errorColor = currentElement.getCssValue("color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
//
//		currentElement = myEnvironment.waitForDynamicElement( By
//				.xpath(myShoppingBagObjs.getTheFifthRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBagObjs.getTheShippingRemovalText()));
//		errorColor = currentElement.getCssValue("color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("rgba(168, 49, 39, 1)"));
//
//		// checks for the yellow outline
//		myShoppingBagObjs.checkProductsForYellowOutline(driver, myEnvironment,
//				7);

		// see that the shipping restriction message is present in the shopping
		// bag
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("//div[@id='divShippingRestrictions']"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("(//div[@id='divShippingRestrictions'])[2]"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));
		currentElement = myEnvironment
				.waitForDynamicElement(
						By.xpath("(//div[@id='divShippingRestrictions'])[3]"));
		System.out.println(currentElement.findElement(By.tagName("a"))
				.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		Assert.assertTrue(currentElement.findElement(By.tagName("a")).getText()
				.contains("DETAILS"));

		// remove all the products that cannot be shipped to Canada
		myShoppingBag.removeProducts( 2);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// removes the gift services from remaining products
		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "GiftServicesErrors");
		ss = ss + 1;
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		// currentElement = myEnvironment.waitForDynamicElement(
		// By.xpath(myHeaderObjs.getTheByXPathLogo()));
		// // myEnvironment.myWaitForWDElement(driver,
		// By.cssSelector(myHeaderObjs.getTheByCSSLogo()));
		// currentElement.click();
		// myEnvironment.waitForPageLoaded();
		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		// click the first image to add more qty
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myProductDetailObjs.getTheMainImage()));
		// myEnvironment.waitForDynamicElement(
		// By.xpath(myProductDetailObjs.getTheMainImage()));
		myProductDetailObjs.addQuantityToBag("8");
		// ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));

		// Test for all of the columns are present in the shopping bag
	myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		// myEnvironment.TakeScreenShot(driver,
		// myEnvironment.getChromeBrowser(),
		// testName, "ShoppingBag", "Normal");
		// ss = ss + 1;
		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));
		myEnvironment.TakeScreenShot(myEnvironment.getChromeBrowser(),
				testName, "Address", "PreFilled");
		ss = ss + 1;
		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheShippingZipCode()));
		String theShippingZip = currentElement.getAttribute("value");
		Assert.assertTrue(theZip.contains(theShippingZip));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		// Checks to see if the shipping price is correct on the payment page.
		ShippingPrice = "";
		ShippingPrice = driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText();
		ShippingPrice = ShippingPrice.substring(1);
		Double theShippingPrice = Double.parseDouble(ShippingPrice);
		Assert.assertEquals(theShippingPrice,
				myPaymentPage.getTheActualCanadaShippingPrice());
		// myEnvironment.TakeScreenShot(driver,
		// myEnvironment.getChromeBrowser(),
		// testName, "PaymentPage", "");
		// ss = ss + 1;
		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// see that Card Services are working
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalenceLink()));
		currentElement.click();
		// myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardNumberInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCheckCardBalanceBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdCardBalence()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		String theBalence = currentElement.getText();

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "PaymentPage", "GiftCardBalenceModal");
		ss = ss + 1;
		System.out.println("This is the balence:				" + theBalence);
		Assert.assertTrue(theBalence.contains("$25.00"));

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myPaymentPage.getTheByLinkX()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.clear();

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		myEnvironment.waitForPageLoaded();
		ss = ss + 1;
		// waits for preview page to load
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheSubTotal()));

		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "PreviewPage", "");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalTax())).getText();
	//	String theDuty = driver.findElement(
	//			By.id(myPreviewObjs.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewObjs.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()))
				.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));

		// myEnvironment.myWaitForWDElement(driver,
		// By.xpath(myConfirmationObjs.getTheOrderNumber()));

		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText());
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationObjs.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ConfirmationPage", "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		OrderNumber = driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText();
		OrderNumber = OrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		Assert.assertEquals(
				"$16.00",
				driver.findElement(
						By.xpath(myConfirmationObjs.getTheShippingAmount()))
						.getText().trim());
		
		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, OrderNumber, testName,
				myEnvironment.getChromeBrowser());

		// Insert a comma for how totals appear in contact center and OG
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);

		
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		
		
		ss = ss + 1;
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		String newTotal = "";
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				OrderNumber, testName, myEnvironment.getChromeBrowser());
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		ss = ss + 1;
		
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Canada Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3  style= \"width:70%;\">The Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				
				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, "MyAccountPage",
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, "HomePage",
						"This is the Home Page.")
				+

				myEnvironment.addSSToFile(testName, "PDPTraditionalGC",
						"This is the GiftCard PDP.")
				+
				
				myEnvironment.addSSToFile(testName, "PLPDinnerware",
						"This is the Dinnerware PLP.")
				+
				
		    	myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+

		    	myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheHighPantsPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice(),
						"This is the Pants PDP.")
				+

				myEnvironment.addSSToFile(testName, "PLPGolfBag",
						"This is the Golf PLP.")
				+
																
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag(),
						"This is the Golf PDP.")
				+
					myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheLowPantsPrice(),
						"This is the Pants PLP.")
				+
				
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheLowPantsPrice(),
						"This is the Pants PDP.")
				+
							
				myEnvironment.addSSToFile(testName, "ShoppingBagProducts",
						"This is the Shopping Bag with products.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalGiftBox",
						"This is the Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalAddToGiftBox1",
						"This is the Add To Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalGiftMessage",
						"This is the Gift Message Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "ShoppingBagProductsWithGiftServices",
						"This is the Shopping Bag containing Products with Gift Services.")
				+
				
				myEnvironment.addSSToFile(testName, "ShoppingBagGiftServicesAndShippingErrors",
						"This is the Shopping Bag containing Products with different types of error messages.")
				+
				
				myEnvironment.addSSToFile(testName, "ShoppingBagGiftServicesErrors",
						"This is the Shopping Bag containing Products with Gift Services error messages.")
				+
				
				myEnvironment.addSSToFile(testName, "PDP8AddedToBag",
						"This is the PDP adding 8 more items.")
				+
				
				myEnvironment.addSSToFile(testName, "AddressPreFilled",
						"This is the Address Page with pre populated fields.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymentPageGiftCardBalenceModal",
						"This is the Gift Card Balence Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
						"This is the Payment Page after Credit Card information entered.")
				+
				
				myEnvironment.addSSToFile(testName, "PreviewPage",
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ConfirmationPage",
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ContactCenter" + OrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymenTech" + OrderNumber,
						"This is the PaymenTech Page.")
				+

				myEnvironment.getPageTestOutcome()
				+ "</center></body></html>");
		bw.close();
	
		
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3  style= \"width:70%;\">The Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add("SignInPageBlank");
		fns.add("MyAccountPage");
		fns.add("HomePage");
		fns.add("PDPTraditionalGC");
		fns.add("PLPDinnerware");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
		fns.add(myProductListingObjs.getThePageName() + myProducts.getTheHighPantsPrice());
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice());
		fns.add("PLPGolfBag");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag());
		fns.add(myProductListingObjs.getThePageName() + myProducts.getTheLowPantsPrice());
		fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheLowPantsPrice());
		fns.add("ShoppingBagProducts");
		fns.add("GiftServicesModalGiftBox");
		fns.add("GiftServicesModalAddToGiftBox1");
		fns.add("GiftServicesModalGiftMessage");
		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("ShoppingBagGiftServicesAndShippingErrors");
		fns.add("ShoppingBagGiftServicesErrors");
		 fns.add("PDP8AddedToBag");
		fns.add("AddressPreFilled");
		fns.add("PaymentPageGiftCardBalenceModal");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + OrderNumber + "");
		fns.add("PaymenTech" + OrderNumber + "");

		
		//ss = ss - 3;
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}