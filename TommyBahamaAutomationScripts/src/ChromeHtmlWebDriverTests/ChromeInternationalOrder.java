package ChromeHtmlWebDriverTests;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.EnvoyPage;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

public class ChromeInternationalOrder {

	// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private SeleniumEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private SignInPage mySIO;
		private Header myHeaderObjs;
		private Men myMenObjs = new Men();
		private ProductListingPage myProductListingObjs;
		private ProductDetailPage myProductDetailObjs;
		private ShoppingBagPage myShoppingBag;

		private PayTech myPayTechObjs;
		private ContactCenter myContactCenterObjs = new ContactCenter();
		private HomeDecor myHomeDecorObjs = new HomeDecor();
		private Product myProducts = new Product();
		private FiftyOne myFiftyOneObjs = new FiftyOne();
		private EnvoyPage myEnvoyPageObjs;

		private String theEnvironment = "";
		int ss = 0;
		double theShippingPrice = 16.00;
		String OrderNumber = "";
		String testName = "TheInternationalOrderTest";
		// url to be checked on various pages
		private String url = "";
		// Main object in the test object is per browser and is used for all
		// interactivity with web page elements.
		private WebDriver driver;

		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		@Before
		public void openBrowser() {
			// baseUrl = System.getProperty("webdriver.base.url");
			Assert.assertTrue(isSupportedPlatform());
			File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			myEnvironment = new SeleniumEnvironment(driver);
			myShoppingBag = new ShoppingBagPage(driver, myEnvironment);

			myHeaderObjs = new Header(driver, myEnvironment);
			 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
			 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
					 myHeaderObjs, testName);
			 myEnvironment.setBrowser(myEnvironment.getChromeBrowser());

			 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);	
			 myEnvoyPageObjs = new EnvoyPage(myEnvironment, driver);
		}

		@Test
		public void TheInternationalOrderTest() throws InterruptedException {
			WebElement ce;
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigate to the testing environment
			driver.get(myEnvironment.getTheTestingEnvironment());

			myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
			myEnvironment.setTestSubject("International Order Test Results");
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			// set implicit wait times so pages do not load due to all the crap that
			// is added by thrid parties
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// //adds a t-shirt to the shopping bag
		/*	myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
					testName, "HomePage", "");
		    ss = ss + 1;*/
		    
			myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs, myFiftyOneObjs.getTheUnitedKingdomFlag(), testName);
			ss = ss + 1;
			
		/*	myProductDetailObjs.selectProduct(
					myHeaderObjs.getTheByXPathHomeDecorTab(),
					myHomeDecorObjs.getTheByXPathDinnerware(),
					myProducts.getTheDinnerWareProduct(), "1",  "Dinnerware");
			ss = ss + 1;
			ss = ss + 1;*/

			// here i have to move the virtual mouse to the tommy bahama logo so it
			// does not activate the hover
			// menu because this confuses selenium and it will sometimes select the
			// incorrect link
			WebElement home = driver.findElement(By.xpath(myHeaderObjs
					.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);

			myProductDetailObjs.selectProductByPrice(
					myHeaderObjs.getTheByXPathMensTab(),
					myMenObjs.getTheByXPathMensTShirts(), "GBP 66.62", "1", 
					"T-Shirt");
			ss = ss + 1;
			ss = ss + 1;
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

			// here i have to move the virtual mouse to the tommy bahama logo so it
			// does not activate the hover
			// menu because this confuses selenium and it will sometimes select the
			// incorrect link
			home = driver
					.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);

			// add shoes to the bag
			myProductDetailObjs.selectProduct(
					myHeaderObjs.getTheByXPathMensTab(),
					myMenObjs.getTheByXPathShoes(), myProducts.getTheShoeProduct(), "1", 
					"Shoes");
			ss = ss + 1;
			ss = ss + 1;
			// here i have to move the virtual mouse to the tommy bahama logo so it
			// does not activate the hover
			// menu because this confuses selenium and it will sometimes select the
			// incorrect link
			home = driver
					.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
			js.executeScript(mouseOverScript, home);

		/*	myProductDetailObjs.selectProduct(
					myHeaderObjs.getTheByXPathHomeDecorTab(),
					myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
					 "Golf");*/

			// continue to shopping bag
			ce = myEnvironment.waitForDynamicElement(
					By.id(myHeaderObjs.getTheByIdPreviewCheckout()));
			ce = myEnvironment.waitForDynamicElement(
					By.linkText(myHeaderObjs.getTheByLinkCheckout()));
			ce.click();

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		

			// Test that thumbnails are present for the items in the shopping bag
			Assert.assertTrue(myEnvironment.isElementPresent(
					By.xpath(myShoppingBag.getTheFirstImage())));

		
//			myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
//					testName, "ShoppingBagProducts", "");
//			ss = ss + 1;
			
			driver.findElement(By.xpath(myShoppingBag.getTheContinueCheckout()))
					.click();
			
			Thread.sleep(5000);
			
		    ce = myEnvironment.waitForDynamicElement(By.name(myEnvoyPageObjs.getTheByNameEnvoyFrame()));
			driver.switchTo().frame(ce);
			
			myEnvoyPageObjs.enterUkAddress();
			
			myEnvoyPageObjs.enterCcInfo();
			
			myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
					testName, "CompletedEnvoyPage", "PaymentByCreditCard");
			ss = ss + 1;
			
			
			
			ce = myEnvironment.waitForDynamicElement(By.id(myEnvoyPageObjs.getTheByIdSubmitBtn()));
			ce.click();
			
			
			
			Thread.sleep(10000);
		
			myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
					testName, "EnvoyConfirmationPage", "PaymentByCreditCard");
			ss = ss + 1;
		
		

		}

		@After
		public void quitDriver() throws MessagingException,
				UnsupportedEncodingException {

			myEnvironment.setNetworkFile("file:///"
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>International Order Test Results</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+ "<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3 style= \"width:70%;\">The International Order Checks for different alerts that notify the user that certain functionality is restricted"
							+ " and makes sure these restrictions are properly displaying on the website when the current country"
							+ " is Great Britan.</h3><table style= \"width:70%;\"><tr><td><p> "
							+ myEnvironment.getTestTextDescription()
							+ myEnvironment.getBrowser()
							+ "\\"
							+ testName
							+ ".jpeg</b></p></td></tr></table><br/>"
							+

						/*	myEnvironment.addSS(testName, "HomePage",
									"This is the Home Page.")
							+*/
							
						myEnvironment.addSS(testName, "ContextChooser",
									"This is the Context Chooser to choose an internation country.")
							+

								myEnvironment.addSS(testName, myProductListingObjs.getThePageName() +  myProducts.getTheGBPTShirtPrice(),
								"This is the T-Shirt PLP.")
						+

						myEnvironment.addSS(testName, myProductDetailObjs.getThePageName() + myProducts.getTheGBPTShirtPrice(),
								"This is the T-Shirt PDP.")
						+

						myEnvironment.addSS(testName, "PLPShoes",
								"This is the Shoes & Sandals PLP.")
						+

						myEnvironment.addSS(testName, myProductDetailObjs.getThePageName() +myProducts.getTheShoeProduct(),
								"This is the Sandals PDP.")
						+
							
						
							/*myEnvironment.addSS(testName, "ShoppingBagProducts",
									"This is the Shopping Bag containing Products.")
							+*/

						myEnvironment.addSS(testName, "CompletedEnvoyPagePaymentByCreditCard",
									"This is the Envoy Page after the Credit Card information is entered.")
							+

						myEnvironment.addSS(testName, "EnvoyConfirmationPagePaymentByCreditCard",
									"This is the Envoy Confirmation Page.")
							+
							
					
							
							"If you were able to see all images then the test passed."
							+ "</center></body></html>", "text/html");

			List<String> fns = new ArrayList<String>();
			//fns.add("HomePage");
			fns.add("ContextChooser");
			fns.add(myProductListingObjs.getThePageName() + myProducts.getTheGBPTShirtPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGBPTShirtPrice());
			fns.add("PLPShoes");
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheShoeProduct());
		//	fns.add("ShoppingBagProducts");			
			fns.add("CompletedEnvoyPagePaymentByCreditCard");			
			fns.add("EnvoyConfirmationPagePaymentByCreditCard");
		

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);

			System.out.println("This is the total number of SS		" + ss);
			driver.quit();
		}
	}
