package ChromeHtmlWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

public class ChromeSplitTenderCanadaOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private AccountPage myAccountObjs;
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	private FiftyOne myFiftyOneObjs = new FiftyOne();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	private Product myProducts = new Product();

	private String theEnvironment = "";
	private String ShippingPrice = "";

	// url to be checked on various pages
	private String url = "";
	String OrderNumber = "";
	private int ss = 0;
	private String testName = "TheSplitTenderCanadaOrderTest";
	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		 myAccountObjs = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
			myHeaderObjs = new Header(driver, myEnvironment);
			 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
			 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
					 myHeaderObjs, testName);
		 myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
		 
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("AccountPage");
			fns.add("HomePageCanada");
			fns.add("PDPTraditionalGC");
			fns.add(myProductListingObjs.getThePageName()  + myProducts.getTheHighPantsPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice());
			fns.add(myProductListingObjs.getThePageName()  + "GolfBag");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag());
			fns.add(myProductListingObjs.getThePageName() + "Dinnerware");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add(myProductListingObjs.getThePageName()  + myProducts.getTheLowPantsPrice());
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheLowPantsPrice());
			fns.add("ShoppingBagProducts");
			fns.add("GiftServicesModalGiftBox");
			fns.add("GiftServicesModalAddToGiftBox1");
			fns.add("GiftServicesModalGiftMessage");
			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add("ShoppingBagGiftServicesAndShippingErrors");
			fns.add("AddressPagePreFilled");
			fns.add("PaymentPage");
			fns.add("PaymentPageCompleted");
			fns.add("PreviewPage");
			fns.add("ConfirmationPage");
			fns.add("ContactCenter" + OrderNumber + "");
			fns.add("PaymenTech" + OrderNumber + "");
			fns.add("PaymenTech" + myPaymentPage.getTheGiftCardNumber());
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\Chrome\\" + testName;
			myEnvironment.removeSS(fns, tempFile);
			
	}

	@Test
	public void TheSplitTenderCanadaOrderTest() throws InterruptedException {

		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// WebDriverWait wait = new WebDriverWait(driver,
		// TimeSpan.FromSeconds(30));
		WebElement currentElement;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());

		myEnvironment.setTestSubject("Canada Split Tender Order Test Results");
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Signe into an canada account
		mySIO.signIn(
				mySIO.getTheCanadaTenUserName(), mySIO.getThePassword());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "AccountPage", "");
		ss = ss + 1;
		// Go to home page
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();

		Thread.sleep(5000);
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "HomePage", "Canada");
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// // Add the Gift Card
		myTraditionalCardObjs.addTraditionalCard(myEnvironment, driver,
				myHeaderObjs, myMenObjs, testName, "PDPTraditionalGC", "");

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathPants(), "$128.00", "1",
				"Pants");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// Add second restricted product
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
			    "GolfBag");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "1", "Dinnerware");

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathPants(), "$98.00", "1", 
				"Pants");

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdPreviewCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "Products");
		ss = ss + 1;
		// add gift services to the products
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		myGiftServiceObjs.addToGiftBox( myShoppingBag.getTheFirstGiftLink(),
				testName, myEnvironment.getChromeBrowser());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheSecondGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		ss = ss + 1;
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "ProductsWithGiftServices");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;

		// continue to address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();

		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "GiftServicesAndShippingErrors");
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ss = ss + 1;
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));

		System.out.println(currentElement.getText());
		System.out.println(myShoppingBag.getTheShippingRestrictionText());
		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRestrictionText()));
		// String errorColor =
		// driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).GetCssValue("Color");
		String errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheCustomsGiftServiceErrorText()));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.CustomsGiftServiceErrorText));
		// Assert.True(driver.FindElement(By.CssSelector(myShoppingBagObjs.CanadaError)).Text.Contains(myShoppingBagObjs.TraditionalGiftCardCanadaErrorText));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstRowGiftServicesError()));
		System.out.println("This is the GiftServicesError:       "
				+ currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheSecondRowGiftServivesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheGiftServicesErrorText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
	/*	currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBagObjs.getTheLastRowGiftServicesError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBagObjs.getTheGiftServicesErrorText()));
		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myShoppingBagObjs.getTheThirdRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBagObjs.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));
		currentElement = myEnvironment
				.waitForDynamicElement( By.xpath(myShoppingBagObjs
						.getTheFourthRowShippingRemovalError()));
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBagObjs.getTheShippingRemovalText()));
		errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

		// checks for the yellow outline
		myShoppingBagObjs.checkProductsForYellowOutline(driver, myEnvironment,
				7);*/

		// see that the shipping restriction message is present in the shopping
		// bag
		/*
		 * currentElement = myEnvironment.waitForDynamicElement(
		 * By.xpath(myShoppingBagObjs.getTheFirstShippingRestriction()));
		 * System.
		 * out.println(currentElement.findElement(By.tagName("a")).getText());
		 * Assert.assertTrue(currentElement.getText().contains(
		 * "Shipping restrictions apply"));
		 * Assert.assertTrue(currentElement.findElement
		 * (By.tagName("a")).getText().contains("DETAILS")); currentElement =
		 * myEnvironment.waitForDynamicElement(
		 * By.xpath(myShoppingBagObjs.getTheSecondShippingRestriction()));
		 * System
		 * .out.println(currentElement.findElement(By.tagName("a")).getText());
		 * Assert.assertTrue(currentElement.getText().contains(
		 * "Shipping restrictions apply"));
		 * Assert.assertTrue(currentElement.findElement
		 * (By.tagName("a")).getText().contains("DETAILS")); currentElement =
		 * myEnvironment.waitForDynamicElement(
		 * By.xpath(myShoppingBagObjs.getTheThirdShippingRestriction()));
		 * System.
		 * out.println(currentElement.findElement(By.tagName("a")).getText());
		 * Assert.assertTrue(currentElement.getText().contains(
		 * "Shipping restrictions apply"));
		 * Assert.assertTrue(currentElement.findElement
		 * (By.tagName("a")).getText().contains("DETAILS"));
		 */
		// remove all the products that cannot be shipped to Canada
		myShoppingBag.removeProducts(2);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));
		currentElement.click();

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "AddressPage", "PreFilled");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		// removes the gift services from remaining products
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(this.EditGiftServices));
		currentElement.click();
		Thread.sleep(3000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		Thread.sleep(3000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		// currentElement.click(); Thread.sleep(2000);
		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
		Thread.sleep(2000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		// click the first image to add more qty
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheFirstImage()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myProductDetailObjs.getTheMainImage()));
		myProductDetailObjs.addQuantityToBag("8");

		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "Normal");

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);
		
		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));

		// check to see if the url is secure on the address page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		currentElement = myEnvironment.fluentWait(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);

		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheBottomCCBtn()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "PaymentPage", "");
		ss = ss + 1;
		
	/*	ShippingPrice = "";
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));

		// Checks to see if the shipping price is correct on the payment page.
		
		ShippingPrice = currentElement.getText();
		ShippingPrice = ShippingPrice.substring(1);*/
		// TheShippingPrice = Double.parseDouble(ShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPaymentObjs.getTheActualCanadaShippingPrice());

		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// use the giftcard for partial payment
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardNumber());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardPinInput()));
		currentElement.sendKeys(myPaymentPage.getTheGiftCardPin());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myPaymentPage.getTheByIdGiftCardBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()));
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPaymentPage.getTheGiftCardPartialPayment()));
		// String giftCardPPColor = currentElement.getCssValue("Color");
		System.out.println(currentElement.getText().trim());
		String thePartialPayment = currentElement.getText().trim();
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "PaymentPage", "GiftCardPayment");

		Assert.assertTrue(thePartialPayment.contains("-$25.00"));
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;
		// waits for preview page to load
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheTotalTax()));
		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "PreviewPage", "");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = currentElement.getText();
		// String theDuty =
		// driver.findElement(By.id(myPreviewObjs.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheSplitTenderSubTotal())).getText();
		String theShipping = driver
				.findElement(By.xpath(myPreviewObjs.getTheShippingAmount()))
				.getText().trim();
		thePartialPayment = driver
				.findElement(By.xpath(myPreviewObjs.getTheGCPartialPayment()))
				.getText().trim();
		String theGC = driver.findElement(By.xpath(myPreviewObjs.getTheUSGC()))
				.getText();
		System.out.println(theShipping);
		System.out.println(thePartialPayment);

		// Assert.True(thePartialPayment.Contains("GIFT CARD:"));
		Assert.assertTrue(thePartialPayment.contains("$25.00"));
		System.out.println(thePartialPayment);
		Assert.assertTrue(theShipping.contains("$16.00"));
		System.out.println(theShipping);
		System.out.println(theGC);
		System.out.println(myPaymentPage.getTheGiftCardNumber().trim());
		Assert.assertTrue(theGC.contains(myPaymentPage.getTheGiftCardNumber()
				.trim()));

		// Submit the order
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));

		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(currentElement.getText());
		Assert.assertTrue(currentElement.getText().contains(
				"ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ConfirmationPage", "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		OrderNumber = currentElement.getText();
		OrderNumber = OrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		Assert.assertTrue("$16.00".contains(driver
				.findElement(
						By.xpath(myConfirmationObjs.getTheShippingAmount()))
				.getText().trim()));

		// Insert a comma for how totals appear in contact center and OG
		

		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, OrderNumber, testName,
				myEnvironment.getChromeBrowser());
		
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		
		ss = ss + 1;
		
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				OrderNumber, testName, myEnvironment.getChromeBrowser());
		
		
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		System.out.println(theTotal);
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		Double theGCTotal = 0.0;
		theGCTotal = myPaymentPage.pullPrice(myPayTechObjs
				.getTheGCAddedAmount());
		theDTotal = theDTotal - theGCTotal;
		String newTotal = "";
		
		
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ss = ss + 1;
		// Adds money back to the gift card so the script is ready to run again
		myPayTechObjs.addMoneyToCard(
				myPaymentPage.getTheGiftCardNumber(),
				myPaymentPage.getTheGiftCardPin(),
				myPaymentPage.getTheGiftCardAddAmount());
		ss = ss + 1;
		// Go to home page
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));

		
		bw.write("<html><body><center><h2>Canada Split Tender Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The Canada Split Tender Order Checks for different alerts that notify the user that certain functionality is restricted"
				+ " and makes sure these restrictions are properly displaying on the website when the current country"
				+ " is Canada. This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+
				
				myEnvironment.addSSToFile(testName, "AccountPage",
						"This is the Account Page.")
				+
									
				myEnvironment.addSSToFile(testName, "HomePageCanada",
						"This is the Canada Home Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PDPTraditionalGC",
						"This is the GiftCard PDP.")
				+
				
					myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName()  + myProducts.getTheHighPantsPrice(),
						"This is the Pants PLP.")
				+
			
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheHighPantsPrice(),
						"This is the Pants PDP.")
				+
				
			myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + "GolfBag",
						"This is the Golf PLP.")
				+
				
										
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag(),
						"This is the Golf PDP.")
				+
				

					myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + "Dinnerware",
						"This is the Dinnerware PLP.")
				+
				
			myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+
				
			myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheLowPantsPrice(),
						"This is the Pants PLP.")
				+

				
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheLowPantsPrice(),
						"This is the Pants PDP.")
				+

			myEnvironment.addSSToFile(testName, "ShoppingBagProducts",
						"This is the Shopping Bag with products.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalGiftBox",
						"This is the Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalAddToGiftBox1",
						"This is the Add To Gift Box Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "GiftServicesModalGiftMessage",
						"This is the Gift Message Modal.")
				+
				
				myEnvironment.addSSToFile(testName, "ShoppingBagProductsWithGiftServices",
						"This is the Shopping Bag containing Products with Gift Services.")
				+
				

				myEnvironment.addSSToFile(testName, "ShoppingBagGiftServicesAndShippingErrors",
						"This is the Shopping Bag containing Products with different types of error messages.")
				+

				myEnvironment.addSSToFile(testName, "AddressPagePreFilled",
						"This is the Address Page with pre populated fields.")
				+

			myEnvironment.addSSToFile(testName, "PaymentPage",
						"This is the Payment Page.")
				+

				myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
						"This is the Payment Page after Credit Card information entered.")
				+

			myEnvironment.addSSToFile(testName, "PreviewPage",
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ConfirmationPage",
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ContactCenter" + OrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, "PaymenTech" + OrderNumber,
						"This is the PaymenTech Page.")
			    +
			    
			
				myEnvironment.addSSToFile(testName, "PaymenTech" + myPaymentPage.getTheGiftCardNumber(),
						"This is the approved add money to gift card PaymenTech Page.")
				
				+
			    
				myEnvironment.getPageTestOutcome()
				
				+ "</center></body></html>");
		bw.close();


		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Split Tender Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Canada Split Tender Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada. This test completes a split tender order with a Credit Card and a Gift Card</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
		fns.add("SignInPageBlank");
		fns.add("AccountPage");
		fns.add("HomePageCanada");

		fns.add("ShoppingBagProducts");
		fns.add("GiftServicesModalGiftBox");
		fns.add("GiftServicesModalAddToGiftBox1");
		fns.add("GiftServicesModalGiftMessage");

		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("ShoppingBagGiftServicesAndShippingErrors");
		fns.add("AddressPagePreFilled");
		fns.add("PaymentPage");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + OrderNumber + "");
		fns.add("PaymenTech" + OrderNumber + "");
		fns.add("PaymenTech" + myPaymentPage.getTheGiftCardNumber());

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		driver.quit();
	}

}
