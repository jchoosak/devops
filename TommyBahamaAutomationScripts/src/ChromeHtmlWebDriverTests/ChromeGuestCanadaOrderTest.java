package ChromeHtmlWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;
import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;

public class ChromeGuestCanadaOrderTest {

	// this is the class where all enviromental details are kept: what site is
	// being
	// tested and any methods that interact with the driver object
	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private Product myProducts = new Product();

	private String theEnvironment = "";
	int ss = 0;
	double theShippingPrice = 16.00;
	String OrderNumber = "";
	String testName = "TheGuestCanadaOrderTest";
	// url to be checked on various pages
	private String url = "";
	// Main object in the test object is per browser and is used for all
	// interactivity with web page elements.
	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);	
		 
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\Chrome\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add("PLPDinnerware");
			fns.add( myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add(myProductListingObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
			fns.add("PLPShoes");
			fns.add(myProductDetailObjs.getThePageName() +  myProducts.getTheShoeProduct());
			fns.add("PLPGolfBag");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag());
			fns.add("GiftServicesModalGiftMessage");
			fns.add("GiftServicesModalGiftWrap");
			fns.add("GiftServicesModalGiftBox");
			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add("AddressPageGuest");
			fns.add("ShoppingBagOversizedShippingErrors");
			fns.add("ShoppingBagWithGiftServicesErrors");
			fns.add("PaymentPageCompleted");
			fns.add("PreviewPage");
			fns.add("ConfirmationPage");
			fns.add("ContactCenter" + OrderNumber + "");
			fns.add("PaymenTech" + OrderNumber + "");
			
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheGuestCanadaOrderTest() throws InterruptedException {
		WebElement currentElement;
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment.setTestSubject("Guest Canada Order Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// //adds a t-shirt to the shopping bag

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "1",  "Dinnerware");
		ss = ss + 1;
		ss = ss + 1;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathMensTShirts(), "$45.00", "1", 
				"T-Shirt");
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);

		// add shoes to the bag
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), myProducts.getTheShoeProduct(), "1", 
				"Shoes");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
	/*	home = driver
				.findElement(By.xpath(myHeaderObjs.getTheFooterEmailBtn()));
		js.executeScript(mouseOverScript, home);*/

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathGolf(), myProducts.getTheGolfBag(), "1",
				 "GolfBag");

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myHeaderObjs.getTheByIdPreviewCheckout()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// add gift services to the products
		myGiftServiceObjs.giftBox(myShoppingBag.getTheFourthGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		ss = ss + 1;
		Thread.sleep(3000);
		myGiftServiceObjs.giftBox(myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		ss = ss + 1;
		myGiftServiceObjs.giftBox(myShoppingBag.getTheFirstGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		ss = ss + 1;
		Thread.sleep(3000);
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBagProducts", "WithGiftServices");
		ss = ss + 1;
		driver.findElement(By.xpath(myShoppingBag.getTheContinueCheckout()))
				.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySIO.getTheContinueAsGuestBtn()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdNewEmail()));
		currentElement.sendKeys(myEnvironment.getTheEcommEmailFive());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdConfirmedEmail()));
		currentElement.sendKeys(myEnvironment.getTheEcommEmailFive());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdPhone1()));
		currentElement.sendKeys("253");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdPhone2()));
		currentElement.sendKeys("205");
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdPhone3()));
		currentElement.sendKeys("1750");

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingCountry()));
		currentElement = currentElement.findElement(By
				.cssSelector(myAddressObjs.getTheCanadaCountry()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingFName()));
		currentElement.sendKeys(myEnvironment.getTheUsShopperFName());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingMName()));
		currentElement.sendKeys(myEnvironment.getTheUsShopperMName());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingLName()));
		currentElement.sendKeys(myEnvironment.getTheUsShopperLName());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingAddress()));
		currentElement.sendKeys(myAddressObjs.getTheCanadaAddress());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingCity()));
		currentElement.sendKeys(myAddressObjs.getTheCanadaCity());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingState()));

		Select clickThis = new Select(currentElement);
		clickThis.selectByValue(myAddressObjs.getTheCanadaState());

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		currentElement.sendKeys(myAddressObjs.getTheCanadaZip());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement.click();
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "AddressPage", "Guest");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		// continue from address page to verify shipping errors occurr
		currentElement = myEnvironment.waitForDynamicElement(
				By.cssSelector(myShoppingBag.getTheByCSSCanadaError()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "OversizedShippingErrors");
		ss = ss + 1;
		System.out.println(currentElement.getText());

		// Make sure to put these checks back once merch is fixed.
		// checks to see all products give the correct error messages
		Assert.assertTrue(currentElement.getText().contains(
				myShoppingBag.getTheShippingRestrictionText()));
		String errorColor = currentElement.getCssValue("Color");
		System.out.println(errorColor);
		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

//		currentElement = myEnvironment
//				.waitForDynamicElement( By.xpath(myShoppingBagObjs
//						.getTheFourthRowShippingRemovalError()));
//		Assert.assertTrue(currentElement.getText().contains(
//				myShoppingBagObjs.getTheShippingRemovalText()));
//		errorColor = currentElement.getCssValue("Color");
//		System.out.println(errorColor);
//		Assert.assertTrue(errorColor.contains("rgb(168, 49, 39)"));

		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheThirdRemoveLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheThirdRemoveLink()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheContinueCheckout()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySIO.getByXPathSignInButton()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySIO.getTheContinueAsGuestBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));

		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "WithGiftServicesErrors");
		ss = ss + 1;
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()));

		// removes the gift services from remaining products
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = driver.findElement(By.xpath(myShoppingBag
				.getTheEditGiftServices()));

		currentElement.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myGiftServiceObjs.getTheByXPathRemoveGiftServices()));
		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myShoppingBag.getTheBottomCheckoutBtn()));
		currentElement.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(mySIO.getTheContinueAsGuestBtn()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText("CONTINUE AS A GUEST"));

		currentElement.click();

		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingCountry()));
		currentElement = currentElement.findElement(By
				.cssSelector(myAddressObjs.getTheCanadaCountry()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
//		currentElement = myEnvironment.waitForDynamicElement(
//				By.xpath(myPaymentPage.getTheBottomCCBtn()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());

		// * This is commented out because currently we are under a month
		// * of free shipping, will uncomment when shipping is returned to
		// normal for US orders.

		// Checks to see if the shipping price is correct on the payment page.
		String ShippingPrice = "";
		ShippingPrice = driver.findElement(
				By.xpath(myPaymentPage.getTheByXPathCanadaShippingPrice()))
				.getText();
		ShippingPrice = ShippingPrice.substring(1);
		theShippingPrice = Double.parseDouble(ShippingPrice);
		Assert.assertTrue(theShippingPrice == myPaymentPage
				.getTheActualCanadaShippingPrice());

		// Checks to see that there is no other shipping options present
		Assert.assertFalse(myEnvironment.isElementPresent(
				By.xpath(myPaymentPage.getTheByXPathTwoDayShipping())));

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		// waits for preview page to load
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheSubTotal()));
		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "Preview", "Page");
		ss = ss + 1;
		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalTax())).getText();
		//String theDuty = driver.findElement(
		//		By.id(myPreviewObjs.getTheByIdDuty())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewObjs.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));

		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText());
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationObjs.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "Confirmation", "Page");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		OrderNumber = driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText();
		OrderNumber = OrderNumber.substring(27);

		// Make sure that the Shipping amount is correct on preview and
		// confirmation page
		Assert.assertTrue("$16.00".contains(driver
				.findElement(
						By.id(myConfirmationObjs.getTheByIdTotalShipping()))
				.getText().trim()));

		// Insert a comma for how totals appear in contact center and OG
		

		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, OrderNumber, testName,
				myEnvironment.getChromeBrowser());
		
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);
		
		ss = ss + 1;
		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		String newTotal = "";
		
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				OrderNumber, testName, myEnvironment.getChromeBrowser());
		
		newTotal = myPayTechObjs
				.formatTotal( newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		myPayTechObjs
				.checkOrderInPayTech( theZip, newTotal);
		ss = ss + 1;
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>Guest Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Guest Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ myEnvironment.getTestTextDescription()
						+ myEnvironment.getBrowser()
						+ "\\"
						+ testName
						+ ".jpeg</b></p></td></tr></table><br/>"
						+

						myEnvironment.addSSToFile(testName, "PLPDinnerware",
								"This is the Dinnerware PLP.")
						+
						
					myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct(),
								"This is the Dinnerware PDP.")
						+
						
										myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() +  myProducts.getTheMidTShirtPrice(),
								"This is the T-Shirt PLP.")
						+

						myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheMidTShirtPrice(),
								"This is the T-Shirt PDP.")
						+
						
								myEnvironment.addSSToFile(testName, "PLPShoes",
								"This is the Shoes & Sandals PLP.")
						+

						myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() +myProducts.getTheShoeProduct(),
								"This is the Sandals PDP.")
						+
						
							myEnvironment.addSSToFile(testName, "PLPGolfBag",
						"This is the Golf PLP.")
				+		
				
				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheGolfBag(),
						"This is the Golf PDP.")
						
						+

			  		myEnvironment.addSSToFile(testName, "GiftServicesModalGiftMessage",
								"This is the Gift Message Modal.")
						+

					myEnvironment.addSSToFile(testName, "GiftServicesModalGiftBox",
								"This is the Gift Box Modal.")
						+

					myEnvironment.addSSToFile(testName, "GiftServicesModalGiftWrap",
								"This is the Shopping Bag with the Gift Wrap Modal.")
						+

					myEnvironment.addSSToFile(testName, "ShoppingBagProductsWithGiftServices",
								"This is the Shopping Bag containing Products with Gift Services.")
						+
						
						myEnvironment.addSSToFile(testName, "AddressPageGuest",
								"This is the Address Page for a Guest User.")
						+
						
						myEnvironment.addSSToFile(testName, "ShoppingBagOversizedShippingErrors",
								"This is the Shopping Bag with Oversized and other Shipping Errors Displayed.")
						+

						myEnvironment.addSSToFile(testName, "ShoppingBagWithGiftServicesErrors",
								"This is the Shopping Bag containing Products with Gift Services error messages.")
						+

					myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
								"This is the Payment Page after Credit Card information entered.")
						+

					myEnvironment.addSSToFile(testName, "PreviewPage",
								"This is the Preview Page.")
						+
						
						myEnvironment.addSSToFile(testName, "ConfirmationPage",
								"This is the Confirmation Page.")
						+
						
						myEnvironment.addSSToFile(testName, "ContactCenter" + OrderNumber,
								"This is the Contact Center Page.")
						+
						
						myEnvironment.addSSToFile(testName, "PaymenTech" + OrderNumber,
								"This is the PaymenTech Page.")
						+
					myEnvironment.getPageTestOutcome()
						+ "</center></body></html>");
		bw.close();
	

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Guest Canada Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The Guest Canada Order Checks for different alerts that notify the user that certain functionality is restricted"
						+ " and makes sure these restrictions are properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
	
	/*	fns.add("PLPDinnerware");
		fns.add( myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
		fns.add(myProductListingObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheMidTShirtPrice());
		
		fns.add("GiftServicesModalGiftMessage");
		fns.add("GiftServicesModalGiftWrap");
		fns.add("GiftServicesModalGiftBox");
		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("AddressPageGuest");
		fns.add("ShoppingBagOversizedShippingErrors");
		fns.add("ShoppingBagWithGiftServicesErrors");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + OrderNumber + "");
		fns.add("PaymenTech" + OrderNumber + "");*/

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}
