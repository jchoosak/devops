package ChromeHtmlWebDriverTests;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.FiftyOne;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ChromeCanadaRestrictionMessageTest {

	private SeleniumEnvironment myEnvironment;
	// The following are the different objects that make up the Tommy Bahama
	// repository that are used throught this scipt.
	private SignInPage mySIO;;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private AccountPage myAccountObjs;
	private FiftyOne myFiftyOneObjs = new FiftyOne();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
	private Product myProducts = new Product();
	private String testName = "TheCanadaCheckRestrictionTest";

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private WebDriver driver;
	private int ss = 0;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		myHeaderObjs = new Header(driver, myEnvironment);
		 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
		 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
				 myHeaderObjs, testName);
		 myAccountObjs = new AccountPage(myEnvironment);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
	}

	@Test
	public void TheCanadaCheckRestrictionTest() throws InterruptedException {
		WebElement currentElement;
		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());

		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		// driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// myEnvironment.waitForPageLoaded();

		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment.setTestSubject("Canada Restriction Message Test Results");
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		// check to see if the proper ad is displayed in header
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.xpath(myHeaderObjs.getTheCountryFlag()));
		// url = driver.getCurrentUrl();
		// myHeaderObjs.checkFlagInHeader(driver, myEnvironment, url,
		// By.xpath(myHeaderObjs.getTheCountryFlag()),
		// myHeaderObjs.getTheCanadaShippingPromoBanner());
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheCanadaFlag(), testName);
		// Signe into an canada account
		mySIO.signIn(mySIO.getTheCanadaUsername(), mySIO.getThePassword());
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		ss = ss + 1;

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MyAccountPage", "");
		ss = ss + 1;
		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();

		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForPageLoaded();
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "HomePage", "");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathHomeDecorTab()));
		currentElement.click();
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		JavascriptExecutor js = (JavascriptExecutor) driver;

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		WebElement home = driver.findElement(By.xpath(myHeaderObjs
				.getTheByXPathSearchInput()));
		js.executeScript(mouseOverScript, home);

		// check restriction message on diffuser product
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHomeDecorObjs.getTheByLinkDiffusers()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myProductListingObjs.getTheByXPathDiffuser()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myProductListingObjs.getThePageName(), "DiffuserShippingRestriction");
		ss = ss + 1;

		myProductListingObjs.checkRestrictionText(myProducts.getTheDiffusor());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myProductDetailObjs.getThePageName(), "DiffuserShippingRestriction");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathHomeDecorTab()));
		currentElement.click();
		myEnvironment.waitForTitle("Men's");

		// here i have to move the virtual mouse to the tommy bahama logo so it
		// does not activate the hover
		// menu because this confuses selenium and it will sometimes select the
		// incorrect link
		home = driver.findElement(By.xpath(myHeaderObjs
				.getTheByXPathSearchInput()));
		js.executeScript(mouseOverScript, home);

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myMenObjs.getTheByXPathGolf()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myProductListingObjs.getThePageName(), "GolfBagShippingRestriction");
		ss = ss + 1;
		myProductListingObjs.checkRestrictionText(myProducts.getTheGolfBag());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myProductDetailObjs.getThePageName(), "GolfBagShippingRestriction");
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathMensTab()));
		currentElement.click();

		// Check Credit restriction message
		currentElement = myEnvironment.fluentWait(
				By.xpath(myMenObjs.getTheByXPathCreditCardsLink()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myTraditionalCardObjs
						.getTheByXPathTraditionalCardImageLink()));
		String linkUrl = currentElement.getAttribute("href");

		// there is a problem with these link because they are stored in a sort
		// of map, to get past his I just grab the
		// link in the link and have the driver navigate to it. Kind of a hack
		// but it seems the best until selenium supports
		// how these links are stored.
		driver.get(linkUrl);
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myTraditionalCardObjs.getTheByXPathCardLink()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement( By
				.xpath(myTraditionalCardObjs
						.getTheByXPathShippingRestrictions()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myProductDetailObjs.getThePageName(), "GiftCardShippingRestriction");
		ss = ss + 1;
		Assert.assertTrue(currentElement.getText().contains(
				"Shipping restrictions apply"));
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myTraditionalCardObjs.getTheTraditionalCardLink()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myProductListingObjs.getThePageName(), "GiftCardShippingRestriction");
		ss = ss + 1;
		myProductListingObjs.checkAllTraditionalCards();

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		myEnvironment.waitForPageLoaded();

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));

		Thread.sleep(1000);
		// Go Back to United States
		myHeaderObjs.selectCountry(driver, myEnvironment, myFiftyOneObjs,
				myFiftyOneObjs.getTheUnitedStatesFlag(), testName);
		myEnvironment.waitForPageLoaded();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
	}

	@After
	public void quitDriver() throws MessagingException,
			UnsupportedEncodingException {
		myEnvironment.setNetworkFile("file:///"
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Canada Restriction Message Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\" >The Canada Restriction Message test is testing that Canada restriction text"
						+ " is properly displaying on the website when the current country"
						+ " is Canada.</h3><table style= \"width:70%;\"><tr><td><p> "
						+ myEnvironment.getTestTextDescription()
						+ myEnvironment.getBrowser()
						+ "\\"
						+ testName
						+ ".jpeg</b></p></td></tr></table><br/>"
						+

//						myEnvironment.addSS(testName, "SignInPageBlank",
//								"This is the blank Sign-In Page.")
//						+
//
//					myEnvironment.addSS(testName, "MyAccountPage",
//								"This is the Account Page.")
//						+
//											
//						myEnvironment.addSS(testName, "HomePage",
//								"This is the Home Page.")
//						+

						myEnvironment.addSS(testName, "PLPDiffuserShippingRestriction",
								"This is the Diffusor PLP with shipping restriction text.")
						+
						
						myEnvironment.addSS(testName, "PDPDiffuserShippingRestriction",
								"This is the Diffusor PDP with shipping restriction text.")
						+
						
						myEnvironment.addSS(testName, "PLPGolfBagShippingRestriction",
								"This is the Golf PLP with shipping restriction text.")
						+
						
						myEnvironment.addSS(testName, "PDPGolfBagShippingRestriction",
								"This is the Golf PDP with shipping restriction text.")
						+
						
//						myEnvironment.addSS(testName, "PDPGiftCardShippingRestriction",
//								"This is the Gift Card PDP with shipping restriction text.")
//						+

						
						myEnvironment.addSS(testName, "PLPGiftCardShippingRestriction",
								"This is the Gift Card PLP with shipping restriction text.")
						+

						"If you were able to see all images then the test passed."
						+ "</center></body></html>", "text/html");

		List<String> fns = new ArrayList<String>();
	//	fns.add("SignInPageBlank");
	//	fns.add("MyAccountPage");
		//fns.add("HomePage");
		fns.add("PLPDiffuserShippingRestriction");
		fns.add("PDPDiffuserShippingRestriction");
		fns.add("PLPGolfBagShippingRestriction");
		fns.add("PDPGolfBagShippingRestriction");
	//	fns.add("PDPGiftCardShippingRestriction");
		fns.add("PLPGiftCardShippingRestriction");
		
		ss = ss - 4;

		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);
		driver.quit();
	}
}