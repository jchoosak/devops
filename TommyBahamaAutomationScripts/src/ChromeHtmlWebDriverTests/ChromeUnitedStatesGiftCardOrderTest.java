package ChromeHtmlWebDriverTests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import TommyBahamaRepository.AccountPage;
import TommyBahamaRepository.AddressPage;
import TommyBahamaRepository.ConfirmationPage;
import TommyBahamaRepository.ContactCenter;

import TommyBahamaRepository.GiftService;
import TommyBahamaRepository.Header;
import TommyBahamaRepository.HomeDecor;
import TommyBahamaRepository.Men;
import TommyBahamaRepository.PaymentPage;
import TommyBahamaRepository.PayTech;
import TommyBahamaRepository.PreviewPage;
import TommyBahamaRepository.Product;
import TommyBahamaRepository.ProductDetailPage;
import TommyBahamaRepository.ProductListingPage;
import TommyBahamaRepository.SeleniumEnvironment;

import TommyBahamaRepository.ShoppingBagPage;
import TommyBahamaRepository.SignInPage;
import TommyBahamaRepository.TraditionalGiftCard;

@RunWith(JUnit4.class)
public class ChromeUnitedStatesGiftCardOrderTest {

	private SeleniumEnvironment myEnvironment;
	private SignInPage mySIO;
	private Header myHeaderObjs;
	private Men myMenObjs = new Men();
	private ProductListingPage myProductListingObjs;
	private ProductDetailPage myProductDetailObjs;
	private ShoppingBagPage myShoppingBag;
	private GiftService myGiftServiceObjs;
	private AddressPage myAddressObjs = new AddressPage();
	private PaymentPage myPaymentPage;
	private PreviewPage myPreviewObjs = new PreviewPage();
	private ConfirmationPage myConfirmationObjs = new ConfirmationPage();
	private AccountPage myAccountObjs;
	private PayTech myPayTechObjs;
	private ContactCenter myContactCenterObjs = new ContactCenter();
	private HomeDecor myHomeDecorObjs = new HomeDecor();
	private TraditionalGiftCard myTraditionalCardObjs = new TraditionalGiftCard();
    private Product myProducts = new Product();
	private String theEnvironment = "";
	private String url = "";
	private String theOrderNumber = "";
	private final String testName = "TheUSGiftCardOrderTest";
	private int ss = 0;

	private WebDriver driver;

	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		// baseUrl = System.getProperty("webdriver.base.url");
		Assert.assertTrue(isSupportedPlatform());
		File file = new File("C:\\Users\\jwest\\Desktop\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		myEnvironment = new SeleniumEnvironment(driver);
		 myAccountObjs = new AccountPage(myEnvironment);
		 myShoppingBag = new ShoppingBagPage(driver, myEnvironment);
		 myGiftServiceObjs = new GiftService(driver, myEnvironment, myShoppingBag);
			myHeaderObjs = new Header(driver, myEnvironment);
			 myProductListingObjs = new ProductListingPage(driver, myEnvironment);
			 myProductDetailObjs = new ProductDetailPage(driver, myEnvironment, myProductListingObjs, 
					 myHeaderObjs, testName);
		 myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		 myPaymentPage = new PaymentPage(myEnvironment, testName);
		 mySIO = new SignInPage(driver, myEnvironment, myHeaderObjs, testName);
			
			List<String> fns = new ArrayList<String>();
			fns.add("SignInPageBlank");
			fns.add("MyAccountPage");
			fns.add("HomePage");
			fns.add( myProductListingObjs.getThePageName() +  myProducts.getTheLowTShirtPrice());
			fns.add( myProductDetailObjs.getThePageName() +  myProducts.getTheLowTShirtPrice());
			fns.add("PLPDinnerware");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
			fns.add("PLPShoes");
			fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct());
			fns.add("PDPTraditionalGC");
			fns.add("GiftServicesModalGiftMessage");

			fns.add("ShoppingBagProductsWithGiftServices");
			fns.add("AddressPagePreFilled");

			fns.add("PaymentPageGiftCardBalenceModal");
			fns.add("PaymentPageCompleted");
			fns.add("PreviewPage");
			fns.add("ConfirmationPage");
			fns.add("ContactCenter" + theOrderNumber + "");
			fns.add("PaymenTech" + theOrderNumber + "");
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\Chrome\\" + testName;
			myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void TheUSGiftCardOrderTest() throws InterruptedException,
			IOException {

		WebElement currentElement;

		// Navigate to the testing environment
		driver.get(myEnvironment.getTheTestingEnvironment());
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
		myEnvironment
				.setTestSubject("The United States Gift Card Order Test Results");
		// set implicit wait times so pages do not load due to all the crap that
		// is added by thrid parties
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Sign into an Washington State account
		mySIO.signIn(
				mySIO.getTheWAGCUserName(), mySIO.getThePassword());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myAccountObjs.getThePersonalInfoEditLink()));
		ss = ss + 1;

		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "MyAccountPage", "");
		ss = ss + 1;

		// Go to home page
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		currentElement.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheByXPathLogo()));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "HomePage", "");
		ss = ss + 1;

		myProductDetailObjs.selectProductByPrice(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathMensTShirts(), "$38.00", "1", 
				"T-Shirt");

		ss = ss + 1;
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathHomeDecorTab(),
				myHomeDecorObjs.getTheByXPathDinnerware(),
				myProducts.getTheDinnerWareProduct(), "2", "DinnerWare");
		ss = ss + 1;
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		// add shoes to the bag
		myProductDetailObjs.selectProduct(
				myHeaderObjs.getTheByXPathMensTab(),
				myMenObjs.getTheByXPathShoes(), myProducts.getTheShoeProduct(), "1", 
				"Shoes");
		ss = ss + 1;
		ss = ss + 1;

		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myHeaderObjs.getTheFooterEmailBtn()));

		myTraditionalCardObjs.addTraditionalCard(myEnvironment, driver,
				myHeaderObjs, myMenObjs, testName, "PDPTraditionalGC", "");
		ss = ss + 1;

		// continue to shopping bag
		currentElement = myEnvironment.waitForDynamicElement(
				By.linkText(myHeaderObjs.getTheByLinkCheckout()));
		currentElement.click();
		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));
		// myEnvironment.TakeScreenShot(
		// myEnvironment.getChromeBrowser(), testName, "ShoppingBag", "");
		// url = driver.Url;

		// add gift services to the products
		myGiftServiceObjs.giftBox(
				myShoppingBag.getTheThirdGiftLink(), testName,
				myEnvironment.getChromeBrowser());
		// myGiftServiceObjs.giftBox(driver, currentElement, myEnvironment,
		// myShoppingBagObjs, myShoppingBagObjs.ThirdGiftLink, testName);
		ss = ss + 1;

		currentElement = myEnvironment
				.waitForDynamicElement( By.cssSelector(myShoppingBag
						.getTheByCSSContinueShopping()));

		// Test for all of the columns are present in the shopping bag
		myShoppingBag.checkShoppingBagColumns(driver);

		// Test that thumbnails are present for the items in the shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstImage())));

		// Test that the information details are present for the items in the
		// shopping bag
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheFirstItemDetails())));

		// Test that information details are showing for the remaining items.
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheSecondItemDetails())));
		Assert.assertTrue(myEnvironment.isElementPresent(
				By.xpath(myShoppingBag.getTheThirdItemDetails())));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ShoppingBag", "ProductsWithGiftServices");
		ss = ss + 1;

		// check to see if the url is secure on the address page
		driver.findElement(
				By.xpath(myShoppingBag.getTheBottomContinueCheckout()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdSameAddress()));
		url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(myAddressObjs.getTheURL()));

		// Shipping address same as billing
		driver.findElement(By.id(myAddressObjs.getTheByIdSameAddress()))
				.click();
		currentElement = myEnvironment.waitForDynamicElement(
				By.id(myAddressObjs.getTheByIdBillingZipCode()));
		String theZip = currentElement.getAttribute("value");
		System.out.println("This is the Zip on the Address page:         "
				+ theZip);
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, myAddressObjs.getThePageName(), "PreFilled");
		ss = ss + 1;

		// check to see if the url is secure on the payment page
		driver.findElement(
				By.id(myAddressObjs.getTheByIdTopContinueCheckoutBtn()))
				.click();
		// myEnvironment.myWaitForWDElement(driver,
		// By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()));
		myHeaderObjs.checkUrl(myPaymentPage.getTheURL(),
				url = driver.getCurrentUrl());
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath(myPaymentPage.getTheNextDaySatShipping()));
		currentElement.click();

		/*
		 * This is commented out because currently we are under a month of free
		 * shipping, will uncomment when shipping is returned to normal for US
		 * orders.
		 */
		// Checks to see if the shipping price is correct on the payment page.
		// ShippingPrice = "";
		// ShippingPrice =
		// driver.FindElement(By.XPath(myPaymentObjs.GroundShipping)).getTheText();
		// ShippingPrice = ShippingPrice.Remove(0, 1);
		// Double.TryParse(ShippingPrice, out TheShippingPrice);
		// Assert.assertEquals(TheShippingPrice,
		// myPaymentObjs.ActualCanadaShippingPrice);

		// Checks to see that there is no other shipping options present
		// Assert.assertTrue(myEnvironment.IsElementPresent(By.XPath(myPaymentObjs.getTheByXPathTwoDayShipping()),
		// driver));

		// see that Card Services are working
		myPaymentPage.checkGcBalence();
		ss = ss + 1;

		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPaymentObjs.getTheByIdCardBalenceLink()()));
		// currentElement.Click();
		// Thread.Sleep(myEnvironment.getThe_Default_Sleep());
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPaymentObjs.getTheByIdgetTheGiftCardNumber()Input()));
		// currentElement.SendKeys(myPaymentObjs.getTheGiftCardNumber());
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPaymentObjs.getTheByIdgetTheByIdCheckCardBalanceBtn()()));
		// currentElement.Click();
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.Id(myPaymentObjs.CardBalence));
		// String theBalence = currentElement.getTheText();
		// myEnvironment.TakeScreenShot(
		// myEnvironment.getChromeBrowser(), testName, "PaymentPage",
		// "GiftCardBalence");
		// currentElement = myEnvironment.waitForDynamicElement(
		// By.LinkText(myPaymentObjs.XLink));
		// currentElement.Click();

		// Enter CC info for payment page
		myPaymentPage.enterCCInfo();
		ss = ss + 1;

		myEnvironment.waitForDocumentReadyState();
		// waits for preview page to load
		myEnvironment.waitForDynamicElement(
				By.xpath(myPreviewObjs.getTheTotalTax()));
		myHeaderObjs.checkUrl(myPreviewObjs.getTheURL(),
				url = driver.getCurrentUrl());
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "PreviewPage", "");
		ss = ss + 1;

		// check to see if the tax is what it should be for a Canada order
		String theTax = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalTax())).getText();
		String theTotal = driver.findElement(
				By.xpath(myPreviewObjs.getTheTotalAmount())).getText();
		// String theShipping =
		// driver.findElement(By.xpath(myPreviewObjs.getTheShippingAmount())).getText().trim();

		// Submit the order
		driver.findElement(
				By.cssSelector(myPreviewObjs.getTheByCSSSubmitButton()))
				.click();
		myEnvironment.waitForDynamicElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber()));
		myHeaderObjs.checkUrl(myConfirmationObjs.getTheURL(),
				url = driver.getCurrentUrl());
		System.out.println(driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText());
		// myHeaderObjs.checkCanadaHeader(driver, myEnvironment, url);
		Assert.assertTrue(driver
				.findElement(By.xpath(myConfirmationObjs.getTheOrderNumber()))
				.getText().contains("ORDER CONFIRMATION NUMBER:"));
		myEnvironment.TakeScreenShot( myEnvironment.getChromeBrowser(),
				testName, "ConfirmationPage", "");
		ss = ss + 1;
		// Store the Order number for CC and for Paytech

		theOrderNumber = driver.findElement(
				By.xpath(myConfirmationObjs.getTheOrderNumber())).getText();
		theOrderNumber = theOrderNumber.substring(27);

		// Insert a comma for how totals appear in contact center and OG
		// Insert a comma for how totals appear in contact center and OG
		

		myContactCenterObjs = new ContactCenter(driver, myEnvironment,
				theEnvironment, theTax, theOrderNumber, testName,
				myEnvironment.getChromeBrowser());
		
		String theContactTotal = myContactCenterObjs.getContactTotal(theTotal);
		// runs the script to look up the order in Contact Center
		myContactCenterObjs.checkOrderInCC(theContactTotal);

		// remove the $ sign because money values in Contact center do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);

		String newTotal = "";
		myPayTechObjs = new PayTech(driver, myEnvironment,  
				theOrderNumber, testName, myEnvironment.getChromeBrowser());
		
		
		newTotal = myPayTechObjs
				.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		// runs the script to look up the order in Paytech.
		
		
		myPayTechObjs
				.checkOrderInPayTech(newTotal);
		ss = ss + 1;
		// driver.close();
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws InterruptedException, MessagingException,
			IOException {

		myEnvironment.setNetworkFile(""
				+ myEnvironment.getNetworkTestDirectory()
				+ myEnvironment.getBrowser() + "\\" + this.testName);
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
				 + "\\" + this.testName + ".html"));
		
		bw.write("<html><body><center><h2>United States Gift Card Order Test Results</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3 style= \"width:70%;\">The United States Gift Card Order tests that products like a Traditional Gift Card can be added to the Shopping Bag and processed through the entire order process. "
				+ "</h3><table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ testName
				+ ".jpeg</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName, "SignInPageBlank",
						"This is the blank Sign-In Page.")
				+

				myEnvironment.addSSToFile(testName, "MyAccountPage",
						"This is the Account Page.")
				+

				myEnvironment.addSSToFile(testName, "HomePage",
						"This is the Home Page.")
				+

				myEnvironment.addSSToFile(testName, myProductListingObjs.getThePageName() + myProducts.getTheLowTShirtPrice(),
						"This is the T-Shirt PLP.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() +  myProducts.getTheLowTShirtPrice(),
						"This is the T-Shirt PDP.")
				+

				myEnvironment.addSSToFile(testName, "PLPDinnerware",
						"This is the Dinnerware PLP.")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName()
						+ myProducts.getTheDinnerWareProduct(),
						"This is the Dinnerware PDP.")
				+

				myEnvironment.addSSToFile(testName, "PLPShoes",
						"This is the Shoes & Sandals PLP..")
				+

				myEnvironment.addSSToFile(testName, myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct(),
						"This is the Shoes & Sandals PDP.")
				+

				myEnvironment.addSSToFile(testName, "PDPTraditionalGC",
						"This is the GiftCard PDP.")
				+

				myEnvironment.addSSToFile(testName,
						"GiftServicesModalGiftMessage",
						"This is the Gift Message Modal.")
				+

				myEnvironment
						.addSSToFile(testName,
								"ShoppingBagProductsWithGiftServices",
								"This is the Shopping Bag containing Products with Gift Services.")
				+

			myEnvironment.addSSToFile(testName, "AddressPagePreFilled",
						"This is the Address Page with pre populated fields.")
				+

				myEnvironment.addSSToFile(testName, "PaymentPageGiftCardBalenceModal",
						"This is the Gift Card Balence Modal.")
				+
				
			myEnvironment.addSSToFile(testName, "PaymentPageCompleted",
						"This is the Payment Page after Credit Card information entered.")
				+

			
			myEnvironment.addSSToFile(testName, "PreviewPage",
						"This is the Preview Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ConfirmationPage",
						"This is the Confirmation Page.")
				+
				
				myEnvironment.addSSToFile(testName, "ContactCenter" + theOrderNumber,
						"This is the Contact Center Page.")
				+
				
				myEnvironment.addSSToFile(testName, myPayTechObjs.getThePageName() + theOrderNumber,
						"This is the PaymenTech Page.")
			    +
			    
            	myEnvironment.getPageTestOutcome()
            	
				+ "</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>United States Gift Card Order Test Results</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3 style= \"width:70%;\">The United States Gift Card Order tests that products like a Traditional Gift Card can be added to the Shopping Bag and processed through the entire order process. "
						+ "</h3><table style= \"width:70%;\"><tr><td><p> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");
		
		
		List<String> fns = new ArrayList<String>();
		fns.add("SignInPageBlank");
		fns.add("MyAccountPage");
		fns.add("HomePage");
		fns.add( myProductListingObjs.getThePageName() +  myProducts.getTheLowTShirtPrice());
		fns.add( myProductDetailObjs.getThePageName() +  myProducts.getTheLowTShirtPrice());
		fns.add("PLPDinnerware");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheDinnerWareProduct());
		fns.add("PLPShoes");
		fns.add(myProductDetailObjs.getThePageName() + myProducts.getTheShoeProduct());
		fns.add("PDPTraditionalGC");
		fns.add("GiftServicesModalGiftMessage");

		fns.add("ShoppingBagProductsWithGiftServices");
		fns.add("AddressPagePreFilled");

		fns.add("PaymentPageGiftCardBalenceModal");
		fns.add("PaymentPageCompleted");
		fns.add("PreviewPage");
		fns.add("ConfirmationPage");
		fns.add("ContactCenter" + theOrderNumber + "");
		fns.add(myPayTechObjs.getThePageName() + theOrderNumber + "");

		ss = ss + 1;
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);

		System.out.println("This is the total number of SS		" + ss);
		driver.quit();
	}
}
