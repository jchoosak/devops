package SeleniumCookbook.tommyBahama.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import TommyBahamaOutletRepository.OutletHMC;
import TommyBahamaOutletRepository.OutletHmcLeftNav;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;

public class Astart {
	 protected WebDriver driver;
	 private StringBuffer verificationErrors = 
	 new StringBuffer();
	 private OutletWebdriverEnvironment myEnvironment;
		private String testName = "index";
		private OutletHMC myHMC;
		private OutletHmcLeftNav myHmcLeftNav = new OutletHmcLeftNav();
		private OutletSignInPage mySignInPage;
	 @Before
	 public void setUp(){
		/*	File fileToProfile = new File( "C:\\Users\\jwest\\Desktop\\Profile");
			FirefoxProfile p = new FirefoxProfile(fileToProfile);
			p.setPreference("javascript.enabled", true);
			p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
	*/			Platform currentPlatform = Platform.getCurrent();		
	if (System.getenv("SELENIUM_BROWSER").contains("FF")) {
	driver = new FirefoxDriver();
	myEnvironment = new OutletWebdriverEnvironment(driver);
	myEnvironment.setBrowser(myEnvironment.getFfBrowser());
	} else if(System.getenv("SELENIUM_BROWSER").contains("SAFARI")) {
		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
	}
	else {
		if(currentPlatform.is(Platform.WINDOWS))
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		else {
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		}
		driver = new ChromeDriver();
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
	
		
	}	
	 myHMC = new OutletHMC(driver, myEnvironment, myHmcLeftNav, testName);
	 mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);

	 }
	 
	 @Test
	 public void testOpeningSite() throws InterruptedException {
			
		WebElement ce;
	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTheTestingEnvironment(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTestSubject(testName);
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		
		// this is to make the right cronjob per environment.
		if(driver.getCurrentUrl().contains("dev")) {
		myHMC.fireSiteDetailsCronJob(myHMC.getTheByIdSiteDetailsFirstCronJob(), mySignInPage);
		} else {
			myHMC.fireSiteDetailsCronJob(myHMC.getTheByIdSiteDetailsFirstCronJob(),  mySignInPage);
			myHMC.fireSiteDetailsCronJob(myHMC.getTheByIdSiteDetailsSecondCronJob(), mySignInPage);
		}		
	 }
	 @After
	 public void tearDown() throws Exception {
	 //Close the browser
		 driver.close();
		 
		 /*String verificationErrorString = 
				 verificationErrors.toString();
				 if (!"".equals(verificationErrorString)) {
				 fail(verificationErrorString);
				 }*/
				 }
				}
