package SeleniumCookbook.tommyBahama.test;



	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.firefox.FirefoxBinary;

import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import TommyBahamaOutletRepository.OutletAddressPage;
import TommyBahamaOutletRepository.OutletCSC;
import TommyBahamaOutletRepository.OutletConfirmationPage;
import TommyBahamaOutletRepository.OutletDeliveryMethodPage;
import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletHomeDecor;
import TommyBahamaOutletRepository.OutletPaymentPage;
import TommyBahamaOutletRepository.OutletPaymentech;
import TommyBahamaOutletRepository.OutletPreviewPage;
import TommyBahamaOutletRepository.OutletProductDetailPage;
import TommyBahamaOutletRepository.OutletProductListingPage;
import TommyBahamaOutletRepository.OutletProducts;
import TommyBahamaOutletRepository.OutletSearchResultsPage;
import TommyBahamaOutletRepository.OutletShoppingCart;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;



	@RunWith(JUnit4.class)
	public class SafariSmokeTest {
		// this is the class where all enviromental details are kept: what site is
		// being
		// tested and any methods that interact with the driver object
		private OutletWebdriverEnvironment myEnvironment;
		// The following are the different objects that make up the Tommy Bahama
		// repository that are used throught this scipt.
		private OutletHeader myHeader;
		private OutletProductListingPage myPLP;
		private OutletProductDetailPage myPDP;
		private OutletSignInPage mySignInPage; 
		private OutletHomeDecor myHomeDecor;
		private OutletProducts  myProducts;
		private OutletShoppingCart myShoppingCart;
		private OutletAddressPage myAddressPage;
		private OutletPaymentPage myPaymentPage;
		private OutletDeliveryMethodPage myDeliveryMethod;
		private OutletPreviewPage myPreviewPage;
		private OutletConfirmationPage myConfirmationPage;
	    private OutletCSC myCSC;
	    private OutletSearchResultsPage mySearchResults;
	    private OutletPaymentech myPaymentech;
<<<<<<< HEAD
		private String testName = "FFSmokeTest";
=======
		private String testName = "SafariSmokeTest";
>>>>>>> 4017059571b87cef1083bec1d699faca86d8186a
		
		private OutletProducts homeProduct = new OutletProducts( "TH31476", "4", "Home");
		
	//	Proxy proxy = new Proxy();
		
	//	ProxyServer server = new ProxyServer(8105);
		// The object which is how all interaction with web elements is made.
		private int ss = 0;
		
		String OrderNumber = "";
		// Main object in the test, driver object is per browser and is used for all
		// interactivity with web page elements.
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		private WebDriver driver;

		@Before
		public void openBrowser() {
		
		   
		//	Assert.assertTrue(isSupportedPlatform());
		/*	myEnvironment = new OutletWebdriverEnvironment();
			File fileToProfile = new File(myEnvironment.getFfProfile());
			FirefoxProfile p = new FirefoxProfile(fileToProfile);
			p.setPreference("javascript.enabled", true);
			p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
			driver = new FirefoxDriver(p);*/
			DesiredCapabilities mc = DesiredCapabilities.safari();
			mc.setCapability("applicationCacheEnabled", true);
			driver = new SafariDriver(mc);

	/*		 String Xport = System.getProperty("lmportal.xvfb.id", ":1");
			    System.out.println(Xport);
			    final File firefoxPath = new File(System.getProperty("lmportal.deploy.firefox.path", 
			        "/usr/bin/firefox"));
			    FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);
			    firefoxBinary.setEnvironmentProperty("DISPLAY", Xport);

			//  FirefoxDriver wd;
			   driver = new FirefoxDriver(firefoxBinary, null);*/
			
			myEnvironment = new OutletWebdriverEnvironment(driver);
			myHeader = new OutletHeader(driver, myEnvironment);
			mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
			 myPLP = new OutletProductListingPage(driver, myEnvironment);
			 myPDP = new OutletProductDetailPage(driver, myEnvironment, myPLP, 
					 myHeader, testName);
			 
			 myPaymentPage = new OutletPaymentPage(myEnvironment, testName);
			 mySearchResults = new OutletSearchResultsPage();
			 myHomeDecor = new OutletHomeDecor();
			 myProducts = new OutletProducts();
			 myAddressPage = new OutletAddressPage();
			 myDeliveryMethod = new OutletDeliveryMethodPage();
			 myPreviewPage = new OutletPreviewPage();
			 myConfirmationPage = new OutletConfirmationPage();
			myShoppingCart = new OutletShoppingCart(driver, myEnvironment);
			
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\" + myEnvironment.getSafariBrowser() + "\\" + testName;
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void FFSmokeEnvironmentTest() throws InterruptedException, IOException {
			WebElement ce;
		//	String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		//	JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigate to the testing environment
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
			myEnvironment.setEnvironment(myEnvironment.getTheTestingEnvironment());
			myEnvironment.setTestSubject(testName);
			
			driver.get(myEnvironment.getTheTestingEnvironment());
			
			mySignInPage.signIn(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());
			
			myEnvironment.waitForTitle("Official Site");
			
			// execute a search
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(myHeader.getTheSearchInput()));
						ce.sendKeys("Men");
						
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(myHeader.getTheSearchBtn()));
						ce.click();
						myEnvironment.waitForPageLoaded();	  
						myEnvironment.waitForDocumentReadyState();
						myEnvironment.waitForURL("Men");
						myEnvironment.waitForTitle("Search Men");
			// select black products
				// have an issue with the left nav only have xpath and the page tends to hang. 		
				/*		ce = myEnvironment.waitForDynamicElement(
								By.xpath(mySearchResults.getTheByXpathBlackCheckBox()));
						ce.click();
						
						// check if black products are present. this should be every product but for now just one.
						
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(mySearchResults.getTheByXpathSecondProduct()));

						Assert.assertTrue(ce.getAttribute("src").contains("023"));*/
						
			
			// add a product to the cart 
						myPDP.selectProduct(				
								myHeader.getTheByXPathHomeDecorTab(),
								myHomeDecor.getTheByXPathDinnerware(),
								homeProduct.getProductId(), homeProduct.getQty(), homeProduct.getName());

						myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, myPDP.getThePreviewPaneName(),
								homeProduct.getProductId());
			// continue to shopping bag
			ce = myEnvironment.waitForDynamicElement(
					By.linkText(myHeader.getTheByLinkCheckout()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForTitle("Shopping");
			
			
			ce = myEnvironment.waitForDynamicElement(
					By.name(myShoppingCart.getTheByNameQtyUpdate()));
			// Thread.Sleep(myEnvironment.DefaultSleep);
			Select clickThis = new Select(ce);
			// Thread.Sleep(myEnvironment.DefaultSleep);
			clickThis.selectByValue("3");
			
			myEnvironment.waitForPageLoaded();	  
			myEnvironment.waitForDocumentReadyState();
			
			// continue to address page
			ce = myEnvironment.waitForDynamicElement(
					By.id(myShoppingCart.getTheByIdBottomContinueCheckout()));
	
	        ce.click();
	        
	    	myEnvironment.waitForPageLoaded();	    	
	    	myEnvironment.waitForDocumentReadyState();
	    	myEnvironment.waitForURL("address");
	        
	    	// continue to delivery options
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myAddressPage.getTheByIdTopContinueCheckoutBtn()));
	
	        ce.click();
	        
	    	myEnvironment.waitForPageLoaded();
	    	myEnvironment.waitForDocumentReadyState();
	    	myEnvironment.waitForURL("delivery-method");
	    	myEnvironment.waitForClickableElement(
 					By.id(myDeliveryMethod.getTheByIdContinueCheckoutButton()));
 	
	    	  // continue to PaymentPage 
 			ce = myEnvironment.waitForDynamicElement(
 					By.id(myDeliveryMethod.getTheByIdContinueCheckoutButton()));
 	
 	        ce.click(); 	        
	    	
 	      	myEnvironment.waitForPageLoaded();
	    	myEnvironment.waitForDocumentReadyState();
	    	myEnvironment.waitForURL("choose-payment-method");
	    	myEnvironment.waitForClickableElement(By.id(myPaymentPage.getTheByIdContinueCheckout()));
	    	
	     // continue to PaymentPage 
	     			ce = myEnvironment.waitForDynamicElement(
	     					By.id(myPaymentPage.getTheByIdContinueCheckout()));
	     	
	     	        ce.click();
	     	        
	     	  
	     	    	myEnvironment.waitForPageLoaded();
	    	    	myEnvironment.waitForDocumentReadyState();
	    	    	myEnvironment.waitForURL("add-payment-method");
	    	     // enter the cc verification number
	    	     			ce = myEnvironment.waitForDynamicElement(
	    	     					By.id(myPaymentPage.getTheByIdCCSecurityCode()));
	    	     	
	    	     	        ce.sendKeys(myPaymentPage.getTheCode());      
	     	
	    	    	
	    	     // continue to Order Review paypage 
	    	     			ce = myEnvironment.waitForDynamicElement(
	    	     					By.id(myPaymentPage.getTheSubmitPaymentBtn()));
	    	     	
	    	     	        ce.click();
	    	     	        
	    	     	   	myEnvironment.waitForPageLoaded();
		    	    	myEnvironment.waitForDocumentReadyState();
		    	    	myEnvironment.waitForURL("summary");
	    	     	        
	    	     	       // submit order
	    	     			ce = myEnvironment.waitForDynamicElement(
	    	     					By.xpath(myPreviewPage.getTheByXpathSubmitButton()));
	    	     	
	    	     	        ce.click();
			
	    	     	      	myEnvironment.waitForPageLoaded();
			    	    	myEnvironment.waitForDocumentReadyState();
			    	    	myEnvironment.waitForTitle("Confirmation");   
			    	    	
			    	    	OrderNumber = driver.getCurrentUrl();
			    			OrderNumber = OrderNumber.substring(64);
			    			
			    			String theTax = driver.findElement(
			    					By.id(myConfirmationPage.getTheTotalTax())).getText();
			    			
			    			System.out.println("This is the tax: 	" + theTax);
			    		//	String theDuty = driver.findElement(
			    		//			By.id(myPreviewPage.getTheByIdDuty())).getText();
			    			String theTotal = driver.findElement(
			    					By.id(myConfirmationPage.getTheTotalAmount())).getText();
			    			
			    			System.out.println("This is the total amount: 	" + theTotal);
			    			myCSC = new OutletCSC(driver, myEnvironment,  "",
			    					theTax, OrderNumber, testName, myEnvironment.getSafariBrowser());
			    			
			    			myCSC.checkOrderInCSC(theTotal);
	
			    			// See that the auth happened in OG
			    			ss = ss + 1;
			    			// remove the $ sign because money values in Contact center do not
			    			// dispaly the dollar sign
			    			Double theDTotal = 0.0;
			    			theDTotal = myPaymentPage.pullPrice(theTotal);
			    			String newTotal = "";
			    			
			    			myPaymentech = new OutletPaymentech(driver, myEnvironment,  
			    					OrderNumber, testName, myEnvironment.getSafariBrowser());
			    			
			    			
			    			newTotal = myPaymentech
			    					.formatTotal(newTotal, theDTotal);
			    			Thread.sleep(myEnvironment.getThe_Special_Sleep());
			    			// runs the script to look up the order in Paytech.
			    			
			    			
			    	//		myPaymentech
			    	//				.checkOrderInPayTech(newTotal);
			    			
			    			
			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException,
				IOException {

			myEnvironment.setNetworkFile(""
					+ myEnvironment.getNetworkTestDirectory()
					+ myEnvironment.getBrowser() + "\\" + this.testName);
			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(myEnvironment.getNetworkFile()
					 + "\\" + this.testName + ".html"));
			
			bw.write("<html><body><center><h2>Outlet Single Division Order</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if a single division order is completed correctly on WS and in CSC.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+
					
					myEnvironment.addSSToFile(testName, mySignInPage.getThePageName(),
							"This is the Sign In page.")
					+
					
					myEnvironment.addSSToFile(testName, myPLP.getThePageName() +  homeProduct.getName(),
						"This is the Dinnerware PLP.")
				+
		
		
				
			myEnvironment.addSSToFile(testName, myPDP.getThePageName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+
				
						
					myEnvironment.addSSToFile(testName, myPDP.getThePreviewPaneName() + homeProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+
					
					myEnvironment.addSSToFile(testName, "LogInPage",
							"This is the CSC login page.")
					+
					
					myEnvironment.addSSToFile(testName, "CSCLandingPage",
							"This is the CSC landing page.")
					+

					myEnvironment.addSSToFile(testName, "SearchOrderFormWithResults",
							"This is the search modal for orders.")
					+
					
					myEnvironment.addSSToFile(testName, "OrderTab",
							"This is the Order Detail page.")
					+

					myEnvironment.addSSToFile(testName, "OrderTab2",
							"This is the bottom of the Order Detail page.")
					+

			     	myEnvironment.getPageTestOutcome()
					+

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Single Divison Order</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+"<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if a single division order is correct on WS and CSC.</h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "file:///"
							+ myEnvironment.getNetworkFile()
							+ "\\" + this.testName + ".html"
							+ "<br/></center></body></html>", "text/html");
						
							
				
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myPLP.getThePageName() + homeProduct.getName());
			fns.add(myPLP.getThePageName() + homeProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + homeProduct.getProductId());
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
			
			ss = ss - 7;
			

			myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(),
					myEnvironment.getNetworkTestDirectory(),
					myEnvironment.getBrowser(), testName);
			driver.quit();

		}

	}

