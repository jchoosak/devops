package TommyBahamaOutletRepository;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OutletProductListingPage {
	private final String theFirstItem = "//div[2]/div[2]/a";
	private final String theFirstImage = "//div[@id='imgThumb_1']/a/img";
	private final String theSecondItem = "//td[2]/div/div[2]/div[2]/a";
	private final String theThridItem = "//td[3]/div/div[2]/div[2]/a";
	private final String theFourthItem = "//table[@id='cat_list_table']/tbody/tr/td[4]/div/div[2]/div[2]/a";
	private final String theEighthItem = "//div[@id='imgContainer_7']/div[2]/div[2]/a";
	private final String theUrlType = "http://";
	private final String theFourtyEightPrice = "$48.00";
	private final String theSeventyTwoPrice = "$72.00";
	private final String theSeventyEightPrice = "$78.00";
	private final String theFourtyFivePrice = "$45.00";
	private final String theThirtyEightPrice = "$38.00";
	private final String theSeventeenPrice = "$17.00";
	private final String theTwentyPrice = "$20.00";
	private final String theEightyEightPrice = "$88.00";
	private final String theNinetyEightPrice = "$98.00";
	private final String theByXPathDiffuser = "//a[contains(text(),'Island Blend Room Diffuser')]";
	private final String theByXPathProductionDiffuser = "//a[contains(text(),'Diffuser Oil Refill')]";
	private final String theFPNextPageLink = "//div[@id='divFooterItemsNav']/a[2]";
	private final String theNextPageLink = "//div[@id='divFooterItemsNav']/a[3]";
	private final String theShowAllLink = "(//a[contains(text(),'Show All')])[2]";
	private final String theByXPathNextPageLink = "//a[contains(text(),'next page >')]";
	// private final String theByLinkNextPageLink = "next page >";
	private final String theCandaShippingRestrictionText = "Please note, this product cannot be shipped to CANADA using our Canada FedEx";
	private final String theAustraliaShippingRestrictionText = "We're sorry, this product cannot be shipped to AUSTRALIA";
	private final String theUnitedKingdomShippingRestrictionText = "We're sorry, this product cannot be shipped to United Kingdom";
	
	private final String theSearchResultCanadaShippingRestrictionText = "Shipping Restriction May Apply";
	private final String theByXPathAustralianCurrency = "//div[@id='imgContainer_2']/div[2]/div";

	private final String thePageName = "PLP";
	
	private WebDriver driver;
	private OutletWebdriverEnvironment myEnvironment;
	
	public OutletProductListingPage(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
	}
	
	public String getTheFirstItem() {
		return theFirstItem;
	}

	public String getTheFirstImage() {
		return theFirstImage;
	}

	public String getTheSecondItem() {
		return theSecondItem;
	}

	public String getTheThridItem() {
		return theThridItem;
	}

	public String getTheFourthItem() {
		return theFourthItem;
	}

	public String getTheEighthItem() {
		return theEighthItem;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheFourtyEightPrice() {
		return theFourtyEightPrice;
	}

	public String getTheSeventyTwoPrice() {
		return theSeventyTwoPrice;
	}

	public String getTheSeventyEightPrice() {
		return theSeventyEightPrice;
	}

	public String getTheFourtyFivePrice() {
		return theFourtyFivePrice;
	}

	public String getTheThirtyEightPrice() {
		return theThirtyEightPrice;
	}

	public String getTheSeventeenPrice() {
		return theSeventeenPrice;
	}

	public String getTheTwentyPrice() {
		return theTwentyPrice;
	}

	public String getTheEightyEightPrice() {
		return theEightyEightPrice;
	}

	public String getTheNinetyEightPrice() {
		return theNinetyEightPrice;
	}

	public String getTheByXPathDiffuser() {
		return theByXPathDiffuser;
	}

	public String getTheByXPathProductionDiffuser() {
		return theByXPathProductionDiffuser;
	}

	public String getTheFPNextPageLink() {
		return theFPNextPageLink;
	}

	public String getTheNextPageLink() {
		return theNextPageLink;
	}

	public String getTheShowAllLink() {
		return theShowAllLink;
	}

	public String getTheByXPathNextPageLink() {
		return theByXPathNextPageLink;
	}

	public String getTheCandaShippingRestrictionText() {
		return theCandaShippingRestrictionText;
	}

	public String getTheAustraliaShippingRestrictionText() {
		return theAustraliaShippingRestrictionText;
	}

	public String getTheSearchResultCanadaShippingRestrictionText() {
		return theSearchResultCanadaShippingRestrictionText;
	}

	public String getTheByXPathAustralianCurrency() {
		return theByXPathAustralianCurrency;
	}

	// / <summary>
	// / This just runs through the PLP for cards and checks that each has the
	// correct Restriction message.
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="myEnvironment"></param>
	public void checkAllTraditionalCards() {
		WebElement currentElement;
		for (int i = 0; i < 2; i++) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i
							+ "']/div[2]/div[2]"));
			System.out.println(currentElement.getText());
			System.out.println(theSearchResultCanadaShippingRestrictionText);
			Assert.assertTrue(currentElement
					.getText()
					.trim()
					.contains(
							theSearchResultCanadaShippingRestrictionText.trim()));
			System.out
					.println("The Restriction text is present for this product!        "
							+ currentElement.getText());
			if (i == 2) {
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
				currentElement.click();
			}
		}

	}

	// / <summary>
	// / This method traverses a PLP and finds the matching product and checks
	// to see that the product
	// / has the proper restriction text. This method is passed the Webdriver
	// and Environment objects and the product ID
	// / of the product being tested.
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="myEnvironment"></param>
	// / <param name="theProductID"></param>
	public void checkRestrictionText(String theProductID)
			throws InterruptedException {
		WebElement currentElement;
		currentElement = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));

		if (myEnvironment.IsElementPresent(By.xpath(this.getTheShowAllLink()))) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheShowAllLink()));
			currentElement.click();
			// currentElement = myEnvironment.waitForDynamicElement(
			// By.XPath("//div[@id='imgThumb_" + 0 + "']/a/img"));
		}
		for (int i = 0; i < 250; i++) {
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
			System.out.println(currentElement.getAttribute("src"));
			Thread.sleep(500);
			if (currentElement.getAttribute("src").contains(theProductID)) {
				// currentElement = myEnvironment.waitForDynamicElement(
				// By.XPath("//div[@id='imgContainer_" + i +
				// "']/div[2]/div[2]"));
				Assert.assertTrue(myEnvironment.IsElementPresent(
						By.cssSelector("#imgContainer_"
								+ i
								+ " > div.cat_product_desc > div.intl_restricted_icon")));
				currentElement = myEnvironment
						.waitForDynamicElement(
								By.cssSelector("#imgContainer_"
										+ i
										+ " > div.cat_product_desc > div.intl_restricted_icon"));
				Assert.assertEquals(currentElement.getText(),
						theSearchResultCanadaShippingRestrictionText);
				System.out
						.println("The Restriction text is present for this product!        "
								+ currentElement.getText());
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
				currentElement.click();
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='choiceContainer']/div[2]"));
				if (currentElement.getText().contains("Canada")) {
					Assert.assertEquals(currentElement.getText(),
							theCandaShippingRestrictionText);
					break;
				} else if (currentElement.getText().contains("AUSTRALIA")) {
					Assert.assertEquals(currentElement.getText(),
							theAustraliaShippingRestrictionText);
					break;
				}  else if (currentElement.getText().contains("United Kingdom")) {
					Assert.assertEquals(currentElement.getText(),
							theUnitedKingdomShippingRestrictionText);
					break;
				}
			}
		}
	}

	// / <summary>
	// / This helper method pulls the product ID number from the swatch's src
	// string.
	// / This helper mehtod is passed the parts of the xpath to find the swatch
	// and the webdriver object.
	// / </summary>
	// / <param name="swatchStringStart"></param>
	// / <param name="swatchStringEnd"></param>
	// / <param name="driver"></param>
	// / <param name="i"></param>
	// / <returns></returns>
	public String pullProductCodeFromSwatch(String swatchStringStart,
			String swatchStringEnd, int i) {
		String url = driver.getCurrentUrl();
		String pulledProduct;
		pulledProduct = driver.findElement(
				By.xpath(swatchStringStart + i + swatchStringEnd))
				.getAttribute("src");
		if (url.contains("www"))
			pulledProduct = pulledProduct.substring(47);
		else
			pulledProduct = pulledProduct.substring(49);
		System.out.println(pulledProduct);
		int index = pulledProduct.indexOf("_");
		pulledProduct = pulledProduct.substring(0, index);
		System.out.println(pulledProduct);
		return pulledProduct;
	}

	// / <summary>
	// / This helper method pulls the Color ID number from the swatch's src
	// string.
	// / This helper mehtod is passed the parts of the xpath to find the swatch
	// and the webdriver object.
	// / </summary>
	// / <param name="swatchStringStart"></param>
	// / <param name="swatchStringEnd"></param>
	// / <param name="driver"></param>
	// / <param name="i"></param>
	// / <returns></returns>
	public String pullColorCodeFromSwatch(String swatchStringStart,
			String swatchStringEnd, int i) {
		String url = driver.getCurrentUrl();
		String pulledColor;
		pulledColor = driver.findElement(
				By.xpath(swatchStringStart + i + swatchStringEnd))
				.getAttribute("src");
		if (url.contains("ecap"))
			pulledColor = pulledColor.substring(56);
		else
			pulledColor = pulledColor.substring(54);
		System.out.println(pulledColor);
		int index = pulledColor.indexOf("_");
		pulledColor = pulledColor.substring(0, index);
		System.out.println(pulledColor);
		return pulledColor;
	}

	// / <summary>
	// / This helper method pulls the Color ID number from the thumbnail's src
	// string.
	// This helper mehtod is passed the parts of the xpath to find the thumbnail
	// and the webdriver object.
	// / </summary>
	// / <param name="thumbNailStart"></param>
	// / <param name="thumbNailEnd"></param>
	// / <param name="driver"></param>
	// / <param name="j"></param>
	// / <returns></returns>
	public String pullColorCodeFromThumbNail(String thumbNailStart_i,
			String thumbNailEnd_i, String thumbNailEnd_j, int i, int j) {
		String pulledColor;
		pulledColor = driver.findElement(
				By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j))
				.getAttribute("src");
		pulledColor = pulledColor.substring(46);
		System.out.println(pulledColor);
		int index = pulledColor.indexOf("_");
		pulledColor = pulledColor.substring(0, index);
		System.out.println(pulledColor);
		return pulledColor;
	}

	/*
	 * This helper method pulls the product ID number from the thumbnail's src
	 * string. This helper mehtod is passed the parts of the xpath to find the
	 * swatch and the webdriver object.
	 */
	public String pullProductCodeFromThumbNail(String thumbNailStart_i,
			String thumbNailEnd_i, String thumbNailEnd_j, int i, int j) {
		String url = driver.getCurrentUrl();
		String productCode;
		productCode = driver.findElement(
				By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j))
				.getAttribute("src");
	//	if (url.contains("ecap") || url.contains("test"))
	//		productCode = productCode.substring(49);
	//	else
			productCode = productCode.substring(47);
		System.out.println(productCode);
		int index = productCode.indexOf("_");
		productCode = productCode.substring(0, index);
		System.out.println(productCode);
		return productCode;
	}

	/*
	 * Choose Swatch method searches for a certain colored swatch and then makes
	 * sure when the product is selected that the PDP loads with this color as
	 * the default. This method also changes the color of each thumbnail as it
	 * searches for the certain color. Plan on now making a version of this
	 * method that just traverses all swatches on a PLP and checks the thumb
	 * nail each time a swatch is clicked. This method is passed the webdriver
	 * and environment objects, the color to be searced for and the PDP object.
	 */
	public void chooseSwatch( String theColorName,
			OutletProductDetailPage myProductDetailObjs) {
		WebElement ce;
		String swatchStringStart = "(//img[@id=''])[";
		String swatchStringEnd = "]";
		String currentProduct = "";
		String thumbNailStart = "//div[@id='imgThumb_";
		String thumbNailEnd = "']/a/img";
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(thumbNailStart + 0 + thumbNailEnd));
		// myEnvironment.myWaitForWDElement(driver, By.XPath(thumbNailStart + 0
		// + thumbNailEnd));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheShowAllLink()));
		ce.click();

		for (int i = 1; i < 1000; i++) {
			if (!myEnvironment.IsElementPresent(
					By.xpath(swatchStringStart + i + swatchStringEnd))) {
				System.out
						.println("This entered color is not present for this product group!");
				break;
			}
			currentProduct = this.pullProductCodeFromSwatch(swatchStringStart,
					swatchStringEnd, i);
			System.out.println("The following is the product code:");
			System.out.println(currentProduct);

			ce = myEnvironment.waitForDynamicElement(
					By.xpath(swatchStringStart + i + swatchStringEnd));
			System.out.println(ce.getAttribute("src"));
			ce.click();
			String currentColor = this.pullColorCodeFromSwatch(
					swatchStringStart, swatchStringEnd, i);
			System.out
					.println("The following is the current color of the swatch that has been clicked.");
			System.out.println(currentColor);

			if (ce.getAttribute("src").contains(theColorName)) {
				// Assert.True(driver.FindElement(By.XPath(thumbNailStart + i +
				// thumbNailEnd)).GetAttribute("src").Contains(currentColor));
				System.out.println("It has the color name...............");
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(thumbNailStart + 0 + thumbNailEnd));
				if (!ce.getAttribute("src").contains(currentProduct)) {
					currentProduct = this.pullProductCodeFromSwatch(
							swatchStringStart, swatchStringEnd, i);
					System.out.println(currentProduct);
					System.out
							.println("Does not contain the current product name..................");
					for (int k = 0; k < 40; k++) {
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(thumbNailStart + k + thumbNailEnd));
						System.out.println(ce.getAttribute("src"));
						if (ce.getAttribute("src").contains(currentProduct)) {
							Assert.assertTrue(ce.getAttribute("src").contains(
									currentColor));
							ce.click();
							System.out.println("A thumbnail image was clicked");
							break;
						}
					}
				} else {
					ce = myEnvironment.waitForDynamicElement(
							By.xpath(thumbNailStart + 0 + thumbNailEnd));
					ce.click();
				}

				ce = myEnvironment.waitForDynamicElement( By
						.xpath(myProductDetailObjs.getTheFirstCrossSellItem()));
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//*[@id='imgDetail']"));
				System.out.println(ce.getAttribute("src"));
				System.out.println(theColorName);
				Assert.assertTrue(ce.getAttribute("src").contains(theColorName));
				System.out
						.println("We are getting past the Assert, why are we not breaking out of this loop?");
				break;
			}
		}
	}

	/*
	 * THis method goes through a PLP and selects each swatch and then checks to
	 * see if the cooresponding thumbnail changed to the correct color. Passed
	 * the Webdriver, environment and PDP objects.
	 */
	public void testPLPSwatches(OutletProductDetailPage myProductDetailObjs) {
		WebElement ce;
		String swatchStringStart = "(//img[@id=''])[";
		String swatchStringEnd = "]";
		String currentProduct = "";
		String thumbNailStart = "//div[@id='imgThumb_";
		String thumbNailEnd = "']/a/img";
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(thumbNailStart + 0 + thumbNailEnd));

		if (myEnvironment.IsElementPresent(By.xpath(this.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheShowAllLink()));
			ce.click();
		}

		// ce = myEnvironment.waitForDynamicElement(
		// By.XPath(this.ShowAllLink));
		// ce.Click();
		for (int i = 1; i < 1000; i++) {
			if (!myEnvironment.IsElementPresent(
					By.xpath(swatchStringStart + i + swatchStringEnd))) {
				System.out
						.println("There is no more swatches to test!");
				break;
			}
			currentProduct = this.pullProductCodeFromSwatch(swatchStringStart,
					swatchStringEnd, i);
			System.out.println("The following is the productcode:");
			System.out.println(currentProduct);
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(swatchStringStart + i + swatchStringEnd));
			System.out.println(ce.getAttribute("src"));
			ce.click();
			String currentColor = this.pullColorCodeFromSwatch(
					swatchStringStart, swatchStringEnd, i);
			System.out
					.println("The following is the current color of the swatch that has been clicked.");
			System.out.println(currentColor);
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(swatchStringStart + i + swatchStringEnd));
			if (ce.getAttribute("src").contains(currentColor)) {
				System.out.println("It has the color name...............");
				ce = myEnvironment.waitForDynamicElement(
						By.xpath(thumbNailStart + 0 + thumbNailEnd));
				if (!ce.getAttribute("src").contains(currentProduct)) {
					currentProduct = this.pullProductCodeFromSwatch(
							swatchStringStart, swatchStringEnd, i);
					System.out.println("The following is the ProductCode.");
					System.out.println(currentProduct);
					for (int k = 0; k < 40; k++) {
						ce = myEnvironment.waitForDynamicElement(
								By.xpath(thumbNailStart + k + thumbNailEnd));
						System.out.println(ce.getAttribute("src"));
						if (ce.getAttribute("src").contains(currentProduct)) {
							Assert.assertTrue(ce.getAttribute("src").contains(
									currentColor));
							break;
						}
					}
				} else
					continue;
			}
		}
	}

	/*
	 * This method goes through a list of links that are in xpath form and then
	 * traverses through the products on the PLP by the xpaths of each product
	 * This method is used to cache the different PLP pages after a build in an
	 * environment. This method is passed the list of links to the different
	 * PLPs, the webdriver and the PLP object.
	 */

	public void traverseLinks(List<String> theList, 
			OutletProductDetailPage myProductDetailObjs, String by)
			throws InterruptedException {
		WebElement currentElement;
		myEnvironment.waitForPageLoaded();
		for (String currentLink : theList) {
			String Url;
			System.out.println("This is the Start of a new PLP page.");
			System.out.println(Url = driver.getCurrentUrl());

			if (by == "xpath") {
				Thread.sleep(5000);
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath(currentLink));
				currentElement.click();
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
			} else {
				currentElement = myEnvironment.waitForDynamicElement(
						By.linkText(currentLink));
				currentElement.click();
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
			}
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));

			if (myEnvironment.IsElementPresent(
					By.xpath(this.getTheShowAllLink()))) { // Thread.sleep(1000);
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath(this.getTheShowAllLink()));
				currentElement.click();
				Thread.sleep(5000);
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				System.out.println(Url = driver.getCurrentUrl());
				Url = driver.getCurrentUrl() + "?showAll=1";
			}

			for (int i = 0; i < 250; i++) {
				// if(i != 0)
				if (myEnvironment.IsElementPresent(
						By.xpath("//div[@id='imgThumb_" + i + "']/a/img"))) {
					currentElement = myEnvironment.waitForClickableElement(
							By.xpath("//div[@id='imgThumb_" + i + "']/a/img"));
					currentElement.click();
					// Thread.sleep(myEnvironment.getThe_Default_Sleep());
					myEnvironment.waitForClickableElement( By
							.cssSelector(myProductDetailObjs
									.getTheByCSSAddToBagLink()));
					// Thread.Sleep(myEnvironment.DefaultSleep);
					driver.get(Url);
					myEnvironment.checkForBrokenImages();

				} else {
					break;
				}
			}
		}
	}

	/*
	 * This method goes through a list of links that are in xpath form and then
	 * traverses through the products on the PLP by the xpaths of each product
	 * This method is used to cache the different PLP pages after a build in an
	 * environment. This method is passed the list of links to the different
	 * PLPs, the webdriver and the PLP object.
	 */

	public void traverseProducts(List<String> theList,
			OutletProductDetailPage myProductDetailObjs, String by)
			throws InterruptedException {
		WebElement currentElement;
		myEnvironment.waitForDocumentReadyState();
		for (String currentLink : theList) {
			String Url;
			System.out.println("This is the Start of a new PLP page.");
			System.out.println(Url = driver.getCurrentUrl());

			if (by == "xpath") {
				// Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath(currentLink));
				currentElement.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
			} else {
				currentElement = myEnvironment.waitForDynamicElement(
						By.linkText(currentLink));
				currentElement.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				Url = driver.getCurrentUrl();
			}
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
			// Thread.sleep(myEnvironment.getThe_Default_Sleep());

			if (myEnvironment.IsElementPresent(
					By.xpath(this.getTheShowAllLink()))) {
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath(this.getTheShowAllLink()));
				currentElement.click();
				Thread.sleep(myEnvironment.getThe_Default_Sleep());
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
				System.out.println(Url = driver.getCurrentUrl());
				Url = driver.getCurrentUrl();

			}
			// currentElement = waitForDynamicElement(
			// By.XPath("//div[@id='imgThumb_" + 0 + "']/a/img"));
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
			currentElement.click();
			while (myEnvironment.IsElementPresent(
					By.linkText("next product >"))) {

				currentElement = myEnvironment.waitForDynamicElement(
						By.linkText("next product >"));

				currentElement.click();

				// Thread.sleep(250);
				myEnvironment.waitForDynamicElement(
						By.xpath(myProductDetailObjs.getTheMainImage()));

			}
			driver.get(Url);
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		}
	}

	public void traversePLPLinks(List<String> theList,
			OutletProductDetailPage myProductDetailObjs, String theBy) {
		int page;
		WebElement currentElement;
		for (String currentLink : theList) {
			page = 1;
			System.out.println("This is the Start of a new PLP page.");
			System.out.println(driver.getCurrentUrl());

			if (theBy == "xpath") {
				currentElement = myEnvironment.waitForDynamicElement(
						By.xpath(currentLink));
				currentElement.click();
			} else {
				currentElement = myEnvironment.waitForDynamicElement(
						By.linkText(currentLink));
				currentElement.click();
			}

			while (myEnvironment.IsElementPresent(
					By.xpath(this.getTheNextPageLink()))
					|| myEnvironment.IsElementPresent(
							By.xpath(this.getTheByXPathNextPageLink()))) {
				
				myEnvironment.checkForBrokenImages();
				if (myEnvironment.IsElementPresent(
						By.xpath("//div[@id='imgThumb_" + 19 + "']/a/img"))
						&& myEnvironment.IsElementPresent(
								By.xpath(this.theFPNextPageLink))
						&& page == 1) {
					driver.findElement(By.xpath(this.theFPNextPageLink))
							.click();
					myEnvironment.waitForDynamicElement(
							By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
					// i = 0;
					page = page + 1;
				} else if (myEnvironment.IsElementPresent(
						By.xpath("//div[@id='imgThumb_" + 19 + "']/a/img"))
						&& myEnvironment.IsElementPresent(
								By.xpath(this.getTheByXPathNextPageLink()))) {
					driver.findElement(
							By.xpath(this.getTheByXPathNextPageLink())).click();
					myEnvironment.waitForDynamicElement(
							By.xpath("//div[@id='imgThumb_" + 0 + "']/a/img"));
					// i = 0;
				} else
					break;
			}
		}
	}
	
	public void traverseOutletPLP(OutletProductDetailPage myProductDetailObjs) {
		WebElement currentElement;
	//	String URL = driver.getCurrentUrl() + "?showAll=1";
		String URL = driver.getCurrentUrl();
		// div[@id='content']/div[3]/div[2]/div[2]/div/div[j]/div/div/a/span/img

	/*	if (myEnvironment.IsElementPresent(By.xpath("//span/img"))) {
			// div[@id='content']/div[3]/div[2]/div[2]/div/div[2]/div/div/a/span/img
			currentElement = myEnvironment.waitForDynamicElement(By
					.xpath("//span/img"));
			currentElement.click();
			// Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myEnvironment.waitForDynamicElement(By
					.cssSelector("div.swatch > img"));
			// Thread.Sleep(myEnvironment.DefaultSleep);
			driver.get(URL);
		}
*/
		for (int i = 1; i < 6; i++) {

			for (int j = 1; j < 4; j++) {
				// if(i != 0)

				if (myEnvironment.IsElementPresent(By
						.xpath("//div[@id='content']/div[3]/div[2]/div[2]/div[" + i
								+ "]/div[" + j + "]/div/div/a/span/img"))) {
					//div[@id='content']/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/a/span/img
					currentElement = myEnvironment.waitForDynamicElement(By
							.xpath("//div[@id='content']/div[3]/div[2]/div[2]/div[" + i
								+ "]/div[" + j + "]/div/div/a/span/img"));
					currentElement.click();
					// Thread.sleep(myEnvironment.getThe_Default_Sleep());
					myEnvironment.waitForDynamicElement(By
							.xpath("//form[@id='addToCartForm']/h2/span[2]"));
					// Thread.Sleep(myEnvironment.DefaultSleep);
					driver.get(URL);

				}

				if (i == 5 && j == 3
						&& myEnvironment.IsElementPresent(By
								.xpath("//a[contains(text(),'Next Page �')]"))) {
					currentElement = myEnvironment.waitForDynamicElement(By
							.xpath("//a[contains(text(),'Next Page �')]"));
					currentElement.click();
					i = 1; 
					j = 1;
					URL = driver.getCurrentUrl();
				}

				else if (i == 14
						&& !myEnvironment.IsElementPresent(By
								.xpath("//a[contains(text(),'Next Page �')]"))) {
					break;
				}
			}
		}
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheUnitedKingdomShippingRestrictionText() {
		return theUnitedKingdomShippingRestrictionText;
	}
}
