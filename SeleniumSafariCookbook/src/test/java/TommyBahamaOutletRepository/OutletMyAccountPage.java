package TommyBahamaOutletRepository;



	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.ui.Select;

	public class OutletMyAccountPage {
	
		   private final String theOrderHistory = "//a[contains(text(),'View your order history')]";
		   private final String theFirstOrder = "//table[@id='order_history']/tbody/tr/td/a";
			
		 
		private final String theByXPathEmailAddress = "//div[@id='content_container_id']/div/div[2]/div/div/div[5]/div/div[3]/div[2]";
	   private final String thePersonalInfoEditLink = "//a[contains(text(),'Edit')]";
	   
	   
	   private final String theByNamePersonalInfoFirstName = "USER_ACCOUNT<>firstName";
	   private final String theByNamePersonalInfoLastName = "USER_ACCOUNT<>lastName";
	 
		private final String theByNamePersonalInfoOldPassword = "USER_ACCOUNT<>oldPassword";
	   private final String theByNamePersonalInfoPassword = "USER_ACCOUNT<>oldPassword";
	   private final String theByNamePersonalInfoConfirmPassword = "USER_ACCOUNT<>confirmPassword";
	   
	   
	   
	   private final String theByIdSubmitPersonalInfo = "editInfoSubmit";
	   private final String theByXPathAddBillingAddress = "//a[contains(text(),'Add Address')]";
	   private final String theByXPathAddShippingAddress = "(//a[contains(text(),'Add Address')])[2]";
	   
	   private final String theByNameBilltoAddressLabel = "BILL_TO_ADDRESS<>label";
		private final String theByNameBilltoAddressFirstName = "BILL_TO_ADDRESS<>firstName";
	   private final String theByNameBilltoAddressLastName = "BILL_TO_ADDRESS<>lastName";
	   private final String theByNameBilltoAddressAddressOne = "BILL_TO_ADDRESS<>address1";
	   private final String theByNameBilltoAddressCity = "BILL_TO_ADDRESS<>city";
	   private final String theByNameBilltoAddressState = "BILL_TO_ADDRESS<>state_cd";
	   private final String theByNameBilltoAddressPostal = "BILL_TO_ADDRESS<>postal";
	   private final String theByNameBilltoAddressPhoneAreaCode = "BILL_TO_ADDRESS<>ATR_txt_add_home_phone1";
	   private final String theByNameBilltoAddressPhonePrefix = "BILL_TO_ADDRESS<>ATR_txt_add_home_phone2";
	   private final String theByNameBilltoAddressPhoneNumber = "BILL_TO_ADDRESS<>ATR_txt_add_home_phone3";
	   private final String theByNameBilltoAddressDefaultAddress = "BILL_TO_ADDRESS<>";
	   private final String theByIdAddrSubmit = "addrSubmit";
	   
	   private final String theByCSSAddedAddress = "td > table > tbody > tr > td";
	   private final String theByCSSEditShipingAddress = "div.billing_container > table > tbody > tr > td > div.title_btn_edit_left > a";
	   
	   private final String theByCSSDeleteBillingAddress = "div.title_btn_delete > a";
	   private final String theByCSSEditBillingAddress = "div.title_btn_edit_left > a";
	   
	  
	   private final String theByCSSDeleteShippingAddress = "div.billing_container > table > tbody > tr > td > div.title_btn_delete > a";
	   private final String theByCSSDeleteCC = "div.title_btn_delete_left > a";
	   
	   private final String theByCSSShippingAddressInfo = "body.editaccountbody";
	   
	   private final String theByCSSAddCard = "span.title_btn_add > a";
	   
	   
	   private final String theByNameCreditCardLabel = "CREDIT_CARD<>label";
	   private final String theByNameCreditCardName = "CREDIT_CARD<>cardholderName";
	   private final String theByNameCreditCardBrand = "CREDIT_CARD<>cardBrand_cd";
	   private final String theByNameCreditCardNumber = "CREDIT_CARD<>cardNum";
	   private final String theByNameCreditCardMonth = "CREDIT_CARD<>cardMonth_cd";
	   private final String theByNameCreditCardYear = "CREDIT_CARD<>cardYear_cd";
	   
	   private final String theByIdAddCCSubmit = "addCCSubmit";
	   
	   
	   private final String theByCSSCreditCardInfo = "div.credit_card";
	   
	   
	   private final String theByNameShiptoAddressLabel = "SHIP_TO_ADDRESS<>label";
	 	

		private final String theByNameShiptoAddressFirstName = "SHIP_TO_ADDRESS<>firstName";
	    private final String theByNameShiptoAddressLastName = "SHIP_TO_ADDRESS<>lastName";
	    private final String theByNameShiptoAddressAddressOne = "SHIP_TO_ADDRESS<>address1";
	    private final String theByNameShiptoAddressCity = "SHIP_TO_ADDRESS<>city";
	    private final String theByNameShiptoAddressState = "SHIP_TO_ADDRESS<>state_cd";
	    private final String theByNameShiptoAddressPostal = "SHIP_TO_ADDRESS<>postal";
	    private final String theByNameShiptoAddressPhoneAreaCode = "SHIP_TO_ADDRESS<>ATR_txt_add_home_phone1";
	    private final String theByNameShiptoAddressPhonePrefix = "SHIP_TO_ADDRESS<>ATR_txt_add_home_phone2";
	    private final String theByNameShiptoAddressPhoneNumber = "SHIP_TO_ADDRESS<>ATR_txt_add_home_phone3";
	    private final String theByNameShiptoAddressDefaultAddress = "SHIP_TO_ADDRESS<>";
	    
	    private final String theByCSSReturnWizard = "form[name=\"returns_link\"] > a";
	    private final String theByCSSFourthOrderReturnWizard = "#nameLink4";
	    private final String theByCSSFirstAmountItemsToReturn = "#selectId_0";
	    private final String theByNameFirstReasonToReturn = "ORDER_LINE_ARRAY<>returnTypeCode";
	    private final String theByCSSSubmitStep2 = "#continueToStep3";
	    private final String theByCSSSiteError = "#site_error_id";
	    
	    private final String theNoItemSelectedReturnErrorMessage = "Please indicate which order line(s) you would like to return.";
	    private final String theNoItemQtySelectedReturnErrorMessage = "Please select a quantity for the line item you wish to return.";
	    private final String theNoReasonReturnErrorMessage = "Please select a reason for the line item you wish to return.";
	    
	    private final String theByCSSSendEmailBtn = "#returnButtonStyleEmailId";
	    
	    private final String theByIdPrintReturnLabelBtn = "continue_step_3b";
	    private final String theByIdEmailReturnLabelBtn = "emailUSPSInputId";
	    
	    private final String theByCSSRmaInfo = "ul.return_orders > li";
	    private final String theByIdEmailWizardInput = "emailUSPSInputId";
	    private final String theByCSSSendEmailReturnWizard = "#returnButtonStyleEmailId";
	    private final String theByCSSConfirmEmailSentLabel = "#label_status_message";
	    private final String theByIdContinueStep4Btn = "continue4";
	    private final String theByClassNameConfirmationRMAInfo = "return_orders";
	    private final String theByCSSPrintConfirmationBtn = "img[alt=\"Print Confirmation\"]";
	    private final String thePageName = "AccountPage";
	    
	    private final String theByCSSRmaNumber = "ul.return_orders > li > b";
	   
	   // private WebDriver driver;
	    OutletWebdriverEnvironment myEnvironment;
	    public  OutletMyAccountPage(OutletWebdriverEnvironment theEnvironment)
	    {
	   // 	driver = theDriver;
	    	myEnvironment = theEnvironment;
	    }
	    
	    
	    public String getTheByNameShiptoAddressFirstName() {
	  		return theByNameShiptoAddressFirstName;
	  	}

	  	public String getTheByNameShiptoAddressLastName() {
	  		return theByNameShiptoAddressLastName;
	  	}

	  	public String getTheByNameShiptoAddressAddressOne() {
	  		return theByNameShiptoAddressAddressOne;
	  	}

	  	public String getTheByNameShiptoAddressCity() {
	  		return theByNameShiptoAddressCity;
	  	}

	  	public String getTheByNameShiptoAddressState() {
	  		return theByNameShiptoAddressState;
	  	}

	  	public String getTheByNameShiptoAddressPostal() {
	  		return theByNameShiptoAddressPostal;
	  	}

	  	public String getTheByNameShiptoAddressPhoneAreaCode() {
	  		return theByNameShiptoAddressPhoneAreaCode;
	  	}

	  	public String getTheByNameShiptoAddressPhonePrefix() {
	  		return theByNameShiptoAddressPhonePrefix;
	  	}

	  	public String getTheByNameShiptoAddressPhoneNumber() {
	  		return theByNameShiptoAddressPhoneNumber;
	  	}

	  	public String getTheByNameShiptoAddressDefaultAddress() {
	  		return theByNameShiptoAddressDefaultAddress;
	  	} 
	   
	  public String getTheByNameBilltoAddressLabel() {
	 		return theByNameBilltoAddressLabel;
	 	}

	 	public String getTheByNameBilltoAddressFirstName() {
	 		return theByNameBilltoAddressFirstName;
	 	}

	 	public String getTheByNameBilltoAddressLastName() {
	 		return theByNameBilltoAddressLastName;
	 	}

	 	public String getTheByNameBilltoAddressAddressOne() {
	 		return theByNameBilltoAddressAddressOne;
	 	}

	 	public String getTheByNameBilltoAddressCity() {
	 		return theByNameBilltoAddressCity;
	 	}

	 	public String getTheByNameBilltoAddressState() {
	 		return theByNameBilltoAddressState;
	 	}

	 	public String getTheByNameBilltoAddressPostal() {
	 		return theByNameBilltoAddressPostal;
	 	}

	 	public String getTheByNameBilltoAddressPhoneAreaCode() {
	 		return theByNameBilltoAddressPhoneAreaCode;
	 	}

	 	public String getTheByNameBilltoAddressPhonePrefix() {
	 		return theByNameBilltoAddressPhonePrefix;
	 	}
	   
	   public String getTheByXPathEmailAddress() {
			return theByXPathEmailAddress;
		}

		public String getThePersonalInfoEditLink() {
			return thePersonalInfoEditLink;
		}
		
		public String getTheByNamePersonalInfoFirstName()
		{
			return theByNamePersonalInfoFirstName;
		}
		
		public String getTheByNamePersonalInfoLastName() {
			return theByNamePersonalInfoLastName;
		}
		
		public String getTheByIdSubmitPersonalInfo() {
			return theByIdSubmitPersonalInfo;
		}

		public String getTheByIdAddrSubmit() {
			return theByIdAddrSubmit;
		}

		public String getTheByNameBilltoAddressPhoneNumber() {
			return theByNameBilltoAddressPhoneNumber;
		}

		public String getTheByXPathAddBillingAddress() {
			return theByXPathAddBillingAddress;
		}


		public String getTheByNameBilltoAddressDefaultAddress() {
			return theByNameBilltoAddressDefaultAddress;
		}

		public String getTheByCSSDeleteBillingAddress() {
			return theByCSSDeleteBillingAddress;
		}

		public String getTheByCSSAddedAddress() {
			return theByCSSAddedAddress;
		}

		public String getTheByCSSEditBillingAddress() {
			return theByCSSEditBillingAddress;
		}

		public String getTheByCSSAddCard() {
			return theByCSSAddCard;
		}

		public String getTheByNameCreditCardLabel() {
			return theByNameCreditCardLabel;
		}

		public String getTheByNameCreditCardName() {
			return theByNameCreditCardName;
		}

		public String getTheByNameCreditCardBrand() {
			return theByNameCreditCardBrand;
		}

		public String getTheByNameCreditCardNumber() {
			return theByNameCreditCardNumber;
		}

		public String getTheByNameCreditCardMonth() {
			return theByNameCreditCardMonth;
		}

		public String getTheByNameCreditCardYear() {
			return theByNameCreditCardYear;
		}

		public String getTheByIdAddCCSubmit() {
			return theByIdAddCCSubmit;
		}
		
		public String getTheByCSSCreditCardInfo() {
			return theByCSSCreditCardInfo;
		}

		public String getTheByCSSDeleteCC() {
			return theByCSSDeleteCC;
		}

		public String getTheByXPathAddShippingAddress() {
			return theByXPathAddShippingAddress;
		}

		public String getTheByNameShiptoAddressLabel() {
			return theByNameShiptoAddressLabel;
		}
		
	  public String getTheByNamePersonalInfoOldPassword() {
			return theByNamePersonalInfoOldPassword;
		}

		public String getTheByNamePersonalInfoPassword() {
			return theByNamePersonalInfoPassword;
		}

		public String getTheByNamePersonalInfoConfirmPassword() {
			return theByNamePersonalInfoConfirmPassword;
		}

		public String getTheByCSSShippingAddressInfo() {
			return theByCSSShippingAddressInfo;
		}

		public String getTheByCSSEditShipingAddress() {
			return theByCSSEditShipingAddress;
		}

		public String getTheByCSSDeleteShippingAddress() {
			return theByCSSDeleteShippingAddress;
		}

		public String getTheCSSShippingAddressInfo() {
			return theByCSSShippingAddressInfo;
		}
		
		public String getTheByCSSReturnWizard() {
			return theByCSSReturnWizard;
		}

		public String getTheByCSSFourthOrderReturnWizard() {
			return theByCSSFourthOrderReturnWizard;
		}

		public String getTheByCSSFirstAmountItemsToReturn() {
			return theByCSSFirstAmountItemsToReturn;
		}

		public String getTheByNameFirstReasonToReturn() {
			return theByNameFirstReasonToReturn;
		}

		public String getTheByCSSSubmitStep2() {
			return theByCSSSubmitStep2;
		}

		public String getTheByCSSSiteError() {
			return theByCSSSiteError;
		}

		public String getTheNoItemSelectedReturnErrorMessage() {
			return theNoItemSelectedReturnErrorMessage;
		}

		public String getTheNoItemQtySelectedReturnErrorMessage() {
			return theNoItemQtySelectedReturnErrorMessage;
		}

		public String getTheNoReasonReturnErrorMessage() {
			return theNoReasonReturnErrorMessage;
		}

		public String getTheByCSSSendEmailBtn() {
			return theByCSSSendEmailBtn;
		}

		public String getTheByIdPrintReturnLabelBtn() {
			return theByIdPrintReturnLabelBtn;
		}

		public String getTheByIdEmailReturnLabelBtn() {
			return theByIdEmailReturnLabelBtn;
		}

		public String getTheByCSSRmaInfo() {
			return theByCSSRmaInfo;
		}

		public String getTheByIdEmailWizardInput() {
			return theByIdEmailWizardInput;
		}

		public String getTheByCSSSendEmailReturnWizard() {
			return theByCSSSendEmailReturnWizard;
		}

		public String getTheByCSSConfirmEmailSentLabel() {
			return theByCSSConfirmEmailSentLabel;
		}

		public String getTheByIdContinueStep4Btn() {
			return theByIdContinueStep4Btn;
		}

		public String getTheByClassNameConfirmationRMAInfo() {
			return theByClassNameConfirmationRMAInfo;
		}

		public String getTheByCSSPrintConfirmationBtn() {
			return theByCSSPrintConfirmationBtn;
		}

		public String getTheByCSSRmaNumber() {
			return theByCSSRmaNumber;
		}

		public String getThePageName() {
			return thePageName;
		}
		
		public void changePersonalInfo( String theFirstName, String theLastName, String theNewPaswrd) {
			// TODO Auto-generated method stub
			
			WebElement ce;
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNamePersonalInfoFirstName()));
			ce.clear();
			ce.sendKeys(theFirstName);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNamePersonalInfoLastName()));
			ce.clear();
			ce.sendKeys(theLastName);
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNamePersonalInfoOldPassword()));
			ce.sendKeys(myEnvironment.getTheOldPaswrd());
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNamePersonalInfoPassword()));
			ce.sendKeys(myEnvironment.getTheNewPaswrd());
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNamePersonalInfoConfirmPassword()));
			ce.sendKeys(myEnvironment.getTheNewPaswrd());
			
			String theOldPaswrd = myEnvironment.getTheNewPaswrd();
			myEnvironment.setTheNewPaswrd(myEnvironment.getTheOldPaswrd());
			myEnvironment.setTheOldPaswrd(theOldPaswrd);
			
			
			ce = myEnvironment.waitForDynamicElement( By.id(this.getTheByIdSubmitPersonalInfo()));
			ce.click();
			
		}
		
		public void addDefaultShippingAddress( String theAddressLabel, String theFirstName, String theLastName,
				String theStreetAddress, String theCity, String theState, String theZipCode,
				String theAreaCode, String thePrefix, String thePhoneNumber) {
			// TODO Auto-generated method stub
			WebElement ce;
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressLabel()));
			ce.clear();
			ce.sendKeys(theAddressLabel);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressFirstName()));
			ce.clear();
			ce.sendKeys(theFirstName);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressLastName()));
			ce.clear();
			ce.sendKeys(theLastName);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressAddressOne()));
			ce.clear();
			ce.sendKeys(theStreetAddress);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressCity()));
			ce.clear();
			ce.sendKeys(theCity);
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressState()));
		  Select theSelectedState = new Select(ce);
		  ce.click();
		  theSelectedState.selectByVisibleText(theState);
		
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPostal()));
			ce.clear();
			ce.sendKeys(theZipCode);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhoneAreaCode()));
			ce.clear();
			ce.sendKeys(theAreaCode);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhonePrefix()));
			ce.clear();
			ce.sendKeys(thePrefix);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhoneNumber()));
			ce.clear();
			ce.sendKeys(thePhoneNumber);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressDefaultAddress()));
			ce.click();
			ce = myEnvironment.waitForDynamicElement( By.id(this.getTheByIdAddrSubmit()));
			ce.click();
		}
		
		public void addDefaultBillingAddress( String theAddressLabel, String theFirstName, String theLastName,
				String theStreetAddress, String theCity, String theState, String theZipCode,
				String theAreaCode, String thePrefix, String thePhoneNumber) {
			// TODO Auto-generated method stub
			WebElement ce;
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressLabel()));
			ce.clear();
			ce.sendKeys(theAddressLabel);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressFirstName()));
			ce.clear();
			ce.sendKeys(theFirstName);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressLastName()));
			ce.clear();
			ce.sendKeys(theLastName);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressAddressOne()));
			ce.clear();
			ce.sendKeys(theStreetAddress);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressCity()));
			ce.clear();
			ce.sendKeys(theCity);
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressState()));
		  Select theSelectedState = new Select(ce);
		  ce.click();
		  theSelectedState.selectByVisibleText(theState);
		
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressPostal()));
			ce.clear();
			ce.sendKeys(theZipCode);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressPhoneAreaCode()));
			ce.clear();
			ce.sendKeys(theAreaCode);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressPhonePrefix()));
			ce.clear();
			ce.sendKeys(thePrefix);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressPhoneNumber()));
			ce.clear();
			ce.sendKeys(thePhoneNumber);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameBilltoAddressDefaultAddress()));
		
			ce.click();
			ce = myEnvironment.waitForDynamicElement( By.id(this.getTheByIdAddrSubmit()));
			ce.click();
		}
		

		public void addCC(String theCardLabel, String theCardName, String theCardBrand, String theCardMonth,
				String theCardYear) {
			// TODO Auto-generated method stub
			
			WebElement ce;
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameCreditCardLabel()));
			ce.sendKeys(theCardLabel);
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameCreditCardName()));	
			ce.sendKeys(theCardName);
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameCreditCardBrand()));
			Select select = new Select(ce);
			select.selectByVisibleText(theCardBrand);
		
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameCreditCardNumber()));
			ce.sendKeys(myEnvironment.getTheVisa());
		
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameCreditCardMonth()));
			select = new Select(ce);
			select.selectByVisibleText(theCardMonth);
			
			ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameCreditCardYear()));
			select = new Select(ce);
			select.selectByVisibleText(theCardYear);
			
			ce = myEnvironment.waitForDynamicElement( By.id(this.getTheByIdAddCCSubmit()));
			ce.click();
			
		}


		public String getTheOrderHistory() {
			return theOrderHistory;
		}


		public String getTheFirstOrder() {
			return theFirstOrder;
		}




		



		


		
		

	}

