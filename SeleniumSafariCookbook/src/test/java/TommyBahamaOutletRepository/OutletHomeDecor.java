package TommyBahamaOutletRepository;

import java.util.ArrayList;
import java.util.List;

public class OutletHomeDecor {

	private List<String> theHomeDecorLinkList = new ArrayList<String>();

	private List<String> theHomeDecorTumblersList = new ArrayList<String>();
	private List<String> theHomeDecorShopByScentList = new ArrayList<String>();
	private List<String> theHomeDecorViewAllBeddingList = new ArrayList<String>();
	private List<String> theHomeDecorViewAllScentsList = new ArrayList<String>();
	private List<String> theHomeDecorShopBeddingSetsList = new ArrayList<String>();
	private List<String> theHomeDecorViewAllBeddingSetList = new ArrayList<String>();

	private final String theByXPathShopByScent = "//a[contains(text(),'Shop By Scent')]";
	private final String theByXPath25LittleLuxuries = "//a[contains(text(),'25 Little Luxuries')]";
	private final String theByXPathPouredCandles = "//a[contains(text(),'Poured Candles')]";
	private final String theByXPathPillarCandleholders = "(//a[contains(text(),'Pillars & Candleholders')])[2]";
	private final String theByXPathDiffusers = "//a[contains(text(),'Diffusers & Potpourri')]";
	private final String theByLinkDiffusers = "Diffusers & Potpourri";
	private final String theByXPathCigarWare = "(//a[contains(text(),'Cigarware')])[2]";
	//private final String theByXPathGolf = "//a[contains(text(),'Golf')]";
	private final String theByXPathGolf = "//a[contains(text(),'Golf')]";
	private final String theByXPathMusicBooks = "(//a[contains(text(),'Music & Books')])[2]";
	private final String theByXPathHomeAccessories = "(//a[contains(text(),'Home Accessories')])[2]";
	private final String theByXPathGlasswareBar = "(//a[contains(text(),'Glassware & Bar')])[2]";
	private final String theByXPathDinnerware = "//a[contains(text(),'Linens and Dinnerware')]";

	private final String theByXPathTableLinens = "(//a[contains(text(),'Table Linens')])[2]";
	private final String theByXPathOutDoorEntertaining = "(//a[contains(text(),'Outdoor Entertaining')])[2]";
	private final String theByXPathViewAllBath = "(//a[contains(text(),'View All Bath')])[2]";
	private final String theByXPathNewArrivals = "(//a[contains(text(),'New Arrivals')])[7]";
	private final String theByXPathGuestFavs = "(//a[contains(text(),'Guest Favorites')])[5]";
	private final String theByXPathCookbook = "(//a[contains(text(),'Our Cookbook')])[2]";
	private final String theByXPathTumblers = "//a[contains(text(),'Tervis Tumbler® Collection')]";
	// private final String theByXPathBeachGear = "//li[3]/ul/li[5]/a";
	// (//a[contains(text(),'Beach Gear')])[2]
	private final String theByXPathBeachGear = "(//a[contains(text(),'Beach Gear')])[2]";
	private final String theByXPathPalmGlassware = "(//a[contains(text(),'Etched Palm Glassware')])[2]";
	private final String theByXPathIslandValues = "(//a[contains(text(),'Island Values')])[3]";
	private final String theByXPathViewAllScents = "//li[9]/ul/li/a";
	private final String theByXPathViewAllGiftDecor = "//li[@id='leftnav_on']/a";
	private final String theByXPathViewAllTableTop = "//li[12]/ul/li/a";
	private final String theByXPathViewAllBeddingBath = "(//a[contains(text(),'View All Bedding')])[2]";
	private final String theByXPathShopBeddingSets = "(//a[contains(text(),'Shop Bedding Sets')])[2]";
	private final String theByXPathViewAll = "(//a[contains(text(),'View All')])[14]";

	private final String theByLinkTextNewArrivals = "New Arrivals";
	private final String theByLink16OunceLink = "16 ounce";
	private final String theByLink24OunceLink = "24 ounce";

	private final String theByLinkIslandBlendLink = "Island Blend";
	private final String theByLinkMauiMangoLink = "Maui Mango";
	private final String theByLinkPineappleCilantroLink = "Pineapple Cilantro";
	private final String theByLinkCoconutMangoLink = "Coconut Mango";
	private final String theByLinkTropicalWinterberryLink = "Tropical Winterberry";
	private final String theByLinkSummerEscapeLink = "Summer Escape";

	private final String theByLinkJulieCayLink = "Julie Cay";
	private final String theByLinkSurfsideStripeLink = "Surfside Stripe";
	private final String theByLinkIndigoOmbreLink = "Indigo Ombre";
	private final String theByLinkKempsBayLink = "Kemps Bay";
	private final String theByLinkBahamianBreezeLink = "Bahamian Breeze";
	private final String theByLinkIslandBotanicalLink = "Island Botanical";
	private final String theByLinkOrangeCayLink = "Orange Cay";
    private final String theByLinkGolf = "Golf";
	private final String theByLinkQueenLink = "Queen";
	private final String theByLinkKingLink = "King";
	private final String theByLinkCaliforniaKingLink = "California King";
	
	
	
	private final String thePageName = "HomeDecor";

	public void createShopBeddingSetsList() {
		theHomeDecorShopBeddingSetsList.add(this.getTheByLinkJulieCayLink());
		theHomeDecorShopBeddingSetsList.add(getTheByLinkSurfsideStripeLink());
		theHomeDecorShopBeddingSetsList.add(getTheByLinkIndigoOmbreLink());
		theHomeDecorShopBeddingSetsList.add(getTheByLinkKempsBayLink());
		theHomeDecorShopBeddingSetsList.add(getTheByLinkBahamianBreezeLink());
		theHomeDecorShopBeddingSetsList.add(getTheByLinkIslandBotanicalLink());
		theHomeDecorShopBeddingSetsList.add(getTheByLinkOrangeCayLink());
	}

	public void createShopByScentList() {
		theHomeDecorShopByScentList.add(this.getTheByLinkIslandBlendLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkMauiMangoLink());
		theHomeDecorShopByScentList.add(this
				.getTheByLinkPineappleCilantroLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkCoconutMangoLink());
		theHomeDecorShopByScentList.add(this
				.getTheByLinkTropicalWinterberryLink());
		theHomeDecorShopByScentList.add(this.getTheByLinkSummerEscapeLink());

	}

	public void createShopByScentViewAllList() {
		theHomeDecorViewAllScentsList.add(this.getTheByXPathViewAllScents());

	}

	public void createShopBeddingSetViewAllList() {
		theHomeDecorViewAllBeddingSetList.add(this
				.getTheByXPathViewAllBeddingBath());

	}

	public void createViewAllBeddingList() {
		theHomeDecorViewAllBeddingList.add(this.getTheByLinkQueenLink());
		theHomeDecorViewAllBeddingList.add(this.getTheByLinkKingLink());
		theHomeDecorViewAllBeddingList.add(this
				.getTheByLinkCaliforniaKingLink());
	}

	public void createHomeDecorTumblerList() {
		theHomeDecorTumblersList.add(this.getTheByLink16OunceLink());
		theHomeDecorTumblersList.add(this.getTheByLink24OunceLink());
	}

	public void createHomeDecorLinkList() {
		theHomeDecorLinkList.add(this.getTheByXPathViewAllScents());
		theHomeDecorLinkList.add(this.getTheByXPathNewArrivals());
		theHomeDecorLinkList.add(this.getTheByXPathIslandValues());
		theHomeDecorLinkList.add(this.getTheByXPathPalmGlassware());
		theHomeDecorLinkList.add(this.getTheByXPathBeachGear());
		theHomeDecorLinkList.add(this.getTheByXPathTumblers());

		theHomeDecorLinkList.add(this.getTheByXPathPouredCandles());
		theHomeDecorLinkList.add(this.getTheByXPathDiffusers());
		theHomeDecorLinkList.add(this.getTheByXPathCigarWare());
		theHomeDecorLinkList.add(this.getTheByXPathGolf());
		theHomeDecorLinkList.add(this.getTheByXPathMusicBooks());
		theHomeDecorLinkList.add(this.getTheByXPathHomeAccessories());
		theHomeDecorLinkList.add(this.getTheByXPathGlasswareBar());
		theHomeDecorLinkList.add(this.getTheByXPathTableLinens());
		theHomeDecorLinkList.add(this.getTheByXPathOutDoorEntertaining());
		theHomeDecorLinkList.add(this.getTheByXPathViewAllBath());
		theHomeDecorLinkList.add(this.getTheByXPathDinnerware());
		theHomeDecorLinkList.add(this.getTheByXPathShopByScent());
		// theHomeDecorLinkList.Add(LittleLuxuries);
		theHomeDecorLinkList.add(this.getTheByXPathViewAllTableTop());
		theHomeDecorLinkList.add(this.getTheByXPathViewAllBeddingBath());
		theHomeDecorLinkList.add(this.getTheByXPathViewAllGiftDecor());
	}

	public List<String> getTheHomeDecorLinkList() {
		return theHomeDecorLinkList;
	}

	public List<String> getTheHomeDecorTumblersList() {
		return theHomeDecorTumblersList;
	}

	public List<String> getTheHomeDecorShopByScentList() {
		return theHomeDecorShopByScentList;
	}

	public List<String> getTheHomeDecorViewAllBeddingList() {
		return theHomeDecorViewAllBeddingList;
	}

	public List<String> getTheHomeDecorViewAllScentsList() {
		return theHomeDecorViewAllScentsList;
	}

	public List<String> getTheHomeDecorShopBeddingSetsList() {
		return theHomeDecorShopBeddingSetsList;
	}

	public List<String> getTheHomeDecorViewAllBeddingSetList() {
		return theHomeDecorViewAllBeddingSetList;
	}

	public String getTheByXPathShopByScent() {
		return theByXPathShopByScent;
	}

	public String getTheByXPath25LittleLuxuries() {
		return theByXPath25LittleLuxuries;
	}

	public String getTheByXPathPouredCandles() {
		return theByXPathPouredCandles;
	}

	public String getTheByXPathPillarCandleholders() {
		return theByXPathPillarCandleholders;
	}

	public String getTheByXPathDiffusers() {
		return theByXPathDiffusers;
	}

	public String getTheByLinkDiffusers() {
		return theByLinkDiffusers;
	}

	public String getTheByXPathCigarWare() {
		return theByXPathCigarWare;
	}

	public String getTheByXPathGolf() {
		return theByXPathGolf;
	}

	public String getTheByXPathMusicBooks() {
		return theByXPathMusicBooks;
	}

	public String getTheByXPathHomeAccessories() {
		return theByXPathHomeAccessories;
	}

	public String getTheByXPathGlasswareBar() {
		return theByXPathGlasswareBar;
	}

	public String getTheByXPathDinnerware() {
		return theByXPathDinnerware;
	}

	public String getTheByXPathTableLinens() {
		return theByXPathTableLinens;
	}

	public String getTheByXPathOutDoorEntertaining() {
		return theByXPathOutDoorEntertaining;
	}

	public String getTheByXPathViewAllBath() {
		return theByXPathViewAllBath;
	}

	public String getTheByXPathNewArrivals() {
		return theByXPathNewArrivals;
	}

	public String getTheByXPathGuestFavs() {
		return theByXPathGuestFavs;
	}

	public String getTheByXPathCookbook() {
		return theByXPathCookbook;
	}

	public String getTheByXPathTumblers() {
		return theByXPathTumblers;
	}

	public String getTheByXPathBeachGear() {
		return theByXPathBeachGear;
	}

	public String getTheByXPathPalmGlassware() {
		return theByXPathPalmGlassware;
	}

	public String getTheByXPathIslandValues() {
		return theByXPathIslandValues;
	}

	public String getTheByXPathViewAllScents() {
		return theByXPathViewAllScents;
	}

	public String getTheByXPathViewAllGiftDecor() {
		return theByXPathViewAllGiftDecor;
	}

	public String getTheByXPathViewAllTableTop() {
		return theByXPathViewAllTableTop;
	}

	public String getTheByXPathViewAllBeddingBath() {
		return theByXPathViewAllBeddingBath;
	}

	public String getTheByXPathShopBeddingSets() {
		return theByXPathShopBeddingSets;
	}

	public String getTheByXPathViewAll() {
		return theByXPathViewAll;
	}

	public String getTheByLink16OunceLink() {
		return theByLink16OunceLink;
	}

	public String getTheByLink24OunceLink() {
		return theByLink24OunceLink;
	}

	public String getTheByLinkIslandBlendLink() {
		return theByLinkIslandBlendLink;
	}

	public String getTheByLinkMauiMangoLink() {
		return theByLinkMauiMangoLink;
	}

	public String getTheByLinkPineappleCilantroLink() {
		return theByLinkPineappleCilantroLink;
	}

	public String getTheByLinkCoconutMangoLink() {
		return theByLinkCoconutMangoLink;
	}

	public String getTheByLinkTropicalWinterberryLink() {
		return theByLinkTropicalWinterberryLink;
	}

	public String getTheByLinkSummerEscapeLink() {
		return theByLinkSummerEscapeLink;
	}

	public String getTheByLinkJulieCayLink() {
		return theByLinkJulieCayLink;
	}

	public String getTheByLinkSurfsideStripeLink() {
		return theByLinkSurfsideStripeLink;
	}

	public String getTheByLinkIndigoOmbreLink() {
		return theByLinkIndigoOmbreLink;
	}

	public String getTheByLinkKempsBayLink() {
		return theByLinkKempsBayLink;
	}

	public String getTheByLinkBahamianBreezeLink() {
		return theByLinkBahamianBreezeLink;
	}

	public String getTheByLinkIslandBotanicalLink() {
		return theByLinkIslandBotanicalLink;
	}

	public String getTheByLinkOrangeCayLink() {
		return theByLinkOrangeCayLink;
	}

	public String getTheByLinkQueenLink() {
		return theByLinkQueenLink;
	}

	public String getTheByLinkKingLink() {
		return theByLinkKingLink;
	}

	public String getTheByLinkCaliforniaKingLink() {
		return theByLinkCaliforniaKingLink;
	}

	public String getTheByLinkTextNewArrivals() {
		return theByLinkTextNewArrivals;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByLinkGolf() {
		return theByLinkGolf;
	}

}
