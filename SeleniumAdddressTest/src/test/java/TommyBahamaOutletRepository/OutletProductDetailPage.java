package TommyBahamaOutletRepository;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

// This is the Product Detail Page Class that contains the element repository for the page and methods that 
// execute functionality that is centered around the PDP. 
public class OutletProductDetailPage {
	private final String theByXPathFirstSize = "//li/div[2]/div[2]/div/a";
	private final String theByLinkMediumSize = "M";
	private final String theByXPathSecondSize = "//div[2]/div[2]/a";
	private final String theByCSSProductPrice = "p.content_price";
	private final String theByXPathThirdSize = "//div[@id='XXL']";
	private final String theByLinkTextXLargeSize = "XL";
	private final String theByLinkTextLargeSize = "L";
	private final String theByLinkText34By32Size = "34 x 32";
	private final String theByLinkText36By32Size = "36 x 32";
	private final String theByXpathSizeEleven = "(//a[contains(text(),'11')])[2]";
	private final String theByXpathAddToBagButton = "//form[@id='addToCartForm']/div[2]/div[3]/div";
	private final String theByCSSAddToBagLink = "#aBtnAddToBagLink";
	private final String theByIdAddToBagLink = "aBtnAddToBagLink";
	private final String theByIdAddToBagBtn = "divAddToBagButton";
	private final String theByOneXByBigLink = "//div[@id='1X x BG']/a";
	private final String theFirstCrossSellItem = "//p/a";
	private final String theByIdSelectQuantity = "qty";
	private final String theProductSize = "//div[@id='divProductSize']/div/div/a";
	private final String theHelpLink = "(Help)";
	private final String theQuantityFour = "4";
	private final String theQuantityTwo = "2";
	private final String theByIDSelectQty = "selQuantity";
	private final String theUrlType = "http://";
	private final String theByCssRestrictionLink = "span.shipping_restrictions > a.tb_modal";
	private final String theByCssRestrictionText = "span.shipping_restrictions";
	private final String theByXPathSwatchDescription = "//span[@id='swatch_description']";
	private final String theMainImage = "//*[@id='divMainImageContainer']";
	private final String theMainProduct = "//div[@id='divMainProductContainer']/div[2]";
	
	private final String theByCSSFinalSaleLink = "span.final_sale.red > a.tb_modal";
	private final String theByCssFinalSaleText = "span.final_sale.red";
	private final String theByCssProductDiscount = "span.cart_item_alert";
	
	private final String thePageName = "PDP";
	private final String thePreviewPaneName = "ProductPreview";
	
	private WebDriver driver;
	private OutletWebdriverEnvironment myEnvironment;
	private OutletProductListingPage myProductListingObjs;
	private OutletHeader myOutletHeader;
	private String testName;
	
	public OutletProductDetailPage(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment,
			OutletProductListingPage theProductListingObjs, OutletHeader theOutletHeader,
		    String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myProductListingObjs = theProductListingObjs;
		myOutletHeader = theOutletHeader;
		testName = theTestName;
	}

	public String getTheByIdAddToBagBtn() {
		return theByIdAddToBagBtn;
	}

	public String getTheByIdAddToBagLink() {
		return theByIdAddToBagLink;
	}

	public String getTheMainProduct() {
		return theMainProduct;
	}

	public String getTheByXPathFirstSize() {
		return theByXPathFirstSize;
	}

	public String getTheByLinkMediumSize() {
		return theByLinkMediumSize;
	}

	public String getTheByXPathSecondSize() {
		return theByXPathSecondSize;
	}

	public String getTheByCSSProductPrice() {
		return theByCSSProductPrice;
	}

	public String getTheByXPathThirdSize() {
		return theByXPathThirdSize;
	}

	public String getTheByLinkTextXLargeSize() {
		return theByLinkTextXLargeSize;
	}

	public String getTheByLinkTextLargeSize() {
		return theByLinkTextLargeSize;
	}

	public String getTheByLinkText34By32Size() {
		return theByLinkText34By32Size;
	}

	public String getTheByLinkText36By32Size() {
		return theByLinkText36By32Size;
	}

	public String getTheByXpathSizeEleven() {
		return theByXpathSizeEleven;
	}

	public String getTheByXpathAddToBagButton() {
		return theByXpathAddToBagButton;
	}

	public String getTheByCSSAddToBagLink() {
		return theByCSSAddToBagLink;
	}

	public String getTheByOneXByBigLink() {
		return theByOneXByBigLink;
	}

	public String getTheFirstCrossSellItem() {
		return theFirstCrossSellItem;
	}

	public String getTheByIdSelectQuantity() {
		return theByIdSelectQuantity;
	}

	public String getTheProductSize() {
		return theProductSize;
	}

	public String getTheHelpLink() {
		return theHelpLink;
	}

	public String getTheQuantityFour() {
		return theQuantityFour;
	}

	public String getTheQuantityTwo() {
		return theQuantityTwo;
	}

	public String getTheByIDSelectQty() {
		return theByIDSelectQty;
	}

	public String getTheUrlType() {
		return theUrlType;
	}


	public String getTheByXPathSwatchDescription() {
		return theByXPathSwatchDescription;
	}

	public String getTheMainImage() {
		return theMainImage;
	}
	
	public String getThePageName() {
		return thePageName;
	}

	public String getThePreviewPaneName() {
		return thePreviewPaneName;
	}

	public String getTheByCssRestrictionLink() {
		return theByCssRestrictionLink;
	}

	/*
	 * This method is a universal method to add products by price to the
	 * shopping bag that are located in sub categories of a section An example
	 * of this is Socks under Mens Accessories. This method is passed the
	 * Webdriver and Environmental objects, the product listing and header
	 * objects The method is also passed the tab to select the first and sub
	 * link to select and the price and quantity
	 */
	public void selectSubProductByPrice(
			String theTab, String theLink, String sublink, String thePrice,
			String theQuantity, String theProductTitle) throws InterruptedException {
		WebElement ce;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce.click();
	
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(sublink));
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle + thePrice);
		String theSelectedItem = "";
		theSelectedItem = this.findWantedUSPrice(thePrice);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		System.out
				.println("THis is the selected item       " + theSelectedItem);
		ce = driver.findElement(By.xpath(theSelectedItem));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				thePrice);
		this.addQuantityToBag( theQuantity,	 thePrice);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag that are located in sub categories of a section An example
	 * of this is Socks under Mens Accessories. This method is passed the
	 * Webdriver and Environmental objects, the product listing and header
	 * objects The method is also passed the tab to select the first and sub
	 * link to select and the product and quantity
	 */
	public void selectSubProductByProduct(
			String theTab, String theLink, String sublink, String theProduct,
			String theQuantity) throws InterruptedException {
		WebElement ce;

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
		myEnvironment.waitForPageLoaded();
		Thread.sleep(2000);
	
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.linkText(sublink));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				"Products");
		WebElement theSelectedItem;
		theSelectedItem = this.findProduct(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = theSelectedItem;
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);
		this.addQuantityToBag(theQuantity);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag This method is passed the Webdriver and Environmental
	 * objects, the product listing and header objects The method is also passed
	 * the tab to select the first link to select and the product and quantity
	 */
	public void selectProduct(String theTab, String theLink, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {

		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
	//	myEnvironment.waitForTitle(theProductTitle);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

		WebElement theSelectedItem;
		theSelectedItem = this.findProduct(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = theSelectedItem;
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSelectQuantity()));
				
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);

		this.addQuantityToBag(theQuantity);
	}
	
	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag This method is passed the Webdriver and Environmental
	 * objects, the product listing and header objects The method is also passed
	 * the tab to select the first link to select and the product and quantity
	 */
	public void selectProduct(String theTab, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {

		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
	
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

		WebElement theSelectedItem;
		theSelectedItem = this.findProduct(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = theSelectedItem;
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSelectQuantity()));
				
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);

		this.addQuantityToBag(theQuantity);
	}
	
	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag This method is passed the Webdriver and Environmental
	 * objects, the product listing and header objects The method is also passed
	 * the tab to select the first link to select and the product and quantity
	 */
	public void selectProductColorVariant(String theTab, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {

		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
	
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

		WebElement theSelectedItem;
		theSelectedItem = this.findProductColorVariant(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = theSelectedItem;
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSelectQuantity()));
				
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);

		this.addQuantityToBag(theQuantity);
	}
	
	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag This method is passed the Webdriver and Environmental
	 * objects, the product listing and header objects The method is also passed
	 * the tab to select the first link to select and the product and quantity
	 */
	public void selectProduct(String theTab, 
			String theQuantity, String theProductTitle) throws InterruptedException {

		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
	
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

	
		// selects whatever first product is here. 
		ce = myEnvironment.waitForDynamicElement(By.xpath("//span/img"));
		
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSelectQuantity()));
				
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				"random product");

		this.addQuantityToBag(theQuantity);
	}

	public void selectProductByHover(String theTab, String theLink, String theProduct,
			String theQuantity, String theProductTitle) throws InterruptedException {
		WebElement ce = myEnvironment.waitForDynamicElement(
				By.xpath(theTab));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(ce).perform();// move to list element
														// that needs to be
														// hovered

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		// action = new Actions(driver);//simply my webdriver
		// action.moveToElement(ce).perform();//move to list element
		// that needs to be hovered
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle);

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		WebElement theSelectedItem;
		theSelectedItem = this.findProduct(theProduct);
		Thread.sleep(myEnvironment.getThe_Default_Sleep());

		ce = theSelectedItem;
		ce.click();

		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProduct);
		this.addQuantityToBag(theQuantity);
	}

	/*
	 * This method is a universal method to add products by product to the
	 * shopping bag by the products price This method is passed the Webdriver
	 * and Environmental objects, the product listing and header objects The
	 * method is also passed the tab to select the first link to select and the
	 * price and quantity
	 */
	public void selectProductByPrice(
			String theTab, String theLink, String thePrice, String theQuantity,
			 String theProductTitle)
			throws InterruptedException {
		WebElement ce = myEnvironment.fluentWait(
				By.xpath(theTab));
		ce.click();

		ce = myEnvironment.fluentWait(
				By.xpath(myOutletHeader.getTheFooterEmailBtn()));
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(ce).perform();// move to list element
														// that needs to be
														// hovered
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theLink));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='footerEmailSignUpInner']/form/div"));
		String thePriceWithoutSpaces = thePrice;
		thePriceWithoutSpaces = thePriceWithoutSpaces.replace(" ", "_");
		System.out.println("this is the Price with a underscore included .                 "  +    thePriceWithoutSpaces);
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PLP",
				theProductTitle + thePriceWithoutSpaces);
		String theSelectedItem = "";
		theSelectedItem = this.findWantedUSPrice(thePrice);
		System.out.println("This is the selected item:        "
				+ theSelectedItem);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(theSelectedItem));
		ce.click();
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		ce = myEnvironment.fluentWait(
				By.xpath(this.getTheMainImage()));

		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, "PDP",
				theProductTitle + thePriceWithoutSpaces);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		this.addQuantityToBag( theQuantity,	 thePrice);
	}

	/*
	 * This method finds a product by the product id that is located in the
	 * src attribute of an element.  This method is passed the
	 * product id of the product that is going to be selected.
	 */
	public WebElement findProduct(String theProduct) {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
		WebElement ce;
		if (myEnvironment.IsElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			ce.click();
		}		
		List<WebElement> list = driver.findElements(By.className("lazy")); 		
		
		WebElement Product = null;
		String productCode;
		
		for(WebElement cwe : list) {			
			productCode = cwe.getAttribute("data-original");
			productCode = productCode.substring(47);
			System.out.println(productCode);
			int index = productCode.indexOf("_");
			
			System.out.println("This is the index of of the _               "              +   index);
			
			
			System.out.println("This is the product codes length             "   + productCode.length());
		
			productCode = productCode.substring(0, index);
			
			if(productCode.contains(theProduct)){
			return Product = cwe; 
			} 
		}
		
		
		
		
	/*	if(driver.getCurrentUrl().contains("dev")) {
		String thumbNailStart_i = "/html/body/div[3]/div/div[4]/div[3]/div[2]/div[2]/div[";
		String thumbNailEnd_i = "]/div[";
		String thumbNailEnd_j = "]/div/div/a/span/img";
		
		for (int i = 1; i < 6; i++) {
			for (int j = 1; j < 4; j++) {
			ce = myEnvironment.waitForClickableElement(
					By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j +  thumbNailEnd_j));
			Product = myProductListingObjs.pullProductCodeFromThumbNail(
					thumbNailStart_i, thumbNailEnd_i, thumbNailEnd_j,  i, j);
			if (Product.contains(theProduct)) {
				Product = thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j;
				return Product;
			}
			}
		}
		
		}
		else {
			String thumbNailStart_i = "/html/body/div[3]/div/div[4]/div[4]/div[2]/div[2]/div[";
			String thumbNailEnd_i = "]/div[";
			String thumbNailEnd_j = "]/div/div/a/span/img";
			
			for (int i = 1; i < 6; i++) {
				for (int j = 1; j < 4; j++) {
				ce = myEnvironment.waitForClickableElement(
						By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j +  thumbNailEnd_j));
				Product = myProductListingObjs.pullProductCodeFromThumbNail(
						thumbNailStart_i, thumbNailEnd_i, thumbNailEnd_j,  i, j);
				if (Product.contains(theProduct)) {
					Product = thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j;
					return Product;
				}
				}
			}
		}*/

		return Product;
	}

	
	/*
	 * This method finds a product by the product id that is located in the
	 * src attribute of an element.  This method is passed the
	 * product id of the product that is going to be selected.
	 */
	public WebElement findProductColorVariant(String theProduct) {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
		WebElement ce;
		if (myEnvironment.IsElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			ce.click();
		}		
		List<WebElement> list = driver.findElements(By.className("lazy")); 		
		
		WebElement Product = null;
		String productCode;
		
		for(WebElement cwe : list) {			
			productCode = cwe.getAttribute("data-original");
			productCode = productCode.substring(47);
			System.out.println(productCode);
			
			int index = OutletProductDetailPage.nthOccurrence(productCode, '_', 1);
			
			System.out.println("This is the index of of the _               "              +   index);
			
			
			System.out.println("This is the product codes length             "   + productCode.length());
		
			productCode = productCode.substring(0, index);
			
			System.out.println("This is the current product code:      " + productCode);
			System.out.println("This is the wanted product code:       " + theProduct);
		
			if(productCode.contains(theProduct)){
			return Product = cwe; 
			} 
		}
		
		
		
		
	/*	if(driver.getCurrentUrl().contains("dev")) {
		String thumbNailStart_i = "/html/body/div[3]/div/div[4]/div[3]/div[2]/div[2]/div[";
		String thumbNailEnd_i = "]/div[";
		String thumbNailEnd_j = "]/div/div/a/span/img";
		
		for (int i = 1; i < 6; i++) {
			for (int j = 1; j < 4; j++) {
			ce = myEnvironment.waitForClickableElement(
					By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j +  thumbNailEnd_j));
			Product = myProductListingObjs.pullProductCodeFromThumbNail(
					thumbNailStart_i, thumbNailEnd_i, thumbNailEnd_j,  i, j);
			if (Product.contains(theProduct)) {
				Product = thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j;
				return Product;
			}
			}
		}
		
		}
		else {
			String thumbNailStart_i = "/html/body/div[3]/div/div[4]/div[4]/div[2]/div[2]/div[";
			String thumbNailEnd_i = "]/div[";
			String thumbNailEnd_j = "]/div/div/a/span/img";
			
			for (int i = 1; i < 6; i++) {
				for (int j = 1; j < 4; j++) {
				ce = myEnvironment.waitForClickableElement(
						By.xpath(thumbNailStart_i + i + thumbNailEnd_i + j +  thumbNailEnd_j));
				Product = myProductListingObjs.pullProductCodeFromThumbNail(
						thumbNailStart_i, thumbNailEnd_i, thumbNailEnd_j,  i, j);
				if (Product.contains(theProduct)) {
					Product = thumbNailStart_i + i + thumbNailEnd_i + j + thumbNailEnd_j;
					return Product;
				}
				}
			}
		}*/

		return Product;
	}

	
	
	public String findWantedUSPrice( String theWantedPrice) {
		String theProduct = "";
		WebElement ce;
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//div[@id='imgContainer_" + 0 + "']/div[2]/div"));
		if (myEnvironment.IsElementPresent(
				By.xpath(myProductListingObjs.getTheShowAllLink()))) {
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(myProductListingObjs.getTheShowAllLink()));
			ce.click();
			// driver.FindElement(By.XPath(myProductListingObjs.ShowAllLink)).Click();
		}
		for (int i = 0; i < 20; i++) {
			String Price = "";
			ce = myEnvironment.waitForDynamicElement(
					By.xpath("//div[@id='imgContainer_" + i + "']/div[2]/div"));
			// myEnvironment.myWaitForWDElement(driver,
			// By.XPath("//div[@id='imgContainer_" + i + "']/div[2]/div"));

			Price = ce.getText();
			Price = Price.trim();
			Assert.assertTrue(Price.contains("$") || Price.contains("GBP"));

			System.out.println("This is the price:       " + Price);
			System.out.println("This is the wanted price      "
					+ theWantedPrice);
			if (Price.contains(theWantedPrice)) {
				theProduct = "//div[@id='cat_list_title_" + i + "']/a";
				return theProduct;
			}
		}
		return theProduct;
	}

	/*
	 * This method adds products to the shopping bag This is passed the normal
	 * objects and the quantity of the object that is to be added to the
	 * shopping bag. For some reason find element combined with the click method
	 * can throw a timed out error. in some instances its needed to put the
	 * element into the IWebElement object and then find it sleep for 205 ms and
	 * then click it Hopefully this bug will be fixed soon or I get an upgrade
	 * on my machine to execute the methods(says this happens on slower
	 * machines). I have done this in places I do not think its required. It is
	 * Required presently in situations like this one where the test is
	 * selecting a value from a drop down. After this it is necesssary to
	 * perfrom the break up that is described above.
	 */
	public void addQuantityToBag(String theQuantity,
			 String thePrice) throws InterruptedException {
		// driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
		WebElement ce;
	
		if (myEnvironment.IsElementPresent(By.xpath(this.getTheProductSize()))) {
			for (int i = 0; i < 10; i++) {
				if (myEnvironment.IsElementPresent(
						By.xpath("//div[2]/div[2]/div[" + i + "]/a"))) {
					ce = myEnvironment.waitForDynamicElement(
							By.xpath("//div[2]/div[2]/div[" + i + "]/a"));
					System.out.println(ce.getAttribute("style"));
					if (ce.getAttribute("style").contains(
							"rgb(51, 103, 153)")
							|| ce.getAttribute("style").contains(
									"#336799")) {
						System.out
								.println("We are at the click so the size should be selected.");
						ce.click();

						break;
					}
				}
			}
		
			if (theQuantity == "8") {
				ce = driver.findElement(By.id(this
						.getTheByIdSelectQuantity()));
				ce = ce.findElement(By
						.cssSelector("option[Value='8']"));	
				ce.click();		
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));
				myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
						"PDP", thePrice);			
				ce.click();
			} else {
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				Select clickThis = new Select(ce);
				Thread.sleep(1000);
				clickThis.selectByValue(theQuantity);
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));	
				ce.click();
	
			}
		} else {
			// This is code to add multiples of the same items to the cart at
			// the same time.
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdSelectQuantity()));
			Select clickThis = new Select(ce);
			clickThis.selectByValue(theQuantity);
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdAddToBagLink()));	
			ce.click();	
		}	
	}

	/*
	 * This method adds products to the shopping bag This is passed the normal
	 * objects and the quantity of the object that is to be added to the
	 * shopping bag. For some reason find element combined with the click method
	 * can throw a timed out error. in some instances its needed to put the
	 * element into the IWebElement object and then find it sleep for 205 ms and
	 * then click it Hopefully this bug will be fixed soon or I get an upgrade
	 * on my machine to execute the methods(says this happens on slower
	 * machines). I have done this in places I do not think its required. It is
	 * Required presently in situations like this one where the test is
	 * selecting a value from a drop down. After this it is necesssary to
	 * perfrom the break up that is described above.
	 */
	public void addQuantityToBag(String theQuantity)
			throws InterruptedException 				{
		
		WebElement ce;

		if (myEnvironment.IsElementPresent(By.xpath("//form[@id='addToCartForm']/div[2]/div[2]/div[2]/span"))) {
			
			System.out.println("We are in the if statement for add quantity to bag.");
			for (int i = 0; i < 50; i++) {
				if (myEnvironment.IsElementPresent(
						By.xpath("//form[@id='addToCartForm']/div[2]/div[2]/div[2]/span[" + i + "]") 
						)) {
					ce = myEnvironment.waitForDynamicElement(
							By.xpath("//form[@id='addToCartForm']/div[2]/div[2]/div[2]/span[" + i + "]"));
					System.out.println(ce.getAttribute("class"));
				
				/*	if (ce.getAttribute("style").contains(
							"rgb(51, 103, 153)")
							|| ce.getAttribute("style").contains(
									"#336799")) {*/
					if(!ce.getAttribute("class").contains("size blue unavailable")) {
						System.out
								.println("We are at the click so the size should be selected.            "  + ce.getAttribute("class"));
						ce.click();
						break;
					}
				}
			}
			
			}
			// This is code to add multiples of the same items to the cart at
			// the same time.
			if (theQuantity == "8") {
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdSelectQuantity()));
				Select clickThis = new Select(ce);
				clickThis.selectByValue(theQuantity);
				ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdAddToBagLink()));
				myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName,
						"PDP", "8AddedToBag");
					ce.click();
		
			
		} else {
	
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdSelectQuantity()));
		    Select clickThis = new Select(ce);
			clickThis.selectByValue(theQuantity);
			//Thread.sleep(myEnvironment.getThe_Default_Sleep());
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathAddToBagButton()));		
			ce.click();
		

		}
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
	}
	
	public static int nthOccurrence(String str, char c, int n) {
	    int pos = str.indexOf(c, 0);
	    while (n-- > 0 && pos != -1)
	        pos = str.indexOf(c, pos+1);
	    return pos;
	}

	public String getTheByCssRestrictionText() {
		return theByCssRestrictionText;
	}


	public String getTheByCssFinalSaleText() {
		return theByCssFinalSaleText;
	}

	public String getTheByCSSFinalSaleLink() {
		return theByCSSFinalSaleLink;
	}

	public String getTheByCssProductDiscount() {
		return theByCssProductDiscount;
	}



}
