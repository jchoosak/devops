package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OutletHAC {
	
	private final String theUsrName = "admin";
	private final String thePswrd = "nimda";
	private final String theByIdMonitoringButton = "monitoring";
	private final String theByLinkCronJobs = "cron jobs";
	private final String theByCSSemptyList = "td.dataTables_empty";
	private WebDriver driver;
	private int cronJobsLoopRan = 1;
	private String testName;
	

	private OutletWebdriverEnvironment myEnvironment;
	
public OutletHAC() {

}

public OutletHAC(WebDriver theDriver,
		OutletWebdriverEnvironment theEnvironment,
	     String theTestName) {

	driver = theDriver;
	myEnvironment = theEnvironment;
	testName = theTestName;

}

public void checkCronJobsFinished(OutletSignInPage mySignInPage) throws InterruptedException {
	// TODO Auto-generated method stub
	WebElement ce;
	if(driver.getCurrentUrl().contains("qa"))
	driver.get(myEnvironment.getTheHACTestingEnvironment());
	else  driver.get(myEnvironment.getTheHACDevTestingEnvironment());
	
	System.out.println("We are not even getting into method for the checking the cron job ");
	if (myEnvironment.IsElementPresent(By.name(mySignInPage
			.getTheHACOutletUsernameName()))) {
		System.out.println("We are not even getting into the sign in page for somereason ");
		mySignInPage.signInHAC(theUsrName, thePswrd);
	}

    ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdMonitoringButton()));
    ce.click();
    ce = myEnvironment.waitForDynamicElement(By.xpath("//a[contains(@href, '/hac/monitoring/cronjobs')]"));
    ce.click();
    
    while(!myEnvironment.IsElementPresent(By.cssSelector(this.theByCSSemptyList))){
    	Thread.sleep(100);
    	cronJobsLoopRan = cronJobsLoopRan + 1;
    	if(cronJobsLoopRan > 10000)
    		break;
    }
}

public String getTheByIdMonitoringButton() {
	return theByIdMonitoringButton;
}

public String getTheByLinkCronJobs() {
	return theByLinkCronJobs;
}

public String getTheByCSSemptyList() {
	return theByCSSemptyList;
}
}
