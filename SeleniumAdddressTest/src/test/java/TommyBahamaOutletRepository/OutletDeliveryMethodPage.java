package TommyBahamaOutletRepository;

public class OutletDeliveryMethodPage {
	
	private final String theByIdContinueCheckoutButton = "chooseDeliveryMethod_continue_button";
	private final String thePageName = "DeliveryMethodPage";
	private final String theByCSSFreeShippingPromo = "dt.promo";
	private final String theByIdFDXGround = "FDXGROUND";
	private final String theByIdFDXSmartPost = "FDXSMARTPOST";
	private final String theByIdFDX3Day = "FDX3DAY";
	private final String theByIdFDX2Day = "FDX2DAY";
	private final String theByIdFDXNextDay = "FDXNEXTDAY";
	private final String theByClassAddress = "item_container";
	private final String theByXpathShippingAmount = "//div[@id='content']/div[3]/div/div/div[2]/div/div[2]/ul/li[2]";

	
	
	public String getTheByIdContinueCheckoutButton() {
		return theByIdContinueCheckoutButton;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByClassAddress() {
		return theByClassAddress;
	}

	public String getTheByCSSFreeShippingPromo() {
		return theByCSSFreeShippingPromo;
	}

	public String getTheByIdFDXGround() {
		return theByIdFDXGround;
	}

	public String getTheByXpathShippingAmount() {
		return theByXpathShippingAmount;
	}

	public String getTheByIdFDX3Day() {
		return theByIdFDX3Day;
	}

	public String getTheByIdFDX2Day() {
		return theByIdFDX2Day;
	}

	public String getTheByIdFDXNextDay() {
		return theByIdFDXNextDay;
	}

	public String getTheByIdFDXSmartPost() {
		return theByIdFDXSmartPost;
	}
	

}
