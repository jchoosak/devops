package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OutletSignInPage {
	private final String signInLink = "signin";
	// private final String signInLink = "Sign In or Register";

	private final String byXPathUserNameObj = "//input[@id='j_username']";
	private final String byXpathPasWrd = "//input[@id='j_password']";
	private final String byXPathSignInButton = "//button[@type='submit']";
	private final String theByXpathHACsignInButton = "//button";
	private final String theCanadaUsername = "ecommtest5@tommybahama.com";
	private final String theSBUsername = "ecommtest4@tommybahama.com";
	private final String theEndToEndUsername = "ecommtest3@tommybahama.com";
	private final String theWAUsername = "ecommtest@tommybahama.com";
	private final String theCALUsername = "ecommtest2@tommybahama.com";
	private final String theUsername = "jack.west@tommybahama.com";
	private final String theCanadaFourUserName = "ecommtest4@tommybahama.com";
	private final String theWAGCUserName = "ecommtest7@tommybahama.com";
	private final String theUSSplitTenderUserName = "ecommtest8@tommybahama.com";
	private final String theCanadaNineUserName = "ecommtest9@tommybahama.com";
	private final String theCanadaTenUserName = "ecommtest10@tommybahama.com";
	private final String theIEUserName = "tb@tb.com";
	private final String theIEPasswrd = "testing";
	private final String thePassword = "testing123";
	private final String theContinueAsGuestBtn = "//a[contains(text(),'CONTINUE AS A GUEST')]";
	
	
	private final String theOutletUsernameId = "j_username";
	private final String theCSCOutletUsernameId = "j_username";
	private final String theOutletPasswordId = "j_password";
	private final String theCSCOutletPasswordId = "j_password";
	private final String theCSCOutletLoginButton = "td.z-button-cm";
	
	private final String theHMCOutletUsernameId = "Main_user";
	private final String theHMCOutletPasswordId = "Main_password";
	private final String theHMCOutletLoginButton = "Main_label";
	
	private final String theHACOutletUsernameName = "j_username";
	private final String theHACOutletPasswordName = "j_password";
	private final String theHACOutletLoginButton = "Main_label";
	// private final String default_wait = "600000";
	
	private WebDriver driver;
	private OutletWebdriverEnvironment myEnvironment;
	private OutletHeader myOutletHeader;
	private String testName;
	
	private final String thePageName = "SignInPage";
	
	public OutletSignInPage(WebDriver theDriver, OutletWebdriverEnvironment theEnvironment,
			OutletHeader theOutletHeader,String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
		myOutletHeader = theOutletHeader;
		testName = theTestName;
		
	}

	public OutletSignInPage(WebDriver theDriver, OutletWebdriverEnvironment theEnvironment
			,String theTestName)
	{
		driver = theDriver;
		myEnvironment = theEnvironment;	
		testName = theTestName;		
	}
	
	public String getTheCanadaFourUserName() {
		return theCanadaFourUserName;
	}

	public String getTheWAGCUserName() {
		return theWAGCUserName;
	}

	public String getTheUSSplitTenderUserName() {
		return theUSSplitTenderUserName;
	}

	public String getTheCanadaNineUserName() {
		return theCanadaNineUserName;
	}

	public String getTheCanadaTenUserName() {
		return theCanadaTenUserName;
	}

	public String getSignInLink() {
		return signInLink;
	}

	public String getByXPathUserNameObj() {
		return byXPathUserNameObj;
	}

	public String getByXpathPasWrd() {
		return byXpathPasWrd;
	}

	public String getByXPathSignInButton() {
		return byXPathSignInButton;
	}

	public String getTheCanadaUsername() {
		return theCanadaUsername;
	}

	public String getTheSBUsername() {
		return theSBUsername;
	}

	public String getTheEndToEndUsername() {
		return theEndToEndUsername;
	}

	public String getTheWAUsername() {
		return theWAUsername;
	}

	public String getTheCALUsername() {
		return theCALUsername;
	}

	public String getTheUsername() {
		return theUsername;
	}

	public String getThePassword() {
		return thePassword;
	}

	public String getTheContinueAsGuestBtn() {
		return theContinueAsGuestBtn;
	}
	
	public String getThePageName() {
		return thePageName;
	}

	public String getTheOutletPasswordId() {
		return theOutletPasswordId;
	}

	public String getTheOutletUsernameId() {
		return theOutletUsernameId;
	}

	public String getTheCSCOutletUsernameId() {
		return theCSCOutletUsernameId;
	}

	public String getTheCSCOutletPasswordId() {
		return theCSCOutletPasswordId;
	}
	
	public String getTheCSCOutletLoginButton() {
		return theCSCOutletLoginButton;
	}

	public String getTheHMCOutletUsernameId() {
		return theHMCOutletUsernameId;
	}

	public String getTheHMCOutletLoginButton() {
		return theHMCOutletLoginButton;
	}

	public String getTheHMCOutletPasswordId() {
		return theHMCOutletPasswordId;
	}

	public void signIn(String theUsername, String password) throws InterruptedException {
		WebElement ce;

		System.out.println("This is the browser:    "   + myEnvironment.getBrowser());
		System.out.println("This is the current url:     " + driver.getCurrentUrl());
		
		if (myEnvironment.getBrowser().contains("IE9") && driver.getCurrentUrl().contains("qa")) {
			System.out.println("We are in the if statement!!!!!!!!");
		driver.get("https://qa-outlet.tommybahama.com/en/login");
		 Thread.sleep(1000);
			driver.get("javascript:document.getElementById('overridelink').click();");
			 Thread.sleep(1000);
		} else {

		}
	
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getByXPathUserNameObj()));
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(), testName, this.getThePageName(),
				"");
		// myEnvironment.hoverToBody(driver, By.xpath("//body"));
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getByXPathUserNameObj()));
		ce.sendKeys(theUsername);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getByXpathPasWrd()));
		ce.sendKeys(password);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getByXPathSignInButton()));

		ce.click();
		 Thread.sleep(1000);
	}
	
	public void signInOutlet(String theUsername, String password) throws InterruptedException {
		WebElement ce;
		
	//	driver.get("https://qa-outlet.tommybahama.com/en/login");
	//	 Thread.sleep(1000);
		
		if (myEnvironment.getBrowser().contains("IE9") && myEnvironment.IsElementPresent(By.id("overridelink"))) {
			System.out.println("We are in the if statement!!!!!!!!");
		
			driver.get("javascript:document.getElementById('overridelink').click();");
			 Thread.sleep(1000);
		} else {

		}
		
	
		 Thread.sleep(1000);
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheOutletUsernameId()));

		ce = myEnvironment.waitForDynamicElement(
				By.id(getTheOutletUsernameId()));
		ce.sendKeys(theUsername);
		 Thread.sleep(1000);
		ce = myEnvironment.waitForDynamicElement(
				By.id(getTheOutletPasswordId()));
		ce.sendKeys(thePassword);
		 Thread.sleep(1000);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath("//button[@type='submit']"));

		ce.click();
		 Thread.sleep(1000);
		// Thread.Sleep(10000);
	}
	
	
	public void signInCSC(String username, String password) throws InterruptedException {
		WebElement ce;
      
		System.out.println("We are in the sign In method for csc.");
		myEnvironment.IsElementPresent(By.name(this.getTheCSCOutletUsernameId()));
		ce = myEnvironment.waitForDynamicElement(
				By.name(this.getTheCSCOutletUsernameId()));
		ce.clear();

		ce = myEnvironment.waitForDynamicElement(
				By.name(getTheCSCOutletUsernameId()));
		ce.sendKeys(username);
		
		ce = myEnvironment.waitForDynamicElement(
				By.name(getTheCSCOutletPasswordId()));
		ce.clear();
		
		ce = myEnvironment.waitForDynamicElement(
				By.name(getTheCSCOutletPasswordId()));
		ce.sendKeys(password);
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				testName, "LogInPage", "");
		
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(getTheCSCOutletLoginButton()));

		ce.click();
		// Thread.Sleep(10000);
	}

	
	public void signInHMC(String username, String password) throws InterruptedException {
		WebElement ce;      
	
	
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheHMCOutletUsernameId()));
		ce.clear();

		ce = myEnvironment.waitForDynamicElement(
				By.id(getTheHMCOutletUsernameId()));
		ce.sendKeys(username);
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(getTheHMCOutletPasswordId()));
		ce.clear();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(getTheHMCOutletPasswordId()));
		ce.sendKeys(password);
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				testName, "LogInPage", "");
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(getTheHMCOutletLoginButton()));

		ce.click();
		myEnvironment.waitForURL("wid");
		
		// Thread.Sleep(10000);
	}
	
	public void signInHAC(String username, String password) throws InterruptedException {
	
	
		WebElement ce;      
	
	
		ce = myEnvironment.waitForDynamicElement(
				By.name(this.getTheHACOutletUsernameName()));
		ce.clear();
		ce.sendKeys(theUsername);
		ce = myEnvironment.waitForDynamicElement(
				By.name(this.getTheHACOutletPasswordName()));
		ce.sendKeys(thePassword);
	

	
		
		myEnvironment.TakeScreenShot( myEnvironment.getBrowser(),
				testName, "HACLogInPage", "");
		
		System.out.println("We are getting to the HAC.");
		
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector("button.button"));

		ce.click();
		myEnvironment.waitForURL("wid");
		System.out.println("We are getting to the HAC.");
		// Thread.Sleep(10000);
	}

	public String getTheIEUserName() {
		return theIEUserName;
	}

	public String getTheIEPasswrd() {
		return theIEPasswrd;
	}

	public String getTheByXpathHACsignInButton() {
		return theByXpathHACsignInButton;
	}

	public String getTheHACOutletUsernameName() {
		return theHACOutletUsernameName;
	}

	public String getTheHACOutletPasswordName() {
		return theHACOutletPasswordName;
	}

	public String getTheHACOutletLoginButton() {
		return theHACOutletLoginButton;
	}
	
}
