package TommyBahamaOutletRepository;

public class OutletConfirmationPage {
	
	private final String theByLinkEmailGS = "email Guest Services";
	private final String theURL = "https://";
	private final String theOrderNumber = "//div[@id='content']/div[2]/div/p";
	private final String theShippingAmount = "//table[@id='billing-summary']/tbody/tr/td[3]/span[2]";
	private final String theByIdTotalShipping = "total_shipping";
	private final String thePageName = "ConfirmationPage";
	private final String theSubTotal = "order_subtotal";
	private final String theSplitTenderSubTotal = "//table[@id='billing-summary']/tbody/tr/td[2]/span[4]";
	private final String theTotalTax = "total_tax";
	private final String theByIdDuty = "spanDuty";
	
	private final String theTotalShippingAmount = "//span[@id='total_shipping']";
	private final String theTotalAmount = "total_order";
	private final String theByCssFinalSaleText = "div.cart_item_alert > b";
	
	private final String theByClassNameFreeShippingMessage = "promo";
	private final String theByXpathFreeShippingMessage = "//dt[2]";
	private final String theByXpathPromoDiscount = "//dd[3]";
	
	private final String theByXpathShippingAmount = "//div[@id='content']/div[2]/div[4]/div/div[2]/div/div[2]/ul/li[3]";
	private final String theByIdOrderSubtotal = "order_subtotal";

	public String getTheByLinkEmailGS() {
		return theByLinkEmailGS;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheOrderNumber() {
		return theOrderNumber;
	}

	public String getTheShippingAmount() {
		return theShippingAmount;
	}

	public String getTheByIdTotalShipping() {
		return theByIdTotalShipping;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheSubTotal() {
		return theSubTotal;
	}

	public String getTheSplitTenderSubTotal() {
		return theSplitTenderSubTotal;
	}

	public String getTheTotalTax() {
		return theTotalTax;
	}

	public String getTheByIdDuty() {
		return theByIdDuty;
	}

	public String getTheTotalShippingAmount() {
		return theTotalShippingAmount;
	}

	public String getTheTotalAmount() {
		return theTotalAmount;
	}

	public String getTheByCssFinalSaleText() {
		return theByCssFinalSaleText;
	}

	public String getTheByXpathShippingAmount() {
		return theByXpathShippingAmount;
	}

	public String getTheByClassNameFreeShippingMessage() {
		return theByClassNameFreeShippingMessage;
	}

	public String getTheByXpathFreeShippingMessage() {
		return theByXpathFreeShippingMessage;
	}

	public String getTheByXpathPromoDiscount() {
		return theByXpathPromoDiscount;
	}

	public String getTheByIdOrderSubtotal() {
		return theByIdOrderSubtotal;
	}

}
