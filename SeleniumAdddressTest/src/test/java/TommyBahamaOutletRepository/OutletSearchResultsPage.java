package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class OutletSearchResultsPage {
	
	
    private final String theByXpathBlackCheckBox = "(//input[@type='checkbox'])[208]";
    private final String theByXpathSecondProduct= "  //div[@id='content']/div[4]/div[4]/div/div[2]/div/div/a/span/img";

    private OutletWebdriverEnvironment myEnvironment;
    private OutletHeader myHeader;

    public OutletSearchResultsPage(OutletWebdriverEnvironment theEnvironment, OutletHeader theHeader)
	{
		myEnvironment = theEnvironment;
		myHeader = theHeader;
	}
    
    public OutletSearchResultsPage()
	{
		
	}
    
    
	public String getTheByXpathBlackCheckBox() {
		return theByXpathBlackCheckBox;
	}


	public String getTheByXpathSecondProduct() {
		return theByXpathSecondProduct;
	}
	
	public void executeSearch(String theSearchTerm) {
		WebElement ce;
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheSearchInput()));
		ce.sendKeys(theSearchTerm);
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheSearchBtn()));
		ce.click();
		myEnvironment.waitForPageLoaded();	  
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForURL(theSearchTerm);
		myEnvironment.waitForTitle("Search " + theSearchTerm);
	}

}
