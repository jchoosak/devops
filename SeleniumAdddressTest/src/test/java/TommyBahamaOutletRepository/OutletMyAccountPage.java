package TommyBahamaOutletRepository;



	import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

	public class OutletMyAccountPage {
	
		   private final String theOrderHistory = "//a[contains(text(),'View your order history')]";
		   private final String theFirstOrder = "//table[@id='order_history']/tbody/tr/td/a";
			
		   private final String theByXpathFirstOrder = "//table[@id='order_history']/tbody/tr/td/a";
		 
		private final String theByXPathEmailAddress = "//div[@id='content_container_id']/div/div[2]/div/div/div[5]/div/div[3]/div[2]";
	   
		
		private final String theByXpathUpdatePersonalDetails = "(//a[contains(@href, '/en/my-account/profile')])[3]";
	    private final String theByXpathUpdateEmailAddress = "//a[contains(@href, '/en/my-account/update-email')]";
	    private final String theByXpathUpdatePassword = "//a[contains(@href, '/en/my-account/update-email')]";
	    
	    private final String theByXpathEditName = "//a[contains(@href, 'update-profile')]";
	    private final String theByXpathEditEmail = "//a[contains(@href, 'update-email')]";
	    private final String theByXpathEditPassword = "//a[contains(@href, 'update-password')]";
	    
	    
	   
	   private final String theByIdFirstName = "profile.firstName";
	   private final String theByIdLastName = "profile.lastName";
	   private final String theByIdNewEmail = "profile.email";
	   private final String theByIdConfirmNewEmail = "profile.checkEmail";
	   private final String theByIdEmailPassword = "profile.pwd";
	   
	   private final String theByXpathSubmitEmailUpdate = "//form[@id='updateEmailForm']/span/button";
	   private final String theByXpathSubmitNameUpdate = "//form[@id='updateProfileForm']/span/button[2]";
	   private final String theByXpathSubmitPasswordUpdate = "//form[@id='updatePasswordForm']/span/button";
	   
	   
		private final String theByIdOldPassword = "profile.currentPassword";
	   private final String theByIdNewPassword = "profile-newPassword";
	   private final String theByIdConfirmPassword = "profile.checkNewPassword";
	   
	   
	   
	   private final String theByIdSubmitPersonalInfo = "editInfoSubmit";
	   private final String theByXPathAddressBook = "(//a[contains(@href, '/en/my-account/address-book')])[3]";
	   private final String theByXPathAddNewAddress = "//a[contains(@href, 'add-address')]";
	   

	   private final String theByIdSetAsDefaultAddress = "Use as default Delivery Address";
	   
	   private final String theByXpathRemoveAddress = "(//a[contains(text(),'Remove')])[2]";
	  
	   
	   
	   private final String theByCSSEditShipingAddress = "div.billing_container > table > tbody > tr > td > div.title_btn_edit_left > a";
	   
	   private final String theByCSSAddressRemovedMessage = "div.information_message.positive > p";
	   private final String theByCSSEditBillingAddress = "div.title_btn_edit_left > a";
	   
	  
	   private final String theByCSSDeleteShippingAddress = "div.billing_container > table > tbody > tr > td > div.title_btn_delete > a";
	   private final String theByCSSDeleteCC = "div.title_btn_delete_left > a";
	   
	   private final String theByCSSShippingAddressInfo = "body.editaccountbody";
	   
	   private final String theByCSSAddCard = "span.title_btn_add > a";
	   
	   
	
	   
	   private final String theByIdAddCCSubmit = "addCCSubmit";
	   
	   
	   private final String theByCSSCreditCardInfo = "div.credit_card";
	   
	   
	   private final String theByNameShiptoAddressLabel = "SHIP_TO_ADDRESS<>label";
	   private final String theByIdShippingZip = "ship_postal";

	    
	    private final String theByCSSReturnWizard = "form[name=\"returns_link\"] > a";
	    private final String theByCSSFourthOrderReturnWizard = "#nameLink4";
	    private final String theByCSSFirstAmountItemsToReturn = "#selectId_0";
	    private final String theByNameFirstReasonToReturn = "ORDER_LINE_ARRAY<>returnTypeCode";
	    private final String theByCSSSubmitStep2 = "#continueToStep3";
	    private final String theByCSSSiteError = "#site_error_id";
	    
	    private final String theNoItemSelectedReturnErrorMessage = "Please indicate which order line(s) you would like to return.";
	    private final String theNoItemQtySelectedReturnErrorMessage = "Please select a quantity for the line item you wish to return.";
	    private final String theNoReasonReturnErrorMessage = "Please select a reason for the line item you wish to return.";
	    
	    private final String theByCSSSendEmailBtn = "#returnButtonStyleEmailId";
	    
	    private final String theByIdPrintReturnLabelBtn = "continue_step_3b";
	    private final String theByIdEmailReturnLabelBtn = "emailUSPSInputId";
	    
	    private final String theByCSSRmaInfo = "ul.return_orders > li";
	    private final String theByIdEmailWizardInput = "emailUSPSInputId";
	    private final String theByCSSSendEmailReturnWizard = "#returnButtonStyleEmailId";
	    private final String theByCSSConfirmEmailSentLabel = "#label_status_message";
	    private final String theByIdContinueStep4Btn = "continue4";
	    private final String theByClassNameConfirmationRMAInfo = "return_orders";
	    private final String theByCSSPrintConfirmationBtn = "img[alt=\"Print Confirmation\"]";
	    private final String thePageName = "AccountPage";
	    
	    private final String theByCSSRmaNumber = "ul.return_orders > li > b";
	   
	   // private WebDriver driver;
	    OutletWebdriverEnvironment myEnvironment;
	    public  OutletMyAccountPage(OutletWebdriverEnvironment theEnvironment)
	    {
	   // 	driver = theDriver;
	    	myEnvironment = theEnvironment;
	    }

	   public String getTheByXPathEmailAddress() {
			return theByXPathEmailAddress;
		}
			
		public String getTheByIdSubmitPersonalInfo() {
			return theByIdSubmitPersonalInfo;
		}

		public String getTheByCSSEditBillingAddress() {
			return theByCSSEditBillingAddress;
		}

		public String getTheByCSSAddCard() {
			return theByCSSAddCard;
		}
		
		public String getTheByIdAddCCSubmit() {
			return theByIdAddCCSubmit;
		}
		
		public String getTheByCSSCreditCardInfo() {
			return theByCSSCreditCardInfo;
		}

		public String getTheByCSSDeleteCC() {
			return theByCSSDeleteCC;
		}

		public String getTheByNameShiptoAddressLabel() {
			return theByNameShiptoAddressLabel;
		}

		public String getTheByCSSShippingAddressInfo() {
			return theByCSSShippingAddressInfo;
		}

		public String getTheByCSSEditShipingAddress() {
			return theByCSSEditShipingAddress;
		}

		public String getTheByCSSDeleteShippingAddress() {
			return theByCSSDeleteShippingAddress;
		}

		public String getTheCSSShippingAddressInfo() {
			return theByCSSShippingAddressInfo;
		}
		
		public String getTheByCSSReturnWizard() {
			return theByCSSReturnWizard;
		}

		public String getTheByCSSFourthOrderReturnWizard() {
			return theByCSSFourthOrderReturnWizard;
		}

		public String getTheByCSSFirstAmountItemsToReturn() {
			return theByCSSFirstAmountItemsToReturn;
		}

		public String getTheByNameFirstReasonToReturn() {
			return theByNameFirstReasonToReturn;
		}

		public String getTheByCSSSubmitStep2() {
			return theByCSSSubmitStep2;
		}

		public String getTheByCSSSiteError() {
			return theByCSSSiteError;
		}

		public String getTheNoItemSelectedReturnErrorMessage() {
			return theNoItemSelectedReturnErrorMessage;
		}

		public String getTheNoItemQtySelectedReturnErrorMessage() {
			return theNoItemQtySelectedReturnErrorMessage;
		}

		public String getTheNoReasonReturnErrorMessage() {
			return theNoReasonReturnErrorMessage;
		}

		public String getTheByCSSSendEmailBtn() {
			return theByCSSSendEmailBtn;
		}

		public String getTheByIdPrintReturnLabelBtn() {
			return theByIdPrintReturnLabelBtn;
		}

		public String getTheByIdEmailReturnLabelBtn() {
			return theByIdEmailReturnLabelBtn;
		}

		public String getTheByCSSRmaInfo() {
			return theByCSSRmaInfo;
		}

		public String getTheByIdEmailWizardInput() {
			return theByIdEmailWizardInput;
		}

		public String getTheByCSSSendEmailReturnWizard() {
			return theByCSSSendEmailReturnWizard;
		}

		public String getTheByCSSConfirmEmailSentLabel() {
			return theByCSSConfirmEmailSentLabel;
		}

		public String getTheByIdContinueStep4Btn() {
			return theByIdContinueStep4Btn;
		}

		public String getTheByClassNameConfirmationRMAInfo() {
			return theByClassNameConfirmationRMAInfo;
		}

		public String getTheByCSSPrintConfirmationBtn() {
			return theByCSSPrintConfirmationBtn;
		}

		public String getTheByCSSRmaNumber() {
			return theByCSSRmaNumber;
		}

		public String getThePageName() {
			return thePageName;
		}
		
		public String getTheOrderHistory() {
			return theOrderHistory;
		}


		public String getTheFirstOrder() {
			return theFirstOrder;
		}


		public String getTheByXPathAddressBook() {
			return theByXPathAddressBook;
		}


		public String getTheByXPathAddNewAddress() {
			return theByXPathAddNewAddress;
		}


		public String getTheByIdSetAsDefaultAddress() {
			return theByIdSetAsDefaultAddress;
		}
		


		public String getTheByXpathRemoveAddress() {
			return theByXpathRemoveAddress;
		}

		public String getTheByCSSAddressRemovedMessage() {
			return theByCSSAddressRemovedMessage;
		}

		public String getTheByXpathUpdatePersonalDetails() {
			return theByXpathUpdatePersonalDetails;
		}

		public String getTheByXpathUpdateEmailAddress() {
			return theByXpathUpdateEmailAddress;
		}

		public String getTheByXpathUpdatePassword() {
			return theByXpathUpdatePassword;
		}

		public String getTheByXpathEditName() {
			return theByXpathEditName;
		}

		public String getTheByXpathEditEmail() {
			return theByXpathEditEmail;
		}

		public String getTheByXpathEditPassword() {
			return theByXpathEditPassword;
		}

		public String getTheByIdFirstName() {
			return theByIdFirstName;
		}

		public String getTheByIdLastName() {
			return theByIdLastName;
		}

		public String getTheByXpathSubmitNameUpdate() {
			return theByXpathSubmitNameUpdate;
		}

		public String getTheByIdNewEmail() {
			return theByIdNewEmail;
		}

		public String getTheByIdConfirmNewEmail() {
			return theByIdConfirmNewEmail;
		}

		public String getTheByIdEmailPassword() {
			return theByIdEmailPassword;
		}

		public String getTheByXpathSubmitEmailUpdate() {
			return theByXpathSubmitEmailUpdate;
		}

		public String getTheByIdOldPassword() {
			return theByIdOldPassword;
		}

		public String getTheByIdNewPassword() {
			return theByIdNewPassword;
		}

		public String getTheByIdConfirmPassword() {
			return theByIdConfirmPassword;
		}

		public String getTheByXpathSubmitPasswordUpdate() {
			return theByXpathSubmitPasswordUpdate;
		}

		public void editName( String theFirstName, String theLastName) {
			// TODO Auto-generated method stub
			
			WebElement ce;
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathEditName()));		
			ce.click();
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdFirstName()));		
			ce.clear();
			ce.sendKeys(theFirstName);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdLastName()));		
			ce.clear();
			ce.sendKeys(theLastName);
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathSubmitNameUpdate()));		
			ce.click();
			
		}
		
		public void editEmail(String theNewEmail, String thePswrd) {
			// TODO Auto-generated method stub
			
			WebElement ce;
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathEditEmail()));		
			ce.click();
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdNewEmail()));		
			ce.clear();
			ce.sendKeys(theNewEmail);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdConfirmNewEmail()));		
			ce.clear();
			ce.sendKeys(theNewEmail);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdEmailPassword()));		
			ce.clear();
			ce.sendKeys(thePswrd);
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathSubmitEmailUpdate()));		
			ce.click();
			
		}
		
		public void editPswrd(String theOldPswrd, String theNewPswrd) throws InterruptedException {
			// TODO Auto-generated method stub
			
			WebElement ce;
			
			myEnvironment.IsElementPresent(By.xpath(this.getTheByXpathEditPassword())); 			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathEditPassword()));		
			ce.click();
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdOldPassword()));		
			ce.clear();
			ce.sendKeys(theOldPswrd);
			
			System.out.println("this is the old password:::			" + theOldPswrd);
			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdNewPassword()));		
			ce.clear();
			ce.sendKeys(theNewPswrd);
			
			System.out.println("this is the NEW password:::			" + theNewPswrd);

			
			ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdConfirmPassword()));		
			ce.clear();
			ce.sendKeys(theNewPswrd);
			
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheByXpathSubmitPasswordUpdate()));		
			ce.click();
			
			Thread.sleep(5000);
			
		}

		public String getTheByXpathFirstOrder() {
			return theByXpathFirstOrder;
		}

		public String getTheByIdShippingZip() {
			return theByIdShippingZip;
		}
		
	}

