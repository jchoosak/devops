package TommyBahamaOutletRepository;

public class OutletPreviewPage {
	private final String thePreviewTitle = "Order Review";
	private final String theByXpathShippingAmount = "//div[@id='checkout_summary_deliverymode_div']/div/div[2]/ul/li[2]";
	private final String theByXpathPromoDiscount = "//div[@id='cart_totals_div']/dl/dd[3]";
	//div[@id='cart_totals_div']/dl/dd[3]
	
	private final String theURL = "https://";
	private final String theSubTotal = "//div[@id='cart_totals_div']/dl/dd";
	private final String theSplitTenderSubTotal = "//table[@id='billing-summary']/tbody/tr/td[2]/span[4]";
	private final String theTotalTax = "sl_totaltax";
	private final String theByIdDuty = "spanDuty";
	private final String theShippingAmount = "//table[@id='billing-summary']/tbody/tr/td[2]/span[2]";
	private final String theTotalShippingAmount = "//span[@id='total_shipping']";
	private final String theTotalAmount = "total sl_total";
	private final String theSplitTenderOrderTotalAmount = "//table[@id='billing-summary']/tbody/tr/td[2]/span[4]";
	private final String theGCPartialPayment = "//table[@id='billing-summary']/tbody/tr/td[2]/u[2]";
	private final String theTotalAmountWithoutGS = "//table[@id='billing-summary']/tbody/tr/td[2]/span[3]";
	private final String theByXpathSubmitButton = "//button[@type='button']";
	private Double theWATaxMultiplier = 0.08;

	private final String theByCssFinalSaleText = "div.cart_item_alert";
	private final String theUSGC = "//table[@id='billing-summary']/tbody/tr/td/span[6]";
	private final String thePageName = "PreviewPage";
	
	private final String theByClassNameSplitOrderTotal = "billing-total";
	private final String theByCssOrderSubtotal = "dl.order_totals > dd";
	

	public String getThePreviewTitle() {
		return thePreviewTitle;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheSubTotal() {
		return theSubTotal;
	}

	public String getTheSplitTenderSubTotal() {
		return theSplitTenderSubTotal;
	}

	public String getTheTotalTax() {
		return theTotalTax;
	}

	public String getTheByIdDuty() {
		return theByIdDuty;
	}

	public String getTheShippingAmount() {
		return theShippingAmount;
	}

	public String getTheTotalShippingAmount() {
		return theTotalShippingAmount;
	}

	public String getTheTotalAmount() {
		return theTotalAmount;
	}

	public String getTheSplitTenderOrderTotalAmount() {
		return theSplitTenderOrderTotalAmount;
	}

	public String getTheGCPartialPayment() {
		return theGCPartialPayment;
	}

	public String getTheTotalAmountWithoutGS() {
		return theTotalAmountWithoutGS;
	}

	public Double getTheWATaxMultiplier() {
		return theWATaxMultiplier;
	}

	public void setTheWATaxMultiplier(Double theWATaxMultiplier) {
		this.theWATaxMultiplier = theWATaxMultiplier;
	}

	public String getTheUSGC() {
		return theUSGC;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByClassNameSplitOrderTotal() {
		return theByClassNameSplitOrderTotal;
	}

	public String getTheByXpathSubmitButton() {
		return theByXpathSubmitButton;
	}

	public String getTheByCssFinalSaleText() {
		return theByCssFinalSaleText;
	}

	public String getTheByXpathShippingAmount() {
		return theByXpathShippingAmount;
	}

	public String getTheByXpathPromoDiscount() {
		return theByXpathPromoDiscount;
	}

	public String getTheByCssOrderSubtotal() {
		return theByCssOrderSubtotal;
	}

}
