package TommyBahamaOutletRepository;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class OutletShoppingCart {
	private final String theByLinkContinueShopping = "Continue Shopping";
	private final String theByCSSContinueShopping = "a.continue-shopping-btn";
	private final String theByCssRestrictionText = "div.shipping_restriction";
	
	private final String theByCssRestrictionLink = "div.shipping_restriction > a.tb_modal";
	private final String theRemoveLink = "//a[contains(text(),'Remove')]";
	private final String theByCssFinalSaleText = "div.cart_item_alert";
	
	private final String theByClassNameProductDiscount = "price_after";	
	private final String theContinueCheckout = "//button[@id='checkoutButtonTop']";
	private final String theByIdBottomContinueCheckout = "checkoutButtonBottom";
	private final String theBottomCheckoutBtn = "(//div[@id='option-buttons']/a)[3]";
	private final String theFirstRemoveLink = "//a[contains(text(),'Remove')]";
	
	
	private final String theByCSSPromoDiscount = "dd.promo";
	private final String theByCSSPromoDiscountMessage = "dt.promo";
	private final String theByXpathPromoDiscountMessage = "/html/body/div[3]/div/div[4]/div[3]/div[2]/div[2]/div/div[2]/dl/span/dt[2]";
	private final String theByCSSOrderSubtotal = "dl.order_totals > dd";
	
	private final String theUpdateQty = "//input[@id='update_line_quantities_btn']";
	private final String theByCSSFiredPromoMessage= "div.span-4.last";
	private final String theByNameQtyUpdate = "quantity";

	private final String theShippingItemPrice = "//div[@id='subtotal']/table/tbody/tr[3]/td[2]";
	private final String theGiftItemPrice = "//div[@id='subtotal']/table/tbody/tr[2]/td[2]";
	private final String theTotalItemPrice = "css=tr.last > td.right-col";
	private final String theShippingLink = "link=exact:When will my order ship?";
	// Could be part of its own Obj Repository
	private final String theShippingModal = "TB_iframeContent";
	private final String theCloseModal = "relative=up";

	private final String theTaxQuestionMark = "link=exact:(?)";
	private final String theTaxModalX = "//a[@id='TB_closeWindowButton']/img";
	private final String theTaxModalText = "Applicable sales tax will be added on all orders based on your shipping address. The amount of sales tax is estimated until the order has shipped and shipping confirmation has been sent. We do not charge sales tax on the purchase of Gift Cards; however, items paid for with Gift Cards will be taxed.";
	private final String theGuaranteeText = "We promise that our quality, service, communication and your overall experience will exceed expectations. Guaranteed.";
	private final String theSuggestionBox = "//div[@id='suggestion-box']/h3";
	private final String theFirstSuggestionLink = "//ul[@id='suggestions']/li/a[2]";
	private final String theSecondSuggestionLink = "//ul[@id='suggestions']/li[2]/a[2]";
	private final String theThridSuggestionLink = "//ul[@id='suggestions']/li[3]/a[2]";
	private final String theFirstSuggestionThumbnail = "css=#suggestions > li > a > img";
	private final String theSecondSuggestionThumbnail = "//ul[@id='suggestions']/li[2]/a/img";
	private final String theThridSuggestionThumbnail = "//ul[@id='suggestions']/li[3]/a/img";
	private final String theSearchInput = "//input[@id='inputSearchFor']";
	private final String theSearchBtn = "//div[@id='divNavSearch']/form/div/a/img";
	private final String theRefineSearch = "//input[@id='refine_search']";
	private final String theSearchInputValue = "Polo Shirts";
	private final String theEmptyShoppingBagText = "If you have an account and are signed in, your items remain in the Shopping Bag for 14 days.";
	private final String theUrlType = "http://";
	// private final String theFirstGiftLink = "Make This a Gift";
		private final String theDetailsLinkText = "DETAILS";
	// rivate String theFirstShippingRestriction =
	// "//table[@id='shopping-table']/tbody/tr[4]/td[2]/span[2]/div[2]";
	private final String theFirstShippingRestriction = "//table[@id='shopping-table']/tbody/tr[4]/td[2]/span[2]/div[2]";
	private final String theSecondShippingRestriction = "//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/div[2]";
	private final String theThirdShippingRestriction = "//table[@id='shopping-table']/tbody/tr[6]/td[2]/span[2]/div[2]";

	
	private final String theByCSSFlipSideImage = "img[alt=\"PGC880001\"]";
	
	private final String thePageName = "ShoppingBag";
	private final String theCandaErrorColor ="rgb(182, 72, 59)";
	private final String theIECanadaErrorColor = "rgba(182, 72, 59, 1)";
	private final String theShippingErrorColor = "rgb(168, 49, 39)";
	private final String theChromeShippingErrorColor = "rgba(168, 49, 39, 1)";
	
	// this is the item in a shopping bag
	private final String item = "ITEM";
	// this is the color of an item in the shopping bag
	private final String color = "COLOR";
	// size of an item in the shopping bag
	private final String size = "SIZE";
	// price of an item in the shopping bag
	private final String price = "PRICE";
	// qty of an item in the shopping bag or PDP
	private final String qty = "QTY";
	// sub total of order
		private final String subtotal = "SUBTOTAL";

	String FirstPrice = "";
	String SecondPrice = "";
	String TotalPrice = "";
	String ShippingPrice = "";
	Double TheFirstPrice = 0.00;
	Double TheSecondPrice = 0.00;
	Double TheTotalPrice = 0.00;
	Double TheShippingPrice = 0.00;
	
	WebDriver driver;
	OutletWebdriverEnvironment myEnvironment;
	
	public OutletShoppingCart(WebDriver theDriver, OutletWebdriverEnvironment theEnvironment) 
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
	}

	public String getTheByLinkContinueShopping() {
		return theByLinkContinueShopping;
	}

	public String getTheByCSSContinueShopping() {
		return theByCSSContinueShopping;
	}

	public String getTheRemoveLink() {
		return theRemoveLink;
	}

	public String getTheContinueCheckout() {
		return theContinueCheckout;
	}

	public String getTheBottomCheckoutBtn() {
		return theBottomCheckoutBtn;
	}

	public String getTheFirstRemoveLink() {
		return theFirstRemoveLink;
	}

	public String getTheUpdateQty() {
		return theUpdateQty;
	}


	public String getTheByNameQtyUpdate() {
		return theByNameQtyUpdate;
	}

	public String getTheShippingItemPrice() {
		return theShippingItemPrice;
	}

	public String getTheGiftItemPrice() {
		return theGiftItemPrice;
	}

	public String getTheTotalItemPrice() {
		return theTotalItemPrice;
	}

	public String getTheShippingLink() {
		return theShippingLink;
	}

	public String getTheShippingModal() {
		return theShippingModal;
	}

	public String getTheCloseModal() {
		return theCloseModal;
	}


	public String getTheTaxQuestionMark() {
		return theTaxQuestionMark;
	}

	public String getTheTaxModalX() {
		return theTaxModalX;
	}

	public String getTheTaxModalText() {
		return theTaxModalText;
	}

	public String getTheGuaranteeText() {
		return theGuaranteeText;
	}

	public String getTheSuggestionBox() {
		return theSuggestionBox;
	}

	public String getTheFirstSuggestionLink() {
		return theFirstSuggestionLink;
	}

	public String getTheSecondSuggestionLink() {
		return theSecondSuggestionLink;
	}

	public String getTheThridSuggestionLink() {
		return theThridSuggestionLink;
	}

	public String getTheFirstSuggestionThumbnail() {
		return theFirstSuggestionThumbnail;
	}

	public String getTheSecondSuggestionThumbnail() {
		return theSecondSuggestionThumbnail;
	}

	public String getTheThridSuggestionThumbnail() {
		return theThridSuggestionThumbnail;
	}

	public String getTheSearchInput() {
		return theSearchInput;
	}

	public String getTheSearchBtn() {
		return theSearchBtn;
	}

	public String getTheRefineSearch() {
		return theRefineSearch;
	}

	public String getTheSearchInputValue() {
		return theSearchInputValue;
	}

	public String getTheEmptyShoppingBagText() {
		return theEmptyShoppingBagText;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	
	public String getTheFirstShippingRestriction() {
		return theFirstShippingRestriction;
	}

	public String getTheSecondShippingRestriction() {
		return theSecondShippingRestriction;
	}

	public String getTheThirdShippingRestriction() {
		return theThirdShippingRestriction;
	}



	public String getFirstPrice() {
		return FirstPrice;
	}

	public void setFirstPrice(String firstPrice) {
		FirstPrice = firstPrice;
	}

	public String getSecondPrice() {
		return SecondPrice;
	}

	public void setSecondPrice(String secondPrice) {
		SecondPrice = secondPrice;
	}

	public String getTotalPrice() {
		return TotalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		TotalPrice = totalPrice;
	}

	public String getShippingPrice() {
		return ShippingPrice;
	}

	public void setShippingPrice(String shippingPrice) {
		ShippingPrice = shippingPrice;
	}

	public Double getTheFirstPrice() {
		return TheFirstPrice;
	}

	public void setTheFirstPrice(Double theFirstPrice) {
		TheFirstPrice = theFirstPrice;
	}

	public Double getTheSecondPrice() {
		return TheSecondPrice;
	}

	public void setTheSecondPrice(Double theSecondPrice) {
		TheSecondPrice = theSecondPrice;
	}

	public Double getTheTotalPrice() {
		return TheTotalPrice;
	}

	public void setTheTotalPrice(Double theTotalPrice) {
		TheTotalPrice = theTotalPrice;
	}

	public Double getTheShippingPrice() {
		return TheShippingPrice;
	}

	public void setTheShippingPrice(Double theShippingPrice) {
		TheShippingPrice = theShippingPrice;
	}



	public String getTheByCSSFlipSideImage() {
		return theByCSSFlipSideImage;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheCandaErrorColor() {
		return theCandaErrorColor;
	}

	public String getTheShippingErrorColor() {
		return theShippingErrorColor;
	}

	public String getTheChromeShippingErrorColor() {
		return theChromeShippingErrorColor;
	}

	public String getTheDetailsLinkText() {
		return theDetailsLinkText;
	}

	public String getTheIECanadaErrorColor() {
		return theIECanadaErrorColor;
	}

	public String getTheByIdBottomContinueCheckout() {
		return theByIdBottomContinueCheckout;
	}

	public String getTheByCssRestrictionText() {
		return theByCssRestrictionText;
	}

	public String getTheByCssRestrictionLink() {
		return theByCssRestrictionLink;
	}

	public String getTheByCssFinalSaleText() {
		return theByCssFinalSaleText;
	}

	public String getItem() {
		return item;
	}

	public String getColor() {
		return color;
	}

	public String getSize() {
		return size;
	}

	public String getPrice() {
		return price;
	}

	public String getQty() {
		return qty;
	}

	public String getSubtotal() {
		return subtotal;
	}
	/*
	 * This method checks prices on the shopping bag and makes sure they are
	 * displayed as a US currency. This method is passed the page object and the
	 * selenium and environment objects.
	 */
	// public void checkUSPrices(ISelenium selenium, ShoppingBagObjs
	// myShoppingBagObjs, SeleniumEnvironment myEnvironment)
	// {
	// TheFirstPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.FirstItemPrice));
	// TheSecondPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.SecondItemPrice));
	// TheShippingPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.ShippingItemPrice));
	// TheTotalPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.TotalItemPrice));
	// GiftPrice = selenium.GetText(myShoppingBagObjs.GiftItemPrice);
	// if (GiftPrice.Length > 0)
	// {
	// GiftPrice = GiftPrice.Remove(0, 1);
	// }
	// else
	// {
	// GiftPrice = "0";
	// }
	// Double.TryParse(GiftPrice, out TheGiftPrice);
	// Assert.AreEqual((TheFirstPrice + TheSecondPrice + TheShippingPrice +
	// TheGiftPrice), TheTotalPrice);
	// selenium.Click(myShoppingBagObjs.FirstRemoveLink);
	// selenium.WaitForPageToLoad(myEnvironment.MyDefaultTimeout);
	// Thread.Sleep(myEnvironment.DefaultSleep);
	// myEnvironment.waitForElement(selenium,
	// myShoppingBagObjs.ContinueShopping);
	// TheFirstPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.FirstItemPrice));
	// TheShippingPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.ShippingItemPrice));
	// TheTotalPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.TotalItemPrice));
	// GiftPrice = selenium.GetText(myShoppingBagObjs.GiftItemPrice);
	// if (GiftPrice.Length > 0)
	// {
	// GiftPrice = GiftPrice.Remove(0, 1);
	// }
	// else
	// {
	// GiftPrice = "0";
	// }
	// Double.TryParse(GiftPrice, out TheGiftPrice);

	// Assert.AreEqual(TheTotalPrice, TheFirstPrice + TheShippingPrice +
	// TheGiftPrice);
	// }
	/*
	 * This method makes sure that the taxes are correct on the Shopping bag.
	 * The strings use the pulled price method to convert the strings to doubles
	 * and then execute the calculations. This method is passed the tax
	 * multiplier that is multiplied by the passed total that should equal the
	 * passed tax.
	 */
	public void checkTaxes(Double taxMultiplier, String Total, String Tax) {
		Double theTax = 0.0;
		Double theTotal = 0.0;
		Double theTaxableTotal = 0.0;
		theTotal = this.pullPrice(Total);
		theTax = this.pullPrice(Tax);
		theTaxableTotal = theTotal - theTax;
		Double theExpectedTax = theTaxableTotal * taxMultiplier;
		Assert.assertEquals(theExpectedTax, theTax);

	}

	/*
	 * This method takes the given string and pulls the price from the string
	 * Then the string is converted to a double and returned.
	 */
	public Double pullPrice(String theString) {
		Double thePrice = 0.0;
		theString = theString.substring(1);
		thePrice = Double.parseDouble(theString);
		return thePrice;
	}

	/*
	 * This method checks to see if the product rows in the shopping bag have a
	 * yellow outline to highlight the restrictions that are present for
	 * restricted items when attempting a canadian order The method is passed
	 * the environment and webdriver objects.
	 */
	public void checkProductsForYellowOutline(int max) {
		WebElement currentElement;
		for (int i = 2; i < max; i++) {// table[@id='shopping-table']/tbody/tr[2]/td
			if (i == 5 & max == 9)
				i = i + 2;
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td"));
			// myEnvironment.myWaitForWDElement(driver,
			// By.XPath("//table[@id='shopping-table']/tbody/tr[" + i +
			// "]/td"));
			System.out.println(currentElement.getAttribute("style"));
			Assert.assertTrue(currentElement.getAttribute("style").contains(
					"rgb(255, 255, 0)")
					|| currentElement.getAttribute("style").contains("#ffff00"));
			// Console.WriteLine(driver.FindElement(By.XPath("//table[@id='shopping-table']/tbody/tr["
			// + i + "]/td")).GetAttribute("style"));
			// Assert.True(driver.FindElement(By.XPath("//table[@id='shopping-table']/tbody/tr["
			// + i +
			// "]/td")).GetAttribute("style").Contains("rgb(255, 255, 0)"));
		}
	}

	/*
	 * This method checks to see if the product rows in the shopping bag have a
	 * yellow outline to highlight the restrictions that are present for
	 * restricted items when attempting a Fifty One order NOTE: I do not
	 * remember but I do not think that this is a valid method. I believe I made
	 * this under assumptions that were not correct. The method is passed the
	 * environment and webdriver objects.
	 */
	public void checkFiftyOneProductsForYellowOutline() {

		for (int i = 2; i < 8; i = i + 2) {
			myEnvironment.waitForDynamicElement(

					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td"));

			System.out.println(driver.findElement(
					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td")).getAttribute("style"));
			Assert.assertTrue(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td")).getAttribute("style")
					.contains("rgb(255, 255, 0)"));
		}
	}

	/*
	 * this method checks the rows in the shopping bag for the restriction links
	 * that should be present on a canadian order for certain products. This is
	 * commmented out because Merch was having issues displaying correctly under
	 * the webdriver test. The method is passed the webdriver and environment
	 * object.
	 */
	public void checkProductsForShippingRestrictionLink() {
		for (int i = 4; i < 6; i++) {// table[@id='shopping-table']/tbody/tr[2]/td
			myEnvironment.waitForDynamicElement(
					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td[2]/div"));

			System.out.println(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td[2]/div"))
					.findElement(By.tagName("a")).getText());
			Assert.assertTrue(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td[2]/div")).getText()
					.contains("Shipping restrictions apply"));
			Assert.assertTrue(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td[2]/div"))
					.findElement(By.tagName("a")).getText() == "DETAILS");
		}
	}



	public void removeAllProducts(int products) throws InterruptedException {

		for (int i = 0; i < products; i++) {
			WebElement ce;

			if (i == products - 1
					&& myEnvironment.IsElementPresent(
							By.xpath("//img[@alt='PGC880001'])[2]"))) {
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//a[contains(text(),'Remove')]"));
				ce.click();
				Thread.sleep(10000);
			}

			else {

				ce = myEnvironment.waitForDynamicElement(
						By.xpath(this.getTheFirstRemoveLink()));
				ce.click();
				Thread.sleep(10000);

			}
		}

	}

	public String getTheByCSSPromoDiscount() {
		return theByCSSPromoDiscount;
	}

	public String getTheByCSSOrderSubtotal() {
		return theByCSSOrderSubtotal;
	}

	public String getTheByCSSFiredPromoMessage() {
		return theByCSSFiredPromoMessage;
	}

	public String getTheByCSSPromoDiscountMessage() {
		return theByCSSPromoDiscountMessage;
	}

	public String getTheByXpathPromoDiscountMessage() {
		return theByXpathPromoDiscountMessage;
	}

	public String getTheByClassNameProductDiscount() {
		return theByClassNameProductDiscount;
	}





	

}
