package TommyBahamaOutletRepository;

import java.awt.RenderingHints.Key;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class OutletHMC {

	private final String theByXPathSubmitLogin = "//span";
	private final String theByIdRememberLogin = "Main_rememberme";

	private final String theUsrName = "admin";
	private final String thePswrd = "nimda";

	private final String theByIdSearchByJob = "Content/AllInstancesSelectEditor[in Content/GenericCondition[CronJob.job]]_select";
	private final String theByIdSearchByProductId = "StringEditor[in GenericCondition[Product.code]]_input";
	private final String theByIdSearchByArticleNumber = "Content/StringEditor[in Content/GenericCondition[Product.code]]_input";
	private final String theByIdSearchByCatalogVersion = "Content/AllInstancesSelectEditor[in Content/GenericCondition[Product.catalogVersion]]_select";
	private final String theByIdFindStockLevelsButton = "Content/GenericShortcut[findstocklevelsforproduct]_div";

	private final String theByIdProductAdminTab = "Content/EditorTab[TommyBahamaProduct.administration]_span";
	private final String theByIdProductStockTab = "Content/EditorTab[TommyBahamaProduct.tab.product.stock]_span";
	private final String theByIdProductSizeVarentStockTab = "Content/EditorTab[TommyBahamaSizeVariant.tab.product.stock]_span";
	private final String theByIdProductShippingRestriction = "Content/EnumerationValueSelectEditor[in Content/Attribute[TommyBahamaProduct.isShippingRestricted]]_select";
	private final String theByIdAvailableAmount = "IntegerEditor[in Attribute[StockLevel.available]]_input";
	private final String theByIdSafariAvailableAmount = "IntegerEditor[in Attribute[StockLevel.available][1]]_input";

	private final String theByIdCloseStockWindow = "ImageToolbarAction[close]_img";
	private final String theByIdProductCatalogVersion = "AllInstancesSelectEditor[in GenericCondition[Product.catalogVersion]]_select";
	private final String theByIdProductSearchButton = "ModalGenericFlexibleSearchOrganizerSearch[Product]_searchbutton";
	private final String theByIdUseProductButton = "ModalGenericItemSearchList[Product][1]_use";

	private final String theSiteDetailsCronJobIndex = "42";
	private final String theByIdDollarOffAmount = "Content/DoubleEditor[in Content/EditableItemListEntry[8796125827988]]_input";

	private final String theByIdFreeGroundShippingWA = "Content/StringDisplay[Free Ground Shipping to WA]_span";
	private final String theByIdPercentOffWithThreshold = "Content/StringDisplay[KF_20%_Off_All_OrdersOver100]_span";
	private final String theByIdQADollarOffAmount = "Content/DoubleEditor[in Content/EditableItemListEntry[8796289700756]]_input";

	private final String theByIdEnabledFreeShippingPromoCheckbox = "Content/BooleanEditor[in Content/Attribute[TommyBahamaShippingStateRestrictionPromotion.enabled]]_checkbox";
	private final String theByIdEnabledPercentOffWithThresholdPromoCheckbox = "Content/BooleanEditor[in Content/Attribute[TommyBahamaPercentOffOrder.enabled]]_checkbox";
	private final String theByIdEnabledDollarOffProductPromoCheckbox = "Content/BooleanEditor[in Content/Attribute[TommyBahamaDollarOffProduct.enabled]]_checkbox";

	private final String theByIdSiteDetailsFirstCronJob = "Content/OrganizerListEntry[Tommy Bahama Site Details Job Ap01][search 2]_img";
	private final String theByIdSiteDetailsSecondCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap02]_span";
	private final String theByIdSiteDetailsThirdCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap03]_span";
	private final String theByIdSiteDetailsFourthCronJob = "Content/StringDisplay[Tommy Bahama Site Details Job Ap04]_span";

	private final String theByIdSearchResultsButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	private final String theByIdCronJobName = "Content/StringEditor[in Content/GenericCondition[CronJob.code]]_input";
	private final String theByIdSubmitSearchButton = "Content/OrganizerSearch[CronJob]_searchbutton";

	private final String theByIdSiteDetailsStartFirstCronJob = "Content/GenericItemChip$1[action.performcronjob]";
	private final String theByIdSiteDetailsStartSecondCronJob = "Content/GenericItemChip$1[action.performcronjob][1]_label";
	private final String theByIdSiteDetailsStartThirdCronJob = "Content/GenericItemChip$1[action.performcronjob][2]_label";
	private final String theByIdSiteDetailsStartFourthCronJob = "Content/GenericItemChip$1[action.performcronjob][3]_label";

	private final String theByIdSearchCronJobResultsButton = "Content/OrganizerSearch[CronJob]_searchbutton";
	private final String theByIdSearchProductResultsButton = "Content/OrganizerSearch[Product]_searchbutton";
	private final String theByIdHomeButton = "Explorer[mcc]_label";
	private final String theByIdEndSessionButton = "Toolbar/ImageToolbarAction[closesession]_img";

	private final String theByIdPercentOffThreshold = "Content/DoubleEditor[in Content/EditableItemListEntry[8796289667988]]_input";
	private final String theByIdDevPercentOffThreshold = "Content/DoubleEditor[in Content/EditableItemListEntry[8796158595988]]_input";

	private final String theByIdThresholdAmount = "DoubleEditor[in Attribute[PromotionPriceRow.price]]_input";
	private final String theByIdFreeShippingThresholdAmount = "Content/DoubleEditor[in Content/Attribute[TommyBahamaShippingStateRestrictionPromotion.percentageDiscount]]_input";
	private final String theByIdDiscountAmount = "Content/DoubleEditor[in Content/Attribute[TommyBahamaPercentOffOrder.percentageDiscount]]_input";

	private final String theByIdStartSolrIndex = "GenericItemChip$11[wizard.footer.start]_label";
	private final String theByIdSaveAndClose = "ImageToolbarAction[close]_img";
	private final String theByIdFreeShippingPromotionEditorImgYes = "Content/OrganizerListEntry[TestFreeShip - TestFreeShip - Yes]_img";
	private final String theByIdFreeShippingPromotionEditorImgNo = "Content/OrganizerListEntry[TestFreeShip - TestFreeShip - No]_span";
	
	private final String theByIdAddProduct = "Content/GenericResortableItemList_!add_true_label";

	private final String theByIdDollarOffProductPromotionEditorImgYes = "Content/OrganizerListEntry[TEST_DOLLAR_OFF - $ Off - Yes]_img";
	private final String theByIdDollarOffProductPromotionEditorImgNo = "Content/OrganizerListEntry[TEST_DOLLAR_OFF - $ Off - No]_img";
	
	
	private final String theByIdPercentageOffPromotionEditorImgYes = "Content/OrganizerListEntry[JW_20%_Off_All_OrdersOver100 - JW_20%_Off_All_OrdersOver100 - Yes]_img";
	private final String theByIdPercentageOffPromotionEditorImgNo = "Content/OrganizerListEntry[JW_20%_Off_All_OrdersOver100 - JW_20%_Off_All_OrdersOver100 - No]_img";
	
	private final String theByIdThresholdEditor = "Content/EditableItemListEntry[8796158595988]_img";
	
	
	public String getTheByIdThresholdAmount() {
		return theByIdThresholdAmount;
	}

	private String theThresholdAmount = "";
	private String theOrderDiscountAmount = "";

	private WebDriver driver;
	private int cronJobsRan = 1;
	private String testName;

	private OutletWebdriverEnvironment myEnvironment;

	private OutletHmcLeftNav myHmcLeftNav;

	public OutletHMC() {

	}

	public OutletHMC(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment,
			OutletHmcLeftNav theHmcLeftNav, String theTestName) {

		driver = theDriver;
		myEnvironment = theEnvironment;
		myHmcLeftNav = theHmcLeftNav;
		testName = theTestName;

	}

	public String getTheByIdPercentOffWithThreshold() {
		return theByIdPercentOffWithThreshold;
	}

	public String getTheByXPathSubmitLogin() {
		return theByXPathSubmitLogin;
	}

	public String getTheByIdSearchByJob() {
		return theByIdSearchByJob;
	}

	public String getTheSiteDetailsCronJobIndex() {
		return theSiteDetailsCronJobIndex;
	}

	public String getTheByIdSiteDetailsFirstCronJob() {
		return theByIdSiteDetailsFirstCronJob;
	}

	public String getTheByIdSiteDetailsSecondCronJob() {
		return theByIdSiteDetailsSecondCronJob;
	}

	public String getTheByIdSiteDetailsThirdCronJob() {
		return theByIdSiteDetailsThirdCronJob;
	}

	public String getTheByIdSiteDetailsFourthCronJob() {
		return theByIdSiteDetailsFourthCronJob;
	}

	public String getTheByIdSubmitSearchButton() {
		return theByIdSubmitSearchButton;
	}

	public String getTheByIdSiteDetailsStartFirstCronJob() {
		return theByIdSiteDetailsStartFirstCronJob;
	}

	public String getTheByIdSiteDetailsStartSecondCronJob() {
		return theByIdSiteDetailsStartSecondCronJob;
	}

	public String getTheByIdSiteDetailsStartThirdCronJob() {
		return theByIdSiteDetailsStartThirdCronJob;
	}

	public String getTheByIdSiteDetailsStartFourthCronJob() {
		return theByIdSiteDetailsStartFourthCronJob;
	}

	public String getTheByIdHomeButton() {
		return theByIdHomeButton;
	}

	public String getTheByIdEndSessionButton() {
		return theByIdEndSessionButton;
	}

	public String getTheByIdSearchByArticleNumber() {
		return theByIdSearchByArticleNumber;
	}

	public String getTheByIdSearchByCatalogVersion() {
		return theByIdSearchByCatalogVersion;
	}

	public String getTheByIdSearchProductResultsButton() {
		return theByIdSearchProductResultsButton;
	}

	public String getTheByIdProductAdminTab() {
		return theByIdProductAdminTab;
	}

	public String getTheByIdProductShippingRestriction() {
		return theByIdProductShippingRestriction;
	}

	public String getTheByIdRememberLogin() {
		return theByIdRememberLogin;
	}

	public String getTheByIdProductStockTab() {
		return theByIdProductStockTab;
	}

	public String getTheByIdFindStockLevelsButton() {
		return theByIdFindStockLevelsButton;
	}

	public String getTheByIdProductSizeVarentStockTab() {
		return theByIdProductSizeVarentStockTab;
	}

	public String getTheByIdAvailableAmount() {
		return theByIdAvailableAmount;
	}

	public String getTheByIdCloseStockWindow() {
		return theByIdCloseStockWindow;
	}

	public String getTheByIdCronJobName() {
		return theByIdCronJobName;
	}

	public String getTheByIdFreeGroundShippingWA() {
		return theByIdFreeGroundShippingWA;
	}

	public String getTheByIdEnabledFreeShippingPromoCheckbox() {
		return theByIdEnabledFreeShippingPromoCheckbox;
	}

	public String getTheByIdEnabledPercentOffWithThresholdPromoCheckbox() {
		return theByIdEnabledPercentOffWithThresholdPromoCheckbox;
	}

	public String getTheByIdPercentOffThreshold() {
		return theByIdPercentOffThreshold;
	}

	public String getTheThresholdAmount() {
		return theThresholdAmount;
	}

	public void setTheThresholdAmount(String theThresholdAmount) {
		this.theThresholdAmount = theThresholdAmount;
	}

	public String getTheByIdSaveAndClose() {
		return theByIdSaveAndClose;
	}

	public String getTheByIdDiscountAmount() {
		return theByIdDiscountAmount;
	}

	public String getTheOrderDiscountAmount() {
		return theOrderDiscountAmount;
	}

	public void setTheOrderDiscountAmount(String theOrderDiscountAmount) {
		this.theOrderDiscountAmount = theOrderDiscountAmount;
	}

	public String getTheByIdDevPercentOffThreshold() {
		return theByIdDevPercentOffThreshold;
	}

	public String getTheByIdFreeShippingThresholdAmount() {
		return theByIdFreeShippingThresholdAmount;
	}

	public String getTheByIdEnabledDollarOffProductPromoCheckbox() {
		return theByIdEnabledDollarOffProductPromoCheckbox;
	}

	public String getTheByIdAddProduct() {
		return theByIdAddProduct;
	}

	public String getTheByIdSearchByProductId() {
		return theByIdSearchByProductId;
	}

	public String getTheByIdProductCatalogVersion() {
		return theByIdProductCatalogVersion;
	}

	public String getTheByIdProductSearchButton() {
		return theByIdProductSearchButton;
	}

	public String getTheByIdUseProductButton() {
		return theByIdUseProductButton;
	}

	public String getTheByIdDollarOffAmount() {
		return theByIdDollarOffAmount;
	}

	public String getTheByIdSearchResultsButton() {
		return theByIdSearchCronJobResultsButton;
	}

	public void createRestrictedProduct(String restrictedProduct, String state,
			OutletSignInPage mySignInPage) throws InterruptedException {

		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else
			driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;

		if (myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
			mySignInPage.signInHMC(theUsrName, thePswrd);

			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavCatalogFolder()));
			ce.click();

			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavProduct()));
			ce.click();

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSearchByArticleNumber()));
			ce.sendKeys(restrictedProduct);

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSearchByCatalogVersion()));
			Select clickThis = new Select(ce);
			clickThis.selectByIndex(7);

			ce = myEnvironment.waitForDynamicElement(By
					.id(this.theByIdSearchProductResultsButton));
			ce.click();

			/*
			 * if (driver.getCurrentUrl().contains("dev")) ce = myEnvironment
			 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" +
			 * restrictedProduct + "]_span")); else
			 */
			ce = myEnvironment
					.waitForDynamicElement(By.id("Content/StringDisplay["
							+ restrictedProduct + "]_span"));
			Actions action = new Actions(driver);
			action.doubleClick(ce).perform();

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdProductAdminTab()));
			ce.click();

		}

		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdProductShippingRestriction()));
		Select clickThis = new Select(ce);
		if (state.contains("on"))
			clickThis.selectByIndex(1);
		else
			clickThis.selectByIndex(0);
		ce = myEnvironment.waitForDynamicElement(By.cssSelector("u"));
		ce.click();
	}

	public void fireSiteDetailsCronJob(String theJob)
			throws InterruptedException {

		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else
			driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;
		// log into the hmc
		ce = myEnvironment.waitForDynamicElement(By.xpath(this
				.getTheByXPathSubmitLogin()));
		ce.click();
		myEnvironment.waitForURL("wid");
		// click the System folder in the hmc
		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavSystemFolder()));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavCronJob()));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdCronJobName()));
		ce.sendKeys("Tom");
		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdSearchResultsButton));
		ce.click();
		ce = myEnvironment.waitForDynamicElement(
				By.id(theJob));
		if(myEnvironment.getBrowser().contains("Safari")) {
		ce.click();
		ce.click();
		} else {
		Actions action = new Actions(driver);
		action.doubleClick(ce).perform();
		}
		ce = myEnvironment.waitForDynamicElement(By
				.id("Content/GenericItemChip$1[action.performcronjob]_label"));
		ce.click();
		String mainwindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();

		for (String window : windows) {
			System.out.println("These are the windows available           :"
					+ window);
			driver.switchTo().window(window);
			System.out
					.println("this is the titles of the windows:              "
							+ driver.getTitle());
			if (driver.getTitle().contains(
					"Start CronJob now - hybris Management Console (hMC)")) {

				driver.close();
				driver.switchTo().window(mainwindow);
			}
		}
		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdEndSessionButton));
		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdEndSessionButton));
		ce.click();
	}

	public String checkStockOfProduct(String theProductId,
			String productSizeVariant, OutletSignInPage mySignInPage)
			throws InterruptedException {

		String stock;
		
		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else
			driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;
		driver.navigate().refresh();

		if (!myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
		myEnvironment.IsElementPresent(By.id(this.getTheByIdEndSessionButton()));
			ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdEndSessionButton()));
			ce.click();
		}
		
		driver.navigate().refresh();
		// log into the hmc
		if (myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
			mySignInPage.signInHMC(theUsrName, thePswrd);

			// click the System folder in the hmc
			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavCatalogFolder()));
			ce.click();

			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavProduct()));
			ce.click();
			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSearchByArticleNumber()));
			ce.sendKeys(theProductId);

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSearchByCatalogVersion()));
			Select clickThis = new Select(ce);

			clickThis.selectByIndex(7);

			ce = myEnvironment.waitForDynamicElement(By
					.id(this.theByIdSearchProductResultsButton));
			ce.click();

			/*
			 * if (driver.getCurrentUrl().contains("dev")) ce = myEnvironment
			 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" +
			 * theProductId + "]_span")); else
			 */
			ce = myEnvironment.waitForDynamicElement(By
					.id("Content/StringDisplay[" + theProductId + "]_span"));
			Actions action = new Actions(driver);
			action.doubleClick(ce).perform();

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdProductStockTab()));
			ce.click();

		} else if(myEnvironment.getBrowser().contains("Safari")) {

			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavProduct()));
			ce.click();

		}
			
	
		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdFindStockLevelsButton()));
		ce.click();

		Thread.sleep(5000);

		String parentWindow = driver.getWindowHandle();

		Set<String> allWindows = driver.getWindowHandles();
		for (String curWindow : allWindows) {
			System.out.println("These are the windows available           :"
					+ curWindow);
			driver.switchTo().window(curWindow);
			System.out
					.println("this is the titles of the windows:              "
							+ driver.getTitle());
			if (!curWindow.equals(parentWindow)) {
		

				System.out.println("I am getting into the findstock window!!!!!");
				if (myEnvironment.getBrowser().contains("Safari")) {
					
					System.out.println("I should be selecting the product!!!!!");
					ce = myEnvironment.waitForDynamicElement(By
							.id("StringDisplay[" + productSizeVariant
									+ "][1]_span"));
					ce.click();
					ce = myEnvironment
							.waitForDynamicElement(By.id("OrganizerListEntry["
											+ productSizeVariant + 
											" - (warehouse_tommyBahamaOutlet - Tommy Bahama Outlet Warehouse)][1]_img"));
					ce.click();
				} else {
					ce = myEnvironment.waitForDynamicElement(By
							.id("StringDisplay[" + productSizeVariant
									+ "]_span"));
					Actions action = new Actions(driver);
					action.doubleClick(ce).perform();

				}
			}
		}
		
		Thread.sleep(5000);
		if(!myEnvironment.getBrowser().contains("Safari")) {
		myEnvironment.IsElementPresent(By.id(this.getTheByIdAvailableAmount()));
		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdAvailableAmount()));
		stock = ce.getAttribute("value");
		} else {
			myEnvironment.IsElementPresent(By.id(this.getTheByIdSafariAvailableAmount()));
			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSafariAvailableAmount()));
			stock = ce.getAttribute("value");
		}

	/*	ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdCloseStockWindow()));
		ce.click();*/

		driver.switchTo().window(parentWindow);

		return stock;

	}

	public void addStockToProduct(String theProductId,
			String productSizeVariant, String theQty,
			OutletSignInPage mySignInPage) throws InterruptedException {
		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else
			driver.get(myEnvironment.getTheHMCTestingEnvironment());
		WebElement ce;
		// log into the hmc
		if (myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
			mySignInPage.signInHMC(theUsrName, thePswrd);

			// click the System folder in the hmc
			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavCatalogFolder()));
			ce.click();

			ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
					.getTheByIDLeftNavProduct()));
			ce.click();
			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSearchByArticleNumber()));
			ce.sendKeys(theProductId);

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdSearchByCatalogVersion()));
			Select clickThis = new Select(ce);
			clickThis.selectByIndex(7);

			Thread.sleep(1000);

			ce = myEnvironment.waitForDynamicElement(By
					.id(this.theByIdSearchProductResultsButton));
			ce.click();

			/*
			 * if (driver.getCurrentUrl().contains("dev")) {
			 * 
			 * myEnvironment.IsElementPresent(By .id("Content/ObjectDisplay[" +
			 * theProductId + "]_span")); ce = myEnvironment
			 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" +
			 * theProductId + "]_span")); } else {
			 */
			myEnvironment.IsElementPresent(By.id("Content/StringDisplay["
					+ theProductId + "]_span"));

			// }
			if (myEnvironment.getBrowser().contains("Safari")) {

				// driver.get("javascript:document.getElementById('Content/StringDisplay["
				// + theProductId + "]_span').click();");
				// driver.get("javascript:document.getElementById('Content/StringDisplay["
				// + theProductId + "]_span').click();");
				ce = myEnvironment
						.waitForDynamicElement(By.id("Content/StringDisplay["
								+ theProductId + "]_span"));
				ce.click();
				Thread.sleep(1000);

				ce = myEnvironment
						.waitForDynamicElement(By
								.id("Content/OrganizerListEntry[TH31493 - Michelle&#39;s Cheers Leather Coasters - Set of Four - (TommyBahamaOutletProductCatalog - Online)]_img"));
				ce.click();
				// ce.click();

			} else {
				ce = myEnvironment
						.waitForDynamicElement(By.id("Content/StringDisplay["
								+ theProductId + "]_span"));
				Actions action = new Actions(driver);
				action.doubleClick(ce).perform();

			}

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdProductStockTab()));
			ce.click();

		}

		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdFindStockLevelsButton()));
		ce.click();

		Thread.sleep(5000);

		String parentWindow = driver.getWindowHandle();

		Set<String> allWindows = driver.getWindowHandles();
		for (String curWindow : allWindows) {
			System.out.println("These are the windows available           :"
					+ curWindow);
			driver.switchTo().window(curWindow);
			System.out
					.println("this is the titles of the windows:              "
							+ driver.getTitle());
			if (!curWindow.equals(parentWindow)) {
				/*
				 * if (driver.getCurrentUrl().contains("dev")) ce =
				 * myEnvironment
				 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" +
				 * productSizeVariant + "]_span")); else
				 */

				if (myEnvironment.getBrowser().contains("Safari")) {
					ce = myEnvironment.waitForDynamicElement(By
							.id("StringDisplay[" + productSizeVariant
									+ "]_span"));
					ce.click();
					ce = myEnvironment
							.waitForDynamicElement(By
									.id("OrganizerListEntry["
											+ productSizeVariant
											+ " - (warehouse_tommyBahamaOutlet - Tommy Bahama Outlet Warehouse)]_img"));
					ce.click();
				} else {
					ce = myEnvironment.waitForDynamicElement(By
							.id("StringDisplay[" + productSizeVariant
									+ "]_span"));
					Actions action = new Actions(driver);
					action.doubleClick(ce).perform();

				}
			}
		}

		Thread.sleep(5000);
		
	

		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdAvailableAmount()));
		ce.clear();
		ce.sendKeys(theQty);

		ce = myEnvironment.waitForDynamicElement(By.cssSelector("u"));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdCloseStockWindow()));
		ce.click();
		if(myEnvironment.getBrowser().contains("Safari"))
		driver.close();

		driver.switchTo().window(parentWindow);

		this.runSolrIndexUpdate();
		//this.runSolrIndexUpdate();

		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdEndSessionButton()));
		ce.click();
	}

	public void activatePromo(String thePromoType, String thePromo,
			OutletSignInPage mySignInPage) throws InterruptedException {

		WebElement ce;
		
	

		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else
			driver.get(myEnvironment.getTheHMCTestingEnvironment());

		if (!myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
		myEnvironment.IsElementPresent(By.id(this.getTheByIdEndSessionButton()));
			ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdEndSessionButton()));
			ce.click();
		}
		
		// log into the hmc
		if (myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId())))
			mySignInPage.signInHMC(theUsrName, thePswrd);

		// click the System folder in the hmc
		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavMarketingFolder()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavPromotions()));
		ce.click();

		if (thePromoType.contains("Free Shipping")) {
			/*
			 * if (driver.getCurrentUrl().contains("dev")) ce = myEnvironment
			 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" + thePromo
			 * + "]_span")); else
			 */
			ce = myEnvironment.waitForDynamicElement(By
					.id("Content/StringDisplay[" + thePromo + "]_span"));
			if(myEnvironment.getBrowser().contains("Safari")) {
				ce.click();
			
				if(	myEnvironment.IsElementPresent(By
						.id(this.getTheByIdFreeShippingPromotionEditorImgYes()))){
					ce = myEnvironment.waitForDynamicElement(By
						.id(this.getTheByIdFreeShippingPromotionEditorImgYes()));
				  } else 	ce = myEnvironment.waitForDynamicElement(By
					.id(this.getTheByIdFreeShippingPromotionEditorImgNo()));
				ce.click();
				
			} else {
				Actions action = new Actions(driver);
				action.doubleClick(ce).perform();
				}
			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdEnabledFreeShippingPromoCheckbox()));
			ce.click();

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdFreeShippingThresholdAmount()));

			this.setTheThresholdAmount(ce.getAttribute("value"));

		} else if (thePromoType.contains("%_Off")) {

			System.out
					.println("We are activate the percentage off order promo!");
			/*
			 * if (driver.getCurrentUrl().contains("dev")) ce = myEnvironment
			 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" + thePromo
			 * + "]_span")); else
			 */
			ce = myEnvironment.waitForDynamicElement(By
					.id("Content/StringDisplay[" + thePromo + "]_span"));

			if(myEnvironment.getBrowser().contains("Safari")) {
				ce.click();
			
				if(	myEnvironment.IsElementPresent(By
						.id(this.getTheByIdPercentageOffPromotionEditorImgYes()))){
					ce = myEnvironment.waitForDynamicElement(By
						.id(this.getTheByIdPercentageOffPromotionEditorImgYes()));
				  } else 	ce = myEnvironment.waitForDynamicElement(By
					.id(this.getTheByIdPercentageOffPromotionEditorImgNo()));
				ce.click();
				
			} else {
				Actions action = new Actions(driver);
				action.doubleClick(ce).perform();
				}

			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdEnabledPercentOffWithThresholdPromoCheckbox()));
			ce.click();

			if (driver.getCurrentUrl().contains("dev")) {
				if(myEnvironment.getBrowser().contains("Safari")) {
					ce = myEnvironment.waitForDynamicElement(By
							.id(this.getTheByIdThresholdEditor()));
					ce.click();
				} else {
				ce = myEnvironment.waitForDynamicElement(By
						.id(this.theByIdDevPercentOffThreshold));
				Actions action = new Actions(driver);
				action.doubleClick(ce).perform();
				}
			} else {
				if(myEnvironment.getBrowser().contains("Safari")) {
					ce = myEnvironment.waitForDynamicElement(By
							.id(this.getTheByIdThresholdEditor()));
					ce.click();
				} else {
				ce = myEnvironment.waitForDynamicElement(By
						.id(this.theByIdPercentOffThreshold));
				Actions	action = new Actions(driver);
				action.doubleClick(ce).perform();
				}
			}

			Thread.sleep(9000);

			String parentWindow = driver.getWindowHandle();

			Set<String> allWindows = driver.getWindowHandles();
			for (String curWindow : allWindows) {
				driver.switchTo().window(curWindow);
				if (!curWindow.equals(parentWindow) ) {

					myEnvironment.IsElementPresent(By.id(this
							.getTheByIdThresholdAmount()));
					ce = myEnvironment.waitForDynamicElement(By.id(this
							.getTheByIdThresholdAmount()));

					this.setTheThresholdAmount(ce.getAttribute("value"));

					System.out.println("This is the thresholdAmount           "
							+ this.getTheThresholdAmount());

					if(myEnvironment.getBrowser().contains("Safari")) {
						driver.close();
					} else {
					ce = myEnvironment.waitForDynamicElement(By.id(this
							.getTheByIdSaveAndClose()));
					ce.click();
					}
					driver.switchTo().window(parentWindow);

					ce = myEnvironment.waitForDynamicElement(By.id(this
							.getTheByIdDiscountAmount()));

					this.setTheOrderDiscountAmount(ce.getAttribute("value"));

					System.out.println("This is the discount Amount           "
							+ this.getTheOrderDiscountAmount());

				}

			}
		}

		ce = myEnvironment.waitForDynamicElement(By.cssSelector("u"));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdEndSessionButton));
		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdEndSessionButton));
		ce.click();

	}

	public void activateProductDollarOffPromo(String thePromo,
			String theProductID, OutletSignInPage mySignInPage)
			throws InterruptedException {
		WebElement ce;

		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheHMCDevTestingEnvironment());
		else
			driver.get(myEnvironment.getTheHMCTestingEnvironment());

		if (!myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId()))) {
		myEnvironment.IsElementPresent(By.id(this.getTheByIdEndSessionButton()));
			ce = myEnvironment.waitForDynamicElement(By.id(this.getTheByIdEndSessionButton()));
			ce.click();
		}
		
		// log into the hmc
		if (myEnvironment.IsElementPresent(By.id(mySignInPage
				.getTheHMCOutletUsernameId())))
			mySignInPage.signInHMC(theUsrName, thePswrd);

		// click the System folder in the hmc
		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavMarketingFolder()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavPromotions()));
		ce.click();
		
		if(myEnvironment.getBrowser().contains("Safari")) {

			ce = myEnvironment.waitForDynamicElement(By.id("Content/StringDisplay["
					+ thePromo + "]_span"));
			ce.click();
			
			if(	myEnvironment.IsElementPresent(By
					.id(this.getTheByIdDollarOffProductPromotionEditorImgYes()))){
				ce = myEnvironment.waitForDynamicElement(By
					.id(this.getTheByIdDollarOffProductPromotionEditorImgYes()));
			  } else 	ce = myEnvironment.waitForDynamicElement(By
				.id(this.getTheByIdDollarOffProductPromotionEditorImgNo()));
			ce.click();
			
		} else {
		Actions action;
		/*
		 * if (driver.getCurrentUrl().contains("dev")) { action = new
		 * Actions(driver); ce =
		 * myEnvironment.waitForDynamicElement(By.id("Content/ObjectDisplay[" +
		 * thePromo + "]_span")); } else {
		 */
		action = new Actions(driver);
		ce = myEnvironment.waitForDynamicElement(By.id("Content/StringDisplay["
				+ thePromo + "]_span"));

		action.doubleClick(ce).perform();
		// }
		}
		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdEnabledDollarOffProductPromoCheckbox()));
		ce.click();

		if(!myEnvironment.getBrowser().contains("Safari")) {

		
		
	
		ce = myEnvironment.waitForDynamicElement(By
				.cssSelector("div.gilcEntry-mandatory"));

		Actions action = new Actions(driver);
		action.contextClick(ce).perform();
		// new Actions(driver).contextClick(elem).perform();
		} else {
			ce = myEnvironment.waitForDynamicElement(By
					.cssSelector("div.gilcEntry-mandatory"));
		//	myEnvironment.onMouseOver(ce);
			if(myEnvironment.onMouseOver(ce)) {
				System.out.println("Are we attempting to open the dropdown?");
				//String selectAll = Keys.m
			//	ce.sendKeys(selectAll);
			}
		}
		
		ce = myEnvironment.waitForDynamicElement(By.id(this
				.getTheByIdAddProduct()));
		ce.click();

		String parentWindow = driver.getWindowHandle();

		Set<String> allWindows = driver.getWindowHandles();
		for (String curWindow : allWindows) {
			driver.switchTo().window(curWindow);
			if (!curWindow.equals(parentWindow)) {
				ce = myEnvironment.waitForDynamicElement(By.id(this
						.getTheByIdSearchByProductId()));
				ce.sendKeys(theProductID);

				ce = myEnvironment.waitForDynamicElement(By.id(this
						.getTheByIdProductCatalogVersion()));
				Select selectThis = new Select(ce);
				selectThis.selectByIndex(7);

				ce = myEnvironment.waitForDynamicElement(By.id(this
						.getTheByIdProductSearchButton()));
				ce.click();

				/*
				 * if (driver.getCurrentUrl().contains("dev")) ce =
				 * myEnvironment
				 * .waitForDynamicElement(By.id("Content/ObjectDisplay[" +
				 * theProductID + "]_span")); else
				 */
				ce = myEnvironment.waitForDynamicElement(By.id("StringDisplay["
						+ theProductID + "]_span"));

				ce.click();

				ce = myEnvironment.waitForDynamicElement(By.id(this
						.getTheByIdUseProductButton()));
				ce.click();

				driver.switchTo().window(parentWindow);
				Thread.sleep(5000);

			}
		}
		if (driver.getCurrentUrl().contains("dev"))
			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdDollarOffAmount()));
		else
			ce = myEnvironment.waitForDynamicElement(By.id(this
					.getTheByIdQADollarOffAmount()));

		this.setTheOrderDiscountAmount(ce.getAttribute("value"));

		System.out.println("This is the dollar off Amount           "
				+ this.getTheOrderDiscountAmount());

		ce = myEnvironment.waitForDynamicElement(By.cssSelector("u"));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdEndSessionButton));
		ce = myEnvironment.waitForDynamicElement(By
				.id(this.theByIdEndSessionButton));
		ce.click();

	}

	public void runSolrIndexUpdate() throws InterruptedException {

		WebElement ce;
		// click the System folder in the hmc
		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavSystemFolder()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavFacetSearch()));
		ce.click();

		ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
				.getTheByIDLeftNavOperationWizard()));
		ce.click();

		Thread.sleep(5000);

		String parentWindow = driver.getWindowHandle();

		Set<String> allWindows = driver.getWindowHandles();
		for (String curWindow : allWindows) {
			driver.switchTo().window(curWindow);
			if ((!curWindow.equals(parentWindow) && driver.getTitle().contains("Solr"))) {
				ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
						.getTheByIDLeftNavIndexerOperation()));
				Select myUpdate = new Select(ce);
				myUpdate.selectByIndex(1);

				ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
						.getTheByIDLeftNavSolrConfig()));
				Select mySolr = new Select(ce);
				mySolr.selectByIndex(1);

			Thread.sleep(5000);
					
				ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
						.getTheByIDLeftNavStartSolr()));
				ce.click();

				while (!myEnvironment.IsElementPresent(By
						.cssSelector(myHmcLeftNav
								.getTheByCSSLeftNavSuccessMessage()))) {
					Thread.sleep(1000);
					System.out
							.println("Waiting for indexer to complete update");
				}

				ce = myEnvironment.waitForDynamicElement(By.id(myHmcLeftNav
						.getTheByIDLeftNavCloseSolr()));
				ce.click();
				
				if(myEnvironment.getBrowser().contains("Safari"))
					driver.close();

				driver.switchTo().window(parentWindow);

			}
		}

	}

	public String getTheByIdQADollarOffAmount() {
		return theByIdQADollarOffAmount;
	}

	public String getTheByIdStartSolrIndex() {
		return theByIdStartSolrIndex;
	}

	public String getTheByIdSafariAvailableAmount() {
		return theByIdSafariAvailableAmount;
	}

	public String getTheByIdFreeShippingPromotionEditorImgYes() {
		return theByIdFreeShippingPromotionEditorImgYes;
	}

	public String getTheByIdFreeShippingPromotionEditorImgNo() {
		return theByIdFreeShippingPromotionEditorImgNo;
	}

	public String getTheByIdDollarOffProductPromotionEditorImgYes() {
		return theByIdDollarOffProductPromotionEditorImgYes;
	}

	public String getTheByIdDollarOffProductPromotionEditorImgNo() {
		return theByIdDollarOffProductPromotionEditorImgNo;
	}

	public String getTheByIdPercentageOffPromotionEditorImgYes() {
		return theByIdPercentageOffPromotionEditorImgYes;
	}

	public String getTheByIdPercentageOffPromotionEditorImgNo() {
		return theByIdPercentageOffPromotionEditorImgNo;
	}

	public String getTheByIdThresholdEditor() {
		return theByIdThresholdEditor;
	}

}
