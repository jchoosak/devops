package TommyBahamaOutletRepository;

import org.openqa.selenium.WebDriver;

public class OutletTrackOrder {
	
	private final String theOutletUsernameId = "j_username";
	private final String theOutletPasswordId = "j_password";
	private final String theByIdOrderNumber = "orderId";
	private final String theByIdZip = "zipCode";
	private final String theByXpathCheckOrderStatusButton = "//form[@id='orderForm']/button";
    private final String theByXpathSubmitLogIn = "//button[@type='submit']";
    private final String theZipErrorMessage = "Sorry. We cannot find your order. Please check your order number and shipping zip/postal code, and try again";
	private WebDriver driver;
	
	private String testName;
	

	private OutletWebdriverEnvironment myEnvironment;
	
public OutletTrackOrder() {

}

public OutletTrackOrder(WebDriver theDriver,
		OutletWebdriverEnvironment theEnvironment,
	     String theTestName) {

	driver = theDriver;
	myEnvironment = theEnvironment;
	testName = theTestName;

}





public String getTheByIdOrderNumber() {
	return theByIdOrderNumber;
}

public String getTheByIdZip() {
	return theByIdZip;
}

public String getTheByXpathCheckOrderStatusButton() {
	return theByXpathCheckOrderStatusButton;
}

public String getTheOutletUsernameId() {
	return theOutletUsernameId;
}

public String getTheOutletPasswordId() {
	return theOutletPasswordId;
}

public String getTheByXpathSubmitLogIn() {
	return theByXpathSubmitLogIn;
}

public String getTheZipErrorMessage() {
	return theZipErrorMessage;
}
}

