package TommyBahamaOutletRepository;

public class OutletHmcLeftNav{

	private final String theByIDLeftNavSystemFolder = "Tree/GenericExplorerMenuTreeNode[system]_label";
	private final String theByIDLeftNavCatalogFolder = "Tree/GenericExplorerMenuTreeNode[catalog]_label";
	private final String theByIDLeftNavCronJob = "Tree/GenericLeafNode[CronJob]_label";
	private final String theByIDLeftNavProduct = "Tree/GenericLeafNode[Product]_label";
	private final String theByIDLeftNavFacetSearch = "Tree/GenericExplorerMenuTreeNode[solrfacetsearch]_label";
    private final String theByIDLeftNavOperationWizard = "Tree/GenericLeafNode[SolrIndexerOperationWizard]_label";
	private final String theByIDLeftNavMarketingFolder = "Tree/GenericExplorerMenuTreeNode[marketing]_label";
	private final String theByIDLeftNavPromotions = "Tree/GenericLeafNode[AbstractPromotion]_label";
	
	private final String theByIDLeftNavIndexerOperation = "EnumerationValueSelectEditor[in Attribute[SolrIndexerOperationWizard.indexerOperation]]_select";
	private final String theByIDLeftNavSolrConfig = "AllInstancesSelectEditor[in Attribute[SolrIndexerOperationWizard.facetSearchConfig]]_select";
	private final String theByIDLeftNavStartSolr = "GenericItemChip$11[wizard.footer.start]_label";
	private final String theByIDLeftNavCloseSolr = "GenericItemChip$12[wizard.footer.cancel]_label";
	private final String theByXpathLeftNavCloseSolr = "//span/table/tbody/tr/td[2]/div";


	private final String theByCSSLeftNavSuccessMessage = "td.message > div";
	
	
	
	public String getTheByIDLeftNavSystemFolder() {
		return theByIDLeftNavSystemFolder;
	}


	public String getTheByIDLeftNavCronJob() {
		return theByIDLeftNavCronJob;
	}


	public String getTheByIDLeftNavCatalogFolder() {
		return theByIDLeftNavCatalogFolder;
	}


	public String getTheByIDLeftNavProduct() {
		return theByIDLeftNavProduct;
	}


	public String getTheByIDLeftNavMarketingFolder() {
		return theByIDLeftNavMarketingFolder;
	}


	public String getTheByIDLeftNavPromotions() {
		return theByIDLeftNavPromotions;
	}


	public String getTheByIDLeftNavFacetSearch() {
		return theByIDLeftNavFacetSearch;
	}


	public String getTheByIDLeftNavOperationWizard() {
		return theByIDLeftNavOperationWizard;
	}


	public String getTheByIDLeftNavIndexerOperation() {
		return theByIDLeftNavIndexerOperation;
	}


	public String getTheByIDLeftNavSolrConfig() {
		return theByIDLeftNavSolrConfig;
	}


	public String getTheByIDLeftNavStartSolr() {
		return theByIDLeftNavStartSolr;
	}


	public String getTheByCSSLeftNavSuccessMessage() {
		return theByCSSLeftNavSuccessMessage;
	}


	public String getTheByIDLeftNavCloseSolr() {
		return theByIDLeftNavCloseSolr;
	}


	public String getTheByXpathLeftNavCloseSolr() {
		return theByXpathLeftNavCloseSolr;
	}
	
	
}
