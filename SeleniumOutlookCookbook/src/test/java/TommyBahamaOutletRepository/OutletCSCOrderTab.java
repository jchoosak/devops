package TommyBahamaOutletRepository;


public class OutletCSCOrderTab {
	
	private final String theByXpathCreateNewTicketButton = "//div[5]/div/div/div[2]/div/span/table/tbody/tr[2]/td[2]";
	private final String theByXpathRefundButton = "//span[2]/table/tbody/tr[2]/td[2]";
	private final String theByXpathWaiveShippingButton = "//td[5]/span/table/tbody/tr[2]/td[2]";
    private final String theByXpathWaiveShippingCheckbox = "//td/span/input";
    private final String theByXpathAddAddressStreet1 = "//div[3]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressStreet2 = "//div[4]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressCity = "//div[5]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressState = "//table[2]/tbody/tr/td[3]/select";
    private final String theByXpathAddAddressZip = "//div[6]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressPhone = "//div[7]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressCreateButton = "//div[2]/div/div[2]/span/table/tbody/tr[2]/td[2]";
    private final String theByXpathOrderTotal = "//div[2]/div/div/div[2]/div/div/div[5]/span[2]";
	private final String theByXpathOrderTax = "//div[2]/div/div/div[2]/div/div/div[2]/span[2]";
	private final String theByXpathOrderSubTotal = "//div[2]/div/div/div[2]/div/div/div/span[2]";
	private final String theByClassShippingValue  = "csOrderTotalValue z-label";
	private final String theByXpathShippingValue = "//div[2]/div/div/div[2]/div/div/div[3]/span[2]";
	private final String theByXpathAuthAmount = "//div[4]/div/div/div[2]/div/div/div[2]/table/tbody[2]/tr/td[5]/div";
	
    private OutletWebdriverEnvironment myEnvironment;
	private String testName;
	private String defaultQaOrder = "10033016";
	private String defaultDevOrder = "10103030";
	
    public OutletCSCOrderTab(OutletWebdriverEnvironment theEnvironment, String theTestName) {
    	
    	myEnvironment = theEnvironment; 
    	testName = theTestName;
    }
 

	public String getTheByXpathAddAddressStreet1() {
		return theByXpathAddAddressStreet1;
	}




	public String getTheByXpathAddAddressStreet2() {
		return theByXpathAddAddressStreet2;
	}




	public String getTheByXpathAddAddressCity() {
		return theByXpathAddAddressCity;
	}




	public String getTheByXpathAddAddressState() {
		return theByXpathAddAddressState;
	}




	public String getTheByXpathAddAddressZip() {
		return theByXpathAddAddressZip;
	}




	public String getTheByXpathAddAddressPhone() {
		return theByXpathAddAddressPhone;
	}




	public String getTheByXpathAddAddressCreateButton() {
		return theByXpathAddAddressCreateButton;
	}

	
	

	public String getTheByXpathCreateNewTicketButton() {
		return theByXpathCreateNewTicketButton;
	}



	public String getTheByXpathRefundButton() {
		return theByXpathRefundButton;
	}







	public String getTheByXpathWaiveShippingButton() {
		return theByXpathWaiveShippingButton;
	}


	public String getTheByXpathWaiveShippingCheckbox() {
		return theByXpathWaiveShippingCheckbox;
	}

	public String getTheByClassShippingValue() {
		return theByClassShippingValue;
	}


	public String getTheByXpathShippingValue() {
		return theByXpathShippingValue;
	}

	public String getTheByXpathOrderTotal() {
		return theByXpathOrderTotal;
	}


	public String getTheByXpathOrderTax() {
		return theByXpathOrderTax;
	}


	public String getTheByXpathOrderSubTotal() {
		return theByXpathOrderSubTotal;
	}


	public String getTheByXpathAuthAmount() {
		return theByXpathAuthAmount;
	}


	public String getDefaultQaOrder() {
		return defaultQaOrder;
	}


	public void setDefaultQaOrder(String defaultQaOrder) {
		this.defaultQaOrder = defaultQaOrder;
	}


	public String getDefaultDevOrder() {
		return defaultDevOrder;
	}


	public void setDefaultDevOrder(String defaultDevOrder) {
		this.defaultDevOrder = defaultDevOrder;
	}
}
