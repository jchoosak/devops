package TommyBahamaOutletRepository;



public class OutletCSCCheckoutTab {

	   private final String theByXpathCartTab = "//li[2]/div/div/div/span";
	    private final String theByXpathNewAddressButton = "//div[2]/div/div/span/table/tbody/tr[2]/td[2]";
	    private final String theByXpathCustomerSVN = "//div/input";
		private final String theByXpathCheckoutUseThisCard = "//td[9]/div/div/span/table/tbody/tr[2]/td[2]";
		private final String theByXpathRemovePaymentLink = "//td[7]/div/a";
	    private final String theByXpathPlaceOrderButton = "//div[2]/div/span/table/tbody/tr[2]/td[2]";
	  private final String theByXpathQtyDropdown = "//option[3]";
	  private final String theByXpathUpdateCartButton = "//td/span/table/tbody/tr[2]/td[2]";
	    
	    private OutletWebdriverEnvironment myEnvironment;
		private String testName;
		
		private final String theByXpathSubtotal = "//div[3]/div/div/div[2]/div/div/div/span[2]";
		private final String theByXpathShipping = "//div[2]/span[2]";
		private final String theByXpathTotalTax = "//div[3]/span[2]";
		private final String theByXpathOrderTotal = "//div[4]/span[2]";
		
	    public OutletCSCCheckoutTab(OutletWebdriverEnvironment theEnvironment, String theTestName) {
	    	
	    	myEnvironment = theEnvironment; 
	    	testName = theTestName;
	    }  

		public String getTheByXpathCartTab() {
			return theByXpathCartTab;
		}

		public String getTheByXpathNewAddressButton() {
			return theByXpathNewAddressButton;
		}

		public String getTheByXpathCustomerSVN() {
			return theByXpathCustomerSVN;
		}



		public String getTheByXpathCheckoutUseThisCard() {
			return theByXpathCheckoutUseThisCard;
		}



		public String getTheByXpathRemovePaymentLink() {
			return theByXpathRemovePaymentLink;
		}



		public String getTheByXpathPlaceOrderButton() {
			return theByXpathPlaceOrderButton;
		}

		public String getTheByXpathQtyDropdown() {
			return theByXpathQtyDropdown;
		}

		public String getTheByXpathUpdateCartButton() {
			return theByXpathUpdateCartButton;
		}

		public String getTheByXpathSubtotal() {
			return theByXpathSubtotal;
		}

		public String getTheByXpathShipping() {
			return theByXpathShipping;
		}

		public String getTheByXpathTotalTax() {
			return theByXpathTotalTax;
		}

		public String getTheByXpathOrderTotal() {
			return theByXpathOrderTotal;
		}

	
}

