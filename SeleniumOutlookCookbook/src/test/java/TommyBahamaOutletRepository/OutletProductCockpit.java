package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;




public class OutletProductCockpit {
	
	private final String theByCssSubmitLogin = "td.z-button-cm";
	private final String theByXpathSearchInput = "//td/table/tbody/tr/td/div/table/tbody/tr/td/input";
	private final String theByXpathSumbitSearch = "//td[2]/img";
	private final String theByXpathProductCatalogOnline = "/html/body/div/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[3]/div[2]/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div/div/div/table/tbody/tr[4]/td/div";
	private final String theByXpathPriceIcon = "//div[5]/div[2]/div/div/div/div/div/table/tbody/tr[3]/td/div/div/table/tbody/tr/td[5]/img";
    private final String theByXpathDevPriceIcon = "//td[5]/img";
	private final String theByXpathFinalPrice  = "/html/body/div[4]/div[3]/div/div/div/div/div/div/div[3]/div[2]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/table/tbody/tr/td[3]/div/div/input";
    private final String theByXpathPriceRowRadio = "//div[8]/div/table/tbody/tr/td[3]/div/span/table/tbody/tr/td/span/input";
    private final String theByXpathPriceRowDropDown = "//div[7]/div/table/tbody/tr/td[3]/div/div/span/span/img";
    private final String theByXpathPriceSale = "//div[6]/table/tbody/tr[3]/td[2]";
	private final String theByXpathOther = "/html/body/div[4]/div[3]/div/div/div/div/div/div/div[3]/div[2]/div/div/div/div/div[3]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]";
    private final String theByXpathCloseX = "/html/body/div[4]/div[2]/div/div/div/table/tbody/tr/td[3]/div";
	private final String theByXpathFirstResult = "//div[2]/div/div/div/img";
	private final String theUsrName = "admin";
	private final String thePswrd = "nimda";
	
	private final String theByXpathSaleOption = "//div[6]/table/tbody/tr[3]/td[2]";
	
	private final String theByXpathFirstPricePieces = "//div[3]/div/div[2]/div/div/div/span";
	private final String theByXpathSecondPricePieces = "//div[2]/div/div[2]/div/span";
	
	private WebDriver driver;
	
	private String testName;
	
	private OutletWebdriverEnvironment myEnvironment;
	
	public OutletProductCockpit(){
		
	}
	
    public OutletProductCockpit(WebDriver theDriver,
			OutletWebdriverEnvironment theEnvironment,  String theTestName){
    	
    	setDriver(theDriver);
    	setMyEnvironment(theEnvironment);
       	setTestName(theTestName);    	
		
	}
    
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public OutletWebdriverEnvironment getMyEnvironment() {
		return myEnvironment;
	}
	public void setMyEnvironment(OutletWebdriverEnvironment myEnvironment) {
		this.myEnvironment = myEnvironment;
	}
	public String getTheByCssSubmitLogin() {
		return theByCssSubmitLogin;
	}
	public String getTheByXpathSearchInput() {
		return theByXpathSearchInput;
	}
	public String getTheByXpathSumbitSearch() {
		return theByXpathSumbitSearch;
	}
	
	public String getTheByXpathProductCatalogOnline() {
		return theByXpathProductCatalogOnline;
	}

	public String getTheByXpathFirstResult() {
		return theByXpathFirstResult;
	}

	public String getTheByXpathPriceIcon() {
		return theByXpathPriceIcon;
	}

	public String getTheByXpathFinalPrice() {
		return theByXpathFinalPrice;
	}

	public String getTheByXpathPriceRowRadio() {
		return theByXpathPriceRowRadio;
	}

	public String getTheByXpathPriceRowDropDown() {
		return theByXpathPriceRowDropDown;
	}

	public String getTheByXpathPriceSale() {
		return theByXpathPriceSale;
	}

	public String getTheByXpathOther() {
		return theByXpathOther;
	}

	public String getTheByXpathCloseX() {
		return theByXpathCloseX;
	}

	public String getTheByXpathFirstPricePieces() {
		return theByXpathFirstPricePieces;
	}

	public String getTheByXpathSecondPricePieces() {
		return theByXpathSecondPricePieces;
	}

	public String getTheByXpathSaleOption() {
		return theByXpathSaleOption;
	}
	
	public void finalSaleProduct(String theProduct, String thePrice) throws InterruptedException {
		
		WebElement ce;

		System.out.println(driver.getCurrentUrl());
		
		if (driver.getCurrentUrl().contains("dev"))
			driver.get(myEnvironment.getTheDevOutletProductCockpitEnvironment());	
		else			
		    driver.get(myEnvironment.getTheOutletProductCockpitEnvironment());	
			
		myEnvironment.IsElementPresent(By.cssSelector(this.getTheByCssSubmitLogin()));
		ce = myEnvironment.waitForDynamicElement(By.cssSelector(this.getTheByCssSubmitLogin()));
		ce.click();		
		
		myEnvironment.IsElementPresent(By.xpath(this.getTheByXpathSearchInput()));

		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathSearchInput()));
		ce.click();
		ce.sendKeys(theProduct);

		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathSumbitSearch()));
		ce.click();
		
		myEnvironment.IsElementPresent(By.cssSelector(this.getTheByXpathSumbitSearch()));
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathSumbitSearch()));
		ce.click();
		
		while(!myEnvironment.IsElementPresent(By.xpath(this.getTheByXpathFirstResult()))){
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
		}

		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathFirstResult()));
		if(myEnvironment.getBrowser().contains("Safari")) {
			ce.click();
			Thread.sleep(myEnvironment.getThe_Default_Sleep());
			myEnvironment.IsElementPresent(By.xpath("//img[@title='Open in Edit Area']"));
			ce = myEnvironment.waitForDynamicElement(By.xpath("//img[@title='Open in Edit Area']"));
			ce.click();
			ce = myEnvironment.waitForDynamicElement(By.xpath("//img[@title='Open in Edit Area']"));
			ce.click();
		
			} else {
			Actions action = new Actions(driver);
			action.doubleClick(ce).perform();
			}
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		
		if (driver.getCurrentUrl().contains("dev")) {
			myEnvironment.IsElementPresent(By.xpath(this.getTheByXpathDevPriceIcon()));
			ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathDevPriceIcon()));
		ce.click();
	
		
		} else	{			  
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathPriceIcon()));
		ce.click();
		}
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathFirstPricePieces()));
		String firstRow = ce.getText();
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathSecondPricePieces()));
		String secondRow = ce.getText();
		
		Double firstRowD = null;
		firstRowD = Double.valueOf(firstRow);
		
		Double secondRowD = null;
		secondRowD = Double.valueOf(secondRow);
		System.out.println("This is the value of the first row          " + firstRowD);
		System.out.println("This is the value of the second row          " + secondRowD);
		if(firstRowD < secondRowD) {
			ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathFirstPricePieces()));
		  //  ce.click();
		}	else {
			ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathSecondPricePieces()));
		//ce.click();
		}
			
		if(myEnvironment.getBrowser().contains("Safari")) {
			ce.click();		
			myEnvironment.IsElementPresent(By.xpath("//td[2]/table/tbody/tr/td/div/div/img"));
			ce = myEnvironment.waitForDynamicElement(By.xpath("//td[2]/table/tbody/tr/td/div/div/img"));
			ce.click();	
			ce = myEnvironment.waitForDynamicElement(By.xpath("//td[2]/table/tbody/tr/td/div/div/img"));
			ce.click();	
			} else {
			Actions action = new Actions(driver);
			action.doubleClick(ce).perform();
			}
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());
		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathFinalPrice()));
		ce.click();
		ce.clear();
		ce.sendKeys(thePrice);
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathOther()));
		ce.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathPriceRowRadio()));
		ce.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathPriceRowDropDown()));
		
	/*	Thread.sleep(myEnvironment.getThe_Default_Sleep());
		ce.sendKeys("s ");
		ce.sendKeys(Keys.BACK_SPACE);
		
		Select selectThis = new Select(ce);
		
		selectThis.deselectByValue("SALE");*/
		ce.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());		
		
		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathSaleOption()));
		ce.click();
		
		Thread.sleep(myEnvironment.getThe_Default_Sleep());	

		ce = myEnvironment.waitForDynamicElement(By.xpath(this.getTheByXpathCloseX()));
		ce.click();
	}

	public String getTheByXpathDevPriceIcon() {
		return theByXpathDevPriceIcon;
	}


}
