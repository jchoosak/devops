package TommyBahamaOutletRepository;

public class OutletProducts {

	
private String productId;
private final String theShoeProduct = "TFM00140";
private final String theDinnerWareProduct = "TH31476";
private final String theGolfBag = "TBG-099";
private final String theDiffusor = "TH30875";
private final String theJacket = "T5667";
private final String theSearchProduct = "TB6020";
private final String theHighTShirtPrice = "T-Shirt$72.00";	
private final String theLowTShirtPrice = "T-Shirt$38.00";
private final String theMidTShirtPrice = "T-Shirt$45.00";
private final String theGBPTShirtPrice = "T-ShirtGBP_66.27";
private final String theHighPantsPrice = "Pants$128.00";
private final String theLowPantsPrice = "Pants$98.00";
private final String theColor = "3426";
private final String theSunglasses = "TB6028";
private final String theDefaultSockPrice = "$20.00";
private final String theJeansPrice = "Jeans$88.00";
private final String theBike = "TH30319";


private  String price;
private  String qty;
private String name;


public OutletProducts() {
	
}

public OutletProducts(String theId, String theQty, String theName) {
	
	setProductId(theId);
	setPrice(theId);
	setQty(theQty);
	setName(theName);
	
}

public String getProductId() {
	return productId;
}

public void setProductId(String productId) {
	this.productId = productId;
}

public String getTheShoeProduct() {
	return theShoeProduct;
}

public String getTheDinnerWareProduct() {
	return theDinnerWareProduct;
}

public String getTheGolfBag() {
	return theGolfBag;
}

public String getTheDiffusor() {
	return theDiffusor;
}

public String getTheJacket() {
	return theJacket;
}

public String getTheSearchProduct() {
	return theSearchProduct;
}

public String getTheHighTShirtPrice() {
	return theHighTShirtPrice;
}

public String getTheLowTShirtPrice() {
	return theLowTShirtPrice;
}

public String getTheHighPantsPrice() {
	return theHighPantsPrice;
}

public String getTheLowPantsPrice() {
	return theLowPantsPrice;
}

public String getTheColor() {
	return theColor;
}

public String getTheSunglasses() {
	return theSunglasses;
}

public String getTheDefaultSockPrice() {
	return theDefaultSockPrice;
}

public String getTheMidTShirtPrice() {
	return theMidTShirtPrice;
}

public String getTheJeansPrice() {
	return theJeansPrice;
}

public String getTheBike() {
	return theBike;
}

public String getTheGBPTShirtPrice() {
	// TODO Auto-generated method stub
	return this.theGBPTShirtPrice;
}

public String getPrice() {
	return price;
}

public void setPrice(String price) {
	this.price = price;
}

public String getQty() {
	return qty;
}

public void setQty(String qty) {
	this.qty = qty;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
	
	
}

