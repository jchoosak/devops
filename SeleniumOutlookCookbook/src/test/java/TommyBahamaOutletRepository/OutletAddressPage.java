package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class OutletAddressPage {
	private final String theByCssRestrictionLink = "a.tb_modal";
	private final String theByIdUseThisAddressButton = "deliveryAddress_select_link_1";

	private final String theURL = "https://";
	private final String theByIdTopContinueCheckoutBtn = "(//a[contains(text(),'Continue Checkout')])[2]";
	private final String theByNameSendEmailsCheckBox = "USER_ACCOUNT<>ATR_bol_send_email_updates";

	private final String theByCSSFinalSaleText= "b";
	private final String theByIdConfirmedEmail = "contact-email-confirm";
	private final String theByCssAddNewAddressButton = "a.neutral.right";
	private final String theByIdPhone2 = "bill_phone2";
	private final String theByIdPhone3 = "bill_phone3";
	private String theFirstNumbers = "253";
	private String theSecondNumbers = "205";
	private String theThirdNumbers = "1570";
	private final String theByIdBillingCountry = "bill_country";
	private final String theUSCountry = "option[value='US']";
	private final String theByIdBillingFName = "billing-name-first";
	private String theFirstName = "Jack";
	private final String theByIdBillingMName = "billing-name-middle";
	private String theMiddleName = "B";
	private final String theByIdBillingLName = "billing-name-last";
	private String theLastName = "West";
	private String theByIdBillingAddress = "billing-address";
	private String theWAAddress = "428 westlake avenue north";
	private String theCanadaAddress = "6361 FALLSVIEW BLVD";
	private final String theByIdBillingCity = "billing-city";
	private String theWACity = "ELLENSBURG";
	
	private final String theByIdBillingState = "billing-state";

	// private String theWAState = "//select[@id='billing-state']/option[48]";
	private final String theWAState = "option[value='WA']";
	private String theCanadaState = "ON";
	private final String theByIdBillingZipCode = "billing-zip-code";
	private String theWAZipCode = "98926-3305";
	private String theCanadaZip = "L2E 3E8";

	private final String theShippingCountry = "ship_country";
	private String theSCountry = "United States";

	private final String theCanadaCountry = "option[value='CA']";
	private final String theByIdShippingFirstName = "address.firstName";
	private String theShippingFirstName = "Jack";
	private final String theShippingMName = "shipping-name-middle";
	private String theSMiddleName = "C";
	private final String theByIdShippingLastName = "address.surname";
	private String theShippingLastName = "West";
	private final String theByIdShippingAddress = "address.line1";
	private final String theByIdShippingAddress2 = "address.line2";
	private String theHawaiiAddress = "4331 KAUAI BEACH DR";
	private final String theShippingAddress2 = "Suite 388";
	private final String theShippingCity = "Seattle";
	private final String theByIdShippingCity = "address.townCity";
	private String theHawaiiCity = "LIHUE";
	private final String theByIdShippingState = "address.region";
	private final String theByIdShippingCountry = "address.country";
	// private String theHawaiistate =
	// "//select[@id='shipping-state']/option[12]";
	private final String theHawaiistate = "#shipping-state > option[value='HI']";
	private final String theByIdShippingZipCode = "address.postcode";
	private String theHawaiiZipCode = "96766-8109";
	private String theSeattleZipCode = "98109";
	private final String theByIdShippingPhone = "address.phone";
	private final String theShippingPhone = "206-767-0988";

	private final String theByIdSaveShippingAddress = "saveAddressInMyAddressBook";
	
	private final String theByCssSubmitShippingAddress = "button.form.change_address_button";
	
	private final String theByIdAccountPaswrd = "account-password";
	private final String theByIdConfirmAccountPaswrd = "account-password-confirm";
	private final String theByIdCreateAccount = "account-create";
	private final String thePaswrd = "testing123";
	private final String theByIdAVSContinueButton = "continueBtn";
	private final String theReturnToSBLink = "//a[contains(text(),'Return to Shopping Bag')]";

	private final String thePageName = "AddressPage";

	private final String theByClassNameProductDiscountPrice = "red";
	private final String theByCssOrderSubtotal = "dd";
	
	private OutletWebdriverEnvironment myEnvironment;
	
	public OutletAddressPage(OutletWebdriverEnvironment theEnvironment) {
		myEnvironment = theEnvironment;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheByIdTopContinueCheckoutBtn() {
		return theByIdTopContinueCheckoutBtn;
	}

	public String getTheByNameSendEmailsCheckBox() {
		return theByNameSendEmailsCheckBox;
	}

	public String getTheByIdConfirmedEmail() {
		return theByIdConfirmedEmail;
	}

	public String getTheByIdPhone2() {
		return theByIdPhone2;
	}

	public String getTheByIdPhone3() {
		return theByIdPhone3;
	}

	public String getTheFirstNumbers() {
		return theFirstNumbers;
	}

	public void setTheFirstNumbers(String theFirstNumbers) {
		this.theFirstNumbers = theFirstNumbers;
	}

	public String getTheSecondNumbers() {
		return theSecondNumbers;
	}

	public void setTheSecondNumbers(String theSecondNumbers) {
		this.theSecondNumbers = theSecondNumbers;
	}

	public String getTheThirdNumbers() {
		return theThirdNumbers;
	}

	public void setTheThirdNumbers(String theThirdNumbers) {
		this.theThirdNumbers = theThirdNumbers;
	}

	public String getTheByIdBillingCountry() {
		return theByIdBillingCountry;
	}

	public String getTheUSCountry() {
		return theUSCountry;
	}

	public String getTheByIdBillingFName() {
		return theByIdBillingFName;
	}

	public String getTheFirstName() {
		return theFirstName;
	}

	public void setTheFirstName(String theFirstName) {
		this.theFirstName = theFirstName;
	}

	public String getTheByIdBillingMName() {
		return theByIdBillingMName;
	}

	public String getTheMiddleName() {
		return theMiddleName;
	}

	public void setTheMiddleName(String theMiddleName) {
		this.theMiddleName = theMiddleName;
	}

	public String getTheByIdBillingLName() {
		return theByIdBillingLName;
	}

	public String getTheLastName() {
		return theLastName;
	}

	public void setTheLastName(String theLastName) {
		this.theLastName = theLastName;
	}

	public String getTheByIdBillingAddress() {
		return theByIdBillingAddress;
	}

	public void setTheByIdBillingAddress(String theByIdBillingAddress) {
		this.theByIdBillingAddress = theByIdBillingAddress;
	}

	public String getTheWAAddress() {
		return theWAAddress;
	}

	public void setTheWAAddress(String theWAAddress) {
		this.theWAAddress = theWAAddress;
	}

	public String getTheCanadaAddress() {
		return theCanadaAddress;
	}

	public void setTheCanadaAddress(String theCanadaAddress) {
		this.theCanadaAddress = theCanadaAddress;
	}

	public String getTheByIdBillingCity() {
		return theByIdBillingCity;
	}

	public String getTheWACity() {
		return theWACity;
	}

	public void setTheWACity(String theWACity) {
		this.theWACity = theWACity;
	}

	public String getTheByIdBillingState() {
		return theByIdBillingState;
	}

	public String getTheWAState() {
		return theWAState;
	}

	public String getTheCanadaState() {
		return theCanadaState;
	}

	public void setTheCanadaState(String theCanadaState) {
		this.theCanadaState = theCanadaState;
	}

	public String getTheByIdBillingZipCode() {
		return theByIdBillingZipCode;
	}

	public String getTheWAZipCode() {
		return theWAZipCode;
	}

	public void setTheWAZipCode(String theWAZipCode) {
		this.theWAZipCode = theWAZipCode;
	}

	public String getTheCanadaZip() {
		return theCanadaZip;
	}

	public void setTheCanadaZip(String theCanadaZip) {
		this.theCanadaZip = theCanadaZip;
	}

	public String getTheShippingCountry() {
		return theShippingCountry;
	}

	public String getTheSCountry() {
		return theSCountry;
	}

	public void setTheSCountry(String theSCountry) {
		this.theSCountry = theSCountry;
	}

	public String getTheCanadaCountry() {
		return theCanadaCountry;
	}

	public String getTheShippingMName() {
		return theShippingMName;
	}

	public String getTheSMiddleName() {
		return theSMiddleName;
	}

	public void setTheSMiddleName(String theSMiddleName) {
		this.theSMiddleName = theSMiddleName;
	}

	public String getTheHawaiiAddress() {
		return theHawaiiAddress;
	}

	public void setTheHawaiiAddress(String theHawaiiAddress) {
		this.theHawaiiAddress = theHawaiiAddress;
	}

	public String getTheHawaiiCity() {
		return theHawaiiCity;
	}

	public void setTheHawaiiCity(String theHawaiiCity) {
		this.theHawaiiCity = theHawaiiCity;
	}

	public String getTheHawaiistate() {
		return theHawaiistate;
	}

	public String getTheHawaiiZipCode() {
		return theHawaiiZipCode;
	}

	public void setTheHawaiiZipCode(String theHawaiiZipCode) {
		this.theHawaiiZipCode = theHawaiiZipCode;
	}

	public String getTheByIdAccountPaswrd() {
		return theByIdAccountPaswrd;
	}

	public String getTheByIdConfirmAccountPaswrd() {
		return theByIdConfirmAccountPaswrd;
	}

	public String getTheByIdCreateAccount() {
		return theByIdCreateAccount;
	}

	public String getThePaswrd() {
		return thePaswrd;
	}

	public String getTheReturnToSBLink() {
		return theReturnToSBLink;
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByCssRestrictionLink() {
		return theByCssRestrictionLink;
	}

	public String getTheByCSSFinalSaleText() {
		return theByCSSFinalSaleText;
	}

	public String getTheByIdUseThisAddressButton() {
		return theByIdUseThisAddressButton;
	}

	public String getTheByClassNameProductDiscountPrice() {
		return theByClassNameProductDiscountPrice;
	}

	public String getTheByCssOrderSubtotal() {
		return theByCssOrderSubtotal;
	}

	public String getTheByIdShippingFirstName() {
		return theByIdShippingFirstName;
	}

	public String getTheByIdShippingLastName() {
		return theByIdShippingLastName;
	}

	public String getTheShippingFirstName() {
		return theShippingFirstName;
	}

	public void setTheShippingFirstName(String theShippingFirstName) {
		this.theShippingFirstName = theShippingFirstName;
	}

	public String getTheShippingLastName() {
		return theShippingLastName;
	}

	public void setTheShippingLastName(String theShippingLastName) {
		this.theShippingLastName = theShippingLastName;
	}

	public String getTheByIdShippingAddress() {
		return theByIdShippingAddress;
	}

	public String getTheByIdShippingAddress2() {
		return theByIdShippingAddress2;
	}

	public String getTheByIdShippingCity() {
		return theByIdShippingCity;
	}

	public String getTheByIdShippingCountry() {
		return theByIdShippingCountry;
	}

	public String getTheByIdShippingState() {
		return theByIdShippingState;
	}

	public String getTheByIdShippingZipCode() {
		return theByIdShippingZipCode;
	}

	public String getTheByIdShippingPhone() {
		return theByIdShippingPhone;
	}

	public String getTheByIdSaveShippingAddress() {
		return theByIdSaveShippingAddress;
	}

	public String getTheByCssSubmitShippingAddress() {
		return theByCssSubmitShippingAddress;
	}

	public String getTheShippingAddress2() {
		return theShippingAddress2;
	}

	public String getTheShippingCity() {
		return theShippingCity;
	}

	public String getTheSeattleZipCode() {
		return theSeattleZipCode;
	}

	public void setTheSeattleZipCode(String theSeattleZipCode) {
		this.theSeattleZipCode = theSeattleZipCode;
	}

	public String getTheShippingPhone() {
		return theShippingPhone;
	}

	public String getTheByIdAVSContinueButton() {
		return theByIdAVSContinueButton;
	}

	public String getTheByCssAddNewAddressButton() {
		return theByCssAddNewAddressButton;
	}
	
	
	public void addShippingAddress( String theAddressLabel, String theFirstName, String theLastName,
			String theStreetAddress, String theCity, String theState, String theZipCode,
			String theAreaCode, String thePrefix, String thePhoneNumber) {
		// TODO Auto-generated method stub
		WebElement ce;
		
		/*ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressLabel()));
		ce.clear();
		ce.sendKeys(theAddressLabel);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressFirstName()));
		ce.clear();
		ce.sendKeys(theFirstName);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressLastName()));
		ce.clear();
		ce.sendKeys(theLastName);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressAddressOne()));
		ce.clear();
		ce.sendKeys(theStreetAddress);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressCity()));
		ce.clear();
		ce.sendKeys(theCity);
		
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressState()));
	  Select theSelectedState = new Select(ce);
	  ce.click();
	  theSelectedState.selectByVisibleText(theState);
	
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPostal()));
		ce.clear();
		ce.sendKeys(theZipCode);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhoneAreaCode()));
		ce.clear();
		ce.sendKeys(theAreaCode);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhonePrefix()));
		ce.clear();
		ce.sendKeys(thePrefix);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhoneNumber()));
		ce.clear();
		ce.sendKeys(thePhoneNumber);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressDefaultAddress()));
		ce.click();
	*/
	}
	
	public void addShippingAddress( ) {
		// TODO Auto-generated method stub
		WebElement ce;
		
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(this.getTheByCssAddNewAddressButton()));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingFirstName()));
		ce.sendKeys(this.getTheFirstName());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingLastName()));
		ce.sendKeys(this.getTheLastName());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingAddress()));
		ce.sendKeys(this.getTheWAAddress());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingAddress2()));
		ce.sendKeys(this.getTheShippingAddress2());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingCity()));
		ce.sendKeys(this.getTheShippingCity());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingCountry()));
			Select selectCountry = new Select(ce);
			selectCountry.selectByIndex(1);
		ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdShippingState()));
		selectCountry = new Select(ce);
				selectCountry.selectByIndex(48);
		ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdShippingZipCode()));
		ce.sendKeys(this.getTheSeattleZipCode());	
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingPhone()));
		ce.sendKeys(this.getTheShippingPhone());
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdSaveShippingAddress()));
		ce.click();
			
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(this.getTheByCssSubmitShippingAddress()));
		ce.click();
		
		
		myEnvironment.IsElementPresent(By.id(this.getTheByIdAVSContinueButton()));
		
		
		//Thread.sleep(10000);
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdAVSContinueButton()));		
		ce.click();
	
	}

	
	public void addDefaultShippingAddress( String theAddressLabel, String theFirstName, String theLastName,
			String theStreetAddress, String theCity, String theState, String theZipCode,
			String theAreaCode, String thePrefix, String thePhoneNumber) {
		// TODO Auto-generated method stub
		WebElement ce;
		
	/*	ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressLabel()));
		ce.clear();
		ce.sendKeys(theAddressLabel);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressFirstName()));
		ce.clear();
		ce.sendKeys(theFirstName);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressLastName()));
		ce.clear();
		ce.sendKeys(theLastName);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressAddressOne()));
		ce.clear();
		ce.sendKeys(theStreetAddress);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressCity()));
		ce.clear();
		ce.sendKeys(theCity);
		
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressState()));
	  Select theSelectedState = new Select(ce);
	  ce.click();
	  theSelectedState.selectByVisibleText(theState);
	
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPostal()));
		ce.clear();
		ce.sendKeys(theZipCode);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhoneAreaCode()));
		ce.clear();
		ce.sendKeys(theAreaCode);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhonePrefix()));
		ce.clear();
		ce.sendKeys(thePrefix);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressPhoneNumber()));
		ce.clear();
		ce.sendKeys(thePhoneNumber);
		ce = myEnvironment.waitForDynamicElement( By.name(this.getTheByNameShiptoAddressDefaultAddress()));
		ce.click();*/
	
	}
	
	public void addDefaultShippingAddress(OutletMyAccountPage myAccount ) {
		// TODO Auto-generated method stub
		WebElement ce;
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myAccount.getTheByXPathAddNewAddress()));		
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingFirstName()));
		ce.clear();
		ce.sendKeys(this.getTheFirstName());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingLastName()));
		ce.clear();
		ce.sendKeys(this.getTheLastName());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingAddress()));
		ce.sendKeys(this.getTheWAAddress());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingAddress2()));
		ce.sendKeys(this.getTheShippingAddress2());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingCity()));
		ce.sendKeys(this.getTheShippingCity());
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingCountry()));
		 Select  selectCountry = new Select(ce);
			selectCountry.selectByIndex(1);
		ce = myEnvironment.waitForDynamicElement(
					By.id(this.getTheByIdShippingState()));
		selectCountry = new Select(ce);
				selectCountry.selectByIndex(48);
		ce = myEnvironment.waitForDynamicElement(
						By.id(this.getTheByIdShippingZipCode()));
		ce.sendKeys(this.getTheSeattleZipCode());	
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(this.getTheByIdShippingPhone()));
		ce.sendKeys(this.getTheShippingPhone());
		
		ce = myEnvironment.waitForDynamicElement(
				By.id(myAccount.getTheByIdSetAsDefaultAddress()));
		ce.click();
			
		ce = myEnvironment.waitForDynamicElement(
				By.cssSelector(this.getTheByCssSubmitShippingAddress()));
		ce.click();
		
	
	}
	
	
}