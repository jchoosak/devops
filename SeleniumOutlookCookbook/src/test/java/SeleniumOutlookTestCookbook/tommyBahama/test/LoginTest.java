package SeleniumOutlookTestCookbook.tommyBahama.test;





import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;
import TommyBahamaOutletRepository.OutletAddressPage;
import TommyBahamaOutletRepository.OutletCSC;
import TommyBahamaOutletRepository.OutletConfirmationPage;
import TommyBahamaOutletRepository.OutletDeliveryMethodPage;
import TommyBahamaOutletRepository.OutletHMC;
import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletHmcLeftNav;
import TommyBahamaOutletRepository.OutletHomeDecor;
import TommyBahamaOutletRepository.OutletPaymentPage;
import TommyBahamaOutletRepository.OutletPaymentech;
import TommyBahamaOutletRepository.OutletPreviewPage;
import TommyBahamaOutletRepository.OutletProductDetailPage;
import TommyBahamaOutletRepository.OutletProductListingPage;
import TommyBahamaOutletRepository.OutletProducts;
import TommyBahamaOutletRepository.OutletSearchResultsPage;
import TommyBahamaOutletRepository.OutletShoppingCart;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;



@RunWith(JUnit4.class)
public class LoginTest {

	private OutletWebdriverEnvironment myEnvironment;
	private OutletHeader myHeader;
	private OutletProductListingPage myPLP;
	private OutletProductDetailPage myPDP;
	private OutletSignInPage mySignInPage;

	private OutletShoppingCart myShoppingCart;
	private OutletAddressPage myAddressPage;
	private OutletPaymentPage myPaymentPage;
	private OutletDeliveryMethodPage myDeliveryMethod;
	private OutletPreviewPage myPreviewPage;
	private OutletConfirmationPage myConfirmationPage;
	private OutletCSC myCSC;
	private OutletSearchResultsPage mySearchResults;
	private OutletPaymentech myPaymentech;
	private String testName = "LogInTest";
	private OutletProducts swimProduct = new OutletProducts("TR924", "4","Swim");
	private String OrderNumber = "";
	private WebDriver driver;
	private OutletHmcLeftNav myHmcLeftNav = new OutletHmcLeftNav();
	private OutletHMC myHMC;
	
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {
		
	Platform currentPlatform = Platform.getCurrent();
	/*	switch (System.getenv("SELENIUM_BROWSER")) {

	case "SAFARI":

		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		break;

	case "FF":

		File fileToProfile = new File("./Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use
															// 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		break;

	case "CHROME":

		if (currentPlatform.is(Platform.WINDOWS))
			System.setProperty("webdriver.chrome.driver",
					"./chromedriver.exe");
		else {
			System.setProperty("webdriver.chrome.driver",
					"/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		break;
		
	case "IE": 		
				Assert.assertTrue(isSupportedPlatform());
			//	File file = new File(myEnvironment.getIeWebdriver());
				System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
				DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
				ieCap.setVersion("9");
				ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
				driver = new InternetExplorerDriver(ieCap);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getIeBrowser());
				break;
	default:
		
	System.out.println("Browser was not valid!");
				
	}*/
	
	if (System.getenv("SELENIUM_BROWSER").contains("FF")) {
		File fileToProfile = new File("./Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
	}	else if(System.getenv("SELENIUM_BROWSER").contains("SAFARI")) {
		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
	}
	else if (System.getenv("SELENIUM_BROWSER").contains("IE")) {
		Assert.assertTrue(isSupportedPlatform());
	//	File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
	}
	
	else {
		if(currentPlatform.is(Platform.WINDOWS))
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		else {
		//	File myChromeDriver = new File("./chromedriver");
		//	myChromeDriver.setExecutable(true);
		//System.setProperty("LD_LIBRARY_PATH", "/opt/google/chrome/lib /usr/local/bin/chromedriver");
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
	
	}
		
		myHeader = new OutletHeader(driver, myEnvironment);
		mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
		myPLP = new OutletProductListingPage(driver, myEnvironment);
		myPDP = new OutletProductDetailPage(driver, myEnvironment, myPLP,
				myHeader, testName);
		 myHMC = new OutletHMC(driver, myEnvironment, myHmcLeftNav, testName);
		myPaymentPage = new OutletPaymentPage(myEnvironment, testName);
		 mySearchResults = new OutletSearchResultsPage(myEnvironment, myHeader);
	
		myAddressPage = new OutletAddressPage(myEnvironment);
		myDeliveryMethod = new OutletDeliveryMethodPage();
		myPreviewPage = new OutletPreviewPage();
		myConfirmationPage = new OutletConfirmationPage();
		myShoppingCart = new OutletShoppingCart(driver, myEnvironment);
		
		String tempFile = "../seleniumhq";
		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myPLP.getThePageName() + swimProduct.getName());
		fns.add(myPDP.getThePageName() + swimProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + swimProduct.getProductId());
		fns.add(myShoppingCart.getThePageName());
		fns.add(myAddressPage.getThePageName());
		fns.add(myDeliveryMethod.getThePageName());
		fns.add(myPaymentPage.getThePageName() + "Select");
		fns.add(myPaymentPage.getThePageName());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());	
		fns.add("LogInPage");
		fns.add("CSCLandingPage");
		fns.add("SearchOrderFormWithResults");
		fns.add("OrderTab");
		fns.add("OrderTab2");
		myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void OutletWSLogInTest() throws InterruptedException,
			IOException {
		WebElement ce;
	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTheTestingEnvironment(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTestSubject(testName);
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		driver.manage().window().maximize();

	for(int i = 0; i < 5; i++) {
		
		System.out.println("This is how many times we have signed in:         " + i);
		
		mySignInPage.signInOutlet(myEnvironment.getTheEcommEmailOne(), myEnvironment.getPassWrd());
		Thread.sleep(1000);
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myHeader.getTheByXPathSignOut()));
		ce.click();	
		
	}
	

	

	myEnvironment.setTestPassed(true);
}

@After
public void quitDriver() throws MessagingException,
		IOException {

	// fires if the test did not pass. Takes SS of last rendered page. 
	if(!myEnvironment.getTestPassed()) {
	myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
			"TestFailed", "OnCurrentPage");
	}

	myEnvironment.setNetworkFile("../seleniumhq/" + this.testName);
	
	
	BufferedWriter bw = new BufferedWriter(new FileWriter(
			myEnvironment.getNetworkFile() + ".html"));
	
	bw.write("<html><body><center><h2>Outlet Email Sign Up Test</h2>"
			+ myEnvironment.getEnvironmentUsedString()
			+ myEnvironment.getEnvironment()
			+ "<br/><br/>"
			+ myEnvironment.getBrowserUsedString()
			+"<b>"
			+ myEnvironment.getBrowser()
			+ "</b><br/><br/>"
			+ "<h3>This test is to see if user is able to sign up for emails.</h3> <table style= \"width:70%;\"><tr><td><p> "
			+ myEnvironment.getTestTextDescription()
			+ myEnvironment.getBrowser()
			+ "\\"
			+ this.testName
			+ "</b></p></td></tr></table><br/>"
			+
			
			myEnvironment.addSSToFile(testName, "SignInPageBlank",
					"This is the landing page.")
			+
			
	    	myEnvironment.getPageTestOutcome()
			+

			"</center></body></html>");
	bw.close();

	BodyPart htmlPart = new MimeBodyPart();
	htmlPart.setContent(
			"<html><body><center><h2>Outlet Email Sign Up</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+"<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if the User is able to sign up for emails.</h3> "
					+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
					+ "file:///"
					+ myEnvironment.getNetworkFile()
					+ "\\" + this.testName + ".html"
					+ "<br/></center></body></html>", "text/html");
				
					
		
	List<String> fns = new ArrayList<String>();
	fns.add("SignInPageBlank");
	fns.add("EmailSignUpBlank");
	fns.add("EmailSignUpFilledOut");
	fns.add("EmailSignUpSuccessModal");
	
	/*ss = ss - 3;
	

	myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
			myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
			myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
			myEnvironment.getJackTitle(),
			myEnvironment.getNetworkTestDirectory(),
			myEnvironment.getBrowser(), testName);*/
	driver.quit();

}

}
