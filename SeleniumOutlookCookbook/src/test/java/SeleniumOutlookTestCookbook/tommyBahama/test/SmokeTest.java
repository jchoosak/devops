package SeleniumOutlookTestCookbook.tommyBahama.test;



	import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;
import TommyBahamaOutletRepository.OutletAddressPage;
import TommyBahamaOutletRepository.OutletCSC;
import TommyBahamaOutletRepository.OutletConfirmationPage;
import TommyBahamaOutletRepository.OutletDeliveryMethodPage;
import TommyBahamaOutletRepository.OutletHMC;
import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletHmcLeftNav;
import TommyBahamaOutletRepository.OutletHomeDecor;

import TommyBahamaOutletRepository.OutletPaymentPage;
import TommyBahamaOutletRepository.OutletPaymentech;
import TommyBahamaOutletRepository.OutletPreviewPage;
import TommyBahamaOutletRepository.OutletProductDetailPage;
import TommyBahamaOutletRepository.OutletProductListingPage;
import TommyBahamaOutletRepository.OutletProducts;
import TommyBahamaOutletRepository.OutletSearchResultsPage;
import TommyBahamaOutletRepository.OutletShoppingCart;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;



	@RunWith(JUnit4.class)
	public class SmokeTest {

		private OutletWebdriverEnvironment myEnvironment;
		private OutletHeader myHeader;
		private OutletProductListingPage myPLP;
		private OutletProductDetailPage myPDP;
		private OutletSignInPage mySignInPage;
	
		private OutletShoppingCart myShoppingCart;
		private OutletAddressPage myAddressPage;
		private OutletPaymentPage myPaymentPage;
		private OutletDeliveryMethodPage myDeliveryMethod;
		private OutletPreviewPage myPreviewPage;
		private OutletConfirmationPage myConfirmationPage;
		private OutletCSC myCSC;
		private OutletSearchResultsPage mySearchResults;
		private OutletPaymentech myPaymentech;
		private String testName = "index";
		private OutletProducts swimProduct = new OutletProducts("TR924", "4","Swim");
		private String OrderNumber = "";
		private WebDriver driver;
		private OutletHmcLeftNav myHmcLeftNav = new OutletHmcLeftNav();
		private OutletHMC myHMC;
		
		private static boolean isSupportedPlatform() {
			Platform currentPlatform = Platform.getCurrent();
			return Platform.MAC.is(currentPlatform)
					|| Platform.WINDOWS.is(currentPlatform);
		}

		@Before
		public void openBrowser() {
			
			Platform currentPlatform = Platform.getCurrent();
			
		/*	switch (System.getenv("SELENIUM_BROWSER")) {

			case "SAFARI":

				DesiredCapabilities mc = DesiredCapabilities.safari();
				mc.setCapability("applicationCacheEnabled", true);
				driver = new SafariDriver(mc);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
				break;

			case "FF":

				File fileToProfile = new File("./Profile");
				FirefoxProfile p = new FirefoxProfile(fileToProfile);
				p.setPreference("javascript.enabled", true);
				p.setPreference("webdriver.load.strategy", "fast"); // can use
																	// 'fast'
				driver = new FirefoxDriver(p);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getFfBrowser());
				break;

			case "CHROME":

				if (currentPlatform.is(Platform.WINDOWS))
					System.setProperty("webdriver.chrome.driver",
							"./chromedriver.exe");
				else {
					System.setProperty("webdriver.chrome.driver",
							"/usr/bin/chromedriver");
				}
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				driver = new ChromeDriver(options);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
				break;
				
			case "IE": 		
						Assert.assertTrue(isSupportedPlatform());
					//	File file = new File(myEnvironment.getIeWebdriver());
						System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
						DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
						ieCap.setVersion("9");
						ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
						driver = new InternetExplorerDriver(ieCap);
						myEnvironment = new OutletWebdriverEnvironment(driver);
						myEnvironment.setBrowser(myEnvironment.getIeBrowser());
						break;
			default:
				
			System.out.println("Browser was not valid!");
						
			}*/
			
			if (System.getenv("SELENIUM_BROWSER").contains("FF")) {
				File fileToProfile = new File("./Profile");
				FirefoxProfile p = new FirefoxProfile(fileToProfile);
				p.setPreference("javascript.enabled", true);
				p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
				driver = new FirefoxDriver(p);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getFfBrowser());
			}	else if(System.getenv("SELENIUM_BROWSER").contains("SAFARI")) {
				DesiredCapabilities mc = DesiredCapabilities.safari();
				mc.setCapability("applicationCacheEnabled", true);
				driver = new SafariDriver(mc);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
			}
			else if (System.getenv("SELENIUM_BROWSER").contains("IE")) {
				//Assert.assertTrue(isSupportedPlatform());
			//	File file = new File(myEnvironment.getIeWebdriver());
				System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
				DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
				ieCap.setVersion("11");
				ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
				driver = new InternetExplorerDriver(ieCap);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getIeBrowser());
			}
			
			else {
				if(currentPlatform.is(Platform.WINDOWS))
				System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
				else {
				//	File myChromeDriver = new File("./chromedriver");
				//	myChromeDriver.setExecutable(true);
				//System.setProperty("LD_LIBRARY_PATH", "/opt/google/chrome/lib /usr/local/bin/chromedriver");
					System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
				}
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				driver = new ChromeDriver(options);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
			
			}
			
			
			myHeader = new OutletHeader(driver, myEnvironment);
			mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
			myPLP = new OutletProductListingPage(driver, myEnvironment);
			myPDP = new OutletProductDetailPage(driver, myEnvironment, myPLP,
					myHeader, testName);
			 myHMC = new OutletHMC(driver, myEnvironment, myHmcLeftNav, testName);
			myPaymentPage = new OutletPaymentPage(myEnvironment, testName);
			 mySearchResults = new OutletSearchResultsPage(myEnvironment, myHeader);
		
			myAddressPage = new OutletAddressPage(myEnvironment);
			myDeliveryMethod = new OutletDeliveryMethodPage();
			myPreviewPage = new OutletPreviewPage();
			myConfirmationPage = new OutletConfirmationPage();
			myShoppingCart = new OutletShoppingCart(driver, myEnvironment);
			
			String tempFile = "../seleniumhq";
			List<String> fns = new ArrayList<String>();
			fns.add(mySignInPage.getThePageName());
			fns.add(myPLP.getThePageName() + swimProduct.getName());
			fns.add(myPDP.getThePageName() + swimProduct.getProductId());
			fns.add(myPDP.getThePreviewPaneName() + swimProduct.getProductId());
			fns.add(myShoppingCart.getThePageName());
			fns.add(myAddressPage.getThePageName());
			fns.add(myDeliveryMethod.getThePageName());
			fns.add(myPaymentPage.getThePageName() + "Select");
			fns.add(myPaymentPage.getThePageName());
			fns.add(myPreviewPage.getThePageName());
			fns.add(myConfirmationPage.getThePageName());	
			fns.add("LogInPage");
			fns.add("CSCLandingPage");
			fns.add("SearchOrderFormWithResults");
			fns.add("OrderTab");
			fns.add("OrderTab2");
			myEnvironment.removeSS(fns, tempFile);
		}

		@Test
		public void SmokeOutletTest() throws InterruptedException,
				IOException {
			WebElement ce;
		
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			System.out.println(System.getenv("SELENIUM_TEST_URL"));
			myEnvironment.setTheTestingEnvironment(System.getenv("SELENIUM_TEST_URL"));
			myEnvironment.setTestSubject(testName);
			
			driver.get(myEnvironment.getTheTestingEnvironment());
			driver.manage().window().maximize();
			
			// this is to make the right cronjob per environment.
	/*		if(driver.getCurrentUrl().contains("dev")) {
			myHMC.fireSiteDetailsCronJob(myHMC.getTheByIdSiteDetailsFirstCronJob());
			} else {
				myHMC.fireSiteDetailsCronJob(myHMC.getTheByIdSiteDetailsFirstCronJob());
				myHMC.fireSiteDetailsCronJob(myHMC.getTheByIdSiteDetailsSecondCronJob());
			}		
*/
			//go to the environment that we are testing.
			driver.get(myEnvironment.getTheTestingEnvironment());

			// Sign in to WS
	if (System.getenv("SELENIUM_BROWSER").contains("IE")) {
		mySignInPage.signIn(mySignInPage.getTheIEUserName(),
				myEnvironment.getPassWrd());
			}
	else mySignInPage.signIn(myEnvironment.getTheEcommEmailOne(),
					myEnvironment.getPassWrd());
			// verify that we are logged in. 
			myEnvironment.waitForTitle("Official Site");
			// execute a search
			mySearchResults.executeSearch("Mens");

	/*	if(driver.getCurrentUrl().contains("qa")) {
			// nav through the top nav to a PLP.
					ce = myEnvironment.waitForDynamicElement(By.xpath(myHeader.getTheByXPathMensTab()));
					ce.click();
						
					myEnvironment.waitForURL("Men");
					
		}
		else {*/
			ce = myEnvironment.waitForDynamicElement(By.xpath(myHeader.getTheByXPathWomensTab()));
			ce.click();
				
			myEnvironment.waitForURL("Women");
		//}
		ce = myEnvironment.waitForDynamicElement(By.xpath("//span/img"));
		ce = myEnvironment.waitForDynamicElement(By.xpath("//span/img"));
		ce.click();
		
		
			
			// add a product to the cart
			myPDP.addQuantityToBag(swimProduct.getQty());
			
		
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myPDP.getThePreviewPaneName(), swimProduct.getProductId());

			// continue to shopping bag
			ce = myEnvironment.waitForDynamicElement(By.linkText(myHeader
					.getTheByLinkCheckout()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForTitle("Shopping");

			ce = myEnvironment.waitForDynamicElement(By.name(myShoppingCart
					.getTheByNameQtyUpdate()));

			// change the value of the product to 3, 
			// this is to make the test the same even if it failed on previous attempts. 
			Select clickThis = new Select(ce);
			clickThis.selectByValue("3");

			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myShoppingCart.getThePageName(), "");

			// continue to address page
			ce = myEnvironment.waitForDynamicElement(By.id(myShoppingCart
					.getTheByIdBottomContinueCheckout()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			myEnvironment.waitForURL("address");
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myAddressPage.getThePageName(), "");

			// continue to delivery options
			ce = myEnvironment.waitForDynamicElement(By.xpath(myAddressPage
					.getTheByIdTopContinueCheckoutBtn()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			myEnvironment.waitForURL("delivery-method");
			myEnvironment.waitForClickableElement(By.id(myDeliveryMethod
					.getTheByIdContinueCheckoutButton()));
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myDeliveryMethod.getThePageName(), "");

			// continue to Select PaymentPage
			ce = myEnvironment.waitForDynamicElement(By.id(myDeliveryMethod
					.getTheByIdContinueCheckoutButton()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			myEnvironment.waitForURL("choose-payment-method");
			myEnvironment.waitForClickableElement(By.id(myPaymentPage
					.getTheByIdContinueCheckout()));
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myPaymentPage.getThePageName(), "Select");

			// continue to PaymentPage
			ce = myEnvironment.waitForDynamicElement(By.id(myPaymentPage
					.getTheByIdContinueCheckout()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			myEnvironment.waitForURL("add-payment-method");
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myPaymentPage.getThePageName(), "");

			// enter the cc verification number
			ce = myEnvironment.waitForDynamicElement(By.id(myPaymentPage
					.getTheByIdCCSecurityCode()));
			ce.sendKeys(myPaymentPage.getTheCode());

			// continue to Order Review page
			ce = myEnvironment.waitForDynamicElement(By.id(myPaymentPage
					.getTheSubmitPaymentBtn()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			myEnvironment.waitForURL("summary");
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myPreviewPage.getThePageName(), "");

			// submit order
			ce = myEnvironment.waitForDynamicElement(By.xpath(myPreviewPage
					.getTheByXpathSubmitButton()));
			ce.click();
			myEnvironment.waitForPageLoaded();
			myEnvironment.waitForDocumentReadyState();
			myEnvironment.waitForTitle("Confirmation");
			
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					myConfirmationPage.getThePageName(), "");

			// pull the order number from the order confirmation page.
			OrderNumber = driver.getCurrentUrl();
			if (driver.getCurrentUrl().contains("dev"))
				OrderNumber = OrderNumber.substring(65);
			else
				OrderNumber = OrderNumber.substring(64);
			
			// pull the tax from the order confirmation page. 
			String theTax = driver.findElement(
					By.id(myConfirmationPage.getTheTotalTax())).getText();
			System.out.println("This is the tax: 	" + theTax);
			String theTotal = driver.findElement(
					By.id(myConfirmationPage.getTheTotalAmount())).getText();

			System.out.println("This is the total amount: 	" + theTotal);
			myCSC = new OutletCSC(driver, myEnvironment, "", theTax, OrderNumber,
					testName, myEnvironment.getBrowser());

			// check the order in the CSC. This searches for the last order created and makes 
			// sure the info matches the order created on WS. 
			myCSC.checkOrderInCSC(theTotal, "");

			// See that the auth happened in OG
			// remove the $ sign because money values in Paymentech do not
			// dispaly the dollar sign
			Double theDTotal = 0.0;
			theDTotal = myPaymentPage.pullPrice(theTotal);
			String newTotal = "";
			myPaymentech = new OutletPaymentech(driver, myEnvironment, OrderNumber,
					testName, myEnvironment.getBrowser());
			newTotal = myPaymentech.formatTotal(newTotal, theDTotal);
		
			
			// This part is currently commented out because of the problem of
			// entering code for new log in location.
			
			// myPaymentech
			// .checkOrderInPayTech(newTotal);

			// Set flag that test passed.
			myEnvironment.setTestPassed(true);
		}

		@After
		public void quitDriver() throws MessagingException, IOException {
		
			// fires if the test did not pass. Takes SS of last rendered page. 
			if(!myEnvironment.getTestPassed()) {
			myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
					"TestFailed", "OnCurrentPage");
			}

			myEnvironment.setNetworkFile("../seleniumhq/" + this.testName);

			BufferedWriter bw = new BufferedWriter(new FileWriter(
					myEnvironment.getNetworkFile() + ".html"));

			bw.write("<html><body><center><h2>Outlet Single Division Order</h2>"
					+ myEnvironment.getEnvironmentUsedString()
					+ myEnvironment.getEnvironment()
					+ "<br/><br/>"
					+ myEnvironment.getBrowserUsedString()
					+ "<b>"
					+ myEnvironment.getBrowser()
					+ "</b><br/><br/>"
					+ "<h3>This test is to see if a single division order is completed correctly on WS and in CSC.</h3> <table style= \"width:70%;\"><tr><td><p> "
					+ myEnvironment.getTestTextDescription()
					+ myEnvironment.getBrowser()
					+ "\\"
					+ this.testName
					+ "</b></p></td></tr></table><br/>"
					+

					myEnvironment.addSSToFile(testName,
							mySignInPage.getThePageName(),
							"This is the Sign In page.")
					+

					myEnvironment.addSSToFile(testName, myPLP.getThePageName()
							+ swimProduct.getName(), "This is the Dinnerware PLP.")
					+

					myEnvironment.addSSToFile(testName, myPDP.getThePageName()
							+ swimProduct.getProductId(),
							"This is the Dinnerware PDP.")
					+

					myEnvironment.addSSToFile(testName,	myPDP.getThePreviewPaneName()
									+ swimProduct.getProductId(),
							"This is the Dinnerware PDP with Preview pane displaying added product.")
					+
					
						myEnvironment.addSSToFile(testName,	myShoppingCart.getThePageName(),
							"This is the Shopping Cart displaying added product with the reduced qty of 3.")
					+
					
						myEnvironment.addSSToFile(testName,	myAddressPage.getThePageName(),
							"This is the Address Page.")
					+
						myEnvironment.addSSToFile(testName,	myDeliveryMethod.getThePageName(),
							"This is the Delivery Method Page.")
					+
					
						myEnvironment.addSSToFile(testName,	myPaymentPage.getThePageName() + "Select",
							"This is the page to select the users payment type.")
					+
					
						myEnvironment.addSSToFile(testName,	myPaymentPage.getThePageName(),
							"This is the Payment Page.")
					+
					
						myEnvironment.addSSToFile(testName,	myPreviewPage.getThePageName(),
							"This is the Review Page to review the current order.")
					+
					
						myEnvironment.addSSToFile(testName,	myConfirmationPage.getThePageName(),
							"This is the Confirmation Page that " +
							"gives the user an order number to track their order.")
					+

					myEnvironment.addSSToFile(testName, "LogInPage",
							"This is the CSC login page.")
					+

					myEnvironment.addSSToFile(testName, "CSCLandingPage",
							"This is the CSC landing page.")
					+

					myEnvironment.addSSToFile(testName,
							"SearchOrderFormWithResults",
							"This is the search modal for orders.")
					+

					myEnvironment.addSSToFile(testName, "OrderTab",
							"This is the Order Detail page.")
					+

					myEnvironment.addSSToFile(testName, "OrderTab2",
							"This is the bottom of the Order Detail page.") +

					myEnvironment.getPageTestOutcome() +

					"</center></body></html>");
			bw.close();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(
					"<html><body><center><h2>Single Divison Order</h2>"
							+ myEnvironment.getEnvironmentUsedString()
							+ myEnvironment.getEnvironment()
							+ "<br/><br/>"
							+ myEnvironment.getBrowserUsedString()
							+ "<b>"
							+ myEnvironment.getBrowser()
							+ "</b><br/><br/>"
							+ "<h3>This test is to see if a single division order is correct on WS and CSC.</h3> "
							+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
							+ "" + myEnvironment.getNetworkFile() + ".html"
							+ "<br/></center></body></html>", "text/html");

		/*	myEnvironment.sendTestResultEmail(htmlPart,
					myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
					myEnvironment.getJackTitle());*/
			driver.quit();

		}

	}
