
package SeleniumOutlookTestCookbook.tommyBahama.test;




import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;
import TommyBahamaOutletRepository.OutletAddressPage;
import TommyBahamaOutletRepository.OutletCSC;
import TommyBahamaOutletRepository.OutletConfirmationPage;
import TommyBahamaOutletRepository.OutletDeliveryMethodPage;
import TommyBahamaOutletRepository.OutletHMC;
import TommyBahamaOutletRepository.OutletHeader;
import TommyBahamaOutletRepository.OutletHmcLeftNav;
import TommyBahamaOutletRepository.OutletHomeDecor;

import TommyBahamaOutletRepository.OutletPaymentPage;
import TommyBahamaOutletRepository.OutletPaymentech;
import TommyBahamaOutletRepository.OutletPreviewPage;
import TommyBahamaOutletRepository.OutletProductDetailPage;
import TommyBahamaOutletRepository.OutletProductListingPage;
import TommyBahamaOutletRepository.OutletProducts;
import TommyBahamaOutletRepository.OutletSearchResultsPage;
import TommyBahamaOutletRepository.OutletShoppingCart;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;



@RunWith(JUnit4.class)
public class RestrictedOrderTest {

	private OutletWebdriverEnvironment myEnvironment;
	private OutletHeader myHeader;
	private OutletProductListingPage myPLP;
	private OutletProductDetailPage myPDP;
	private OutletSignInPage mySignInPage;

	private OutletShoppingCart myShoppingCart;
	private OutletAddressPage myAddressPage;
	private OutletPaymentPage myPaymentPage;
	private OutletDeliveryMethodPage myDeliveryMethod;
	private OutletPreviewPage myPreviewPage;
	private OutletConfirmationPage myConfirmationPage;
	private OutletCSC myCSC;
	private OutletSearchResultsPage mySearchResults;
	private OutletPaymentech myPaymentech;
	private String testName = "restrictedProduct";
	private OutletProducts restrictedProduct = new OutletProducts("TH31397", "4","RestrictedProduct");
	private String OrderNumber = "";
	private WebDriver driver;
	private OutletHmcLeftNav myHmcLeftNav = new OutletHmcLeftNav();
	private OutletHMC myHMC;
	
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	@Before
	public void openBrowser() {

		
	Platform currentPlatform = Platform.getCurrent();
		
	/*	switch (System.getenv("SELENIUM_BROWSER")) {

	case "SAFARI":

		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		break;

	case "FF":

		File fileToProfile = new File("./Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use
															// 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		break;

	case "CHROME":

		if (currentPlatform.is(Platform.WINDOWS))
			System.setProperty("webdriver.chrome.driver",
					"./chromedriver.exe");
		else {
			System.setProperty("webdriver.chrome.driver",
					"/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		break;
		
	case "IE": 		
				Assert.assertTrue(isSupportedPlatform());
			//	File file = new File(myEnvironment.getIeWebdriver());
				System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
				DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
				ieCap.setVersion("9");
				ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
				driver = new InternetExplorerDriver(ieCap);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getIeBrowser());
				break;
	default:
		
	System.out.println("Browser was not valid!");
				
	}*/
	
	if (System.getenv("SELENIUM_BROWSER").contains("FF")) {
		File fileToProfile = new File("./Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
	}	else if(System.getenv("SELENIUM_BROWSER").contains("SAFARI")) {
		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
	}
	else if (System.getenv("SELENIUM_BROWSER").contains("IE")) {
		Assert.assertTrue(isSupportedPlatform());
	//	File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
	}
	
	else {
		if(currentPlatform.is(Platform.WINDOWS))
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		else {
		//	File myChromeDriver = new File("./chromedriver");
		//	myChromeDriver.setExecutable(true);
		//System.setProperty("LD_LIBRARY_PATH", "/opt/google/chrome/lib /usr/local/bin/chromedriver");
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
	
	}
		
		myHeader = new OutletHeader(driver, myEnvironment);
		mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
		myPLP = new OutletProductListingPage(driver, myEnvironment);
		myPDP = new OutletProductDetailPage(driver, myEnvironment, myPLP,
				myHeader, testName);
		 myHMC = new OutletHMC(driver, myEnvironment, myHmcLeftNav, testName);
		myPaymentPage = new OutletPaymentPage(myEnvironment, testName);
		 mySearchResults = new OutletSearchResultsPage(myEnvironment, myHeader);
	
		myAddressPage = new OutletAddressPage(myEnvironment);
		myDeliveryMethod = new OutletDeliveryMethodPage();
		myPreviewPage = new OutletPreviewPage();
		myConfirmationPage = new OutletConfirmationPage();
		myShoppingCart = new OutletShoppingCart(driver, myEnvironment);
		
		String tempFile = "../seleniumhq";
		List<String> fns = new ArrayList<String>();
		fns.add(mySignInPage.getThePageName());
		fns.add(myPLP.getThePageName() + restrictedProduct.getName());
		fns.add(myPDP.getThePageName() + restrictedProduct.getProductId());
		fns.add(myPDP.getThePreviewPaneName() + restrictedProduct.getProductId());
		fns.add(myShoppingCart.getThePageName());
		fns.add(myAddressPage.getThePageName());
		fns.add(myDeliveryMethod.getThePageName());
		fns.add(myPaymentPage.getThePageName() + "Select");
		fns.add(myPaymentPage.getThePageName());
		fns.add(myPreviewPage.getThePageName());
		fns.add(myConfirmationPage.getThePageName());	
		fns.add("LogInPage");
		fns.add("CSCLandingPage");
		fns.add("SearchOrderFormWithResults");
		fns.add("OrderTab");
		fns.add("OrderTab2");
		myEnvironment.removeSS(fns, tempFile);

	}

	@Test
	public void RestrictedOrderOutletTest() throws InterruptedException,
			IOException {
		WebElement ce;
	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTheTestingEnvironment(System.getenv("SELENIUM_TEST_URL"));
		myEnvironment.setTestSubject(testName);
		
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		driver.manage().window().maximize();
		
		myHMC.createRestrictedProduct(restrictedProduct.getProductId(), "on", mySignInPage);
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		// Sign in to WS
		mySignInPage.signIn(myEnvironment.getTheEcommEmailTwo(),
				myEnvironment.getPassWrd());

		myEnvironment.waitForTitle("Official Site");

		// execute a search
	//	mySearchResults.executeSearch("Men");

		// add a product to the cart
		myPDP.selectProduct(myHeader.getTheByXPathHomeDecorTab(),	
				restrictedProduct.getProductId(), restrictedProduct.getQty(), restrictedProduct.getName());
		
	/*	ce = myEnvironment.waitForDynamicElement(By.xpath(myHeader.getTheByXPathSwimTab()));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(By.xpath(myLeftNav.getTheByXPath4To6Inseam()));
		ce.click();
		
		myEnvironment.waitForURL("Swim");
		ce = myEnvironment.waitForDynamicElement(By.xpath("//span/img"));
		ce.click();
		
		myPDP.addQuantityToBag(swimProduct.getQty());*/
		

		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myPDP.getThePreviewPaneName(), restrictedProduct.getProductId());
		
		// check that restriction is present. 
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myPDP.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myPDP.getTheByCssRestrictionText())));
	//	ce = myEnvironment.waitForDynamicElement(By.cssSelector("span.shipping_restrictions > a.tb_modal"));

		// continue to shopping bag
		ce = myEnvironment.waitForDynamicElement(By.linkText(myHeader
				.getTheByLinkCheckout()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForTitle("Shopping");
		
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));

		ce = myEnvironment.waitForDynamicElement(By.name(myShoppingCart
				.getTheByNameQtyUpdate()));

		Select clickThis = new Select(ce);
		clickThis.selectByValue("3");

		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myShoppingCart.getThePageName(), "");

		// continue to address page
		ce = myEnvironment.waitForDynamicElement(By.id(myShoppingCart
				.getTheByIdBottomContinueCheckout()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForURL("address");
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myAddressPage.getThePageName(), "");

		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myAddressPage.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));
		
		// continue to delivery options
		ce = myEnvironment.waitForDynamicElement(By.xpath(myAddressPage
				.getTheByIdTopContinueCheckoutBtn()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForURL("delivery-method");
		myEnvironment.waitForClickableElement(By.id(myDeliveryMethod
				.getTheByIdContinueCheckoutButton()));
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myDeliveryMethod.getThePageName(), "");
		
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));

		// continue to Select PaymentPage
		ce = myEnvironment.waitForDynamicElement(By.id(myDeliveryMethod
				.getTheByIdContinueCheckoutButton()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForURL("choose-payment-method");
		myEnvironment.waitForClickableElement(By.id(myPaymentPage
				.getTheByIdContinueCheckout()));
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myPaymentPage.getThePageName(), "Select");
		
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myAddressPage.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));

		// continue to PaymentPage
		ce = myEnvironment.waitForDynamicElement(By.id(myPaymentPage
				.getTheByIdContinueCheckout()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForURL("add-payment-method");
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myPaymentPage.getThePageName(), "");
		
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));

		// enter the cc verification number
		ce = myEnvironment.waitForDynamicElement(By.id(myPaymentPage
				.getTheByIdCCSecurityCode()));
		ce.sendKeys(myPaymentPage.getTheCode());

		// continue to Order Review paypage
		ce = myEnvironment.waitForDynamicElement(By.id(myPaymentPage
				.getTheSubmitPaymentBtn()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForURL("summary");
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myPreviewPage.getThePageName(), "");

		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myAddressPage.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));
		
		// submit order
		ce = myEnvironment.waitForDynamicElement(By.xpath(myPreviewPage
				.getTheByXpathSubmitButton()));
		ce.click();
		myEnvironment.waitForPageLoaded();
		myEnvironment.waitForDocumentReadyState();
		myEnvironment.waitForTitle("Confirmation");
		
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				myConfirmationPage.getThePageName(), "");
		
		OrderNumber = driver.getCurrentUrl();
		if (driver.getCurrentUrl().contains("dev"))
			OrderNumber = OrderNumber.substring(65);
		else
			OrderNumber = OrderNumber.substring(64);

		
		
		ce = myEnvironment.waitForDynamicElement(By.id(myConfirmationPage.getTheTotalTax()));
		String theTax = ce.getText();
		System.out.println("This is the tax: 	" + theTax);
		ce = myEnvironment.waitForDynamicElement(By.id(myConfirmationPage.getTheTotalAmount()));
		String theTotal = ce.getText();
		
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionLink())));
		Assert.assertTrue(myEnvironment.IsElementPresent(
				By.cssSelector(myShoppingCart.getTheByCssRestrictionText())));

	

		System.out.println("This is the total amount: 	" + theTotal);
		myCSC = new OutletCSC(driver, myEnvironment, "", theTax, OrderNumber,
				testName, myEnvironment.getBrowser());

		// check the order in Contact Center
		myCSC.checkOrderInCSC(theTotal, restrictedProduct.getName());

		// See that the auth happened in OG
		// remove the $ sign because money values in Paymentech do not
		// dispaly the dollar sign
		Double theDTotal = 0.0;
		theDTotal = myPaymentPage.pullPrice(theTotal);
		String newTotal = "";
		myPaymentech = new OutletPaymentech(driver, myEnvironment, OrderNumber,
				testName, myEnvironment.getBrowser());
		newTotal = myPaymentech.formatTotal(newTotal, theDTotal);
		Thread.sleep(myEnvironment.getThe_Special_Sleep());
		
		// turn restriction off for the product (this needs to be commented out to presently
		// view the order in CSC. If the product is not still restricted, it will display a null pointer
		myHMC.createRestrictedProduct(restrictedProduct.getProductId(), "off", mySignInPage);
		
		// This part is currently commented out because of the problem of
		// entering code for new log in location.
		
		// myPaymentech
		// .checkOrderInPayTech(newTotal);

		// Set flag that test passed.
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException, IOException {
	
		// fires if the test did not pass. Takes SS of last rendered page. 
		if(!myEnvironment.getTestPassed()) {
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				"TestFailed", "OnCurrentPage");
		}

		myEnvironment.setNetworkFile("../seleniumhq/" + this.testName);

		BufferedWriter bw = new BufferedWriter(new FileWriter(
				myEnvironment.getNetworkFile() + ".html"));

		bw.write("<html><body><center><h2>Outlet Single Division Order</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+ "<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This test is to see if a single division order is completed correctly on WS and in CSC.</h3> <table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ this.testName
				+ "</b></p></td></tr></table><br/>"
				+

				myEnvironment.addSSToFile(testName,
						mySignInPage.getThePageName(),
						"This is the Sign In page.")
				+

				myEnvironment.addSSToFile(testName, myPLP.getThePageName()
						+ restrictedProduct.getName(), "This is the Dinnerware PLP.")
				+

				myEnvironment.addSSToFile(testName, myPDP.getThePageName()
						+ restrictedProduct.getProductId(),
						"This is the Dinnerware PDP.")
				+

				myEnvironment.addSSToFile(testName,	myPDP.getThePreviewPaneName()
								+ restrictedProduct.getProductId(),
						"This is the Dinnerware PDP with Preview pane displaying added product.")
				+
				
					myEnvironment.addSSToFile(testName,	myShoppingCart.getThePageName(),
						"This is the Shopping Cart displaying added product with the reduced qty of 3.")
				+
				
					myEnvironment.addSSToFile(testName,	myAddressPage.getThePageName(),
						"This is the Address Page.")
				+
					myEnvironment.addSSToFile(testName,	myDeliveryMethod.getThePageName(),
						"This is the Delivery Method Page.")
				+
				
					myEnvironment.addSSToFile(testName,	myPaymentPage.getThePageName() + "Select",
						"This is the page to select the users payment type.")
				+
				
					myEnvironment.addSSToFile(testName,	myPaymentPage.getThePageName(),
						"This is the Payment Page.")
				+
				
					myEnvironment.addSSToFile(testName,	myPreviewPage.getThePageName(),
						"This is the Review Page to review the current order.")
				+
				
					myEnvironment.addSSToFile(testName,	myConfirmationPage.getThePageName(),
						"This is the Confirmation Page that " +
						"gives the user an order number to track their order.")
				+

				myEnvironment.addSSToFile(testName, "LogInPage",
						"This is the CSC login page.")
				+

				myEnvironment.addSSToFile(testName, "CSCLandingPage",
						"This is the CSC landing page.")
				+

				myEnvironment.addSSToFile(testName,
						"SearchOrderFormWithResults",
						"This is the search modal for orders.")
				+

				myEnvironment.addSSToFile(testName, "OrderTab",
						"This is the Order Detail page.")
				+

				myEnvironment.addSSToFile(testName, "OrderTab2",
						"This is the bottom of the Order Detail page.") +

				myEnvironment.getPageTestOutcome() +

				"</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Single Divison Order</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+ "<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This test is to see if a single division order is correct on WS and CSC.</h3> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "" + myEnvironment.getNetworkFile() + ".html"
						+ "<br/></center></body></html>", "text/html");

	/*	myEnvironment.sendTestResultEmail(htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle());*/
		driver.quit();

	}

}
