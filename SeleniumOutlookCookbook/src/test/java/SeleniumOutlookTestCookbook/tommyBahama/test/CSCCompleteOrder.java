package SeleniumOutlookTestCookbook.tommyBahama.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import TommyBahamaOutletRepository.OutletCSCMenu;
import TommyBahamaOutletRepository.OutletCSCCheckoutTab;
import TommyBahamaOutletRepository.OutletCSCCartTab;
import TommyBahamaOutletRepository.OutletCSCOrderTab;
import TommyBahamaOutletRepository.OutletCustomerTab;
import TommyBahamaOutletRepository.OutletSignInPage;
import TommyBahamaOutletRepository.OutletWebdriverEnvironment;
import TommyBahamaOutletRepository.WaitTool;


@RunWith(JUnit4.class)
public class CSCCompleteOrder {
	
	private OutletWebdriverEnvironment myEnvironment;
	
	// Sign In page for different sites
	private OutletSignInPage mySignInPage;
	
	// This is the left panel in the CSC 
	private OutletCSCMenu myCSCMenu;
	
	// This is the customer tab in the CSC. 
	private OutletCustomerTab myCustomerTab;
	
	//This is the order tab in the CSC.
	private OutletCSCCartTab myCartTab;
	
	//This is the checkout tab in the CSC
	private OutletCSCCheckoutTab myCheckoutTab;
	
	private OutletCSCOrderTab myOrderTab;
	
	private String testName = "IeCSCcompleteOrder";
	

	// The object which is how all interaction with web elements is made.
	private int ss = 0;

	// Main object in the test, driver object is per browser and is used for all
	// interactivity with web page elements.
	private static boolean isSupportedPlatform() {
		Platform currentPlatform = Platform.getCurrent();
		return Platform.MAC.is(currentPlatform)
				|| Platform.WINDOWS.is(currentPlatform);
	}

	private WebDriver driver;

	@Before
	public void openBrowser() {
	
	Platform currentPlatform = Platform.getCurrent();
		
	/*	switch (System.getenv("SELENIUM_BROWSER")) {

	case "SAFARI":

		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
		break;

	case "FF":

		File fileToProfile = new File("./Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use
															// 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
		break;

	case "CHROME":

		if (currentPlatform.is(Platform.WINDOWS))
			System.setProperty("webdriver.chrome.driver",
					"./chromedriver.exe");
		else {
			System.setProperty("webdriver.chrome.driver",
					"/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
		break;
		
	case "IE": 		
				Assert.assertTrue(isSupportedPlatform());
			//	File file = new File(myEnvironment.getIeWebdriver());
				System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
				DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
				ieCap.setVersion("9");
				ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
				driver = new InternetExplorerDriver(ieCap);
				myEnvironment = new OutletWebdriverEnvironment(driver);
				myEnvironment.setBrowser(myEnvironment.getIeBrowser());
				break;
	default:
		
	System.out.println("Browser was not valid!");
				
	}*/
	
	if (System.getenv("SELENIUM_BROWSER").contains("FF")) {
		File fileToProfile = new File("./Profile");
		FirefoxProfile p = new FirefoxProfile(fileToProfile);
		p.setPreference("javascript.enabled", true);
		p.setPreference("webdriver.load.strategy", "fast"); // can use 'fast'
		driver = new FirefoxDriver(p);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getFfBrowser());
	}	else if(System.getenv("SELENIUM_BROWSER").contains("SAFARI")) {
		DesiredCapabilities mc = DesiredCapabilities.safari();
		mc.setCapability("applicationCacheEnabled", true);
		driver = new SafariDriver(mc);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getSafariBrowser());
	}
	else if (System.getenv("SELENIUM_BROWSER").contains("IE")) {
		Assert.assertTrue(isSupportedPlatform());
	//	File file = new File(myEnvironment.getIeWebdriver());
		System.setProperty("webdriver.ie.driver", "./IEDriverServer.exe");
		DesiredCapabilities ieCap = DesiredCapabilities.internetExplorer();
		ieCap.setVersion("9");
		ieCap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCap.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, false);
		driver = new InternetExplorerDriver(ieCap);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getIeBrowser());
	}
	
	else {
		if(currentPlatform.is(Platform.WINDOWS))
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		else {
		//	File myChromeDriver = new File("./chromedriver");
		//	myChromeDriver.setExecutable(true);
		//System.setProperty("LD_LIBRARY_PATH", "/opt/google/chrome/lib /usr/local/bin/chromedriver");
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		myEnvironment = new OutletWebdriverEnvironment(driver);
		myEnvironment.setBrowser(myEnvironment.getChromeBrowser());
	
	}
		myCSCMenu = new OutletCSCMenu();
	
	
		myCustomerTab = new OutletCustomerTab(myEnvironment, testName);
		myCartTab = new OutletCSCCartTab(myEnvironment, testName);
		myCheckoutTab = new OutletCSCCheckoutTab(myEnvironment, testName);
		myOrderTab = new OutletCSCOrderTab(myEnvironment, testName);
		 mySignInPage = new OutletSignInPage(driver, myEnvironment, testName);
			String tempFile = "F:\\eCommerce\\Jack\\JavaTests\\" + myEnvironment.getBrowser() + "\\" + testName;
		List<String> fns = new ArrayList<String>();
		fns.add("LogInPage");
		fns.add("CSCLandingPage");
		fns.add("SearchCustomerForm");
		fns.add("SearchCustomerFormWithResults");
		fns.add("CustomerTab");
		fns.add("CartTab");
		fns.add("CartTabWithSearchResults");
		fns.add("CartWithDressesAdded");
		fns.add("CheckoutTab");
		fns.add("CheckoutTabWithPaymentMethod");
		fns.add("OrderTab");
		
		myEnvironment.removeSS(fns, tempFile);
	}

	@Test
	public void CSCcompleteOrderTest() throws InterruptedException, IOException {
		
		WebElement ce;
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println(System.getenv("SELENIUM_TEST_CSC_URL"));
		myEnvironment.setTheTestingEnvironment(System.getenv("SELENIUM_TEST_CSC_URL"));
		myEnvironment.setTestSubject(testName);
		
		driver.get(myEnvironment.getTheTestingEnvironment());
		driver.manage().window().maximize();
		
		mySignInPage.signInCSC(myEnvironment.getJackEmail(), myEnvironment.getPassWrd());
		
	
		
		ce = myEnvironment.waitForDynamicElement(By.linkText(myCSCMenu.getTheByLinkFindCustomerLink()));
		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "CSCLandingPage", "");
		
		ce.click();	
	
		ce = myEnvironment.waitForDynamicElement(By.xpath(myCSCMenu.getTheByXpathSearchByEmail()));
		ce = myEnvironment.waitForDynamicElement(By.xpath(myCSCMenu.getTheByXpathSearchByEmail()));
		
	    ce.sendKeys(myEnvironment.getTheEcommEmailOne());		

		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchCustomerForm", "");
		 
		if(!myEnvironment.getBrowser().contains("Chrome")) {
		String selectEnter = Keys.chord(Keys.ENTER);
		driver.findElement(By.xpath(myCSCMenu.getTheByXpathCreateButton())).sendKeys(selectEnter);
		}
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myCSCMenu.getTheByXpathSubmitSearchButton()));
		ce.click();		
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(myCSCMenu.getTheByXPathCustomerSelectButton()));
		
		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "SearchCustomerFormWithResults", "");
		
	
		ce.click();



	ce = WaitTool.waitForElement(driver, By.xpath(myCustomerTab.getTheByXpathPasswordResetButton()), 10);

	myEnvironment.waitForDocumentReadyState();
	ce = WaitTool.waitForElement(driver, By.xpath("//div[2]/span/table/tbody/tr[2]/td[2]"), 10);
	
	//ce = WaitTool.waitForElement(driver, By.xpath(myCustomerTab.getTheByXpathPasswordResetButton()), 10);
	
	Assert.assertTrue( WaitTool.waitForTextPresent(driver, By.xpath(myCustomerTab.getTheByXpathPasswordResetButton()), "Send Password Reset Link", 10));
	
		
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "CustomerTab", "");	
	

	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCustomerTab.getTheByXpathCustomerDefaultAddressDropdown()), 10);
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCustomerTab.getTheByXpathPersonalDetailsName()), 10);
	
	System.out.println(ce.getAttribute("value"));
	
	Assert.assertTrue(ce.getAttribute("value").contains("Jack West"));
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCartTab()));
	ce.click();
	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCartId()));
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCartId()));
	
	
	
	Assert.assertTrue(ce.getText().contains("Jack West"));
	
	
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "CartTab", "");	
	
	
	//Thread.sleep(10000);
	myEnvironment.IsElementPresent(By.xpath(myCartTab.getTheByXpathCartSearchInput()));
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCartSearchInput()));
	
	System.out.println(myEnvironment.getBrowser());

	if(!myEnvironment.getBrowser().contains("Safari")) {
	  ce.sendKeys("dresses");
	}else {
	
	 try {
		  ce.sendKeys("dresses");
        } catch (StaleElementReferenceException ex) {
         
        }

	}
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCartSearchButton()));
	
//	selectEnter = Keys.chord(Keys.ENTER);
//	driver.findElement(
//			By.xpath(myCartTab.getTheByXpathCartSearchInput())).sendKeys(selectEnter);

	ce.click();
	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCartTab.getTheByXpathCartSearchResultQtyDropdown()), 10);
	
	//ce = myEnvironment.waitForDynamicElement(
	//		By.xpath(myCartTab.getTheByXpathCartSearchResultQtyDropdown()));
	
	Select clickThis = new Select(ce);
	clickThis.selectByIndex(1);
	
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "CartTabWithSearchResults", "");	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCartAddtoBagButton()));
	
	ce.click();
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCartTab.getTheByXpathCheckoutButton()), 10);
	ce = WaitTool.waitForElement(driver, By.xpath(myCartTab.getTheByXpathCheckoutButton()), 10);
	
	
/*	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathQtyDropdown()));
	
	Select selectThis = new Select(ce);
	
	selectThis.selectByIndex(2);
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathUpdateCartButton()));
	
	ce.click();*/
	
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "CartWithDressesAdded", "");	

	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCartTab.getTheByXpathCheckoutButton()));
	ce.click();
	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathPlaceOrderButton()), 10);
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCheckoutTab.getTheByXpathNewAddressButton()));
	
	
	
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "CheckoutTab", "");	
	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathShipping()), 10);
	String checkOutShipping = ce.getText();
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathSubtotal()), 10);
	String checkOutSubTotal = ce.getText();
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathOrderTotal()), 10);
	String checkOutOrderTotal = ce.getText();
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathTotalTax()), 10);
	String checkOutOrderTax = ce.getText();

	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCheckoutTab.getTheByXpathCustomerSVN()));
	
	ce.click();


	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCheckoutTab.getTheByXpathCustomerSVN()));
	
	ce.sendKeys("111");
		
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCheckoutTab.getTheByXpathNewAddressButton()));
	
	WaitTool.dependableClick(driver, By.xpath(myCheckoutTab.getTheByXpathCheckoutUseThisCard()));	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathCheckoutUseThisCard()), 10);	

	ce.click();
	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myCheckoutTab.getTheByXpathRemovePaymentLink()));
	
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "CheckoutTabWithPaymentMethod", "");	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myCheckoutTab.getTheByXpathPlaceOrderButton()), 10);
	
	//ce = myEnvironment.waitForDynamicElement(
	//		By.xpath(myCheckoutTab.getTheByXpathPlaceOrderButton()));
	
	ce.click();
	
	
	ce = myEnvironment.waitForDynamicElement(
			By.xpath(myOrderTab.getTheByXpathCreateNewTicketButton()));
	
	myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
			testName, "OrderTab", "");	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myOrderTab.getTheByXpathShippingValue()), 10);
	Assert.assertTrue(ce.getText().contains(checkOutShipping));
	
	ce = WaitTool.waitForElement(driver, By.xpath(myOrderTab.getTheByXpathOrderSubTotal()), 10);
	Assert.assertTrue(ce.getText().contains(checkOutSubTotal));

	ce = WaitTool.waitForElement(driver, By.xpath(myOrderTab.getTheByXpathOrderTax()), 10);
	Assert.assertTrue(ce.getText().contains(checkOutOrderTax));
	
	
	ce = WaitTool.waitForElement(driver, By.xpath(myOrderTab.getTheByXpathOrderTotal()), 10);
	Assert.assertTrue(ce.getText().contains(checkOutOrderTotal));
	
	WebElement ce2;
	
	
	ce2 = WaitTool.waitForElement(driver, By.xpath(myOrderTab.getTheByXpathAuthAmount()), 10);
	
	Assert.assertTrue(ce.getText().contains(ce2.getText()));
	
	
		myEnvironment.setTestPassed(true);
	}

	@After
	public void quitDriver() throws MessagingException,
			IOException {

		// fires if the test did not pass. Takes SS of last rendered page. 
		if(!myEnvironment.getTestPassed()) {
		myEnvironment.TakeScreenShot(myEnvironment.getBrowser(), testName,
				"TestFailed", "OnCurrentPage");
		}

		myEnvironment.setNetworkFile("../seleniumhq/" + this.testName);

		BufferedWriter bw = new BufferedWriter(new FileWriter(
				myEnvironment.getNetworkFile() + ".html"));
		
		bw.write("<html><body><center><h2>Finding a Customer In Customer Service Cockpit!</h2>"
				+ myEnvironment.getEnvironmentUsedString()
				+ myEnvironment.getEnvironment()
				+ "<br/><br/>"
				+ myEnvironment.getBrowserUsedString()
				+"<b>"
				+ myEnvironment.getBrowser()
				+ "</b><br/><br/>"
				+ "<h3>This test is to see if a GSR can find a customer correctly in the Customer Service Cockpit.</h3> <table style= \"width:70%;\"><tr><td><p> "
				+ myEnvironment.getTestTextDescription()
				+ myEnvironment.getBrowser()
				+ "\\"
				+ this.testName
				+ "</b></p></td></tr></table><br/>"
				+
				
				myEnvironment.addSSToFile(testName, "LogInPage",
						"This is the Customer Service Cockpit login page.")
				+
				
				myEnvironment.addSSToFile(testName, "CSCLandingPage",
						"This is the Customer Service Cockpit landing page.")
				+
				
				myEnvironment.addSSToFile(testName, "SearchCustomerForm",
						"This is the Customer Service Cockpit Search For Customers form.")
				+
				
				myEnvironment.addSSToFile(testName, "SearchCustomerFormWithResults",
						"This is the Customer Service Cockpit Search For Customers form with results.")
				+		
				myEnvironment.addSSToFile(testName, "CustomerTab",
						"This is the top of the Customer Service Cockpit Customer Tab for the customer who was searched for.")
				+	

				myEnvironment.addSSToFile(testName, "CartTab",
						"This is the Cart Tab.")
				+	
				
					myEnvironment.addSSToFile(testName, "CartTabWithSearchResults",
						"This is the Cart Tab with a QTY of 4 selected for the first dress product displayed in the search results.")
				+	
					myEnvironment.addSSToFile(testName, "CartWithDressesAdded",
						"This is the Cart Tab with a 4 dresses added to the cart.")
				+
					myEnvironment.addSSToFile(testName, "CheckoutTab",
						"This is the Checkout Tab with 4 dresses added to the cart.")
				+
						myEnvironment.addSSToFile(testName, "CheckoutTabWithPaymentMethod",
						"This is the Checkout Tab with Payment Method displayed.")
				+
				
						myEnvironment.addSSToFile(testName, "OrderTab",
						"This is the Checkout Tab with Payment Method displayed.")
				+
		     	myEnvironment.getPageTestOutcome()
				+

				"</center></body></html>");
		bw.close();

		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(
				"<html><body><center><h2>Find Customer in Customer Service Cockpit</h2>"
						+ myEnvironment.getEnvironmentUsedString()
						+ myEnvironment.getEnvironment()
						+ "<br/><br/>"
						+ myEnvironment.getBrowserUsedString()
						+"<b>"
						+ myEnvironment.getBrowser()
						+ "</b><br/><br/>"
						+ "<h3>This test is to see if a Guest Service Representative can find a customer in the Customer Service Cockpit. </h3> "
						+ "<h3>Do to the size of the test emails. It is necessary to open the html file by clicking the link below. Next, send the html file as an email to view the results.</h3> "
						+ "file:///"
						+ myEnvironment.getNetworkFile()
						+ "\\" + this.testName + ".html"
						+ "<br/></center></body></html>", "text/html");
					
						
			
		List<String> fns = new ArrayList<String>();
		fns.add("LogInPage");
		fns.add("CSCLandingPage");
		fns.add("SearchCustomerForm");
		fns.add("SearchCustomerFormWithResults");
		fns.add("CustomerTab");
		fns.add("CartTab");
		fns.add("CartTabWithSearchResults");
		fns.add("CartWithDressesAdded");
		fns.add("CheckoutTab");
		fns.add("CheckoutTabWithPaymentMethod");
		fns.add("OrderTab");
		
		ss = ss - 5;
		
/*
		myEnvironment.sendTestResultEmail(fns, ss, htmlPart,
				myEnvironment.getTestSubject(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(), myEnvironment.getJackEmail(),
				myEnvironment.getJackTitle(),
				myEnvironment.getNetworkTestDirectory(),
				myEnvironment.getBrowser(), testName);*/
		driver.quit();

	}
	
}
