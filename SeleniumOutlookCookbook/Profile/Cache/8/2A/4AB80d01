<?xml version="1.0" encoding="utf-8" ?>
<searchresults total_results="5">
                    <addon id="13530">
  <name>AlertStopper</name>
  <type id="1">Extension</type>
  <guid>alertstopper@dorian.meric</guid>
  <slug>alertstopper</slug>
  <version>200.000</version>
  <status id="4">Fully Reviewed</status>
  <authors>
        <author id="4855674">
      <name>Dorian Meric</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/dorian-meric/?src=api</link>
    </author>      </authors>
  <summary>&#34;Google Chrome&#34;-like option for blocking repetitive alert(), confirm() and prompt() dialogs. Gets rid of spam dialogs and other infinite loops!

With AlertStopper, dissipate your JavaScript dialogs problems instantly!</summary>
  <description>Ever been spammed with lots of alert dialogs that require you to click &#34;OK&#34; many times? Or even worse, ever been locked into a while() loop producing these alerts?

Have you already resorted to killing the process to get out of it, pestering against the genius who had this awesome idea to make alert() boxes so easy to abuse and so well-made that is stops you from doing anything else?

Well then probably this extension is for you! Now you will have a Google Chrome-like option of disabling any additional alert() dialogs after the first one that appears!
So easy and simple, but it can save you a lot of frustration and time, so why hesitate?

And, as a world-exclusivity, you can now block confirm() dialogs as well! It will even remember which action you chose (OK or Cancel)!

And if any additional argument was necessary, with such a catchy name, it cannot disappoint you!


Bye-bye Rickrolls, or other very funny infinite loops that force you to kill the process! You will wonder how you ever did without it!</description><license>
    <name>Custom License</name>
    <url>https://addons.mozilla.org/en-US/firefox/addon/alertstopper/license/200.000</url>
  </license>  <icon size="64">https://addons.cdn.mozilla.net/img/uploads/addon_icons/13/13530-64.png?modified=1294666326</icon>
  <icon size="32">https://addons.cdn.mozilla.net/img/uploads/addon_icons/13/13530-32.png?modified=1294666326</icon>
  <compatible_applications><application>
      <name>Firefox</name>
      <application_id>1</application_id>
      <min_version>1.5</min_version>
      <max_version>4.0.*</max_version>
      <appID>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</appID>
    </application></compatible_applications>
  <all_compatible_os><os>ALL</os></all_compatible_os>
  <eula></eula><previews><preview position="0">
      <full type="image/png" width="200" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/35/35431.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="200" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/35/35431.png?src=api&amp;modified=1331247702
      </thumbnail></preview><preview position="1">
      <full type="image/png" width="700" height="525">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/35/35432.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="200" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/35/35432.png?src=api&amp;modified=1331247702
      </thumbnail></preview><preview position="1">
      <full type="image/png" width="700" height="525">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/35/35433.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="200" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/35/35433.png?src=api&amp;modified=1331247702
      </thumbnail></preview></previews><rating>4</rating>
  <learnmore>https://addons.mozilla.org/en-US/firefox/addon/alertstopper/?src=api</learnmore><install hash="sha256:d46bd73c4efd499f3a2c5a574360319e0c43ccbe2b93c466d6ffc94959244165"
    os="ALL"
    size="7168">https://addons.mozilla.org/firefox/downloads/file/107521/alertstopper-200.000-fx.xpi?src=api</install>
      <developer_comments>It&#39;s cool, at least my mother said so.</developer_comments>
    <reviews num="20">
      https://addons.mozilla.org/en-US/firefox/addon/alertstopper/reviews/?src=api
    </reviews>
    <total_downloads>17005</total_downloads>
    <weekly_downloads>45</weekly_downloads>
    <daily_users>558</daily_users>
    <created epoch="1249495037">
      2009-08-05T17:57:17Z
    </created>
    <last_updated epoch="1294666269">
      2011-01-10T13:31:09Z
    </last_updated>
    <homepage></homepage>
    <support></support>
    <featured>0</featured>
        <performance>
      </performance>
    </addon>

                  <addon id="722">
  <name>NoScript</name>
  <type id="1">Extension</type>
  <guid>{73a6fe31-595d-460b-a920-fcc0f8843232}</guid>
  <slug>noscript</slug>
  <version>2.6.5.9</version>
  <status id="4">Fully Reviewed</status>
  <authors>
        <author id="143">
      <name>Giorgio Maone</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/giorgio-maone/?src=api</link>
    </author>      </authors>
  <summary>The best security you can get in a web browser!
Allow active content to run only from sites you trust, and protect yourself against XSS and Clickjacking attacks.</summary>
  <description>Winner of the &#34;2006 PC World World Class Award&#34;, this tool provides extra protection to your Firefox. 
It allows JavaScript, Java and other executable content to run only from trusted domains of your choice, e.g. your home-banking web site, guarding your &#34;trust boundaries&#34; against cross-site scripting attacks (XSS), cross-zone DNS rebinding / CSRF attacks (router hacking), and Clickjacking attempts, thanks to its unique ClearClick technology. It also implements the DoNotTrack tracking opt-out proposal by default, see &lt;a href=&#34;http://outgoing.mozilla.org/v1/6fba316f40d885f24a2f91590f5efed1c18b6212/http%3A//snipurl.com/nsdntrack&#34; rel=&#34;nofollow&#34;&gt;http://snipurl.com/nsdntrack&lt;/a&gt; .
Such a preemptive approach  prevents exploitation of security vulnerabilities (known and even unknown!) with no loss of functionality... 
Experts do agree: Firefox is really safer with NoScript ;-)</description><license>
    <name>GNU General Public License, version 2.0</name>
    <url>http://www.gnu.org/licenses/gpl-2.0.html</url>
  </license>  <icon size="64">https://addons.cdn.mozilla.net/img/uploads/addon_icons/0/722-64.png?modified=1354116333</icon>
  <icon size="32">https://addons.cdn.mozilla.net/img/uploads/addon_icons/0/722-32.png?modified=1354116333</icon>
  <compatible_applications><application>
      <name>Firefox</name>
      <application_id>1</application_id>
      <min_version>3.0.9</min_version>
      <max_version>22.0</max_version>
      <appID>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</appID>
    </application><application>
      <name>SeaMonkey</name>
      <application_id>59</application_id>
      <min_version>2.0</min_version>
      <max_version>2.19</max_version>
      <appID>{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}</appID>
    </application><application>
      <name>Mobile</name>
      <application_id>60</application_id>
      <min_version>1.0</min_version>
      <max_version>2.0a1pre</max_version>
      <appID>{a23983c0-fd0e-11dc-95ff-0800200c9a66}</appID>
    </application></compatible_applications>
  <all_compatible_os><os>ALL</os></all_compatible_os>
  <eula></eula><previews><preview position="1">
      <full type="image/png" width="306" height="242">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/0/846.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="189" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/0/846.png?src=api&amp;modified=1331247702
      </thumbnail><caption>Enable JavaScript only where you&#39;re sure it is safe, with ONE CLICK!</caption></preview><preview position="1">
      <full type="image/png" width="545" height="485">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/1/1060.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="168" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/1/1060.png?src=api&amp;modified=1331247702
      </thumbnail><caption>NoScript can block any kind of potentially dangerous active content, not just JavaScript!</caption></preview><preview position="1">
      <full type="image/png" width="479" height="303">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/1/1062.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="200" height="126">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/1/1062.png?src=api&amp;modified=1331247702
      </thumbnail><caption>NoScript&#39;s ClearClick module is the first and only client-side protection against Clickjacking!</caption></preview><preview position="1">
      <full type="image/png" width="504" height="385">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52395.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="196" height="150">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52395.png?src=api&amp;modified=1331247702
      </thumbnail><caption>NoScript&#39;s powerful XSS filter has been the first and is still the most effective.</caption></preview></previews><rating>5</rating>
  <learnmore>https://addons.mozilla.org/en-US/firefox/addon/noscript/?src=api</learnmore><install hash="sha256:b7c3204573597447166ebb410cd552916e90bff855334681be47f4b9607fbe26"
    os="ALL"
    size="531916">https://addons.mozilla.org/firefox/downloads/file/197420/noscript-2.6.5.9-fx+sm+fn.xpi?src=api</install>
  <install hash="sha256:03c019120cfc6829daa94d57b9d3a339a0e107b6cc5024543bf1507bba2a68b2" os=""
             size="532099"
             status="Beta">
      https://addons.mozilla.org/firefox/downloads/file/199196/noscript-2.6.6rc4-fn+fx+sm.xpi?src=api
    </install>
    <contribution_data>
      <link>https://addons.mozilla.org/en-US/firefox/addon/noscript/contribute/?src=api</link>
                <suggested_amount currency="USD" amount="15.00">
            $15.00
          </suggested_amount>
            <meet_developers>
        https://addons.mozilla.org/en-US/firefox/addon/noscript/developers?src=api
      </meet_developers>
    </contribution_data>    <developer_comments>Since this is not a support forum, you won&#39;t receive any help here, while your question will likely disappear.

If you need support, please
1) read the FAQ: &lt;a href=&#34;http://outgoing.mozilla.org/v1/6021d06a853bee08a69de2754a562d506ec29736/http%3A//noscript.net/faq&#34; rel=&#34;nofollow&#34;&gt;http://noscript.net/faq&lt;/a&gt; 
2) visit this forum: &lt;a href=&#34;http://outgoing.mozilla.org/v1/ed565e649dbfb56dc05cb7739b83c2e98a4e4ce0/http%3A//noscript.net/forum&#34; rel=&#34;nofollow&#34;&gt;http://noscript.net/forum&lt;/a&gt; 
3) contact me directly: &lt;a href=&#34;http://outgoing.mozilla.org/v1/f4c007e6fd2929c3cb7bbc07a2d51f5108db9729/http%3A//maone.net&#34; rel=&#34;nofollow&#34;&gt;http://maone.net&lt;/a&gt;

You&#39;re welcome!

INSTALLATION ISSUES ARE COVERED BY FAQ 2.1, &lt;a href=&#34;http://outgoing.mozilla.org/v1/9d49115b0b3bcd37d07c0b937b31474077c28674/http%3A//noscript.net/faq%23qa2_1&#34; rel=&#34;nofollow&#34;&gt;http://noscript.net/faq#qa2_1&lt;/a&gt;

IMPORTANT: before asking or commenting about the *completely anonymous* request made to &lt;a href=&#34;http://outgoing.mozilla.org/v1/13162f3015e332a78ed6fa90b06926f2c0b835bf/https%3A//secure.informaction.com/ipecho&#34; rel=&#34;nofollow&#34;&gt;https://secure.informaction.com/ipecho&lt;/a&gt; on startup, or the those sent to your own WAN IP periodically, please read about the WAN IP protection feature at &lt;a href=&#34;http://outgoing.mozilla.org/v1/d996f1aa54557de4939051258206e2d0321a9d6e/http%3A//noscript.net/abe/wan&#34; rel=&#34;nofollow&#34;&gt;http://noscript.net/abe/wan&lt;/a&gt; (mentioned also in the release notes for 2.0 and in the privacy policy here). If you&#39;re in doubt about the full anonymity and total privacy of this feature, just check (or let someone you trust check) the source code here, the file is content/DNS.js. Thanks.</developer_comments>
    <reviews num="1200">
      https://addons.mozilla.org/en-US/firefox/addon/noscript/reviews/?src=api
    </reviews>
    <total_downloads>99271676</total_downloads>
    <weekly_downloads>108370</weekly_downloads>
    <daily_users>2031895</daily_users>
    <created epoch="1116006692">
      2005-05-13T17:51:32Z
    </created>
    <last_updated epoch="1364498151">
      2013-03-28T19:15:51Z
    </last_updated>
    <homepage>http://noscript.net</homepage>
    <support>http://noscript.net/forum</support>
    <featured>1</featured>
        <performance>
            <application name="fx" version="8.0.1">
                <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="479.26" average="475.42"
                  above_threshold="false" />
        </platform>        <platform name="WINNT" version="6.1">
          <result type="ts" baseline="548.47" average="552.74"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6 (rev4)">
          <result type="ts" baseline="637.58" average="642.58"
                  above_threshold="false" />
        </platform></application>      <application name="fx" version="6.0.2">
                <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="476.21" average="530.68"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6.2">
          <result type="ts" baseline="654.42" average="691.47"
                  above_threshold="false" />
        </platform>        <platform name="WINNT" version="6.1">
          <result type="ts" baseline="519.79" average="569.26"
                  above_threshold="false" />
        </platform></application>      <application name="fx" version="7.0.1">
                <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="479.68" average="534.95"
                  above_threshold="false" />
        </platform>        <platform name="WINNT" version="6.1">
          <result type="ts" baseline="547.16" average="599.47"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6.2 (rev3)">
          <result type="ts" baseline="667.37" average="699.47"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6.2">
          <result type="ts" baseline="671.16" average="698.05"
                  above_threshold="false" />
        </platform></application>      <application name="fx" version="8.0">
                <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="477.95" average="470.37"
                  above_threshold="false" />
        </platform>        <platform name="WINNT" version="6.1">
          <result type="ts" baseline="541.58" average="553.26"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6 (rev4)">
          <result type="ts" baseline="624.84" average="633.79"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6.2 (rev3)">
          <result type="ts" baseline="657.21" average="658.53"
                  above_threshold="false" />
        </platform></application></performance>
    </addon>

                  <addon id="1843">
  <name>Firebug</name>
  <type id="1">Extension</type>
  <guid>firebug@software.joehewitt.com</guid>
  <slug>firebug</slug>
  <version>1.11.2</version>
  <status id="4">Fully Reviewed</status>
  <authors>
        <author id="9265">
      <name>Joe Hewitt</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/joe-hewitt/?src=api</link>
    </author><author id="857086">
      <name>Jan Odvarko</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/jan-odvarko/?src=api</link>
    </author><author id="95117">
      <name>robcee</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/robcee/?src=api</link>
    </author><author id="4957771">
      <name>FirebugWorkingGroup</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/firebugworkinggroup/?src=api</link>
    </author>      </authors>
  <summary>Firebug integrates with Firefox to put a wealth of development tools at your fingertips while you browse. You can edit, debug, and monitor CSS, HTML, and JavaScript live in any web page...</summary>
  <description>&lt;a href=&#34;http://outgoing.mozilla.org/v1/fc4997ab3399c96f4fddde69c127a1a7ac4d471f70a11827f5965b04dd3d60a0/https%3A//twitter.com/%23%21/firebugnews&#34; rel=&#34;nofollow&#34;&gt;Follow Firebug news on Twitter&lt;/a&gt;

Compatibility table:

&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Firefox 3.6&lt;/strong&gt; with &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/?page=1#version-1.7.3&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.7.3&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 4.0&lt;/strong&gt; with &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/?page=1#version-1.7.3&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.7.3&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 5.0&lt;/strong&gt; with &lt;strong&gt;Firebug 1.8.2&lt;/strong&gt; (and also &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/?page=1#version-1.7.3&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.7.3&lt;/a&gt;)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 6.0&lt;/strong&gt; with &lt;strong&gt;Firebug 1.8.2&lt;/strong&gt; (and also &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 7.0&lt;/strong&gt; with &lt;strong&gt;Firebug 1.8.2&lt;/strong&gt; (and also &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 8.0&lt;/strong&gt; with &lt;strong&gt;Firebug 1.8.3&lt;/strong&gt; (and also &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 9.0&lt;/strong&gt; with &lt;strong&gt;Firebug 1.8.4&lt;/strong&gt; (and also &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 10.0&lt;/strong&gt; with &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 11.0&lt;/strong&gt; with &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 12.0&lt;/strong&gt; with &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 13.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.10&lt;/b&gt; (and also &lt;a href=&#34;https://addons.mozilla.org/en-US/firefox/addon/firebug/versions/&#34; rel=&#34;nofollow&#34;&gt;Firebug 1.9&lt;/a&gt;)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 14.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.10&lt;/b&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 15.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.10&lt;/b&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 16.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.10&lt;/b&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 17.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.11&lt;/b&gt; (and also Firebug 1.10)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 18.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.11&lt;/b&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 19.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.11&lt;/b&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Firefox 20.0&lt;/strong&gt; with &lt;b&gt;Firebug 1.11&lt;/b&gt;&lt;/li&gt;&lt;/ul&gt;
Firebug integrates with Firefox to put a wealth of development tools at your fingertips while you browse. You can edit, debug, and monitor CSS, HTML, and JavaScript live in any web page.

Visit the Firebug website for documentation and screen shots: &lt;a href=&#34;http://outgoing.mozilla.org/v1/835dbec6bd77fa1341bf5841171bc90e8a5d4069419f083a25b7f81f9fb254cb/http%3A//getfirebug.com&#34; rel=&#34;nofollow&#34;&gt;http://getfirebug.com&lt;/a&gt;
Ask questions on Firebug newsgroup: &lt;a href=&#34;http://outgoing.mozilla.org/v1/2398490bb3575c96d4cf9054e4f11236717aa6b9f353f5ada85dd8c74ed0dbbe/https%3A//groups.google.com/forum/%23%21forum/firebug&#34; rel=&#34;nofollow&#34;&gt;https://groups.google.com/forum/#!forum/firebug&lt;/a&gt;
Report an issue: &lt;a href=&#34;http://outgoing.mozilla.org/v1/82ba7e7d7600d78965967de98579334986246b827dcc5c8a0bcf0ba036ef62dd/http%3A//code.google.com/p/fbug/issues/list&#34; rel=&#34;nofollow&#34;&gt;http://code.google.com/p/fbug/issues/list&lt;/a&gt;

&lt;i&gt;Please note that bug reports or feature requests in comments on this page won&#39;t be tracked. Use the issue tracker or Firebug newsgroup instead thanks!&lt;/i&gt;</description><license>
    <name>BSD License</name>
    <url>http://www.opensource.org/licenses/bsd-license.php</url>
  </license>  <icon size="64">https://addons.cdn.mozilla.net/img/uploads/addon_icons/1/1843-64.png?modified=1356112905</icon>
  <icon size="32">https://addons.cdn.mozilla.net/img/uploads/addon_icons/1/1843-32.png?modified=1356112905</icon>
  <compatible_applications><application>
      <name>Firefox</name>
      <application_id>1</application_id>
      <min_version>17.0</min_version>
      <max_version>20.0a1</max_version>
      <appID>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</appID>
    </application></compatible_applications>
  <all_compatible_os><os>ALL</os></all_compatible_os>
  <eula></eula><previews><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52710.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52710.png?src=api&amp;modified=1295279900
      </thumbnail><caption>Command line and its auto-completion.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52711.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52711.png?src=api&amp;modified=1295279900
      </thumbnail><caption>Console panel with an example error logs.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52712.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52712.png?src=api&amp;modified=1295279900
      </thumbnail><caption>CSS panel with inline editor.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52713.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52713.png?src=api&amp;modified=1295279900
      </thumbnail><caption>DOM panel displays structure of the current page.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52714.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52714.png?src=api&amp;modified=1295279900
      </thumbnail><caption>HTML panel shows markup of the current page.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52715.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52715.png?src=api&amp;modified=1295279900
      </thumbnail><caption>Layout panel reveals layout of selected elements.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52716.png?src=api&amp;modified=1295279900
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52716.png?src=api&amp;modified=1295279900
      </thumbnail><caption>Net panel monitors HTTP communication.</caption></preview><preview position="1">
      <full type="" width="700" height="330">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/52/52717.png?src=api&amp;modified=1295279901
      </full>
      <thumbnail type="" width="200" height="94">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/52/52717.png?src=api&amp;modified=1295279901
      </thumbnail><caption>Script panel allows to explore and debug JavaScript on the current page.</caption></preview></previews><rating>5</rating>
  <learnmore>https://addons.mozilla.org/en-US/firefox/addon/firebug/?src=api</learnmore><install hash="sha256:21a2e4c8a92bd453a378d4cce444e6a8cc303a509e9fe97c91fd5a9ec273ff56"
    os="ALL"
    size="2163784">https://addons.mozilla.org/firefox/downloads/file/190938/firebug-1.11.2-fx.xpi?src=api</install>
  <install hash="sha256:5afdb93893cca90fb6b67d1e66ddfe265ed351445fc517accf401a2668e959e5" os=""
             size="2163787"
             status="Beta">
      https://addons.mozilla.org/firefox/downloads/file/194512/firebug-1.11.2b2-fx.xpi?src=api
    </install>
    <contribution_data>
      <link>https://addons.mozilla.org/en-US/firefox/addon/firebug/contribute/?src=api</link>
                <suggested_amount currency="USD" amount="10.00">
            $10.00
          </suggested_amount>
            <meet_developers>
        https://addons.mozilla.org/en-US/firefox/addon/firebug/developers?src=api
      </meet_developers>
    </contribution_data>    <developer_comments>If you have any problems, please read the Firebug FAQ.

&lt;a href=&#34;http://outgoing.mozilla.org/v1/66081b6c09e8af4862a003442794f2d2c2bc9096/http%3A//getfirebug.com/wiki/index.php/FAQ&#34;&gt;http://getfirebug.com/wiki/index.php/FAQ&lt;/a&gt;
</developer_comments>
    <reviews num="1478">
      https://addons.mozilla.org/en-US/firefox/addon/firebug/reviews/?src=api
    </reviews>
    <total_downloads>57152068</total_downloads>
    <weekly_downloads>231738</weekly_downloads>
    <daily_users>2956632</daily_users>
    <created epoch="1137054450">
      2006-01-12T08:27:30Z
    </created>
    <last_updated epoch="1361602253">
      2013-02-23T06:50:53Z
    </last_updated>
    <homepage>http://getfirebug.com/</homepage>
    <support>http://getfirebug.com</support>
    <featured>0</featured>
        <performance>
            <application name="fx" version="8.0.1">
                <platform name="WINNT" version="6.1">
          <result type="ts" baseline="548.47" average="541.42"
                  above_threshold="false" />
        </platform>        <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="479.26" average="471.95"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6 (rev4)">
          <result type="ts" baseline="637.58" average="633.89"
                  above_threshold="false" />
        </platform></application>      <application name="fx" version="6.0.2">
                <platform name="WINNT" version="6.1">
          <result type="ts" baseline="519.79" average="969.42"
                  above_threshold="true" />
        </platform>        <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="476.21" average="976.21"
                  above_threshold="true" />
        </platform>        <platform name="MacOSX" version="10.6.2">
          <result type="ts" baseline="654.42" average="1160.53"
                  above_threshold="true" />
        </platform></application>      <application name="fx" version="7.0.1">
                <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="479.68" average="964.11"
                  above_threshold="true" />
        </platform>        <platform name="WINNT" version="6.1">
          <result type="ts" baseline="547.16" average="1015.05"
                  above_threshold="true" />
        </platform>        <platform name="MacOSX" version="10.6.2 (rev3)">
          <result type="ts" baseline="667.37" average="1176.47"
                  above_threshold="true" />
        </platform>        <platform name="MacOSX" version="10.6.2">
          <result type="ts" baseline="671.16" average="1177.95"
                  above_threshold="true" />
        </platform></application>      <application name="fx" version="8.0">
                <platform name="WINNT" version="6.1">
          <result type="ts" baseline="541.58" average="538.47"
                  above_threshold="false" />
        </platform>        <platform name="Fedora" version="12 - Constantine">
          <result type="ts" baseline="477.95" average="481.26"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6 (rev4)">
          <result type="ts" baseline="624.84" average="640.79"
                  above_threshold="false" />
        </platform>        <platform name="MacOSX" version="10.6.2 (rev3)">
          <result type="ts" baseline="657.21" average="654.21"
                  above_threshold="false" />
        </platform></application></performance>
    </addon>

                  <addon id="4922">
  <name>YesScript</name>
  <type id="1">Extension</type>
  <guid>yesscript@userstyles.org</guid>
  <slug>yesscript</slug>
  <version>1.9</version>
  <status id="4">Fully Reviewed</status>
  <authors>
        <author id="9170">
      <name>Jason Barnabe</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/jason-barnabe/?src=api</link>
    </author>      </authors>
  <summary>A JavaScript blacklist</summary>
  <description>YesScript lets you make a blacklist of sites that aren&#39;t allowed to run JavaScript. Use YesScript on sites that annoy you or hog your system resources. One click to the icon in the status bar turns scripts on or off for the current site.

Unlike NoScript, YesScript does absolutely nothing to improve your security. I believe that Firefox is secure enough by default and that blocking all scripts by default is paranoia. YesScript strives to remove hassles from your browsing experience, rather than add them.</description><license>
    <name>GNU General Public License, version 3.0</name>
    <url>http://www.gnu.org/licenses/gpl-3.0.html</url>
  </license>  <icon size="64">https://addons.cdn.mozilla.net/img/uploads/addon_icons/4/4922-64.png?modified=1352316671</icon>
  <icon size="32">https://addons.cdn.mozilla.net/img/uploads/addon_icons/4/4922-32.png?modified=1352316671</icon>
  <compatible_applications><application>
      <name>SeaMonkey</name>
      <application_id>59</application_id>
      <min_version>2.0</min_version>
      <max_version>2.14a1</max_version>
      <appID>{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}</appID>
    </application><application>
      <name>Firefox</name>
      <application_id>1</application_id>
      <min_version>3.0</min_version>
      <max_version>20.*</max_version>
      <appID>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</appID>
    </application></compatible_applications>
  <all_compatible_os><os>ALL</os></all_compatible_os>
  <eula></eula><previews><preview position="1">
      <full type="image/png" width="411" height="229">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/19/19576.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png" width="200" height="111">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/19/19576.png?src=api&amp;modified=1331247702
      </thumbnail><caption>The YesScript Blacklist dialog</caption></preview></previews><rating>4</rating>
  <learnmore>https://addons.mozilla.org/en-US/firefox/addon/yesscript/?src=api</learnmore><install hash="sha256:93b0ae8a1367a1e1b737966415fe2972469327cb170cecc818a24482e285e8e8"
    os="ALL"
    size="52224">https://addons.mozilla.org/firefox/downloads/file/109886/yesscript-1.9-fx+sm.xpi?src=api</install>
  <contribution_data>
      <link>https://addons.mozilla.org/en-US/firefox/addon/yesscript/contribute/?src=api</link>
                <suggested_amount currency="USD" amount="2.00">
            $2.00
          </suggested_amount>
            <meet_developers>
        https://addons.mozilla.org/en-US/firefox/addon/yesscript/developers?src=api
      </meet_developers>
    </contribution_data>    <developer_comments></developer_comments>
    <reviews num="137">
      https://addons.mozilla.org/en-US/firefox/addon/yesscript/reviews/?src=api
    </reviews>
    <total_downloads>357288</total_downloads>
    <weekly_downloads>2083</weekly_downloads>
    <daily_users>29232</daily_users>
    <created epoch="1178343588">
      2007-05-05T05:39:48Z
    </created>
    <last_updated epoch="1296551741">
      2011-02-01T09:15:41Z
    </last_updated>
    <homepage></homepage>
    <support></support>
    <featured>0</featured>
        <performance>
      </performance>
    </addon>

                  <addon id="9449">
  <name>Microsoft .NET Framework Assistant</name>
  <type id="1">Extension</type>
  <guid>{20a82645-c095-46ed-80e3-08825760534b}</guid>
  <slug>microsoft-net-framework-assist</slug>
  <version>1.3.1</version>
  <status id="4">Fully Reviewed</status>
  <authors>
        <author id="3048315">
      <name>Microsoft Corporation</name>
      <link>https://addons.mozilla.org/en-US/firefox/user/microsoft-corporation/?src=api</link>
    </author>      </authors>
  <summary>Adds ClickOnce support and the ability to report installed .NET versions to the web server.</summary>
  <description>This extension requires version 3.5 SP1 (or greater) of the .NET Framework to be installed and is the only official extension published by Microsoft to support ClickOnce.</description><license>
    <name>Custom License</name>
    <url>https://addons.mozilla.org/en-US/firefox/addon/microsoft-net-framework-assist/license/1.3.1</url>
  </license>  <icon size="64"></icon>
  <icon size="32"></icon>
  <compatible_applications><application>
      <name>Firefox</name>
      <application_id>1</application_id>
      <min_version>3.0</min_version>
      <max_version>20.*</max_version>
      <appID>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</appID>
    </application></compatible_applications>
  <all_compatible_os><os>WINNT</os></all_compatible_os>
  <eula>Microsoft Public License (Ms-PL)

This license governs use of the accompanying software. If you use the software, you accept this license. If you do not accept the license, do not use the software.

1. Definitions

The terms “reproduce,” “reproduction,” “derivative works,” and “distribution” have the same meaning here as under U.S. copyright law.

A “contribution” is the original software, or any additions or changes to the software.

A “contributor” is any person that distributes its contribution under this license.

 “Licensed patents” are a contributor’s patent claims that read directly on its contribution.

2. Grant of Rights

(A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its contribution, prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.

(B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make, have made, use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the contribution in the software.

3. Conditions and Limitations

(A) No Trademark License- This license does not grant you rights to use any contributors’ name, logo, or trademarks.

(B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, your patent license from such contributor to the software ends automatically.

(C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, and attribution notices that are present in the software.

(D) If you distribute any portion of the software in source code form, you may do so only under this license by including a complete copy of this license with your distribution. If you distribute any portion of the software in compiled or object code form, you may only do so under a license that complies with this license.

(E) The software is licensed “as-is.” You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. You may have additional consumer rights under your local laws which this license cannot change. To the extent permitted under your local laws, the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-infringement.</eula><previews><preview position="0">
      <full type="image/png">
        https://addons.cdn.mozilla.net/img/uploads/previews/full/33/33820.png?src=api&amp;modified=1331247702
      </full>
      <thumbnail type="image/png">
        https://addons.cdn.mozilla.net/img/uploads/previews/thumbs/33/33820.png?src=api&amp;modified=1331247702
      </thumbnail><caption>The extension&#39;s listing in the Add-ons window and the available options.</caption></preview></previews><rating>2</rating>
  <learnmore>https://addons.mozilla.org/en-US/firefox/addon/microsoft-net-framework-assist/?src=api</learnmore><install hash="sha256:7817dc6511b13a5a951e1afeda2de9601894bc96a579dca17190524f0b70f649"
    os="WINNT"
    size="20480">https://addons.mozilla.org/firefox/downloads/file/127966/microsoft_net_framework_assistant-1.3.1-fx-windows.xpi?src=api</install>
      <developer_comments></developer_comments>
    <reviews num="113">
      https://addons.mozilla.org/en-US/firefox/addon/microsoft-net-framework-assist/reviews/?src=api
    </reviews>
    <total_downloads>1181119</total_downloads>
    <weekly_downloads>10719</weekly_downloads>
    <daily_users>1181119</daily_users>
    <created epoch="1225499999">
      2008-11-01T00:39:59Z
    </created>
    <last_updated epoch="1313020080">
      2011-08-10T23:48:00Z
    </last_updated>
    <homepage>http://www.microsoft.com</homepage>
    <support></support>
    <featured>0</featured>
        <performance>
      </performance>
    </addon>

          </searchresults>