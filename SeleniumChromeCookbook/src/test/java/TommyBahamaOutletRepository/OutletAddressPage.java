package TommyBahamaOutletRepository;

public class OutletAddressPage {
	private final String theFirstItem = "//div[2]/div[2]/a";
	private final String theAddressTitle = "Address Details";
	private final String theByLinkPrivacyLink = "Privacy Policy";
	private final String theURL = "https://";
	private final String theByIdTopContinueCheckoutBtn = "(//a[contains(text(),'Continue Checkout')])[2]";
	private final String theByNameSendEmailsCheckBox = "USER_ACCOUNT<>ATR_bol_send_email_updates";
	private String isChecked = "true";
	private String isNotChecked = "false";
	private final String theByIdNewEmail = "contact-email";
	private final String theByIdConfirmedEmail = "contact-email-confirm";
	private final String theByIdPhone1 = "bill_phone1";
	private final String theByIdPhone2 = "bill_phone2";
	private final String theByIdPhone3 = "bill_phone3";
	private String theFirstNumbers = "253";
	private String theSecondNumbers = "205";
	private String theThirdNumbers = "1570";
	private final String theByIdBillingCountry = "bill_country";
	private final String theUSCountry = "option[value='US']";
	private final String theByIdBillingFName = "billing-name-first";
	private String theFirstName = "Jack";
	private final String theByIdBillingMName = "billing-name-middle";
	private String theMiddleName = "B";
	private final String theByIdBillingLName = "billing-name-last";
	private String theLastName = "West";
	private String theByIdBillingAddress = "billing-address";
	private String theWAAddress = "123 N MAIN ST";
	private String theCanadaAddress = "6361 FALLSVIEW BLVD";
	private final String theByIdBillingCity = "billing-city";
	private String theWACity = "ELLENSBURG";
	private String theCanadaCity = "NIAGARA FALLS";
	private final String theByIdBillingState = "billing-state";

	// private String theWAState = "//select[@id='billing-state']/option[48]";
	private final String theWAState = "option[value='WA']";
	private String theCanadaState = "ON";
	private final String theByIdBillingZipCode = "billing-zip-code";
	private String theWAZipCode = "98926-3305";
	private String theCanadaZip = "L2E 3E8";

	private final String theShippingCountry = "ship_country";
	private String theSCountry = "United States";

	private final String theCanadaCountry = "option[value='CA']";
	private final String theShippingFName = "shipping-name-first";
	private String theSFirstName = "Jack";
	private final String theShippingMName = "shipping-name-middle";
	private String theSMiddleName = "C";
	private final String theShippingLName = "shipping-name-last";
	private String theSLastName = "West";
	private final String theShippingAddress = "shipping-address";
	private String theHawaiiAddress = "4331 KAUAI BEACH DR";
	private final String theShippingCity = "shipping-city";
	private String theHawaiiCity = "LIHUE";
	private final String theShippingState = "shipping-state";

	// private String theHawaiistate =
	// "//select[@id='shipping-state']/option[12]";
	private final String theHawaiistate = "#shipping-state > option[value='HI']";
	private final String theShippingZipCode = "shipping-zip-code";
	private String theHawaiiZipCode = "96766-8109";

	private final String theSavedBillingAddress = "id=billing-use-saved";
	private final String theByIdSameAddress = "same-as-billing";
	private final String theByIdAccountPaswrd = "account-password";
	private final String theByIdConfirmAccountPaswrd = "account-password-confirm";
	private final String theByIdCreateAccount = "account-create";
	private final String thePaswrd = "testing123";
	private final String theCaliforniaBillingAddress = "//option[@value='56297194365409585']";
	private final String theReturnToSBLink = "//a[contains(text(),'Return to Shopping Bag')]";

	private final String thePageName = "AddressPage";
	
	public String getTheFirstItem() {
		return theFirstItem;
	}

	public String getTheAddressTitle() {
		return theAddressTitle;
	}

	public String getTheByLinkPrivacyLink() {
		return theByLinkPrivacyLink;
	}

	public String getTheURL() {
		return theURL;
	}

	public String getTheByIdTopContinueCheckoutBtn() {
		return theByIdTopContinueCheckoutBtn;
	}

	public String getTheByNameSendEmailsCheckBox() {
		return theByNameSendEmailsCheckBox;
	}

	public String getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}

	public String getIsNotChecked() {
		return isNotChecked;
	}

	public void setIsNotChecked(String isNotChecked) {
		this.isNotChecked = isNotChecked;
	}

	public String getTheByIdNewEmail() {
		return theByIdNewEmail;
	}

	public String getTheByIdConfirmedEmail() {
		return theByIdConfirmedEmail;
	}

	public String getTheByIdPhone1() {
		return theByIdPhone1;
	}

	public String getTheByIdPhone2() {
		return theByIdPhone2;
	}

	public String getTheByIdPhone3() {
		return theByIdPhone3;
	}

	public String getTheFirstNumbers() {
		return theFirstNumbers;
	}

	public void setTheFirstNumbers(String theFirstNumbers) {
		this.theFirstNumbers = theFirstNumbers;
	}

	public String getTheSecondNumbers() {
		return theSecondNumbers;
	}

	public void setTheSecondNumbers(String theSecondNumbers) {
		this.theSecondNumbers = theSecondNumbers;
	}

	public String getTheThirdNumbers() {
		return theThirdNumbers;
	}

	public void setTheThirdNumbers(String theThirdNumbers) {
		this.theThirdNumbers = theThirdNumbers;
	}

	public String getTheByIdBillingCountry() {
		return theByIdBillingCountry;
	}

	public String getTheUSCountry() {
		return theUSCountry;
	}

	public String getTheByIdBillingFName() {
		return theByIdBillingFName;
	}

	public String getTheFirstName() {
		return theFirstName;
	}

	public void setTheFirstName(String theFirstName) {
		this.theFirstName = theFirstName;
	}

	public String getTheByIdBillingMName() {
		return theByIdBillingMName;
	}

	public String getTheMiddleName() {
		return theMiddleName;
	}

	public void setTheMiddleName(String theMiddleName) {
		this.theMiddleName = theMiddleName;
	}

	public String getTheByIdBillingLName() {
		return theByIdBillingLName;
	}

	public String getTheLastName() {
		return theLastName;
	}

	public void setTheLastName(String theLastName) {
		this.theLastName = theLastName;
	}

	public String getTheByIdBillingAddress() {
		return theByIdBillingAddress;
	}

	public void setTheByIdBillingAddress(String theByIdBillingAddress) {
		this.theByIdBillingAddress = theByIdBillingAddress;
	}

	public String getTheWAAddress() {
		return theWAAddress;
	}

	public void setTheWAAddress(String theWAAddress) {
		this.theWAAddress = theWAAddress;
	}

	public String getTheCanadaAddress() {
		return theCanadaAddress;
	}

	public void setTheCanadaAddress(String theCanadaAddress) {
		this.theCanadaAddress = theCanadaAddress;
	}

	public String getTheByIdBillingCity() {
		return theByIdBillingCity;
	}

	public String getTheWACity() {
		return theWACity;
	}

	public void setTheWACity(String theWACity) {
		this.theWACity = theWACity;
	}

	public String getTheCanadaCity() {
		return theCanadaCity;
	}

	public void setTheCanadaCity(String theCanadaCity) {
		this.theCanadaCity = theCanadaCity;
	}

	public String getTheByIdBillingState() {
		return theByIdBillingState;
	}

	public String getTheWAState() {
		return theWAState;
	}

	public String getTheCanadaState() {
		return theCanadaState;
	}

	public void setTheCanadaState(String theCanadaState) {
		this.theCanadaState = theCanadaState;
	}

	public String getTheByIdBillingZipCode() {
		return theByIdBillingZipCode;
	}

	public String getTheWAZipCode() {
		return theWAZipCode;
	}

	public void setTheWAZipCode(String theWAZipCode) {
		this.theWAZipCode = theWAZipCode;
	}

	public String getTheCanadaZip() {
		return theCanadaZip;
	}

	public void setTheCanadaZip(String theCanadaZip) {
		this.theCanadaZip = theCanadaZip;
	}

	public String getTheShippingCountry() {
		return theShippingCountry;
	}

	public String getTheSCountry() {
		return theSCountry;
	}

	public void setTheSCountry(String theSCountry) {
		this.theSCountry = theSCountry;
	}

	public String getTheCanadaCountry() {
		return theCanadaCountry;
	}

	public String getTheShippingFName() {
		return theShippingFName;
	}

	public String getTheSFirstName() {
		return theSFirstName;
	}

	public void setTheSFirstName(String theSFirstName) {
		this.theSFirstName = theSFirstName;
	}

	public String getTheShippingMName() {
		return theShippingMName;
	}

	public String getTheSMiddleName() {
		return theSMiddleName;
	}

	public void setTheSMiddleName(String theSMiddleName) {
		this.theSMiddleName = theSMiddleName;
	}

	public String getTheShippingLName() {
		return theShippingLName;
	}

	public String getTheSLastName() {
		return theSLastName;
	}

	public void setTheSLastName(String theSLastName) {
		this.theSLastName = theSLastName;
	}

	public String getTheShippingAddress() {
		return theShippingAddress;
	}

	public String getTheHawaiiAddress() {
		return theHawaiiAddress;
	}

	public void setTheHawaiiAddress(String theHawaiiAddress) {
		this.theHawaiiAddress = theHawaiiAddress;
	}

	public String getTheShippingCity() {
		return theShippingCity;
	}

	public String getTheHawaiiCity() {
		return theHawaiiCity;
	}

	public void setTheHawaiiCity(String theHawaiiCity) {
		this.theHawaiiCity = theHawaiiCity;
	}

	public String getTheShippingState() {
		return theShippingState;
	}

	public String getTheHawaiistate() {
		return theHawaiistate;
	}

	public String getTheShippingZipCode() {
		return theShippingZipCode;
	}

	public String getTheHawaiiZipCode() {
		return theHawaiiZipCode;
	}

	public void setTheHawaiiZipCode(String theHawaiiZipCode) {
		this.theHawaiiZipCode = theHawaiiZipCode;
	}

	public String getTheSavedBillingAddress() {
		return theSavedBillingAddress;
	}

	public String getTheByIdSameAddress() {
		return theByIdSameAddress;
	}

	public String getTheByIdAccountPaswrd() {
		return theByIdAccountPaswrd;
	}

	public String getTheByIdConfirmAccountPaswrd() {
		return theByIdConfirmAccountPaswrd;
	}

	public String getTheByIdCreateAccount() {
		return theByIdCreateAccount;
	}

	public String getThePaswrd() {
		return thePaswrd;
	}

	public String getTheCaliforniaBillingAddress() {
		return theCaliforniaBillingAddress;
	}

	public String getTheReturnToSBLink() {
		return theReturnToSBLink;
	}

	public String getThePageName() {
		return thePageName;
	}

}