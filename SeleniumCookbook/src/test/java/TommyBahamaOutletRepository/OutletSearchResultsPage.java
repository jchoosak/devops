package TommyBahamaOutletRepository;

public class OutletSearchResultsPage {
	
	
    private final String theByXpathBlackCheckBox = "(//input[@type='checkbox'])[208]";
    private final String theByXpathSecondProduct= "  //div[@id='content']/div[4]/div[4]/div/div[2]/div/div/a/span/img";


    public OutletSearchResultsPage()
	{
		
	}
    
    
	public String getTheByXpathBlackCheckBox() {
		return theByXpathBlackCheckBox;
	}


	public String getTheByXpathSecondProduct() {
		return theByXpathSecondProduct;
	}
	

}
