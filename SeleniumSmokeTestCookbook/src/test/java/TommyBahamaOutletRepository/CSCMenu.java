package TommyBahamaOutletRepository;

public class CSCMenu {
	
	private final String theByLinkFindCustomerLink = "Find Customer";
	private final String theByLinkNewCustomerLink = "New Customer";
	private final String theByLinkFindOrderLink = "Find Order";
	private final String thePageName = "HomePage";
	private final String theByCSSCustomerName = "//div[2]/table/tbody/tr/td[3]/div/input";
	
	
	private final String theByXpathCustomerEmail = "/html/body/div[4]/div[3]/div/div/div/div/div/div/div/div[2]/div/div/div/input[2]";
	private final String theByXpathCustomerName = "//div/input";
	private final String theByXpathNewCustomerName = "//div[2]/table/tbody/tr/td[3]/div/input";
	private final String theByXpathCreateButton = "//div[3]/span/table/tbody/tr[2]/td[2]";
	private final String theByXPathCustomerSelectButton = "//td[4]/div/span/table/tbody/tr[2]/td[2]";

	private final String theByIdFindCustomerLink = "z_dj_n2";
    private final String theByXpathSearchByEmail = "//input[2]";
    private final String theByXpathSubmitCustomerSearch = "//span[3]/table/tbody/tr[2]/td[2]";
    private final String theByXpathSubmitSearchButton = "//span[3]/table/tbody/tr/td[2]";
    private final String theByXpathSubmitOrderSearchButton = "//span[2]/table/tbody/tr[2]/td[2]";
    private final String theByXPathSearchSelectButton = "//td[5]/div/span/table/tbody/tr[2]/td[2]";
  //td[5]/div/span/table/tbody/tr[2]/td[2]

	public String getThePageName() {
		return thePageName;
	}

	public String getTheByLinkFindCustomerLink() {
		return theByLinkFindCustomerLink;
	}

	public String getTheByLinkNewCustomerLink() {
		return theByLinkNewCustomerLink;
	}

	public String getTheByCSSCustomerName() {
		return theByCSSCustomerName;
	}

	public String getTheByXpathCustomerEmail() {
		return theByXpathCustomerEmail;
	}

	public String getTheByXpathCreateButton() {
		return theByXpathCreateButton;
	}

	public String getTheByIdFindCustomerLink() {
		return theByIdFindCustomerLink;
	}

	public String getTheByXPathSearchSelectButton() {
		return theByXPathSearchSelectButton;
	}


	public String getTheByXpathSearchByEmail() {
		return theByXpathSearchByEmail;
	}

	public String getTheByXpathSubmitSearchButton() {
		return theByXpathSubmitSearchButton;
	}

	public String getTheByLinkFindOrderLink() {
		return theByLinkFindOrderLink;
	}

	public String getTheByXpathCustomerName() {
		return theByXpathCustomerName;
	}

	public String getTheByXpathSubmitOrderSearchButton() {
		return theByXpathSubmitOrderSearchButton;
	}

	public String getTheByXpathNewCustomerName() {
		return theByXpathNewCustomerName;
	}

	public String getTheByXPathCustomerSelectButton() {
		return theByXPathCustomerSelectButton;
	}

	public String getTheByXpathSubmitCustomerSearch() {
		return theByXpathSubmitCustomerSearch;
	}


}
