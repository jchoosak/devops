package TommyBahamaOutletRepository;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class OutletShoppingCart {
	private final String theByLinkContinueShopping = "Continue Shopping";
	private final String theByCSSContinueShopping = "a.continue-shopping-btn";
	private final String theMakeThisAGift = "//table[@id='shopping-table']/tbody/tr[3]/td[2]/span[2]/a";
	private final String theRemoveLink = "//a[contains(text(),'Remove')]";
	private final String theThirdRemoveLink = "(//a[contains(text(),'Remove')])[3]";
	private final String theFourthRemoveLink = "(//a[contains(text(),'Remove')])[4]";
	private final String theFifthRemoveLink = "(//a[contains(text(),'Remove')])[5]";
	private final String theOrderShipLink = "//div[@id='subtotal']/table/tbody/tr[2]/td/a";
	private final String theContinueCheckout = "//button[@id='checkoutButtonTop']";
	private final String theByIdBottomContinueCheckout = "checkoutButtonBottom";
	private final String theBottomCheckoutBtn = "(//div[@id='option-buttons']/a)[3]";
	private final String theItemColumn = "//table[@id='shopping-table']/tbody/tr/td";
	private final String theColorColumn = "//table[@id='shopping-table']/tbody/tr/td[2]";
	private final String theSizeColumn = "//table[@id='shopping-table']/tbody/tr/td[3]";
	private final String thePriceColumn = "//table[@id='shopping-table']/tbody/tr/td[4]";
	private final String theQtyColumn = "//table[@id='shopping-table']/tbody/tr/td[5]";
	private final String theSubTotalColumn = "//table[@id='shopping-table']/tbody/tr/td[6]";
	private final String theFirstImage = "//table[@id='shopping-table']/tbody/tr[2]/td/a/img";
	private final String theSecondImage = "xpath=(//img[@id=''])[5]";
	private final String theThirdImage = "xpath=(//img[@id=''])[6]";
	private final String theFirstItemDetails = "//table[@id='shopping-table']/tbody/tr[2]/td[2]/span";
	private final String theSecondItemDetails = "//table[@id='shopping-table']/tbody/tr[3]/td[2]/span";
	private final String theThirdItemDetails = "//table[@id='shopping-table']/tbody/tr[4]/td[2]/span";
	private final String theFirstRemoveLink = "//a[contains(text(),'Remove')]";
	private final String theGiftServices = "//table[@id='shopping-table']/tbody/tr[2]/td[2]/span[2]/a";
	private final String theUpdateQty = "//input[@id='update_line_quantities_btn']";
	private final String theUpdateSelection2 = "//input[15]";
	private final String theByNameQtyUpdate = "quantity";
	private final String theFirstGiftDetails = "//table[@id='shopping-table']/tbody/tr[2]/td[2]/span[2]/div";
	private final String theSecondGiftDetails = "//table[@id='shopping-table']/tbody/tr[3]/td[2]/span[2]/div";
	private final String theEditGiftServices = "//a[contains(text(),'Edit Gift Services')]";
	private final String theEditNoteServices = "//a[contains(text(),'Edit Note')]";
	private final String theVerifiedMessage = "//textarea[@id='For all the hard work you have done for Tommy Bahama!']";
	private final String theFirstItemPrice = "//table[@id='shopping-table']/tbody/tr[2]/td[7]";
	private final String theSecondItemPrice = "//table[@id='shopping-table']/tbody/tr[3]/td[7]";
	private final String theShippingItemPrice = "//div[@id='subtotal']/table/tbody/tr[3]/td[2]";
	private final String theGiftItemPrice = "//div[@id='subtotal']/table/tbody/tr[2]/td[2]";
	private final String theTotalItemPrice = "css=tr.last > td.right-col";
	private final String theShippingLink = "link=exact:When will my order ship?";
	// Could be part of its own Obj Repository
	private final String theShippingModal = "TB_iframeContent";
	private final String theCloseModal = "relative=up";
	private final String theShippingModalUSLink = "//a[@id='aUS']";
	private final String theShippingModalCALink = "//a[@id='aCanada']";
	private final String theShippingModalIntl = "//a[@id='aIntl']";
	private final String theShippingModalIntlFAQ = "//div[@id='divTabs']/a[5]";
	private final String theShippingModalFAQ = "//a[@id='aFAQ']";
	private final String theShippingModalText = "Standard Ground Shipping - $8";
	private final String theShippingModalX = "css=img[alt='close']";
	private final String theTaxQuestionMark = "link=exact:(?)";
	private final String theTaxModalX = "//a[@id='TB_closeWindowButton']/img";
	private final String theTaxModalText = "Applicable sales tax will be added on all orders based on your shipping address. The amount of sales tax is estimated until the order has shipped and shipping confirmation has been sent. We do not charge sales tax on the purchase of Gift Cards; however, items paid for with Gift Cards will be taxed.";
	private final String theGuaranteeText = "We promise that our quality, service, communication and your overall experience will exceed expectations. Guaranteed.";
	private final String theSuggestionBox = "//div[@id='suggestion-box']/h3";
	private final String theFirstSuggestionLink = "//ul[@id='suggestions']/li/a[2]";
	private final String theSecondSuggestionLink = "//ul[@id='suggestions']/li[2]/a[2]";
	private final String theThridSuggestionLink = "//ul[@id='suggestions']/li[3]/a[2]";
	private final String theFirstSuggestionThumbnail = "css=#suggestions > li > a > img";
	private final String theSecondSuggestionThumbnail = "//ul[@id='suggestions']/li[2]/a/img";
	private final String theThridSuggestionThumbnail = "//ul[@id='suggestions']/li[3]/a/img";
	private final String theSearchInput = "//input[@id='inputSearchFor']";
	private final String theSearchBtn = "//div[@id='divNavSearch']/form/div/a/img";
	private final String theRefineSearch = "//input[@id='refine_search']";
	private final String theSearchInputValue = "Polo Shirts";
	private final String theEmptyShoppingBagText = "If you have an account and are signed in, your items remain in the Shopping Bag for 14 days.";
	private final String theUrlType = "http://";
	// private final String theFirstGiftLink = "Make This a Gift";
	private final String theFirstGiftLink = "//a[contains(text(),'Make This a Gift')]";
	private final String theSecondGiftLink = "(//a[contains(text(),'Make This a Gift')])[2]";
	private final String theThirdGiftLink = "(//a[contains(text(),'Make This a Gift')])[3]";
	private final String theFourthGiftLink = "(//a[contains(text(),'Make This a Gift')])[4]";
	private final String theByCSSOversizedFreightMessage = "div.site_message";
	private final String theOversizedFreightMessageText = "- Your order contains an Oversized Item(s) that can only be shipped via Ground Shipping. If you need items from your order to be shipped faster, please remove those items from this order and place a separate order. For further assistance, please contact Guest Services at 1-866-986-8282.";
	private final String theByCSSCanadaError = "div.site_error";
	private final String theTraditionalGiftCardCanadaErrorText = "Traditional Gift Cards cannot be shipped to Canada. However, Virtual Gift Cards can be sent to any email address and can be redeemed at TommyBahama.com and the order can be shipped to Canada. Please remove the Gift Card from your Shopping Bag to continue.";
	private final String theCustomsGiftServiceErrorText = "Due to customs restrictions, Gift Services are not available for international orders. Please use the 'Edit Gift Services' link to remove gift-wrap options. If you have questions about Gift Wrap restrictions, please call Guest Services at 1-866-986-8282 or 00+1.206.905.2808.";
	private final String theShippingRestrictionText = "We're sorry but the item(s) highlighted below has a Shipping Restriction. Please update or remove the item from your Shopping Bag to continue. If you have questions about shipping restrictions, please call Guest Services at 1-866-986-8282 or 00+1.206.905.2808.";
	private final String theGiftServicesErrorText = "Gift Services are not available for international orders.";
	private final String theShippingRemovalText = "This item cannot be shipped to your address. Please remove it from your bag.";
	private final String theOversizedShippingText = "Your order contains an Oversized Item(s) that can only be shipped via Ground Shipping";
	private final String theHawaiiShippingText = "Ground shipping is not available to Hawaii or Alaska.";
	private final String theLineItemShippingRestrictionText = "Shipping restrictions apply";
	private final String theDetailsLinkText = "DETAILS";
	// rivate String theFirstShippingRestriction =
	// "//table[@id='shopping-table']/tbody/tr[4]/td[2]/span[2]/div[2]";
	private final String theFirstShippingRestriction = "//table[@id='shopping-table']/tbody/tr[4]/td[2]/span[2]/div[2]";
	private final String theSecondShippingRestriction = "//table[@id='shopping-table']/tbody/tr[5]/td[2]/span[2]/div[2]";
	private final String theThirdShippingRestriction = "//table[@id='shopping-table']/tbody/tr[6]/td[2]/span[2]/div[2]";

	private final String theShippingRestriction = "//div[@id='divShippingRestrictions']";
	private final String theFifthDetailsShippingRestriction = "(//div[@id='divShippingRestrictions'])[5]";
	private final String theFourthDetailsShippingRestriction = "(//div[@id='divShippingRestrictions'])[4]";
	private final String theFirstRowGiftServicesError = "//table[@id='shopping-table']/tbody/tr[2]/td[2]/div";
	private final String theSecondRowGiftServivesError = "//table[@id='shopping-table']/tbody/tr[3]/td[2]/div";
	private final String theLastRowGiftServicesError = "//table[@id='shopping-table']/tbody/tr[6]/td[2]/div";
	private final String theThirdRowShippingRemovalError = "//table[@id='shopping-table']/tbody/tr[4]/td[2]/div";
	private final String theFourthRowShippingRemovalError = "//table[@id='shopping-table']/tbody/tr[5]/td[2]/div";
	private final String theFifthRowShippingRemovalError = "//table[@id='shopping-table']/tbody/tr[6]/td[2]/div";
	private final String theSixthRowShippingRemovalError = "//table[@id='shopping-table']/tbody/tr[7]/td[2]/div";
	private final String theSeventhRowShippingRemovalError = "//table[@id='shopping-table']/tbody/tr[8]/td[2]/div";
	private final String theByXPathFlipSideMessage = "//table[@id='shopping-table']/tbody/tr[5]/td[2]/span";
	private final String theByXPath51FlipSideMessage = "//table[@id='shopping-table']/tbody/tr[6]/td[2]/span";

	private final String theByCSSFlipSideImage = "img[alt=\"PGC880001\"]";
	
	private final String thePageName = "ShoppingBag";
	private final String theCandaErrorColor ="rgb(182, 72, 59)";
	private final String theIECanadaErrorColor = "rgba(182, 72, 59, 1)";
	private final String theShippingErrorColor = "rgb(168, 49, 39)";
	private final String theChromeShippingErrorColor = "rgba(168, 49, 39, 1)";
	
	// this is the item in a shopping bag
	private final String item = "ITEM";
	// this is the color of an item in the shopping bag
	private final String color = "COLOR";
	// size of an item in the shopping bag
	private final String size = "SIZE";
	// price of an item in the shopping bag
	private final String price = "PRICE";
	// qty of an item in the shopping bag or PDP
	private final String qty = "QTY";
	// sub total of order
		private final String subtotal = "SUBTOTAL";

	String FirstPrice = "";
	String SecondPrice = "";
	String TotalPrice = "";
	String ShippingPrice = "";
	Double TheFirstPrice = 0.00;
	Double TheSecondPrice = 0.00;
	Double TheTotalPrice = 0.00;
	Double TheShippingPrice = 0.00;
	
	WebDriver driver;
	OutletWebdriverEnvironment myEnvironment;
	
	public OutletShoppingCart(WebDriver theDriver, OutletWebdriverEnvironment theEnvironment) 
	{
		driver = theDriver;
		myEnvironment = theEnvironment;
	}

	public String getTheByLinkContinueShopping() {
		return theByLinkContinueShopping;
	}

	public String getTheByCSSContinueShopping() {
		return theByCSSContinueShopping;
	}

	public String getTheMakeThisAGift() {
		return theMakeThisAGift;
	}

	public String getTheRemoveLink() {
		return theRemoveLink;
	}

	public String getTheThirdRemoveLink() {
		return theThirdRemoveLink;
	}

	public String getTheFourthRemoveLink() {
		return theFourthRemoveLink;
	}

	public String getTheFifthRemoveLink() {
		return theFifthRemoveLink;
	}

	public String getTheOrderShipLink() {
		return theOrderShipLink;
	}

	public String getTheContinueCheckout() {
		return theContinueCheckout;
	}

	public String getTheBottomCheckoutBtn() {
		return theBottomCheckoutBtn;
	}

	public String getTheItemColumn() {
		return theItemColumn;
	}

	public String getTheColorColumn() {
		return theColorColumn;
	}

	public String getTheSizeColumn() {
		return theSizeColumn;
	}

	public String getThePriceColumn() {
		return thePriceColumn;
	}

	public String getTheQtyColumn() {
		return theQtyColumn;
	}

	public String getTheSubTotalColumn() {
		return theSubTotalColumn;
	}

	public String getTheFirstImage() {
		return theFirstImage;
	}

	public String getTheSecondImage() {
		return theSecondImage;
	}

	public String getTheThirdImage() {
		return theThirdImage;
	}

	public String getTheFirstItemDetails() {
		return theFirstItemDetails;
	}

	public String getTheSecondItemDetails() {
		return theSecondItemDetails;
	}

	public String getTheThirdItemDetails() {
		return theThirdItemDetails;
	}

	public String getTheFirstRemoveLink() {
		return theFirstRemoveLink;
	}

	public String getTheGiftServices() {
		return theGiftServices;
	}

	public String getTheUpdateQty() {
		return theUpdateQty;
	}

	public String getTheUpdateSelection2() {
		return theUpdateSelection2;
	}

	public String getTheByNameQtyUpdate() {
		return theByNameQtyUpdate;
	}

	public String getTheFirstGiftDetails() {
		return theFirstGiftDetails;
	}

	public String getTheSecondGiftDetails() {
		return theSecondGiftDetails;
	}

	public String getTheEditGiftServices() {
		return theEditGiftServices;
	}

	public String getTheEditNoteServices() {
		return theEditNoteServices;
	}

	public String getTheVerifiedMessage() {
		return theVerifiedMessage;
	}

	public String getTheFirstItemPrice() {
		return theFirstItemPrice;
	}

	public String getTheSecondItemPrice() {
		return theSecondItemPrice;
	}

	public String getTheShippingItemPrice() {
		return theShippingItemPrice;
	}

	public String getTheGiftItemPrice() {
		return theGiftItemPrice;
	}

	public String getTheTotalItemPrice() {
		return theTotalItemPrice;
	}

	public String getTheShippingLink() {
		return theShippingLink;
	}

	public String getTheShippingModal() {
		return theShippingModal;
	}

	public String getTheCloseModal() {
		return theCloseModal;
	}

	public String getTheShippingModalUSLink() {
		return theShippingModalUSLink;
	}

	public String getTheShippingModalCALink() {
		return theShippingModalCALink;
	}

	public String getTheShippingModalIntl() {
		return theShippingModalIntl;
	}

	public String getTheShippingModalIntlFAQ() {
		return theShippingModalIntlFAQ;
	}

	public String getTheShippingModalFAQ() {
		return theShippingModalFAQ;
	}

	public String getTheShippingModalText() {
		return theShippingModalText;
	}

	public String getTheShippingModalX() {
		return theShippingModalX;
	}

	public String getTheTaxQuestionMark() {
		return theTaxQuestionMark;
	}

	public String getTheTaxModalX() {
		return theTaxModalX;
	}

	public String getTheTaxModalText() {
		return theTaxModalText;
	}

	public String getTheGuaranteeText() {
		return theGuaranteeText;
	}

	public String getTheSuggestionBox() {
		return theSuggestionBox;
	}

	public String getTheFirstSuggestionLink() {
		return theFirstSuggestionLink;
	}

	public String getTheSecondSuggestionLink() {
		return theSecondSuggestionLink;
	}

	public String getTheThridSuggestionLink() {
		return theThridSuggestionLink;
	}

	public String getTheFirstSuggestionThumbnail() {
		return theFirstSuggestionThumbnail;
	}

	public String getTheSecondSuggestionThumbnail() {
		return theSecondSuggestionThumbnail;
	}

	public String getTheThridSuggestionThumbnail() {
		return theThridSuggestionThumbnail;
	}

	public String getTheSearchInput() {
		return theSearchInput;
	}

	public String getTheSearchBtn() {
		return theSearchBtn;
	}

	public String getTheRefineSearch() {
		return theRefineSearch;
	}

	public String getTheSearchInputValue() {
		return theSearchInputValue;
	}

	public String getTheEmptyShoppingBagText() {
		return theEmptyShoppingBagText;
	}

	public String getTheUrlType() {
		return theUrlType;
	}

	public String getTheFirstGiftLink() {
		return theFirstGiftLink;
	}

	public String getTheSecondGiftLink() {
		return theSecondGiftLink;
	}

	public String getTheThirdGiftLink() {
		return theThirdGiftLink;
	}

	public String getTheFourthGiftLink() {
		return theFourthGiftLink;
	}

	public String getTheByCSSOversizedFreightMessage() {
		return theByCSSOversizedFreightMessage;
	}

	public String getTheOversizedFreightMessageText() {
		return theOversizedFreightMessageText;
	}

	public String getTheByCSSCanadaError() {
		return theByCSSCanadaError;
	}

	public String getTheTraditionalGiftCardCanadaErrorText() {
		return theTraditionalGiftCardCanadaErrorText;
	}

	public String getTheCustomsGiftServiceErrorText() {
		return theCustomsGiftServiceErrorText;
	}

	public String getTheShippingRestrictionText() {
		return theShippingRestrictionText;
	}

	public String getTheGiftServicesErrorText() {
		return theGiftServicesErrorText;
	}

	public String getTheShippingRemovalText() {
		return theShippingRemovalText;
	}

	public String getTheOversizedShippingText() {
		return theOversizedShippingText;
	}

	public String getTheHawaiiShippingText() {
		return theHawaiiShippingText;
	}

	public String getTheFirstShippingRestriction() {
		return theFirstShippingRestriction;
	}

	public String getTheSecondShippingRestriction() {
		return theSecondShippingRestriction;
	}

	public String getTheThirdShippingRestriction() {
		return theThirdShippingRestriction;
	}

	public String getTheFirstRowGiftServicesError() {
		return theFirstRowGiftServicesError;
	}

	public String getTheSecondRowGiftServivesError() {
		return theSecondRowGiftServivesError;
	}

	public String getTheLastRowGiftServicesError() {
		return theLastRowGiftServicesError;
	}

	public String getTheThirdRowShippingRemovalError() {
		return theThirdRowShippingRemovalError;
	}

	public String getTheFourthRowShippingRemovalError() {
		return theFourthRowShippingRemovalError;
	}

	public String getTheFifthRowShippingRemovalError() {
		return theFifthRowShippingRemovalError;
	}

	public String getTheByXPathFlipSideMessage() {
		return theByXPathFlipSideMessage;
	}

	public String getTheByXPath51FlipSideMessage() {
		return theByXPath51FlipSideMessage;
	}

	public String getFirstPrice() {
		return FirstPrice;
	}

	public void setFirstPrice(String firstPrice) {
		FirstPrice = firstPrice;
	}

	public String getSecondPrice() {
		return SecondPrice;
	}

	public void setSecondPrice(String secondPrice) {
		SecondPrice = secondPrice;
	}

	public String getTotalPrice() {
		return TotalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		TotalPrice = totalPrice;
	}

	public String getShippingPrice() {
		return ShippingPrice;
	}

	public void setShippingPrice(String shippingPrice) {
		ShippingPrice = shippingPrice;
	}

	public Double getTheFirstPrice() {
		return TheFirstPrice;
	}

	public void setTheFirstPrice(Double theFirstPrice) {
		TheFirstPrice = theFirstPrice;
	}

	public Double getTheSecondPrice() {
		return TheSecondPrice;
	}

	public void setTheSecondPrice(Double theSecondPrice) {
		TheSecondPrice = theSecondPrice;
	}

	public Double getTheTotalPrice() {
		return TheTotalPrice;
	}

	public void setTheTotalPrice(Double theTotalPrice) {
		TheTotalPrice = theTotalPrice;
	}

	public Double getTheShippingPrice() {
		return TheShippingPrice;
	}

	public void setTheShippingPrice(Double theShippingPrice) {
		TheShippingPrice = theShippingPrice;
	}

	public String getTheSixthRowShippingRemovalError() {
		return theSixthRowShippingRemovalError;
	}

	public String getTheSeventhRowShippingRemovalError() {
		return theSeventhRowShippingRemovalError;
	}

	public String getTheByCSSFlipSideImage() {
		return theByCSSFlipSideImage;
	}

	public String getTheFourthDetailsShippingRestriction() {
		return theFourthDetailsShippingRestriction;
	}

	public String getTheFifthDetailsShippingRestriction() {
		return theFifthDetailsShippingRestriction;
	}

	public String getItem() {
		return item;
	}

	public String getColor() {
		return color;
	}

	public String getSize() {
		return size;
	}

	public String getPrice() {
		return price;
	}

	public String getQty() {
		return qty;
	}

	public String getSubtotal() {
		return subtotal;
	}
	/*
	 * This method checks prices on the shopping bag and makes sure they are
	 * displayed as a US currency. This method is passed the page object and the
	 * selenium and environment objects.
	 */
	// public void checkUSPrices(ISelenium selenium, ShoppingBagObjs
	// myShoppingBagObjs, SeleniumEnvironment myEnvironment)
	// {
	// TheFirstPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.FirstItemPrice));
	// TheSecondPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.SecondItemPrice));
	// TheShippingPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.ShippingItemPrice));
	// TheTotalPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.TotalItemPrice));
	// GiftPrice = selenium.GetText(myShoppingBagObjs.GiftItemPrice);
	// if (GiftPrice.Length > 0)
	// {
	// GiftPrice = GiftPrice.Remove(0, 1);
	// }
	// else
	// {
	// GiftPrice = "0";
	// }
	// Double.TryParse(GiftPrice, out TheGiftPrice);
	// Assert.AreEqual((TheFirstPrice + TheSecondPrice + TheShippingPrice +
	// TheGiftPrice), TheTotalPrice);
	// selenium.Click(myShoppingBagObjs.FirstRemoveLink);
	// selenium.WaitForPageToLoad(myEnvironment.MyDefaultTimeout);
	// Thread.Sleep(myEnvironment.DefaultSleep);
	// myEnvironment.waitForElement(selenium,
	// myShoppingBagObjs.ContinueShopping);
	// TheFirstPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.FirstItemPrice));
	// TheShippingPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.ShippingItemPrice));
	// TheTotalPrice =
	// this.pullPrice(selenium.GetText(myShoppingBagObjs.TotalItemPrice));
	// GiftPrice = selenium.GetText(myShoppingBagObjs.GiftItemPrice);
	// if (GiftPrice.Length > 0)
	// {
	// GiftPrice = GiftPrice.Remove(0, 1);
	// }
	// else
	// {
	// GiftPrice = "0";
	// }
	// Double.TryParse(GiftPrice, out TheGiftPrice);

	// Assert.AreEqual(TheTotalPrice, TheFirstPrice + TheShippingPrice +
	// TheGiftPrice);
	// }
	/*
	 * This method makes sure that the taxes are correct on the Shopping bag.
	 * The strings use the pulled price method to convert the strings to doubles
	 * and then execute the calculations. This method is passed the tax
	 * multiplier that is multiplied by the passed total that should equal the
	 * passed tax.
	 */
	public void checkTaxes(Double taxMultiplier, String Total, String Tax) {
		Double theTax = 0.0;
		Double theTotal = 0.0;
		Double theTaxableTotal = 0.0;
		theTotal = this.pullPrice(Total);
		theTax = this.pullPrice(Tax);
		theTaxableTotal = theTotal - theTax;
		Double theExpectedTax = theTaxableTotal * taxMultiplier;
		Assert.assertEquals(theExpectedTax, theTax);

	}

	/*
	 * This method takes the given string and pulls the price from the string
	 * Then the string is converted to a double and returned.
	 */
	public Double pullPrice(String theString) {
		Double thePrice = 0.0;
		theString = theString.substring(1);
		thePrice = Double.parseDouble(theString);
		return thePrice;
	}

	/*
	 * This method checks to see if the product rows in the shopping bag have a
	 * yellow outline to highlight the restrictions that are present for
	 * restricted items when attempting a canadian order The method is passed
	 * the environment and webdriver objects.
	 */
	public void checkProductsForYellowOutline(int max) {
		WebElement currentElement;
		for (int i = 2; i < max; i++) {// table[@id='shopping-table']/tbody/tr[2]/td
			if (i == 5 & max == 9)
				i = i + 2;
			currentElement = myEnvironment.waitForDynamicElement(
					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td"));
			// myEnvironment.myWaitForWDElement(driver,
			// By.XPath("//table[@id='shopping-table']/tbody/tr[" + i +
			// "]/td"));
			System.out.println(currentElement.getAttribute("style"));
			Assert.assertTrue(currentElement.getAttribute("style").contains(
					"rgb(255, 255, 0)")
					|| currentElement.getAttribute("style").contains("#ffff00"));
			// Console.WriteLine(driver.FindElement(By.XPath("//table[@id='shopping-table']/tbody/tr["
			// + i + "]/td")).GetAttribute("style"));
			// Assert.True(driver.FindElement(By.XPath("//table[@id='shopping-table']/tbody/tr["
			// + i +
			// "]/td")).GetAttribute("style").Contains("rgb(255, 255, 0)"));
		}
	}

	/*
	 * This method checks to see if the product rows in the shopping bag have a
	 * yellow outline to highlight the restrictions that are present for
	 * restricted items when attempting a Fifty One order NOTE: I do not
	 * remember but I do not think that this is a valid method. I believe I made
	 * this under assumptions that were not correct. The method is passed the
	 * environment and webdriver objects.
	 */
	public void checkFiftyOneProductsForYellowOutline() {

		for (int i = 2; i < 8; i = i + 2) {
			myEnvironment.waitForDynamicElement(

					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td"));

			System.out.println(driver.findElement(
					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td")).getAttribute("style"));
			Assert.assertTrue(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td")).getAttribute("style")
					.contains("rgb(255, 255, 0)"));
		}
	}

	/*
	 * this method checks the rows in the shopping bag for the restriction links
	 * that should be present on a canadian order for certain products. This is
	 * commmented out because Merch was having issues displaying correctly under
	 * the webdriver test. The method is passed the webdriver and environment
	 * object.
	 */
	public void checkProductsForShippingRestrictionLink() {
		for (int i = 4; i < 6; i++) {// table[@id='shopping-table']/tbody/tr[2]/td
			myEnvironment.waitForDynamicElement(
					By.xpath("//table[@id='shopping-table']/tbody/tr[" + i
							+ "]/td[2]/div"));

			System.out.println(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td[2]/div"))
					.findElement(By.tagName("a")).getText());
			Assert.assertTrue(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td[2]/div")).getText()
					.contains("Shipping restrictions apply"));
			Assert.assertTrue(driver
					.findElement(
							By.xpath("//table[@id='shopping-table']/tbody/tr["
									+ i + "]/td[2]/div"))
					.findElement(By.tagName("a")).getText() == "DETAILS");
		}
	}

	public void removeProducts(int products) throws InterruptedException {
		for (int i = 0; i <= products; i++) {
			WebElement ce;
			ce = myEnvironment.waitForDynamicElement(
					By.xpath(this.getTheThirdRemoveLink()));
			ce.click();
			Thread.sleep(myEnvironment.getThe_Special_Sleep());
		}
	}

	public void removeAllProducts(int products) throws InterruptedException {

		for (int i = 0; i < products; i++) {
			WebElement ce;

			if (i == products - 1
					&& myEnvironment.IsElementPresent(
							By.xpath("//img[@alt='PGC880001'])[2]"))) {
				ce = myEnvironment.waitForDynamicElement(
						By.xpath("//a[contains(text(),'Remove')]"));
				ce.click();
				Thread.sleep(10000);
			}

			else {

				ce = myEnvironment.waitForDynamicElement(
						By.xpath(this.getTheFirstRemoveLink()));
				ce.click();
				Thread.sleep(10000);

			}
		}

	}



	public void checkShoppingBagColumns(WebDriver driver) {
		// TODO Auto-generated method stub
		Assert.assertEquals(
				this.getItem(),
				driver.findElement(
						By.xpath(this.getTheItemColumn()))
						.getText());
		Assert.assertEquals(
				this.getColor(),
				driver.findElement(
						By.xpath(this.getTheColorColumn()))
						.getText());
		Assert.assertEquals(
				this.getSize(),
				driver.findElement(
						By.xpath(this.getTheSizeColumn()))
						.getText());
		Assert.assertEquals(
				this.getPrice(),
				driver.findElement(
						By.xpath(this.getThePriceColumn()))
						.getText());
		Assert.assertEquals(
				this.getQty(),
				driver.findElement(
						By.xpath(this.getTheQtyColumn()))
						.getText());
		Assert.assertEquals(
				this.getSubtotal(),
				driver.findElement(
						By.xpath(this.getTheSubTotalColumn()))
						.getText());
	}

	public String getThePageName() {
		return thePageName;
	}

	public String getTheCandaErrorColor() {
		return theCandaErrorColor;
	}

	public String getTheShippingErrorColor() {
		return theShippingErrorColor;
	}

	public String getTheChromeShippingErrorColor() {
		return theChromeShippingErrorColor;
	}

	public String getTheShippingRestriction() {
		return theShippingRestriction;
	}

	public String getTheLineItemShippingRestrictionText() {
		return theLineItemShippingRestrictionText;
	}

	public String getTheDetailsLinkText() {
		return theDetailsLinkText;
	}

	public String getTheIECanadaErrorColor() {
		return theIECanadaErrorColor;
	}

	public String getTheByIdBottomContinueCheckout() {
		return theByIdBottomContinueCheckout;
	}

}
