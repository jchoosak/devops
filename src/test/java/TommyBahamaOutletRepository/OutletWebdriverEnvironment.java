package TommyBahamaOutletRepository;



import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.gargoylesoftware.htmlunit.javascript.host.Screen;
import com.google.common.base.Function;
import com.thoughtworks.selenium.Selenium;

/// <summary>
/// This the Enviroment class that performs main actions with the testing enviroment of where 
/// the test is currently testing and other global actions on web elements. 
/// the TraversLink methods will be moved to the ProductListingObjs class. 
/// I also need to perform a major naming convention overhaul on this testing platform. 
/// </summary>
public class OutletWebdriverEnvironment {

	// this is the enviroment that the test will open to.
	private String environment = "http://ecap03/store/index_intl.jsp";

	
	private final String chromeWebdriver = "C:\\Users\\jwest\\Desktop\\chromedriver.exe";
	private final String ieWebdriver = "C:\\Users\\jwest\\Desktop\\IEDriverServer.exe";
	private final String ffProfile = "C:\\Users\\jwest\\Desktop\\Profile";
	// this is for the old selenium Remote Control to let it know what browswer
	// to use
	private String browser = "*chrome";
	private final int the_Traverse_Link_Sleep = 300;
	private final int port = 3333;
	private int timer = 0;
	// universal testing account pswrd
	
    private final String theVisa = "4111111111111111";
    private final String theMaster = "5555555555554444";
    // universal testing account pswrd
 
    private String passWrd = "testing123";
    private String theOldPaswrd = "testing123";
    private String theNewPaswrd = "testing1234";
	
	
	// response from server error'
	private final int the_Default_Sleep = 5000;
	// this is for special cases where the action needs to wait. Very bad to use
	// this but sometimes its good before finding elegant answer
	private final int the_Special_Sleep = 8000;
	private final String serverHost = "localhost";
	


	// this is the enviroment that the test will open to. Redundent
	//private final String theTestingEnvironment = "http://www.tommybahama.com/";
	//private final String theTestingEnvironment = "http://test.tommybahama.com/";
    //private final String theTestingEnvironment = "https://outlet.tommybahama.com/en/login";
  //  private final String theTestingEnvironment = "http://ecweb01/store/index_intl.jsp";
	private String theTestingEnvironment = "https://dev-outlet.tommybahama.com/en/login";
	
	//http://csc.tbahama.com/ytbcscockpit/login.zul
	//private final String theOutletCSCTestingEnvironment = "http://www.tommybahama.com/";
	//private final String theOutletCSCTestingEnvironment = "http://csc.tbahama.com/ytbcscockpit/login.zul";
	
	

	private String theOutletCSCTestingEnvironment = "http:/dev-csc.tbahama.com/ytbcscockpit/login.zul";
	
	// the different environments for the tommybahama web site.
	
	private final String theQAEnvironment = "https://qa-outlet.tommybahama.com/en/login";

	private final String theProductionEnvironment = "https://outlet.tommybahama.com/en/login";

	// will prob move this to the address page.
	private String theUsShopperFName = "Jack";
	private String theUsShopperMName = "B";
	private String theUsShopperLName = "West";
	private final String theEcommEmailOne = "ecommtest@tommybahama.com";
	private final String theEcommEmailTwo = "ecommtest2@tommybahama.com";
	private final String theEcommEmailFive = "ecommtest5@tommybahama.com";

	// private String theEcommEmailSix = "ecommtest6@tommybahama.com";
	private String theSiteBody = "//body";
	private final String ffBrowser = "FF";
	private final String ieBrowser = "IE9";
	private final String chromeBrowser = "Chrome";
	private final String safariBrowser = "Safari";

	// private String chromeBrowser = "chrome";
	private final String ffErrorColor = "168, 49, 39";
	private final String ieErrorColor = "#a83127";
	private final String ieGiftCardDiscountColor = "rgba(153, 51, 51, 1)";

	private final String jackEmail = "jack.west@tommybahama.com";
	private final String karenEmail = "Karen.Fall@tommybahama.com";
	private final String kalliEmail = "Kalli.Hubel@tommybahama.com";
	private final String lonEmail = "Lon.Frey@tommybahama.com";
	private final String dougEmail = "Doug.Nichols@tommybahama.com";
	private final String robertEmail = "Robert.Grant@tommybahama.com";

	private final String jackTitle = "Jack West, QA Tester";
	private final String karenTitle = "Karen Fall, Functional Analyst";
	private final String kalliTitle = "Kalli Hubel, Systems and Development Program Manager";
	private final String lonTitle = "Lon Frey, Director of Development for eCommerce";
	private final String dougTitle = "Doug Nichols, Sr. Director of Digital Strategy and Architecture";
	private final String robertTitle = "Web Developer, eCommerce Web";
	private final String browserUsedString = "The browser that was used for this test is          <br/>";
    private final String environmentUsedString = "The testing environment used for this test was:          <br/>";
	
	
	private String testSubject = "";

	private String networkTestDirectory = "f:\\eCommerce\\Jack\\JavaTests\\";
//	private String networkTestDirectory = "f:\\eCommerce\\Jack\\JavaTests\\";
	private String testTextDescription = "Above each screenshot is a textual description of what page on the website the screenshot is displaying. If the text and the screenshot do not match or multiple images are missing then the test failed. If an individual image is not displaying correctly in the email, click on the directory link above the image to view it. Alternatively, each screenshot can be viewed at: <b> F:\\eCommerce\\Jack\\JavaTests\\";
	private String networkFile = "";
	
	private boolean testPassed = false;
	
	
	private WebDriver driver;
	
	public OutletWebdriverEnvironment() 
	{
		
	}
	
	
	public OutletWebdriverEnvironment(WebDriver theDriver) 
	{
		driver = theDriver;
	}
	
	public boolean getTestPassed(){
		return testPassed;
	}
	public void setTestPassed(boolean b) {
		// TODO Auto-generated method stub
		testPassed = b;
	
	}
	
	public String getNetworkTestDirectory() {
		return networkTestDirectory;
	}

	public void setNetworkTestDirectory(String networkTestDirectory) {
		this.networkTestDirectory = networkTestDirectory;
	}

	public String getJackTitle() {

		return jackTitle;
	}

	public String getKarenTitle() {
		return karenTitle;
	}

	public String getKalliTitle() {
		return kalliTitle;
	}

	public String getLonTitle() {
		return lonTitle;
	}

	public String getDougTitle() {
		return dougTitle;
	}

	public String getKarenEmail() {
		return karenEmail;
	}

	public String getKalliEmail() {
		return kalliEmail;
	}

	public String getLonEmail() {
		return lonEmail;
	}

	public String getDougEmail() {
		return dougEmail;
	}

	public String getSafariBrowser() {
		return safariBrowser;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public int getThe_Traverse_Link_Sleep() {
		return the_Traverse_Link_Sleep;
	}

	public int getPort() {
		return port;
	}

	public String getPassWrd() {
		return passWrd;
	}

	public int getThe_Default_Sleep() {
		return the_Default_Sleep;
	}

	public int getThe_Special_Sleep() {
		return the_Special_Sleep;
	}

	public String getServerHost() {
		return serverHost;
	}

	public String getTheTestingEnvironment() {
		return theTestingEnvironment;
	}



	public String getTheQAEnvironment() {
		return theQAEnvironment;
	}

	

	public String getTheProductionEnvironment() {
		return theProductionEnvironment;
	}

	public String getTheUsShopperFName() {
		return theUsShopperFName;
	}

	public void setTheUsShopperFName(String theUsShopperFName) {
		this.theUsShopperFName = theUsShopperFName;
	}

	public String getTheUsShopperMName() {
		return theUsShopperMName;
	}

	public void setTheUsShopperMName(String theUsShopperMName) {
		this.theUsShopperMName = theUsShopperMName;
	}

	public String getTheUsShopperLName() {
		return theUsShopperLName;
	}

	public void setTheUsShopperLName(String theUsShopperLName) {
		this.theUsShopperLName = theUsShopperLName;
	}

	public String getTheEcommEmailOne() {
		return theEcommEmailOne;
	}

	public String getTheEcommEmailTwo() {
		return theEcommEmailTwo;
	}

	public String getTheEcommEmailFive() {
		return theEcommEmailFive;
	}

	public String getTheSiteBody() {
		return theSiteBody;
	}

	public void setTheSiteBody(String theSiteBody) {
		this.theSiteBody = theSiteBody;
	}

	public String getFfBrowser() {
		return ffBrowser;
	}

	public String getIeBrowser() {
		return ieBrowser;
	}

	public String getFfErrorColor() {
		return ffErrorColor;
	}

	public String getIeErrorColor() {
		return ieErrorColor;
	}

	public String getIeGiftCardDiscountColor() {
		return ieGiftCardDiscountColor;
	}

	public String getJackEmail() {
		return jackEmail;
	}

	public String getTestSubject() {
		return testSubject;
	}

	public void setTestSubject(String testSubject) {
		this.testSubject = testSubject;
	}

	public String getTestTextDescription() {
		return testTextDescription;
	}

	public void setTestTextDescription(String testTextDescription) {
		this.testTextDescription = testTextDescription;
	}

	public String getNetworkFile() {
		return networkFile;
	}

	public void setNetworkFile(String networkFile) {
		this.networkFile = networkFile;
	}
	
	public String getRobertTitle() {
		return robertTitle;
	}

	public String getRobertEmail() {
		return robertEmail;
	}

	public String getTheOldPaswrd() {
		return theOldPaswrd;
	}

	public void setTheOldPaswrd(String theOldPaswrd) {
		this.theOldPaswrd = theOldPaswrd;
	}

	public String getTheNewPaswrd() {
		return theNewPaswrd;
	}

	public void setTheNewPaswrd(String theNewPaswrd) {
		this.theNewPaswrd = theNewPaswrd;
	}

	public String getTheVisa() {
		return theVisa;
	}

	public String getTheMaster() {
		return theMaster;
	}
	
	public String getBrowserUsedString() {
		return browserUsedString;
	}

	public String getEnvironmentUsedString() {
		return environmentUsedString;
	}
	
	public String getTheOutletCSCTestingEnvironment() {
		return theOutletCSCTestingEnvironment;
	}
	
	public void setTheTestingEnvironment(String theTestingEnvironment) {
		this.theTestingEnvironment = theTestingEnvironment;
	}


	public void setTheOutletCSCTestingEnvironment(
			String theOutletCSCTestingEnvironment) {
		this.theOutletCSCTestingEnvironment = theOutletCSCTestingEnvironment;
	}

	// / <summary>
	// / webdriver boolean method to verify element is present
	// / this method is passed the driver and the BY type which is the CSS type.
	// / </summary>
	// / <param name="by"></param>
	// / <param name="driver"></param>
	// / <returns></returns>
	public boolean IsElementPresent(By by) {
		System.out
				.println("Checking if this element is present:                        "
						+ by.toString());
		try {
			Thread.sleep(the_Traverse_Link_Sleep);
			driver.findElement(by);

			return true;
		} catch (Exception e) {

			return false;
		}
	}

	public WebElement fluentWait( final By locator) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return driver.findElement(locator);
			}
		});
		return foo;
	};

	// / <summary>
	// / this is the standard Implicit wait for an element
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="by"></param>
	// / <returns>WebElement</returns>
	public WebElement waitForDynamicElement( By by) {

		WebElement myDynamicElement;
		myDynamicElement = (new WebDriverWait(driver, 90))
				.until(ExpectedConditions.presenceOfElementLocated(by));

		System.out.println("Found the next element:                        "
				+ by.toString());

		return myDynamicElement;
	}

	// / <summary>
	// / this is the standard Implicit wait for an element
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="by"></param>
	// / <returns>WebElement</returns>
	public WebElement waitForClickableElement( By by) {
        try {
		WebElement myDynamicElement;
		myDynamicElement = (new WebDriverWait(driver, 0))
				.until(ExpectedConditions.elementToBeClickable(by));
        
		return myDynamicElement;
		
        }
        
        catch (Exception TimeoutException) {
        return null;
        }
	}

	public void waitForPageLoaded() {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 120);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.assertFalse("Timeout waiting for Page Load Request to complete.",
					true);
		}
	}

	// / <summary>
	// / this method generates a random string to be added to a valid email
	// address ending
	// / passed the length of the string to be made.
	// / </summary>
	// / <param name="length"></param>
	// / <returns>String</returns>
	public String generateRandomString(int length) {
		// Initiate objects & vars
		Random random = new Random();
		String randomString = "@tommyqa.com";
		int randNumber;
		// Loop ‘length’ times to generate a random number or character
		for (int i = 0; i < length; i++) {
			if ((random.nextInt(3) + 1) == 1)
				randNumber = random.nextInt(123) + 97; // char {a-z}
			else
				randNumber = random.nextInt(90) + 65; // int {A-Z}
			// append random char or digit to random string
			randomString = (char) randNumber + randomString;
		}
		// return the random string
		return randomString;
	}

	
	// / <summary>
	// / This method takes a screen shot of the current page. This Screen shot
	// is of the entire page but does NOT include
	// / the browser. I have passed multiple strings to add information to the
	// name of the SS so it gives information.
	// / I may want to change pageDescription to browser or add another string.
	// / </summary>
	// / <param name="webDriver"></param>
	// / <param name="testName"></param>
	// / <param name="pageName"></param>
	// / <param name="pageDescription"></param>
	public void TakeSs( String browserName,
			String testName, String pageName, String pageDescription) {

		String createdFolderLocation = "F:\\eCommerce\\Jack\\JavaTests\\"
				+ browserName + "/" + testName + "/" + pageName
				+ pageDescription + ".Jpeg";

		System.out
				.println("This is the location for the current screenshot:        "
						+ createdFolderLocation);
		// Take the screenshot

		File screenshot = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
	
		try {
			FileUtils.copyFile(screenshot, new File(createdFolderLocation));
			// Thread.sleep(1000);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// / <summary>
	// / This method takes a screen shot of the current page. This Screen shot
	// is of the entire page but does NOT include
	// / the browser. I have passed multiple strings to add information to the
	// name of the SS so it gives information.
	// / I may want to change pageDescription to browser or add another string.
	// / </summary>
	// / <param name="webDriver"></param>
	// / <param name="testName"></param>
	// / <param name="pageName"></param>
	// / <param name="pageDescription"></param>
	public void TakeScreenShot( String browserName,
			String testName, String pageName, String pageDescription) {

		
		String createdFolderLocation = "../seleniumhq/" + pageName
				+ pageDescription + ".jpeg";

		System.out
				.println("This is the location for the current screenshot:        "
						+ createdFolderLocation);
	
		File screenshot = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
	
		try {
			FileUtils.copyFile(screenshot, new File(createdFolderLocation));
			// Thread.sleep(1000);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// / <summary>
	// / This is a method to move the mouse somewhere where it does not affect
	// the test by
	// / activating popup menus. the hover popups get in the way of other
	// actions
	// / </summary>
	// / <param name="driver"></param>
	// / <param name="by"></param>
	public void hoverTo( By by) {

		WebElement ce = driver.findElement(by);
		System.out.println("Moving to hover over 	" + ce);
		Actions action = new Actions(driver);// simply my webdriver
		action.moveToElement(ce).perform();// move to list element that needs to
											// be hovered
	}

	public String getChromeBrowser() {
		return chromeBrowser;
	}

	public void sendTestResultEmail(BodyPart htmlPart, String subject, String toAddress,
			String toTitle, String fromAddress, String fromTitle			
			) throws MessagingException,
			UnsupportedEncodingException {
		// setup the smtp
		Properties props = new Properties();
		props.put("mail.smtp.host", "mail02sea.tbahama.com");
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.socketFactory.port", "25");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.debug", "true");
		// create the session
		Session session = Session.getInstance(props);
		// Build the message and pass the session
		Message msg = new MimeMessage(session);
		msg.setSubject(subject);
		
		InternetAddress to = new InternetAddress(toAddress, toTitle);
		InternetAddress from = new InternetAddress(fromAddress, fromTitle);
		msg.addRecipient(Message.RecipientType.TO, to);
		msg.setFrom(from);
		Multipart multipart = new MimeMultipart("related");
		// add the html
		multipart.addBodyPart(htmlPart);

		msg.setContent(multipart);
		Transport transport = session.getTransport("smtp");
		transport.connect();
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();
	}

	public void waitForDocumentReadyState() {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 60);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.assertFalse("Timeout waiting for Page Load Request to complete.",
					true);
		}
	}


	

	public void handleMultipleWindows(String windowTitle) {
         Set<String> windows = driver.getWindowHandles();

         for (String window : windows) {
        	 System.out.println("These are the windows available           :"   + window);
             driver.switchTo().window(window);
             System.out.println("this is the titles of the windows:              "  + driver.getTitle());
             if (driver.getTitle().contains(windowTitle)) {
                 return;
             }
         }
     }
	
	public void handleMultipleWindowsWithSameTitle(String windowTitle) {
        Set<String> windows = driver.getWindowHandles();

 
     
        for (String window : windows) {
       	 System.out.println("These are the windows available           :"   + window);
         
       	 int theSpot = window.lastIndexOf(windowTitle);
       	 
       	 driver.switchTo().frame(theSpot);
            System.out.println("this is the titles of the windows:              "  + driver.getTitle());
            if (driver.getTitle().contains(windowTitle)) {
                return;
            }
        }
    }
	
	public void waitForTitle(String theTitle)
	{
		while (!driver.getTitle().contains(theTitle))
		{
		System.out.println("Waiting for this title:        " + theTitle);
		System.out.println("This is the current title:          " + driver.getTitle());
		timer = timer + 1;
			if (timer > 1000)
			{
				timer = 0;
				break;
			}
		}
	}
	
	public void waitForURL(String theTitle)
	{
		while (!driver.getCurrentUrl().contains(theTitle))
		{
		System.out.println("Waiting for this URL:        " + theTitle);
		System.out.println("This is the current URL:          " + driver.getCurrentUrl());
		timer = timer + 1;
			if (timer > 1000)
			{
				timer = 0;
				break;
			}
		}
	}

	public String addSS(String testName, String thePage, String theDescription) {
		// TODO Auto-generated method stub
		String ss = "<h3>" + theDescription +  "</h3>"
				+ "<h4><a href="
				+ this.getNetworkFile()
				+ "\\" + thePage +  ".jpeg>"
				+ this.getNetworkTestDirectory()
				+ this.getBrowser()
				+ "\\"
				+ testName
				+ "\\" + thePage +  ".jpeg> </a><br/>"
				+ "<img src=\"cid:" + thePage + "\""  + "/><br/>";
				
		return ss;
	}
	
	public String addSSToFile(String testName, String thePage, String theDescription) {
		// TODO Auto-generated method stub
		String ss = "<h3>" + theDescription +  "</h3>"
				+ "<h4><a href="
				+ testName
				+ "/" + thePage +  ".jpeg>"
				+ testName
				+ "/" + thePage +  ".jpeg> </a><br/>"
				+ "<img src=\"" + thePage +  ".jpeg\""  + "/><br/>";
				
		return ss;
	}

	

	public String getPageTestOutcome() {
		if(testPassed == true)
		return "This test passed!";
		else return "THIS TEST FAILED!";
	}
	
	public void removeSS(List<String> fns, String tempFile)
	{
		for (int i = 1; i <= fns.size(); i++) {
			
			File fileTemp = new File(tempFile + "/" + fns.get(i - 1) + ".jpeg");
			
			System.out.println(fileTemp);
			if (fileTemp.exists()){					
				System.out.println("This file location will be deleted!!!!!!!!!!");
				System.out.println(fileTemp);
			    fileTemp.delete();
			}
			
		}
	}
	
	
	
	public void waitUntilJQueryProcessingHasCompleted(int timeout) {

	    new WebDriverWait(driver, timeout) {

	    }.until(new ExpectedCondition<Boolean>() {

	        public Boolean apply(WebDriver driver) {

	            boolean jQueryActive = (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");

	            return jQueryActive;

	        }

	    });

	}

	public void waitUntilJQueryProcessingHasCompleted() {
	List<WebElement> allImages = driver.findElements(By.tagName("img"));
	for (WebElement image : allImages) {
	  boolean loaded = (Boolean) ((JavascriptExecutor) driver).executeScript(
	      "return arguments[0].complete", image);
	
	  if (!loaded) {
	    // Your error handling here.
		  Assert.assertTrue(loaded = true);
	  }
	}
	}
	
	
	public void checkForBrokenImages() {	
		
	    int imageCount = ((Selenium) driver).getXpathCount("//img").intValue();
	    for (int i = 0; i < imageCount; i++) {
	        String currentImage = "this.browserbot.getUserWindow().document.images[" + i + "]";
	        Assert.assertEquals(((Selenium) driver).getEval("(!" + currentImage + ".complete) ? false : !(typeof " + currentImage + ".naturalWidth != \"undefined\" && " + currentImage + ".naturalWidth == 0);"), "true", "Broken image: " + ((Selenium) driver).getEval(currentImage + ".src"));
		}
	}

	public String getChromeWebdriver() {
		return chromeWebdriver;
	}

	public String getIeWebdriver() {
		return ieWebdriver;
	}

	public String getFfProfile() {
		return ffProfile;
	}



//	\\nas01sea\public\

}