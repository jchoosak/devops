package TommyBahamaOutletRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class OutletCustomerTab {
	
	private final String theByXpathPasswordResetButton = "//div[3]/span/table/tbody/tr[2]/td[2]";
	private final String theByXpathAddAddressButton = "//div[2]/div/div[3]/span/table/tbody/tr[2]/td[2]";
	private final String theByXpathAddAddressFirstName = "//div[2]/div/div/div/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressLastName = "//div[2]/div/div/div[2]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressStreet1 = "//div[3]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressStreet2 = "//div[4]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressCity = "//div[5]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressState = "//table[2]/tbody/tr/td[3]/select";
    private final String theByXpathAddAddressZip = "//div[6]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressPhone = "//div[7]/table/tbody/tr/td[3]/div/input";
    private final String theByXpathAddAddressCreateButton = "//div[2]/div/div[2]/span/table/tbody/tr[2]/td[2]";
    
    private final String theByXpathCustomerStreetAddress = "//td[3]/div/div/table/tbody/tr/td/div/input";
    private final String theByXpathCustomerPhone = "//td[9]/div/div/table/tbody/tr/td/div/input";
    private final String theByXpathCustomerAddressDeleteButton = "//td[10]/div/span/table/tbody/tr[2]/td[2]";
    private final String theByXpathCustomerConfirmDeleteButton = "//td/span/table/tbody/tr[2]/td[2]";
    private final String theByXpathCustomerDefaultAddressDropdown = "//div[2]/select";
    private final String theByXpathCustomerRefreshButton = "//div[2]/span/table/tbody/tr[2]/td[2]";
	
    private final String theByXpathPersonalDetailsName = "//div/input";
    
    private OutletWebdriverEnvironment myEnvironment;
	private String testName;
	
	
    public OutletCustomerTab(OutletWebdriverEnvironment theEnvironment, String theTestName) {
    	
    	myEnvironment = theEnvironment; 
    	testName = theTestName;
    }
    
    public String getTheByXpathPasswordResetButton() {
		return theByXpathPasswordResetButton;
	}




	public String getTheByXpathAddAddressButton() {
		return theByXpathAddAddressButton;
	}




	public String getTheByXpathAddAddressFirstName() {
		return theByXpathAddAddressFirstName;
	}




	public String getTheByXpathAddAddressLastName() {
		return theByXpathAddAddressLastName;
	}




	public String getTheByXpathAddAddressStreet1() {
		return theByXpathAddAddressStreet1;
	}




	public String getTheByXpathAddAddressStreet2() {
		return theByXpathAddAddressStreet2;
	}




	public String getTheByXpathAddAddressCity() {
		return theByXpathAddAddressCity;
	}




	public String getTheByXpathAddAddressState() {
		return theByXpathAddAddressState;
	}




	public String getTheByXpathAddAddressZip() {
		return theByXpathAddAddressZip;
	}




	public String getTheByXpathAddAddressPhone() {
		return theByXpathAddAddressPhone;
	}




	public String getTheByXpathAddAddressCreateButton() {
		return theByXpathAddAddressCreateButton;
	}

	
	public void addAddress() {
	
		WebElement ce;
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressButton()));
		ce.click();
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressFirstName()));
				
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressFirstName()));
		ce.sendKeys("Jack");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressLastName()));
		ce.sendKeys("West");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressStreet1()));
		ce.sendKeys("428 WESTLAKE AVE N");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressStreet2()));
		ce.sendKeys("Suite #400");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressCity()));
		ce.sendKeys("Seattle");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressState()));
		Select clickThis = new Select(ce);
		clickThis.selectByIndex(48);
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressZip()));
		ce.sendKeys("98109");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(getTheByXpathAddAddressPhone()));
		ce.sendKeys("2532563698");
		
		myEnvironment.TakeScreenShot( myEnvironment.getIeBrowser(),
				testName, "CustomerAddAddressForm", "");
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheByXpathAddAddressCreateButton()));
		
		ce = myEnvironment.waitForDynamicElement(
				By.xpath(this.getTheByXpathAddAddressCreateButton()));
		ce.click();
		
	}

	public String getTheByXpathCustomerStreetAddress() {
		return theByXpathCustomerStreetAddress;
	}

	public String getTheByXpathCustomerAddressDeleteButton() {
		return theByXpathCustomerAddressDeleteButton;
	}

	public String getTheByXpathCustomerConfirmDeleteButton() {
		return theByXpathCustomerConfirmDeleteButton;
	}

	public String getTheByXpathCustomerPhone() {
		return theByXpathCustomerPhone;
	}

	public String getTheByXpathCustomerDefaultAddressDropdown() {
		return theByXpathCustomerDefaultAddressDropdown;
	}

	public String getTheByXpathCustomerRefreshButton() {
		return theByXpathCustomerRefreshButton;
	}

	public String getTheByXpathPersonalDetailsName() {
		return theByXpathPersonalDetailsName;
	}
}